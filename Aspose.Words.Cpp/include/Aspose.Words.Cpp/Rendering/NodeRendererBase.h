//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Rendering/NodeRendererBase.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <drawing/size_f.h>
#include <drawing/size.h>
#include <drawing/rectangle_f.h>
#include <drawing/rectangle.h>
#include <drawing/graphics.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class ImageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeResourceWriter; } } } } }
namespace Aspose { namespace Rendering { namespace Aps { class ApsCanvas; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class IActualBoundsProvider; } } } }
namespace Aspose { namespace Words { namespace Saving { class ImageSaveOptions; } } }

namespace Aspose {

namespace Words {

namespace Rendering {

/// Base class for <see cref="Aspose::Words::Rendering::ShapeRenderer">ShapeRenderer</see> and <see cref="Aspose::Words::Rendering::OfficeMathRenderer">OfficeMathRenderer</see>.
class ASPOSE_WORDS_SHARED_CLASS NodeRendererBase : public System::Object
{
    typedef NodeRendererBase ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::ImageShapeWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeResourceWriter;

public:

    /// Gets the actual size of the shape in points.
    /// 
    /// This property returns the size of the actual (as rendered on the page) bounding box of the shape.
    /// The size takes into account shape rotation (if any).
    ASPOSE_WORDS_SHARED_API System::Drawing::SizeF get_SizeInPoints();
    /// Gets the actual bounds of the shape in points.
    /// 
    /// This property returns the actual (as rendered on the page) bounding box of the shape.
    /// The bounds takes into account shape rotation (if any).
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF get_BoundsInPoints();
    /// Gets the opaque bounds of the shape in points.
    /// 
    /// This property returns the opaque (i.e. transparent parts of the shape are ignored) bounding box of the shape.
    /// The bounds takes the shape rotation into account.
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF get_OpaqueBoundsInPoints();

    /// Calculates the size of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_SizeInPoints">SizeInPoints</see> into size in pixels and it is useful
    /// when you want to create a bitmap for rendering the shape neatly onto the bitmap.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param dpi The resolution (horizontal and vertical) to convert from points to pixels (dots per inch).
    /// 
    /// @return The size of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Size GetSizeInPixels(float scale, float dpi);
    /// Calculates the size of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_SizeInPoints">SizeInPoints</see> into size in pixels and it is useful
    /// when you want to create a bitmap for rendering the shape neatly onto the bitmap.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param horizontalDpi The horizontal resolution to convert from points to pixels (dots per inch).
    /// @param verticalDpi The vertical resolution to convert from points to pixels (dots per inch).
    /// 
    /// @return The size of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Size GetSizeInPixels(float scale, float horizontalDpi, float verticalDpi);
    /// Calculates the bounds of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_BoundsInPoints">BoundsInPoints</see> into rectangle in pixels.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param dpi The resolution (horizontal and vertical) to convert from points to pixels (dots per inch).
    /// 
    /// @return The actual (as rendered on the page) bounding box of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Rectangle GetBoundsInPixels(float scale, float dpi);
    /// Calculates the bounds of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_BoundsInPoints">BoundsInPoints</see> into rectangle in pixels.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param horizontalDpi The horizontal resolution to convert from points to pixels (dots per inch).
    /// @param verticalDpi The vertical resolution to convert from points to pixels (dots per inch).
    /// 
    /// @return The actual (as rendered on the page) bounding box of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Rectangle GetBoundsInPixels(float scale, float horizontalDpi, float verticalDpi);
    /// Calculates the opaque bounds of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_OpaqueBoundsInPoints">OpaqueBoundsInPoints</see> into rectangle in pixels and it is useful
    /// when you want to create a bitmap for rendering the shape with only opaque part of the shape.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param dpi The resolution to convert from points to pixels (dots per inch).
    /// 
    /// @return The opaque rectangle of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Rectangle GetOpaqueBoundsInPixels(float scale, float dpi);
    /// Calculates the opaque bounds of the shape in pixels for a specified zoom factor and resolution.
    /// 
    /// This method converts <see cref="Aspose::Words::Rendering::NodeRendererBase::get_OpaqueBoundsInPoints">OpaqueBoundsInPoints</see> into rectangle in pixels and it is useful
    /// when you want to create a bitmap for rendering the shape with only opaque part of the shape.
    /// 
    /// @param scale The zoom factor (1.0 is 100\%).
    /// @param horizontalDpi The horizontal resolution to convert from points to pixels (dots per inch).
    /// @param verticalDpi The vertical resolution to convert from points to pixels (dots per inch).
    /// 
    /// @return The opaque rectangle of the shape in pixels.
    ASPOSE_WORDS_SHARED_API System::Drawing::Rectangle GetOpaqueBoundsInPixels(float scale, float horizontalDpi, float verticalDpi);
    /// Renders the shape into a <see cref="System::Drawing::Graphics">Graphics</see>
    /// object to a specified scale.
    /// 
    /// @param graphics The object where to render to.
    /// @param x The X coordinate (in world units) of the top left corner of the rendered shape.
    /// @param y The Y coordinate (in world units) of the top left corner of the rendered shape.
    /// @param scale The scale for rendering the shape (1.0 is 100\%).
    /// 
    /// @return The width and height (in world units) of the rendered shape.
    ASPOSE_WORDS_SHARED_API System::Drawing::SizeF RenderToScale(System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float scale);
    /// Renders the shape into a <see cref="System::Drawing::Graphics">Graphics</see>
    /// object to a specified size.
    /// 
    /// @param graphics The object where to render to.
    /// @param x The X coordinate (in world units) of the top left corner of the rendered shape.
    /// @param y The Y coordinate (in world units) of the top left corner of the rendered shape.
    /// @param width The maximum width (in world units) that can be occupied by the rendered shape.
    /// @param height The maximum height (in world units) that can be occupied by the rendered shape.
    /// 
    /// @return The scale that was automatically calculated for the rendered shape to fit the specified size.
    ASPOSE_WORDS_SHARED_API float RenderToSize(System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float width, float height);
    /// Renders the shape into an image and saves into a file.
    /// 
    /// @param fileName The name for the image file. If a file with the specified name already exists, the existing file is overwritten.
    /// @param saveOptions Specifies the options that control how the shape is rendered and saved. Can be null.
    ASPOSE_WORDS_SHARED_API void Save(System::String fileName, System::SharedPtr<Aspose::Words::Saving::ImageSaveOptions> saveOptions);
    /// Renders the shape into an image and saves into a stream.
    /// 
    /// @param stream The stream where to save the image of the shape.
    /// @param saveOptions Specifies the options that control how the shape is rendered and saved. Can be null.
    ///     If this is null, the image will be saved in the PNG format.
    ASPOSE_WORDS_SHARED_API void Save(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::Saving::ImageSaveOptions> saveOptions);

    ASPOSE_WORDS_SHARED_API NodeRendererBase();

protected:

    System::SharedPtr<Aspose::Rendering::Aps::ApsCanvas> get_Aps() const;
    void set_Aps(System::SharedPtr<Aspose::Rendering::Aps::ApsCanvas> value);
    System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const;
    void set_Document(System::SharedPtr<Aspose::Words::DocumentBase> value);

    void SetActualBoundsProvider(System::SharedPtr<Aspose::Words::ApsBuilder::Shapes::IActualBoundsProvider> result);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::ApsBuilder::Shapes::IActualBoundsProvider> mActualBoundsProvider;
    System::SharedPtr<Aspose::Rendering::Aps::ApsCanvas> mCanvas;
    System::Drawing::RectangleF mOpaqueBounds;
    System::SharedPtr<Aspose::Words::DocumentBase> mDocument;

    static float FixTranslateComponent(float sourceTranslate);
    void SetOpaqueBounds();

};

}
}
}
