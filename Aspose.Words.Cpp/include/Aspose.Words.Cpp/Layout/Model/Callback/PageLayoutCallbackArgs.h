//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Layout/Model/Callback/PageLayoutCallbackArgs.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Layout/Model/Callback/PageLayoutEvent.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { namespace Core { class Base; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class Part; } } } }

namespace Aspose {

namespace Words {

namespace Layout {

/// An argument passed into <see cref="Aspose::Words::Layout::IPageLayoutCallback::Notify(System::SharedPtr<Aspose::Words::Layout::PageLayoutCallbackArgs>)">Notify()</see>
class ASPOSE_WORDS_SHARED_CLASS PageLayoutCallbackArgs : public System::Object
{
    typedef PageLayoutCallbackArgs ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::Core::Base;
    friend class Aspose::Words::Layout::Core::DocumentLayout;

public:

    /// Gets event.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Layout::PageLayoutEvent get_Event() const;
    /// Gets document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> get_Document() const;
    /// Gets 0-based index of the page in the document this event relates to.
    /// Returns negative value if there is no associated page, or if page was removed during reflow.
    ASPOSE_WORDS_SHARED_API int32_t get_PageIndex();

protected:

    System::SharedPtr<Aspose::Words::Layout::Core::Part> get_Part() const;
    System::SharedPtr<Aspose::Words::Layout::Core::DocumentLayout> get_Layout() const;

    PageLayoutCallbackArgs(Aspose::Words::Layout::PageLayoutEvent e);

    void Init(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Layout::Core::DocumentLayout> layout, System::SharedPtr<Aspose::Words::Layout::Core::Part> part);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Layout::PageLayoutEvent pr_Event;
    System::SharedPtr<Aspose::Words::Document> _Document;
    System::SharedPtr<Aspose::Words::Layout::Core::DocumentLayout> _Layout;
    System::SharedPtr<Aspose::Words::Layout::Core::Part> _Part;

};

}
}
}
