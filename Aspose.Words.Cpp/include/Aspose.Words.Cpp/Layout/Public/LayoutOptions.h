//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Layout/Public/LayoutOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { class LayoutOptionsCore; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Layout { class RevisionOptions; } } }
namespace Aspose { namespace Words { namespace Shaping { class ITextShaperFactory; } } }
namespace Aspose { namespace Words { namespace Layout { class IPageLayoutCallback; } } }

namespace Aspose {

namespace Words {

namespace Layout {

/// Holds the options that allow controlling the document layout process.
/// 
/// You do not create instances of this class directly. Use the <see cref="Aspose::Words::Document::get_LayoutOptions">LayoutOptions</see> property to access layout options for this document.
/// 
/// Note that after changing any of the options present in this class, <see cref="Aspose::Words::Document::UpdatePageLayout">UpdatePageLayout</see> method
/// should be called in order for the changed options to be applied to the layout.
class ASPOSE_WORDS_SHARED_CLASS LayoutOptions : public System::Object
{
    typedef LayoutOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Layout::LayoutOptionsCore;
    friend class Aspose::Words::Document;

public:

    /// Gets revision options.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Layout::RevisionOptions> get_RevisionOptions() const;
    /// Gets indication of whether hidden text in the document is rendered.
    /// Default is False.
    ASPOSE_WORDS_SHARED_API bool get_ShowHiddenText() const;
    /// Sets indication of whether hidden text in the document is rendered.
    /// Default is False.
    ASPOSE_WORDS_SHARED_API void set_ShowHiddenText(bool value);
    /// Gets indication of whether paragraph marks are rendered.
    /// Default is False.
    ASPOSE_WORDS_SHARED_API bool get_ShowParagraphMarks() const;
    /// Sets indication of whether paragraph marks are rendered.
    /// Default is False.
    ASPOSE_WORDS_SHARED_API void set_ShowParagraphMarks(bool value);
    /// Gets indication of whether comments are rendered.
    /// Default is True.
    ASPOSE_WORDS_SHARED_API bool get_ShowComments();
    /// Sets indication of whether comments are rendered.
    /// Default is True.
    ASPOSE_WORDS_SHARED_API void set_ShowComments(bool value);
    /// Gets <see cref="Aspose::Words::Shaping::ITextShaperFactory">ITextShaperFactory</see> implementation used for Advanced Typography rendering features.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Shaping::ITextShaperFactory> get_TextShaperFactory() const;
    /// Sets <see cref="Aspose::Words::Shaping::ITextShaperFactory">ITextShaperFactory</see> implementation used for Advanced Typography rendering features.
    ASPOSE_WORDS_SHARED_API void set_TextShaperFactory(System::SharedPtr<Aspose::Words::Shaping::ITextShaperFactory> value);
    /// Gets <see cref="Aspose::Words::Layout::IPageLayoutCallback">IPageLayoutCallback</see> implementation used by page layout model.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Layout::IPageLayoutCallback> get_Callback() const;
    /// Sets <see cref="Aspose::Words::Layout::IPageLayoutCallback">IPageLayoutCallback</see> implementation used by page layout model.
    ASPOSE_WORDS_SHARED_API void set_Callback(System::SharedPtr<Aspose::Words::Layout::IPageLayoutCallback> value);
    /// Gets indication of whether the "Use printer metrics to lay out document" compatibility option is ignored.
    /// Default is True.
    ASPOSE_WORDS_SHARED_API bool get_IgnorePrinterMetrics() const;
    /// Sets indication of whether the "Use printer metrics to lay out document" compatibility option is ignored.
    /// Default is True.
    ASPOSE_WORDS_SHARED_API void set_IgnorePrinterMetrics(bool value);

    ASPOSE_WORDS_SHARED_API LayoutOptions();

protected:

    static const bool IsIgnorePrinterMetricsDefaultValue;

    bool GetHasChanged(bool reset);
    System::SharedPtr<Aspose::Words::Layout::LayoutOptions> Clone();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Layout::RevisionOptions> mRevisionOptions;
    bool mIsShowHiddenText;
    bool mIsShowParagraphMarks;
    bool mIsHideComments;
    System::SharedPtr<Aspose::Words::Shaping::ITextShaperFactory> mTextShaperFactory;
    System::SharedPtr<Aspose::Words::Layout::IPageLayoutCallback> mCallback;
    bool mIsIgnorePrinterMetrics;
    bool mHasChanged;

};

}
}
}
