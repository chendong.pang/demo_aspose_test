//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Licensing/License.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {
namespace Words {

/// Provides methods to license the component.
class License : public System::Object
{
    typedef License ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:
    /// Initializes a new instance of this class.
    ASPOSE_WORDS_SHARED_API License();
    /// Licenses the component.
    /// 
    /// Tries to find the license in the following locations:
    /// 
    /// 1. Explicit path.
    /// 
    /// 2. The folder that contains the Aspose component assembly.
    /// 
    /// 3. The folder that contains the client's calling assembly.
    /// 
    /// 4. The folder that contains the entry (startup) assembly.
    /// 
    /// 5. An embedded resource in the client's calling assembly.
    /// 
    /// <b>Note:</b>On the .NET Compact Framework, tries to find the license only in these locations:
    /// 
    /// 1. Explicit path.
    /// 
    /// 2. An embedded resource in the client's calling assembly.
    /// 
    /// @param licenseName Can be a full or short file name or name of an embedded resource.
    ///     Use an empty string to switch to evaluation mode.
    ASPOSE_WORDS_SHARED_API void SetLicense(System::String licenseName);
    /// Licenses the component.
    /// 
    /// Use this method to load a license from a stream.
    /// 
    /// @param stream A stream that contains the license.
    ASPOSE_WORDS_SHARED_API void SetLicense(System::SharedPtr<System::IO::Stream> stream);
};

}
}