//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/ListLevel.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Numbering/NumberStyle.h"
#include "Aspose.Words.Cpp/Model/Lists/ListTrailingCharacter.h"
#include "Aspose.Words.Cpp/Model/Lists/ListLevelAlignment.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRunAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Comparison { class ListComparer; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanListRevisionsHelper; } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace Validation { class StyleIstdNormalizer; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListLevelId; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlNumberListItem; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlNumberListItemCreator; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlBulletListItem; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlModelList; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlModelListLevel; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlPictureBulletListItem; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlParagraphArranger; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelInitializer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlListWrapper; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ListWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanList; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabel; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Validation { class LegacyListFormattingConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class HtmlOLElementDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class HtmlULElementDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace HtmlList { class HtmlListItemMarker; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class NumberRevision; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class List; } } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Lists { class ListFactory; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelOverride; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelCollection; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListStyle; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListStyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListLevelPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxNumberingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxNumberingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlListWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyListHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListLevelHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfListTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlListsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlListWriter; } } } } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlUtil; } } } }
namespace Aspose { namespace Words { enum class CharacterCategory; } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { namespace Drawing { class ImageData; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { class Pair; }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }

namespace Aspose {

namespace Words {

namespace Lists {

/// Defines formatting for a list level.
/// 
/// You do not create objects of this class. List level objects are created automatically
/// when a list is created. You access <see cref="Aspose::Words::Lists::ListLevel">ListLevel</see> objects via the
/// <see cref="Aspose::Words::Lists::ListLevelCollection">ListLevelCollection</see> collection.
/// 
/// Use the properties of <see cref="Aspose::Words::Lists::ListLevel">ListLevel</see> to specify list formatting
/// for individual list levels.
class ASPOSE_WORDS_SHARED_CLASS ListLevel : public Aspose::Words::IRunAttrSource
{
    typedef ListLevel ThisType;
    typedef Aspose::Words::IRunAttrSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Comparison::ListComparer;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::Validation::StyleIstdNormalizer;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::RW::Html::Reader::HtmlListLevelId;
    friend class Aspose::Words::RW::Html::Reader::HtmlNumberListItem;
    friend class Aspose::Words::RW::Html::Reader::HtmlNumberListItemCreator;
    friend class Aspose::Words::RW::Html::Reader::HtmlBulletListItem;
    friend class Aspose::Words::RW::Html::Reader::HtmlModelList;
    friend class Aspose::Words::RW::Html::Reader::HtmlModelListLevel;
    friend class Aspose::Words::RW::Html::Reader::HtmlPictureBulletListItem;
    friend class Aspose::Words::RW::Html::Reader::HtmlParagraphArranger;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriterBase;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::Lists::ListLevelInitializer;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Html::HtmlListWrapper;
    friend class Aspose::Words::RW::Html::Writer::HtmlParagraphWriter;
    friend class Aspose::Words::RW::Doc::Reader::ListReader;
    friend class Aspose::Words::RW::Doc::Reader::ParaPrReader;
    friend class Aspose::Words::RW::Doc::Writer::ListWriter;
    friend class Aspose::Words::RW::Doc::Writer::ParaPrWriter;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Layout::Core::SpanList;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Lists::ListLabel;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Validation::LegacyListFormattingConverter;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::RW::Html::Css::New::HtmlOLElementDef;
    friend class Aspose::Words::RW::Html::Css::New::HtmlULElementDef;
    friend class Aspose::Words::RW::Html::HtmlList::HtmlListItemMarker;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::Lists::ListLabelUtil;
    friend class Aspose::Words::Revisions::NumberRevision;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::List;
    friend class Aspose::Words::Lists::ListDef;
    friend class Aspose::Words::Lists::ListFactory;
    friend class Aspose::Words::Lists::ListLevelOverride;
    friend class Aspose::Words::Lists::ListLevelCollection;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::ListFormat;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::ParaPr;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtListStyle;
    friend class Aspose::Words::RW::Odt::Reader::OdtListStyleReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtListLevelPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtStylesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxNumberingReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxNumberingWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Html::Writer::HtmlListWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyListHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListLevelHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfParaPrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfListTableWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfParaPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlListsReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlListWriter;
    friend class Aspose::Words::Model::Nrx::NrxXmlUtil;

public:
    using System::Object::Equals;

public:

    /// Returns the starting number for this list level.
    /// 
    /// Default value is 1.
    ASPOSE_WORDS_SHARED_API int32_t get_StartAt();
    /// Sets the starting number for this list level.
    /// 
    /// Default value is 1.
    ASPOSE_WORDS_SHARED_API void set_StartAt(int32_t value);
    /// Returns the number style for this list level.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NumberStyle get_NumberStyle() const;
    /// Sets the number style for this list level.
    ASPOSE_WORDS_SHARED_API void set_NumberStyle(Aspose::Words::NumberStyle value);
    /// Returns or sets the number format for the list level.
    /// 
    /// Among normal text characters, the string can contain placeholder characters \\x0000 to \\x0008
    /// representing the numbers from the corresponding list levels.
    /// 
    /// For example, the string "\x0000.\x0001)" will generate a list label
    /// that looks something like "1.5)". The number "1" is the current number from
    /// the 1st list level, the number "5" is the current number from the 2nd list level.
    /// 
    /// Null is not allowed, but an empty string meaning no number is valid.
    ASPOSE_WORDS_SHARED_API System::String get_NumberFormat() const;
    /// Setter for Aspose::Words::Lists::ListLevel::get_NumberFormat
    ASPOSE_WORDS_SHARED_API void set_NumberFormat(System::String value);
    /// Gets the justification of the actual number of the list item.
    /// 
    /// The list label is justified relative to the <see cref="Aspose::Words::Lists::ListLevel::get_NumberPosition">NumberPosition</see> property.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Lists::ListLevelAlignment get_Alignment() const;
    /// Sets the justification of the actual number of the list item.
    /// 
    /// The list label is justified relative to the <see cref="Aspose::Words::Lists::ListLevel::get_NumberPosition">NumberPosition</see> property.
    ASPOSE_WORDS_SHARED_API void set_Alignment(Aspose::Words::Lists::ListLevelAlignment value);
    /// True if the level turns all inherited numbers to Arabic, false if it preserves their number style.
    ASPOSE_WORDS_SHARED_API bool get_IsLegal() const;
    /// True if the level turns all inherited numbers to Arabic, false if it preserves their number style.
    ASPOSE_WORDS_SHARED_API void set_IsLegal(bool value);
    /// Sets or returns the list level that must appear before the specified list level restarts numbering.
    /// 
    /// The value of -1 means the numbering will continue.
    ASPOSE_WORDS_SHARED_API int32_t get_RestartAfterLevel() const;
    /// Sets or returns the list level that must appear before the specified list level restarts numbering.
    /// 
    /// The value of -1 means the numbering will continue.
    ASPOSE_WORDS_SHARED_API void set_RestartAfterLevel(int32_t value);
    /// Returns the character inserted after the number for the list level.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Lists::ListTrailingCharacter get_TrailingCharacter() const;
    /// Sets the character inserted after the number for the list level.
    ASPOSE_WORDS_SHARED_API void set_TrailingCharacter(Aspose::Words::Lists::ListTrailingCharacter value);
    /// Specifies character formatting used for the list label.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Returns or sets the tab position (in points) for the list level.
    /// 
    /// Has effect only when <see cref="Aspose::Words::Lists::ListLevel::get_TrailingCharacter">TrailingCharacter</see> is a tab.
    /// 
    /// @sa Aspose::Words::Lists::ListLevel::get_NumberPosition
    /// @sa Aspose::Words::Lists::ListLevel::get_TextPosition
    ASPOSE_WORDS_SHARED_API double get_TabPosition() const;
    /// Setter for Aspose::Words::Lists::ListLevel::get_TabPosition
    ASPOSE_WORDS_SHARED_API void set_TabPosition(double value);
    /// Returns or sets the position (in points) of the number or bullet for the list level.
    /// 
    /// <see cref="Aspose::Words::Lists::ListLevel::get_NumberPosition">NumberPosition</see> corresponds to LeftIndent plus FirstLineIndent of the paragraph.
    /// 
    /// @sa Aspose::Words::Lists::ListLevel::get_TextPosition
    /// @sa Aspose::Words::Lists::ListLevel::get_TabPosition
    ASPOSE_WORDS_SHARED_API double get_NumberPosition() const;
    /// Setter for Aspose::Words::Lists::ListLevel::get_NumberPosition
    ASPOSE_WORDS_SHARED_API void set_NumberPosition(double value);
    /// Returns or sets the position (in points) for the second line of wrapping text for the list level.
    /// 
    /// <see cref="Aspose::Words::Lists::ListLevel::get_TextPosition">TextPosition</see> corresponds to LeftIndent of the paragraph.
    /// 
    /// @sa Aspose::Words::Lists::ListLevel::get_NumberPosition
    /// @sa Aspose::Words::Lists::ListLevel::get_TabPosition
    ASPOSE_WORDS_SHARED_API double get_TextPosition() const;
    /// Setter for Aspose::Words::Lists::ListLevel::get_TextPosition
    ASPOSE_WORDS_SHARED_API void set_TextPosition(double value);
    /// Gets or sets the paragraph style that is linked to this list level.
    /// 
    /// This property is null when the list level is not linked to a paragraph style.
    /// This property can be set to null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> get_LinkedStyle();
    /// Setter for Aspose::Words::Lists::ListLevel::get_LinkedStyle
    ASPOSE_WORDS_SHARED_API void set_LinkedStyle(System::SharedPtr<Aspose::Words::Style> value);
    /// Returns image data of the picture bullet shape for the current list level.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::ImageData> get_ImageData();

    /// Creates picture bullet shape for the current list level.
    ASPOSE_WORDS_SHARED_API void CreatePictureBullet();
    /// Deletes picture bullet for the current list level.
    ASPOSE_WORDS_SHARED_API void DeletePictureBullet();
    /// Compares with the specified ListLevel.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Lists::ListLevel> level);
    /// Calculates hash code for this object.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;

protected:

    System::String get_CustomNumberStyle();
    void set_CustomNumberStyle(System::String value);
    Aspose::Words::CharacterCategory get_LabelCharacterCategory() const;
    System::SharedPtr<Aspose::Words::Drawing::Shape> get_PictureBullet();
    int32_t get_ParaStyleIstd() const;
    void set_ParaStyleIstd(int32_t value);
    System::SharedPtr<Aspose::Words::ParaPr> get_ParaPr() const;
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr() const;
    int32_t get_PictureBulletId() const;
    void set_PictureBulletId(int32_t value);
    bool get_HasPictureBullet();
    int32_t get_LevelNumber() const;
    bool get_IsRestartAfterLevelCustom() const;
    bool get_Legacy();
    void set_Legacy(bool value);
    bool get_LegacyPrev();
    void set_LegacyPrev(bool value);
    bool get_LegacyPrevSpace();
    void set_LegacyPrevSpace(bool value);
    int32_t get_LegacySpace();
    void set_LegacySpace(int32_t value);
    int32_t get_LegacyIndent();
    void set_LegacyIndent(int32_t value);

    bool IsTentative;
    static const int32_t MaxNumberFormatLength;
    static const int32_t MinLevel;
    static const int32_t MaxLevels;
    static const int32_t LeftIndent;
    static const int32_t DefaultNumberHanging;

    ListLevel(System::SharedPtr<Aspose::Words::DocumentBase> document, int32_t levelNumber);
    ListLevel(System::SharedPtr<Aspose::Words::Lists::ListLevel> parentListLevel, System::SharedPtr<Aspose::Words::IRunAttrSource> parentRunAttrs);

    System::SharedPtr<Aspose::Words::Lists::ListLevel> Clone(System::SharedPtr<Aspose::Words::DocumentBase> dstDocument);
    bool EqualsCore(System::SharedPtr<Aspose::Words::Lists::ListLevel> level, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::SharedPtr<Aspose::Pair>>> alreadyComparedLinkedStyles);
    void SetStartAtSafe(int32_t value);
    void SetRestartAfterLevelSafe(int32_t value);
    void SetNumberFormatSafe(System::String value);
    void SetLegacyTabPosition(double position);
    static bool IsNumberFormatValid(System::String value);
    static bool IsNumberFormatCharsValid(System::String value);
    static bool IsNumberFormatLengthValid(System::String value);
    static bool IsPlaceholder(char16_t c);
    static bool IsPlaceholderValid(System::String numberFormat, int32_t levelNumber);
    static System::String RemovePlaceholders(System::String numberFormat);
    static bool IsLegacySpaceValid(int32_t legacySpace);
    static bool IsStartAtValid(int32_t value);
    static bool IsRestartAfterLevelValid(int32_t value);
    static bool IsLevelNumberValid(int32_t levelNumber);
    static int32_t MakeLevelNumberValid(int32_t levelNumber);
    static bool IsNumberStyleValid(Aspose::Words::NumberStyle numberStyle);
    static bool IsAlignmentValid(Aspose::Words::Lists::ListLevelAlignment alignment);
    static bool IsTrailingCharacterValid(Aspose::Words::Lists::ListTrailingCharacter trailingChar);
    static bool AreSameLegacyListLevels(System::SharedPtr<Aspose::Words::Lists::ListLevel> expected, System::SharedPtr<Aspose::Words::Lists::ListLevel> actual);

    virtual ASPOSE_WORDS_SHARED_API ~ListLevel();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Lists::ListLevel> get_GetInstance() const;

    System::WeakPtr<Aspose::Words::DocumentBase> mDocument;
    int32_t mLevelNumber;
    int32_t mStartAt;
    Aspose::Words::NumberStyle mNumberStyle;
    System::String mCustomNumberStyle;
    System::String mNumberFormat;
    Aspose::Words::Lists::ListLevelAlignment mAlignment;
    bool mIsLegal;
    int32_t mRestartAfterLevel;
    Aspose::Words::Lists::ListTrailingCharacter mTrailingCharacter;
    System::SharedPtr<Aspose::Words::ParaPr> mParaPr;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    int32_t mPictureBulletId;
    bool mLegacy;
    int32_t mLegacySpace;
    int32_t mLegacyIndent;
    bool mLegacyPrevSpace;
    bool mLegacyPrev;
    int32_t mParaStyleIstd;
    System::SharedPtr<Aspose::Words::Font> mFont;
    System::WeakPtr<Aspose::Words::IRunAttrSource> mParentRunAttrSource;
    System::WeakPtr<Aspose::Words::Lists::ListLevel> mParentListLevel;

    static const System::String& PlaceHoldersString();
    bool GenericEquals(System::SharedPtr<Aspose::Words::Lists::ListLevel> level);

};

}
}
}
