//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/ListCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Lists/ListTemplate.h"
#include "Aspose.Words.Cpp/Model/Lists/List.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Lists { class ListNumberingRestarter; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace Validation { class StyleIstdNormalizer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Fields { class HiddenParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentCleaner; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ListWriter; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeIdGenerator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Dofr { class DofrFiler; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListFactory; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListStyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxNumberingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxNumberingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfPictureBulletHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfGroupState; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyListHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListOverrideTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfListTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlListsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Collections { class IntToIntDictionary; } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace Lists { enum class ListType; } } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class ImportContext; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { enum class RevisionsView; } }

namespace Aspose {

namespace Words {

namespace Lists {

/// Stores and manages formatting of bulleted and numbered lists used in a document.
/// 
/// A list in a Microsoft Word document is a set of list formatting properties.
/// The formatting of the lists is stored in the <see cref="Aspose::Words::Lists::ListCollection">ListCollection</see> collection separately
/// from the paragraphs of text.
/// 
/// You do not create objects of this class. There is always only one <see cref="Aspose::Words::Lists::ListCollection">ListCollection</see>
/// object per document and it is accessible via the <see cref="Aspose::Words::DocumentBase::get_Lists">Lists</see> property.
/// 
/// To create a new list based on a predefined list template or based on a list style,
/// use the <see cref="Aspose::Words::Lists::ListCollection::Add(System::SharedPtr<Aspose::Words::Style>)">Add()</see> method.
/// 
/// To create a new list with formatting identical to an existing list,
/// use the <see cref="Aspose::Words::Lists::ListCollection::AddCopy(System::SharedPtr<Aspose::Words::Lists::List>)">AddCopy()</see> method.
/// 
/// To make a paragraph bulleted or numbered, you need to apply list formatting
/// to a paragraph by assigning a <see cref="Aspose::Words::Lists::List">List</see> object to the
/// <see cref="Aspose::Words::ListFormat::get_List">List</see> property of <see cref="Aspose::Words::ListFormat">ListFormat</see>.
/// 
/// To remove list formatting from a paragraph, use the <see cref="Aspose::Words::ListFormat::RemoveNumbers">RemoveNumbers</see>
/// method.
/// 
/// If you know a bit about WordprocessingML, then you might know it defines separate concepts
/// for "list" and "list definition". This exactly corresponds to how list formatting is stored
/// in a Microsoft Word document at the low level. List definition is like a "schema" and
/// list is like an instance of a list definition.
/// 
/// To simplify programming model, Aspose.Words hides the distinction between list and list
/// definition in much the same way like Microsoft Word hides this in its user interface.
/// This allows you to concentrate more on how you want your document to look like, rather than
/// building low-level objects to satisfy requirements of the Microsoft Word file format.
/// 
/// It is not possible to delete lists once they are created in the current version of Aspose.Words.
/// This is similar to Microsoft Word where user does not have explicit control over list definitions.
/// 
/// @sa Aspose::Words::Lists::List
/// @sa Aspose::Words::Lists::ListLevel
/// @sa Aspose::Words::ListFormat
class ASPOSE_WORDS_SHARED_CLASS ListCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Lists::List>>
{
    typedef ListCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Lists::List>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Lists::ListNumberingRestarter;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::Validation::StyleIstdNormalizer;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Fields::HiddenParagraphTocEntry;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::Validation::DocumentCleaner;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Doc::Reader::ListReader;
    friend class Aspose::Words::RW::Doc::Writer::ListWriter;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::Validation::ShapeIdGenerator;
    friend class Aspose::Words::RW::Doc::Dofr::DofrFiler;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::List;
    friend class Aspose::Words::Lists::ListFactory;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::MailMerging::MailMerge;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::ListFormat;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtListStyleReader;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxNumberingReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxNumberingWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfPictureBulletHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfGroupState;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyListHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListOverrideTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfParaPrReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Writer::RtfListTableWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlListsReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Gets the count of numbered and bulleted lists in the document.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    /// Gets the owner document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const;

    /// Gets the enumerator object that will enumerate lists in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Lists::List>>> GetEnumerator() override;
    /// Creates a new list based on a predefined template and adds it to the collection of lists in the document.
    /// 
    /// Aspose.Words list templates correspond to the 21 list templates available
    /// in the Bullets and Numbering dialog box in Microsoft Word 2003.
    /// 
    /// All lists created using this method have 9 list levels.
    /// 
    /// @param listTemplate The template of the list.
    /// 
    /// @return The newly created list.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> Add(Aspose::Words::Lists::ListTemplate listTemplate);
    /// Creates a new list that references a list style and adds it to the collection of lists in the document.
    /// 
    /// The newly created list references the list style. If you change the properties of the list
    /// style, it is reflected in the properties of the list. Vice versa, if you change the properties
    /// of the list, it is reflected in the properties of the list style.
    /// 
    /// @param listStyle The list style.
    /// 
    /// @return The newly created list.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> Add(System::SharedPtr<Aspose::Words::Style> listStyle);
    /// Creates a new list by copying the specified list and adding it to the collection of lists in the document.
    /// 
    /// The source list can be from any document. If the source list belongs to a different document,
    /// a copy of the list is created and added to the current document.
    /// 
    /// If the source list is a reference to or a definition of a list style,
    /// the newly created list is not related to the original list style.
    /// 
    /// @param srcList The source list to copy from.
    /// 
    /// @return The newly created list.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> AddCopy(System::SharedPtr<Aspose::Words::Lists::List> srcList);
    /// Gets a list by a list identifier.
    /// 
    /// You don't normally need to use this method. Most of the time you apply list formatting
    /// to paragraphs just by settings the <see cref="Aspose::Words::ListFormat::get_List">List</see> property
    /// of the <see cref="Aspose::Words::ListFormat">ListFormat</see> object.
    /// 
    /// @param listId The list identifier.
    /// 
    /// @return Returns the list object. Returns null if a list with the specified identifier was not found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> GetListByListId(int32_t listId);

    /// Gets a list by index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> idx_get(int32_t index);

protected:

    int32_t get_ListDefCount();
    int32_t get_PictureBulletCount();
    System::SharedPtr<System::Object> get_NumIdMacAtCleanup() const;
    void set_NumIdMacAtCleanup(System::SharedPtr<System::Object> value);
    System::SharedPtr<Aspose::Collections::IntToIntDictionary> get_ListIdTranslationTable() const;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::ListDef>>> get_ListDefs() const;

    ListCollection(System::SharedPtr<Aspose::Words::DocumentBase> document);

    System::SharedPtr<Aspose::Words::Lists::List> AddCopy(System::SharedPtr<Aspose::Words::Lists::List> srcList, bool isImport);
    System::SharedPtr<Aspose::Words::Lists::List> FetchListByListId(int32_t listId);
    System::SharedPtr<Aspose::Words::Lists::List> FetchListByListIdResolveStyleReference(int32_t listId);
    System::SharedPtr<Aspose::Words::Lists::List> AddEmpty(Aspose::Words::Lists::ListType listType);
    void AddList(System::SharedPtr<Aspose::Words::Lists::List> list);
    void AddListDef(System::SharedPtr<Aspose::Words::Lists::ListDef> listDef);
    System::SharedPtr<Aspose::Words::Lists::ListDef> GetListDefByIndex(int32_t index);
    System::SharedPtr<Aspose::Words::Lists::ListDef> GetListDefByListDefId(int32_t listDefId);
    System::SharedPtr<Aspose::Words::Lists::ListDef> FetchListDefByListDefId(int32_t listDefId);
    int32_t GetIndexOfListDefByListDefId(int32_t listDefId);
    System::SharedPtr<Aspose::Words::Lists::ListCollection> Clone(System::SharedPtr<Aspose::Words::DocumentBase> dstDocument, System::SharedPtr<Aspose::Words::INodeCloningListener> nodeCloningListener);
    int32_t ImportList(System::SharedPtr<Aspose::Words::ImportContext> context, int32_t srcListId);
    int32_t CopyList(int32_t srcListId, System::SharedPtr<Aspose::Words::ImportContext> context);
    int32_t MakeUniquieListDefId();
    int32_t GetNextAvailableListDefId();
    int32_t GetNextAvailableListId();

    System::SharedPtr<Aspose::Words::Lists::List> idx_get(System::String listDefName);

    void SortByListId();
    int32_t AddPictureBullet(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);
    System::SharedPtr<Aspose::Words::Drawing::Shape> GetPictureBullet(int32_t index);
    void SetPictureBullet(int32_t index, System::SharedPtr<Aspose::Words::Drawing::Shape> value);
    void ExpandDirectList(System::SharedPtr<Aspose::Words::ParaPr> srcParaPr, System::SharedPtr<Aspose::Words::ParaPr> dstParaPr);
    System::SharedPtr<Aspose::Words::Lists::ListLevel> GetDirectListLevel(System::SharedPtr<Aspose::Words::ParaPr> paraPr);
    System::SharedPtr<Aspose::Words::Lists::ListLevel> GetDirectListLevel(System::SharedPtr<Aspose::Words::ParaPr> paraPr, Aspose::Words::RevisionsView revisionsView);
    void RemoveCore(int32_t listId);
    int32_t GetTranslatedListId(int32_t listId);
    void RemoveTranslation();
    void ClearListStyleReferencesLevels();
    void FixUpListsWithCircularReferences();

    virtual ASPOSE_WORDS_SHARED_API ~ListCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::DocumentBase> mDocument;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::List>>> mLists;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::ListDef>>> mListDefs;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Drawing::Shape>>> mPictureBullets;
    System::SharedPtr<Aspose::Collections::IntToIntDictionary> mListIdTranslationTable;
    int32_t mMaxUsedListId;
    int32_t mMaxUsedListDefId;
    System::SharedPtr<System::Object> mNumIdMacAtCleanup;

    System::SharedPtr<Aspose::Words::Lists::ListDef> AddEmptyListDef(Aspose::Words::Lists::ListType listType, int32_t listDefId);
    int32_t ImportList(System::SharedPtr<Aspose::Words::ImportContext> context, int32_t srcListId, bool isImportListStyle);
    bool ImportListCore(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Lists::List> srcList);
    static void ImportLinkedParagraphStyle(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Lists::ListLevel> srcLevel, System::SharedPtr<Aspose::Words::Lists::ListLevel> dstLevel);
    static void CopyLinkedParagraphStyle(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Lists::ListLevel> srcLevel, System::SharedPtr<Aspose::Words::Lists::ListLevel> dstLevel);
    static void ImportPictureBullet(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Lists::ListLevel> srcLevel, System::SharedPtr<Aspose::Words::Lists::ListLevel> dstLevel);
    System::SharedPtr<Aspose::Words::Lists::List> FindListWithEqualDefIdAndOverrides(System::SharedPtr<Aspose::Words::Lists::List> refList);
    static void RemoveListDefFromCircularReference(System::SharedPtr<Aspose::Words::Lists::ListDef> listDef, System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Style>>> circularStyles);
    static System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::ListDef>>> GetListDefsWithCircularReferences(System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::ListDef>>> listDefs, System::SharedPtr<Aspose::Words::Lists::ListDef> listDef);
    static void ClearFirstListTabStop(System::SharedPtr<Aspose::Words::ParaPr> paraPr);

};

}
}
}
