//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/ListLabel.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Formatting/IRunAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanListRevisionsHelper; } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlListWrapper; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanList; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormat; } } }
namespace Aspose { namespace Words { namespace Validation { class LegacyListFormattingConverter; } } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUtil; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlFontResourcesCollector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlListWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlListWriter; } } } } }
namespace Aspose { namespace Words { namespace Lists { enum class ListTrailingCharacter; } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberState; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { enum class RevisionsView; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { enum class RunPrExpandFlags; } }

namespace Aspose {

namespace Words {

namespace Lists {

/// Defines properties specific to a list label.
class ASPOSE_WORDS_SHARED_CLASS ListLabel : public Aspose::Words::IRunAttrSource
{
    typedef ListLabel ThisType;
    typedef Aspose::Words::IRunAttrSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::RW::Html::HtmlListWrapper;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Layout::Core::SpanList;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Fields::FieldFormat;
    friend class Aspose::Words::Validation::LegacyListFormattingConverter;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::Lists::ListLabelUtil;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Html::Writer::HtmlFontResourcesCollector;
    friend class Aspose::Words::RW::Html::Writer::HtmlListWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlListWriter;

public:

    /// Gets the list label font.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Gets a string representation of list label.
    ASPOSE_WORDS_SHARED_API System::String get_LabelString();
    /// Gets a numeric value for this label.
    ASPOSE_WORDS_SHARED_API int32_t get_LabelValue();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;

protected:

    System::SharedPtr<Aspose::Words::Lists::ListNumberState> get_NumberState() const;
    System::SharedPtr<Aspose::Words::Lists::ListNumberState> get_NumberStateFinal() const;
    bool get_HasChars();
    bool get_HasCharsFinal();
    System::ArrayPtr<System::String> get_LabelFragments() const;
    System::ArrayPtr<System::String> get_LabelFragmentsOriginal() const;
    System::ArrayPtr<System::String> get_LabelFragmentsFinal() const;
    System::ArrayPtr<System::String> get_LabelArabicNumbers() const;
    System::ArrayPtr<System::String> get_LabelArabicNumbersOriginal() const;
    System::ArrayPtr<System::String> get_LabelArabicNumbersFinal() const;
    int32_t get_LabelValueFinal();
    System::String get_LabelStringOriginal();
    System::String get_LabelStringFinal();
    Aspose::Words::Lists::ListTrailingCharacter get_TrailingCharacter();
    Aspose::Words::Lists::ListTrailingCharacter get_TrailingCharacterFinal();
    System::SharedPtr<Aspose::Words::Lists::ListLevel> get_ListLevel();

    ListLabel(System::SharedPtr<Aspose::Words::Paragraph> para);

    void SetLabelStringAndValue(System::ArrayPtr<System::String> labelFragments, System::ArrayPtr<System::String> labelArabicNumbers, System::SharedPtr<Aspose::Words::Lists::ListNumberState> listNumberState, Aspose::Words::RevisionsView view);
    System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr(bool expandListLevelFormatting, Aspose::Words::RunPrExpandFlags flags);

    virtual ASPOSE_WORDS_SHARED_API ~ListLabel();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Paragraph> mPara;
    System::SharedPtr<Aspose::Words::ListFormat> mListFormat;
    System::SharedPtr<Aspose::Words::Font> mFont;
    System::ArrayPtr<System::String> mLabelFragmentsOriginal;
    System::ArrayPtr<System::String> mLabelFragmentsFinal;
    System::ArrayPtr<System::String> mLabelArabicNumbersOriginal;
    System::ArrayPtr<System::String> mLabelArabicNumbersFinal;
    System::SharedPtr<Aspose::Words::Lists::ListNumberState> mNumberStateOriginal;
    System::SharedPtr<Aspose::Words::Lists::ListNumberState> mNumberStateFinal;

    static void ThrowCannotChangeException();
    System::SharedPtr<System::Object> ProcessListLabelAttributeBoolExAware(System::SharedPtr<System::Object> value, int32_t key);

};

}
}
}
