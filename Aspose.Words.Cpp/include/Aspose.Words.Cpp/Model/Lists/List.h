//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/List.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/icomparable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Comparison { class ListComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class StylesheetComparer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListFactory; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentCleaner; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ListWriter; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberGenerator; } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberState; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtOfficeStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListStyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxNumberingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxNumberingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyListHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListOverrideLevelHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListOverrideTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfListTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlListsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelOverrideCollection; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelCollection; } } }
namespace Aspose { class Pair; }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelOverride; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }

namespace Aspose {

namespace Words {

namespace Lists {

/// Represents formatting of a list.
/// 
/// A list in a Microsoft Word document is a set of list formatting properties.
/// Each list can have up to 9 levels and formatting properties, such as number style, start value,
/// indent, tab position etc are defined separately for each level.
/// 
/// A <see cref="Aspose::Words::Lists::List">List</see> object always belongs to the <see cref="Aspose::Words::Lists::ListCollection">ListCollection</see> collection.
/// 
/// To create a new list, use the Add methods of the <see cref="Aspose::Words::Lists::ListCollection">ListCollection</see> collection.
/// 
/// To modify formatting of a list, use <see cref="Aspose::Words::Lists::ListLevel">ListLevel</see> objects found in
/// the <see cref="Aspose::Words::Lists::List::get_ListLevels">ListLevels</see> collection.
/// 
/// To apply or remove list formatting from a paragraph, use <see cref="Aspose::Words::ListFormat">ListFormat</see>.
/// 
/// @sa Aspose::Words::Lists::ListCollection
/// @sa Aspose::Words::Lists::ListLevel
/// @sa Aspose::Words::ListFormat
class ASPOSE_WORDS_SHARED_CLASS List : public System::IComparable<System::SharedPtr<Aspose::Words::Lists::List>>
{
    typedef List ThisType;
    typedef System::IComparable<System::SharedPtr<Aspose::Words::Lists::List>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comparison::ListComparer;
    friend class Aspose::Words::Comparison::StylesheetComparer;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::Fields::FieldNumListFactory;
    friend class Aspose::Words::Validation::DocumentCleaner;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Doc::Reader::ListReader;
    friend class Aspose::Words::RW::Doc::Writer::ListWriter;
    friend class Aspose::Words::Lists::ListNumberGenerator;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::ListDef;
    friend class Aspose::Words::Lists::ListNumberState;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::ListFormat;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtOfficeStylesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtListStyleReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxNumberingReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxNumberingWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyListHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListOverrideLevelHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListOverrideTableHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfListTableWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlListsReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Gets the unique identifier of the list.
    /// 
    /// You do not normally need to use this property. But if you use it, you normally do so
    /// in conjunction with the <see cref="Aspose::Words::Lists::ListCollection::GetListByListId(int32_t)">GetListByListId()</see> method to find a
    /// list by its identifier.
    ASPOSE_WORDS_SHARED_API int32_t get_ListId() const;
    /// Gets the owner document.
    /// 
    /// A list always has a parent document and is valid only in the context of that document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const;
    /// Returns true when the list contains 9 levels; false when 1 level.
    /// 
    /// The lists that you create with Aspose.Words are always multi-level lists and contain 9 levels.
    /// 
    /// Microsoft Word 2003 and later always create multi-level lists with 9 levels.
    /// But in some documents, created with earlier versions of Microsoft Word you might encounter
    /// lists that have 1 level only.
    ASPOSE_WORDS_SHARED_API bool get_IsMultiLevel();
    /// Gets the collection of list levels for this list.
    /// 
    /// Use this property to access and modify formatting individual to each level of the list.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::ListLevelCollection> get_ListLevels();
    /// Specifies whether list should be restarted at each section.
    /// Default value is <b>false</b>.
    /// 
    /// This option is supported only in RTF, DOC and DOCX document formats.
    /// 
    /// This option will be written to DOCX only if <see cref="Aspose::Words::Saving::OoxmlCompliance">OoxmlCompliance</see> is higher then <see cref="Aspose::Words::Saving::OoxmlCompliance::Ecma376_2006">Ecma376_2006</see>.
    ASPOSE_WORDS_SHARED_API bool get_IsRestartAtEachSection();
    /// Setter for Aspose::Words::Lists::List::get_IsRestartAtEachSection
    ASPOSE_WORDS_SHARED_API void set_IsRestartAtEachSection(bool value);
    /// Returns true if this list is a definition of a list style.
    /// 
    /// When this property is true, the <see cref="Aspose::Words::Lists::List::get_Style">Style</see> property returns the list style that
    /// this list defines.
    /// 
    /// By modifying properties of a list that defines a list style, you modify the properties
    /// of the list style.
    /// 
    /// A list that is a definition of a list style cannot be applied directly to paragraphs
    /// to make them numbered.
    /// 
    /// @sa Aspose::Words::Lists::List::get_Style
    /// @sa Aspose::Words::Lists::List::get_IsListStyleReference
    ASPOSE_WORDS_SHARED_API bool get_IsListStyleDefinition();
    /// Returns true if this list is a reference to a list style.
    /// 
    /// Note, modifying properties of a list that is a reference to list style has no effect.
    /// The list formatting specified in the list style itself always takes precedence.
    /// 
    /// @sa Aspose::Words::Lists::List::get_Style
    /// @sa Aspose::Words::Lists::List::get_IsListStyleDefinition
    ASPOSE_WORDS_SHARED_API bool get_IsListStyleReference();
    /// Gets the list style that this list references or defines.
    /// 
    /// If this list is not associated with a list style, the property will return null.
    /// 
    /// A list could be a reference to a list style, in this case <see cref="Aspose::Words::Lists::List::get_IsListStyleReference">IsListStyleReference</see>
    /// will be true.
    /// 
    /// A list could be a definition of a list style, in this case <see cref="Aspose::Words::Lists::List::get_IsListStyleDefinition">IsListStyleDefinition</see>
    /// will be true. Such a list cannot be applied to paragraphs in the document directly.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> get_Style();

    /// Compares with the specified list.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Lists::List> list);
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Calculates hash code for this list object.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    /// Compares the specified list to the current list.
    ASPOSE_WORDS_SHARED_API int32_t CompareTo(System::SharedPtr<Aspose::Words::Lists::List> other) override;

protected:

    System::SharedPtr<Aspose::Words::Lists::ListLevelOverrideCollection> get_Overrides() const;
    System::SharedPtr<Aspose::Words::Lists::ListDef> get_ListDef() const;
    int32_t get_ListDefId() const;
    void set_ListDefId(int32_t value);

    List(System::SharedPtr<Aspose::Words::DocumentBase> document, int32_t listId);
    List(System::SharedPtr<Aspose::Words::Lists::ListDef> listDef, int32_t listId);

    System::SharedPtr<Aspose::Words::Lists::List> Clone(System::SharedPtr<Aspose::Words::DocumentBase> document, int32_t listId);
    bool EqualsCore(System::SharedPtr<Aspose::Words::Lists::List> list, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::SharedPtr<Aspose::Pair>>> alreadyComparedLinkedStyles);
    void SetListId(int32_t listId);
    System::SharedPtr<Aspose::Words::Lists::ListLevelOverride> GetFormattingOverride(int32_t level);
    bool IsStartAtOverridden(int32_t level);
    int32_t GetStartAtOverrideAware(int32_t level);
    System::SharedPtr<Aspose::Words::Lists::ListLevel> GetListLevelOverrideAware(int32_t level);
    static bool AreSameLegacyLists(System::SharedPtr<Aspose::Words::Lists::List> expected, System::SharedPtr<Aspose::Words::Lists::List> actual);

    virtual ASPOSE_WORDS_SHARED_API ~List();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::DocumentBase> mDocument;
    int32_t mListId;
    int32_t mListDefId;
    mutable System::SharedPtr<Aspose::Words::Lists::ListDef> mListDefCache;
    System::SharedPtr<Aspose::Words::Lists::ListLevelOverrideCollection> mOverrides;

    System::SharedPtr<Aspose::Words::Lists::ListLevelOverride> GetStartAtOverride(int32_t level);

};

}
}
}
