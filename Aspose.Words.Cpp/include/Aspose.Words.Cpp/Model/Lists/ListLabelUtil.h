//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/ListLabelUtil.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Numbering/NumberStyle.h"
#include "Aspose.Words.Cpp/Model/Lists/ListLabelUtil.h"
#include "Aspose.Words.Cpp/Model/Document/RevisionsView.h"

namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberGenerator; } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberState; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }

namespace Aspose {

namespace Words {

namespace Lists {

/// \cond
class IListLabelBuildBehaviour : public System::Object
{
    typedef IListLabelBuildBehaviour ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:

    virtual void NotifyListNumberAppended(int32_t listLabelLength) = 0;
    virtual bool ShouldAppendNotListNumberChar(char16_t c) = 0;
    virtual int32_t FinilazeListLabelLength(int32_t listLabelLength) = 0;

};/// \endcond

/// \cond
class ListLabelUtil
{
    typedef ListLabelUtil ThisType;

private:

    /// ListLabelAction enumeration.
    enum class ListLabelAction
    {
        Undetermined,
        UpdateAndSet,
        UpdateAndClear,
        Clear
    };

private:

    class IListLabelBuilder : public System::Object
    {
        typedef IListLabelBuilder ThisType;
        typedef System::Object BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        virtual int32_t get_Length() = 0;
        virtual void set_Length(int32_t value) = 0;

        virtual void Append(char16_t c) = 0;
        virtual void Append(System::String s) = 0;

    };

    class ListLabelBuilder : public Aspose::Words::Lists::ListLabelUtil::IListLabelBuilder
    {
        typedef ListLabelBuilder ThisType;
        typedef Aspose::Words::Lists::ListLabelUtil::IListLabelBuilder BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        int32_t get_Length() override;
        void set_Length(int32_t value) override;

        System::String ToString() const override;
        void Append(char16_t c) override;
        void Append(System::String s) override;

        ListLabelBuilder();

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<System::Text::StringBuilder> mBuilder;

    };

    class ListLabelFragmentsBuilder : public Aspose::Words::Lists::ListLabelUtil::IListLabelBuilder
    {
        typedef ListLabelFragmentsBuilder ThisType;
        typedef Aspose::Words::Lists::ListLabelUtil::IListLabelBuilder BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        int32_t get_Length() override;
        void set_Length(int32_t value) override;

        System::ArrayPtr<System::String> GetFragments();
        void Append(char16_t c) override;
        void Append(System::String s) override;

        ListLabelFragmentsBuilder();

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<System::Collections::Generic::List<System::String>> mList;

    };

    class EmptyListLabelBuildBehaviour : public Aspose::Words::Lists::IListLabelBuildBehaviour
    {
        typedef EmptyListLabelBuildBehaviour ThisType;
        typedef Aspose::Words::Lists::IListLabelBuildBehaviour BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour>& Instance();
        void NotifyListNumberAppended(int32_t listLabelLength) override;
        bool ShouldAppendNotListNumberChar(char16_t c) override;
        int32_t FinilazeListLabelLength(int32_t listLabelLength) override;

    private:

        EmptyListLabelBuildBehaviour();

    };

public:

    static bool UpdateParagraphListLabel(System::SharedPtr<Aspose::Words::Paragraph> para, System::SharedPtr<Aspose::Words::Lists::ListNumberGenerator> generator, Aspose::Words::RevisionsView view);
    static System::String BuildFullListLabel(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour, System::String separator);
    static System::String BuildFullListLabel(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, int32_t minLevel, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour, System::String separator);
    static System::String BuildListLabel(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour);
    static bool HasCounter(Aspose::Words::NumberStyle numberStyle);

private:

    static Aspose::Words::Lists::ListLabelUtil::ListLabelAction GetListLabelAction(System::SharedPtr<Aspose::Words::Paragraph> para, Aspose::Words::RevisionsView view);
    static Aspose::Words::Lists::ListLabelUtil::ListLabelAction AnalyzeEmptinessAndPageBreaksInParagraph(System::SharedPtr<Aspose::Words::Paragraph> para);
    static bool IsActionUpdateAndSet(System::SharedPtr<Aspose::Words::Paragraph> para);
    static bool ContainsNonPageBreakChars(System::String text);
    static System::ArrayPtr<System::String> BuildListLabelFragments(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, bool isArabicNumbersWithoutSeparators);
    static bool IsAccountedForRef(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, int32_t level);
    static System::String BuildLevelLabelString(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::ListLevel> levelPr, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour, int32_t& minLevelUsed);
    static System::ArrayPtr<System::String> BuildLevelLabelFragments(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::ListLevel> levelPr, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour, bool isArabicNumbersWithoutSeparators, int32_t& minLevelUsed);
    static int32_t BuildLevelLabel(System::SharedPtr<Aspose::Words::Lists::ListLabelUtil::IListLabelBuilder> builder, System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::ListLevel> levelPr, System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> behaviour, bool isArabicNumbersWithoutSeparators);
    static System::String BuildListLabelPart(System::SharedPtr<Aspose::Words::Lists::ListNumberState> state, System::SharedPtr<Aspose::Words::Lists::ListLevel> currentLevelPr, int32_t levelOfLabel, bool isArabicNumbersWithoutSeparators);
    static System::SharedPtr<Aspose::Words::Lists::ListLabelUtil::ListLabelBuilder> CreateListLabelBuilder();
    static System::SharedPtr<Aspose::Words::Lists::ListLabelUtil::ListLabelFragmentsBuilder> CreateListLabelFragmentsBuilder();

};/// \endcond

}
}
}
