//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Lists/ListLevelCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Lists/ListLevel.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ListWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Lists { class List; } } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberState; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListLevelHandler; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Lists { enum class ListType; } } }

namespace Aspose {

namespace Words {

namespace Lists {

/// A collection of list formatting for each level in a list.
class ASPOSE_WORDS_SHARED_CLASS ListLevelCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Lists::ListLevel>>
{
    typedef ListLevelCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Lists::ListLevel>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::RW::Doc::Reader::ListReader;
    friend class Aspose::Words::RW::Doc::Writer::ListWriter;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Lists::List;
    friend class Aspose::Words::Lists::ListDef;
    friend class Aspose::Words::Lists::ListNumberState;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListLevelHandler;

public:

    /// Gets the number of levels in this list.
    /// 
    /// There could be 1 or 9 levels in a list.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets the enumerator object that will enumerate levels in this list.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Lists::ListLevel>>> GetEnumerator() override;

    /// Gets a list level by index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::ListLevel> idx_get(int32_t index);
    /// Gets a list level by index.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::SharedPtr<Aspose::Words::Lists::ListLevel> value);

protected:

    ListLevelCollection();
    ListLevelCollection(System::SharedPtr<Aspose::Words::DocumentBase> document, Aspose::Words::Lists::ListType listType);

    void Add(System::SharedPtr<Aspose::Words::Lists::ListLevel> listLevel);
    void FixUpLevelCount(Aspose::Words::Lists::ListType listType, System::SharedPtr<Aspose::Words::DocumentBase> doc);
    System::SharedPtr<Aspose::Words::Lists::ListLevel> FetchListLevel(int32_t level);
    int32_t CorrectLevel(int32_t level);
    System::SharedPtr<Aspose::Words::Lists::ListLevelCollection> Clone(System::SharedPtr<Aspose::Words::DocumentBase> dstDocument);
    void Clear();

    virtual ASPOSE_WORDS_SHARED_API ~ListLevelCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Lists::ListLevel>>> mItems;

    static int32_t GetExpectedLevelCount(Aspose::Words::Lists::ListType listType);

};

}
}
}
