//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Importing/NodeImporter.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/collections/dictionary.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Importing/ImportFormatMode.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Fields { class ExternalDocumentModifier; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { class ImportContext; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Collections { template<typename> class IntToObjDictionary; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedIntegerListGeneric; } } }
namespace Aspose { namespace Words { class BookmarkStart; } }
namespace Aspose { namespace Words { class BookmarkEnd; } }
namespace Aspose { namespace Words { class ImportFormatOptions; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class CompositeNode; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class IInline; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class BuildingBlock; } } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class GlossaryDocument; } } }
namespace Aspose { namespace Words { namespace Markup { class CustomXmlPartCollection; } } }
namespace Aspose { namespace Words { class INodeWithAnnotationId; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { class AttrCollection; } }
namespace Aspose { namespace Words { class IParaAttrSource; } }
namespace Aspose { namespace Words { namespace Lists { class List; } } }
namespace Aspose { namespace Words { class WordAttrCollection; } }

namespace Aspose {

namespace Words {

/// Allows to efficiently perform repeated import of nodes from one document to another.
/// 
/// Aspose.Words provides functionality for easy copying and moving fragments
/// between Microsoft Word documents. This is known as "importing nodes".
/// Before you can insert a fragment from one document into another, you need to "import" it.
/// Importing creates a deep clone of the original node, ready to be inserted into the
/// destination document.
/// 
/// The simplest way to import a node is to use the <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool)">ImportNode()</see> method
/// provided by the <see cref="Aspose::Words::DocumentBase">DocumentBase</see> object.
/// 
/// However, when you need to import nodes from one document to another multiple times,
/// it is better to use the <see cref="Aspose::Words::NodeImporter">NodeImporter</see> class. The <see cref="Aspose::Words::NodeImporter">NodeImporter</see>
/// class allows to minimize the number of styles and lists created in the destination document.
/// 
/// Copying or moving fragments from one Microsoft Word document to another presents a number
/// of technical challenges for Aspose.Words. In a Word document, styles and list formatting
/// are stored centrally, separately from the text of the document. The paragraphs
/// and runs of text merely reference the styles by internal unique identifiers.
/// 
/// The challenges arise from the fact that styles and lists are different in different documents.
/// For example, to copy a paragraph formatted with the Heading 1 style from one document to another,
/// a number of things must be taken into account: decide whether to copy the Heading 1 style from
/// the source document to the destination document, clone the paragraph, update the cloned
/// paragraph so it refers to the correct Heading 1 style in the destination document.
/// If the style had to be copied, all the styles that it references (based on style
/// and next paragraph style) should be analyzed and possibly copied too and so on.
/// Similar issues exist when copying bulleted or numbered paragraphs because Microsoft Word
/// stores list definitions separately from text.
/// 
/// The <see cref="Aspose::Words::NodeImporter">NodeImporter</see> class is like a context, that holds the "translation tables"
/// during the import. It correctly translates between styles and lists in the source and
/// destination documents.
/// 
/// @sa Aspose::Words::Document
/// @sa Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool)
class ASPOSE_WORDS_SHARED_CLASS NodeImporter : public System::Object
{
    typedef NodeImporter ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Fields::ExternalDocumentModifier;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;

public:

    /// Initializes a new instance of the <see cref="Aspose::Words::NodeImporter">NodeImporter</see> class.
    /// 
    /// @param srcDoc The source document.
    /// @param dstDoc The destination document that will be the owner of imported nodes.
    /// @param importFormatMode Specifies how to merge style formatting that clashes.
    ASPOSE_WORDS_SHARED_API NodeImporter(System::SharedPtr<Aspose::Words::DocumentBase> srcDoc, System::SharedPtr<Aspose::Words::DocumentBase> dstDoc, Aspose::Words::ImportFormatMode importFormatMode);
    /// Initializes a new instance of the <see cref="Aspose::Words::NodeImporter">NodeImporter</see> class.
    /// 
    /// @param srcDoc The source document.
    /// @param dstDoc The destination document that will be the owner of imported nodes.
    /// @param importFormatMode Specifies how to merge style formatting that clashes.
    /// @param importFormatOptions Specifies various options to format imported node.
    ASPOSE_WORDS_SHARED_API NodeImporter(System::SharedPtr<Aspose::Words::DocumentBase> srcDoc, System::SharedPtr<Aspose::Words::DocumentBase> dstDoc, Aspose::Words::ImportFormatMode importFormatMode, System::SharedPtr<Aspose::Words::ImportFormatOptions> importFormatOptions);

    /// Imports a node from one document into another.
    /// 
    /// Importing a node creates a copy of the source node belonging to the importing document.
    /// The returned node has no parent. The source node is not altered or removed from the original document.
    /// 
    /// Before a node from another document can be inserted into this document, it must be imported.
    /// During import, document-specific properties such as references to styles and lists are translated
    /// from the original to the importing document. After the node was imported, it can be inserted
    /// into the appropriate place in the document using <see cref="Aspose::Words::CompositeNode::InsertBefore(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertBefore()</see> or
    /// <see cref="Aspose::Words::CompositeNode::InsertAfter(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertAfter()</see>.
    /// 
    /// If the source node already belongs to the destination document, then simply a deep clone
    /// of the source node is created.
    /// 
    /// @param srcNode The node to import.
    /// @param isImportChildren True to import all child nodes recursively; otherwise, false.
    /// 
    /// @return The cloned, imported node. The node belongs to the destination document, but has no parent.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren);

protected:

    System::SharedPtr<Aspose::Words::ImportContext> get_Context() const;

    NodeImporter(System::SharedPtr<Aspose::Words::DocumentBase> srcDoc, System::SharedPtr<Aspose::Words::DocumentBase> dstDoc, System::SharedPtr<Aspose::Words::ImportContext> context);

    System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> nodeCloningListener);
    void ImportAndAppendChildren(System::SharedPtr<Aspose::Words::CompositeNode> srcContainer, System::SharedPtr<Aspose::Words::CompositeNode> dstContainer);
    static void ImportList(System::SharedPtr<Aspose::Words::ParaPr> srcParaPr, System::SharedPtr<Aspose::Words::ParaPr> dstParaPr, System::SharedPtr<Aspose::Words::ImportContext> context);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::String>> get_BookmarkNames();

    System::SharedPtr<Aspose::Words::ImportContext> mContext;
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>> mImportedShapes;
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>> mDmlTxbxByTxbxId;
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>>>> mDmlLinkedTxbxByLnkId;
    System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::String>> mBookmarkNamesCache;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::String, System::String>> mBookmarkNamesTranslationTable;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::BookmarkStart>>> mGoBackBookmarkStarts;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::BookmarkEnd>>> mGoBackBookmarkEnds;

    static const System::String& GoBackBookmark();

    bool mIsApplyFormattingDifference;
    System::SharedPtr<Aspose::Words::FormattingDifferenceCalculator> mFormattingDifferenceCalculator;

    void UpdateShapeIds();
    void UpdateDmlTextboxIds();
    void ProcessBrokenTxbxChain(System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>> lnkItems);
    void UpdateTxBxNextShapeId(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> dml, System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>> lnkItems);
    void RemoveGoBackBookmarks();
    System::SharedPtr<Aspose::Words::Node> ImportNodeCore(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> nodeCloningListener);
    void UpdateShapes();
    void ImportAndAppendChildrenCore(System::SharedPtr<Aspose::Words::CompositeNode> srcContainer, System::SharedPtr<Aspose::Words::CompositeNode> dstContainer, System::SharedPtr<Aspose::Words::INodeCloningListener> nodeCloningListener);
    void ImportNodeWithoutChildren(System::SharedPtr<Aspose::Words::Node> srcNode, System::SharedPtr<Aspose::Words::Node> dstNode);
    void ImportShapeBase(System::SharedPtr<Aspose::Words::Node> srcNode, System::SharedPtr<Aspose::Words::Node> dstNode);
    void ImportCell(System::SharedPtr<Aspose::Words::Node> dstNode);
    void ImportRow(System::SharedPtr<Aspose::Words::Node> dstNode);
    void ImportDmlNode(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> srcDml, System::SharedPtr<Aspose::Words::Drawing::ShapeBase> dstDml);
    void CollectTextBoxes(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> dstDml);
    void CollectLinkedTxbx(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> lnkTxbx);
    void ImportIInline(System::SharedPtr<Aspose::Words::IInline> srcInline, System::SharedPtr<Aspose::Words::IInline> dstInline);
    void ImportParagraph(System::SharedPtr<Aspose::Words::Paragraph> srcPara, System::SharedPtr<Aspose::Words::Paragraph> dstPara);
    void ImportStructuredDocumentTag(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> srcSdt, System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> dstSdt);
    void ImportStructuredDocumentTagRange(System::SharedPtr<Aspose::Words::Node> srcSdtRange, System::SharedPtr<Aspose::Words::Node> dstSdtRange);
    static void ImportPlaceholderIfNeeded(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> dstSdt, System::SharedPtr<Aspose::Words::BuildingBlocks::BuildingBlock> placeholderReference);
    static bool IsImportPlaceholder(System::SharedPtr<Aspose::Words::BuildingBlocks::GlossaryDocument> glossary, System::SharedPtr<Aspose::Words::BuildingBlocks::BuildingBlock> placeholderReference);
    void ImportCustomXmlPartsIfNeeded(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt);
    static bool ContainsId(System::SharedPtr<Aspose::Words::Markup::CustomXmlPartCollection> collection, System::String id);
    static System::SharedPtr<Aspose::Words::BuildingBlocks::GlossaryDocument> GetOrCreateGlossaryDocument(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    void ImportINodeWithCommentId(System::SharedPtr<Aspose::Words::INodeWithAnnotationId> src, System::SharedPtr<Aspose::Words::INodeWithAnnotationId> dst);
    void CopyThemeNoOverride();
    void UpdateBookmarkNameIfNeeded(System::SharedPtr<Aspose::Words::BookmarkStart> srcBookmarkStart, System::SharedPtr<Aspose::Words::BookmarkStart> dstBookmarkStart);
    void UpdateBookmarkNameIfNeeded(System::SharedPtr<Aspose::Words::BookmarkEnd> srcBookmarkEnd, System::SharedPtr<Aspose::Words::BookmarkEnd> dstBookmarkEnd);
    System::String GetNewBookmarkName(System::String name);
    System::SharedPtr<Aspose::Words::Style> ImportStyle(System::SharedPtr<Aspose::Words::Tables::Row> row);
    void ImportStyleUseDestinationMode(System::SharedPtr<Aspose::Words::AttrCollection> srcAttrs, System::SharedPtr<Aspose::Words::AttrCollection> dstAttrs, int32_t istdKey);
    void ImportList(System::SharedPtr<Aspose::Words::ParaPr> srcParaPr, System::SharedPtr<Aspose::Words::ParaPr> dstParaPr);
    void CreateMissingBuiltInParagraphStyles(System::SharedPtr<Aspose::Words::Paragraph> paragraph);
    void CreateMissedBuiltInStyle(System::SharedPtr<Aspose::Words::Style> builtinStyle);
    static int64_t GetNonReusableListDefId(System::SharedPtr<Aspose::Words::Paragraph> curPara);
    static int32_t GetEffectiveParaIndent(System::SharedPtr<Aspose::Words::IParaAttrSource> para);
    static System::SharedPtr<Aspose::Words::Paragraph> GetPrevNonEmptyPara(System::SharedPtr<Aspose::Words::Paragraph> curPara);
    static bool IsContainsOnlyEmptyRunsOrCrossAnnotation(System::SharedPtr<Aspose::Words::Paragraph> para);
    bool CanApplyFormattingDifference(System::SharedPtr<Aspose::Words::IInline> inline_);
    bool CanApplyFormattingDifference(System::SharedPtr<Aspose::Words::Paragraph> para);
    bool IsIgnoreParaInTextBox(System::SharedPtr<Aspose::Words::Paragraph> para);
    bool CanPreserveSourceStyles(System::SharedPtr<Aspose::Words::Node> node);
    bool AreListsEqual(System::SharedPtr<Aspose::Words::Lists::List> srcList, System::SharedPtr<Aspose::Words::Lists::List> dstList);
    static System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<int32_t>> GetUsedStyles(System::SharedPtr<Aspose::Words::Node> node);
    static void CollectUsedStyles(System::SharedPtr<Aspose::Words::Node> node, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<int32_t>> usedStyleIds);
    static void AddStyle(System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<int32_t>> hashSet, System::SharedPtr<Aspose::Words::WordAttrCollection> attrs, int32_t istdKey);

};

}
}
