//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Revisions/RevisionGroupCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Revisions/RevisionGroup.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Revisions { enum class EditRevisionType; } } }
namespace Aspose { namespace Words { enum class NodeType; } }
namespace Aspose { namespace Words { namespace Revisions { class FormatRevision; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class WordAttrCollection; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { class MoveRangeStart; } }
namespace Aspose { namespace Words { namespace Revisions { class ITrackableNode; } } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class Paragraph; } }

namespace Aspose {

namespace Words {

/// A collection of <see cref="Aspose::Words::RevisionGroup">RevisionGroup</see> objects that represent revision groups in the document.
/// 
/// You do not create instances of this class directly. Use the <see cref="Aspose::Words::RevisionCollection::get_Groups">Groups</see>
/// property to get revision groups present in a document.
class ASPOSE_WORDS_SHARED_CLASS RevisionGroupCollection FINAL : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::RevisionGroup>>
{
    typedef RevisionGroupCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::RevisionGroup>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RevisionCollection;

public:

    /// Returns the number of revision groups in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::RevisionGroup>>> GetEnumerator() override;

    /// Returns a revision group at the specified index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RevisionGroup> idx_get(int32_t index);

protected:

    RevisionGroupCollection(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    virtual ASPOSE_WORDS_SHARED_API ~RevisionGroupCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    static System::ArrayPtr<int32_t>& gEffectKeys();
    static System::ArrayPtr<int32_t>& gIgnoreKeys();

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::RevisionGroup>>> mItems;

    void CollectEditRevisions(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::Revisions::EditRevisionType type);
    void CollectMoveRevisions(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    void CollectFormatRevisions(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::NodeType nodeType);
    void CollectTableRevisions(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    static System::SharedPtr<Aspose::Words::Revisions::FormatRevision> GetFormatRevision(System::SharedPtr<Aspose::Words::Tables::Table> table);
    static System::SharedPtr<Aspose::Words::WordAttrCollection> GetPr(System::SharedPtr<Aspose::Words::Node> node, Aspose::Words::NodeType desiredNodeType);
    System::SharedPtr<Aspose::Words::Node> ExtractRevisionGroup(System::SharedPtr<Aspose::Words::Node> node, System::SharedPtr<Aspose::Words::Revisions::EditRevision> revision);
    System::SharedPtr<Aspose::Words::Node> ExtractRevisionGroup(System::SharedPtr<Aspose::Words::Node> node, System::SharedPtr<Aspose::Words::Revisions::FormatRevision> revision, Aspose::Words::NodeType desiredNodeType);
    System::SharedPtr<Aspose::Words::Node> ExtractMoveRevisionGroup(System::SharedPtr<Aspose::Words::MoveRangeStart> moveStartNode);
    static System::SharedPtr<Aspose::Words::Revisions::EditRevision> GetEditRevision(System::SharedPtr<Aspose::Words::Revisions::ITrackableNode> trackable, Aspose::Words::Revisions::EditRevisionType type);
    static System::SharedPtr<Aspose::Words::ParaPr> CalculateListRevision(System::SharedPtr<Aspose::Words::Paragraph> p);
    static bool AreEqual(System::SharedPtr<Aspose::Words::Revisions::FormatRevision> formatRevision1, System::SharedPtr<Aspose::Words::Revisions::FormatRevision> formatRevision2);
    static bool AreEqual(System::SharedPtr<Aspose::Words::Revisions::EditRevision> editRevision1, System::SharedPtr<Aspose::Words::Revisions::EditRevision> editRevision2);
    static System::SharedPtr<Aspose::Words::Node> NextNode(System::SharedPtr<Aspose::Words::Node> node);
    static bool IsIgnorableNode(System::SharedPtr<Aspose::Words::Node> node);
    static System::SharedPtr<Aspose::Words::Node> DeepestNode(System::SharedPtr<Aspose::Words::Node> node);

};

}
}
