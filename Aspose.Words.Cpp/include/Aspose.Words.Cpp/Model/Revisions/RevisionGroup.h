//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Revisions/RevisionGroup.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Revisions/RevisionType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { class RevisionCommentsPreprocessor; } } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class FormatRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionBase; } } }

namespace Aspose {

namespace Words {

/// Represents a group of sequential <see cref="Aspose::Words::Revision">Revision</see> objects.
class ASPOSE_WORDS_SHARED_CLASS RevisionGroup : public System::Object
{
    typedef RevisionGroup ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::RevisionCommentsPreprocessor;
    friend class Aspose::Words::RevisionGroupCollection;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::Layout::DocumentSpanConverter;

public:

    /// Returns inserted/deleted/moved text or description of format change.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Gets the author of this revision group.
    ASPOSE_WORDS_SHARED_API System::String get_Author();
    /// Gets the type of revisions included in this group.
    ASPOSE_WORDS_SHARED_API Aspose::Words::RevisionType get_RevisionType();

protected:

    System::SharedPtr<Aspose::Words::Node> get_Start();
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> get_VisibleNodes();
    System::String get_VisibleText();
    System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_EditRevision();
    System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveRevision();
    System::SharedPtr<Aspose::Words::Revisions::FormatRevision> get_FormatRevision();
    System::SharedPtr<Aspose::Words::Revisions::RevisionBase> get_Revision() const;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> get_Nodes() const;
    System::String get_MoveRangeName() const;
    bool get_IsFontNameRevision();

    RevisionGroup(System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> nodes, System::SharedPtr<Aspose::Words::Revisions::EditRevision> revision);
    RevisionGroup(System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> nodes, System::SharedPtr<Aspose::Words::Revisions::FormatRevision> revision);
    RevisionGroup(System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> nodes, System::SharedPtr<Aspose::Words::Revisions::MoveRevision> revision, System::String moveRangeName);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Revisions::RevisionBase> mRevision;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> mNodes;
    System::String mMoveRangeName;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> mVisibleNodes;

    static System::ArrayPtr<int32_t>& gFontNameKeys();

};

}
}
