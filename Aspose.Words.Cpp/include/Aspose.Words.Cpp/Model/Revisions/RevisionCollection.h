//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Revisions/RevisionCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ilist.h>
#include <system/collections/ienumerator_ng.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Revisions/Revision.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RevisionNodeMatcher; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { class RevisionGroup; } }
namespace Aspose { namespace Words { class WordAttrCollection; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { enum class RevisionType; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionBase; } } }
namespace Aspose { template<typename> class EnumeratorWrapperPalGeneric; }

namespace Aspose {

namespace Words {

/// A collection of <see cref="Aspose::Words::Revision">Revision</see> objects that represent revisions in the document.
/// 
/// You do not create instances of this class directly. Use the <see cref="Aspose::Words::Document::get_Revisions">Revisions</see> property to get revisions present in a document.
class ASPOSE_WORDS_SHARED_CLASS RevisionCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Revision>>
{
    typedef RevisionCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Revision>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Revision;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::StyleCollection;

private:

    class RevisionIterator FINAL : public System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Revision>>
    {
        typedef RevisionIterator ThisType;
        typedef System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Revision>> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Revision> get_Current() const override;

        RevisionIterator(System::SharedPtr<Aspose::Words::RevisionCollection> revisions);

        void Dispose() override;
        bool MoveNext() override;
        void Reset() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::WeakPtr<Aspose::Words::RevisionCollection> mCollection;
        int32_t mCollectionChangeCount;
        System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Revision>>> mNodeRevisions;
        System::SharedPtr<Aspose::EnumeratorWrapperPalGeneric<System::SharedPtr<Aspose::Words::Revision>>> mStyleRevisions;
        int32_t mIndex;
        System::SharedPtr<Aspose::Words::Revision> mCurNode;

        static const System::String& ExceptionMessage();

    };

public:

    /// Returns the number of revisions in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    /// Collection of revision groups.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RevisionGroupCollection> get_Groups();

    /// Accepts all revisions in this collection.
    ASPOSE_WORDS_SHARED_API void AcceptAll();
    /// Rejects all revisions in this collection.
    ASPOSE_WORDS_SHARED_API void RejectAll();

    /// Returns a Revision at the specified index.
    /// 
    /// The index is zero-based.
    /// 
    /// Negative indexes are allowed and indicate access from the back of the collection.
    /// For example -1 means the last item, -2 means the second before last and so on.
    /// 
    /// If index is greater than or equal to the number of items in the list, this returns a null reference.
    /// 
    /// If index is negative and its absolute value is greater than the number of items in the list, this returns a null reference.
    /// 
    /// @param index An index into the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revision> idx_get(int32_t index);

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Revision>>> GetEnumerator() override;

protected:

    int32_t get_EmptyFormatRevisionsCount();
    int32_t get_ChangeCount() const;

    RevisionCollection(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    void Invalidate(System::SharedPtr<Aspose::Words::Revision> revision);
    void Remove(System::SharedPtr<Aspose::Words::Style> style);
    void AddStyleRevisions(System::SharedPtr<Aspose::Words::Style> style);
    System::SharedPtr<Aspose::Words::RevisionGroup> GetGroupByRevision(System::SharedPtr<Aspose::Words::Revision> revision);
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Revision>>> ToList();

    virtual ASPOSE_WORDS_SHARED_API ~RevisionCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t get_DocumentChangeCount();

    int32_t mChangeCount;
    int32_t mDocumentChangeCount;
    System::WeakPtr<Aspose::Words::DocumentBase> mDocument;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Revision>>> mNodeRevisions;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Revision>>> mEmptyFormatRevisions;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Revision>>> mStyleRevisions;

    static System::SharedPtr<Aspose::Words::RevisionNodeMatcher>& gRevisionNodeMatcher();

    System::SharedPtr<Aspose::Words::RevisionGroupCollection> mRevisionGroups;

    void InvalidateAll();
    void CollectRevisions();
    void CheckInvalidate();
    void AddEmptyStyleFormatRevision(System::SharedPtr<Aspose::Words::Style> style);
    void AddNodeRevisions(System::SharedPtr<Aspose::Words::WordAttrCollection> attrCollection, System::SharedPtr<Aspose::Words::Node> node);
    void AddNodeRevision(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Revision>>> revArr, Aspose::Words::RevisionType type, System::SharedPtr<Aspose::Words::Revisions::RevisionBase> revision, System::SharedPtr<Aspose::Words::Node> nodeWithRevisions, bool incChangeCount);
    void AddStyleRevision(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Revision>>> revArr, System::SharedPtr<Aspose::Words::Revisions::RevisionBase> revision, System::SharedPtr<Aspose::Words::Style> styleWithRevisions, bool incChangeCount);
    void IncrementChangeCount(bool incChangeCount);

};

}
}
