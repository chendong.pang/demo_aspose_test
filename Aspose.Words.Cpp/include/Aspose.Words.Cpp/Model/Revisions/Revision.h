//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Revisions/Revision.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/date_time.h>

#include "Aspose.Words.Cpp/Model/Revisions/RevisionType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTrackedChangesWriter; } } } } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { class RevisionGroup; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionBase; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionHandlingContext; } } }

namespace Aspose {

namespace Words {

/// Represents a revision (tracked change) in a document node or style.
/// Use <see cref="Aspose::Words::Revision::get_RevisionType">RevisionType</see> to check the type of this revision.
class ASPOSE_WORDS_SHARED_CLASS Revision : public System::Object
{
    typedef Revision ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Odt::Writer::OdtTrackedChangesWriter;
    friend class Aspose::Words::RevisionCollection;

public:

    /// Gets the author of this revision. Can not be empty string or null.
    ASPOSE_WORDS_SHARED_API System::String get_Author();
    /// Sets the author of this revision. Can not be empty string or null.
    ASPOSE_WORDS_SHARED_API void set_Author(System::String value);
    /// Gets the date/time of this revision.
    ASPOSE_WORDS_SHARED_API System::DateTime get_DateTime();
    /// Sets the date/time of this revision.
    ASPOSE_WORDS_SHARED_API void set_DateTime(System::DateTime value);
    /// Gets the type of this revision.
    ASPOSE_WORDS_SHARED_API Aspose::Words::RevisionType get_RevisionType() const;
    /// Gets the immediate parent node (owner) of this revision.
    /// This property will work for any revision type other than <see cref="Aspose::Words::RevisionType::StyleDefinitionChange">StyleDefinitionChange</see>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_ParentNode();
    /// Gets the immediate parent style (owner) of this revision.
    /// This property will work for only for the <see cref="Aspose::Words::RevisionType::StyleDefinitionChange">StyleDefinitionChange</see> revision type.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> get_ParentStyle();
    /// Gets the revision group. Returns null if the revision does not belong to any group.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RevisionGroup> get_Group();

    /// Accepts this revision.
    ASPOSE_WORDS_SHARED_API void Accept();
    /// Reject this revision.
    ASPOSE_WORDS_SHARED_API void Reject();

protected:

    bool get_IsInline();

    Revision(Aspose::Words::RevisionType revType, System::SharedPtr<Aspose::Words::Revisions::RevisionBase> revBase, System::SharedPtr<Aspose::Words::Node> parentNode, System::SharedPtr<Aspose::Words::RevisionCollection> parentCollection);
    Revision(Aspose::Words::RevisionType revType, System::SharedPtr<Aspose::Words::Revisions::RevisionBase> revBase, System::SharedPtr<Aspose::Words::Style> parentStyle, System::SharedPtr<Aspose::Words::RevisionCollection> parentCollection);

    void HandleRevisions(bool notifyCollection, System::SharedPtr<Aspose::Words::Revisions::RevisionHandlingContext> context);

    virtual ASPOSE_WORDS_SHARED_API ~Revision();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::RevisionType mType;
    System::SharedPtr<Aspose::Words::Revisions::RevisionBase> mRevisionBase;
    System::SharedPtr<Aspose::Words::Node> mParentNode;
    System::SharedPtr<Aspose::Words::Style> mParentStyle;
    bool mIsInline;
    System::WeakPtr<Aspose::Words::RevisionCollection> mParentCollection;

    Revision(Aspose::Words::RevisionType revType, System::SharedPtr<Aspose::Words::Revisions::RevisionBase> revBase, System::SharedPtr<Aspose::Words::RevisionCollection> parentCollection);

};

}
}
