//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Borders/Border.h
#pragma once

#include <system/string.h>
#include <system/array.h>
#include <mutex>
#include <memory>
#include <drawing/color.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Formatting/Intern/InternableComplexAttr.h"
#include "Aspose.Words.Cpp/Model/Formatting/IComplexAttr.h"
#include "Aspose.Words.Cpp/Model/Borders/LineStyle.h"

namespace Aspose { namespace Words { namespace Fields { class FieldIndexFormatApplier; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorRemover; } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class ImageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlOutlineToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRunPrWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class ParagraphFormatter; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRenderer; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlShapePrFiller; } } } }
namespace Aspose { namespace Words { class HtmlBlock; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { namespace CommonBorder { class BorderInfo; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { namespace CommonBorder { class CommonBorderContainer; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlImageWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class CellPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class PrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class CellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class PrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class ApsUtil; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LprBorder; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LprSpan; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutParagraphBlock; } } } }
namespace Aspose { namespace Words { class BorderCollection; } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Tables { class CnfExpanderCellPr; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class Iso29500ComplianceEnforcer; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Borders { class BorderGrid; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Borders { class BorderGridPoint; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Borders { class BorderLineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class LineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ImageBorderApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeBoundsFinder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBorderNew; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssWidthStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssPaddingIndividualPropertyDefBase; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBorderStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Tables { class CompactedSprmBorderSort; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ShapePr; } } } }
namespace Aspose { namespace Words { class SectPr; } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxXmlBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtBorder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtGraphicPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtPageLayoutPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Celler { class CellerTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class DocxBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class BorderFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapePrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Shapes { class Picf; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssBorder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class CssPropsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtBorderProperties; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtPageLayoutProperties; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfBorderReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStyleCollapser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfBorderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfCellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlAttributesWriter; } } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { class IBorderAttrSource; } }
namespace Aspose { namespace Collections { template<typename> class IntToObjDictionary; } }

namespace Aspose {

namespace Words {

/// Represents a border of an object.
/// 
/// Borders can be applied to various document elements including paragraph,
/// run of text inside a paragraph or a table cell.
class ASPOSE_WORDS_SHARED_CLASS Border : public Aspose::Words::InternableComplexAttr, public Aspose::Words::IComplexAttr
{
    typedef Border ThisType;
    typedef Aspose::Words::InternableComplexAttr BaseType;
    typedef Aspose::Words::IComplexAttr BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldIndexFormatApplier;
    friend class Aspose::Words::Themes::ThemeColorRemover;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::ImageShapeWriter;
    friend class Aspose::Words::Validation::VmlToDml::VmlOutlineToDmlConverter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRunPrWriterBase;
    friend class Aspose::Words::RW::Html::Css::New::ParagraphFormatter;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRenderer;
    friend class Aspose::Words::Validation::DmlToVml::DmlShapePrFiller;
    friend class Aspose::Words::HtmlBlock;
    friend class Aspose::Words::RW::Html::Reader::CommonBorder::BorderInfo;
    friend class Aspose::Words::RW::Html::Reader::CommonBorder::CommonBorderContainer;
    friend class Aspose::Words::RW::Html::Writer::HtmlImageWriter;
    friend class Aspose::Words::RW::Doc::Reader::CellPrReader;
    friend class Aspose::Words::RW::Doc::Reader::PrReader;
    friend class Aspose::Words::RW::Doc::Writer::CellPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::PrWriter;
    friend class Aspose::Words::Layout::Core::ApsUtil;
    friend class Aspose::Words::Layout::Core::LprBorder;
    friend class Aspose::Words::Layout::Core::LprSpan;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::PreAps::LayoutParagraphBlock;
    friend class Aspose::Words::BorderCollection;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Tables::CnfExpanderCellPr;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::Iso29500ComplianceEnforcer;
    friend class Aspose::Words::ApsBuilder::Borders::BorderGrid;
    friend class Aspose::Words::ApsBuilder::Borders::BorderGridPoint;
    friend class Aspose::Words::ApsBuilder::Borders::BorderLineApsBuilder;
    friend class Aspose::Words::ApsBuilder::Spans::LineApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ImageBorderApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeBoundsFinder;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;
    friend class Aspose::Words::RW::Html::Css::New::CssBorderNew;
    friend class Aspose::Words::RW::Html::Css::New::CssWidthStyleConverter;
    friend class Aspose::Words::RW::Html::Css::New::CssPaddingIndividualPropertyDefBase;
    friend class Aspose::Words::RW::Html::Css::New::CssBorderStyleConverter;
    friend class Aspose::Words::RW::Html::Writer::HtmlTableWriter;
    friend class Aspose::Words::RW::Doc::Tables::CompactedSprmBorderSort;
    friend class Aspose::Words::RW::Nrx::Writer::NrxParaPrWriter;
    friend class Aspose::Words::Drawing::Core::ShapePr;
    friend class Aspose::Words::SectPr;
    friend class Aspose::Words::Tables::CellPr;
    friend class Aspose::Words::Tables::TablePr;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::ParaPr;
    friend class Aspose::Words::RunPr;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Nrx::Writer::NrxXmlBuilder;
    friend class Aspose::Words::RW::Odt::Reader::OdtBorder;
    friend class Aspose::Words::RW::Odt::Writer::OdtGraphicPropertiesWriter;
    friend class Aspose::Words::RW::Vml::VmlShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtPageLayoutPropertiesReader;
    friend class Aspose::Words::Model::Nrx::NrxXmlReader;
    friend class Aspose::Words::RW::Celler::CellerTable;
    friend class Aspose::Words::RW::Nrx::Writer::DocxBuilder;
    friend class Aspose::Words::RW::Doc::BorderFiler;
    friend class Aspose::Words::RW::Doc::Escher::EsShapePrWriter;
    friend class Aspose::Words::RW::Doc::Shapes::Picf;
    friend class Aspose::Words::RW::Html::CssBorder;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Html::Writer::CssPropsWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtBorderProperties;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtPageLayoutProperties;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfBorderReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStyleCollapser;
    friend class Aspose::Words::RW::Rtf::Writer::RtfBorderWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfCellPrWriter;
    friend class Aspose::Words::RW::Vml::VmlUtil;
    friend class Aspose::Words::RW::Wml::Writer::WmlBuilder;
    friend class Aspose::Words::RW::Xaml::Writer::XamlAttributesWriter;

public:

    /// Gets the border style.
    /// 
    /// If you set line style to none, then line width is automatically changed to zero.
    ASPOSE_WORDS_SHARED_API Aspose::Words::LineStyle get_LineStyle();
    /// Sets the border style.
    /// 
    /// If you set line style to none, then line width is automatically changed to zero.
    ASPOSE_WORDS_SHARED_API void set_LineStyle(Aspose::Words::LineStyle value);
    /// Gets or sets the border width in points.
    /// 
    /// If you set line width greater than zero when line style is none, the line style is
    /// automatically changed to single line.
    ASPOSE_WORDS_SHARED_API double get_LineWidth();
    /// Setter for Aspose::Words::Border::get_LineWidth
    ASPOSE_WORDS_SHARED_API void set_LineWidth(double value);
    /// Returns true if the LineStyle is not LineStyle.None.
    ASPOSE_WORDS_SHARED_API bool get_IsVisible();
    /// Gets the border color.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color();
    /// Sets the border color.
    ASPOSE_WORDS_SHARED_API void set_Color(System::Drawing::Color value);
    /// Gets distance of the border from text or from the page edge in points.
    /// 
    /// @sa Aspose::Words::PageSetup::get_BorderDistanceFrom
    ASPOSE_WORDS_SHARED_API double get_DistanceFromText();
    /// Sets distance of the border from text or from the page edge in points.
    /// 
    /// @sa Aspose::Words::PageSetup::get_BorderDistanceFrom
    ASPOSE_WORDS_SHARED_API void set_DistanceFromText(double value);
    /// Gets or sets a value indicating whether the border has a shadow.
    /// 
    /// In Microsoft Word, for a border to have a shadow, the borders on all four sides
    /// (left, top, right and bottom) should be of the same type, width, color and all should have
    /// the Shadow property set to true.
    ASPOSE_WORDS_SHARED_API bool get_Shadow();
    /// Setter for Aspose::Words::Border::get_Shadow
    ASPOSE_WORDS_SHARED_API void set_Shadow(bool value);
    ASPOSE_WORDS_SHARED_API bool get_IsInheritedComplexAttr() override;

    /// Resets border properties to default values.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    /// Determines whether the specified border is equal in value to the current border.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Border> rhs);
    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Serves as a hash function for this type.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IComplexAttr> DeepCloneComplexAttr() override;

protected:

    void set_LineStyleInternal(Aspose::Words::LineStyle value);
    double get_LineWidthZeroAware();
    bool get_IsPageBorderArt();
    float get_BorderWidth();
    float get_TotalWidthAndDistance();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ColorInternal();
    void set_ColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    bool get_Frame();
    void set_Frame(bool value);
    System::String get_ThemeColor() const;
    void set_ThemeColor(System::String value);
    System::String get_ThemeShade() const;
    void set_ThemeShade(System::String value);
    System::String get_ThemeTint() const;
    void set_ThemeTint(System::String value);
    int32_t get_RawLineWidth() const;
    void set_RawLineWidth(int32_t value);
    int32_t get_RawDistanceFromText() const;
    void set_RawDistanceFromText(int32_t value);
    int32_t get_PartsCount();
    bool get_IsInherited();
    int32_t get_BorderNumber();
    int32_t get_Weight();
    bool get_IsNil() const;
    void set_IsNil(bool value);

    static System::SharedPtr<Aspose::Words::Border>& Empty();

    Border();
    Border(Aspose::Words::LineStyle lineStyle, int32_t rawLineWidth, System::SharedPtr<Aspose::Drawing::DrColor> color);
    Border(System::SharedPtr<Aspose::Words::IBorderAttrSource> parent, int32_t key);

    static bool GetIsVisible(Aspose::Words::LineStyle lineStyle, float lineWidth, float distance);
    void SetLineWidthSafe(double lineWidth);
    void SetDistanceFromTextSafe(double distanceFromText);
    bool CanContinue(System::SharedPtr<Aspose::Words::Border> rhs);
    bool IsMirroredWith(System::SharedPtr<Aspose::Words::Border> rhs);
    static bool GetIsMirrored(Aspose::Words::LineStyle lineStyle1, Aspose::Words::LineStyle lineStyle2);
    bool CanConnect(System::SharedPtr<Aspose::Words::Border> rhs, bool& isMirrored);
    System::SharedPtr<Aspose::Words::Border> Clone();
    void CopyFrom(System::SharedPtr<Aspose::Words::Border> src);
    static System::ArrayPtr<float> GetDashPattern(Aspose::Words::LineStyle lineStyle, float lineWidth);
    static System::ArrayPtr<float> GetPartWidths(Aspose::Words::LineStyle lineStyle, float lineWidth);
    static int32_t GetPartsCount(Aspose::Words::LineStyle lineStyle);
    static float GetActualWidth(Aspose::Words::LineStyle lineStyle, float lineWidth);
    static System::SharedPtr<Aspose::Words::Border> CreateNilBorder();
    static System::SharedPtr<Aspose::Words::Border> CreateNilBorder(System::SharedPtr<Aspose::Words::Border> border);
    static System::SharedPtr<Aspose::Words::Border> GetWinningBorder(System::SharedPtr<Aspose::Words::Border> a, System::SharedPtr<Aspose::Words::Border> b);
    bool HasValidLineStyle();
    void SetParent(System::SharedPtr<Aspose::Words::IBorderAttrSource> parent);

    virtual ASPOSE_WORDS_SHARED_API ~Border();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Border> get_Inherited();
    int32_t get_Brightness0();
    int32_t get_Brightness1();
    int32_t get_Brightness2();

    System::SharedPtr<Aspose::Words::IBorderAttrSource> mParent;
    int32_t mKey;
    Aspose::Words::LineStyle mLineStyle;
    int32_t mRawLineWidth;
    System::SharedPtr<Aspose::Drawing::DrColor> mColor;
    int32_t mRawDistanceFromText;
    bool mShadow;
    bool mFrame;
    System::String mThemeColor;
    System::String mThemeShade;
    System::String mThemeTint;
    bool mIsNil;

    static System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::ArrayPtr<float>>>& gDashPatternMap();
    static System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::ArrayPtr<float>>>& gPartWidthMap();

    static const int32_t MaxLineWidth;
    static const int32_t MaxDistanceFromText;

    void SetLineWidthCore(double lineWidth, bool isThrow);
    void SetDistanceFromTextCore(double distanceFromText, bool isThrow);
    static Aspose::Words::LineStyle GetMirroredLineStyle(Aspose::Words::LineStyle lineStyle);
    static int32_t GetBorderNumber(Aspose::Words::LineStyle lineStyle);

    static void __StaticConstructor__();

    void BeforeChange();

};

}
}
