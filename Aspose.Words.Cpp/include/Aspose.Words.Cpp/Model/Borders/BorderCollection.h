//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Borders/BorderCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Borders/Border.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { class BorderAttrSourceAdapter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutPageBorder; } } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { namespace Drawing { class ImageData; } } }
namespace Aspose { namespace Words { class PageSetup; } }
namespace Aspose { namespace Words { namespace Tables { class CellFormat; } } }
namespace Aspose { namespace Words { namespace Tables { class RowFormat; } } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssBorder; } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { class IBorderAttrSource; } }
namespace Aspose { namespace Words { enum class BorderType; } }

namespace Aspose {

namespace Words {

/// A collection of Border objects.
class ASPOSE_WORDS_SHARED_CLASS BorderCollection FINAL : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Border>>
{
    typedef BorderCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Border>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::BorderAttrSourceAdapter;
    friend class Aspose::Words::Layout::PreAps::LayoutPageBorder;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::Drawing::ImageData;
    friend class Aspose::Words::PageSetup;
    friend class Aspose::Words::Tables::CellFormat;
    friend class Aspose::Words::Tables::RowFormat;
    friend class Aspose::Words::ParagraphFormat;
    friend class Aspose::Words::RW::Html::CssBorder;

public:
    using System::Object::Equals;

private:

    class BordersEnumerator FINAL : public System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Border>>
    {
        typedef BordersEnumerator ThisType;
        typedef System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Border>> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Border> get_Current() const override;

        BordersEnumerator(System::SharedPtr<Aspose::Words::BorderCollection> borders);

        void Dispose() override;
        bool MoveNext() override;
        void Reset() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::BorderCollection> mBorders;
        int32_t mBorderIndex;

    };

public:

    /// Gets the left border.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Left();
    /// Gets the right border.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Right();
    /// Gets the top border.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Top();
    /// Gets the bottom border.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Bottom();
    /// Gets the horizontal border that is used between cells or conforming paragraphs.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Horizontal();
    /// Gets the vertical border that is used between cells.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Vertical();
    /// Gets the number of borders in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    /// Gets or sets the border width in points.
    /// 
    /// Returns the width of the first border in the collection.
    /// 
    /// Sets the width of all borders in the collection excluding diagonal borders.
    ASPOSE_WORDS_SHARED_API double get_LineWidth();
    /// Setter for Aspose::Words::BorderCollection::get_LineWidth
    ASPOSE_WORDS_SHARED_API void set_LineWidth(double value);
    /// Gets or sets the border style.
    /// 
    /// Returns the style of the first border in the collection.
    /// 
    /// Sets the style of all borders in the collection excluding diagonal borders.
    ASPOSE_WORDS_SHARED_API Aspose::Words::LineStyle get_LineStyle();
    /// Setter for Aspose::Words::BorderCollection::get_LineStyle
    ASPOSE_WORDS_SHARED_API void set_LineStyle(Aspose::Words::LineStyle value);
    /// Gets or sets the border color.
    /// 
    /// Returns the color of the first border in the collection.
    /// 
    /// Sets the color of all borders in the collection excluding diagonal borders.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color();
    /// Setter for Aspose::Words::BorderCollection::get_Color
    ASPOSE_WORDS_SHARED_API void set_Color(System::Drawing::Color value);
    /// Gets or sets distance of the border from text in points.
    /// 
    /// Gets the distance from text for the first border.
    /// 
    /// Sets the distance from text for all borders in the collection excluding diagonal borders.
    /// 
    /// Has no effect and will be automatically reset to zero for borders of table cells.
    ASPOSE_WORDS_SHARED_API double get_DistanceFromText();
    /// Setter for Aspose::Words::BorderCollection::get_DistanceFromText
    ASPOSE_WORDS_SHARED_API void set_DistanceFromText(double value);
    /// Gets or sets a value indicating whether the border has a shadow.
    /// 
    /// Gets the value from the first border in the collection.
    /// 
    /// Sets the value for all borders in the collection excluding diagonal borders.
    ASPOSE_WORDS_SHARED_API bool get_Shadow();
    /// Setter for Aspose::Words::BorderCollection::get_Shadow
    ASPOSE_WORDS_SHARED_API void set_Shadow(bool value);

    /// Compares collections of borders.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::BorderCollection> brColl);

    /// Retrieves a Border object by border type.
    /// 
    /// Note that not all borders are present for different document elements.
    /// This method throws an exception if you request a border not applicable to the current object.
    /// 
    /// @param borderType A <see cref="Aspose::Words::BorderType">BorderType</see> value
    ///     that specifies the type of the border to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> idx_get(Aspose::Words::BorderType borderType);
    /// Retrieves a Border object by index.
    /// 
    /// @param index Zero-based index of the border to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> idx_get(int32_t index);

    /// Removes all borders of an object.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    /// Returns an enumerator object that can be used to iterate over all borders in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Border>>> GetEnumerator() override;

protected:

    System::SharedPtr<Aspose::Drawing::DrColor> get_ColorInternal();
    void set_ColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    bool get_Frame();
    void set_Frame(bool value);
    bool get_IsVisible();

    BorderCollection(System::SharedPtr<Aspose::Words::IBorderAttrSource> parent);

    bool IsPossible(Aspose::Words::BorderType borderType);
    void KeepParentAsSharedPtr();

    virtual ASPOSE_WORDS_SHARED_API ~BorderCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::IBorderAttrSource> mParent;

    static bool IsCommonBorder(Aspose::Words::BorderType borderType);

};

}
}
