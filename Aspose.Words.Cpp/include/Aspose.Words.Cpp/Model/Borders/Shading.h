//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Borders/Shading.h
#pragma once

#include <system/string.h>
#include <drawing/color.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Formatting/Intern/InternableComplexAttr.h"
#include "Aspose.Words.Cpp/Model/Formatting/IComplexAttr.h"
#include "Aspose.Words.Cpp/Model/Borders/TextureIndex.h"

namespace Aspose { namespace Words { namespace Model { namespace Drawing { namespace Core { class ShadingBuilder; } } } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorRemover; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRunPrWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBackgroundColor; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathElement; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class CellPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class CellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class PrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class ApsUtil; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LprSpan; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutCell; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutParagraphBlock; } } } }
namespace Aspose { namespace Words { class FontColorResolver; } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class LineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class ShadingApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBackgroundColorPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class CellFormat; } } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class DocxBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class ShadingFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssShading; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class CssPropsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfShadingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfCellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfShadingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlAttributesWriter; } } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { class IShadingAttrSource; } }

namespace Aspose {

namespace Words {

/// Contains shading attributes for an object.
class ASPOSE_WORDS_SHARED_CLASS Shading : public Aspose::Words::InternableComplexAttr, public Aspose::Words::IComplexAttr
{
    typedef Shading ThisType;
    typedef Aspose::Words::InternableComplexAttr BaseType;
    typedef Aspose::Words::IComplexAttr BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Model::Drawing::Core::ShadingBuilder;
    friend class Aspose::Words::Themes::ThemeColorRemover;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRunPrWriterBase;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Html::Css::New::CssBackgroundColor;
    friend class Aspose::Words::ApsBuilder::Math::MathElement;
    friend class Aspose::Words::RW::Doc::Reader::CellPrReader;
    friend class Aspose::Words::RW::Doc::Writer::CellPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::PrWriter;
    friend class Aspose::Words::Layout::Core::ApsUtil;
    friend class Aspose::Words::Layout::Core::LprSpan;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::PreAps::LayoutCell;
    friend class Aspose::Words::Layout::PreAps::LayoutParagraphBlock;
    friend class Aspose::Words::FontColorResolver;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::ApsBuilder::Spans::LineApsBuilder;
    friend class Aspose::Words::ApsBuilder::Spans::ShadingApsBuilder;
    friend class Aspose::Words::RW::Html::Css::New::CssBackgroundColorPropertyDef;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxParaPrWriter;
    friend class Aspose::Words::Tables::CellFormat;
    friend class Aspose::Words::Tables::CellPr;
    friend class Aspose::Words::Tables::TablePr;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::ParagraphFormat;
    friend class Aspose::Words::ParaPr;
    friend class Aspose::Words::RunPr;
    friend class Aspose::Words::Model::Nrx::NrxXmlReader;
    friend class Aspose::Words::RW::Nrx::Writer::DocxBuilder;
    friend class Aspose::Words::RW::Doc::ShadingFiler;
    friend class Aspose::Words::RW::Html::CssShading;
    friend class Aspose::Words::RW::Html::Writer::CssPropsWriter;
    friend class Aspose::Words::RW::Odt::OdtUtil;
    friend class Aspose::Words::RW::Rtf::Reader::RtfShadingReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfCellPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfShadingWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlBuilder;
    friend class Aspose::Words::RW::Xaml::Writer::XamlAttributesWriter;

public:

    ASPOSE_WORDS_SHARED_API bool get_IsInheritedComplexAttr() override;
    /// Gets the color that's applied to the background of the Shading object.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_BackgroundPatternColor();
    /// Sets the color that's applied to the background of the Shading object.
    ASPOSE_WORDS_SHARED_API void set_BackgroundPatternColor(System::Drawing::Color value);
    /// Gets the color that's applied to the foreground of the Shading object.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_ForegroundPatternColor();
    /// Sets the color that's applied to the foreground of the Shading object.
    ASPOSE_WORDS_SHARED_API void set_ForegroundPatternColor(System::Drawing::Color value);
    /// Gets the shading texture.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TextureIndex get_Texture();
    /// Sets the shading texture.
    ASPOSE_WORDS_SHARED_API void set_Texture(Aspose::Words::TextureIndex value);

    /// Removes shading from the object.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    /// Determines whether the specified Shading is equal in value to the current Shading.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Shading> rhs);
    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Serves as a hash function for this type.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IComplexAttr> DeepCloneComplexAttr() override;

protected:

    bool get_IsInherited();
    System::SharedPtr<Aspose::Drawing::DrColor> get_BackgroundPatternColorInternal();
    void set_BackgroundPatternColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_ForegroundPatternColorInternal();
    void set_ForegroundPatternColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    System::String get_ThemeColor() const;
    void set_ThemeColor(System::String value);
    System::String get_ThemeShade() const;
    void set_ThemeShade(System::String value);
    System::String get_ThemeTint() const;
    void set_ThemeTint(System::String value);
    System::String get_ThemeFill() const;
    void set_ThemeFill(System::String value);
    System::String get_ThemeFillShade() const;
    void set_ThemeFillShade(System::String value);
    System::String get_ThemeFillTint() const;
    void set_ThemeFillTint(System::String value);
    bool get_IsVisible();

    Shading();
    Shading(System::SharedPtr<Aspose::Words::IShadingAttrSource> parent, int32_t key);

    System::SharedPtr<Aspose::Words::Shading> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~Shading();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Shading> get_Inherited();

    System::SharedPtr<Aspose::Words::IShadingAttrSource> mParent;
    int32_t mKey;
    Aspose::Words::TextureIndex mTexture;
    System::SharedPtr<Aspose::Drawing::DrColor> mForegroundPatternColor;
    System::SharedPtr<Aspose::Drawing::DrColor> mBackgroundPatternColor;
    System::String mThemeColor;
    System::String mThemeShade;
    System::String mThemeTint;
    System::String mThemeFill;
    System::String mThemeFillShade;
    System::String mThemeFillTint;

    void BeforeChange();
    void CopyFrom(System::SharedPtr<Aspose::Words::Shading> src);

};

}
}
