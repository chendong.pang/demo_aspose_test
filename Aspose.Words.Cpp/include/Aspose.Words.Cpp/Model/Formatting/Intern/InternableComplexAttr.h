//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Formatting/Intern/InternableComplexAttr.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { class AttrCollection; } }

namespace Aspose {

namespace Words {

/// Base class for internable complex attribute.
/// Internable complex attribute should notify parent collection when going to be changed.
class ASPOSE_WORDS_SHARED_CLASS InternableComplexAttr : public virtual System::Object
{
    typedef InternableComplexAttr ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Formatting::Intern::InternManager;

protected:

    bool get_IsAttached();

    ASPOSE_WORDS_SHARED_API void NotifyChanging();
    void Attach(System::SharedPtr<Aspose::Words::AttrCollection> pr);
    void Detach();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::AttrCollection> mPr;

};

}
}
