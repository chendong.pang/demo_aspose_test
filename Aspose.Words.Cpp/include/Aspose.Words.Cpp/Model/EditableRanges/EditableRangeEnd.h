//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/EditableRanges/EditableRangeEnd.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Revisions/DisplacedByType.h"
#include "Aspose.Words.Cpp/Model/Text/INodeWithCommentId.h"
#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/IDisplaceableByCustomXml.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace EditableRanges { class FileEditableRanges; } } } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxEditableRangesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfEditableRangeHandler; } } } } }
namespace Aspose { namespace Words { class EditableRangeStart; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Represents an end of an editable range in a Word document.
/// 
/// A complete editable range  in a Word document consists of a <see cref="Aspose::Words::EditableRangeEnd::get_EditableRangeStart">EditableRangeStart</see>
/// and a matching <see cref="Aspose::Words::EditableRangeEnd">EditableRangeEnd</see> with the same Id.
/// 
/// <see cref="Aspose::Words::EditableRangeEnd::get_EditableRangeStart">EditableRangeStart</see> and <see cref="Aspose::Words::EditableRangeEnd">EditableRangeEnd</see> are just markers inside a document
/// that specify where the editable range starts and ends.
/// 
/// Use the <see cref="Aspose::Words::EditableRange">EditableRange</see> class as a "facade" to work with an editable range
/// as a single object.
/// 
/// @note Currently editable ranges are supported only at the inline-level, that is inside <see cref="Aspose::Words::Paragraph">Paragraph</see>,
/// but editable range start and editable range end can be in different paragraphs.
class ASPOSE_WORDS_SHARED_CLASS EditableRangeEnd FINAL : public Aspose::Words::Node, public Aspose::Words::IDisplaceableByCustomXml, public Aspose::Words::INodeWithAnnotationId
{
    typedef EditableRangeEnd ThisType;
    typedef Aspose::Words::Node BaseType;
    typedef Aspose::Words::IDisplaceableByCustomXml BaseType1;
    typedef Aspose::Words::INodeWithAnnotationId BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Doc::EditableRanges::FileEditableRanges;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::RW::Nrx::Reader::NrxEditableRangesReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfEditableRangeHandler;

public:

    /// Corresponding EditableRangeStart, received by ID.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::EditableRangeStart> get_EditableRangeStart();
    /// Specifies the identifier of the editable range.
    ASPOSE_WORDS_SHARED_API int32_t get_Id() const;
    /// Specifies the identifier of the editable range.
    ASPOSE_WORDS_SHARED_API void set_Id(int32_t value);
    /// Returns <see cref="Aspose::Words::NodeType::EditableRangeEnd">EditableRangeEnd</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Revisions::DisplacedByType get_DisplacedByCustomXml() override;
    ASPOSE_WORDS_SHARED_API void set_DisplacedByCustomXml(Aspose::Words::Revisions::DisplacedByType value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_IdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_IdInternal(int32_t value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_ParentIdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_ParentIdInternal(int32_t value) override;

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitEditableRangeEnd(System::SharedPtr<Aspose::Words::EditableRangeEnd>)">VisitEditableRangeEnd()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Aspose::Words::Revisions::DisplacedByType get_DisplacedBy() const;
    void set_DisplacedBy(Aspose::Words::Revisions::DisplacedByType value);

    EditableRangeEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    EditableRangeEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc, int32_t id);

    virtual ASPOSE_WORDS_SHARED_API ~EditableRangeEnd();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t mId;
    Aspose::Words::Revisions::DisplacedByType mDisplacedBy;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
