//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Footnotes/FootnoteOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Footnotes/FootnoteLocation.h"
#include "Aspose.Words.Cpp/Model/Numbering/NumberStyle.h"
#include "Aspose.Words.Cpp/Model/Footnotes/IFootnoteOptions.h"
#include "Aspose.Words.Cpp/Model/Footnotes/FootnotePosition.h"
#include "Aspose.Words.Cpp/Model/Footnotes/FootnoteNumberingRule.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class PageSetup; } }
namespace Aspose { namespace Words { class ISectionAttrSource; } }

namespace Aspose {

namespace Words {

/// Represents the footnote numbering options for a document or section.
/// 
/// @sa Aspose::Words::Document::get_FootnoteOptions
/// @sa Aspose::Words::PageSetup::get_FootnoteOptions
class ASPOSE_WORDS_SHARED_CLASS FootnoteOptions FINAL : public Aspose::Words::IFootnoteOptions
{
    typedef FootnoteOptions ThisType;
    typedef Aspose::Words::IFootnoteOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::PageSetup;

public:

    /// Specifies the footnotes position.
    ASPOSE_WORDS_SHARED_API Aspose::Words::FootnotePosition get_Position();
    /// Specifies the footnotes position.
    ASPOSE_WORDS_SHARED_API void set_Position(Aspose::Words::FootnotePosition value);
    /// Specifies the number format for automatically numbered footnotes.
    /// 
    /// Not all number styles are applicable for this property. For the list of applicable
    /// number styles see the Insert Footnote or Endnote dialog box in Microsoft Word. If you select
    /// a number style that is not applicable, Microsoft Word will revert to a default value.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NumberStyle get_NumberStyle() override;
    /// Setter for Aspose::Words::FootnoteOptions::get_NumberStyle
    ASPOSE_WORDS_SHARED_API void set_NumberStyle(Aspose::Words::NumberStyle value) override;
    /// Specifies the starting number or character for the first automatically numbered footnotes.
    /// 
    /// This property has effect only when <see cref="Aspose::Words::FootnoteOptions::get_RestartRule">RestartRule</see> is set to
    /// <see cref="Aspose::Words::FootnoteNumberingRule::Continuous">Continuous</see>.
    ASPOSE_WORDS_SHARED_API int32_t get_StartNumber() override;
    /// Setter for Aspose::Words::FootnoteOptions::get_StartNumber
    ASPOSE_WORDS_SHARED_API void set_StartNumber(int32_t value) override;
    /// Determines when automatic numbering restarts.
    ASPOSE_WORDS_SHARED_API Aspose::Words::FootnoteNumberingRule get_RestartRule() override;
    /// Determines when automatic numbering restarts.
    ASPOSE_WORDS_SHARED_API void set_RestartRule(Aspose::Words::FootnoteNumberingRule value) override;
    /// Specifies the number of columns with which the footnotes area is formatted.
    ASPOSE_WORDS_SHARED_API int32_t get_Columns();
    /// Specifies the number of columns with which the footnotes area is formatted.
    ASPOSE_WORDS_SHARED_API void set_Columns(int32_t value);
    ASPOSE_WORDS_SHARED_API Aspose::Words::FootnoteLocation get_Location() override;
    ASPOSE_WORDS_SHARED_API void set_Location(Aspose::Words::FootnoteLocation value) override;

protected:

    FootnoteOptions(System::SharedPtr<Aspose::Words::ISectionAttrSource> parent);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::ISectionAttrSource> mParent;

    System::SharedPtr<System::Object> FetchAttr(int32_t key);
    void SetAttr(int32_t key, System::SharedPtr<System::Object> value);

};

}
}
