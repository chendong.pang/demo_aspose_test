//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Footnotes/Footnote.h
#pragma once

#include "Aspose.Words.Cpp/Model/Text/InlineStory.h"
#include "Aspose.Words.Cpp/Model/Sections/StoryType.h"
#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Footnotes/FootnoteType.h"

namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRunReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFootnoteHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }

namespace Aspose {

namespace Words {

/// Represents a container for text of a footnote or endnote.
/// 
/// The <b>Footnote</b> class is used to represent both footnotes and endnotes in a Word document.
/// 
/// <b>Footnote</b> is an inline-level node and can only be a child of <b>Paragraph</b>.
/// 
/// <b>Footnote</b> can contain <b>Paragraph</b> and <b>Table</b> child nodes.
/// 
/// @sa Aspose::Words::Footnote::get_FootnoteType
/// @sa Aspose::Words::DocumentBuilder::InsertFootnote(Aspose::Words::FootnoteType, System::String)
/// @sa Aspose::Words::FootnoteOptions
/// @sa Aspose::Words::EndnoteOptions
class ASPOSE_WORDS_SHARED_CLASS Footnote : public Aspose::Words::InlineStory, public Aspose::Words::Revisions::ITrackableNode
{
    typedef Footnote ThisType;
    typedef Aspose::Words::InlineStory BaseType;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Wml::Reader::WmlRunReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFootnoteHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;

public:

    /// Returns <b>NodeType.Footnote</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns <b>StoryType.Footnotes</b> or <b>StoryType.Endnotes</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StoryType get_StoryType() override;
    /// Returns a value that specifies whether this is a footnote or endnote.
    ASPOSE_WORDS_SHARED_API Aspose::Words::FootnoteType get_FootnoteType() const;
    /// Holds a value that specifies whether this is a auto-numbered footnote or
    /// footnote with user defined custom reference mark.
    ASPOSE_WORDS_SHARED_API bool get_IsAuto() const;
    /// Holds a value that specifies whether this is a auto-numbered footnote or
    /// footnote with user defined custom reference mark.
    ASPOSE_WORDS_SHARED_API void set_IsAuto(bool value);
    /// Gets/sets custom reference mark to be used for this footnote.
    /// Default value is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>), meaning auto-numbered footnotes are used.
    /// 
    /// If this property is set to <b>empty string</b> (<see cref="System::String::Empty">Empty</see>) or null, then <see cref="Aspose::Words::Footnote::get_IsAuto">IsAuto</see> property will automatically be set to true,
    /// if set to anything else then <see cref="Aspose::Words::Footnote::get_IsAuto">IsAuto</see> will be set to false.
    /// 
    /// RTF-format can only store 1 symbol as custom reference mark, so upon export only the first symbol will be written others will be discard.
    ASPOSE_WORDS_SHARED_API System::String get_ReferenceMark() const;
    /// Setter for Aspose::Words::Footnote::get_ReferenceMark
    ASPOSE_WORDS_SHARED_API void set_ReferenceMark(System::String value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    /// Initializes an instance of the <b>Footnote</b> class.
    /// 
    /// When <b>Footnote</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Footnote</b> to the document use InsertAfter or InsertBefore
    /// on the paragraph where you want the footnote inserted.
    /// 
    /// @param doc The owner document.
    /// @param footnoteType A <see cref="Aspose::Words::Footnote::get_FootnoteType">FootnoteType</see> value
    ///     that specifies whether this is a footnote or endnote.
    ASPOSE_WORDS_SHARED_API Footnote(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::FootnoteType footnoteType);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Footnote(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::FootnoteType footnoteType, bool isAuto, System::String referenceMark, System::SharedPtr<Aspose::Words::RunPr> runPr);

    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    void SetFootnoteType(Aspose::Words::FootnoteType type);

    virtual ASPOSE_WORDS_SHARED_API ~Footnote();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::FootnoteType mFootnoteType;
    bool mIsAuto;
    System::String mReferenceMark;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
