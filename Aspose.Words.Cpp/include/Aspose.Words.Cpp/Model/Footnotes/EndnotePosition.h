//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Footnotes/EndnotePosition.h
#pragma once

#include <system/object_ext.h>
#include <system/enum.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

/// Defines the endnote position.
/// 
/// @sa Aspose::Words::EndnoteOptions
enum class EndnotePosition
{
    /// Endnotes are output at the end of the section.
    EndOfSection = 0,
    /// Endnotes are output at the end of the document.
    EndOfDocument = 3
};

}
}

template<>
struct EnumMetaInfo<Aspose::Words::EndnotePosition>
{
    static const ASPOSE_WORDS_SHARED_API std::array<std::pair<Aspose::Words::EndnotePosition, const char_t*>, 2>& values();
};
