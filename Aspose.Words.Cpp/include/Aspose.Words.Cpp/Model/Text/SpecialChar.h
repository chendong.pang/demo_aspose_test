//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/SpecialChar.h
#pragma once

#include "Aspose.Words.Cpp/Model/Text/Inline.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { class AbsolutePositionTab; } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldChar; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { class Comment; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Base class for special characters in the document.
/// 
/// A Microsoft Word document can include a number of special characters
/// that represent fields, form fields, shapes, OLE objects, footnotes etc. For the list
/// of special characters see <see cref="Aspose::Words::ControlChar">ControlChar</see>.
/// 
/// <b>SpecialChar</b> is an inline-node and can only be a child of <b>Paragraph</b>.
/// 
/// <b>SpecialChar</b> char is used as a base class for more specific classes
/// that represent special characters that Aspose.Words provides programmatic access for.
/// The <b>SpecialChar</b> class is also used itself to represent special character for which
/// Aspose.Words does not provide detailed programmatic access.
class ASPOSE_WORDS_SHARED_CLASS SpecialChar : public Aspose::Words::Inline
{
    typedef SpecialChar ThisType;
    typedef Aspose::Words::Inline BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::AbsolutePositionTab;
    friend class Aspose::Words::Fields::FormField;
    friend class Aspose::Words::Fields::FieldChar;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Comment;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;

public:

    /// Returns <b>NodeType.SpecialChar</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Accepts a visitor.
    /// 
    /// Calls DocumentVisitor.VisitSpecialChar.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Gets the special character that this node represents.
    /// 
    /// @return The string that contains the character that this node represents.
    ASPOSE_WORDS_SHARED_API System::String GetText() override;

protected:

    SpecialChar(System::SharedPtr<Aspose::Words::DocumentBase> doc, char16_t ch, System::SharedPtr<Aspose::Words::RunPr> runPr);

    virtual ASPOSE_WORDS_SHARED_API ~SpecialChar();

private:

    System::String mText;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
