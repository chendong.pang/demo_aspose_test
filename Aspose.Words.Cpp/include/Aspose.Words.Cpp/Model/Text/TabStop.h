//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/TabStop.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Text/TabLeader.h"
#include "Aspose.Words.Cpp/Model/Text/TabAlignment.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxParaPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlTextShapeReader; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelInitializer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlTextShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class TabStopCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class TabStopFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfParaPrWriter; } } } } }

namespace Aspose {

namespace Words {

/// Represents a single custom tab stop. The <b>TabStop</b> object is a member of the
/// <see cref="Aspose::Words::TabStopCollection">TabStopCollection</see> collection.
/// 
/// Normally, a tab stop specifies a position where a tab stop exists. But because
/// tab stops can be inherited from parent styles, it might be needed for the child object
/// to define explicitly that there is no tab stop at a given position. To clear
/// an inherited tab stop at a given position, create a <b>TabStop</b> object and set
/// <see cref="Aspose::Words::TabStop::get_Alignment">Alignment</see> to <c>TabAlignment.Clear</c>.
/// 
/// For more information see <see cref="Aspose::Words::TabStopCollection">TabStopCollection</see>.
/// 
/// @sa Aspose::Words::ParagraphFormat
/// @sa Aspose::Words::TabStopCollection
/// @sa Aspose::Words::Document::get_DefaultTabStop
class ASPOSE_WORDS_SHARED_CLASS TabStop FINAL : public System::Object
{
    typedef TabStop ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::RW::Nrx::Reader::NrxParaPrReaderBase;
    friend class Aspose::Words::RW::Dml::Reader::DmlTextShapeReader;
    friend class Aspose::Words::Lists::ListLevelInitializer;
    friend class Aspose::Words::RW::Dml::Writer::DmlTextShapeWriter;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::Nrx::Writer::NrxParaPrWriter;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::TabStopCollection;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Doc::TabStopFiler;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfParaPrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfParaPrWriter;

public:
    using System::Object::Equals;

public:

    /// Gets the position of the tab stop in points.
    ASPOSE_WORDS_SHARED_API double get_Position();
    /// Gets the alignment of text at this tab stop.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TabAlignment get_Alignment() const;
    /// Sets the alignment of text at this tab stop.
    ASPOSE_WORDS_SHARED_API void set_Alignment(Aspose::Words::TabAlignment value);
    /// Gets the type of the leader line displayed under the tab character.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TabLeader get_Leader() const;
    /// Sets the type of the leader line displayed under the tab character.
    ASPOSE_WORDS_SHARED_API void set_Leader(Aspose::Words::TabLeader value);
    /// Returns true if this tab stop clears any existing tab stops in this position.
    ASPOSE_WORDS_SHARED_API bool get_IsClear();

    /// Initializes a new instance of this class.
    ASPOSE_WORDS_SHARED_API TabStop(double position);
    /// Initializes a new instance of this class.
    /// 
    /// @param position The position of the tab stop in points.
    /// @param alignment A <see cref="Aspose::Words::TabAlignment">TabAlignment</see> value that
    ///     specifies the alignment of text at this tab stop.
    /// @param leader A <see cref="Aspose::Words::TabLeader">TabLeader</see> value that specifies
    ///     the type of the leader line displayed under the tab character.
    ASPOSE_WORDS_SHARED_API TabStop(double position, Aspose::Words::TabAlignment alignment, Aspose::Words::TabLeader leader);

    /// Compares with the specified TabStop.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::TabStop> rhs);
    /// Calculates hash code for this object.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;

protected:

    int32_t get_PositionTwips() const;
    void set_PositionTwips(int32_t value);
    int32_t get_ToleranceTwips() const;
    void set_ToleranceTwips(int32_t value);
    bool get_Undocumented40() const;
    void set_Undocumented40(bool value);
    bool get_IsLegacyTab() const;
    void set_IsLegacyTab(bool value);

    TabStop();
    TabStop(int32_t positionTwips, Aspose::Words::TabAlignment alignment, Aspose::Words::TabLeader leader);

    System::SharedPtr<Aspose::Words::TabStop> Clone();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t mPositionTwips;
    Aspose::Words::TabAlignment mAlignment;
    Aspose::Words::TabLeader mLeader;
    int32_t mToleranceTwips;
    bool mUndocumented40;
    bool mIsLegacyTab;

};

}
}
