//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/CommentRangeEnd.h
#pragma once

#include "Aspose.Words.Cpp/Model/Revisions/DisplacedByType.h"
#include "Aspose.Words.Cpp/Model/Text/INodeWithCommentId.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/IDisplaceableByCustomXml.h"

namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCommentsReader; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Denotes the end of a region of text that has a comment associated with it.
/// 
/// To create a comment anchored to a region of text, you need to create a <see cref="Aspose::Words::Comment">Comment</see> and
/// then create <see cref="Aspose::Words::CommentRangeStart">CommentRangeStart</see> and <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see> and set their identifiers
/// to the same <see cref="Aspose::Words::Comment::get_Id">Id</see> value.
/// 
/// <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see> is an inline-level node and can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
/// 
/// @sa Aspose::Words::Comment
/// @sa Aspose::Words::CommentRangeStart
class ASPOSE_WORDS_SHARED_CLASS CommentRangeEnd FINAL : public Aspose::Words::Node, public Aspose::Words::IDisplaceableByCustomXml, public Aspose::Words::INodeWithAnnotationId
{
    typedef CommentRangeEnd ThisType;
    typedef Aspose::Words::Node BaseType;
    typedef Aspose::Words::IDisplaceableByCustomXml BaseType1;
    typedef Aspose::Words::INodeWithAnnotationId BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Docx::Reader::DocxCommentsReader;

public:

    /// Specifies the identifier of the comment to which this region is linked to.
    ASPOSE_WORDS_SHARED_API int32_t get_Id() const;
    /// Specifies the identifier of the comment to which this region is linked to.
    ASPOSE_WORDS_SHARED_API void set_Id(int32_t value);
    ASPOSE_WORDS_SHARED_API Aspose::Words::Revisions::DisplacedByType get_DisplacedByCustomXml() override;
    ASPOSE_WORDS_SHARED_API void set_DisplacedByCustomXml(Aspose::Words::Revisions::DisplacedByType value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_IdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_IdInternal(int32_t value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_ParentIdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_ParentIdInternal(int32_t value) override;
    /// Returns <see cref="Aspose::Words::NodeType::CommentRangeEnd">CommentRangeEnd</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Initializes a new instance of this class.
    /// 
    /// When <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see> is created, it belongs to the specified document, but is not
    /// yet part of the document and <see cref="Aspose::Words::Node::get_ParentNode">ParentNode</see> is null.
    /// 
    /// To append a <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see> to the document use InsertAfter or InsertBefore
    /// on the paragraph where you want the comment inserted.
    /// 
    /// @param doc The owner document.
    /// @param id The comment identifier to which this object is linked.
    ASPOSE_WORDS_SHARED_API CommentRangeEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc, int32_t id);

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitCommentRangeEnd(System::SharedPtr<Aspose::Words::CommentRangeEnd>)">VisitCommentRangeEnd()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Aspose::Words::Revisions::DisplacedByType get_DisplacedBy() const;
    void set_DisplacedBy(Aspose::Words::Revisions::DisplacedByType value);

    virtual ASPOSE_WORDS_SHARED_API ~CommentRangeEnd();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t mId;
    Aspose::Words::Revisions::DisplacedByType mDisplacedBy;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
