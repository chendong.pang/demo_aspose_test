//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/Run.h
#pragma once

#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Text/Inline.h"

namespace Aspose { namespace Words { namespace Fields { class FieldQuoteUpdater; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class StoryComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class ComplexScriptRunUpdater; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Fields { class ToaEntry; } } }
namespace Aspose { namespace Words { class NodeEnumerator; } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBarcodeUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { class Ruby; } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTextHelper; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxBidiControlCharReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlRubyReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSymbol; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryNodeModifier; } } }
namespace Aspose { namespace Words { namespace MailMerging { class TagReplacer; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { namespace Layout { class DocumentRunSplitter; } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtFontSetter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { class MiscRwUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfSpecialFieldHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFootnoteWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfPictureHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfOleHandler; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Represents a run of characters with the same font formatting.
/// 
/// All text of the document is stored in runs of text.
/// 
/// <b>Run</b> can only be a child of <b>Paragraph</b> or inline <b>StructuredDocumentTag</b>.
class ASPOSE_WORDS_SHARED_CLASS Run : public Aspose::Words::Inline
{
    typedef Run ThisType;
    typedef Aspose::Words::Inline BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldQuoteUpdater;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::StoryComparer;
    friend class Aspose::Words::Validation::ComplexScriptRunUpdater;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Fields::ToaEntry;
    friend class Aspose::Words::NodeEnumerator;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Replacing::FindReplace;
    friend class Aspose::Words::Fields::FieldAppender;
    friend class Aspose::Words::Fields::FieldBarcodeUtil;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Ruby;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::FieldTextHelper;
    friend class Aspose::Words::Fields::FieldTextHelper;
    friend class Aspose::Words::RW::Docx::Reader::DocxBidiControlCharReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlRubyReader;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRunWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Fields::FieldSymbol;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::TocEntryNodeModifier;
    friend class Aspose::Words::MailMerging::TagReplacer;
    friend class Aspose::Words::NodeRange;
    friend class Aspose::Words::Layout::DocumentRunSplitter;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Txt::Reader::TxtFontSetter;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Inline;
    friend class Aspose::Words::RunPr;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::MiscRwUtil;
    friend class Aspose::Words::RW::Rtf::Reader::RtfSpecialFieldHandler;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFootnoteWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfPictureHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfOleHandler;

public:

    /// Returns <b>NodeType.Run</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Gets the text of the run.
    ASPOSE_WORDS_SHARED_API System::String get_Text() const;
    /// Sets the text of the run.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);

    /// Initializes a new instance of the <b>Run</b> class.
    /// 
    /// When <b>Run</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Run</b> to the document use InsertAfter or InsertBefore
    /// on the paragraph where you want the run inserted.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Run(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    /// Initializes a new instance of the <b>Run</b> class.
    /// 
    /// When <b>Run</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Run</b> to the document use InsertAfter or InsertBefore
    /// on the paragraph where you want the run inserted.
    /// 
    /// @param doc The owner document.
    /// @param text The text of the run.
    ASPOSE_WORDS_SHARED_API Run(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::String text);

    /// Accepts a visitor.
    /// 
    /// Calls DocumentVisitor.VisitRun.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Gets the text of the run.
    /// 
    /// @return The text of the run.
    ASPOSE_WORDS_SHARED_API System::String GetText() override;

protected:

    bool get_IsSymbolic();

    static System::ArrayPtr<int32_t>& KeysToIgnoreInComparisonOnJoinLtrRuns();
    static System::ArrayPtr<int32_t>& KeysToIgnoreInComparisonOnJoinRtlRuns();

    Run(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::String text, System::SharedPtr<Aspose::Words::RunPr> runPr);

    System::SharedPtr<Aspose::Words::Run> CutText(int32_t startIndex, int32_t length, bool isClone);
    System::SharedPtr<Aspose::Words::Run> SplitBefore(int32_t leftPartLength);
    System::SharedPtr<Aspose::Words::Run> SplitAfter(int32_t leftPartLength);
    static bool IsSymbolicCharacter(System::String text);

    virtual ASPOSE_WORDS_SHARED_API ~Run();

private:

    System::String mText;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
