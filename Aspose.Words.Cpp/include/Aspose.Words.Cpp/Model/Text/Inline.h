//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/Inline.h
#pragma once

#include "Aspose.Words.Cpp/Model/Formatting/RunPrExpandFlags.h"
#include "Aspose.Words.Cpp/Model/Text/IInline.h"
#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Comparison { class SdtComparer; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexAndTablesEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldQuoteUpdater; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace TextEffects { class DmlTextEffectsPostApplier; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class StoryComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class ComplexScriptRunUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Fields { class ToaEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldExtractor; } } }
namespace Aspose { namespace Words { namespace Fields { class HiddenAttributeCache; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMathUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownInlineCodeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class DefaultFormatApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { class Ruby; } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class FallbackShapeValidator; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class OfficeMathToApsConverter; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFakeResultRunPrProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEQ; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlRubyReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLWriter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRunWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSymbol; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldHyperlink; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFieldFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryNodeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryStartingRunAutomate; } } }
namespace Aspose { namespace Words { namespace Fields { class TocHyperlinkEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class TocNormalEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class CharFormatProvider; } } }
namespace Aspose { namespace Words { namespace MailMerging { class TagReplacer; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtPlaceholderManager; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { class DocumentPosition; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { class SpecialChar; } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeTokenBuilder; } } }
namespace Aspose { namespace Words { namespace Fields { class TextResultApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Fields { class OldResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBundle; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class FieldValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtFontSetter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumUtil; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMathPropertiesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { class RunWriter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFootnoteHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlParaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStoryReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }

namespace Aspose {

namespace Words {

/// Base class for inline-level nodes that can have character formatting associated with them, but cannot have child nodes of their own.
/// 
/// A class derived from <b>Inline</b> can be a child of <b>Paragraph</b>.
class ASPOSE_WORDS_SHARED_CLASS Inline : public Aspose::Words::Node, public Aspose::Words::IInline, public Aspose::Words::Revisions::ITrackableNode
{
    typedef Inline ThisType;
    typedef Aspose::Words::Node BaseType;
    typedef Aspose::Words::IInline BaseType1;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Comparison::SdtComparer;
    friend class Aspose::Words::Fields::IndexAndTablesEntryAttributeModifier;
    friend class Aspose::Words::Fields::FieldQuoteUpdater;
    friend class Aspose::Words::ApsBuilder::Dml::TextEffects::DmlTextEffectsPostApplier;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Comparison::StoryComparer;
    friend class Aspose::Words::Validation::ComplexScriptRunUpdater;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Fields::ToaEntry;
    friend class Aspose::Words::Fields::FieldExtractor;
    friend class Aspose::Words::Fields::HiddenAttributeCache;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::Math::OfficeMathUtil;
    friend class Aspose::Words::RW::Docx::Writer::DocxFieldsWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxFieldsWriter;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownInlineCodeWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriterBase;
    friend class Aspose::Words::RW::Wml::Writer::WmlFieldsWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Fields::DefaultFormatApplier;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Ruby;
    friend class Aspose::Words::Validation::DmlToVml::FallbackShapeValidator;
    friend class Aspose::Words::ApsBuilder::Math::OfficeMathToApsConverter;
    friend class Aspose::Words::Fields::FieldFakeResultRunPrProvider;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Fields::FieldEQ;
    friend class Aspose::Words::RW::Html::Reader::HtmlRubyReader;
    friend class Aspose::Words::RW::MathML::MathMLWriter;
    friend class Aspose::Words::RW::MathML::MathMLReader;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRunWriter;
    friend class Aspose::Words::Fields::FieldSymbol;
    friend class Aspose::Words::Fields::FieldHyperlink;
    friend class Aspose::Words::Fields::FieldHyperlink;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::MergeFieldFinder;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocEntryNodeModifier;
    friend class Aspose::Words::Fields::TocEntryStartingRunAutomate;
    friend class Aspose::Words::Fields::TocHyperlinkEntryAttributeModifier;
    friend class Aspose::Words::Fields::TocNormalEntryAttributeModifier;
    friend class Aspose::Words::Fields::CharFormatProvider;
    friend class Aspose::Words::MailMerging::TagReplacer;
    friend class Aspose::Words::Markup::SdtPlaceholderManager;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::DocumentPosition;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::SpecialChar;
    friend class Aspose::Words::Fields::FieldCodeTokenBuilder;
    friend class Aspose::Words::Fields::TextResultApplier;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::Fields::OldResultEnumerator;
    friend class Aspose::Words::Fields::FormField;
    friend class Aspose::Words::Fields::FieldBundle;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::FieldValidator;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlSpanWriter;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Txt::Reader::TxtFontSetter;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldNumUtil;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::Run;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMathPropertiesHandler;
    friend class Aspose::Words::RW::RunWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Docx::Reader::DocxHyperlinkReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxRunWriter;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSpanWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFootnoteHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlParaReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlStoryReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlRunWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::Node::Clone;

public:

    /// Retrieves the parent <see cref="Aspose::Words::Paragraph">Paragraph</see> of this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph();
    /// Provides access to the font formatting of this object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Returns true if this object was inserted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsInsertRevision();
    /// Returns true if this object was deleted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsDeleteRevision();
    /// Returns <b>true</b> if this object was moved (deleted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveFromRevision();
    /// Returns <b>true</b> if this object was moved (inserted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveToRevision();
    /// Returns true if formatting of the object was changed in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsFormatRevision();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> get_RunPr_IInline() override;
    ASPOSE_WORDS_SHARED_API void set_RunPr_IInline(System::SharedPtr<Aspose::Words::RunPr> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr_IInline(Aspose::Words::RunPrExpandFlags flags) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t fontAttr, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;

protected:

    bool get_IsHiddenOrDeleted();
    System::SharedPtr<Aspose::Words::Style> get_CharacterStyle();
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr() const;
    void set_RunPr(System::SharedPtr<Aspose::Words::RunPr> value);
    bool get_IsWriteAsSymbol();

    Inline(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr);

    bool IsVisitorAcceptable(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr(Aspose::Words::RunPrExpandFlags flags);
    void ExpandRunPr(System::SharedPtr<Aspose::Words::RunPr> dstRunPr, Aspose::Words::RunPrExpandFlags flags);

    virtual ASPOSE_WORDS_SHARED_API ~Inline();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    System::SharedPtr<Aspose::Words::Font> mFont;

};

}
}
