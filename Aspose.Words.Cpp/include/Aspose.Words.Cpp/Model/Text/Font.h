//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/Font.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/collections/sorted_list.h>
#include <drawing/font_style.h>
#include <drawing/color.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Text/Underline.h"
#include "Aspose.Words.Cpp/Model/Text/TextEffect.h"
#include "Aspose.Words.Cpp/Model/Text/TextDmlEffect.h"
#include "Aspose.Words.Cpp/Model/Text/EmphasisMark.h"
#include "Aspose.Words.Cpp/Model/Styles/StyleIdentifier.h"
#include "Aspose.Words.Cpp/Model/Formatting/IShadingAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IBorderAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class IndexAndTablesEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace TextEffects { class DmlTextEffectsPostApplier; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlFontNameResolver; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class FontFormatterHtmlRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class FontFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class FontFormatterWordRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterWordRules; } } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceOptions; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartTitle; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRenderer; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlListWrapper; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlRevisionWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { class AttrBoolEx; } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LprSpan; } } } }
namespace Aspose { namespace Words { namespace Layout { class FontProps; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabel; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class LineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Underlines { class UnderlineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Underlines { class UnderlineMetrics; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class ListProps; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCommonBorderSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlI18nWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlSpanWriter; } } } } }
namespace Aspose { namespace Words { class InlineStory; } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace TableLayout { class ParagraphMeasurer; } } }
namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { class RunWriter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssFont; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlAttributesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlListWriter; } } } } }
namespace Aspose { namespace Words { enum class CharacterCategory; } }
namespace Aspose { namespace Words { enum class RunVerticalAlignment; } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { class Border; } }
namespace Aspose { namespace Words { class Shading; } }
namespace Aspose { namespace Words { class IRunAttrSource; } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class PrinterMetrics; } } } }
namespace Aspose { namespace Words { enum class BorderType; } }
namespace Aspose { namespace Words { namespace Themes { class Theme; } } }
namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { class DocumentBase; } }

namespace Aspose {

namespace Words {

/// Contains font attributes (font name, font size, color, and so on) for an object.
/// 
/// You do not create instances of the <see cref="Aspose::Words::Font">Font</see> class directly. You just use
/// <see cref="Aspose::Words::Font">Font</see> to access the font properties of the various objects such as <see cref="Aspose::Words::Run">Run</see>,
/// <see cref="Aspose::Words::Paragraph">Paragraph</see>, <see cref="Aspose::Words::Style">Style</see>, <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see>.
class ASPOSE_WORDS_SHARED_CLASS Font : public Aspose::Words::IBorderAttrSource, public Aspose::Words::IShadingAttrSource
{
    typedef Font ThisType;
    typedef Aspose::Words::IBorderAttrSource BaseType;
    typedef Aspose::Words::IShadingAttrSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::IndexAndTablesEntryAttributeModifier;
    friend class Aspose::Words::ApsBuilder::Dml::TextEffects::DmlTextEffectsPostApplier;
    friend class Aspose::Words::RW::Html::HtmlFontNameResolver;
    friend class Aspose::Words::RW::Html::Css::New::FontFormatterHtmlRules;
    friend class Aspose::Words::RW::Html::Css::New::FontFormatter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Html::Css::New::FontFormatterWordRules;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterWordRules;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::Replacing::FindReplaceOptions;
    friend class Aspose::Words::Drawing::Charts::ChartTitle;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRenderer;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::RW::Html::HtmlListWrapper;
    friend class Aspose::Words::RW::Html::Writer::HtmlRevisionWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::AttrBoolEx;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Layout::Core::LprSpan;
    friend class Aspose::Words::Layout::FontProps;
    friend class Aspose::Words::Lists::ListLabel;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Drawing::ShapeBase;
    friend class Aspose::Words::ApsBuilder::Spans::LineApsBuilder;
    friend class Aspose::Words::ApsBuilder::Underlines::UnderlineApsBuilder;
    friend class Aspose::Words::ApsBuilder::Underlines::UnderlineMetrics;
    friend class Aspose::Words::RW::Html::ListProps;
    friend class Aspose::Words::RW::Html::Writer::HtmlCommonBorderSpanWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlI18nWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlSpanWriter;
    friend class Aspose::Words::InlineStory;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::TableLayout::ParagraphMeasurer;
    friend class Aspose::Words::Inline;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::RunWriter;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Html::CssFont;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Xaml::Writer::XamlRunWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlAttributesWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlListWriter;

public:

    /// Gets or sets the name of the font.
    /// 
    /// When getting, returns <see cref="Aspose::Words::Font::get_NameAscii">NameAscii</see>.
    /// 
    /// When setting, sets <see cref="Aspose::Words::Font::get_NameAscii">NameAscii</see>, <see cref="Aspose::Words::Font::get_NameBi">NameBi</see>, <see cref="Aspose::Words::Font::get_NameFarEast">NameFarEast</see>
    /// and <see cref="Aspose::Words::Font::get_NameOther">NameOther</see> to the specified value.
    ASPOSE_WORDS_SHARED_API System::String get_Name();
    /// Setter for Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Returns the font used for Latin text (characters with character codes from 0 (zero) through 127).
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API System::String get_NameAscii();
    /// Sets the font used for Latin text (characters with character codes from 0 (zero) through 127).
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API void set_NameAscii(System::String value);
    /// Returns the name of the font in a right-to-left language document.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API System::String get_NameBi();
    /// Sets the name of the font in a right-to-left language document.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API void set_NameBi(System::String value);
    /// Returns an East Asian font name.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API System::String get_NameFarEast();
    /// Sets an East Asian font name.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API void set_NameFarEast(System::String value);
    /// Returns the font used for characters with character codes from 128 through 255.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API System::String get_NameOther();
    /// Sets the font used for characters with character codes from 128 through 255.
    /// 
    /// @sa Aspose::Words::Font::get_Name
    ASPOSE_WORDS_SHARED_API void set_NameOther(System::String value);
    /// Gets the font size in points.
    ASPOSE_WORDS_SHARED_API double get_Size();
    /// Sets the font size in points.
    ASPOSE_WORDS_SHARED_API void set_Size(double value);
    /// Gets the font size in points used in a right-to-left document.
    ASPOSE_WORDS_SHARED_API double get_SizeBi();
    /// Sets the font size in points used in a right-to-left document.
    ASPOSE_WORDS_SHARED_API void set_SizeBi(double value);
    /// True if the font is formatted as bold.
    ASPOSE_WORDS_SHARED_API bool get_Bold();
    /// True if the font is formatted as bold.
    ASPOSE_WORDS_SHARED_API void set_Bold(bool value);
    /// True if the right-to-left text is formatted as bold.
    ASPOSE_WORDS_SHARED_API bool get_BoldBi();
    /// True if the right-to-left text is formatted as bold.
    ASPOSE_WORDS_SHARED_API void set_BoldBi(bool value);
    /// True if the font is formatted as italic.
    ASPOSE_WORDS_SHARED_API bool get_Italic();
    /// True if the font is formatted as italic.
    ASPOSE_WORDS_SHARED_API void set_Italic(bool value);
    /// True if the right-to-left text is formatted as italic.
    ASPOSE_WORDS_SHARED_API bool get_ItalicBi();
    /// True if the right-to-left text is formatted as italic.
    ASPOSE_WORDS_SHARED_API void set_ItalicBi(bool value);
    /// Gets the color of the font.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color();
    /// Sets the color of the font.
    ASPOSE_WORDS_SHARED_API void set_Color(System::Drawing::Color value);
    /// Returns the present calculated color of the text (black or white) to be used for 'auto color'.
    /// If the color is not 'auto' then returns <see cref="Aspose::Words::Font::get_Color">Color</see>.
    /// 
    /// When text has 'automatic color', the actual color of text is calculated automatically
    /// so that it is readable against the background color. As you change the background color,
    /// the text color will automatically switch to black or white in MS Word to maximize legibility.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_AutoColor();
    /// True if the font is formatted as strikethrough text.
    ASPOSE_WORDS_SHARED_API bool get_StrikeThrough();
    /// True if the font is formatted as strikethrough text.
    ASPOSE_WORDS_SHARED_API void set_StrikeThrough(bool value);
    /// True if the font is formatted as double strikethrough text.
    ASPOSE_WORDS_SHARED_API bool get_DoubleStrikeThrough();
    /// True if the font is formatted as double strikethrough text.
    ASPOSE_WORDS_SHARED_API void set_DoubleStrikeThrough(bool value);
    /// True if the font is formatted as shadowed.
    ASPOSE_WORDS_SHARED_API bool get_Shadow();
    /// True if the font is formatted as shadowed.
    ASPOSE_WORDS_SHARED_API void set_Shadow(bool value);
    /// True if the font is formatted as outline.
    ASPOSE_WORDS_SHARED_API bool get_Outline();
    /// True if the font is formatted as outline.
    ASPOSE_WORDS_SHARED_API void set_Outline(bool value);
    /// True if the font is formatted as embossed.
    ASPOSE_WORDS_SHARED_API bool get_Emboss();
    /// True if the font is formatted as embossed.
    ASPOSE_WORDS_SHARED_API void set_Emboss(bool value);
    /// True if the font is formatted as engraved.
    ASPOSE_WORDS_SHARED_API bool get_Engrave();
    /// True if the font is formatted as engraved.
    ASPOSE_WORDS_SHARED_API void set_Engrave(bool value);
    /// True if the font is formatted as superscript.
    ASPOSE_WORDS_SHARED_API bool get_Superscript();
    /// True if the font is formatted as superscript.
    ASPOSE_WORDS_SHARED_API void set_Superscript(bool value);
    /// True if the font is formatted as subscript.
    ASPOSE_WORDS_SHARED_API bool get_Subscript();
    /// True if the font is formatted as subscript.
    ASPOSE_WORDS_SHARED_API void set_Subscript(bool value);
    /// True if the font is formatted as small capital letters.
    ASPOSE_WORDS_SHARED_API bool get_SmallCaps();
    /// True if the font is formatted as small capital letters.
    ASPOSE_WORDS_SHARED_API void set_SmallCaps(bool value);
    /// True if the font is formatted as all capital letters.
    ASPOSE_WORDS_SHARED_API bool get_AllCaps();
    /// True if the font is formatted as all capital letters.
    ASPOSE_WORDS_SHARED_API void set_AllCaps(bool value);
    /// True if the font is formatted as hidden text.
    ASPOSE_WORDS_SHARED_API bool get_Hidden();
    /// True if the font is formatted as hidden text.
    ASPOSE_WORDS_SHARED_API void set_Hidden(bool value);
    /// Gets the type of underline applied to the font.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Underline get_Underline();
    /// Sets the type of underline applied to the font.
    ASPOSE_WORDS_SHARED_API void set_Underline(Aspose::Words::Underline value);
    /// Gets the color of the underline applied to the font.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_UnderlineColor();
    /// Sets the color of the underline applied to the font.
    ASPOSE_WORDS_SHARED_API void set_UnderlineColor(System::Drawing::Color value);
    /// Gets character width scaling in percent.
    ASPOSE_WORDS_SHARED_API int32_t get_Scaling();
    /// Sets character width scaling in percent.
    ASPOSE_WORDS_SHARED_API void set_Scaling(int32_t value);
    /// Returns the spacing (in points) between characters .
    ASPOSE_WORDS_SHARED_API double get_Spacing();
    /// Sets the spacing (in points) between characters .
    ASPOSE_WORDS_SHARED_API void set_Spacing(double value);
    /// Returns line spacing of this font (in points).
    ASPOSE_WORDS_SHARED_API double get_LineSpacing();
    /// Gets the position of text (in points) relative to the base line.
    /// A positive number raises the text, and a negative number lowers it.
    ASPOSE_WORDS_SHARED_API double get_Position();
    /// Sets the position of text (in points) relative to the base line.
    /// A positive number raises the text, and a negative number lowers it.
    ASPOSE_WORDS_SHARED_API void set_Position(double value);
    /// Gets the font size at which kerning starts.
    ASPOSE_WORDS_SHARED_API double get_Kerning();
    /// Sets the font size at which kerning starts.
    ASPOSE_WORDS_SHARED_API void set_Kerning(double value);
    /// Gets the highlight (marker) color.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_HighlightColor();
    /// Sets the highlight (marker) color.
    ASPOSE_WORDS_SHARED_API void set_HighlightColor(System::Drawing::Color value);
    /// Gets the font animation effect.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TextEffect get_TextEffect();
    /// Sets the font animation effect.
    ASPOSE_WORDS_SHARED_API void set_TextEffect(Aspose::Words::TextEffect value);
    /// Specifies whether the contents of this run shall have right-to-left characteristics.
    /// 
    /// This property, when on, shall not be used with strongly left-to-right text. Any behavior under that condition is unspecified.
    /// This property, when off, shall not be used with strong right-to-left text. Any behavior under that condition is unspecified.
    /// 
    /// When the contents of this run are displayed, all characters shall be treated as complex script characters for formatting
    /// purposes. This means that <see cref="Aspose::Words::Font::get_BoldBi">BoldBi</see>, <see cref="Aspose::Words::Font::get_ItalicBi">ItalicBi</see>, <see cref="Aspose::Words::Font::get_SizeBi">SizeBi</see> and a corresponding font name
    /// will be used when rendering this run.
    /// 
    /// Also, when the contents of this run are displayed, this property acts as a right-to-left override for characters
    /// which are classified as "weak types" and "neutral types".
    ASPOSE_WORDS_SHARED_API bool get_Bidi();
    /// Setter for Aspose::Words::Font::get_Bidi
    ASPOSE_WORDS_SHARED_API void set_Bidi(bool value);
    /// Specifies whether the contents of this run shall be treated as complex script text regardless
    /// of their Unicode character values when determining the formatting for this run.
    ASPOSE_WORDS_SHARED_API bool get_ComplexScript();
    /// Specifies whether the contents of this run shall be treated as complex script text regardless
    /// of their Unicode character values when determining the formatting for this run.
    ASPOSE_WORDS_SHARED_API void set_ComplexScript(bool value);
    /// True when the formatted characters are not to be spell checked.
    ASPOSE_WORDS_SHARED_API bool get_NoProofing();
    /// True when the formatted characters are not to be spell checked.
    ASPOSE_WORDS_SHARED_API void set_NoProofing(bool value);
    /// Gets the locale identifier (language) of the formatted characters.
    ASPOSE_WORDS_SHARED_API int32_t get_LocaleId();
    /// Sets the locale identifier (language) of the formatted characters.
    ASPOSE_WORDS_SHARED_API void set_LocaleId(int32_t value);
    /// Gets the locale identifier (language) of the formatted right-to-left characters.
    ASPOSE_WORDS_SHARED_API int32_t get_LocaleIdBi();
    /// Sets the locale identifier (language) of the formatted right-to-left characters.
    ASPOSE_WORDS_SHARED_API void set_LocaleIdBi(int32_t value);
    /// Gets the locale identifier (language) of the formatted Asian characters.
    ASPOSE_WORDS_SHARED_API int32_t get_LocaleIdFarEast();
    /// Sets the locale identifier (language) of the formatted Asian characters.
    ASPOSE_WORDS_SHARED_API void set_LocaleIdFarEast(int32_t value);
    /// Returns a Border object that specifies border for the font.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Border> get_Border();
    /// Returns a Shading object that refers to the shading formatting for the font.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Shading> get_Shading();
    /// Gets the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> get_Style();
    /// Sets the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API void set_Style(System::SharedPtr<Aspose::Words::Style> value);
    /// Gets the name of the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API System::String get_StyleName();
    /// Sets the name of the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API void set_StyleName(System::String value);
    /// Gets the locale independent style identifier of the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StyleIdentifier get_StyleIdentifier();
    /// Sets the locale independent style identifier of the character style applied to this formatting.
    ASPOSE_WORDS_SHARED_API void set_StyleIdentifier(Aspose::Words::StyleIdentifier value);
    /// Specifies whether the current font should use the document grid characters per line settings
    /// when laying out.
    ASPOSE_WORDS_SHARED_API bool get_SnapToGrid();
    /// Specifies whether the current font should use the document grid characters per line settings
    /// when laying out.
    ASPOSE_WORDS_SHARED_API void set_SnapToGrid(bool value);
    /// Gets the emphasis mark applied to this formatting.
    ASPOSE_WORDS_SHARED_API Aspose::Words::EmphasisMark get_EmphasisMark();
    /// Sets the emphasis mark applied to this formatting.
    ASPOSE_WORDS_SHARED_API void set_EmphasisMark(Aspose::Words::EmphasisMark value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::SortedList<Aspose::Words::BorderType, int32_t>> get_PossibleBorderKeys() override;

    /// Resets to default font formatting.
    /// 
    /// Removes all font formatting specified explicitly on the object from which
    /// <b>Font</b> was obtained so the font formatting will be inherited from
    /// the appropriate parent.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    /// Checks if particular DrawingML text effect is applied.
    /// 
    /// @param dmlEffectType DrawingML text effect type.
    /// 
    /// @return True if particular DrawingML text effect is applied.
    ASPOSE_WORDS_SHARED_API bool HasDmlEffect(Aspose::Words::TextDmlEffect dmlEffectType);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetBorderAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedShadingAttr(int32_t key) override;

protected:

    Aspose::Words::CharacterCategory get_CharacterCategoryHint();
    System::Drawing::FontStyle get_FontStyle();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ColorInternal();
    void set_ColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_UnderlineColorInternal();
    void set_UnderlineColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    int32_t get_SpacingRaw();
    void set_SpacingRaw(int32_t value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_HighlightColorInternal();
    void set_HighlightColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    int32_t get_Istd();
    void set_Istd(int32_t value);
    Aspose::Words::RunVerticalAlignment get_VerticalAlignment();
    void set_VerticalAlignment(Aspose::Words::RunVerticalAlignment value);
    bool get_HasCharacterCategoryHint();
    System::SharedPtr<Aspose::Words::IRunAttrSource> get_Parent() const;

    static const float SmallCapsSizeFactor;
    static const float SubscriptSizeFactor;
    static const float SubscriptPositionFactor;
    static const float SuperscriptPositionFactor;

    Font(System::SharedPtr<Aspose::Words::IRunAttrSource> parent, System::SharedPtr<Aspose::Words::DocumentBase> doc);
    Font();

    System::String GetFontName(Aspose::Words::CharacterCategory category);
    int32_t GetLocaleId(Aspose::Words::CharacterCategory category);
    float GetActualSize(bool isIgnoreSmallCaps);
    System::SharedPtr<System::Object> FetchAttr(int32_t key);
    bool HasDirectAttr(int32_t key);
    System::Drawing::FontStyle GetFontStyle(Aspose::Words::CharacterCategory charCategory);

    virtual ASPOSE_WORDS_SHARED_API ~Font();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Run> get_ParentRun();
    System::SharedPtr<Aspose::Words::Layout::Core::PrinterMetrics> get_PrinterMetrics();

    System::WeakPtr<Aspose::Words::IRunAttrSource> mParent;
    System::WeakPtr<Aspose::Words::StyleCollection> mStyles;
    System::WeakPtr<Aspose::Words::Themes::Theme> mTheme;
    System::SharedPtr<Aspose::Words::Fonts::DocumentFontProvider> mFontProvider;
    System::SharedPtr<Aspose::Words::Layout::Core::PrinterMetrics> mPrinterMetrics;

    bool GetBool(int32_t key);
    void SetBool(int32_t key, bool value);
    Aspose::Words::CharacterCategory GetCharacterCategory();

};

}
}
