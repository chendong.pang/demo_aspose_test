//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/TabStopCollection.h
#pragma once

#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Text/TabLeader.h"
#include "Aspose.Words.Cpp/Model/Text/TabAlignment.h"
#include "Aspose.Words.Cpp/Model/Formatting/Intern/InternableComplexAttr.h"
#include "Aspose.Words.Cpp/Model/Formatting/IExpandableAttr.h"

namespace Aspose { namespace Words { class IComplexAttr; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxParaPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlTextShapeReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Text { class DmlParagraphProperties; } } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevelInitializer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlListWrapper; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTabStopExtractor; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutParagraphBlock; } } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class TabStopFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfParaPrReader; } } } } }
namespace Aspose { namespace Words { class TabStop; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedIntegerListGeneric; } } }

namespace Aspose {

namespace Words {

/// A collection of <see cref="Aspose::Words::TabStop">TabStop</see> objects that represent custom tabs for a paragraph or a style.
/// 
/// In Microsoft Word documents, a tab stop can be defined in the properties of a paragraph
/// style or directly in the properties of a paragraph. A style can be based on another style.
/// Therefore, the complete set of tab stops for a given object is a combination of tab stops
/// defined directly on this object and tab stops inherited from the parent styles.
/// 
/// In Aspose.Words, when you obtain a <b>TabStops</b> collection for a paragraph or a style,
/// it contains only the custom tab stops defined directly for this paragraph or style.
/// The collection does not include tab stops defined in the parent styles or default tab stops.
/// 
/// @sa Aspose::Words::ParagraphFormat
/// @sa Aspose::Words::TabStop
/// @sa Aspose::Words::Document::get_DefaultTabStop
class ASPOSE_WORDS_SHARED_CLASS TabStopCollection : public Aspose::Words::InternableComplexAttr, public Aspose::Words::IExpandableAttr
{
    typedef TabStopCollection ThisType;
    typedef Aspose::Words::InternableComplexAttr BaseType;
    typedef Aspose::Words::IExpandableAttr BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::RW::Nrx::Reader::NrxParaPrReaderBase;
    friend class Aspose::Words::RW::Dml::Reader::DmlTextShapeReader;
    friend class Aspose::Words::Drawing::Core::Dml::Text::DmlParagraphProperties;
    friend class Aspose::Words::Lists::ListLevelInitializer;
    friend class Aspose::Words::RW::Html::HtmlListWrapper;
    friend class Aspose::Words::RW::Html::Writer::HtmlTabStopExtractor;
    friend class Aspose::Words::RW::Doc::Reader::ParaPrReader;
    friend class Aspose::Words::RW::Doc::Writer::ParaPrWriter;
    friend class Aspose::Words::Layout::PreAps::LayoutParagraphBlock;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::ParagraphFormat;
    friend class Aspose::Words::ParaPr;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Doc::TabStopFiler;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfParaPrReader;

public:

    /// Gets the number of tab stops in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    ASPOSE_WORDS_SHARED_API bool get_IsInheritedComplexAttr() override;

    /// Determines whether the specified TabStopCollection is equal in value to the current TabStopCollection.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::TabStopCollection> rhs);
    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Serves as a hash function for this type.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;

    /// Gets a tab stop at the given index.
    /// 
    /// @param index An index into the collection of tab stops.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::TabStop> idx_get(int32_t index);
    /// Gets a tab stop at the specified position.
    /// 
    /// @param position The position (in points) of the tab stop.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::TabStop> idx_get(double position);

    /// Deletes all tab stop positions.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Gets the position (in points) of the tab stop at the specified index.
    /// 
    /// @param index An index into the collection of tab stops.
    /// 
    /// @return The position of the tab stop.
    ASPOSE_WORDS_SHARED_API double GetPositionByIndex(int32_t index);
    /// Gets the index of a tab stop with the specified position in points.
    ASPOSE_WORDS_SHARED_API int32_t GetIndexByPosition(double position);
    /// Adds or replaces a tab stop in the collection.
    /// 
    /// If a tab stop already exists at the specified position, it is replaced.
    /// 
    /// @param tabStop A tab stop object to add.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::TabStop> tabStop);
    /// Adds or replaces a tab stop in the collection.
    /// 
    /// If a tab stop already exists at the specified position, it is replaced.
    /// 
    /// @param position A position (in points) where to add the tab stop.
    /// @param alignment A <see cref="Aspose::Words::TabAlignment">TabAlignment</see> value that
    ///     specifies the alignment of text at the tab stop.
    /// @param leader A <see cref="Aspose::Words::TabLeader">TabLeader</see> value that
    ///     specifies the type of the leader line displayed under the tab character.
    ASPOSE_WORDS_SHARED_API void Add(double position, Aspose::Words::TabAlignment alignment, Aspose::Words::TabLeader leader);
    /// Removes a tab stop at the specified position from the collection.
    /// 
    /// @param position The position (in points) of the tab stop to remove.
    ASPOSE_WORDS_SHARED_API void RemoveByPosition(double position);
    /// Removes a tab stop at the specified index from the collection.
    /// 
    /// @param index An index into the collection of tab stops.
    ASPOSE_WORDS_SHARED_API void RemoveByIndex(int32_t index);
    /// Gets a first tab stop to the right of the specified position.
    /// 
    /// Skips tab stops with <b>Alignment</b> set to <c>TabAlignment.Bar</c>.
    /// 
    /// @param position The reference position (in points).
    /// 
    /// @return A tab stop object or null if a suitable tab stop was not found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::TabStop> After(double position);
    /// Gets a first tab stop to the left of the specified position.
    /// 
    /// Skips tab stops with <b>Alignment</b> set to <c>TabAlignment.Bar</c>.
    /// 
    /// @param position The reference position (in points).
    /// 
    /// @return A tab stop object or null if a suitable tab stop was not found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::TabStop> Before(double position);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IComplexAttr> DeepCloneComplexAttr() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IExpandableAttr> Expand(System::SharedPtr<Aspose::Words::IExpandableAttr> parentAttr) override;
    ASPOSE_WORDS_SHARED_API void Collapse(System::SharedPtr<Aspose::Words::IExpandableAttr> parentAttr) override;

protected:

    bool get_HasTolerances();
    void set_HasTolerances(bool value);

    TabStopCollection();

    System::SharedPtr<Aspose::Words::TabStopCollection> Clone();
    System::SharedPtr<Aspose::Words::TabStop> GetByPositionTwips(int32_t positionTwips);
    int32_t GetPositionTwipsByIndex(int32_t index);
    int32_t GetIndexByPositionTwips(int32_t positionTwips);
    void RemoveByPositionTwips(int32_t positionTwips);
    void RemoveClearTabStops();
    System::ArrayPtr<float> GetBarTabPositions();
    void RemoveClearingWithTolerance();

    virtual ASPOSE_WORDS_SHARED_API ~TabStopCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::TabStop>>> mItems;
    bool mHasTolerances;

};

}
}
