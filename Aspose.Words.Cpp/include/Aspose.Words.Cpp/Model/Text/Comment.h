//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/Comment.h
#pragma once

#include <system/date_time.h>

#include "Aspose.Words.Cpp/Model/Text/INodeWithCommentId.h"
#include "Aspose.Words.Cpp/Model/Text/InlineStory.h"
#include "Aspose.Words.Cpp/Model/Sections/StoryType.h"
#include "Aspose.Words.Cpp/Model/Revisions/IMoveTrackableNode.h"

namespace Aspose { namespace Words { class CommentCollection; } }
namespace Aspose { namespace Words { class EditableRangeEnd; } }
namespace Aspose { namespace Words { class EditableRangeStart; } }
namespace Aspose { namespace Words { class MoveRangeEnd; } }
namespace Aspose { namespace Words { class MoveRangeStart; } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { class CommentRangeEnd; } }
namespace Aspose { namespace Words { class CommentRangeStart; } }
namespace Aspose { namespace Words { namespace Validation { class AnnotationValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCommentWriter; } } } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCommentsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxCommentsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Comments { class FileComments; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfAnnotationHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlAnnotationReader; } } } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }

namespace Aspose {

namespace Words {

/// Represents a container for text of a comment.
/// 
/// A comment is an annotation which is anchored to a region of text or to a position in text.
/// A comment can contain an arbitrary amount of block-level content.
/// 
/// If a <see cref="Aspose::Words::Comment">Comment</see> object occurs on its own, the comment is anchored to
/// the position of the <see cref="Aspose::Words::Comment">Comment</see> object.
/// 
/// To anchor a comment to a region of text three objects are required: <see cref="Aspose::Words::Comment">Comment</see>,
/// <see cref="Aspose::Words::CommentRangeStart">CommentRangeStart</see> and <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see>. All three objects need to share the same
/// <see cref="Aspose::Words::Comment::get_Id">Id</see> value.
/// 
/// <see cref="Aspose::Words::Comment">Comment</see> is an inline-level node and can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
/// 
/// <see cref="Aspose::Words::Comment">Comment</see> can contain <see cref="Aspose::Words::Paragraph">Paragraph</see> and <see cref="Aspose::Words::Tables::Table">Table</see> child nodes.
/// 
/// @sa Aspose::Words::CommentRangeStart
/// @sa Aspose::Words::CommentRangeEnd
class ASPOSE_WORDS_SHARED_CLASS Comment FINAL : public Aspose::Words::InlineStory, public Aspose::Words::INodeWithAnnotationId, public Aspose::Words::Revisions::IMoveTrackableNode
{
    typedef Comment ThisType;
    typedef Aspose::Words::InlineStory BaseType;
    typedef Aspose::Words::INodeWithAnnotationId BaseType1;
    typedef Aspose::Words::Revisions::IMoveTrackableNode BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::CommentCollection;
    friend class Aspose::Words::EditableRangeEnd;
    friend class Aspose::Words::EditableRangeStart;
    friend class Aspose::Words::MoveRangeEnd;
    friend class Aspose::Words::MoveRangeStart;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::CommentRangeEnd;
    friend class Aspose::Words::CommentRangeStart;
    friend class Aspose::Words::Validation::AnnotationValidator;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlCommentWriter;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxCommentsReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxCommentsWriter;
    friend class Aspose::Words::RW::Doc::Comments::FileComments;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfAnnotationHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlAnnotationReader;

public:

    /// Returns <b>NodeType.Comment</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns <b>StoryType.Comments</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StoryType get_StoryType() override;
    /// Gets the comment identifier.
    /// 
    /// The comment identifier allows to anchor a comment to a region of text in the document.
    /// The region must be demarcated using the <see cref="Aspose::Words::CommentRangeStart">CommentRangeStart</see> and <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see>
    /// object sharing the same identifier value as the <see cref="Aspose::Words::Comment">Comment</see> object.
    /// 
    /// You would use this value when looking for the <see cref="Aspose::Words::CommentRangeStart">CommentRangeStart</see> and
    /// <see cref="Aspose::Words::CommentRangeEnd">CommentRangeEnd</see> nodes that are linked to this comment.
    /// 
    /// Comment identifiers are supposed to be unique across a document and Aspose.Words automatically
    /// maintains comment identifiers when loading, saving and combining documents.
    ASPOSE_WORDS_SHARED_API int32_t get_Id() const;
    ASPOSE_WORDS_SHARED_API int32_t get_IdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_IdInternal(int32_t value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_ParentIdInternal() override;
    ASPOSE_WORDS_SHARED_API void set_ParentIdInternal(int32_t value) override;
    /// Returns or sets the initials of the user associated with a specific comment.
    /// 
    /// Cannot be null.
    /// 
    /// Default is empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Initial() const;
    /// Setter for Aspose::Words::Comment::get_Initial
    ASPOSE_WORDS_SHARED_API void set_Initial(System::String value);
    /// Gets the date and time that the comment was made.
    /// 
    /// Default is <see cref="System::DateTime::MinValue">MinValue</see>.
    ASPOSE_WORDS_SHARED_API System::DateTime get_DateTime() const;
    /// Gets the date and time that the comment was made.
    /// 
    /// Default is <see cref="System::DateTime::MinValue">MinValue</see>.
    ASPOSE_WORDS_SHARED_API void set_DateTime(System::DateTime value);
    /// Returns or sets the author name for a comment.
    /// 
    /// Cannot be null.
    /// 
    /// Default is empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Author() const;
    /// Setter for Aspose::Words::Comment::get_Author
    ASPOSE_WORDS_SHARED_API void set_Author(System::String value);
    /// Returns the parent Comment object. Returns null for top-level comments.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Comment> get_Ancestor();
    /// Returns a collection of <see cref="Aspose::Words::Comment">Comment</see> objects that are immediate children of the specified comment.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CommentCollection> get_Replies();
    /// Gets flag indicating that the comment has been marked done.
    ASPOSE_WORDS_SHARED_API bool get_Done() const;
    /// Sets flag indicating that the comment has been marked done.
    ASPOSE_WORDS_SHARED_API void set_Done(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    /// Initializes a new instance of the <b>Comment</b> class.
    /// 
    /// When <b>Comment</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Comment</b> to the document use InsertAfter or InsertBefore
    /// on the paragraph where you want the comment inserted.
    /// 
    /// After creating a comment, don't forget to set its <see cref="Aspose::Words::Comment::get_Author">Author</see>,
    /// <see cref="Aspose::Words::Comment::get_Initial">Initial</see> and <see cref="Aspose::Words::Comment::get_DateTime">DateTime</see> properties.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Comment(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    /// Initializes a new instance of the <b>Comment</b> class.
    /// 
    /// @param doc The owner document.
    /// @param author The author name for the comment. Cannot be null.
    /// @param initial The author initials for the comment. Cannot be null.
    /// @param dateTime The date and time for the comment.
    ASPOSE_WORDS_SHARED_API Comment(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::String author, System::String initial, System::DateTime dateTime);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Adds a reply to this comment.
    /// 
    /// @param author The author name for the reply.
    /// @param initial The author initials for the reply.
    /// @param dateTime The date and time for the reply.
    /// @param text The reply text.
    /// 
    /// @return The created <see cref="Aspose::Words::Comment">Comment</see> node for the reply.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Comment> AddReply(System::String author, System::String initial, System::DateTime dateTime, System::String text);
    /// Removes the specified reply to this comment.
    /// 
    /// @param reply The comment node of the deleting reply.
    ASPOSE_WORDS_SHARED_API void RemoveReply(System::SharedPtr<Aspose::Words::Comment> reply);
    /// Removes all replies to this comment.
    ASPOSE_WORDS_SHARED_API void RemoveAllReplies();
    /// This is a convenience method that allows to easily set text of the comment.
    /// 
    /// This method allows to quickly set text of a comment from a string. The string can contain
    /// paragraph breaks, this will create paragraphs of text in the comment accordingly.
    /// If you want to insert more complex elements into the comment, for example bookmarks
    /// or tables or apply rich formatting, then you need to use the appropriate node classes to
    /// build up the comment text.
    /// 
    /// @param text The new text of the comment.
    ASPOSE_WORDS_SHARED_API void SetText(System::String text);

protected:

    System::DateTime get_UtcDateTime() const;
    void set_UtcDateTime(System::DateTime value);
    int32_t get_ParentId() const;
    void set_ParentId(int32_t value);
    int32_t get_DurableId() const;
    void set_DurableId(int32_t value);
    bool get_IsIntelligentPlaceholder() const;
    void set_IsIntelligentPlaceholder(bool value);

    static const int32_t NoParent;

    Comment(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr);

    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

    virtual ASPOSE_WORDS_SHARED_API ~Comment();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t pr_DurableId;
    bool pr_IsIntelligentPlaceholder;
    bool mDone;
    int32_t mId;
    int32_t mParentId;
    System::String mInitial;
    System::String mAuthor;
    System::DateTime mDateTime;
    System::DateTime mUtcDateTime;
    System::SharedPtr<Aspose::Words::CommentCollection> mReplies;

    void InsertReplyRangeStartNode(System::SharedPtr<Aspose::Words::CommentRangeStart> parentRangeStart, System::SharedPtr<Aspose::Words::CommentRangeStart> insertingRangeStart);
    void InsertReplyRangeEndNode(System::SharedPtr<Aspose::Words::CommentRangeEnd> parentRangeEnd, System::SharedPtr<Aspose::Words::CommentRangeEnd> insertingRangeEnd);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
