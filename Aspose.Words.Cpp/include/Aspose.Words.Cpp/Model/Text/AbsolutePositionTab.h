//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/AbsolutePositionTab.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Text/SpecialChar.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Comparison { class AbsolutePositionTabComparer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanTabAbsolute; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfRunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { enum class AbsolutePositionTabAlignment; } }
namespace Aspose { namespace Words { enum class AbsolutePositionTabPositioningBase; } }
namespace Aspose { namespace Words { enum class AbsolutePositionTabLeaderChar; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// An absolute position tab is a character which is used to advance the position on
/// the current line of text when displaying this WordprocessingML content.
class ASPOSE_WORDS_SHARED_CLASS AbsolutePositionTab : public Aspose::Words::SpecialChar
{
    typedef AbsolutePositionTab ThisType;
    typedef Aspose::Words::SpecialChar BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comparison::AbsolutePositionTabComparer;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Layout::Core::SpanTabAbsolute;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Odt::OdtUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfRunPrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;

public:

    /// Accepts a visitor.
    /// 
    /// Calls DocumentVisitor.VisitAbsolutePositionTab.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Aspose::Words::AbsolutePositionTabAlignment get_Alignment() const;
    void set_Alignment(Aspose::Words::AbsolutePositionTabAlignment value);
    Aspose::Words::AbsolutePositionTabPositioningBase get_PositioningBase() const;
    void set_PositioningBase(Aspose::Words::AbsolutePositionTabPositioningBase value);
    Aspose::Words::AbsolutePositionTabLeaderChar get_LeaderChar() const;
    void set_LeaderChar(Aspose::Words::AbsolutePositionTabLeaderChar value);

    AbsolutePositionTab(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr);

    virtual ASPOSE_WORDS_SHARED_API ~AbsolutePositionTab();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::AbsolutePositionTabAlignment mAlignment;
    Aspose::Words::AbsolutePositionTabLeaderChar mLeader;
    Aspose::Words::AbsolutePositionTabPositioningBase mPositioningBase;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
