//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/Paragraph.h
#pragma once

#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRunAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IParaAttrSource.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlNodeCustomizer; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberingRestarter; } } }
namespace Aspose { namespace Words { class StyleSeparatorInserter; } }
namespace Aspose { namespace Words { namespace Fields { class FieldAutoTextUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludePictureUpdater; } } }
namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Layout { class RevisionCommentsPreprocessor; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanListRevisionsHelper; } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class RowComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Fields { class HiddenParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlBulletListItem; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlAsFormFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlAsSdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlParagraphArranger; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Reader { class MhtmlReader; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlBlockWriter; } } } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class FallbackShapeValidator; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRenderer; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class TableLayout; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class TocParagraphLevelFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEQ; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTextHelper; } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlListWrapper; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { namespace CommonBorder { class CommonBorderContainer; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTabStopExtractor; } } } } }
namespace Aspose { namespace Words { namespace RW { class FormatRevisionUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChangeReader; } } } } }
namespace Aspose { namespace Words { class RevisionNodeMatcher; } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace Fields { class FieldSymbol; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { class NodeCopier; } }
namespace Aspose { namespace Words { class NodeTextCollector; } }
namespace Aspose { namespace Words { class NodeUtil; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanList; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class UnitConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeParser; } } }
namespace Aspose { namespace Words { namespace Fields { class NodeRangeResultApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Fields { class OldResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabel; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class LegacyListFormattingConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { class TableBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class ListProps; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlFootnoteWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlI18nWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlParaBorderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class NavigationPointCollector; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUtil; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlFontResourcesCollector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableCellReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFieldWriter; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxInlineReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxParaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxCommentsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssParagraph; } } } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlCommon { class HtmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlListWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFootnoteWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtBookmarkWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtTableBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFootnoteHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyListHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlParaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlListWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { class Story; } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { class FrameFormat; } }
namespace Aspose { namespace Words { class RunCollection; } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { enum class RevisionsView; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { enum class ParaPrExpandFlags; } }
namespace Aspose { namespace Words { enum class RunPrExpandFlags; } }
namespace Aspose { namespace Words { class TabStop; } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Lists { class List; } } }

namespace Aspose {

namespace Words {

/// Represents a paragraph of text.
/// 
/// <see cref="Aspose::Words::Paragraph">Paragraph</see> is a block-level node and can be a child of classes derived from
/// <see cref="Aspose::Words::Story">Story</see> or <see cref="Aspose::Words::InlineStory">InlineStory</see>.
/// 
/// <see cref="Aspose::Words::Paragraph">Paragraph</see> can contain any number of inline-level nodes and bookmarks.
/// 
/// The complete list of child nodes that can occur inside a paragraph consists of
/// <see cref="Aspose::Words::BookmarkStart">BookmarkStart</see>, <see cref="Aspose::Words::BookmarkEnd">BookmarkEnd</see>,
/// <see cref="Aspose::Words::Fields::FieldStart">FieldStart</see>, <see cref="Aspose::Words::Fields::FieldSeparator">FieldSeparator</see>,
/// <see cref="Aspose::Words::Fields::FieldEnd">FieldEnd</see>, <see cref="Aspose::Words::Fields::FormField">FormField</see>,
/// <see cref="Aspose::Words::Comment">Comment</see>, <see cref="Aspose::Words::Footnote">Footnote</see>,
/// <see cref="Aspose::Words::Run">Run</see>, <see cref="Aspose::Words::SpecialChar">SpecialChar</see>,
/// <see cref="Aspose::Words::Drawing::Shape">Shape</see>, <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see>,
/// <see cref="Aspose::Words::Markup::SmartTag">SmartTag</see>.
/// 
/// A valid paragraph in Microsoft Word always ends with a paragraph break character and
/// a minimal valid paragraph consists just of a paragraph break. The <b>Paragraph</b>
/// class automatically appends the appropriate paragraph break character at the end
/// and this character is not part of the child nodes of the <b>Paragraph</b>, therefore
/// a <b>Paragraph</b> can be empty.
/// 
/// Do not include the end of paragraph <see cref="Aspose::Words::ControlChar::ParagraphBreak">ControlChar.ParagraphBreak</see>
/// or end of cell <see cref="Aspose::Words::ControlChar::Cell">ControlChar.Cell</see> characters inside the text of
/// the paragraph as it might make the paragraph invalid when the document is opened in Microsoft Word.
class ASPOSE_WORDS_SHARED_CLASS Paragraph : public Aspose::Words::CompositeNode, public Aspose::Words::IParaAttrSource, public Aspose::Words::IRunAttrSource, public Aspose::Words::Revisions::ITrackableNode
{
    typedef Paragraph ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::IParaAttrSource BaseType1;
    typedef Aspose::Words::IRunAttrSource BaseType2;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType3;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2, BaseType3> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Drawing::Core::Dml::DmlNodeCustomizer;
    friend class Aspose::Words::Lists::ListNumberingRestarter;
    friend class Aspose::Words::StyleSeparatorInserter;
    friend class Aspose::Words::Fields::FieldAutoTextUpdater;
    friend class Aspose::Words::Fields::FieldIncludePictureUpdater;
    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Layout::RevisionCommentsPreprocessor;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Comparison::RowComparer;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Fields::HiddenParagraphTocEntry;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::RW::Html::Reader::HtmlBulletListItem;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlAsFormFieldReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlAsSdtReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlParagraphArranger;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownTableWriter;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriterBase;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::RW::Mhtml::Reader::MhtmlReader;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::RW::Html::Writer::HtmlBlockWriter;
    friend class Aspose::Words::Replacing::FindReplace;
    friend class Aspose::Words::MailMerging::MailMergeRegion;
    friend class Aspose::Words::RevisionGroupCollection;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Validation::DmlToVml::FallbackShapeValidator;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRenderer;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::Layout::Core::TableLayout;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::TocParagraphLevelFinder;
    friend class Aspose::Words::Fields::FieldEQ;
    friend class Aspose::Words::Fields::FieldTextHelper;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::RW::Html::HtmlListWrapper;
    friend class Aspose::Words::RW::Html::Reader::CommonBorder::CommonBorderContainer;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlTabStopExtractor;
    friend class Aspose::Words::RW::FormatRevisionUtil;
    friend class Aspose::Words::RW::Odt::Reader::OdtChangeReader;
    friend class Aspose::Words::RevisionNodeMatcher;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::Fields::FieldSymbol;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocEntryExtractor;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::NodeCopier;
    friend class Aspose::Words::NodeTextCollector;
    friend class Aspose::Words::NodeUtil;
    friend class Aspose::Words::Layout::Core::SpanList;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::Lists::ListLabelUpdater;
    friend class Aspose::Words::Validation::UnitConverter;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Fields::FieldCodeParser;
    friend class Aspose::Words::Fields::NodeRangeResultApplier;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::Fields::OldResultEnumerator;
    friend class Aspose::Words::Lists::ListLabel;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::LegacyListFormattingConverter;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::TableBuilder;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;
    friend class Aspose::Words::RW::Html::ListProps;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlFootnoteWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlI18nWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlParaBorderWriter;
    friend class Aspose::Words::RW::Html::Writer::NavigationPointCollector;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::Lists::ListLabelUtil;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::Tables::Row;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::ListFormat;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlFontResourcesCollector;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableCellReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFieldWriter;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Docx::Reader::DocxInlineReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxParaReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxCommentsWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::CssParagraph;
    friend class Aspose::Words::RW::HtmlCommon::HtmlUtil;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Html::Writer::HtmlListWriter;
    friend class Aspose::Words::RW::Odt::OdtUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtFootnoteWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtBookmarkWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSpanWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtTableBuilder;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFootnoteHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyListHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlParaReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlListWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlWriter;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Returns <b>NodeType.Paragraph</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Retrieves the parent section-level story that can be <see cref="Aspose::Words::Body">Body</see> or <see cref="Aspose::Words::HeaderFooter">HeaderFooter</see>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Story> get_ParentStory();
    /// Retrieves the parent <see cref="Aspose::Words::Section">Section</see> of the paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> get_ParentSection();
    /// True if this paragraph is an immediate child of <see cref="Aspose::Words::Tables::Cell">Cell</see>; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsInCell();
    /// True if this paragraph is the last paragraph in a <see cref="Aspose::Words::Tables::Cell">Cell</see>; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsEndOfCell();
    /// True if this paragraph break is a Style Separator. A style separator allows one
    /// paragraph to consist of parts that have different paragraph styles.
    ASPOSE_WORDS_SHARED_API bool get_BreakIsStyleSeparator();
    /// True if this paragraph is the last paragraph in the <b>Body</b> (main text story) of a <b>Section</b>; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsEndOfSection();
    /// True if this paragraph is the last paragraph in the <b>HeaderFooter</b> (main text story) of a <b>Section</b>; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsEndOfHeaderFooter();
    /// True if this paragraph is the last paragraph in the last section of the document.
    ASPOSE_WORDS_SHARED_API bool get_IsEndOfDocument();
    /// Provides access to the paragraph formatting properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphFormat> get_ParagraphFormat();
    /// Provides access to the list formatting properties of the paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ListFormat> get_ListFormat();
    /// Provides access to the paragraph formatting properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::FrameFormat> get_FrameFormat();
    /// Gets a <see cref="Aspose::Words::Paragraph::get_ListLabel">ListLabel</see> object that provides access to list numbering value and formatting
    /// for this paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::ListLabel> get_ListLabel();
    /// Provides access to the typed collection of pieces of text inside the paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunCollection> get_Runs();
    /// Provides access to the font formatting of the paragraph break character.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_ParagraphBreakFont();
    /// Returns true if this object was inserted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsInsertRevision();
    /// Returns true if this object was deleted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsDeleteRevision();
    /// Returns <b>true</b> if this object was moved (deleted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveFromRevision();
    /// Returns <b>true</b> if this object was moved (inserted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveToRevision();
    /// Returns true if formatting of the object was changed in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsFormatRevision();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    /// True when the paragraph is an item in a bulleted or numbered list in original revision.
    ASPOSE_WORDS_SHARED_API bool get_IsListItem();

    /// Initializes a new instance of the <b>Paragraph</b> class.
    /// 
    /// When <b>Paragraph</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Paragraph</b> to the document use InsertAfter or InsertBefore
    /// on the story where you want the paragraph inserted.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Paragraph(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Gets the text of this paragraph including the end of paragraph character.
    /// 
    /// The text of all child nodes is concatenated and the end of paragraph character is appended as follows:
    /// 
    /// - If the paragraph is the last paragraph of <see cref="Aspose::Words::Body">Body</see>, then
    ///   <see cref="Aspose::Words::ControlChar::SectionBreak">ControlChar.SectionBreak</see> (\\x000c) is appended.
    /// - If the paragraph is the last paragraph of <see cref="Aspose::Words::Tables::Cell">Cell</see>, then
    ///   <see cref="Aspose::Words::ControlChar::Cell">ControlChar.Cell</see> (\\x0007) is appended.
    /// - For all other paragraphs
    ///   <see cref="Aspose::Words::ControlChar::ParagraphBreak">ControlChar.ParagraphBreak</see> (\\r) is appended.
    /// 
    /// The returned string includes all control and special characters as described in <see cref="Aspose::Words::ControlChar">ControlChar</see>.
    ASPOSE_WORDS_SHARED_API System::String GetText() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetParaAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearParaAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;
    /// Returns array of all tab stops applied to this paragraph, including applied indirectly by styles or lists.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::SharedPtr<Aspose::Words::TabStop>> GetEffectiveTabStops();
    /// Joins runs with the same formatting in the paragraph.
    /// 
    /// @return Number of joins performed. When <b>N</b> adjacent runs are being joined they count as <b>N - 1</b> joins.
    ASPOSE_WORDS_SHARED_API int32_t JoinRunsWithSameFormatting();
    /// Appends a field to this paragraph.
    /// 
    /// @param fieldType The type of the field to append.
    /// @param updateField Specifies whether to update the field immediately.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the appended field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> AppendField(Aspose::Words::Fields::FieldType fieldType, bool updateField);
    /// Appends a field to this paragraph.
    /// 
    /// @param fieldCode The field code to append (without curly braces).
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the appended field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> AppendField(System::String fieldCode);
    /// Appends a field to this paragraph.
    /// 
    /// @param fieldCode The field code to append (without curly braces).
    /// @param fieldValue The field value to append. Pass null for fields that do not have a value.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the appended field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> AppendField(System::String fieldCode, System::String fieldValue);
    /// Inserts a field into this paragraph.
    /// 
    /// @param fieldType The type of the field to insert.
    /// @param updateField Specifies whether to update the field immediately.
    /// @param refNode Reference node inside this paragraph (if refNode is null, then appends to the end of the paragraph).
    /// @param isAfter Whether to insert the field after or before reference node.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the inserted field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> InsertField(Aspose::Words::Fields::FieldType fieldType, bool updateField, System::SharedPtr<Aspose::Words::Node> refNode, bool isAfter);
    /// Inserts a field into this paragraph.
    /// 
    /// @param fieldCode The field code to insert (without curly braces).
    /// @param refNode Reference node inside this paragraph (if refNode is null, then appends to the end of the paragraph).
    /// @param isAfter Whether to insert the field after or before reference node.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the inserted field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> InsertField(System::String fieldCode, System::SharedPtr<Aspose::Words::Node> refNode, bool isAfter);
    /// Inserts a field into this paragraph.
    /// 
    /// @param fieldCode The field code to insert (without curly braces).
    /// @param fieldValue The field value to insert. Pass null for fields that do not have a value.
    /// @param refNode Reference node inside this paragraph (if refNode is null, then appends to the end of the paragraph).
    /// @param isAfter Whether to insert the field after or before reference node.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the inserted field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> InsertField(System::String fieldCode, System::String fieldValue, System::SharedPtr<Aspose::Words::Node> refNode, bool isAfter);

protected:

    bool get_IsStartOfCell();
    bool get_IsStartOfRow();
    bool get_IsEndOfRow();
    bool get_IsStartOfTable();
    bool get_IsEndOfTable();
    System::SharedPtr<Aspose::Words::Tables::Cell> get_ParentCell();
    System::SharedPtr<Aspose::Words::Tables::Row> get_ParentRow();
    System::SharedPtr<Aspose::Words::Tables::Table> get_ParentTable();
    bool get_IsInShape();
    bool get_IsSectionBreakParagraph();
    System::SharedPtr<Aspose::Words::Style> get_ParagraphStyle();
    System::SharedPtr<Aspose::Words::Style> get_ParagraphBreakCharacterStyle();
    System::SharedPtr<Aspose::Words::ParaPr> get_ParaPr() const;
    void set_ParaPr(System::SharedPtr<Aspose::Words::ParaPr> value);
    System::SharedPtr<Aspose::Words::RunPr> get_ParagraphBreakRunPr() const;
    void set_ParagraphBreakRunPr(System::SharedPtr<Aspose::Words::RunPr> value);
    bool get_IsEndOfComment();
    bool get_IsEndOfFootnote();
    bool get_IsEndOfNoteSeparator();
    bool get_IsEndOfTextbox();
    System::SharedPtr<Aspose::Words::Run> get_FirstRun();
    bool get_IsInMainTextStory();
    bool get_HasListLabel();
    bool get_HasListLabelFinal();
    bool get_HasRevisions();
    bool get_HasNonWhitespaceChildren();
    bool get_IsEmptyOrContainsOnlyCrossAnnotation();
    bool get_IsListItemOriginal();
    bool get_IsListItemFinal();
    System::SharedPtr<Aspose::Words::Lists::ListLevel> get_ListLevel();
    int32_t get_ParaId() const;
    void set_ParaId(int32_t value);
    int32_t get_TextId() const;
    void set_TextId(int32_t value);

    Paragraph(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::ParaPr> paraPr, System::SharedPtr<Aspose::Words::RunPr> runPr);

    System::SharedPtr<Aspose::Words::Style> GetParagraphStyle(Aspose::Words::RevisionsView view);
    void CloneListLabelStringAndValueIfNeeded(System::SharedPtr<Aspose::Words::Paragraph> other);
    void ResetListLabel(Aspose::Words::RevisionsView view);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    System::SharedPtr<Aspose::Words::ParaPr> GetExpandedParaPr(Aspose::Words::ParaPrExpandFlags flags);
    void ExpandParaPr(System::SharedPtr<Aspose::Words::ParaPr> dstParaPr, Aspose::Words::ParaPrExpandFlags flags);
    System::SharedPtr<Aspose::Words::RunPr> GetExpandedParagraphBreakRunPr(Aspose::Words::RunPrExpandFlags flags);
    void ExpandParagraphBreakRunPr(System::SharedPtr<Aspose::Words::RunPr> dstRunPr, Aspose::Words::RunPrExpandFlags flags);
    void ApplyFormatting(System::SharedPtr<Aspose::Words::ParaPr> newParaPr);
    void GetMinMaxFontSize(double& minFontSize, double& maxFontSize, bool& sizeCanChange);
    ASPOSE_WORDS_SHARED_API System::String GetEndText() override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    System::SharedPtr<System::Object> FetchInheritedParaAttr(int32_t key, Aspose::Words::RevisionsView view);
    System::SharedPtr<System::Object> FetchParaAttr(int32_t key, Aspose::Words::RevisionsView view);
    bool IsConformingWithNext();
    bool IsSameFloatingPositioning(System::SharedPtr<Aspose::Words::Paragraph> rhs);
    System::SharedPtr<Aspose::Words::Run> GetLastRun();
    System::SharedPtr<Aspose::Words::Run> GetLastRun(bool skipEmpty);
    int32_t JoinRunsWithSameFormatting(System::SharedPtr<System::Text::StringBuilder> sb);
    System::SharedPtr<Aspose::Words::Lists::List> GetList(bool isRevised);
    System::SharedPtr<Aspose::Words::Lists::ListLevel> GetListLevel(bool isRevised);

    virtual ASPOSE_WORDS_SHARED_API ~Paragraph();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsTableStyleAffected();

    System::SharedPtr<Aspose::Words::ParaPr> mParaPr;
    System::SharedPtr<Aspose::Words::RunPr> mParagraphBreakRunPr;
    System::SharedPtr<Aspose::Words::ParagraphFormat> mParagraphFormat;
    System::SharedPtr<Aspose::Words::FrameFormat> mFrameFormat;
    System::SharedPtr<Aspose::Words::ListFormat> mListFormat;
    System::SharedPtr<Aspose::Words::Lists::ListLabel> mListLabel;
    System::SharedPtr<Aspose::Words::RunCollection> mRuns;
    int32_t mParaId;
    int32_t mTextId;

    static void ConsiderInMinMaxFontSize(System::SharedPtr<Aspose::Words::IRunAttrSource> attrSource, double& minFontSize, double& maxFontSize, bool& sizeCanChange);
    static int32_t JoinRunsSpecificParent(System::SharedPtr<Aspose::Words::CompositeNode> parent, System::SharedPtr<System::Text::StringBuilder> sb);
    static void FlushJoinedRunsText(System::SharedPtr<Aspose::Words::Run> run, System::SharedPtr<System::Text::StringBuilder> sb);
    System::SharedPtr<Aspose::Words::RunPr> GetRunPr(System::SharedPtr<Aspose::Words::Node> refNode);
    void UpdateFromUnits();
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
