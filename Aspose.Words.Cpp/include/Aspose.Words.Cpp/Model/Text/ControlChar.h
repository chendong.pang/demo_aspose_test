//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/ControlChar.h
#pragma once

#include <system/string.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceLegacy; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanDirectionalFormattingCharacter; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutSpanCharacterCollectionGenerator; } } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerIndexer; } } }
namespace Aspose { namespace Words { namespace Layout { class TextSplitter; } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMathUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { class LinkDestinationBlock; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { class MarkdownUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class DelimiterFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LineBandReflower; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { namespace Shaping { class ArabicShaper; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanRef; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanSymbol; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanType; } } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentRunSplitter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class LineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { namespace Shaping { class RunString; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageDocumentTreeWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { class Comment; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfTokenizer2; } } } } }
namespace Aspose { namespace Words { namespace RW { class RunWriter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlWriter; } } } } }

namespace Aspose {

namespace Words {

/// Control characters often encountered in documents.
class ASPOSE_WORDS_SHARED_CLASS ControlChar
{
    typedef ControlChar ThisType;

    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Replacing::FindReplaceLegacy;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Layout::Core::SpanDirectionalFormattingCharacter;
    friend class Aspose::Words::Layout::PreAps::LayoutSpanCharacterCollectionGenerator;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::ComparerIndexer;
    friend class Aspose::Words::Layout::TextSplitter;
    friend class Aspose::Words::Math::OfficeMathUtil;
    friend class Aspose::Words::RW::Markdown::LinkDestinationBlock;
    friend class Aspose::Words::RW::Markdown::MarkdownUtil;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Fields::DelimiterFieldBuildingBlock;
    friend class Aspose::Words::Layout::Core::LineBandReflower;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::Layout::Core::Shaping::ArabicShaper;
    friend class Aspose::Words::Fields::FieldCodeIndex;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Layout::Core::SpanRef;
    friend class Aspose::Words::Layout::Core::SpanSymbol;
    friend class Aspose::Words::Layout::Core::SpanType;
    friend class Aspose::Words::Layout::DocumentRunSplitter;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Fields::FormField;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::ApsBuilder::Spans::LineApsBuilder;
    friend class Aspose::Words::Layout::Core::Shaping::RunString;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageDocumentTreeWriter;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Comment;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Rtf::Reader::RtfTokenizer2;
    friend class Aspose::Words::RW::RunWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxRunWriter;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Doc::Writer::ShapeWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtSpanWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlRunWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlWriter;

public:

    /// End of a table cell or end of a table row character: "\x0007" or "\a".
    static ASPOSE_WORDS_SHARED_API System::String& Cell();
    /// Tab character: "\x0009" or "\t".
    static ASPOSE_WORDS_SHARED_API System::String& Tab();
    /// Line feed character: "\x000a" or "\n". Same as <see cref="Aspose::Words::ControlChar::LineFeed">LineFeed</see>.
    static ASPOSE_WORDS_SHARED_API System::String& Lf();
    /// Line feed character: "\x000a" or "\n". Same as <see cref="Aspose::Words::ControlChar::Lf">Lf</see>.
    static ASPOSE_WORDS_SHARED_API System::String& LineFeed();
    /// Line break character: "\x000b" or "\v".
    static ASPOSE_WORDS_SHARED_API System::String& LineBreak();
    /// Page break character: "\x000c" or "\f". Note it has the same value as <see cref="Aspose::Words::ControlChar::SectionBreak">SectionBreak</see>.
    static ASPOSE_WORDS_SHARED_API System::String& PageBreak();
    /// End of section character: "\x000c" or "\f". Note it has the same value as <see cref="Aspose::Words::ControlChar::PageBreak">PageBreak</see>.
    static ASPOSE_WORDS_SHARED_API System::String& SectionBreak();
    /// Carriage return character: "\x000d" or "\r". Same as <see cref="Aspose::Words::ControlChar::ParagraphBreak">ParagraphBreak</see>.
    static ASPOSE_WORDS_SHARED_API System::String& Cr();
    /// End of paragraph character: "\x000d" or "\r". Same as <see cref="Aspose::Words::ControlChar::Cr">Cr</see>
    static ASPOSE_WORDS_SHARED_API System::String& ParagraphBreak();
    /// End of column character: "\x000e".
    static ASPOSE_WORDS_SHARED_API System::String& ColumnBreak();
    /// Carriage return followed by line feed character: "\x000d\x000a" or "\r\n".
    /// Not used as such in Microsoft Word documents, but commonly used in text files for paragraph breaks.
    static ASPOSE_WORDS_SHARED_API System::String& CrLf();
    /// Non-breaking space character: "\x00a0".
    static ASPOSE_WORDS_SHARED_API System::String& NonBreakingSpace();

protected:

    static System::String& Picture();
    static System::String& FootnoteRef();
    static System::String& AnnotationRef();
    static System::String& FieldStart();
    static System::String& FieldSeparator();
    static System::String& FieldEnd();
    static System::String& SdtStart();
    static System::String& SdtEnd();
    static System::String& Space();
    static System::String& NonBreakingHyphen();
    static System::String& OptionalHyphen();
    static System::String& UnicodeNonBreakingHyphen();
    static System::String& UnicodeOptionalHyphen();
    static System::String& DefaultTextInput();

public:

    /// End of a table cell or end of a table row character: (char)7 or "\a".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t CellChar = (char16_t)7;
    /// Tab character: (char)9 or "\t".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t TabChar = (char16_t)9;
    /// Line feed character: (char)10 or "\n".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t LineFeedChar = (char16_t)10;
    /// Line break character: (char)11 or "\v".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t LineBreakChar = (char16_t)11;
    /// Page break character: (char)12 or "\f".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t PageBreakChar = (char16_t)12;
    /// End of section character: (char)12 or "\f".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t SectionBreakChar = (char16_t)12;
    /// End of paragraph character: (char)13 or "\r".
    static constexpr ASPOSE_WORDS_SHARED_API char16_t ParagraphBreakChar = (char16_t)13;
    /// End of column character: (char)14.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t ColumnBreakChar = (char16_t)14;
    /// Start of MS Word field character: (char)19.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t FieldStartChar = (char16_t)19;
    /// Field separator character separates field code from field value. Optional in some fields. Value: (char)20.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t FieldSeparatorChar = (char16_t)20;
    /// End of MS Word field character: (char)21.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t FieldEndChar = (char16_t)21;
    /// Nonbreaking Hyphen in Microsoft Word is (char)30.
    /// 
    /// Nonbreaking Hyphen in Microsoft Word does not correspond to the
    /// Unicode character U+2011 non-breaking hyphen but instead represents
    /// internal information that tells Microsoft Word to display a hyphen and not to break a line.
    /// 
    /// Useful info: http://www.cs.tut.fi/~jkorpela/dashes.html#linebreaks.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t NonBreakingHyphenChar = (char16_t)30;
    /// Optional Hyphen in Microsoft Word is (char)31.
    /// 
    /// Optional Hyphen in Microsoft Word does not correspond to the Unicode character U+00AD soft hyphen.
    /// Instead, it inserts internal information that tells Word about a possible hyphenation point.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t OptionalHyphenChar = (char16_t)31;
    /// Space character: (char)32.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t SpaceChar = (char16_t)32;
    /// Non-breaking space character: (char)160.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t NonBreakingSpaceChar = u'\x00a0';
    /// This is the "o" character used as a default value in text input form fields.
    static constexpr ASPOSE_WORDS_SHARED_API char16_t DefaultTextInputChar = u'\x2002';

protected:

    static constexpr char16_t PictureChar = (char16_t)1;
    static constexpr char16_t FootnoteRefChar = (char16_t)2;
    static constexpr char16_t FootnoteSeparatorChar = (char16_t)3;
    static constexpr char16_t FootnoteContinuationChar = (char16_t)4;
    static constexpr char16_t AnnotationRefChar = (char16_t)5;
    static constexpr char16_t SdtStartChar = (char16_t)0x3C;
    static constexpr char16_t SdtEndChar = (char16_t)0x3E;
    static constexpr char16_t BackSlashChar = u'\\';
    static constexpr char16_t YenChar = u'\x00a5';
    static constexpr char16_t UnicodeOptionalHyphenChar = u'\x00ad';
    static constexpr char16_t EnSpaceChar = u'\x2002';
    static constexpr char16_t EmSpaceChar = u'\x2003';
    static constexpr char16_t FourPerEmSpaceChar = u'\x2005';
    static constexpr char16_t IdeographicSpaceChar = u'\x3000';
    static constexpr char16_t LeftToRightMarkChar = u'\x200e';
    static constexpr char16_t RightToLeftMarkChar = u'\x200f';
    static constexpr char16_t LeftToRightEmbedding = u'\x202A';
    static constexpr char16_t RightToLeftEmbedding = u'\x202B';
    static constexpr char16_t LeftToRightOverride = u'\x202D';
    static constexpr char16_t RightToLeftOverride = u'\x202E';
    static constexpr char16_t PopDirectionalFormatting = u'\x202C';
    static constexpr char16_t LeftToRightIsolate = u'\x2066';
    static constexpr char16_t RightToLeftIsolate = u'\x2067';
    static constexpr char16_t FirstStrongIsolate = u'\x2068';
    static constexpr char16_t PopDirectionalIsolate = u'\x2069';
    static constexpr char16_t ArabicLetterMark = u'\x061C';
    static constexpr char16_t UnicodeNonBreakingHyphenChar = u'\x2011';
    static constexpr char16_t EnDashChar = u'\x2013';
    static constexpr char16_t EmDashChar = u'\x2014';
    static constexpr char16_t ZeroWidthNonJoinerChar = u'\x200c';
    static constexpr char16_t ZeroWidthJoinerChar = u'\x200d';
    static constexpr char16_t ZeroWidthSpaceChar = u'\x200b';
    static constexpr char16_t ZeroWithNoBreakSpace = u'\xfeff';
    static constexpr char16_t UnicodeLineSeparator = u'\x2028';
    static constexpr char16_t NoWidthOptionalBreakChar = ZeroWidthNonJoinerChar;
    static constexpr char16_t NoWidthNonBreakChar = ZeroWidthJoinerChar;

};

}
}
