//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Text/CommentCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/NodeMatchers/NodeMatcher.h"
#include "Aspose.Words.Cpp/Model/Nodes/NodeCollection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Comment; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class DocumentBase; } }

namespace Aspose {

namespace Words {

/// Provides typed access to a collection of <see cref="Aspose::Words::Comment">Comment</see> nodes.
class ASPOSE_WORDS_SHARED_CLASS CommentCollection : public Aspose::Words::NodeCollection
{
    typedef CommentCollection ThisType;
    typedef Aspose::Words::NodeCollection BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comment;

private:

    class CommentReplyMatcher : public Aspose::Words::NodeMatcher
    {
        typedef CommentReplyMatcher ThisType;
        typedef Aspose::Words::NodeMatcher BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        bool get_IsSkipMarkupNodes() override;

        CommentReplyMatcher(System::SharedPtr<Aspose::Words::Comment> parentComment);

        bool IsMatch(System::SharedPtr<Aspose::Words::Node> node) override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::WeakPtr<Aspose::Words::Comment> mParentComment;

    };

public:

    /// Retrieves a <b>Comment</b> at the given index.
    /// 
    /// The index is zero-based.
    /// 
    /// Negative indexes are allowed and indicate access from the back of the collection.
    /// For example -1 means the last item, -2 means the second before last and so on.
    /// 
    /// If index is greater than or equal to the number of items in the list, this returns a null reference.
    /// 
    /// If index is negative and its absolute value is greater than the number of items in the list, this returns a null reference.
    /// 
    /// @param index An index into the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Comment> idx_get(int32_t index);

protected:

    CommentCollection(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Comment> parentComment);

    virtual ASPOSE_WORDS_SHARED_API ~CommentCollection();

};

}
}
