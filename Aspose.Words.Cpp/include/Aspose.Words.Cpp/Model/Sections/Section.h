//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Sections/Section.h
#pragma once

#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Formatting/ISectionAttrSource.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { class ImportInfoCollector; } }
namespace Aspose { namespace Words { class StyleSeparatorInserter; } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxParaPrReaderBase; } } } } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeSizeValidationHelper; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { class NodeCopier; } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class UnitConverter; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormat; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class Iso29500ComplianceEnforcer; } } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlI18nWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxSectPrWriter; } } } } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextIndexesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtSectionReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtHeaderFooterReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtMasterPageReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextContentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtContentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSectionPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSectionWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfSectPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlBodyReader; } } } } }
namespace Aspose { namespace Words { class Body; } }
namespace Aspose { namespace Words { class HeaderFooterCollection; } }
namespace Aspose { namespace Words { class PageSetup; } }
namespace Aspose { namespace Words { class SectPr; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { class Story; } }
namespace Aspose { namespace Words { enum class StoryType; } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { enum class HeaderFooterType; } }

namespace Aspose {

namespace Words {

/// Represents a single section in a document.
/// 
/// <b>Section</b> can have one <see cref="Aspose::Words::Section::get_Body">Body</see> and maximum one <see cref="Aspose::Words::HeaderFooter">HeaderFooter</see>
/// of each <see cref="Aspose::Words::HeaderFooterType">HeaderFooterType</see>. <b>Body</b> and <b>HeaderFooter</b> nodes
/// can be in any order inside <b>Section</b>.
/// 
/// A minimal valid section needs to have <b>Body</b> with one <b>Paragraph</b>.
/// 
/// Each section has its own set of properties that specify page size, orientation, margins etc.
/// 
/// You can create a copy of a section using <see cref="Aspose::Words::Node::Clone(bool)">Clone()</see>. The copy can be inserted into
/// the same or different document.
/// 
/// To add, insert or remove a whole section including section break and
/// section properties use methods of the <b>Sections</b> object.
/// 
/// To copy and insert just content of the section excluding the section break
/// and section properties use <b>AppendContent</b> and <b>PrependContent</b> methods.
class ASPOSE_WORDS_SHARED_CLASS Section FINAL : public Aspose::Words::CompositeNode, public Aspose::Words::ISectionAttrSource
{
    typedef Section ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::ISectionAttrSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::ImportInfoCollector;
    friend class Aspose::Words::StyleSeparatorInserter;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxParaPrReaderBase;
    friend class Aspose::Words::RevisionGroupCollection;
    friend class Aspose::Words::Drawing::ShapeSizeValidationHelper;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::NodeCopier;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Validation::UnitConverter;
    friend class Aspose::Words::Fields::FieldFormat;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::Iso29500ComplianceEnforcer;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlI18nWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxSectPrWriter;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextIndexesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtSectionReader;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtHeaderFooterReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtMasterPageReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextContentReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtContentReader;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::OdtUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtSectionPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSectionWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfSectPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlBodyReader;

public:
    using Aspose::Words::CompositeNode::Clone;

private:

    class FirstSectionAttrSource : public Aspose::Words::ISectionAttrSource
    {
        typedef FirstSectionAttrSource ThisType;
        typedef Aspose::Words::ISectionAttrSource BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        FirstSectionAttrSource(System::SharedPtr<Aspose::Words::Section> section);

        System::SharedPtr<System::Object> GetDirectSectionAttr(int32_t key) override;
        System::SharedPtr<System::Object> FetchInheritedSectionAttr(int32_t key) override;
        System::SharedPtr<System::Object> FetchSectionAttr(int32_t key) override;
        void SetSectionAttr(int32_t key, System::SharedPtr<System::Object> value) override;
        void ClearSectionAttrs() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::ISectionAttrSource> get_FirstOrCurrentSection();

        System::WeakPtr<Aspose::Words::Section> mSection;

    };

public:

    /// Returns <b>NodeType.Section</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns the <b>Body</b> child node of the section.
    /// 
    /// <b>Body</b> contains main text of the section.
    /// 
    /// Returns null if the section does not have a <b>Body</b> node among its children.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Body> get_Body();
    /// Provides access to the headers and footers nodes of the section.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::HeaderFooterCollection> get_HeadersFooters();
    /// Returns an object that represents page setup and section properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::PageSetup> get_PageSetup();
    /// True if the section is protected for forms. When a section is protected for forms,
    /// users can select and modify text only in form fields in Microsoft Word.
    ASPOSE_WORDS_SHARED_API bool get_ProtectedForForms();
    /// True if the section is protected for forms. When a section is protected for forms,
    /// users can select and modify text only in form fields in Microsoft Word.
    ASPOSE_WORDS_SHARED_API void set_ProtectedForForms(bool value);

    /// Initializes a new instance of the Section class.
    /// 
    /// When the section is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To include Section into a document use Document.InsertAfter, Document.InsertBefore
    /// or Sections.Add and Section.Insert methods.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Section(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Creates a duplicate of this section.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> Clone();
    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Inserts a copy of content of the source section at the beginning of this section.
    /// 
    /// Only content of <see cref="Aspose::Words::Section::get_Body">Body</see> of the source section is copied, page setup,
    /// headers and footers are not copied.
    /// 
    /// The nodes are automatically imported if the source section belongs to a different document.
    /// 
    /// No new section is created in the destination document.
    /// 
    /// @param sourceSection The section to copy content from.
    ASPOSE_WORDS_SHARED_API void PrependContent(System::SharedPtr<Aspose::Words::Section> sourceSection);
    /// Inserts a copy of content of the source section at the end of this section.
    /// 
    /// Only content of <see cref="Aspose::Words::Section::get_Body">Body</see> of the source section is copied, page setup,
    /// headers and footers are not copied.
    /// 
    /// The nodes are automatically imported if the source section belongs to a different document.
    /// 
    /// No new section is created in the destination document.
    /// 
    /// @param sourceSection The section to copy content from.
    ASPOSE_WORDS_SHARED_API void AppendContent(System::SharedPtr<Aspose::Words::Section> sourceSection);
    /// Clears the section.
    /// 
    /// The text of <see cref="Aspose::Words::Section::get_Body">Body</see> is cleared, only one empty paragraph is left that represents the section break.
    /// 
    /// The text of all headers and footers is cleared, but <see cref="Aspose::Words::HeaderFooter">HeaderFooter</see> objects themselves are not removed.
    ASPOSE_WORDS_SHARED_API void ClearContent();
    /// Clears the headers and footers of this section.
    /// 
    /// The text of all headers and footers is cleared, but <see cref="Aspose::Words::HeaderFooter">HeaderFooter</see> objects themselves are not removed.
    /// 
    /// This makes headers and footers of this section linked to headers and footers of the previous section.
    ASPOSE_WORDS_SHARED_API void ClearHeadersFooters();
    /// Deletes all shapes (drawing objects) from the headers and footers of this section.
    ASPOSE_WORDS_SHARED_API void DeleteHeaderFooterShapes();
    /// Ensures that the section has Body with one Paragraph.
    ASPOSE_WORDS_SHARED_API void EnsureMinimum();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetSectionAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearSectionAttrs() override;

protected:

    System::SharedPtr<Aspose::Words::SectPr> get_SectPr() const;
    void set_SectPr(System::SharedPtr<Aspose::Words::SectPr> value);
    bool get_IsFirstSection();
    bool get_IsLastSection();
    bool get_IsSingleSection();

    Section(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::SectPr> sectPr);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    System::SharedPtr<Aspose::Words::Story> GetStory(Aspose::Words::StoryType storyType);
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    void AddWatermark(System::SharedPtr<Aspose::Words::Drawing::Shape> shape, bool createHeaders);
    void RemoveWatermark();

    virtual ASPOSE_WORDS_SHARED_API ~Section();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::SectPr> mSectPr;
    System::SharedPtr<Aspose::Words::PageSetup> mPageSetup;
    System::SharedPtr<Aspose::Words::HeaderFooterCollection> mHeadersFooters;

    void AddWatermarkIntoHeader(System::SharedPtr<Aspose::Words::Drawing::Shape> shape, Aspose::Words::HeaderFooterType headerType, bool createHeader);
    void InsertContent(System::SharedPtr<Aspose::Words::Section> sourceSection, bool isAfter);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
