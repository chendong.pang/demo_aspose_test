//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Sections/Story.h
#pragma once

#include "Aspose.Words.Cpp/Model/Sections/StoryType.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"

namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { class Body; } }
namespace Aspose { namespace Words { class HeaderFooter; } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtContentReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphWriter; } } } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class ParagraphCollection; } }
namespace Aspose { namespace Words { namespace Tables { class TableCollection; } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeCollection; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }

namespace Aspose {

namespace Words {

/// Base class for elements that contain block-level nodes <see cref="Aspose::Words::Paragraph">Paragraph</see> and <see cref="Aspose::Words::Tables::Table">Table</see>.
/// 
/// Text of a Word document is said to consist of several stories.
/// The main text is stored in the main text story represented by <see cref="Aspose::Words::Body">Body</see>,
/// each header and footer is stored in a separate story represented by <see cref="Aspose::Words::HeaderFooter">HeaderFooter</see>.
class ASPOSE_WORDS_SHARED_CLASS Story : public Aspose::Words::CompositeNode
{
    typedef Story ThisType;
    typedef Aspose::Words::CompositeNode BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::Body;
    friend class Aspose::Words::HeaderFooter;
    friend class Aspose::Words::Section;
    friend class Aspose::Words::RW::Odt::Reader::OdtContentReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Gets the type of this story.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StoryType get_StoryType() const;
    /// Gets the first paragraph in the story.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_FirstParagraph();
    /// Gets the last paragraph in the story.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_LastParagraph();
    /// Gets a collection of paragraphs that are immediate children of the story.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphCollection> get_Paragraphs();
    /// Gets a collection of tables that are immediate children of the story.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::TableCollection> get_Tables();

    /// Deletes all shapes from the text of this story.
    ASPOSE_WORDS_SHARED_API void DeleteShapes();
    /// A shortcut method that creates a <see cref="Aspose::Words::Paragraph">Paragraph</see> object with optional text and appends it to the end of this object.
    /// 
    /// @param text The text for the paragraph. Can be null or empty string.
    /// 
    /// @return The newly created and appended paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> AppendParagraph(System::String text);

protected:

    System::SharedPtr<Aspose::Words::Drawing::ShapeCollection> get_Shapes();
    bool get_IsSectionBreak();

    Story(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::StoryType storyType);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;

    virtual ASPOSE_WORDS_SHARED_API ~Story();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::StoryType mStoryType;
    System::SharedPtr<Aspose::Words::ParagraphCollection> mParagraphs;
    System::SharedPtr<Aspose::Words::Tables::TableCollection> mTables;
    System::SharedPtr<Aspose::Words::Drawing::ShapeCollection> mShapes;

};

}
}
