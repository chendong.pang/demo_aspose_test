//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Sections/TextColumn.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxSectPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class SectPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class SectPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxSectPrWriter; } } } } }
namespace Aspose { namespace Words { class SectPr; } }
namespace Aspose { namespace Words { class TextColumnCollectionInternal; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtPageLayoutPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSectionPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfSectPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfSectPrWriter; } } } } }

namespace Aspose {

namespace Words {

/// Represents a single text column. <b>TextColumn</b> is a member of the <see cref="Aspose::Words::TextColumnCollection">TextColumnCollection</see> collection.
/// The <b>TextColumns</b> collection includes all the columns in a section of a document.
/// 
/// <b>TextColumn</b> objects are only used to specify columns with custom width and spacing. If you want
/// the columns in the document to be of equal width, set TextColumns.<see cref="Aspose::Words::TextColumnCollection::get_EvenlySpaced">EvenlySpaced</see> to <b>true</b>.
/// 
/// When a new <b>TextColumn</b> is created it has its width and spacing set to zero.
/// 
/// @sa Aspose::Words::TextColumnCollection
/// @sa Aspose::Words::PageSetup
/// @sa Aspose::Words::Section
class ASPOSE_WORDS_SHARED_CLASS TextColumn : public System::Object
{
    typedef TextColumn ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Nrx::Reader::NrxSectPrReaderBase;
    friend class Aspose::Words::RW::Doc::Reader::SectPrReader;
    friend class Aspose::Words::RW::Doc::Writer::SectPrWriter;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxSectPrWriter;
    friend class Aspose::Words::SectPr;
    friend class Aspose::Words::TextColumnCollectionInternal;
    friend class Aspose::Words::RW::Odt::Reader::OdtPageLayoutPropertiesReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtSectionPropertiesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfSectPrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfSectPrWriter;

public:

    /// Gets the width of the text column in points.
    ASPOSE_WORDS_SHARED_API double get_Width();
    /// Sets the width of the text column in points.
    ASPOSE_WORDS_SHARED_API void set_Width(double value);
    /// Gets the space between this column and the next column in points. Not required for the last column.
    ASPOSE_WORDS_SHARED_API double get_SpaceAfter();
    /// Sets the space between this column and the next column in points. Not required for the last column.
    ASPOSE_WORDS_SHARED_API void set_SpaceAfter(double value);

protected:

    int32_t get_RawWidth() const;
    void set_RawWidth(int32_t value);
    int32_t get_RawSpaceAfter() const;
    void set_RawSpaceAfter(int32_t value);

    TextColumn();

    System::SharedPtr<Aspose::Words::TextColumn> Clone();

private:

    int32_t mWidth;
    int32_t mSpaceAfter;

};

}
}
