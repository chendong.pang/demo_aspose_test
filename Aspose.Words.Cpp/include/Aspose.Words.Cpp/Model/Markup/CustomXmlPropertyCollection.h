//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/CustomXmlPropertyCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Markup/CustomXmlProperty.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSmartTagReader; } } } } }
namespace Aspose { namespace Words { namespace Markup { class SmartTag; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Markup { class SmartTagStartProcessor; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlInlineReader; } } } } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedStringListGeneric; } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents a collection of custom XML attributes or smart tag properties.
/// 
/// Items are <see cref="Aspose::Words::Markup::CustomXmlProperty">CustomXmlProperty</see> objects.
class ASPOSE_WORDS_SHARED_CLASS CustomXmlPropertyCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty>>
{
    typedef CustomXmlPropertyCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::RW::Docx::Reader::DocxSmartTagReader;
    friend class Aspose::Words::Markup::SmartTag;
    friend class Aspose::Words::RW::Doc::Markup::SmartTagStartProcessor;
    friend class Aspose::Words::RW::Wml::Reader::WmlInlineReader;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets a property with the specified name.
    /// 
    /// @param name Case-sensitive name of the property to locate.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty> idx_get(System::String name);
    /// Gets a property at the specified index.
    /// 
    /// @param index Zero-based index of the property.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty> idx_get(int32_t index);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty>>> GetEnumerator() override;
    /// Adds a property to the collection.
    /// 
    /// @param property The property to add.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty> property);
    /// Determines whether the collection contains a property with the given name.
    /// 
    /// @param name Case-sensitive name of the property to locate.
    /// 
    /// @return True if the item is found in the collection; otherwise, false.
    ASPOSE_WORDS_SHARED_API bool Contains(System::String name);
    /// Returns the zero-based index of the specified property in the collection.
    /// 
    /// @param name The case-sensitive name of the property.
    /// 
    /// @return The zero based index. Negative value if not found.
    ASPOSE_WORDS_SHARED_API int32_t IndexOfKey(System::String name);
    /// Removes a property with the specified name from the collection.
    /// 
    /// @param name The case-sensitive name of the property.
    ASPOSE_WORDS_SHARED_API void Remove(System::String name);
    /// Removes a property at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all elements from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();

protected:

    CustomXmlPropertyCollection();

    void AddSafe(System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty> property);
    System::SharedPtr<Aspose::Words::Markup::CustomXmlPropertyCollection> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~CustomXmlPropertyCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::SortedStringListGeneric<System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty>>> mItems;

};

}
}
}
