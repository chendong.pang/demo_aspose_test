//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/CustomPartCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Markup/CustomPart.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents a collection of <see cref="Aspose::Words::Markup::CustomPart">CustomPart</see> objects.
/// 
/// You do not normally need to create instances of this class. You access custom parts
/// related to the OOXML package via the <see cref="Aspose::Words::Document::get_PackageCustomParts">PackageCustomParts</see> property.
/// 
/// @sa Aspose::Words::Markup::CustomPart
/// @sa Aspose::Words::Document::get_PackageCustomParts
class ASPOSE_WORDS_SHARED_CLASS CustomPartCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomPart>>
{
    typedef CustomPartCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomPart>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets or sets an item at the specified index.
    /// 
    /// @param index Zero-based index of the item.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomPart> idx_get(int32_t index);
    /// Gets or sets an item at the specified index.
    /// 
    /// @param index Zero-based index of the item.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::SharedPtr<Aspose::Words::Markup::CustomPart> value);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Markup::CustomPart>>> GetEnumerator() override;
    /// Adds an item to the collection.
    /// 
    /// @param part The item to add.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Markup::CustomPart> part);
    /// Removes an item at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all elements from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Makes a deep copy of this collection and its items.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomPartCollection> Clone();

    ASPOSE_WORDS_SHARED_API CustomPartCollection();

protected:

    virtual ASPOSE_WORDS_SHARED_API ~CustomPartCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Markup::CustomPart>>> mItems;

};

}
}
}
