//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/CustomXmlPartCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Markup/CustomXmlPart.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Doc { class CustomXmlPartFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents a collection of Custom XML Parts. The items are <see cref="Aspose::Words::Markup::CustomXmlPart">CustomXmlPart</see> objects.
/// 
/// You do not normally need to create instances of this class. You can access custom XML data
/// stored in a document via the <see cref="Aspose::Words::Document::get_CustomXmlParts">CustomXmlParts</see> property.
/// 
/// @sa Aspose::Words::Markup::CustomXmlPart
/// @sa Aspose::Words::Document::get_CustomXmlParts
class ASPOSE_WORDS_SHARED_CLASS CustomXmlPartCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomXmlPart>>
{
    typedef CustomXmlPartCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::CustomXmlPart>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Doc::CustomXmlPartFiler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets or sets an item at the specified index.
    /// 
    /// @param index Zero-based index of the item.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> idx_get(int32_t index);
    /// Gets or sets an item at the specified index.
    /// 
    /// @param index Zero-based index of the item.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> value);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Markup::CustomXmlPart>>> GetEnumerator() override;
    /// Adds an item to the collection.
    /// 
    /// @param part The custom XML part to add.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> part);
    /// Creates a new XML part with the specified XML and adds it to the collection.
    /// 
    /// @param id Identifier of a new custom XML part.
    /// @param xml XML data of the part.
    /// 
    /// @return Created custom XML part.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> Add(System::String id, System::String xml);
    /// Removes an item at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all elements from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Finds and returns a custom XML part by its identifier.
    /// 
    /// @param id Case-sensitive string that identifies the custom XML part.
    /// 
    /// @return Returns <c>null</c> if a custom XML part with the specified identifier is not found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> GetById(System::String id);
    /// Makes a deep copy of this collection and its items.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPartCollection> Clone();

    ASPOSE_WORDS_SHARED_API CustomXmlPartCollection();

protected:

    System::ArrayPtr<uint8_t> get_DataStoreRaw() const;
    void set_DataStoreRaw(System::ArrayPtr<uint8_t> value);

    virtual ASPOSE_WORDS_SHARED_API ~CustomXmlPartCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::ArrayPtr<uint8_t> mDataStoreRaw;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Markup::CustomXmlPart>>> mItems;

};

}
}
}
