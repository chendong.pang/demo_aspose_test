//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/CustomXmlProperty.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Markup { class CustomXmlPropertyCollection; } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents a single custom XML attribute or a smart tag property.
/// 
/// Used as an item of a <see cref="Aspose::Words::Markup::CustomXmlPropertyCollection">CustomXmlPropertyCollection</see> collection.
class ASPOSE_WORDS_SHARED_CLASS CustomXmlProperty : public System::Object
{
    typedef CustomXmlProperty ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Markup::CustomXmlPropertyCollection;

public:

    /// Specifies the name of the custom XML attribute or smart tag property.
    /// 
    /// Cannot be null.
    /// 
    /// Default is empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Gets or sets the namespace URI of the custom XML attribute or smart tag property.
    /// 
    /// Cannot be null.
    /// 
    /// Default is empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Uri() const;
    /// Setter for Aspose::Words::Markup::CustomXmlProperty::get_Uri
    ASPOSE_WORDS_SHARED_API void set_Uri(System::String value);
    /// Gets or sets the value of the custom XML attribute or smart tag property.
    /// 
    /// Cannot be null.
    /// 
    /// Default is empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Value() const;
    /// Setter for Aspose::Words::Markup::CustomXmlProperty::get_Value
    ASPOSE_WORDS_SHARED_API void set_Value(System::String value);

    /// Initializes a new instance of this class.
    /// 
    /// @param name The name of the property. Cannot be null.
    /// @param uri The namespace URI of the property. Cannot be null.
    /// @param value The value of the property. Cannot be null.
    ASPOSE_WORDS_SHARED_API CustomXmlProperty(System::String name, System::String uri, System::String value);

protected:

    System::SharedPtr<Aspose::Words::Markup::CustomXmlProperty> Clone();

private:

    System::String mName;
    System::String mUri;
    System::String mValue;

};

}
}
}
