//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/Sdt/SdtListItemCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Markup/Sdt/SdtListItem.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutSpanSdtControl; } } } }
namespace Aspose { namespace Words { namespace Markup { class SdtDropDownListBase; } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSdtReader; } } } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Provides access to <see cref="Aspose::Words::Markup::SdtListItem">SdtListItem</see> elements of a structured document tag.
class ASPOSE_WORDS_SHARED_CLASS SdtListItemCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::SdtListItem>>
{
    typedef SdtListItemCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Markup::SdtListItem>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::PreAps::LayoutSpanSdtControl;
    friend class Aspose::Words::Markup::SdtDropDownListBase;
    friend class Aspose::Words::Markup::XmlMapping;
    friend class Aspose::Words::RW::Docx::Writer::DocxSdtWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxSdtReader;

public:

    /// Specifies currently selected value in this list.
    /// Null value allowed, meaning that no currently selected entry is associated with this list item collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::SdtListItem> get_SelectedValue();
    /// Specifies currently selected value in this list.
    /// Null value allowed, meaning that no currently selected entry is associated with this list item collection.
    ASPOSE_WORDS_SHARED_API void set_SelectedValue(System::SharedPtr<Aspose::Words::Markup::SdtListItem> value);
    /// Gets number of items in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Markup::SdtListItem>>> GetEnumerator() override;
    /// Adds an item to this collection.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Markup::SdtListItem> item);
    /// Removes a list item at the specified index.
    /// 
    /// @param index The zero-based index of the item to remove.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Clears all items from this collection.
    ASPOSE_WORDS_SHARED_API void Clear();

    /// Returns a <see cref="Aspose::Words::Markup::SdtListItem">SdtListItem</see> object given its zero-based index in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::SdtListItem> idx_get(int32_t index);

protected:

    System::String get_LastItemValue() const;
    void set_LastItemValue(System::String value);
    int32_t get_SelectedIndex() const;

    SdtListItemCollection();

    System::SharedPtr<Aspose::Words::Markup::SdtListItemCollection> Clone();
    int32_t IndexOfItemValue(System::String value);

    virtual ASPOSE_WORDS_SHARED_API ~SdtListItemCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t mLastValueIdx;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Markup::SdtListItem>>> mChildren;
    System::String mLastItemValue;

};

}
}
}
