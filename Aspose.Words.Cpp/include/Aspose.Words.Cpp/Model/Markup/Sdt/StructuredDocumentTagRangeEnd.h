//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/Sdt/StructuredDocumentTagRangeEnd.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace RW { class MarkupResolver; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents an end of <b>ranged</b> structured document tag which accepts multi-sections content.
/// See also <see cref="Aspose::Words::Markup::StructuredDocumentTagRangeStart">StructuredDocumentTagRangeStart</see> node.
class ASPOSE_WORDS_SHARED_CLASS StructuredDocumentTagRangeEnd : public Aspose::Words::Node
{
    typedef StructuredDocumentTagRangeEnd ThisType;
    typedef Aspose::Words::Node BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::RW::MarkupResolver;
    friend class Aspose::Words::NodeImporter;

public:

    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Specifies a unique read-only persistent numerical Id for this <b>StructuredDocumentTagRange</b> node.
    /// Corresponding <see cref="Aspose::Words::Markup::StructuredDocumentTagRangeStart">StructuredDocumentTagRangeStart</see> node has the same <see cref="Aspose::Words::Markup::StructuredDocumentTagRangeStart::get_Id">Id</see>.
    ASPOSE_WORDS_SHARED_API int32_t get_Id() const;

    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    StructuredDocumentTagRangeEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc, int32_t id);

    void SetIdInternal(int32_t id);

    virtual ASPOSE_WORDS_SHARED_API ~StructuredDocumentTagRangeEnd();

private:

    int32_t pr_Id;

    ASPOSE_WORDS_SHARED_API void set_Id(int32_t value);

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
