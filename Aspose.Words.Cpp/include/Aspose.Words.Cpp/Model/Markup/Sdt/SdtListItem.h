//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/Sdt/SdtListItem.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Markup { class SdtListItemCollection; } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// This element specifies a single list item within a parent <see cref="Aspose::Words::Markup::SdtType::ComboBox">ComboBox</see> or <see cref="Aspose::Words::Markup::SdtType::DropDownList">DropDownList</see> structured document tag.
class ASPOSE_WORDS_SHARED_CLASS SdtListItem : public System::Object
{
    typedef SdtListItem ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Markup::SdtListItemCollection;

public:

    /// Gets the text to display in the run content in place of the <see cref="Aspose::Words::Markup::SdtListItem::get_Value">Value</see> attribute contents for this list item.
    /// 
    /// Cannot be null and cannot be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_DisplayText() const;
    /// Gets the value of this list item.
    /// 
    /// Cannot be null and cannot be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Value() const;

    /// Initializes a new instance of this class.
    ASPOSE_WORDS_SHARED_API SdtListItem(System::String displayText, System::String value);
    /// Initializes a new instance of this class.
    ASPOSE_WORDS_SHARED_API SdtListItem(System::String value);

protected:

    System::SharedPtr<Aspose::Words::Markup::SdtListItem> Clone();

private:

    System::String mValue;
    System::String mDisplayText;

};

}
}
}
