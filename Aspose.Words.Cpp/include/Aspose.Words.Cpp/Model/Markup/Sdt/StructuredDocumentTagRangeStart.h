//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/Sdt/StructuredDocumentTagRangeStart.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/Navigation/NodeFinder.h"
#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Markup/Sdt/SdtType.h"
#include "Aspose.Words.Cpp/Model/Markup/MarkupLevel.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { class MarkupResolver; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTagRangeEnd; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Represents a start of <b>ranged</b> structured document tag which accepts multi-sections content.
/// See also <see cref="Aspose::Words::Markup::StructuredDocumentTagRangeEnd">StructuredDocumentTagRangeEnd</see>.
class ASPOSE_WORDS_SHARED_CLASS StructuredDocumentTagRangeStart : public Aspose::Words::Node
{
    typedef StructuredDocumentTagRangeStart ThisType;
    typedef Aspose::Words::Node BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::MarkupResolver;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;

public:
    using Aspose::Words::Node::Clone;

private:

    class RangeNodeFinder : public Aspose::Words::NodeFinder
    {
        typedef RangeNodeFinder ThisType;
        typedef Aspose::Words::NodeFinder BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTagRangeEnd> FindRangeEnd(System::SharedPtr<Aspose::Words::NodeRange> range, int32_t id);

    protected:

        bool OnNodeFinding() override;

    private:

        static System::ArrayPtr<Aspose::Words::NodeType>& gNodeTypes();

        int32_t mId;

        RangeNodeFinder(System::SharedPtr<Aspose::Words::NodeRange> range, int32_t id);

    };

public:

    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Gets the level at which this <b>SDT range start</b> occurs in the document tree.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Markup::MarkupLevel get_Level() const;
    /// Gets type of this <b>Structured document tag</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Markup::SdtType get_SdtType();
    /// Specifies a unique read-only persistent numerical Id for this <b>SDT</b>.
    /// 
    /// Id attribute shall follow these rules:
    /// - The document shall retain SDT ids only if the whole document is cloned <see cref="Aspose::Words::Document::Clone">Clone</see>.
    /// - During <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool)">ImportNode()</see>
    /// Id shall be retained if import does not cause conflicts with other SDT Ids in
    /// the target document.
    /// - If multiple SDT nodes specify the same decimal number value for the Id attribute,
    /// then the first SDT in the document shall maintain this original Id,
    /// and all subsequent SDT nodes shall have new identifiers assigned to them when the document is loaded.
    /// - During standalone SDT <see cref="Aspose::Words::Markup::StructuredDocumentTag::Clone(bool, System::SharedPtr<Aspose::Words::INodeCloningListener>)">Clone()</see> operation new unique ID will be generated for the cloned SDT node.
    /// - If Id is not specified in the source document, then the SDT node shall have a new unique identifier assigned
    /// to it when the document is loaded.
    ASPOSE_WORDS_SHARED_API int32_t get_Id();
    /// When set to true, this property will prohibit a user from deleting this <b>SDT</b>.
    ASPOSE_WORDS_SHARED_API bool get_LockContentControl();
    /// When set to true, this property will prohibit a user from editing the contents of this <b>SDT</b>.
    ASPOSE_WORDS_SHARED_API bool get_LockContents();
    /// Specifies whether the content of this <b>SDT</b> shall be interpreted to contain placeholder text
    /// (as opposed to regular text contents within the SDT).
    /// if set to true, this state shall be resumed (showing placeholder text) upon opening this document.
    ASPOSE_WORDS_SHARED_API bool get_IsShowingPlaceholderText();
    /// Specifies a tag associated with the current SDT node.
    /// Can not be null.
    ASPOSE_WORDS_SHARED_API System::String get_Tag();
    /// Specifies the friendly name associated with this <b>SDT</b>.
    /// Can not be null.
    ASPOSE_WORDS_SHARED_API System::String get_Title() const;
    /// Gets an object that represents the mapping of this structured document tag range to XML data
    /// in a custom XML part of the current document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::XmlMapping> get_XmlMapping();
    /// Specifies end of range if the StructuredDocumentTag is a ranged structured document tag.
    /// Otherwise returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTagRangeEnd> get_RangeEnd();

    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> get_InternalSdt() const;
    void set_InternalSdt(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> value);

    StructuredDocumentTagRangeStart(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    void SetId(int32_t id);

    virtual ASPOSE_WORDS_SHARED_API ~StructuredDocumentTagRangeStart();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> mInternalSdt;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
