//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Markup/Sdt/XmlMapping.h
#pragma once

#include <xml/xml_node.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ilist.h>
#include <system/collections/idictionary.h>
#include <system/array.h>
#include <mutex>
#include <memory>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Validation { class CustomXmlValidator; } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentUpdater; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtRepeatingSection; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSdtWriter; } } } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSdtReader; } } } } }
namespace Aspose { namespace Words { namespace Markup { class CustomXmlPart; } } }
namespace Aspose { namespace Collections { class StringToStringDictionary; } }
namespace Aspose { namespace Words { namespace Markup { class XmlMappingCache; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace Properties { class BuiltInDocumentProperties; } } }

namespace Aspose {

namespace Words {

namespace Markup {

/// Specifies the information that is used to establish a mapping between the parent
/// structured document tag and an XML element stored within a custom XML data part in the document.
class ASPOSE_WORDS_SHARED_CLASS XmlMapping : public System::Object
{
    typedef XmlMapping ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Validation::CustomXmlValidator;
    friend class Aspose::Words::Replacing::FindReplace;
    friend class Aspose::Words::Markup::SdtContentUpdater;
    friend class Aspose::Words::Markup::SdtRepeatingSection;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::Docx::Writer::DocxSdtWriter;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::RW::Docx::Reader::DocxSdtReader;

public:

    /// Returns XML namespace prefix mappings to evaluate the <see cref="Aspose::Words::Markup::XmlMapping::get_XPath">XPath</see>.
    ASPOSE_WORDS_SHARED_API System::String get_PrefixMappings() const;
    /// Returns the XPath expression, which is evaluated to find the custom XML node
    /// that is mapped to the parent structured document tag.
    ASPOSE_WORDS_SHARED_API System::String get_XPath() const;
    /// Returns the custom XML data part to which the parent structured document tag is mapped.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> get_CustomXmlPart();
    /// Returns <b>true</b> if the parent structured document tag is successfully mapped to XML data.
    ASPOSE_WORDS_SHARED_API bool get_IsMapped();
    /// Specifies the custom XML data identifier for the custom XML data part which
    /// shall be used to evaluate the <see cref="Aspose::Words::Markup::XmlMapping::get_XPath">XPath</see> expression.
    ASPOSE_WORDS_SHARED_API System::String get_StoreItemId();

    /// Sets a mapping between the parent structured document tag and an XML node of a custom XML data part.
    /// 
    /// @param customXmlPart A custom XML data part to map to.
    /// @param xPath An XPath expression to find the XML node.
    /// @param prefixMapping XML namespace prefix mappings to evaluate the XPath.
    /// 
    /// @return A flag indicating whether the parent structured document tag is successfully mapped to
    ///     the XML node.
    ASPOSE_WORDS_SHARED_API bool SetMapping(System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> customXmlPart, System::String xPath, System::String prefixMapping);
    /// Deletes mapping of the parent structured document to XML data.
    ASPOSE_WORDS_SHARED_API void Delete();

protected:

    int32_t get_MappedNodesCount();
    ASPOSE_WORDS_SHARED_API void set_StoreItemId(System::String value);
    bool get_IsDocx15Extension();
    bool get_IsEmpty();

    XmlMapping(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt);

    System::SharedPtr<Aspose::Words::Markup::XmlMapping> Clone(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> parentSdt);
    void ResolveCustomXmlPart();
    void SetMapping(System::String storeItemId, System::String xPath, System::String prefixMapping);
    bool IsValid();
    void SetXPath(System::String value);
    System::String GetValue();
    void SetValue(System::String value);
    void SetValues(System::SharedPtr<System::Collections::Generic::IList<System::String>> values);
    bool UpdateContent();
    bool UpdateContent(System::SharedPtr<Aspose::Words::Markup::XmlMappingCache> cache);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsCoreProperties();
    bool get_IsExtendedProperties();

    System::String mXPath;
    System::String mPrefixMappings;
    System::String mStoreItemId;
    System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> mCustomXmlPart;
    System::WeakPtr<Aspose::Words::Markup::StructuredDocumentTag> mSdt;

    static const System::String& CorePropertiesStoreItemId();
    static const System::String& ExtendedPropertiesStoreItemId();
    static const System::String& PartnerControlsNamespace();
    static System::SharedPtr<Aspose::Collections::StringToStringDictionary>& gPathToPropertyName();
    static bool UpdateDataBoundContent(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt, System::SharedPtr<Aspose::Words::Markup::XmlMappingCache> cache);
    static bool UpdateTextboxSdt(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt, System::String newContent);
    static void InsertDocument(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt, System::SharedPtr<Aspose::Words::Document> doc);
    static bool NeedMoveCurrentSdtToBlockLevel(System::SharedPtr<Aspose::Words::Document> insertedDoc);
    static bool UpdatePicture(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt, System::String imageBytes);
    static void UpdatePictureSize(System::SharedPtr<Aspose::Words::Drawing::Shape> contentShape);
    static bool UpdateCheckbox(System::SharedPtr<Aspose::Words::Markup::StructuredDocumentTag> sdt, System::String value);
    static System::String GetOverallInnerText(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<System::Xml::XmlNode>>> xmlNodes);
    static System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<System::Xml::XmlNode>>> ExtractXmlNodes(System::ArrayPtr<uint8_t> xmlData, System::String storeItemId, System::String xPath, System::String prefixMappings, System::SharedPtr<Aspose::Words::Markup::XmlMappingCache> cache);
    static System::SharedPtr<System::Collections::Generic::IDictionary<System::String, System::String>> PrefixMappingsAsDictionary(System::String prefixMappings);
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<System::Xml::XmlNode>>> GetBoundXmlNodes(System::SharedPtr<Aspose::Words::Markup::XmlMappingCache> cache);
    System::String GetPropertyName();
    System::SharedPtr<Aspose::Words::Markup::CustomXmlPart> GetCustomXmlPartById(System::String partId);
    static bool IsPartnerControlsNamespace(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<System::Xml::XmlNode>>> xmlNodes);
    static bool HasChildElementNode(System::SharedPtr<System::Xml::XmlNode> xmlNode);
    static System::ArrayPtr<uint8_t> CreateExtendedPropertiesXml(System::SharedPtr<Aspose::Words::Properties::BuiltInDocumentProperties> props);
    static System::ArrayPtr<uint8_t> CreateCorePropertiesXml(System::SharedPtr<Aspose::Words::Properties::BuiltInDocumentProperties> props);

    static void __StaticConstructor__();

};

}
}
}
