//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/Node.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/constraints.h>
#include <system/collections/stack.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { class NodeRemover; } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceLegacy; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTagRangeStart; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTagRangeEnd; } } }
namespace Aspose { namespace Words { class AnnotationUtil; } }
namespace Aspose { namespace Words { class RangeBound; } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRangeRevisionHelper; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class FloaterZOrderSorter; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { class MoveRange; } }
namespace Aspose { namespace Words { class NodeCollection; } }
namespace Aspose { namespace Words { namespace Layout { class RevisionCommentsPreprocessor; } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerIndexer; } } }
namespace Aspose { namespace Words { namespace Comparison { class StoryComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class ComplexScriptRunUpdater; } } }
namespace Aspose { namespace Words { class CompositeNode; } }
namespace Aspose { namespace Words { namespace RW { namespace Chm { namespace Reader { class ChmReader; } } } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUnlinker; } } }
namespace Aspose { namespace Words { namespace Fields { class HiddenParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMathUtil; } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceIndexer; } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { class InlineMarkupResolver; } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownHyperlinkWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownTableWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class OfficeMathToShapeConverter; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntriesBookmarkDeferredRemover; } } }
namespace Aspose { namespace Words { class NodeEnumerator; } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlToDmlComplexShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBuilder; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace MailMerging { class RepalacedTagRestoreCollector; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkDeleter; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkMover; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionHandlingContext; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlDiagramReader; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlPictureRenderer; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldDisplayContext; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class WarningGenerator; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class StoryLayoutBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class OfficeMathToApsConverter; } } } }
namespace Aspose { namespace Words { namespace Layout { class RunWrapFinder; } } }
namespace Aspose { namespace Words { class RangeDocumentBuilder; } }
namespace Aspose { namespace Words { namespace Fields { class FieldFakeResultAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldOldResultNodeCollection; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentUpdater; } } }
namespace Aspose { namespace Words { class EditableRangeEnd; } }
namespace Aspose { namespace Words { class EditableRangeStart; } }
namespace Aspose { namespace Words { class MoveRangeEnd; } }
namespace Aspose { namespace Words { class MoveRangeFinder; } }
namespace Aspose { namespace Words { class MoveRangeStart; } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { namespace CommonBorder { class CommonBorderContainer; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTabStopExtractor; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLWriter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTrackedChangesWriter; } } } } }
namespace Aspose { namespace Words { class SubDocument; } }
namespace Aspose { namespace Words { namespace Fields { class FieldPageRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldXE; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { namespace MailMerging { class TagReplacer; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { class ShapeFieldRemover; } }
namespace Aspose { namespace Words { class NodeCopier; } }
namespace Aspose { namespace Words { class NodeTextCollector; } }
namespace Aspose { namespace Words { class DocumentPosition; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class NodeUtil; } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutSpanPicture; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { class BookmarkEnd; } }
namespace Aspose { namespace Words { class BookmarkStart; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class RectangularCellRange; } } } }
namespace Aspose { namespace Words { namespace Fields { class NodeRangeFieldCodeTokenizer; } } }
namespace Aspose { namespace Words { namespace Fields { class ResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { class CommentRangeEnd; } }
namespace Aspose { namespace Words { class CommentRangeStart; } }
namespace Aspose { namespace Words { namespace Validation { class AnnotationValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class CommentValidatorItem; } } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { namespace Validation { class BookmarkValidator; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class FieldValidator; } } }
namespace Aspose { namespace Words { namespace Saving { class ImageSavingArgs; } } }
namespace Aspose { namespace Words { class TableBuilder; } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssPositionStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCommonBorderSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlParaBorderWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageDocumentTreeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeResourceWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumUtil; } } }
namespace Aspose { namespace Words { class FootnoteSeparatorCollection; } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { namespace TableLayout { class ParagraphMeasurer; } } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlDocumentSplitter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextContentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtContentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFieldWriter; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssParagraph; } } } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlCommon { class HtmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFootnoteWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtBookmarkWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyListHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlDocPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { enum class NodeLevel; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Range; } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { namespace Saving { class SaveOptions; } } }

namespace Aspose {

namespace Words {

/// Base class for all nodes of a Word document.
/// 
/// A document is represented as a tree of nodes, similar to DOM or XmlDocument.
/// 
/// For more info see the Composite design pattern.
/// 
/// The <see cref="Aspose::Words::Node">Node</see> class:
/// 
/// - Defines the child node interface.
/// - Defines the interface for visiting nodes.
/// - Provides default cloning capability.
/// - Implements parent node and owner document mechanisms.
/// - Implements access to sibling nodes.
class ASPOSE_WORDS_SHARED_CLASS Node : public virtual System::Object
{
    typedef Node ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::NodeRemover;
    friend class Aspose::Words::Replacing::FindReplaceLegacy;
    friend class Aspose::Words::Markup::StructuredDocumentTagRangeStart;
    friend class Aspose::Words::Markup::StructuredDocumentTagRangeEnd;
    friend class Aspose::Words::AnnotationUtil;
    friend class Aspose::Words::RangeBound;
    friend class Aspose::Words::Revisions::MoveRangeRevisionHelper;
    friend class Aspose::Words::Layout::PreAps::FloaterZOrderSorter;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::MoveRange;
    friend class Aspose::Words::NodeCollection;
    friend class Aspose::Words::Layout::RevisionCommentsPreprocessor;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::ComparerIndexer;
    friend class Aspose::Words::Comparison::StoryComparer;
    friend class Aspose::Words::Validation::ComplexScriptRunUpdater;
    friend class Aspose::Words::CompositeNode;
    friend class Aspose::Words::RW::Chm::Reader::ChmReader;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Fields::FieldUnlinker;
    friend class Aspose::Words::Fields::HiddenParagraphTocEntry;
    friend class Aspose::Words::Math::OfficeMathUtil;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::Replacing::FindReplaceIndexer;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::InlineMarkupResolver;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownHyperlinkWriter;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownTableWriter;
    friend class Aspose::Words::Validation::OfficeMathToShapeConverter;
    friend class Aspose::Words::Fields::TocEntriesBookmarkDeferredRemover;
    friend class Aspose::Words::Fields::TocEntriesBookmarkDeferredRemover;
    friend class Aspose::Words::NodeEnumerator;
    friend class Aspose::Words::Validation::VmlToDml::VmlToDmlComplexShapeConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::Replacing::FindReplace;
    friend class Aspose::Words::MailMerging::MailMergeRegion;
    friend class Aspose::Words::Fields::FieldBuilder;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::MailMerging::RepalacedTagRestoreCollector;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Revisions::BookmarkDeleter;
    friend class Aspose::Words::Revisions::BookmarkMover;
    friend class Aspose::Words::Revisions::RevisionHandlingContext;
    friend class Aspose::Words::Revisions::RevisionHandlingContext;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::RW::Dml::Reader::DmlDiagramReader;
    friend class Aspose::Words::ApsBuilder::Dml::DmlPictureRenderer;
    friend class Aspose::Words::Fields::FieldDisplayContext;
    friend class Aspose::Words::Layout::Core::WarningGenerator;
    friend class Aspose::Words::Layout::Core::StoryLayoutBuilder;
    friend class Aspose::Words::ApsBuilder::Math::OfficeMathToApsConverter;
    friend class Aspose::Words::Layout::RunWrapFinder;
    friend class Aspose::Words::RangeDocumentBuilder;
    friend class Aspose::Words::Fields::FieldFakeResultAppender;
    friend class Aspose::Words::Fields::FieldOldResultNodeCollection;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::Markup::SdtContentUpdater;
    friend class Aspose::Words::EditableRangeEnd;
    friend class Aspose::Words::EditableRangeStart;
    friend class Aspose::Words::MoveRangeEnd;
    friend class Aspose::Words::MoveRangeFinder;
    friend class Aspose::Words::MoveRangeStart;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;
    friend class Aspose::Words::RW::Html::Reader::CommonBorder::CommonBorderContainer;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlTabStopExtractor;
    friend class Aspose::Words::RW::MathML::MathMLWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTrackedChangesWriter;
    friend class Aspose::Words::SubDocument;
    friend class Aspose::Words::Fields::FieldPageRef;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::FieldXE;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocEntryExtractor;
    friend class Aspose::Words::Fields::FieldUtil;
    friend class Aspose::Words::MailMerging::TagReplacer;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::Markup::XmlMapping;
    friend class Aspose::Words::ShapeFieldRemover;
    friend class Aspose::Words::NodeCopier;
    friend class Aspose::Words::NodeTextCollector;
    friend class Aspose::Words::DocumentPosition;
    friend class Aspose::Words::NodeRange;
    friend class Aspose::Words::NodeUtil;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Layout::PreAps::LayoutSpanPicture;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::BookmarkEnd;
    friend class Aspose::Words::BookmarkStart;
    friend class Aspose::Words::Fields::FieldUpdater;
    friend class Aspose::Words::Fields::Expressions::RectangularCellRange;
    friend class Aspose::Words::Fields::NodeRangeFieldCodeTokenizer;
    friend class Aspose::Words::Fields::ResultEnumerator;
    friend class Aspose::Words::Fields::FormField;
    friend class Aspose::Words::CommentRangeEnd;
    friend class Aspose::Words::CommentRangeStart;
    friend class Aspose::Words::Validation::AnnotationValidator;
    friend class Aspose::Words::Validation::CommentValidatorItem;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Validation::BookmarkValidator;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::FieldValidator;
    friend class Aspose::Words::Saving::ImageSavingArgs;
    friend class Aspose::Words::TableBuilder;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::Drawing::ShapeBase;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::RW::Html::Css::New::CssPositionStyleConverter;
    friend class Aspose::Words::RW::Html::Writer::HtmlCommonBorderSpanWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlParaBorderWriter;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageDocumentTreeWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeResourceWriter;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldNumUtil;
    friend class Aspose::Words::FootnoteSeparatorCollection;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::TableLayout::ParagraphMeasurer;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::Inline;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlDocumentSplitter;
    friend class Aspose::Words::RW::Odt::Writer::OdtShapeWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextContentReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtContentReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFieldWriter;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::CssParagraph;
    friend class Aspose::Words::RW::HtmlCommon::HtmlUtil;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFootnoteWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtBookmarkWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSpanWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyListHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlDocPrReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using System::Object::ToString;

public:

    /// Gets the type of this node.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const = 0;
    /// Gets the immediate parent of this node.
    /// 
    /// If a node has just been created and not yet added to the tree,
    /// or if it has been removed from the tree, the parent is null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CompositeNode> get_ParentNode();
    /// Gets the document to which this node belongs.
    /// 
    /// The node always belongs to a document even if it has just been created
    /// and not yet added to the tree, or if it has been removed from the tree.
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const;
    /// Gets the node immediately preceding this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_PreviousSibling();
    /// Gets the node immediately following this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_NextSibling();
    /// Returns true if this node can contain other nodes.
    virtual ASPOSE_WORDS_SHARED_API bool get_IsComposite();
    /// Returns a <b>Range</b> object that represents the portion of a document that is contained in this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Range> get_Range();

    /// Creates a duplicate of the node.
    /// 
    /// This method serves as a copy constructor for nodes.
    /// The cloned node has no parent, but belongs to the same document as the original node.
    /// 
    /// This method always performs a deep copy of the node. The <i>isCloneChildren</i> parameter
    /// specifies whether to perform copy all child nodes as well.
    /// 
    /// @param isCloneChildren True to recursively clone the subtree under the specified node;
    ///     false to clone only the node itself.
    /// 
    /// @return The cloned node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren);
    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    virtual ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) = 0;
    /// Gets the text of this node and of all its children.
    /// 
    /// The returned string includes all control and special characters as described in <see cref="Aspose::Words::ControlChar">ControlChar</see>.
    virtual ASPOSE_WORDS_SHARED_API System::String GetText();
    template <typename T>
    T GetAncestorOf()
    {
        typedef Aspose::Words::CompositeNode BaseT_CompositeNode;
assert_is_base_of(BaseT_CompositeNode, T);
System::SharedPtr<Aspose::Words::Node> curParent = System::StaticCast<Aspose::Words::Node>(this->get_ParentNode());
while (curParent != nullptr)
{
    auto parent = System::StaticCast_noexcept<typename T::Pointee_>(curParent);
    if (parent != nullptr)
    {
        return parent;
    }
    curParent = System::StaticCast<Aspose::Words::Node>(curParent->get_ParentNode());
}
return nullptr;
    }
    /// Gets the first ancestor of the specified <see cref="Aspose::Words::NodeType">NodeType</see>.
    /// 
    /// @param ancestorType The node type of the ancestor to retrieve.
    /// 
    /// @return The ancestor of the specified type or null if no ancestor of this type was found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CompositeNode> GetAncestor(Aspose::Words::NodeType ancestorType);
    /// Removes itself from the parent.
    ASPOSE_WORDS_SHARED_API void Remove();
    /// Gets next node according to the pre-order tree traversal algorithm.
    /// 
    /// @param rootNode The top node (limit) of traversal.
    /// 
    /// @return Next node in pre-order order. Null if reached the rootNode.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> NextPreOrder(System::SharedPtr<Aspose::Words::Node> rootNode);
    /// Gets the previous node according to the pre-order tree traversal algorithm.
    /// 
    /// @param rootNode The top node (limit) of traversal.
    /// 
    /// @return Previous node in pre-order order. Null if reached the rootNode.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> PreviousPreOrder(System::SharedPtr<Aspose::Words::Node> rootNode);
    /// Exports the content of the node into a string in the specified format.
    /// 
    /// @return The content of the node in the specified format.
    ASPOSE_WORDS_SHARED_API System::String ToString(Aspose::Words::SaveFormat saveFormat);
    /// Exports the content of the node into a string using the specified save options.
    /// 
    /// @param saveOptions Specifies the options that control how the node is saved.
    /// 
    /// @return The content of the node in the specified format.
    ASPOSE_WORDS_SHARED_API System::String ToString(System::SharedPtr<Aspose::Words::Saving::SaveOptions> saveOptions);
    /// A utility method that converts a node type enum value into a user friendly string.
    static ASPOSE_WORDS_SHARED_API System::String NodeTypeToString(Aspose::Words::NodeType nodeType);

protected:

    System::SharedPtr<Aspose::Words::CompositeNode> get_FirstNonMarkupParentNode();
    System::SharedPtr<Aspose::Words::CompositeNode> get_FirstMeaningfulParentNode();
    System::SharedPtr<Aspose::Words::Node> get_PreviousNonAnnotationSibling();
    System::SharedPtr<Aspose::Words::Node> get_PreviousNonMarkupNodeLimited();
    System::SharedPtr<Aspose::Words::Node> get_NextNonMarkupNodeLimited();
    System::SharedPtr<Aspose::Words::CompositeNode> get_PreviousNonMarkupCompositeLimited();
    System::SharedPtr<Aspose::Words::CompositeNode> get_NextNonMarkupCompositeLimited();
    System::SharedPtr<Aspose::Words::Node> get_NextNonAnnotationSibling();
    bool get_IsLastChild();
    bool get_IsFirstChild();
    bool get_IsFirstNonZeroLengthChild();
    bool get_IsLastNonZeroLengthChild();
    System::SharedPtr<Aspose::Words::Node> get_NextNode() const;
    void set_NextNode(System::SharedPtr<Aspose::Words::Node> value);
    System::SharedPtr<Aspose::Words::Node> get_PrevNode() const;
    void set_PrevNode(System::SharedPtr<Aspose::Words::Node> value);
    System::SharedPtr<Aspose::Words::Node> get_NextOrParent();
    Aspose::Words::NodeLevel get_NodeLevel();
    bool get_IsRemoved();

    ASPOSE_WORDS_SHARED_API Node();
    ASPOSE_WORDS_SHARED_API Node(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    System::SharedPtr<Aspose::Words::Document> FetchDocument();
    System::SharedPtr<Aspose::Words::Document> FetchDocumentOrGlossaryMain();
    int32_t GetStart();
    int32_t GetEnd();
    bool Contains(int32_t position);
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetNodeFromPos(int32_t position);
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener);
    static ASPOSE_WORDS_SHARED_API bool VisitorActionToBool(Aspose::Words::VisitorAction action);
    virtual ASPOSE_WORDS_SHARED_API void GetTextToBuilder(System::SharedPtr<System::Text::StringBuilder> builder);
    void SetParent(System::SharedPtr<Aspose::Words::Node> parentNode);
    void SetDocument(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    bool IsAncestorNode(System::SharedPtr<Aspose::Words::Node> node);
    System::SharedPtr<Aspose::Words::Node> GetTopmostAncestor();
    virtual ASPOSE_WORDS_SHARED_API int32_t GetTextLength();
    System::SharedPtr<Aspose::Words::Node> NextSiblingOfType(Aspose::Words::NodeType nodeType);
    System::SharedPtr<Aspose::Words::Node> PreviousSiblingOfType(Aspose::Words::NodeType nodeType);
    System::SharedPtr<Aspose::Words::Node> PreviousPreOrderOfType(System::SharedPtr<Aspose::Words::Node> rootNode, Aspose::Words::NodeType nodeType);
    System::SharedPtr<Aspose::Words::Node> GetNearestSibling(bool isNext);
    System::SharedPtr<Aspose::Words::Node> GetStoryAncestor(Aspose::Words::NodeType nodeType);
    System::SharedPtr<Aspose::Words::DocumentPosition> GetBeforeDocumentPosition();
    System::SharedPtr<Aspose::Words::DocumentPosition> GetAfterDocumentPosition();
    bool IsAbove(System::SharedPtr<Aspose::Words::Node> node);
    static System::SharedPtr<Aspose::Words::Node> GetCommonAncestor(System::SharedPtr<Aspose::Words::Node> nodeA, System::SharedPtr<Aspose::Words::Node> nodeB);
    static bool NodeAIsAboveNodeB(System::SharedPtr<Aspose::Words::Node> a, System::SharedPtr<Aspose::Words::Node> b);
    System::String GetNodeId() const;

    virtual ASPOSE_WORDS_SHARED_API ~Node();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Node> mParentNode;
    System::SharedPtr<Aspose::Words::Node> mNextNode;
    System::WeakPtr<Aspose::Words::Node> mPrevNode;

    static System::SharedPtr<System::Collections::Generic::Stack<System::SharedPtr<Aspose::Words::Node>>> GetAncestors(System::SharedPtr<Aspose::Words::Node> node);
    static System::SharedPtr<Aspose::Words::Node> FindTheLastMatchingObject(System::SharedPtr<System::Collections::Generic::Stack<System::SharedPtr<Aspose::Words::Node>>> a, System::SharedPtr<System::Collections::Generic::Stack<System::SharedPtr<Aspose::Words::Node>>> b);
    virtual ThisType* CppMemberwiseClone() const = 0;

};

}
}
