//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/NodeChangingArgs.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Nodes/NodeChangingAction.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }

namespace Aspose {

namespace Words {

/// Provides data for methods of the <see cref="Aspose::Words::INodeChangingCallback">INodeChangingCallback</see> interface.
/// 
/// @sa Aspose::Words::DocumentBase
/// @sa Aspose::Words::INodeChangingCallback
/// @sa Aspose::Words::NodeChangingAction
class ASPOSE_WORDS_SHARED_CLASS NodeChangingArgs : public System::Object
{
    typedef NodeChangingArgs ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::DocumentBase;

public:

    /// Gets the <see cref="Aspose::Words::NodeChangingArgs::get_Node">Node</see> that is being added or removed.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_Node() const;
    /// Gets the node's parent before the operation began.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_OldParent() const;
    /// Gets the node's parent that will be set after the operation completes.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_NewParent() const;
    /// Gets a value indicating what type of node change event is occurring.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeChangingAction get_Action() const;

protected:

    NodeChangingArgs(System::SharedPtr<Aspose::Words::Node> node, System::SharedPtr<Aspose::Words::Node> oldParent, System::SharedPtr<Aspose::Words::Node> newParent, Aspose::Words::NodeChangingAction action);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Node> mNode;
    System::SharedPtr<Aspose::Words::Node> mOldParent;
    System::SharedPtr<Aspose::Words::Node> mNewParent;
    Aspose::Words::NodeChangingAction mAction;

};

}
}
