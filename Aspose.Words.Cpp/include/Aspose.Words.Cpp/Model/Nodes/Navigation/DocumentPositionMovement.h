//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/Navigation/DocumentPositionMovement.h
#pragma once

namespace Aspose {

namespace Words {

/// DocumentPositionMovement enumeration.
enum class DocumentPositionMovement
{

    None,

    Inside,

    StartEnd,

    Sibling,

    Above,

    Below
};

}
}
