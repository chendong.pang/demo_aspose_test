//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/Navigation/NodeEnumerator.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>

#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/Navigation/IDocumentPositionListener.h"

namespace Aspose { namespace Words { class NodeTraverser; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class DocumentPosition; } }
namespace Aspose { namespace Words { class INodeModifier; } }
namespace Aspose { namespace Words { enum class NodeExtractBehavior; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { enum class DocumentPositionMovement; } }

namespace Aspose {

namespace Words {

/// \cond
class NodeEnumerator : public System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Node>>, public Aspose::Words::IDocumentPositionListener
{
    typedef NodeEnumerator ThisType;
    typedef System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Node>> BaseType;
    typedef Aspose::Words::IDocumentPositionListener BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::NodeTraverser;

public:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_Current() const;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> get_Range() const;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_CurrentNode() const;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentPosition> get_CurrentPosition() const;

    ASPOSE_WORDS_SHARED_API NodeEnumerator(System::SharedPtr<Aspose::Words::NodeRange> range);

    ASPOSE_WORDS_SHARED_API void Dispose();
    ASPOSE_WORDS_SHARED_API bool MoveToNextNode();
    ASPOSE_WORDS_SHARED_API bool MoveToNextNode(bool isDeep);
    ASPOSE_WORDS_SHARED_API bool MoveToNextNode(bool isDeep, bool isByNode);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> ExtractCurrentNode(System::SharedPtr<Aspose::Words::INodeModifier> modifier, Aspose::Words::NodeExtractBehavior behavior);
    ASPOSE_WORDS_SHARED_API bool MoveNext();
    ASPOSE_WORDS_SHARED_API void Reset();
    ASPOSE_WORDS_SHARED_API void NotifyMoved(Aspose::Words::DocumentPositionMovement movement);

protected:

    ASPOSE_WORDS_SHARED_API bool get_IsStart();

    ASPOSE_WORDS_SHARED_API bool get_IsEnd();

    ASPOSE_WORDS_SHARED_API bool get_IsStartNode();

    ASPOSE_WORDS_SHARED_API bool get_IsEndNode();

    ASPOSE_WORDS_SHARED_API NodeEnumerator(System::SharedPtr<Aspose::Words::NodeRange> range, bool stopAtInvalidRangeNodes);

    ASPOSE_WORDS_SHARED_API bool IsCurrentNodeValid();

    ASPOSE_WORDS_SHARED_API bool IsRangeNodeValid(System::SharedPtr<Aspose::Words::Node> rangeNode);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> ExtractRangeNode(System::SharedPtr<Aspose::Words::Node> rangeNode, System::SharedPtr<Aspose::Words::INodeModifier> modifier, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener, bool cloneNode, bool modifyChildren);

    virtual ASPOSE_WORDS_SHARED_API bool NeedSkipRangeNodeExtract(System::SharedPtr<Aspose::Words::Node> rangeNode);
    virtual ASPOSE_WORDS_SHARED_API void OnMoved(Aspose::Words::DocumentPositionMovement movement);
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetValidParent(System::SharedPtr<Aspose::Words::Node> node);
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetValidChild(System::SharedPtr<Aspose::Words::Node> node);

    virtual ASPOSE_WORDS_SHARED_API ~NodeEnumerator();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::NodeRange> mRange;
    bool mStopAtInvalidRangeNodes;
    System::SharedPtr<Aspose::Words::DocumentPosition> mCurrentPosition;

    bool IsEof(bool isByNode);

    bool CanStopAtCurrentNode();

    bool IsRangeNodePartiallyValid(System::SharedPtr<Aspose::Words::Node> rangeNode);

    System::SharedPtr<Aspose::Words::Node> ExtractRangeNode(System::SharedPtr<Aspose::Words::Node> rangeNode, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener, bool cloneNode, bool cloneChildren, bool checkValidity);

    bool NeedCloneNodeOnExtract(Aspose::Words::NodeExtractBehavior behavior);

    static bool NeedModifyChildrenOnExtract(Aspose::Words::NodeExtractBehavior behavior);

};/// \endcond

}
}
