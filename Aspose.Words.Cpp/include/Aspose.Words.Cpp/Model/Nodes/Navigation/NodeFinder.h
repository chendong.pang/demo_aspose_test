//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/Navigation/NodeFinder.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ilist.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Nodes/Navigation/NodeEnumerator.h"

namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTagRangeStart; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntriesBookmarkDeferredRemover; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdaterBookmarkFinder; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { enum class NodeType; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }
namespace Aspose { namespace Words { class NodeRange; } }

namespace Aspose {

namespace Words {

/// \cond
class NodeFinder : public Aspose::Words::NodeEnumerator
{
    typedef NodeFinder ThisType;
    typedef Aspose::Words::NodeEnumerator BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Markup::StructuredDocumentTagRangeStart;
    friend class Aspose::Words::Fields::TocEntriesBookmarkDeferredRemover;
    friend class Aspose::Words::Fields::FieldUpdaterBookmarkFinder;

public:

    static System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Node>>> FindNodes(System::SharedPtr<Aspose::Words::NodeRange> range, const System::ArrayPtr<Aspose::Words::NodeType>& nodeTypes);
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Node>>> Find();

protected:

    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Node>>> get_Nodes() const;

    NodeFinder(System::SharedPtr<Aspose::Words::NodeRange> range, System::ArrayPtr<Aspose::Words::NodeType> nodeTypes);

    virtual bool OnNodeFinding();
    System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<Aspose::Words::NodeType>> mNodeTypes;
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Node>>> mNodes;

};/// \endcond

}
}
