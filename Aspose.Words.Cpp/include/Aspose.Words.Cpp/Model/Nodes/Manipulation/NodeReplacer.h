//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/Manipulation/NodeReplacer.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Nodes/Manipulation/INodeCopierListener.h"

namespace Aspose { namespace Words { class CompositeNodeReplacer; } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { enum class NodeType; } }
namespace Aspose { namespace Words { class NodeRange; } }

namespace Aspose {

namespace Words {

/// \cond
class NodeReplacer : public Aspose::Words::INodeCopierListener
{
    typedef NodeReplacer ThisType;
    typedef Aspose::Words::INodeCopierListener BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::CompositeNodeReplacer;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::FieldRefUtil;

public:

    void NotifyNodeCloned(System::SharedPtr<Aspose::Words::Node> source, System::SharedPtr<Aspose::Words::Node> clone) override;
    void NotifyNodeRangeCopied(System::SharedPtr<Aspose::Words::NodeRange> sourceRange, System::SharedPtr<Aspose::Words::NodeRange> insertedRange) override;

protected:

    NodeReplacer(const System::ArrayPtr<Aspose::Words::NodeType>& nodeTypes);

    virtual void ReplaceCollectedNode(System::SharedPtr<Aspose::Words::Node> node) = 0;
    virtual void CollectClone(System::SharedPtr<Aspose::Words::Node> source, System::SharedPtr<Aspose::Words::Node> clone);
    virtual void FinalizeReplace();
    System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_HasClones();

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> mClones;
    System::ArrayPtr<Aspose::Words::NodeType> mNodeTypes;

};/// \endcond

}
}
