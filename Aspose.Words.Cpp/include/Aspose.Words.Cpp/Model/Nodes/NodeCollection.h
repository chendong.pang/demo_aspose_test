//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/NodeCollection.h
#pragma once

#include <system/enumerator_adapter.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator_ng.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Nodes/NodeCollectionEnumerator.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/INodeCollection.h"

namespace Aspose { namespace Words { class ImportInfoCollector; } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRangeRevisionHelper; } } }
namespace Aspose { namespace Words { class CommentCollection; } }
namespace Aspose { namespace Words { class CompositeNode; } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeCollection; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkDeleter; } } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class BuildingBlockCollection; } } }
namespace Aspose { namespace Words { namespace Rendering { class ThumbnailGenerator; } } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { namespace Fields { class FormFieldCollection; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class HeaderFooterCollection; } }
namespace Aspose { namespace Words { class SectionCollection; } }
namespace Aspose { namespace Words { namespace Tables { class CellCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class RowCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class TableCollection; } } }
namespace Aspose { namespace Words { class Comment; } }
namespace Aspose { namespace Words { class ParagraphCollection; } }
namespace Aspose { namespace Words { class RunCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class NodeMatcher; } }
namespace Aspose { namespace Words { enum class NodeType; } }
namespace Aspose { namespace Words { template<typename> class NodeCollectionEnumerator; } }

namespace Aspose {

namespace Words {

/// Represents a collection of nodes of a specific type.
/// 
/// <b>NodeCollection</b> does not own the nodes it contains, rather, is just a selection of nodes
/// of the specified type, but the nodes are stored in the tree under their respective parent nodes.
/// 
/// <b>NodeCollection</b> supports indexed access, iteration and provides add and remove methods.
/// 
/// The <b>NodeCollection</b> collection is "live", i.e. changes to the children of the node object
/// that it was created from are immediately reflected in the nodes returned by the <b>NodeCollection</b>
/// properties and methods.
/// 
/// <b>NodeCollection</b> is returned by <see cref="Aspose::Words::CompositeNode::GetChildNodes(Aspose::Words::NodeType, bool)">GetChildNodes()</see>
/// and also serves as a base class for typed node collections such as <see cref="Aspose::Words::SectionCollection">SectionCollection</see>,
/// <see cref="Aspose::Words::ParagraphCollection">ParagraphCollection</see> etc.
/// 
/// <b>NodeCollection</b> can be "flat" and contain only immediate children of the node it was created
/// from, or it can be "deep" and contain all descendant children.
class ASPOSE_WORDS_SHARED_CLASS NodeCollection : public Aspose::Words::INodeCollection, public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>>
{
    typedef NodeCollection ThisType;
    typedef Aspose::Words::INodeCollection BaseType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>> BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ImportInfoCollector;
    friend class Aspose::Words::Revisions::MoveRangeRevisionHelper;
    friend class Aspose::Words::CommentCollection;
    friend class Aspose::Words::CompositeNode;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::Drawing::ShapeCollection;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Revisions::BookmarkDeleter;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::BuildingBlocks::BuildingBlockCollection;
    friend class Aspose::Words::Rendering::ThumbnailGenerator;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::Fields::FormFieldCollection;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::HeaderFooterCollection;
    friend class Aspose::Words::SectionCollection;
    friend class Aspose::Words::Tables::CellCollection;
    friend class Aspose::Words::Tables::RowCollection;
    friend class Aspose::Words::Tables::TableCollection;
    friend class Aspose::Words::Comment;
    friend class Aspose::Words::ParagraphCollection;
    friend class Aspose::Words::RunCollection;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;

public:

    /// Gets the number of nodes in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CompositeNode> get_Container() override;

    /// Retrieves a node at the given index.
    /// 
    /// The index is zero-based.
    /// 
    /// Negative indexes are allowed and indicate access from the back of the collection.
    /// For example -1 means the last item, -2 means the second before last and so on.
    /// 
    /// If index is greater than or equal to the number of items in the list, this returns a null reference.
    /// 
    /// If index is negative and its absolute value is greater than the number of items in the list, this returns a null reference.
    /// 
    /// @param index An index into the collection of nodes.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> idx_get(int32_t index);

    /// Adds a node to the end of the collection.
    /// 
    /// The node is inserted as a child into the node object from which the collection was created.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param node The node to be added to the end of the collection.
    /// 
    /// @exception System::NotSupportedException The <b>NodeCollection</b> is a "deep" collection.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Node> node);
    /// Inserts a node into the collection at the specified index.
    /// 
    /// The node is inserted as a child into the node object from which the collection was created.
    /// 
    /// If the index is equal to or greater than Count, the node is added at the end of the collection.
    /// 
    /// If the index is negative and its absolute value is greater than Count, the node is added at the end of the collection.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param index The zero-based index of the node.
    ///     Negative indexes are allowed and indicate access from the back of the list.
    ///     For example -1 means the last node, -2 means the second before last and so on.
    /// @param node The node to insert.
    /// 
    /// @exception System::NotSupportedException The <b>NodeCollection</b> is a "deep" collection.
    ASPOSE_WORDS_SHARED_API void Insert(int32_t index, System::SharedPtr<Aspose::Words::Node> node);
    /// Removes the node from the collection and from the document.
    /// 
    /// @param node The node to remove.
    ASPOSE_WORDS_SHARED_API void Remove(System::SharedPtr<Aspose::Words::Node> node);
    /// Removes the node at the specified index from the collection and from the document.
    /// 
    /// @param index The zero-based index of the node.
    ///     Negative indexes are allowed and indicate access from the back of the list.
    ///     For example -1 means the last node, -2 means the second before last and so on.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all nodes from this collection and from the document.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Determines whether a node is in the collection.
    /// 
    /// This method performs a linear search; therefore, the average execution time is proportional to Count.
    /// 
    /// @param node The node to locate.
    /// 
    /// @return True if item is found in the collection; otherwise, false.
    ASPOSE_WORDS_SHARED_API bool Contains(System::SharedPtr<Aspose::Words::Node> node);
    /// Returns the zero-based index of the specified node.
    /// 
    /// This method performs a linear search; therefore, the average execution time is proportional to Count.
    /// 
    /// @param node The node to locate.
    /// 
    /// @return The zero-based index of the node within the collection, if found; otherwise, -1.
    ASPOSE_WORDS_SHARED_API int32_t IndexOf(System::SharedPtr<Aspose::Words::Node> node);
    /// Copies all nodes from the collection to a new array of nodes.
    /// 
    /// You should not be adding/removing nodes while iterating over a collection
    /// of nodes because it invalidates the iterator and requires refreshes for live collections.
    /// 
    /// To be able to add/remove nodes during iteration, use this method to copy
    /// nodes into a fixed-size array and then iterate over the array.
    /// 
    /// @return An array of nodes.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::SharedPtr<Aspose::Words::Node>> ToArray();
    /// Provides a simple "foreach" style iteration over the collection of nodes.
    /// 
    /// @return An IEnumerator.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Node>>> GetEnumerator() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetNextMatchingNode(System::SharedPtr<Aspose::Words::Node> curNode) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetCurrentNode() override;

protected:

    NodeCollection(System::SharedPtr<Aspose::Words::CompositeNode> container, Aspose::Words::NodeType nodeType, bool isDeep);
    NodeCollection(System::SharedPtr<Aspose::Words::CompositeNode> container, System::ArrayPtr<Aspose::Words::NodeType> nodeTypes, bool isDeep);
    NodeCollection(System::SharedPtr<Aspose::Words::CompositeNode> container, System::SharedPtr<Aspose::Words::NodeMatcher> matcher, bool isDeep);

    template <typename TNode>
    System::SharedPtr<Aspose::Words::NodeCollectionEnumerator<TNode>> GetNodeEnumerator()
    {
        typedef Aspose::Words::Node BaseT_Node;
        assert_is_base_of(BaseT_Node, TNode);

        return System::MakeObject<Aspose::Words::NodeCollectionEnumerator<TNode>>(System::MakeSharedPtr(this));
    }

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> ToNodeList();
    template <typename TValue>
    System::SharedPtr<System::Collections::Generic::List<TValue>> ToList()
    {
        typedef Aspose::Words::Node BaseT_Node;
        assert_is_base_of(BaseT_Node, TValue);

        System::SharedPtr<System::Collections::Generic::List<TValue>> result = System::MakeObject<System::Collections::Generic::List<TValue>>();
        for (auto node : System::IterateOver(this))
        {
            result->Add(System::StaticCast<typename TValue::Pointee_>(node));
        }
        return result;
    }

    System::SharedPtr<Aspose::Words::Node> GetNthMatchingNode(System::SharedPtr<Aspose::Words::Node> startNode, int32_t count);

    virtual ASPOSE_WORDS_SHARED_API ~NodeCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t get_DocumentTreeChangeCount();

    System::SharedPtr<Aspose::Words::Node> mCurrent;
    System::WeakPtr<Aspose::Words::CompositeNode> mContainer;
    System::WeakPtr<Aspose::Words::DocumentBase> mDocument;
    bool mIsDeep;
    int32_t mInitialChangeCount;
    int32_t mCurIndex;
    System::WeakPtr<Aspose::Words::Node> mCurNode;
    int32_t mCount;
    System::SharedPtr<Aspose::Words::NodeMatcher> mMatcher;

    System::SharedPtr<Aspose::Words::Node> GetMatchingNode(System::SharedPtr<Aspose::Words::Node> curNode, bool isForward);
    System::SharedPtr<Aspose::Words::Node> FindMatchingNode(System::SharedPtr<Aspose::Words::Node> curNode, bool isForward);
    System::SharedPtr<Aspose::Words::Node> TraverseDeep(bool isForward, System::SharedPtr<Aspose::Words::Node> node);
    System::SharedPtr<Aspose::Words::Node> TraverseFlat(bool isForward, System::SharedPtr<Aspose::Words::Node> node);
    System::SharedPtr<Aspose::Words::Node> TraverseFlatSkipMarkupNodes(bool isForward, System::SharedPtr<Aspose::Words::Node> node);
    void CheckInvalidate();
    void Invalidate();

};

}
}
