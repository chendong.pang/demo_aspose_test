//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Nodes/CompositeNode.h
#pragma once

#include <xml/xpath/xpath_navigator.h>
#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/INodeCollection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LayoutShapeDrawingMLAdapter; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LayoutShapeVMLAdapter; } } } }
namespace Aspose { namespace Words { class ImportInfoCollector; } }
namespace Aspose { namespace Words { class NodeRemover; } }
namespace Aspose { namespace Words { class AnnotationUtil; } }
namespace Aspose { namespace Words { class StyleSeparatorInserter; } }
namespace Aspose { namespace Words { namespace Fields { class FieldQuoteUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { class NodeCollection; } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerIndexer; } } }
namespace Aspose { namespace Words { class NodeIndexer; } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceIndexer; } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { class InlineMarkupResolver; } } }
namespace Aspose { namespace Words { class NodeEnumerator; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { class RevisionGroup; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkDeleter; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkMover; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class WarningGenerator; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldOldResultNodeCollection; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTrackedChangesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMath; } } }
namespace Aspose { namespace Words { class NodeCopier; } }
namespace Aspose { namespace Words { class NodeTextCollector; } }
namespace Aspose { namespace Words { class NodeRangeVisitingHelper; } }
namespace Aspose { namespace Words { class NodeUtil; } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class BuildingBlock; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Fields { class NodeRangeResultApplier; } } }
namespace Aspose { namespace Words { class FootnoteSeparator; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlEquationXmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { class MarkupResolver; } } }
namespace Aspose { namespace Words { class InlineStory; } }
namespace Aspose { namespace Words { class FootnoteSeparatorCollection; } }
namespace Aspose { namespace Words { namespace Markup { class SmartTag; } } }
namespace Aspose { namespace Words { class Story; } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlDocumentSplitter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextIndexesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableCellReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlInlineReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStoryReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapeFiler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlBodyReader; } } } } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class NodeList; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Base class for nodes that can contain other nodes.
/// 
/// A document is represented as a tree of nodes, similar to DOM or XmlDocument.
/// 
/// For more info see the Composite design pattern.
/// 
/// The <see cref="Aspose::Words::CompositeNode">CompositeNode</see> class:
/// 
/// - Provides access to the child nodes.
/// - Implements Composite operations such as insert and remove children.
/// - Provides methods for XPath navigation.
class ASPOSE_WORDS_SHARED_CLASS CompositeNode : public Aspose::Words::Node, public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>>, public Aspose::Words::INodeCollection
{
    typedef CompositeNode ThisType;
    typedef Aspose::Words::Node BaseType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>> BaseType1;
    typedef Aspose::Words::INodeCollection BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::Core::LayoutShapeDrawingMLAdapter;
    friend class Aspose::Words::Layout::Core::LayoutShapeVMLAdapter;
    friend class Aspose::Words::ImportInfoCollector;
    friend class Aspose::Words::NodeRemover;
    friend class Aspose::Words::AnnotationUtil;
    friend class Aspose::Words::StyleSeparatorInserter;
    friend class Aspose::Words::Fields::FieldQuoteUpdater;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::NodeCollection;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::ComparerIndexer;
    friend class Aspose::Words::NodeIndexer;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Replacing::FindReplaceIndexer;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::Docx::Writer::DocxFieldsWriter;
    friend class Aspose::Words::RW::InlineMarkupResolver;
    friend class Aspose::Words::NodeEnumerator;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Replacing::FindReplace;
    friend class Aspose::Words::RevisionGroup;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Revisions::BookmarkDeleter;
    friend class Aspose::Words::Revisions::BookmarkMover;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Layout::Core::WarningGenerator;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::Fields::FieldOldResultNodeCollection;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::RW::Odt::Writer::OdtTrackedChangesWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::TocEntryExtractor;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Markup::XmlMapping;
    friend class Aspose::Words::Math::OfficeMath;
    friend class Aspose::Words::NodeCopier;
    friend class Aspose::Words::NodeTextCollector;
    friend class Aspose::Words::NodeRangeVisitingHelper;
    friend class Aspose::Words::NodeUtil;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::BuildingBlocks::BuildingBlock;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Fields::NodeRangeResultApplier;
    friend class Aspose::Words::FootnoteSeparator;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::Drawing::ShapeBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlEquationXmlWriter;
    friend class Aspose::Words::RW::MarkupResolver;
    friend class Aspose::Words::InlineStory;
    friend class Aspose::Words::FootnoteSeparatorCollection;
    friend class Aspose::Words::Markup::SmartTag;
    friend class Aspose::Words::Story;
    friend class Aspose::Words::Section;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::Tables::Cell;
    friend class Aspose::Words::Tables::Row;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlDocumentSplitter;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextIndexesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableCellReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableReader;
    friend class Aspose::Words::RW::Vml::VmlShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlInlineReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Docx::Reader::DocxStoryReader;
    friend class Aspose::Words::RW::Doc::Escher::EsShapeFiler;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlBodyReader;

public:
    using Aspose::Words::Node::Clone;

public:

    /// Returns true as this node can have child nodes.
    ASPOSE_WORDS_SHARED_API bool get_IsComposite() override;
    /// Returns true if this node has any child nodes.
    ASPOSE_WORDS_SHARED_API bool get_HasChildNodes();
    /// Gets all immediate child nodes of this node.
    /// 
    /// Note, <see cref="Aspose::Words::CompositeNode::get_ChildNodes">ChildNodes</see> is equivalent to calling <c>GetChildNodes(NodeType.Any, false)</c>
    /// and creates and returns a new collection every time it is accessed.
    /// 
    /// If there are no child nodes, this property returns an empty collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeCollection> get_ChildNodes();
    /// Gets the first child of the node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_FirstChild() const;
    /// Gets the last child of the node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_LastChild() const;
    /// Gets the number of immediate children of this node.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CompositeNode> get_Container() override;

    /// Gets the text of this node and of all its children.
    /// 
    /// The returned string includes all control and special characters as described in <see cref="Aspose::Words::ControlChar">ControlChar</see>.
    ASPOSE_WORDS_SHARED_API System::String GetText() override;
    /// Returns a live collection of child nodes that match the specified type.
    /// 
    /// The collection of nodes returned by this method is always live.
    /// 
    /// A live collection is always in sync with the document. For example, if you
    /// selected all sections in a document and enumerate through the collection
    /// deleting the sections, the section is removed from the collection immediately
    /// when it is removed from the document.
    /// 
    /// @param nodeType Specifies the type of nodes to select.
    /// @param isDeep True to select from all child nodes recursively.
    ///     False to select only among immediate children.
    /// 
    /// @return A live collection of child nodes of the specified type.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeCollection> GetChildNodes(Aspose::Words::NodeType nodeType, bool isDeep);
    /// Returns an Nth child node that matches the specified type.
    /// 
    /// If index is out of range, a null is returned.
    /// 
    /// @param nodeType Specifies the type of the child node.
    /// @param index Zero based index of the child node to select.
    ///     Negative indexes are also allowed and indicate access from the end,
    ///     that is -1 means the last node.
    /// @param isDeep True to select from all child nodes recursively.
    ///     False to select only among immediate children. See remarks for more info.
    /// 
    /// @return The child node that matches the criteria or null if no matching node is found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetChild(Aspose::Words::NodeType nodeType, int32_t index, bool isDeep);
    /// Selects a list of nodes matching the XPath expression.
    /// 
    /// Only expressions with element names are supported at the moment. Expressions
    /// that use attribute names are not supported.
    /// 
    /// @param xpath The XPath expression.
    /// 
    /// @return A list of nodes matching the XPath query.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeList> SelectNodes(System::String xpath);
    /// Selects the first Node that matches the XPath expression.
    /// 
    /// Only expressions with element names are supported at the moment. Expressions
    /// that use attribute names are not supported.
    /// 
    /// @param xpath The XPath expression.
    /// 
    /// @return The first Node that matches the XPath query or null if no matching node is found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> SelectSingleNode(System::String xpath);
    /// Provides support for the for each style iteration over the child nodes of this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Node>>> GetEnumerator() override;
    /// Adds the specified node to the end of the list of child nodes for this node.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param newChild The node to add.
    /// 
    /// @return The node added.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> AppendChild(System::SharedPtr<Aspose::Words::Node> newChild);
    /// Adds the specified node to the beginning of the list of child nodes for this node.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param newChild The node to add.
    /// 
    /// @return The node added.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> PrependChild(System::SharedPtr<Aspose::Words::Node> newChild);
    /// Inserts the specified node immediately after the specified reference node.
    /// 
    /// If refChild is null, inserts newChild at the beginning of the list of child nodes.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param newChild The Node to insert.
    /// @param refChild The Node that is the reference node. The newNode is placed after the refNode.
    /// 
    /// @return The inserted node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> InsertAfter(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> refChild);
    /// Inserts the specified node immediately before the specified reference node.
    /// 
    /// If refChild is null, inserts newChild at the end of the list of child nodes.
    /// 
    /// If the newChild is already in the tree, it is first removed.
    /// 
    /// If the node being inserted was created from another document, you should use
    /// <see cref="Aspose::Words::DocumentBase::ImportNode(System::SharedPtr<Aspose::Words::Node>, bool, Aspose::Words::ImportFormatMode)">ImportNode()</see> to import the node to the current document.
    /// The imported node can then be inserted into the current document.
    /// 
    /// @param newChild The Node to insert.
    /// @param refChild The Node that is the reference node. The newChild is placed before this node.
    /// 
    /// @return The inserted node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> InsertBefore(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> refChild);
    /// Removes the specified child node.
    /// 
    /// The parent of oldChild is set to null after the node is removed.
    /// 
    /// @param oldChild The node to remove.
    /// 
    /// @return The removed node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> RemoveChild(System::SharedPtr<Aspose::Words::Node> oldChild);
    /// Removes all the child nodes of the current node.
    ASPOSE_WORDS_SHARED_API void RemoveAllChildren();
    /// Removes all <see cref="Aspose::Words::Markup::SmartTag">SmartTag</see> descendant nodes of the current node.
    ASPOSE_WORDS_SHARED_API void RemoveSmartTags();
    /// Returns the index of the specified child node in the child node array.
    ASPOSE_WORDS_SHARED_API int32_t IndexOf(System::SharedPtr<Aspose::Words::Node> child);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetNextMatchingNode(System::SharedPtr<Aspose::Words::Node> curNode) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetCurrentNode() override;

protected:

    bool get_HasNonMarkupDescendants();
    bool get_HasOneChildOnly();
    System::SharedPtr<Aspose::Words::Node> get_FirstNonMarkupDescendant();
    System::SharedPtr<Aspose::Words::Node> get_LastNonMarkupDescendant();
    System::SharedPtr<Aspose::Words::CompositeNode> get_FirstNonMarkupCompositeDescendant();
    System::SharedPtr<Aspose::Words::CompositeNode> get_LastNonMarkupCompositeDescendant();
    System::SharedPtr<Aspose::Words::Node> get_FirstNonAnnotationChild();
    System::SharedPtr<Aspose::Words::Node> get_LastNonAnnotationChild();

    ASPOSE_WORDS_SHARED_API CompositeNode();
    ASPOSE_WORDS_SHARED_API CompositeNode(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    ASPOSE_WORDS_SHARED_API void CoreRemoveSelfOnly();
    bool HasInlineNodes();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API void GetTextToBuilder(System::SharedPtr<System::Text::StringBuilder> builder) override;
    System::SharedPtr<Aspose::Words::NodeCollection> GetChildNodes(System::ArrayPtr<Aspose::Words::NodeType> nodeTypes, bool isDeep);
    System::SharedPtr<Aspose::Words::Node> AppendChildForLoad(System::SharedPtr<Aspose::Words::Node> newChild);
    void InsertBefore(System::SharedPtr<Aspose::Words::Node> start, System::SharedPtr<Aspose::Words::Node> end, System::SharedPtr<Aspose::Words::Node> refNode);
    void InsertAfter(System::SharedPtr<Aspose::Words::Node> start, System::SharedPtr<Aspose::Words::Node> end, System::SharedPtr<Aspose::Words::Node> refNode);
    int32_t IndexOfChildByDescendant(System::SharedPtr<Aspose::Words::Node> descendant, bool skipAnnotations);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> GetNodeFromPos(int32_t position) override;
    ASPOSE_WORDS_SHARED_API int32_t GetTextLength() override;
    virtual ASPOSE_WORDS_SHARED_API System::String GetEndText();
    System::String GetChildrenText();
    ASPOSE_WORDS_SHARED_API bool AcceptCore(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor);
    virtual Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) = 0;
    virtual Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) = 0;
    ASPOSE_WORDS_SHARED_API bool AcceptChildren(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor);
    virtual bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) = 0;
    System::SharedPtr<Aspose::Words::Node> Insert(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> refChild, bool isAfter);

    virtual ASPOSE_WORDS_SHARED_API ~CompositeNode();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Node> mFirstChild;
    System::SharedPtr<Aspose::Words::Node> mLastChild;
    System::SharedPtr<Aspose::Words::Node> mCurrent;

    void GetChildrenTextInternal(System::SharedPtr<System::Text::StringBuilder> builder);
    void InsertAfterCore(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> prevChild);
    void InsertBeforeCore(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> nextChild);
    System::SharedPtr<Aspose::Words::Node> RemoveChildCore(System::SharedPtr<Aspose::Words::Node> oldChild);

};

}
}
