//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Math/OfficeMath.h
#pragma once

#include <system/text/string_builder.h>
#include <system/text/encoding.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>

#include "Aspose.Words.Cpp/Model/Formatting/RunPrExpandFlags.h"
#include "Aspose.Words.Cpp/Model/Text/IInline.h"
#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Math/OfficeMathJustification.h"
#include "Aspose.Words.Cpp/Model/Math/OfficeMathDisplayType.h"
#include "Aspose.Words.Cpp/Model/Math/MathObjectType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathMatrixRowElement; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LayoutShapeOfficeMathAdapter; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldQuoteUpdater; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMathUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxMathReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLReadContext; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class FallbackShapeValidator; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathFunctionElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathArrayElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathMatrixElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathBracketElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathGroupCharElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathLimitElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathAccentElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathElementFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathFractionElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathNaryElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathRadicalElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathScriptElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class OfficeMathToApsConverter; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEQ; } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLWriter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLReader; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Math { class MathObject; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectFraction; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectFunction; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectLowerLimit; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectMatrix; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectNAry; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectOMathPara; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectPreSubSuperscript; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectRadical; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectSubscript; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectSubSuperscript; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectSuperscript; } } }
namespace Aspose { namespace Words { namespace Math { class MathObjectUpperLimit; } } }
namespace Aspose { namespace Words { class NodeCopier; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanShape; } } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class MathValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class MathObjectMatrixValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMathHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMathPropertiesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfMathWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { namespace Rendering { class OfficeMathRenderer; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }

namespace Aspose {

namespace Words {

namespace Math {

/// Represents an Office Math object such as function, equation, matrix or alike. Can contain child elements
/// including runs of mathematical text, bookmarks, comments, other <see cref="Aspose::Words::Math::OfficeMath">OfficeMath</see> instances and some other nodes.
/// 
/// In this version of Aspose.Words, <see cref="Aspose::Words::Math::OfficeMath">OfficeMath</see> nodes do not provide public methods
/// and properties to create or modify a OfficeMath object. In this version you are not able to instantiate
/// <see cref="Aspose::Words::Math">Math</see> nodes or modify existing except deleting them.
/// 
/// <see cref="Aspose::Words::Math::OfficeMath">OfficeMath</see> can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
class ASPOSE_WORDS_SHARED_CLASS OfficeMath : public Aspose::Words::CompositeNode, public Aspose::Words::IInline, public Aspose::Words::Revisions::ITrackableNode
{
    typedef OfficeMath ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::IInline BaseType1;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Math::MathMatrixRowElement;
    friend class Aspose::Words::Layout::Core::LayoutShapeOfficeMathAdapter;
    friend class Aspose::Words::Fields::FieldQuoteUpdater;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::Math::OfficeMathUtil;
    friend class Aspose::Words::RW::Nrx::Reader::NrxMathReaderBase;
    friend class Aspose::Words::RW::MathML::MathMLReadContext;
    friend class Aspose::Words::Validation::DmlToVml::FallbackShapeValidator;
    friend class Aspose::Words::ApsBuilder::Math::MathFunctionElement;
    friend class Aspose::Words::ApsBuilder::Math::MathElement;
    friend class Aspose::Words::ApsBuilder::Math::MathArrayElement;
    friend class Aspose::Words::ApsBuilder::Math::MathMatrixElement;
    friend class Aspose::Words::ApsBuilder::Math::MathBracketElement;
    friend class Aspose::Words::ApsBuilder::Math::MathGroupCharElement;
    friend class Aspose::Words::ApsBuilder::Math::MathLimitElement;
    friend class Aspose::Words::ApsBuilder::Math::MathAccentElement;
    friend class Aspose::Words::ApsBuilder::Math::MathElementFactory;
    friend class Aspose::Words::ApsBuilder::Math::MathFractionElement;
    friend class Aspose::Words::ApsBuilder::Math::MathNaryElement;
    friend class Aspose::Words::ApsBuilder::Math::MathRadicalElement;
    friend class Aspose::Words::ApsBuilder::Math::MathScriptElement;
    friend class Aspose::Words::ApsBuilder::Math::OfficeMathToApsConverter;
    friend class Aspose::Words::Fields::FieldEQ;
    friend class Aspose::Words::RW::MathML::MathMLWriter;
    friend class Aspose::Words::RW::MathML::MathMLReader;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Math::MathObject;
    friend class Aspose::Words::Math::MathObjectFraction;
    friend class Aspose::Words::Math::MathObjectFunction;
    friend class Aspose::Words::Math::MathObjectLowerLimit;
    friend class Aspose::Words::Math::MathObjectMatrix;
    friend class Aspose::Words::Math::MathObjectNAry;
    friend class Aspose::Words::Math::MathObjectOMathPara;
    friend class Aspose::Words::Math::MathObjectPreSubSuperscript;
    friend class Aspose::Words::Math::MathObjectRadical;
    friend class Aspose::Words::Math::MathObjectSubscript;
    friend class Aspose::Words::Math::MathObjectSubSuperscript;
    friend class Aspose::Words::Math::MathObjectSuperscript;
    friend class Aspose::Words::Math::MathObjectUpperLimit;
    friend class Aspose::Words::NodeCopier;
    friend class Aspose::Words::Layout::Core::SpanShape;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Validation::MathValidator;
    friend class Aspose::Words::Validation::MathObjectMatrixValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Odt::Reader::OdtObjectReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMathHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMathPropertiesHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfMathWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Returns <b>NodeType.OfficeMath</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Retrieves the parent <see cref="Aspose::Words::Paragraph">Paragraph</see> of this node.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> get_RunPr_IInline() override;
    ASPOSE_WORDS_SHARED_API void set_RunPr_IInline(System::SharedPtr<Aspose::Words::RunPr> value) override;
    /// Gets type <see cref="Aspose::Words::Math::OfficeMath::get_MathObjectType">MathObjectType</see> of this Office Math object.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Math::MathObjectType get_MathObjectType() const;
    /// Gets/sets an encoding that was used to encode equation XML, if this office math object is read from
    /// equation XML. We use the encoding on saving a document to write in same encoding that it was read.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Text::Encoding> get_EquationXmlEncoding() const;
    /// Gets/sets an encoding that was used to encode equation XML, if this office math object is read from
    /// equation XML. We use the encoding on saving a document to write in same encoding that it was read.
    ASPOSE_WORDS_SHARED_API void set_EquationXmlEncoding(System::SharedPtr<System::Text::Encoding> value);
    /// Gets/sets Office Math justification.
    /// 
    /// Justification cannot be set to the Office Math with display format type <see cref="Aspose::Words::Math::OfficeMathDisplayType::Inline">Inline</see>.
    /// 
    /// Inline justification cannot be set to the Office Math with display format type <see cref="Aspose::Words::Math::OfficeMathDisplayType::Display">Display</see>.
    /// 
    /// Corresponding <see cref="Aspose::Words::Math::OfficeMath::get_DisplayType">DisplayType</see> has to be set before setting Office Math justification.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Math::OfficeMathJustification get_Justification();
    /// Setter for Aspose::Words::Math::OfficeMath::get_Justification
    ASPOSE_WORDS_SHARED_API void set_Justification(Aspose::Words::Math::OfficeMathJustification value);
    /// Gets/sets Office Math display format type which represents whether an equation is displayed inline with the text
    /// or displayed on its own line.
    /// 
    /// Display format type has effect for top level Office Math only.
    /// 
    /// Returned display format type is always <see cref="Aspose::Words::Math::OfficeMathDisplayType::Inline">Inline</see> for nested Office Math.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Math::OfficeMathDisplayType get_DisplayType();
    /// Setter for Aspose::Words::Math::OfficeMath::get_DisplayType
    ASPOSE_WORDS_SHARED_API void set_DisplayType(Aspose::Words::Math::OfficeMathDisplayType value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Creates and returns an object that can be used to render this equation into an image.
    /// 
    /// This method just invokes the <see cref="Aspose::Words::Rendering::OfficeMathRenderer">OfficeMathRenderer</see> constructor and passes
    /// this object as a parameter.
    /// 
    /// @return The renderer object for this equation.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Rendering::OfficeMathRenderer> GetMathRenderer();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t fontAttr, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr_IInline(Aspose::Words::RunPrExpandFlags flags) override;

protected:

    bool get_IsTopLevel();
    bool get_IsInline();
    bool get_IsConvertedFromRuby() const;
    void set_IsConvertedFromRuby(bool value);
    bool get_IsConvertedFromEQ() const;
    void set_IsConvertedFromEQ(bool value);
    System::SharedPtr<Aspose::Words::Math::MathObject> get_MathObject() const;
    void set_MathObject(System::SharedPtr<Aspose::Words::Math::MathObject> value);
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr() const;

    OfficeMath(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Math::MathObject> mathObject, System::SharedPtr<Aspose::Words::RunPr> runPr);
    OfficeMath(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Math::MathObject> mathObject);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    System::SharedPtr<Aspose::Words::Math::OfficeMath> GetTopLevelOfficeMath();
    bool HasDeleteRevision();
    bool HasInsertRevision();
    System::String GetId();

    virtual ASPOSE_WORDS_SHARED_API ~OfficeMath();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Math::MathObject> mMathObject;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    System::SharedPtr<System::Text::Encoding> mEquationXmlEncoding;
    bool mIsConvertedFromRuby;
    bool mIsConvertedFromEQ;

    bool HasRevisableParts();
    bool AllRevisablePartsHaveDeleteRevisions();
    bool AllRevisablePartsHaveInsertRevisions();
    System::String ToStringInternal(System::String indent) const;
    bool IsRevisable();
    static void ChildNodesToString(System::SharedPtr<System::Text::StringBuilder> sb, System::SharedPtr<Aspose::Words::CompositeNode> rootNode, System::String indent);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
