//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Styles/ConditionalStyleCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/collections/icollection.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Styles/ConditionalStyle.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class TablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class RunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class TableFontLookup; } } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Collections { template<typename> class IntToObjDictionary; } }
namespace Aspose { namespace Words { enum class TableStyleOverrideType; } }
namespace Aspose { namespace Words { enum class ConditionalStyleType; } }

namespace Aspose {

namespace Words {

/// Represents a collection of <see cref="Aspose::Words::ConditionalStyle">ConditionalStyle</see> objects.
class ASPOSE_WORDS_SHARED_CLASS ConditionalStyleCollection FINAL : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::ConditionalStyle>>
{
    typedef ConditionalStyleCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::ConditionalStyle>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::RW::Doc::Reader::TablePrReader;
    friend class Aspose::Words::RW::Doc::Writer::ParaPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::RunPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::RW::Doc::TableFontLookup;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::RW::Docx::Writer::DocxStylesWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfStylesheetWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

private:

    class ConditionalStylesEnumerator FINAL : public System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::ConditionalStyle>>
    {
        typedef ConditionalStylesEnumerator ThisType;
        typedef System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::ConditionalStyle>> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::ConditionalStyle> get_Current() const override;

        ConditionalStylesEnumerator(System::SharedPtr<Aspose::Words::ConditionalStyleCollection> collection);

        void Dispose() override;
        bool MoveNext() override;
        void Reset() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::ConditionalStyleCollection> mCollection;
        int32_t mIndex;

    };

public:

    /// Gets the number of conditional styles in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count() const;
    /// Gets the first row style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_FirstRow();
    /// Gets the first column style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_FirstColumn();
    /// Gets the last row style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_LastRow();
    /// Gets the last column style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_LastColumn();
    /// Gets the odd row banding style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_OddRowBanding();
    /// Gets the odd column banding style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_OddColumnBanding();
    /// Gets the even row banding style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_EvenRowBanding();
    /// Gets the even column banding style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_EvenColumnBanding();
    /// Gets the top left cell style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_TopLeftCell();
    /// Gets the top right cell style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_TopRightCell();
    /// Gets the bottom left cell style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_BottomLeftCell();
    /// Gets the bottom right cell style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> get_BottomRightCell();

    /// Clears all conditional styles of the table style.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    /// Returns an enumerator object that can be used to iterate over all conditional styles in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::ConditionalStyle>>> GetEnumerator() override;

    /// Retrieves a <see cref="Aspose::Words::ConditionalStyle">ConditionalStyle</see> object by conditional style type.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> idx_get(Aspose::Words::ConditionalStyleType conditionalStyleType);
    /// Retrieves a <see cref="Aspose::Words::ConditionalStyle">ConditionalStyle</see> object by index.
    /// 
    /// @param index Zero-based index of the conditional style to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyle> idx_get(int32_t index);

protected:

    System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::ConditionalStyle>>> get_DefinedStyles();

    ConditionalStyleCollection(System::SharedPtr<Aspose::Words::TableStyle> parentStyle);

    void Add(System::SharedPtr<Aspose::Words::ConditionalStyle> conditionalStyle);
    void Clear();
    bool ContainsTableStyleOverride(Aspose::Words::TableStyleOverrideType type);
    bool ContainsConditionalStyle(Aspose::Words::ConditionalStyleType type);

    System::SharedPtr<Aspose::Words::ConditionalStyle> idx_get(Aspose::Words::TableStyleOverrideType overrideType);

    virtual ASPOSE_WORDS_SHARED_API ~ConditionalStyleCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::TableStyle> mParentStyle;
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Words::ConditionalStyle>>> mItems;

    static int32_t& gCount();
    static Aspose::Words::TableStyleOverrideType ConditionalStyleTypeToOverrideType(Aspose::Words::ConditionalStyleType value);

};

}
}
