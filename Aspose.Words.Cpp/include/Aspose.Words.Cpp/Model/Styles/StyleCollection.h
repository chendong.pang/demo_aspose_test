//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Styles/StyleCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/collections/dictionary.h>

#include "Aspose.Words.Cpp/Model/Styles/Style.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class LanguagePreferences; } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexFormatApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexAndTablesEntryAttributeModifier; } } }
namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Comparison { class StylesheetComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { namespace Validation { class StyleIstdNormalizer; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterHtmlRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterWordRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfDefaultRunPrHandler; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class TocParagraphLevelFinder; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentCleaner; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStylesheet; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class StyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class StyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocHyperlinkEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtPlaceholderManager; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Validation { class UnitConverter; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabel; } } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Tables { class DocTableContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxParaPrWriter; } } } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class LatentStyles; } }
namespace Aspose { namespace Words { class StyleIndex; } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtOfficeStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Pdf { namespace DirectWriter { class PdfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLatentStylesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfLatentStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxStyleUtil; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfGroupState; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStyleCollapser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStylesheetHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfHeaderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriterContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlFontsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFontsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxStyleIdGenerator; } } } } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedIntegerListGeneric; } } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedStringListGeneric; } } }
namespace Aspose { namespace Words { enum class StyleIdentifier; } }
namespace Aspose { namespace Words { enum class LoadFormat; } }
namespace Aspose { namespace Words { enum class RunPrExpandFlags; } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class ImportContext; } }

namespace Aspose {

namespace Words {

/// A collection of Style objects that represent both the built-in and user-defined styles in a document.
class ASPOSE_WORDS_SHARED_CLASS StyleCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Style>>
{
    typedef StyleCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Style>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::LanguagePreferences;
    friend class Aspose::Words::Fields::FieldIndexFormatApplier;
    friend class Aspose::Words::Fields::IndexAndTablesEntryAttributeModifier;
    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Comparison::StylesheetComparer;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::Validation::StyleIstdNormalizer;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterHtmlRules;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterWordRules;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::RW::Rtf::Reader::RtfDefaultRunPrHandler;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::TocParagraphLevelFinder;
    friend class Aspose::Words::Validation::DocumentCleaner;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlStylesheet;
    friend class Aspose::Words::RW::Doc::Reader::StyleReader;
    friend class Aspose::Words::RW::Doc::Writer::StyleWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocHyperlinkEntryAttributeModifier;
    friend class Aspose::Words::Markup::SdtPlaceholderManager;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Validation::UnitConverter;
    friend class Aspose::Words::Lists::ListLabel;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::RW::Doc::Tables::DocTableContext;
    friend class Aspose::Words::RW::Nrx::Writer::NrxParaPrWriter;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::ListDef;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::LatentStyles;
    friend class Aspose::Words::StyleIndex;
    friend class Aspose::Words::Tables::Row;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::ParagraphFormat;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::NrxTableUtil;
    friend class Aspose::Words::RW::Odt::Reader::OdtOfficeStylesReader;
    friend class Aspose::Words::RW::Pdf::DirectWriter::PdfWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLatentStylesHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfLatentStylesWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxStylesWriter;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxStyleUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfGroupState;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStyleCollapser;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStylesheetHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfHeaderWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfStylesheetWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriterContext;
    friend class Aspose::Words::RW::Wml::Reader::WmlFontsReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlStylesReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlDocPrWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlFontsWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxStyleIdGenerator;

public:

    /// Gets the owner document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const;
    /// Gets document default text formatting.
    /// 
    /// Note that document-wide defaults were introduced in Microsoft Word 2007 and are fully supported in OOXML formats (<see cref="Aspose::Words::LoadFormat::Docx">Docx</see>) only.
    /// Earlier document formats have limited support for this feature and only font names can be stored.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_DefaultFont();
    /// Gets document default paragraph formatting.
    /// 
    /// Note that document-wide defaults were introduced in Microsoft Word 2007 and are fully supported in OOXML formats (<see cref="Aspose::Words::LoadFormat::Docx">Docx</see>) only.
    /// Earlier document formats have no support for document default paragraph formatting.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphFormat> get_DefaultParagraphFormat();
    /// Gets the number of styles in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets a style by name or alias.
    /// 
    /// Case sensitive, returns null if the style with the given name is not found.
    /// 
    /// If this is an English name of a built in style that does not yet exist, automatically creates it.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> idx_get(System::String name);
    /// Gets a built-in style by its locale independent identifier.
    /// 
    /// When accessing a style that does not yet exist, automatically creates it.
    /// 
    /// @param sti A <see cref="Aspose::Words::StyleIdentifier">StyleIdentifier</see> value that specifies the built in style to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> idx_get(Aspose::Words::StyleIdentifier sti);
    /// Gets a style by index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> idx_get(int32_t index);

    /// Gets an enumerator object that will enumerate styles in the alphabetical order of their names.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Style>>> GetEnumerator() override;
    /// Creates a new user defined style and adds it the collection.
    /// 
    /// You can create character, paragraph or a list style.
    /// 
    /// When creating a list style, the style is created with default numbered list formatting (1 \\ a \\ i).
    /// 
    /// Throws an exception if a style with this name already exists.
    /// 
    /// @param type A <see cref="Aspose::Words::StyleType">StyleType</see> value that specifies the type of the style to create.
    /// @param name Case sensitive name of the style to create.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> Add(Aspose::Words::StyleType type, System::String name);
    /// Copies a style into this collection.
    /// 
    /// Style to be copied can belong to the same document as well as to different document.
    /// 
    /// Linked style is copied.
    /// 
    /// This method does doesn't copy base styles.
    /// 
    /// If collection already contains a style with the same name, then new name is
    /// automatically generated by adding "_number" suffix starting from 0 e.g. "Normal_0", "Heading 1_1" etc.
    /// Use <see cref="Aspose::Words::Style::get_Name">Name</see> setter for changing the name of the imported style.
    /// 
    /// @param style Style to be copied.
    /// 
    /// @return Copied style ready for usage.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> AddCopy(System::SharedPtr<Aspose::Words::Style> style);

protected:

    int32_t get_MaxIstd();
    System::SharedPtr<Aspose::Words::RunPr> get_DefaultRunPr() const;
    System::SharedPtr<Aspose::Words::ParaPr> get_DefaultParaPr() const;
    bool get_IsExpandDocumentDefaultsNeeded();
    System::SharedPtr<Aspose::Words::StyleCollection> get_BuiltInStyles();
    System::SharedPtr<Aspose::Words::LatentStyles> get_LatentStyles() const;

    static const int32_t FixedIstdCount;
    static const int32_t MaxFixedIstd;
    static const int32_t MaxKnownSti2003;
    static const int32_t MaxKnownSti2007;
    static const int32_t VersionOfBuiltInNames2007;

    static const System::String& NormalStyleName();

    StyleCollection(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    void Validate();
    static bool IsIgnorableDefaultRunAttr(int32_t key);
    static System::SharedPtr<Aspose::Words::StyleCollection> GetBuiltInStyles(Aspose::Words::LoadFormat loadFormat);
    int32_t GetNextFreeIstd();
    bool HasFreeIstd();
    void Remove(System::String styleName);
    void RemoveCore(System::SharedPtr<Aspose::Words::Style> style);
    void CopyStylesFromTemplate(System::SharedPtr<Aspose::Words::StyleCollection> template_);
    void Add(System::SharedPtr<Aspose::Words::Style> style);
    void UpdateNameMap(System::SharedPtr<Aspose::Words::Style> newStyle, System::String oldName, System::String newName);
    void UpdateStiMap(System::SharedPtr<Aspose::Words::Style> style, Aspose::Words::StyleIdentifier oldSti, Aspose::Words::StyleIdentifier newSti);
    void UpdateIstdMap(System::SharedPtr<Aspose::Words::Style> style, int32_t oldIstd, int32_t newIstd);
    void UpdateIstdMap();
    void AddForLoad(System::SharedPtr<Aspose::Words::Style> style, System::ArrayPtr<System::String> aliases);
    System::String GetUniqueStyleName(System::String name);
    System::SharedPtr<Aspose::Words::StyleCollection> Clone(System::SharedPtr<Aspose::Words::DocumentBase> dstDoc);
    System::String GetAliases(System::SharedPtr<Aspose::Words::Style> style, bool isIncludeName);
    System::SharedPtr<Aspose::Words::Style> GetByIstd(int32_t istd, bool isAllowAutoCreate);
    System::SharedPtr<Aspose::Words::Style> GetByName(System::String name, bool isAllowAutoCreate);
    System::SharedPtr<Aspose::Words::Style> GetBySti(Aspose::Words::StyleIdentifier sti, bool isAllowAutoCreate);
    bool Contains(Aspose::Words::StyleIdentifier styleIdentifier);
    System::SharedPtr<Aspose::Words::Style> FetchByIstd(int32_t istd, int32_t defaultIstd);
    System::SharedPtr<Aspose::Words::Style> FetchByName(System::String name);
    System::SharedPtr<Aspose::Words::Style> FetchBySti(Aspose::Words::StyleIdentifier sti);
    System::SharedPtr<Aspose::Words::Style> ImportStyle(System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<Aspose::Words::Style> ImportStyle(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<Aspose::Words::Style> FindLocaleIndependentMatch(System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<Aspose::Words::Style> FindLocaleIndependentMatchSameType(System::SharedPtr<Aspose::Words::Style> srcStyle);
    void EnsureMinimum();
    void FixUpBasedOnStyles();
    void RemoveLocaleIdsFromStyles();
    void UpdateFromTemplate(System::SharedPtr<Aspose::Words::Document> template_);
    int32_t CopyStyle(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::ImportContext> context);

    virtual ASPOSE_WORDS_SHARED_API ~StyleCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    static System::SharedPtr<Aspose::Words::StyleCollection> get_BuiltInStyles2003();
    static System::SharedPtr<Aspose::Words::StyleCollection> get_BuiltInStyles2007();
    static System::SharedPtr<Aspose::Words::StyleCollection> get_BuiltInStyles2013();
    bool get_IsBuiltinStylesTemplate();

    System::WeakPtr<Aspose::Words::DocumentBase> mDoc;
    System::SharedPtr<Aspose::Words::RunPr> mDefaultRunPr;
    System::SharedPtr<Aspose::Words::ParaPr> mDefaultParaPr;
    System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Style>>> mStylesByIstd;
    System::SharedPtr<Aspose::Collections::Generic::SortedStringListGeneric<System::SharedPtr<Aspose::Words::Style>>> mStylesByName;
    System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Style>>> mBuiltInStylesBySti;
    System::SharedPtr<Aspose::Words::LatentStyles> mLatentStyles;

    static System::SharedPtr<Aspose::Words::Document>& gAllStylesCache2003();
    static System::SharedPtr<System::Object>& gAllStylesSyncRoot2003();
    static System::SharedPtr<Aspose::Words::Document>& gAllStylesCache2007();
    static System::SharedPtr<System::Object>& gAllStylesSyncRoot2007();
    static System::SharedPtr<Aspose::Words::Document>& gAllStylesCache2013();
    static System::SharedPtr<System::Object>& gAllStylesSyncRoot2013();

    System::SharedPtr<Aspose::Words::Font> mDefaultFont;
    System::SharedPtr<Aspose::Words::ParagraphFormat> mDefaultParagraphFormat;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::SharedPtr<Aspose::Words::Style>, System::String>> mStyleAliasesCache;

    static System::SharedPtr<Aspose::Words::Document> ReadBuiltInStylesResource(System::String resourceName);
    int32_t CalculateNextIstd();
    void RemoveFromByNameList(System::SharedPtr<Aspose::Words::Style> style);
    static int32_t GetDefaultStyleIndex(Aspose::Words::StyleType styleType);
    void ProcessTableStyle(int32_t istd, int32_t newIstd);
    void ProcessListStyle(System::SharedPtr<Aspose::Words::Style> style, int32_t istd, int32_t newIstd);
    void ProcessParaStyle(int32_t istd, int32_t newIstd);
    void ProcessCharStyle(int32_t istd, int32_t newIstd);
    static void UpdateRunPr(System::SharedPtr<Aspose::Words::RunPr> runPr, int32_t istd, int32_t newIstd);
    static void ExpandStyle(System::SharedPtr<Aspose::Words::Style> style, System::SharedPtr<Aspose::Words::Style> removedStyle);
    void UpdateModelWithIstd(System::SharedPtr<Aspose::Words::Style> newStyle, int32_t istd, int32_t newIstd);
    void UpdateStyles(int32_t istd, int32_t newIstd);
    void UpdateDomWithIstd(System::SharedPtr<Aspose::Words::Style> style, int32_t istd, int32_t newIstd);
    static bool IsValidStyleType(System::SharedPtr<Aspose::Words::Style> style);
    System::SharedPtr<Aspose::Words::Style> AddCopyCore(System::SharedPtr<Aspose::Words::Style> style);
    static void CopyStyleAttributes(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    static void CopyParaPr(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    static void CopyRunPr(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    static Aspose::Words::RunPrExpandFlags GetRunPrExpandFlags(System::SharedPtr<Aspose::Words::Style> dstStyle, bool isStyleLinkedInSource);
    static void CopyTableStyle(System::SharedPtr<Aspose::Words::TableStyle> srcTableStyle, System::SharedPtr<Aspose::Words::TableStyle> dstTableStyle);
    void CacheAliases();
    System::SharedPtr<Aspose::Words::Style> GetBuiltInStylePrototype(Aspose::Words::StyleIdentifier sti);
    Aspose::Words::LoadFormat GetLoadFormat();
    System::SharedPtr<Aspose::Words::Style> ImportStyleKeepSourceFormatting(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<Aspose::Words::Style> ImportStyleUseDestinationStyles(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<Aspose::Words::Style> AddImportedStyle(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Style> dstStyle);
    static void ImportListDefinition(System::SharedPtr<Aspose::Words::ImportContext> context, System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    void EnsureReservedValid(Aspose::Words::StyleIdentifier sti, System::String styleName);
    static void RemoveZeroFontSize(System::SharedPtr<Aspose::Words::RunPr> runPr, int32_t key);
    System::SharedPtr<Aspose::Words::Style> FindEqualStyle(System::SharedPtr<Aspose::Words::Style> srcStyle);
    void CalculateDifference(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    int32_t GetDstBasedOnIstd(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::ImportContext> context);
    void CopyAll(System::SharedPtr<Aspose::Words::StyleCollection> srcStyles, System::SharedPtr<Aspose::Words::ImportContext> context);
    System::SharedPtr<Aspose::Words::Style> CopyStyleCore(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::ImportContext> context);
    static void CloneFormatting(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    static void UpdateNameAndAliases(System::SharedPtr<Aspose::Words::Style> srcStyle, System::SharedPtr<Aspose::Words::Style> dstStyle);
    System::SharedPtr<Aspose::Words::Style> GetByNameSameType(System::SharedPtr<Aspose::Words::Style> srcStyle);
    void FixBalloonStyles();

};

}
}
