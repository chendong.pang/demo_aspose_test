//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Styles/Style.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Styles/StyleType.h"
#include "Aspose.Words.Cpp/Model/Styles/StyleIdentifier.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRunAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IParaAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace System { class ObjectExt; }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexFormatApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexAndTablesEntryAttributeModifier; } } }
namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class StylesheetComparer; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace Validation { class StyleIstdNormalizer; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Reader { class MarkdownReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterHtmlRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class HtmlSelectorStyleCache; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterWordRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class TocParagraphLevelFinder; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentCleaner; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStylesheet; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ParaPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class RunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class StyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class RunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class StyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { class FormatRevisionUtil; } } }
namespace Aspose { namespace Words { class Revision; } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocHyperlinkEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class TocNormalEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtPlaceholderManager; } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class UnitConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabel; } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { class ImportContext; } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListDef; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLevel; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class StyleIndex; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtEnum; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtOfficeStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxStyleUtil; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxUnresolvedStylePartCollection; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfGroupState; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListOverrideTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfListTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStyleCollapser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStylesheetHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlParaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxStyleIdGenerator; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class FrameFormat; } }
namespace Aspose { namespace Words { namespace Lists { class List; } } }
namespace Aspose { namespace Words { class ListFormat; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { class Pair; }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }
namespace Aspose { namespace Words { enum class ParaPrExpandFlags; } }
namespace Aspose { namespace Words { enum class RunPrExpandFlags; } }
namespace Aspose { namespace Words { class IWarningCallback; } }
namespace Aspose { namespace Words { enum class WarningType; } }
namespace Aspose { namespace Words { enum class WarningSource; } }
namespace Aspose { namespace Words { enum class RevisionsView; } }

namespace Aspose {

namespace Words {

/// Represents a single built-in or user-defined style.
class ASPOSE_WORDS_SHARED_CLASS Style : public Aspose::Words::IParaAttrSource, public Aspose::Words::IRunAttrSource
{
    typedef Style ThisType;
    typedef Aspose::Words::IParaAttrSource BaseType;
    typedef Aspose::Words::IRunAttrSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class System::ObjectExt;
    friend class Aspose::Words::Fields::FieldIndexFormatApplier;
    friend class Aspose::Words::Fields::FieldIndexFormatApplier;
    friend class Aspose::Words::Fields::FieldIndexFormatApplier;
    friend class Aspose::Words::Fields::IndexAndTablesEntryAttributeModifier;
    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Comparison::StylesheetComparer;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::Validation::StyleIstdNormalizer;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::RW::Markdown::Reader::MarkdownReaderContext;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterHtmlRules;
    friend class Aspose::Words::RW::Html::Css::New::HtmlSelectorStyleCache;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterWordRules;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::TocParagraphLevelFinder;
    friend class Aspose::Words::Validation::DocumentCleaner;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlStylesheet;
    friend class Aspose::Words::RW::Doc::Reader::ParaPrReader;
    friend class Aspose::Words::RW::Doc::Reader::RunPrReader;
    friend class Aspose::Words::RW::Doc::Reader::StyleReader;
    friend class Aspose::Words::RW::Doc::Writer::ParaPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::RunPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::StyleWriter;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::RW::FormatRevisionUtil;
    friend class Aspose::Words::Revision;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocHyperlinkEntryAttributeModifier;
    friend class Aspose::Words::Fields::TocNormalEntryAttributeModifier;
    friend class Aspose::Words::Markup::SdtPlaceholderManager;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Validation::UnitConverter;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Lists::ListLabel;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::ConditionalStyle;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::ImportContext;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::ListDef;
    friend class Aspose::Words::Lists::ListLevel;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::StyleIndex;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::ParagraphFormat;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::NrxTableUtil;
    friend class Aspose::Words::RW::Odt::OdtEnum;
    friend class Aspose::Words::RW::Odt::Reader::OdtOfficeStylesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtStylesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxStylesWriter;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Nrx::Reader::NrxStyleUtil;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxUnresolvedStylePartCollection;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfGroupState;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListOverrideTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfListTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStyleCollapser;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStylesheetHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfStylesheetWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlParaReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlStylesReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxStyleIdGenerator;

public:
    using System::Object::Equals;

public:

    /// Gets or sets the name of the style.
    /// 
    /// Can not be empty string.
    /// 
    /// If there already is a style with such name in the collection, then this style will override it. All affected nodes will reference new style.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Setter for Aspose::Words::Style::get_Name
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Gets the locale independent style identifier for a built-in style.
    /// 
    /// For user defined (custom) styles, this property returns <see cref="Aspose::Words::StyleIdentifier::User">User</see>.
    /// 
    /// @sa Aspose::Words::Style::get_Name
    ASPOSE_WORDS_SHARED_API Aspose::Words::StyleIdentifier get_StyleIdentifier() const;
    /// Gets all aliases of this style. If style has no aliases then empty array of string is returned.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::String> get_Aliases();
    /// True when the style is one of the built-in Heading styles.
    ASPOSE_WORDS_SHARED_API bool get_IsHeading();
    /// Gets the style type (paragraph or character).
    ASPOSE_WORDS_SHARED_API Aspose::Words::StyleType get_Type() const;
    /// Gets the owner document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document();
    /// Gets the name of the Style linked to this one. Returns Empty string if no styles are linked.
    ASPOSE_WORDS_SHARED_API System::String get_LinkedStyleName();
    /// Gets/sets the name of the style this style is based on.
    ASPOSE_WORDS_SHARED_API System::String get_BaseStyleName();
    /// Gets/sets the name of the style this style is based on.
    ASPOSE_WORDS_SHARED_API void set_BaseStyleName(System::String value);
    /// Gets/sets the name of the style to be applied automatically to a new paragraph inserted after a
    /// paragraph formatted with the specified style.
    ASPOSE_WORDS_SHARED_API System::String get_NextParagraphStyleName();
    /// Gets/sets the name of the style to be applied automatically to a new paragraph inserted after a
    /// paragraph formatted with the specified style.
    ASPOSE_WORDS_SHARED_API void set_NextParagraphStyleName(System::String value);
    /// True if this style is one of the built-in styles in MS Word.
    ASPOSE_WORDS_SHARED_API bool get_BuiltIn();
    /// Gets the character formatting of the style.
    /// 
    /// For list styles this property returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Gets the paragraph formatting of the style.
    /// 
    /// For character and list styles this property returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphFormat> get_ParagraphFormat();
    /// Gets the list that defines formatting of this list style.
    /// 
    /// This property is only valid for list styles.
    /// For other style types this property returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::List> get_List();
    /// Provides access to the list formatting properties of a paragraph style.
    /// 
    /// This property is only valid for paragraph styles.
    /// For other style types this property returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ListFormat> get_ListFormat();
    /// Specifies whether this style is shown in the Quick Style gallery inside MS Word UI.
    ASPOSE_WORDS_SHARED_API bool get_IsQuickStyle() const;
    /// Specifies whether this style is shown in the Quick Style gallery inside MS Word UI.
    ASPOSE_WORDS_SHARED_API void set_IsQuickStyle(bool value);
    /// Gets the collection of styles this style belongs to.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::StyleCollection> get_Styles() const;

    /// Removes the specified style from the document.
    /// 
    /// Style removal has following effects on the document model:
    /// 
    /// - All references to the style are removed from corresponding paragraphs, runs and tables.
    /// - If base style is removed its formatting is moved to child styles.
    /// - If style to be deleted has a linked style, then both of these are deleted.
    ASPOSE_WORDS_SHARED_API void Remove();
    /// Compares with the specified style.
    /// Styles Istds are compared for built-in styles only.
    /// Styles defaults are not included in comparison.
    /// Base style, linked style and next paragraph style are recursively compared.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Style> style);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetParaAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearParaAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;

protected:

    Aspose::Words::StyleIdentifier get_ClonedFromStyleIdentifier() const;
    System::SharedPtr<Aspose::Words::FrameFormat> get_FrameFormat();
    int32_t get_Istd() const;
    int32_t get_BasedOnIstd() const;
    void set_BasedOnIstd(int32_t value);
    int32_t get_NextIstd() const;
    void set_NextIstd(int32_t value);
    int32_t get_LinkedIstd() const;
    void set_LinkedIstd(int32_t value);
    int32_t get_Rsid() const;
    void set_Rsid(int32_t value);
    bool get_AutomaticallyUpdate() const;
    void set_AutomaticallyUpdate(bool value);
    bool get_Hidden() const;
    void set_Hidden(bool value);
    bool get_SemiHidden() const;
    void set_SemiHidden(bool value);
    bool get_Locked() const;
    void set_Locked(bool value);
    bool get_UnhideWhenUsed() const;
    void set_UnhideWhenUsed(bool value);
    int32_t get_UIPriority() const;
    void set_UIPriority(int32_t value);
    bool get_PersonalCompose() const;
    void set_PersonalCompose(bool value);
    bool get_PersonalReply() const;
    void set_PersonalReply(bool value);
    bool get_Personal() const;
    void set_Personal(bool value);
    bool get_LidsSet() const;
    void set_LidsSet(bool value);
    bool get_IsTopLevelParaStyle();
    System::SharedPtr<Aspose::Words::ParaPr> get_ParaPr() const;
    void set_ParaPr(System::SharedPtr<Aspose::Words::ParaPr> value);
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr() const;
    void set_RunPr(System::SharedPtr<Aspose::Words::RunPr> value);
    bool get_HasRevisions();
    bool get_HasEmptyFormatRevision();

    bool RawInvalidHeight;
    bool RawHasUpe;
    bool RawInternalUse;

    static System::ArrayPtr<int32_t>& ComparisonIgnorableKeys();

    static const int32_t MaxUIPriority;

    static System::SharedPtr<Aspose::Words::Style> Create(Aspose::Words::StyleType styleType);
    static System::SharedPtr<Aspose::Words::Style> Create(Aspose::Words::StyleType styleType, int32_t istd, Aspose::Words::StyleIdentifier sti, System::String name);

    ASPOSE_WORDS_SHARED_API Style(Aspose::Words::StyleType styleType);

    System::String GetNameWithAliases();
    System::ArrayPtr<System::String> GetAliasesInternal();
    void SetNameCore(System::String value, bool updateStyleCollection);
    void SetNameCore(System::String value);
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> Clone();
    void ClearCaches();
    bool Equals(System::SharedPtr<Aspose::Words::Style> style, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::SharedPtr<Aspose::Pair>>> alreadyCompared);
    static bool AreEqual(System::SharedPtr<Aspose::Words::Style> styleA, System::SharedPtr<Aspose::Words::Style> styleB, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::SharedPtr<Aspose::Pair>>> alreadyCompared);
    void SetStyles(System::SharedPtr<Aspose::Words::StyleCollection> styles);
    void SetStyleType(Aspose::Words::StyleType styleType);
    void SetIstd(int32_t istd);
    void SetIstd(int32_t istd, bool updateStyleCollection);
    void SetStyleIdentifier(Aspose::Words::StyleIdentifier sti);
    void SetStyleIdentifier(Aspose::Words::StyleIdentifier sti, bool updateStyleCollection);
    System::SharedPtr<Aspose::Words::Style> GetBaseStyle();
    System::SharedPtr<Aspose::Words::Style> GetNextStyle();
    System::SharedPtr<Aspose::Words::Style> GetLinkedStyle();
    virtual ASPOSE_WORDS_SHARED_API bool HasFormatting();
    void FixUpBasedOnMissing();
    void FixUpBasedOnCircularReferences();
    void ExpandParaPr(System::SharedPtr<Aspose::Words::ParaPr> dstParaPr, Aspose::Words::ParaPrExpandFlags flags);
    System::SharedPtr<Aspose::Words::ParaPr> GetExpandedParaPr(Aspose::Words::ParaPrExpandFlags flags);
    void ExpandRunPr(System::SharedPtr<Aspose::Words::RunPr> dstRunPr, Aspose::Words::RunPrExpandFlags flags);
    System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr(Aspose::Words::RunPrExpandFlags flags);
    System::SharedPtr<System::Object> GetFontAttr(int32_t key, bool isAllowDefault);
    void Validate(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback);
    void ApplyFormatting(System::SharedPtr<Aspose::Words::ParaPr> newParaPr, System::SharedPtr<Aspose::Words::RunPr> newRunPr);
    void CopyGenerics(System::SharedPtr<Aspose::Words::Style> srcStyle);
    System::SharedPtr<System::Object> GetInheritedFontAttr(int32_t key, bool isAllowDefault);
    System::SharedPtr<System::Object> GetParaAttr(int32_t key, Aspose::Words::RevisionsView view);
    System::SharedPtr<System::Object> GetAttrFromReferredList(int32_t key);
    System::SharedPtr<Aspose::Words::Lists::List> GetListInternal();

    virtual ASPOSE_WORDS_SHARED_API ~Style();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsInheritRunAndParaFormatting();

    int32_t mIstd;
    Aspose::Words::StyleIdentifier mSti;
    Aspose::Words::StyleIdentifier mClonedFromSti;
    Aspose::Words::StyleType mType;
    int32_t mBasedOnIstd;
    int32_t mNextIstd;
    bool mAutomaticallyUpdate;
    bool mHidden;
    bool mSemiHidden;
    bool mLocked;
    bool mUnhideWhenUsed;
    bool mIsQuickStyle;
    int32_t mUIPriority;
    bool mPersonalCompose;
    bool mPersonalReply;
    bool mPersonal;
    bool mLidsSet;
    int32_t mLinkedIstd;
    int32_t mRsid;
    System::String mName;
    System::SharedPtr<Aspose::Words::ParaPr> mParaPr;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    System::WeakPtr<Aspose::Words::StyleCollection> mStyles;
    System::SharedPtr<Aspose::Words::Font> mFontCache;
    System::SharedPtr<Aspose::Words::ParagraphFormat> mParagraphFormatCache;
    System::SharedPtr<Aspose::Words::FrameFormat> mFrameFormatCache;
    System::SharedPtr<Aspose::Words::ListFormat> mListFormatCache;

    static System::SharedPtr<Aspose::Words::RunPr>& gEmptyRunPr();
    static System::SharedPtr<Aspose::Words::ParaPr>& gEmptyParaPr();
    void FixUpShallowCircularReferences();
    bool VerifySuitableAsBaseStyle(System::SharedPtr<Aspose::Words::Style> proposedBaseStyle, bool isThrow);
    void ExpandRunPrDefaults(System::SharedPtr<Aspose::Words::RunPr> dstRunPr, Aspose::Words::RunPrExpandFlags flags);
    void ExpandParaPrDefaults(System::SharedPtr<Aspose::Words::ParaPr> dstParaPr, Aspose::Words::ParaPrExpandFlags flags);
    void ValidateOutlineLevel(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback);
    void RemoveRunPrIstdThatPointsToParaStyle(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback);
    void FixUpNonExistentLinkedStyle(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback);
    void RemoveNumberRevision();
    void RemoveMagicFontSize();
    static void RemoveMagicFontSize(System::SharedPtr<Aspose::Words::RunPr> runPr, int32_t key);
    void CheckReservedStyleIndex(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback);
    void ValidateTableStyle();
    static void Warn(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback, Aspose::Words::WarningType warningType, Aspose::Words::WarningSource warningSource, System::String description);
    static void WarnUnexpected(System::SharedPtr<Aspose::Words::IWarningCallback> warningCallback, System::String description, const System::ArrayPtr<System::SharedPtr<System::Object>>& args);
    void FixUpOutlineLevel();
    System::SharedPtr<System::Object> GetInheritedParaAttr(int32_t key, Aspose::Words::RevisionsView view);
    bool EqualsCore(System::SharedPtr<Aspose::Words::Style> style, System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<System::SharedPtr<Aspose::Pair>>> alreadyCompared);
    bool BuiltinReservedEquals(System::SharedPtr<Aspose::Words::Style> style);
    bool GenericEquals(System::SharedPtr<Aspose::Words::Style> style);
    bool DirectFormattingEquals(System::SharedPtr<Aspose::Words::Style> style);
    virtual ThisType* CppMemberwiseClone() const { return new ThisType(*this); }

};

}
}
