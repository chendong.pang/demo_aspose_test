//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Styles/TableStyle.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/sorted_list.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Tables/TableAlignment.h"
#include "Aspose.Words.Cpp/Model/Styles/Style.h"
#include "Aspose.Words.Cpp/Model/Formatting/IShadingAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRowAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/ICellAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IBorderAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class PrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class TablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStyleCollapser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStylesheetHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { enum class BorderType; } }
namespace Aspose { namespace Words { class BorderCollection; } }
namespace Aspose { namespace Words { class Shading; } }
namespace Aspose { namespace Words { class ConditionalStyleCollection; } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { enum class TableStyleOverrideType; } }
namespace Aspose { namespace Words { class ConditionalStyle; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class WordAttrCollection; } }
namespace Aspose { namespace Words { enum class AttrCollectionType; } }

namespace Aspose {

namespace Words {

/// Represents a table style.
class ASPOSE_WORDS_SHARED_CLASS TableStyle : public Aspose::Words::Style, public Aspose::Words::ICellAttrSource, public Aspose::Words::IRowAttrSource, public Aspose::Words::IBorderAttrSource, public Aspose::Words::IShadingAttrSource
{
    typedef TableStyle ThisType;
    typedef Aspose::Words::Style BaseType;
    typedef Aspose::Words::ICellAttrSource BaseType1;
    typedef Aspose::Words::IRowAttrSource BaseType2;
    typedef Aspose::Words::IBorderAttrSource BaseType3;
    typedef Aspose::Words::IShadingAttrSource BaseType4;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2, BaseType3, BaseType4> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::RW::Doc::Reader::PrReader;
    friend class Aspose::Words::RW::Doc::Reader::TablePrReader;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::Tables::Cell;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Nrx::NrxTableUtil;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStyleCollapser;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStylesheetHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfStylesheetWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlStylesReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

protected:
    using Aspose::Words::Style::ExpandParaPr;

protected:
    using Aspose::Words::Style::ExpandRunPr;

protected:
    using Aspose::Words::Style::GetParaAttr;

public:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::SortedList<Aspose::Words::BorderType, int32_t>> get_PossibleBorderKeys() override;
    /// Gets a flag indicating whether text in a table row is allowed to split across a page break.
    ASPOSE_WORDS_SHARED_API bool get_AllowBreakAcrossPages();
    /// Sets a flag indicating whether text in a table row is allowed to split across a page break.
    ASPOSE_WORDS_SHARED_API void set_AllowBreakAcrossPages(bool value);
    /// Gets the collection of default cell borders for the style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BorderCollection> get_Borders();
    /// Gets the amount of space (in points) to add to the left of the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_LeftPadding();
    /// Sets the amount of space (in points) to add to the left of the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_LeftPadding(double value);
    /// Gets the amount of space (in points) to add to the right of the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_RightPadding();
    /// Sets the amount of space (in points) to add to the right of the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_RightPadding(double value);
    /// Gets the amount of space (in points) to add above the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_TopPadding();
    /// Sets the amount of space (in points) to add above the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_TopPadding(double value);
    /// Gets the amount of space (in points) to add below the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_BottomPadding();
    /// Sets the amount of space (in points) to add below the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_BottomPadding(double value);
    /// Specifies the alignment for the table style.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Tables::TableAlignment get_Alignment();
    /// Specifies the alignment for the table style.
    ASPOSE_WORDS_SHARED_API void set_Alignment(Aspose::Words::Tables::TableAlignment value);
    /// Gets the amount of space (in points) between the cells.
    ASPOSE_WORDS_SHARED_API double get_CellSpacing();
    /// Sets the amount of space (in points) between the cells.
    ASPOSE_WORDS_SHARED_API void set_CellSpacing(double value);
    /// Gets or sets whether this is a style for a right-to-left table.
    /// 
    /// When <b>true</b>, the cells in rows are laid out right to left.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_Bidi();
    /// Setter for Aspose::Words::TableStyle::get_Bidi
    ASPOSE_WORDS_SHARED_API void set_Bidi(bool value);
    /// Gets the value that represents the left indent of a table.
    ASPOSE_WORDS_SHARED_API double get_LeftIndent();
    /// Sets the value that represents the left indent of a table.
    ASPOSE_WORDS_SHARED_API void set_LeftIndent(double value);
    /// Gets a <see cref="Aspose::Words::Shading">Shading</see> object that refers to the shading formatting for table cells.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Shading> get_Shading();
    /// Gets a number of rows to include in the banding when the style specifies odd/even row banding.
    ASPOSE_WORDS_SHARED_API int32_t get_RowStripe();
    /// Sets a number of rows to include in the banding when the style specifies odd/even row banding.
    ASPOSE_WORDS_SHARED_API void set_RowStripe(int32_t value);
    /// Gets a number of columns to include in the banding when the style specifies odd/even columns banding.
    ASPOSE_WORDS_SHARED_API int32_t get_ColumnStripe();
    /// Sets a number of columns to include in the banding when the style specifies odd/even columns banding.
    ASPOSE_WORDS_SHARED_API void set_ColumnStripe(int32_t value);
    /// Collection of conditional styles that may be defined for this table style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ConditionalStyleCollection> get_ConditionalStyles();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetCellAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearCellAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRowAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearRowAttrs() override;
    ASPOSE_WORDS_SHARED_API void ResetToDefaultAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetBorderAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedShadingAttr(int32_t key) override;

protected:

    System::SharedPtr<Aspose::Words::Tables::TablePr> get_TablePr() const;
    void set_TablePr(System::SharedPtr<Aspose::Words::Tables::TablePr> value);
    System::SharedPtr<Aspose::Words::Tables::TablePr> get_RowPr() const;
    void set_RowPr(System::SharedPtr<Aspose::Words::Tables::TablePr> value);
    System::SharedPtr<Aspose::Words::Tables::CellPr> get_CellPr() const;
    void set_CellPr(System::SharedPtr<Aspose::Words::Tables::CellPr> value);

    TableStyle();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> Clone() override;
    static void CloneProperties(System::SharedPtr<Aspose::Words::TableStyle> src, System::SharedPtr<Aspose::Words::TableStyle> dst);
    void AddConditionalStyle(System::SharedPtr<Aspose::Words::ConditionalStyle> conditionalStyle);
    void ClearConditionalStyles();
    System::SharedPtr<Aspose::Words::Tables::TablePr> GetExpandedTablePr();
    void ExpandParaPr(System::SharedPtr<Aspose::Words::Tables::Cell> cell, System::SharedPtr<Aspose::Words::ParaPr> dstParaPr);
    void ExpandRunPr(System::SharedPtr<Aspose::Words::Tables::Cell> cell, System::SharedPtr<Aspose::Words::RunPr> dstRunPr);
    System::SharedPtr<Aspose::Words::Tables::TablePr> GetExpandedRowPr();
    System::SharedPtr<Aspose::Words::Tables::CellPr> GetExpandedCellPr();
    System::SharedPtr<Aspose::Words::ConditionalStyle> GetExpandedConditionalStyle(Aspose::Words::TableStyleOverrideType type);
    System::SharedPtr<Aspose::Words::WordAttrCollection> FetchConditionalStylePr(Aspose::Words::TableStyleOverrideType type, Aspose::Words::AttrCollectionType prType);
    ASPOSE_WORDS_SHARED_API bool HasFormatting() override;
    System::SharedPtr<System::Object> GetCellAttr(int32_t key, System::SharedPtr<Aspose::Words::Tables::Cell> cell);
    System::SharedPtr<System::Object> GetRunAttr(int32_t key, System::SharedPtr<Aspose::Words::Tables::Cell> cell);
    System::SharedPtr<System::Object> GetParaAttr(int32_t key, System::SharedPtr<Aspose::Words::Tables::Cell> cell);
    bool IsApplicable(System::SharedPtr<Aspose::Words::Tables::Cell> cell, System::SharedPtr<Aspose::Words::ConditionalStyle> conditionalStyle);

    virtual ASPOSE_WORDS_SHARED_API ~TableStyle();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_HasConditionalFormatting();

    System::SharedPtr<Aspose::Words::Tables::TablePr> mTablePr;
    System::SharedPtr<Aspose::Words::Tables::TablePr> mRowPr;
    System::SharedPtr<Aspose::Words::Tables::CellPr> mCellPr;
    System::SharedPtr<Aspose::Words::BorderCollection> mBorders;
    System::SharedPtr<Aspose::Words::ConditionalStyleCollection> mConditionalStyles;

    static System::ArrayPtr<Aspose::Words::TableStyleOverrideType>& gFetchOrder();
    static System::ArrayPtr<Aspose::Words::TableStyleOverrideType>& gFetchOrderDesc();
    System::SharedPtr<Aspose::Words::ConditionalStyle> NextTableStylePrDesc(System::SharedPtr<Aspose::Words::ConditionalStyle> tableStylePr);
    void ExpandTablePr(System::SharedPtr<Aspose::Words::Tables::TablePr> dstTablePr);
    void ExpandRowPr(System::SharedPtr<Aspose::Words::Tables::TablePr> dstRowPr);
    void ExpandCellPr(System::SharedPtr<Aspose::Words::Tables::CellPr> dstCellPr);
    System::SharedPtr<System::Object> GetAttr(int32_t key, System::SharedPtr<Aspose::Words::Tables::Cell> cell, Aspose::Words::AttrCollectionType type);
    System::SharedPtr<Aspose::Words::ConditionalStyle> NextConditionalStyle(System::SharedPtr<Aspose::Words::ConditionalStyle> conditionalStyle);
    static int32_t TransformCellBorderKey(Aspose::Words::TableStyleOverrideType type, System::SharedPtr<Aspose::Words::Tables::Cell> cell, int32_t key);
    System::SharedPtr<Aspose::Words::WordAttrCollection> GetCollection(Aspose::Words::AttrCollectionType type);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
