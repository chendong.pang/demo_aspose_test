//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Styles/ConditionalStyle.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/sorted_list.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Styles/ConditionalStyleType.h"
#include "Aspose.Words.Cpp/Model/Formatting/IShadingAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRunAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IParaAttrSource.h"
#include "Aspose.Words.Cpp/Model/Formatting/IBorderAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace System { class ObjectExt; }
namespace Aspose { namespace Words { class ConditionalStyleCollection; } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class DuplicateStyleRemover; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class TablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class RunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Tables { class CnfExpanderRunPr; } } }
namespace Aspose { namespace Words { namespace Tables { class CnfExpanderParaPr; } } }
namespace Aspose { namespace Words { namespace Tables { class CnfExpanderCellPr; } } }
namespace Aspose { namespace Words { namespace Tables { class CnfExpanderRowPr; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class TableFontLookup; } } } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfStylesheetHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { enum class TableStyleOverrideType; } }
namespace Aspose { namespace Words { enum class BorderType; } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class Shading; } }
namespace Aspose { namespace Words { class BorderCollection; } }
namespace Aspose { namespace Words { class ParaPr; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { enum class AttrCollectionType; } }
namespace Aspose { namespace Words { class WordAttrCollection; } }

namespace Aspose {

namespace Words {

/// Represents special formatting applied to some area of a table with assigned table style.
class ASPOSE_WORDS_SHARED_CLASS ConditionalStyle FINAL : public Aspose::Words::IBorderAttrSource, public Aspose::Words::IShadingAttrSource, public Aspose::Words::IParaAttrSource, public Aspose::Words::IRunAttrSource
{
    typedef ConditionalStyle ThisType;
    typedef Aspose::Words::IBorderAttrSource BaseType;
    typedef Aspose::Words::IShadingAttrSource BaseType1;
    typedef Aspose::Words::IParaAttrSource BaseType2;
    typedef Aspose::Words::IRunAttrSource BaseType3;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2, BaseType3> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class System::ObjectExt;
    friend class Aspose::Words::ConditionalStyleCollection;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::Validation::DuplicateStyleRemover;
    friend class Aspose::Words::RW::Doc::Reader::TablePrReader;
    friend class Aspose::Words::RW::Doc::Writer::ParaPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::RunPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Tables::CnfExpanderRunPr;
    friend class Aspose::Words::Tables::CnfExpanderParaPr;
    friend class Aspose::Words::Tables::CnfExpanderCellPr;
    friend class Aspose::Words::Tables::CnfExpanderRowPr;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::RW::Doc::TableFontLookup;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfStylesheetHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfStylesheetWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlStylesReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::SortedList<Aspose::Words::BorderType, int32_t>> get_PossibleBorderKeys() override;
    /// Gets the paragraph formatting of the conditional style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphFormat> get_ParagraphFormat();
    /// Gets the character formatting of the conditional style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Gets a <see cref="Aspose::Words::Shading">Shading</see> object that refers to the shading formatting for this conditional style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Shading> get_Shading();
    /// Gets the collection of default cell borders for the conditional style.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BorderCollection> get_Borders();
    /// Gets the amount of space (in points) to add to the left of the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_LeftPadding();
    /// Sets the amount of space (in points) to add to the left of the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_LeftPadding(double value);
    /// Gets the amount of space (in points) to add to the right of the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_RightPadding();
    /// Sets the amount of space (in points) to add to the right of the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_RightPadding(double value);
    /// Gets the amount of space (in points) to add above the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_TopPadding();
    /// Sets the amount of space (in points) to add above the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_TopPadding(double value);
    /// Gets the amount of space (in points) to add below the contents of table cells.
    ASPOSE_WORDS_SHARED_API double get_BottomPadding();
    /// Sets the amount of space (in points) to add below the contents of table cells.
    ASPOSE_WORDS_SHARED_API void set_BottomPadding(double value);
    /// Gets table area to which this conditional style relates.
    ASPOSE_WORDS_SHARED_API Aspose::Words::ConditionalStyleType get_Type();

    /// Clears formatting of this conditional style.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Calculates hash code for this object.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetBorderAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedShadingAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetParaAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearParaAttrs() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchParaAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;

protected:

    Aspose::Words::TableStyleOverrideType get_OverrideType() const;
    System::SharedPtr<Aspose::Words::ParaPr> get_ParaPr();
    void set_ParaPr(System::SharedPtr<Aspose::Words::ParaPr> value);
    bool get_HasParagraphFormatting() const;
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr();
    void set_RunPr(System::SharedPtr<Aspose::Words::RunPr> value);
    bool get_HasRunFormatting() const;
    System::SharedPtr<Aspose::Words::Tables::TablePr> get_TablePr();
    void set_TablePr(System::SharedPtr<Aspose::Words::Tables::TablePr> value);
    bool get_HasTableFormatting() const;
    System::SharedPtr<Aspose::Words::Tables::TablePr> get_RowPr();
    void set_RowPr(System::SharedPtr<Aspose::Words::Tables::TablePr> value);
    bool get_HasRowFormatting() const;
    System::SharedPtr<Aspose::Words::Tables::CellPr> get_CellPr();
    void set_CellPr(System::SharedPtr<Aspose::Words::Tables::CellPr> value);
    bool get_HasCellFormatting() const;
    bool get_HasFormatting();

    ConditionalStyle(Aspose::Words::TableStyleOverrideType overrideType);
    ConditionalStyle(Aspose::Words::TableStyleOverrideType overrideType, System::SharedPtr<Aspose::Words::TableStyle> parentStyle);

    System::SharedPtr<Aspose::Words::ConditionalStyle> Clone();
    bool Equals(System::SharedPtr<Aspose::Words::ConditionalStyle> style);
    void SetParentStyle(System::SharedPtr<Aspose::Words::TableStyle> value);
    System::SharedPtr<Aspose::Words::WordAttrCollection> GetCollection(Aspose::Words::AttrCollectionType type);

    virtual ASPOSE_WORDS_SHARED_API ~ConditionalStyle();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::TableStyleOverrideType mOverrideType;
    System::SharedPtr<Aspose::Words::ParaPr> mParaPr;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    System::SharedPtr<Aspose::Words::Tables::TablePr> mTablePr;
    System::SharedPtr<Aspose::Words::Tables::TablePr> mRowPr;
    System::SharedPtr<Aspose::Words::Tables::CellPr> mCellPr;
    System::WeakPtr<Aspose::Words::TableStyle> mParentStyle;
    System::SharedPtr<Aspose::Words::ParagraphFormat> mParagraphFormatCache;
    System::SharedPtr<Aspose::Words::Font> mFontCache;
    System::SharedPtr<Aspose::Words::BorderCollection> mBordersCache;

    System::SharedPtr<System::Object> GetInheritedConditionalStyleAttr(Aspose::Words::AttrCollectionType type, int32_t key);
    System::SharedPtr<System::Object> FetchInheritedCellAttr(int32_t key);
    System::SharedPtr<System::Object> GetCellAttr(int32_t key);

};

}
}
