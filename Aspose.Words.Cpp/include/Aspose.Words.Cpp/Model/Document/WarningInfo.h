//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/WarningInfo.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>

#include "Aspose.Words.Cpp/Model/Document/WarningType.h"
#include "Aspose.Words.Cpp/Model/Document/WarningSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class PenFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace WordArt { class VmlWordArtBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Chm { namespace Reader { class ChmReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace Validation { class CustomXmlValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlNegativeMarginEliminator; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlAsFormFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class ImageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class OfficeMathToShapeConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverterUtil; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class UnsupportedShapeDetector; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartFormat; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Extrusion { class ExtrusionRendererFactory; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Extrusion { class PseudoExtrusionShapeBuilder; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Wrapping { class TightWrapInfo; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class WarningGenerator; } } } }
namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlVmlShapeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MathML { class MathMLReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class StyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class StoryRevisionStack; } } } } }
namespace Aspose { namespace Words { class WarningUtil; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { class WarningCallbackCoreAdapter; } }
namespace Aspose { namespace Words { namespace Validation { class AnnotationValidator; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Validation { class BookmarkValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class FieldValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class Iso29500ComplianceEnforcer; } } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageWarningCallbackBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Svg { namespace Reader { class SvgReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { class MarkupResolver; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCustomXmlPartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlCommon { class HtmlResourceLoader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Signatures { class SignatureReader; } } } } }
namespace Aspose { namespace Words { class SectPr; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtEnum; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsRecord; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class DocxBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Warnings { enum class WarningTypeCore; } }
namespace Aspose { namespace Warnings { enum class WarningSourceCore; } }

namespace Aspose {

namespace Words {

/// Contains information about a warning that Aspose.Words issued during document loading or saving.
/// 
/// You do not create instances of this class. Objects of this class are created
/// and passed by Aspose.Words to the <see cref="Aspose::Words::IWarningCallback::Warning(System::SharedPtr<Aspose::Words::WarningInfo>)">Warning()</see> method.
/// 
/// @sa Aspose::Words::IWarningCallback
class ASPOSE_WORDS_SHARED_CLASS WarningInfo : public System::Object
{
    typedef WarningInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Shapes::PenFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::WordArt::VmlWordArtBuilder;
    friend class Aspose::Words::RW::Chm::Reader::ChmReader;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::Validation::CustomXmlValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlNegativeMarginEliminator;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlAsFormFieldReader;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::ImageShapeWriter;
    friend class Aspose::Words::Validation::OfficeMathToShapeConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverterUtil;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Validation::UnsupportedShapeDetector;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartFormat;
    friend class Aspose::Words::ApsBuilder::Shapes::Extrusion::ExtrusionRendererFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::Extrusion::PseudoExtrusionShapeBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::Wrapping::TightWrapInfo;
    friend class Aspose::Words::Layout::Core::WarningGenerator;
    friend class Aspose::Words::Fonts::DocumentFontProvider;
    friend class Aspose::Words::RW::Html::Reader::HtmlVmlShapeReader;
    friend class Aspose::Words::RW::MathML::MathMLReader;
    friend class Aspose::Words::RW::Doc::Reader::StyleReader;
    friend class Aspose::Words::RW::Nrx::Reader::StoryRevisionStack;
    friend class Aspose::Words::WarningUtil;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::WarningCallbackCoreAdapter;
    friend class Aspose::Words::Validation::AnnotationValidator;
    friend class Aspose::Words::Style;
    friend class Aspose::Words::Validation::BookmarkValidator;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::FieldValidator;
    friend class Aspose::Words::Validation::Iso29500ComplianceEnforcer;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageWarningCallbackBase;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigReader;
    friend class Aspose::Words::RW::Svg::Reader::SvgReaderContext;
    friend class Aspose::Words::RW::MarkupResolver;
    friend class Aspose::Words::RW::Docx::Reader::DocxCustomXmlPartReader;
    friend class Aspose::Words::RW::HtmlCommon::HtmlResourceLoader;
    friend class Aspose::Words::RW::Doc::Signatures::SignatureReader;
    friend class Aspose::Words::SectPr;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Odt::OdtEnum;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::Model::Nrx::NrxXmlReader;
    friend class Aspose::Words::RW::Doc::Escher::EsRecord;
    friend class Aspose::Words::RW::Nrx::Writer::DocxBuilder;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Returns the type of the warning.
    ASPOSE_WORDS_SHARED_API Aspose::Words::WarningType get_WarningType() const;
    /// Returns the description of the warning.
    ASPOSE_WORDS_SHARED_API System::String get_Description() const;
    /// Returns the source of the warning.
    ASPOSE_WORDS_SHARED_API Aspose::Words::WarningSource get_Source() const;

protected:

    WarningInfo(Aspose::Words::WarningType warningType, Aspose::Words::WarningSource source, System::String description);

    static Aspose::Words::WarningType ConvertWarningTypeCoreToWarningType(Aspose::Warnings::WarningTypeCore warningTypeCore);
    static Aspose::Words::WarningSource ConvertWarningSourceCoreToWarningSource(Aspose::Warnings::WarningSourceCore warningSourceCore);
    static System::String WarningSourceToString(Aspose::Words::WarningSource warningSource);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::WarningType mWarningType;
    Aspose::Words::WarningSource mSource;
    System::String mDescription;

};

}
}
