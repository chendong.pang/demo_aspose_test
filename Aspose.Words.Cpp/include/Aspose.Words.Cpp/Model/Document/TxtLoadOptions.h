//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/TxtLoadOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Document/TxtTrailingSpacesOptions.h"
#include "Aspose.Words.Cpp/Model/Document/TxtLeadingSpacesOptions.h"
#include "Aspose.Words.Cpp/Model/Document/LoadOptions.h"
#include "Aspose.Words.Cpp/Model/Document/DocumentDirection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }

namespace Aspose {

namespace Words {

/// Allows to specify additional options when loading <see cref="Aspose::Words::LoadFormat::Text">Text</see> document into a <see cref="Aspose::Words::Document">Document</see> object.
class ASPOSE_WORDS_SHARED_CLASS TxtLoadOptions : public Aspose::Words::LoadOptions
{
    typedef TxtLoadOptions ThisType;
    typedef Aspose::Words::LoadOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;

public:

    /// Allows to specify how numbered list items are recognized when document is imported from plain text format.
    /// The default value is true.
    /// 
    /// If this option is set to false, lists recognition algorithm detects list paragraphs, when list numbers ends with
    /// either dot, right bracket or bullet symbols (such as "•", "*", "-" or "o").
    /// 
    /// If this option is set to true, whitespaces are also used as list number delimeters:
    /// list recognition algorithm for Arabic style numbering (1., 1.1.2.) uses both whitespaces and dot (".") symbols.
    ASPOSE_WORDS_SHARED_API bool get_DetectNumberingWithWhitespaces() const;
    /// Setter for Aspose::Words::TxtLoadOptions::get_DetectNumberingWithWhitespaces
    ASPOSE_WORDS_SHARED_API void set_DetectNumberingWithWhitespaces(bool value);
    /// Gets preferred option of a trailing space handling.
    /// Default value is <see cref="Aspose::Words::TxtTrailingSpacesOptions::Trim">Trim</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TxtTrailingSpacesOptions get_TrailingSpacesOptions() const;
    /// Sets preferred option of a trailing space handling.
    /// Default value is <see cref="Aspose::Words::TxtTrailingSpacesOptions::Trim">Trim</see>.
    ASPOSE_WORDS_SHARED_API void set_TrailingSpacesOptions(Aspose::Words::TxtTrailingSpacesOptions value);
    /// Gets preferred option of a leading space handling.
    /// Default value is <see cref="Aspose::Words::TxtLeadingSpacesOptions::ConvertToIndent">ConvertToIndent</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::TxtLeadingSpacesOptions get_LeadingSpacesOptions() const;
    /// Sets preferred option of a leading space handling.
    /// Default value is <see cref="Aspose::Words::TxtLeadingSpacesOptions::ConvertToIndent">ConvertToIndent</see>.
    ASPOSE_WORDS_SHARED_API void set_LeadingSpacesOptions(Aspose::Words::TxtLeadingSpacesOptions value);
    /// Gets a document direction.
    /// The default value is <see cref="Aspose::Words::DocumentDirection::LeftToRight">LeftToRight</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::DocumentDirection get_DocumentDirection() const;
    /// Sets a document direction.
    /// The default value is <see cref="Aspose::Words::DocumentDirection::LeftToRight">LeftToRight</see>.
    ASPOSE_WORDS_SHARED_API void set_DocumentDirection(Aspose::Words::DocumentDirection value);

    /// Initializes a new instance of this class with default values.
    ASPOSE_WORDS_SHARED_API TxtLoadOptions();

protected:

    TxtLoadOptions(System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mDetectNumberingWithWhitespaces;
    Aspose::Words::TxtLeadingSpacesOptions mLeadingSpacesOptions;
    Aspose::Words::TxtTrailingSpacesOptions mTrailingSpacesOptions;
    Aspose::Words::DocumentDirection mDocumentDirection;

};

}
}
