//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/DocumentReaderPluginLoadException.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/runtime/serialization/streaming_context.h>
#include <system/runtime/serialization/serialization_info.h>
#include <system/object.h>
#include <system/exceptions.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Pdf { namespace Reader { class PdfReader; } } } } }

namespace Aspose {

namespace Words {

class Details_DocumentReaderPluginLoadException;
/// Thrown during document load, when the plugin required for reading the document format cannot be loaded.
using DocumentReaderPluginLoadException = System::ExceptionWrapper<Details_DocumentReaderPluginLoadException>;

/// Thrown during document load, when the plugin required for reading the document format cannot be loaded.
class ASPOSE_WORDS_SHARED_CLASS Details_DocumentReaderPluginLoadException : public System::Details_Exception
{
    typedef Details_DocumentReaderPluginLoadException ThisType;
    typedef System::Details_Exception BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Pdf::Reader::PdfReader;
    friend class System::ExceptionWrapperHelper;

protected:

    [[noreturn]] void DoThrow(const System::ExceptionPtr& self) const override;

    Details_DocumentReaderPluginLoadException(System::String message);
    Details_DocumentReaderPluginLoadException(System::String message, System::Exception innerException);
    ASPOSE_WORDS_SHARED_API Details_DocumentReaderPluginLoadException(System::SharedPtr<System::Runtime::Serialization::SerializationInfo> info, System::Runtime::Serialization::StreamingContext context);
    ASPOSE_WORDS_SHARED_API Details_DocumentReaderPluginLoadException();

};

}
}
