//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/HtmlLoadOptions.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Document/LoadOptions.h"
#include "Aspose.Words.Cpp/Model/Document/LoadFormat.h"
#include "Aspose.Words.Cpp/Model/Document/HtmlControlType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Reader { class MhtmlDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }

namespace Aspose {

namespace Words {

/// Allows to specify additional options when loading HTML document into a <see cref="Aspose::Words::Document">Document</see> object.
class ASPOSE_WORDS_SHARED_CLASS HtmlLoadOptions : public Aspose::Words::LoadOptions
{
    typedef HtmlLoadOptions ThisType;
    typedef Aspose::Words::LoadOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Html::Reader::HtmlDocumentReader;
    friend class Aspose::Words::RW::Mhtml::Reader::MhtmlDocumentReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;

public:

    /// Gets a value indicating whether to support VML images.
    ASPOSE_WORDS_SHARED_API bool get_SupportVml() const;
    /// Sets a value indicating whether to support VML images.
    ASPOSE_WORDS_SHARED_API void set_SupportVml(bool value);
    /// The number of milliseconds to wait before the web request times out. The default value is 100000 milliseconds
    /// (100 seconds).
    ASPOSE_WORDS_SHARED_API int32_t get_WebRequestTimeout() const;
    /// The number of milliseconds to wait before the web request times out. The default value is 100000 milliseconds
    /// (100 seconds).
    ASPOSE_WORDS_SHARED_API void set_WebRequestTimeout(int32_t value);
    /// Gets preffered type of document nodes that will represent imported \<input\> and \<select\> elements.
    /// Default value is <see cref="Aspose::Words::HtmlControlType::FormField">FormField</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::HtmlControlType get_PreferredControlType() const;
    /// Sets preffered type of document nodes that will represent imported \<input\> and \<select\> elements.
    /// Default value is <see cref="Aspose::Words::HtmlControlType::FormField">FormField</see>.
    ASPOSE_WORDS_SHARED_API void set_PreferredControlType(Aspose::Words::HtmlControlType value);

    /// Initializes a new instance of this class with default values.
    ASPOSE_WORDS_SHARED_API HtmlLoadOptions();
    /// A shortcut to initialize a new instance of this class with the specified password to load an encrypted document.
    /// 
    /// @param password The password to open an encrypted document. Can be null or empty string.
    ASPOSE_WORDS_SHARED_API HtmlLoadOptions(System::String password);
    /// A shortcut to initialize a new instance of this class with properties set to the specified values.
    /// 
    /// @param loadFormat The format of the document to be loaded.
    /// @param password The password to open an encrypted document. Can be null or empty string.
    /// @param baseUri The string that will be used to resolve relative URIs to absolute. Can be null or empty string.
    ASPOSE_WORDS_SHARED_API HtmlLoadOptions(Aspose::Words::LoadFormat loadFormat, System::String password, System::String baseUri);

protected:

    bool get_ApplyFormattingAsMsWord() const;
    void set_ApplyFormattingAsMsWord(bool value);

    HtmlLoadOptions(System::SharedPtr<Aspose::Words::HtmlLoadOptions> htmlLoadOptions);
    HtmlLoadOptions(System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::LoadOptions> Clone() override;

    virtual ASPOSE_WORDS_SHARED_API ~HtmlLoadOptions();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mSupportVml;
    int32_t mWebRequestTimeout;
    bool mApplyFormattingAsMsWord;
    Aspose::Words::HtmlControlType mPreferedControlType;

};

}
}
