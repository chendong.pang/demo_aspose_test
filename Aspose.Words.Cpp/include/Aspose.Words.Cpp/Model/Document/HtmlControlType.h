//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/HtmlControlType.h
#pragma once

#include <system/object_ext.h>
#include <system/enum.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

/// Type of document nodes that represent \<input\> and \<select\> elements imported from HTML.
enum class HtmlControlType
{
    FormField,
    StructuredDocumentTag
};

}
}

template<>
struct EnumMetaInfo<Aspose::Words::HtmlControlType>
{
    static const ASPOSE_WORDS_SHARED_API std::array<std::pair<Aspose::Words::HtmlControlType, const char_t*>, 2>& values();
};
