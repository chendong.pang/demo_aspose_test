//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/LoadOptions.h
#pragma once

#include <system/text/encoding.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Settings/MsWordVersion.h"
#include "Aspose.Words.Cpp/Model/Document/LoadFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class RtfLoadOptions; } }
namespace Aspose { namespace Words { class TxtLoadOptions; } }
namespace Aspose { namespace Words { class HtmlLoadOptions; } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfDestinationHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxCellReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxCellPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxParaPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { class PlainTextDocument; } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxFontTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxNumberingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxParaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxRowPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxRowReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxRunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxTablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfAttrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlFontsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlListsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRowPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRowReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlTablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlTableReader; } } } } }
namespace Aspose { namespace Words { enum class LoadMode; } }
namespace Aspose { namespace Words { namespace Loading { class IResourceLoadingCallback; } } }
namespace Aspose { namespace Words { class IWarningCallback; } }
namespace Aspose { namespace Words { namespace Fonts { class FontSettings; } } }
namespace Aspose { namespace Words { namespace Licensing { class VentureLicense; } } }
namespace Aspose { namespace Words { class LanguagePreferences; } }

namespace Aspose {

namespace Words {

/// Allows to specify additional options (such as password or base URI) when
/// loading a document into a <see cref="Aspose::Words::Document">Document</see> object.
class ASPOSE_WORDS_SHARED_CLASS LoadOptions : public System::Object
{
    typedef LoadOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RtfLoadOptions;
    friend class Aspose::Words::TxtLoadOptions;
    friend class Aspose::Words::HtmlLoadOptions;
    friend class Aspose::Words::RW::Nrx::Reader::NrxSettingsReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfDestinationHandler;
    friend class Aspose::Words::RW::Nrx::Reader::NrxCellReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxCellPrReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxParaPrReaderBase;
    friend class Aspose::Words::RW::Doc::Reader::DocDocumentReader;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::PlainTextDocument;
    friend class Aspose::Words::RW::Doc::Reader::DocReaderContext;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxFontTableReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxNumberingReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxParaReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxRowPrReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxRowReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxRunPrReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxTablePrReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxTableReader;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfAttrReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlFontsReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlListsReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlRowPrReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlRowReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlRunPrReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlStylesReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlTablePrReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlTableReader;

public:

    /// Specifies the format of the document to be loaded.
    /// Default is <see cref="Aspose::Words::LoadFormat::Auto">Auto</see>.
    /// 
    /// It is recommended that you specify the <see cref="Aspose::Words::LoadFormat::Auto">Auto</see> value and let Aspose.Words detect
    /// the file format automatically. If you know the format of the document you are about to load, you can specify the format
    /// explicitly and this will slightly reduce the loading time by the overhead associated with auto detecting the format.
    /// If you specify an explicit load format and it will turn out to be wrong, the auto detection will be invoked and a second
    /// attempt to load the file will be made.
    ASPOSE_WORDS_SHARED_API Aspose::Words::LoadFormat get_LoadFormat() const;
    /// Setter for Aspose::Words::LoadOptions::get_LoadFormat
    ASPOSE_WORDS_SHARED_API void set_LoadFormat(Aspose::Words::LoadFormat value);
    /// Gets or sets the password for opening an encrypted document.
    /// Can be null or empty string. Default is null.
    /// 
    /// You need to know the password to open an encrypted document. If the document is not encrypted, set this to null or empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Password() const;
    /// Setter for Aspose::Words::LoadOptions::get_Password
    ASPOSE_WORDS_SHARED_API void set_Password(System::String value);
    /// Gets or sets the string that will be used to resolve relative URIs found in the document into absolute URIs when required.
    /// Can be null or empty string. Default is null.
    /// 
    /// This property is used to resolve relative URIs into absolute in the following cases:
    /// 
    /// -# When loading an HTML document from a stream and the document contains images with
    ///    relative URIs and does not have a base URI specified in the BASE HTML element.
    /// -# When saving a document to PDF and other formats, to retrieve images linked using relative URIs
    ///    so the images can be saved into the output document.
    ASPOSE_WORDS_SHARED_API System::String get_BaseUri() const;
    /// Setter for Aspose::Words::LoadOptions::get_BaseUri
    ASPOSE_WORDS_SHARED_API void set_BaseUri(System::String value);
    /// Gets or sets the encoding that will be used to load an HTML, TXT, or CHM document if the encoding is not specified
    /// inside the document.
    /// Can be null. Default is null.
    /// 
    /// This property is used only when loading HTML, TXT, or CHM documents.
    /// 
    /// If encoding is not specified inside the document and this property is <c>null</c>, then the system will try to
    /// automatically detect the encoding.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Text::Encoding> get_Encoding() const;
    /// Setter for Aspose::Words::LoadOptions::get_Encoding
    ASPOSE_WORDS_SHARED_API void set_Encoding(System::SharedPtr<System::Text::Encoding> value);
    /// Allows to control how external resources (images, style sheets) are loaded when a document is imported from HTML, MHTML.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> get_ResourceLoadingCallback() const;
    /// Allows to control how external resources (images, style sheets) are loaded when a document is imported from HTML, MHTML.
    ASPOSE_WORDS_SHARED_API void set_ResourceLoadingCallback(System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> value);
    /// Called during a load operation, when an issue is detected that might result in data or formatting fidelity loss.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IWarningCallback> get_WarningCallback() const;
    /// Called during a load operation, when an issue is detected that might result in data or formatting fidelity loss.
    ASPOSE_WORDS_SHARED_API void set_WarningCallback(System::SharedPtr<Aspose::Words::IWarningCallback> value);
    /// Gets or sets whether to preserve the INCLUDEPICTURE field when reading Microsoft Word formats.
    /// The default value is false.
    /// 
    /// By default, the INCLUDEPICTURE field is converted into a shape object. You can override that if you need
    /// the field to be preserved, for example, if you wish to update it programmatically. Note however that this
    /// approach is not common for Aspose.Words. Use it on your own risk.
    /// 
    /// One of the possible use cases may be using a MERGEFIELD as a child field to dynamically change the source path
    /// of the picture. In this case you need the INCLUDEPICTURE to be preserved in the model.
    ASPOSE_WORDS_SHARED_API bool get_PreserveIncludePictureField() const;
    /// Setter for Aspose::Words::LoadOptions::get_PreserveIncludePictureField
    ASPOSE_WORDS_SHARED_API void set_PreserveIncludePictureField(bool value);
    /// Gets whether to convert shapes with EquationXML to Office Math objects.
    ASPOSE_WORDS_SHARED_API bool get_ConvertShapeToOfficeMath() const;
    /// Sets whether to convert shapes with EquationXML to Office Math objects.
    ASPOSE_WORDS_SHARED_API void set_ConvertShapeToOfficeMath(bool value);
    /// Allows to specify document font settings.
    /// 
    /// When loading some formats, Aspose.Words may require to resolve the fonts. For example, when loading HTML documents Aspose.Words
    /// may resolve the fonts to perform font fallback.
    /// 
    /// If set to null, default static font settings <see cref="Aspose::Words::Fonts::FontSettings::get_DefaultInstance">DefaultInstance</see> will be used.
    /// 
    /// The default value is null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::FontSettings> get_FontSettings() const;
    /// Setter for Aspose::Words::LoadOptions::get_FontSettings
    ASPOSE_WORDS_SHARED_API void set_FontSettings(System::SharedPtr<Aspose::Words::Fonts::FontSettings> value);
    /// Allows to use temporary files when reading document.
    /// By default this property is <c>null</c> and no temporary files are used.
    /// 
    /// The folder must exist and be writable, otherwise an exception will be thrown.
    /// 
    /// Aspose.Words automatically deletes all temporary files when reading is complete.
    ASPOSE_WORDS_SHARED_API System::String get_TempFolder() const;
    /// Setter for Aspose::Words::LoadOptions::get_TempFolder
    ASPOSE_WORDS_SHARED_API void set_TempFolder(System::String value);
    /// Gets whether to convert metafile (<see cref="Aspose::FileFormat::Wmf">Wmf</see> or <see cref="Aspose::FileFormat::Emf">Emf</see>) images to <see cref="Aspose::FileFormat::Png">Png</see> image format.
    ASPOSE_WORDS_SHARED_API bool get_ConvertMetafilesToPng() const;
    /// Sets whether to convert metafile (<see cref="Aspose::FileFormat::Wmf">Wmf</see> or <see cref="Aspose::FileFormat::Emf">Emf</see>) images to <see cref="Aspose::FileFormat::Png">Png</see> image format.
    ASPOSE_WORDS_SHARED_API void set_ConvertMetafilesToPng(bool value);
    /// Allows to specify that the document loading process should match a specific MS Word version.
    /// Default value is <see cref="Aspose::Words::Settings::MsWordVersion::Word2007">Word2007</see>
    ASPOSE_WORDS_SHARED_API Aspose::Words::Settings::MsWordVersion get_MswVersion() const;
    /// Allows to specify that the document loading process should match a specific MS Word version.
    /// Default value is <see cref="Aspose::Words::Settings::MsWordVersion::Word2007">Word2007</see>
    ASPOSE_WORDS_SHARED_API void set_MswVersion(Aspose::Words::Settings::MsWordVersion value);
    /// Specifies whether to update the fields with the <c>dirty</c> attribute.
    ASPOSE_WORDS_SHARED_API bool get_UpdateDirtyFields() const;
    /// Specifies whether to update the fields with the <c>dirty</c> attribute.
    ASPOSE_WORDS_SHARED_API void set_UpdateDirtyFields(bool value);
    /// Gets language preferences that will be used when document is loading.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::LanguagePreferences> get_LanguagePreferences() const;

    /// Initializes a new instance of this class with default values.
    ASPOSE_WORDS_SHARED_API LoadOptions();
    /// A shortcut to initialize a new instance of this class with the specified password to load an encrypted document.
    /// 
    /// @param password The password to open an encrypted document. Can be null or empty string.
    ASPOSE_WORDS_SHARED_API LoadOptions(System::String password);
    /// A shortcut to initialize a new instance of this class with properties set to the specified values.
    /// 
    /// @param loadFormat The format of the document to be loaded.
    /// @param password The password to open an encrypted document. Can be null or empty string.
    /// @param baseUri The string that will be used to resolve relative URIs to absolute. Can be null or empty string.
    ASPOSE_WORDS_SHARED_API LoadOptions(Aspose::Words::LoadFormat loadFormat, System::String password, System::String baseUri);

protected:

    Aspose::Words::LoadMode get_LoadMode() const;
    void set_LoadMode(Aspose::Words::LoadMode value);
    bool get_SkipFormatting();
    System::SharedPtr<Aspose::Words::Licensing::VentureLicense> get_VentureLicense() const;
    void set_VentureLicense(System::SharedPtr<Aspose::Words::Licensing::VentureLicense> value);
    bool get_ReadDocTheme() const;
    void set_ReadDocTheme(bool value);

    LoadOptions(System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);

    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::LoadOptions> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~LoadOptions();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::LoadFormat mLoadFormat;
    System::String mPassword;
    System::String mBaseUri;
    System::SharedPtr<System::Text::Encoding> mEncoding;
    System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> mResourceLoadingCallback;
    System::SharedPtr<Aspose::Words::IWarningCallback> mWarningCallback;
    bool mPreserveIncludePictureField;
    bool mAllowTrailingWhitespaceForListItems;
    System::SharedPtr<Aspose::Words::Fonts::FontSettings> mFontSettings;
    Aspose::Words::LoadMode mLoadMode;
    System::SharedPtr<Aspose::Words::Licensing::VentureLicense> mVentureLicense;
    bool mUpdateDirtyFields;
    bool mReadDocTheme;
    System::String mTempFolder;
    bool mConvertShapeToOfficeMath;
    Aspose::Words::Settings::MsWordVersion mMsWordVersion;
    System::SharedPtr<Aspose::Words::LanguagePreferences> mLanguagePreferences;
    bool mConvertMetafilesToPng;

};

}
}
