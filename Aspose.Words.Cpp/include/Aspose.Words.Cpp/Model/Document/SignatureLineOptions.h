//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/SignatureLineOptions.h
#pragma once

#include <system/string.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

/// Allows to specify options for signature line being inserted. Used in <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see>.
class ASPOSE_WORDS_SHARED_CLASS SignatureLineOptions : public System::Object
{
    typedef SignatureLineOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets suggested signer of the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Signer() const;
    /// Sets suggested signer of the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Signer(System::String value);
    /// Gets suggested signer's title.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_SignerTitle() const;
    /// Sets suggested signer's title.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_SignerTitle(System::String value);
    /// Gets suggested signer's e-mail address.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Email() const;
    /// Sets suggested signer's e-mail address.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Email(System::String value);
    /// Gets a value indicating that default instructions is shown in the Sign dialog.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_DefaultInstructions() const;
    /// Sets a value indicating that default instructions is shown in the Sign dialog.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_DefaultInstructions(bool value);
    /// Gets instructions to the signer that are displayed on signing the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Instructions() const;
    /// Sets instructions to the signer that are displayed on signing the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Instructions(System::String value);
    /// Gets a value indicating that the signer can add comments in the Sign dialog.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_AllowComments() const;
    /// Sets a value indicating that the signer can add comments in the Sign dialog.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_AllowComments(bool value);
    /// Gets a value indicating that sign date is shown in the signature line.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowDate() const;
    /// Sets a value indicating that sign date is shown in the signature line.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowDate(bool value);

    ASPOSE_WORDS_SHARED_API SignatureLineOptions();

private:

    System::String mSigner;
    System::String mSignerTitle;
    System::String mEmail;
    bool mDefaultInstructions;
    System::String mInstructions;
    bool mAllowComments;
    bool mShowDate;

};

}
}
