//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/DocumentBase.h
#pragma once

#include <system/memory_management.h>
#include <system/enum_helpers.h>
#include <drawing/color.h>

#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Importing/ImportFormatMode.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramTextBoxFitCache; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathContext; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class PageLayout; } } } }
namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlShapeInserter; } } } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceLegacy; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { class NodeCollection; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace ShapeEffects { class DmlEffectRenderingContext; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace TextEffects { class DmlTextEffectsPostApplier; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlRenderingServiceLocator; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { namespace Layout { class DmlTextFontRepository; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace WordArt { class VmlWordArtBuilder; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanListRevisionsHelper; } } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class DocumentComparer; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class ComplexScriptRunUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { class VideoInserter; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class SignatureLineRenderingHelper; } } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { class FormatRevisionText; } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlAsSdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownEmphasesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfRsidTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxBackgroundReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxPeopleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxSectPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxPeopleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRunPrWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxFootnotesWriter; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { class SuspendTrackRevisionsDocument; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Diagrams { namespace ComplexTypes { class DmlShapeProperties; } } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Rendering { class NodeRendererBase; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartTitle; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlPictureRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace ShapeEffects { class DmlSimplifiedBlurHelper; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathElement; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { class BookmarkCache; } }
namespace Aspose { namespace Words { class EditableRangeStart; } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace Validation { class DrawingMLIdValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTrackedChangesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTrackedChangesWriter; } } } } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanShape; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentRunSplitter; } } }
namespace Aspose { namespace Words { namespace Layout { class FontProps; } } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class GlossaryDocument; } } }
namespace Aspose { namespace Words { namespace Validation { class AnnotationValidator; } } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class LegacyListFormattingConverter; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeIdGenerator; } } }
namespace Aspose { namespace Words { class WordCounter; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeBoundsFinder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { class FixedPageWriterBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class ListProps; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlSpanWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfPgpHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfPgpWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class TextBox; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUtil; } } }
namespace Aspose { namespace Words { class ImportContext; } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { class Comment; } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class Range; } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace Rendering { class AsposeWordsPrintDocument; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMailMergeHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMailMergeODSOHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfMathPropertiesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtHeaderFooterReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtPageLayoutPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtAutomaticStylesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtContentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Celler { class CellerTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxFootnotesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSdtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxWebSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFontTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxWebSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Comments { class FileComments; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPrFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSectionWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfDocPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFileHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFontTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfInfoGroupHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfHeaderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfInfoGroupWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfParaPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfRevisionWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlDocPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFontsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlListWriter; } } } } }
namespace Aspose { namespace Words { class INodeChangingCallback; } }
namespace Aspose { namespace Words { namespace Loading { class IResourceLoadingCallback; } } }
namespace CsToCppPorter { class MemoryManagement; }
namespace Aspose { namespace Words { namespace Fonts { class FontInfoCollection; } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class HtmlBlockCollection; } }
namespace Aspose { namespace Words { class PersonCollection; } }
namespace Aspose { namespace Words { class IWarningCallback; } }
namespace Aspose { namespace Words { namespace Settings { class DocPr; } } }
namespace Aspose { namespace Words { class VariableCollection; } }
namespace Aspose { namespace Words { class FootnoteSeparatorCollection; } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtPlaceholderManager; } } }
namespace Aspose { namespace Words { class UniqueIdManager; } }
namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { class NodeChangingCallbackChain; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class NodeChangingArgs; } }
namespace Aspose { namespace Words { enum class NodeChangingAction; } }
namespace Aspose { namespace Words { enum class WarningType; } }
namespace Aspose { namespace Words { enum class WarningSource; } }
namespace Aspose { namespace Words { namespace Themes { class Theme; } } }

namespace Aspose {

namespace Words {

/// Provides the abstract base class for a main document and a glossary document of a Word document.
/// 
/// Aspose.Words represents a Word document as a tree of nodes. <see cref="Aspose::Words::DocumentBase">DocumentBase</see> is a
/// root node of the tree that contains all other nodes of the document.
/// 
/// <see cref="Aspose::Words::DocumentBase">DocumentBase</see> also stores document-wide information such as <see cref="Aspose::Words::DocumentBase::get_Styles">Styles</see> and
/// <see cref="Aspose::Words::DocumentBase::get_Lists">Lists</see> that the tree nodes might refer to.
/// 
/// @sa Aspose::Words::Document
/// @sa Aspose::Words::DocumentBase
class ASPOSE_WORDS_SHARED_CLASS DocumentBase : public Aspose::Words::CompositeNode
{
    typedef DocumentBase ThisType;
    typedef Aspose::Words::CompositeNode BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramTextBoxFitCache;
    friend class Aspose::Words::ApsBuilder::Math::MathContext;
    friend class Aspose::Words::Layout::PreAps::PageLayout;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Drawing::Core::Dml::DmlShapeInserter;
    friend class Aspose::Words::Replacing::FindReplaceLegacy;
    friend class Aspose::Words::Node;
    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::NodeCollection;
    friend class Aspose::Words::ApsBuilder::Dml::ShapeEffects::DmlEffectRenderingContext;
    friend class Aspose::Words::ApsBuilder::Dml::TextEffects::DmlTextEffectsPostApplier;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::ApsBuilder::Dml::DmlRenderingServiceLocator;
    friend class Aspose::Words::ApsBuilder::Dml::Text::Layout::DmlTextFontRepository;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Shapes::WordArt::VmlWordArtBuilder;
    friend class Aspose::Words::Layout::Core::SpanListRevisionsHelper;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::DocumentComparer;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::Validation::ComplexScriptRunUpdater;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::CompositeNode;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::VideoInserter;
    friend class Aspose::Words::Drawing::Core::SignatureLineRenderingHelper;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::FormatRevisionText;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlAsSdtReader;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownEmphasesWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxSettingsReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfRsidTableHandler;
    friend class Aspose::Words::RW::Docx::Reader::DocxBackgroundReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxPeopleReader;
    friend class Aspose::Words::RW::Nrx::Reader::NrxSectPrReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxPeopleWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRunPrWriterBase;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxFootnotesWriter;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::SuspendTrackRevisionsDocument;
    friend class Aspose::Words::Drawing::Core::Dml::Diagrams::ComplexTypes::DmlShapeProperties;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Rendering::NodeRendererBase;
    friend class Aspose::Words::Drawing::Charts::ChartTitle;
    friend class Aspose::Words::ApsBuilder::Dml::DmlPictureRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::ShapeEffects::DmlSimplifiedBlurHelper;
    friend class Aspose::Words::ApsBuilder::Math::MathElement;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::BookmarkCache;
    friend class Aspose::Words::EditableRangeStart;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::Validation::DrawingMLIdValidator;
    friend class Aspose::Words::RW::Odt::Reader::OdtTrackedChangesReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtTrackedChangesWriter;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::Layout::Core::SpanShape;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentRunSplitter;
    friend class Aspose::Words::Layout::FontProps;
    friend class Aspose::Words::BuildingBlocks::GlossaryDocument;
    friend class Aspose::Words::Validation::AnnotationValidator;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::LegacyListFormattingConverter;
    friend class Aspose::Words::Validation::RunPrValidator;
    friend class Aspose::Words::Validation::ShapeIdGenerator;
    friend class Aspose::Words::WordCounter;
    friend class Aspose::Words::Drawing::ShapeBase;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeBoundsFinder;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;
    friend class Aspose::Words::RW::FixedPageWriterBase;
    friend class Aspose::Words::RW::Html::ListProps;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlSpanWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxSettingsWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfPgpHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfPgpWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;
    friend class Aspose::Words::Drawing::TextBox;
    friend class Aspose::Words::Lists::ListLabelUtil;
    friend class Aspose::Words::ImportContext;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::MailMerging::MailMerge;
    friend class Aspose::Words::Section;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::Comment;
    friend class Aspose::Words::Font;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::Range;
    friend class Aspose::Words::Run;
    friend class Aspose::Words::Rendering::AsposeWordsPrintDocument;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMailMergeHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMailMergeODSOHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfMathPropertiesHandler;
    friend class Aspose::Words::RW::Vml::VmlShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtHeaderFooterReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtPageLayoutPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphPropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtSettingsReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtAutomaticStylesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtContentReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtReader;
    friend class Aspose::Words::RW::Celler::CellerTable;
    friend class Aspose::Words::RW::Docx::Reader::DocxFootnotesReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxSdtReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxSettingsReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxWebSettingsReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxFontTableWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxRunWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxWebSettingsWriter;
    friend class Aspose::Words::RW::Doc::Comments::FileComments;
    friend class Aspose::Words::RW::Doc::DocPrFiler;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSectionWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtStylesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSettingsWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfDocPrReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFileHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFontTableHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfInfoGroupHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfHeaderWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfInfoGroupWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfParaPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfRevisionWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlDocPrReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlDocPrWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlFontsWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlListWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() const override;
    /// Called when a node is inserted or removed in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::INodeChangingCallback> get_NodeChangingCallback();
    /// Called when a node is inserted or removed in the document.
    ASPOSE_WORDS_SHARED_API void set_NodeChangingCallback(System::SharedPtr<Aspose::Words::INodeChangingCallback> value);
    /// Allows to control how external resources are loaded.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> get_ResourceLoadingCallback() const;
    /// Allows to control how external resources are loaded.
    ASPOSE_WORDS_SHARED_API void set_ResourceLoadingCallback(System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> value);
    /// Provides access to properties of fonts used in this document.
    /// 
    /// This collection of font definitions is loaded as is from the document.
    /// Font definitions might be optional, missing or incomplete in some documents.
    /// 
    /// Do not rely on this collection to ascertain that a particular font is used in the document.
    /// You should only use this collection to get information about fonts that might be used in the document.
    /// 
    /// @sa Aspose::Words::Fonts::FontInfoCollection
    /// @sa Aspose::Words::Fonts::FontInfo
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> get_FontInfos() const;
    /// Returns a collection of styles defined in the document.
    /// 
    /// For more information see the description of the <see cref="Aspose::Words::StyleCollection">StyleCollection</see> class.
    /// 
    /// @sa Aspose::Words::StyleCollection
    /// @sa Aspose::Words::Style
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::StyleCollection> get_Styles() const;
    /// Provides access to the list formatting used in the document.
    /// 
    /// For more information see the description of the <see cref="Aspose::Words::Lists::ListCollection">ListCollection</see> class.
    /// 
    /// @sa Aspose::Words::Lists::ListCollection
    /// @sa Aspose::Words::Lists::List
    /// @sa Aspose::Words::ListFormat
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Lists::ListCollection> get_Lists() const;
    /// Called during various document processing procedures when an issue is detected that might result
    /// in data or formatting fidelity loss.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IWarningCallback> get_WarningCallback() const;
    /// Called during various document processing procedures when an issue is detected that might result
    /// in data or formatting fidelity loss.
    ASPOSE_WORDS_SHARED_API void set_WarningCallback(System::SharedPtr<Aspose::Words::IWarningCallback> value);
    /// Gets or sets the background shape of the document. Can be null.
    /// 
    /// Microsoft Word allows only a shape that has its <see cref="Aspose::Words::Drawing::ShapeBase::get_ShapeType">ShapeType</see> property equal
    /// to <see cref="Aspose::Words::Drawing::ShapeType::Rectangle">Rectangle</see> to be used as a background shape for a document.
    /// 
    /// Microsoft Word supports only the fill properties of a background shape. All other properties
    /// are ignored.
    /// 
    /// Setting this property to a non-null value will also set the <see cref="Aspose::Words::Settings::ViewOptions::get_DisplayBackgroundShape">DisplayBackgroundShape</see> to true.
    /// 
    /// @sa Aspose::Words::Settings::ViewOptions::get_DisplayBackgroundShape
    /// @sa Aspose::Words::DocumentBase::get_PageColor
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Shape> get_BackgroundShape() const;
    /// Setter for Aspose::Words::DocumentBase::get_BackgroundShape
    ASPOSE_WORDS_SHARED_API void set_BackgroundShape(System::SharedPtr<Aspose::Words::Drawing::Shape> value);
    /// Gets or sets the page color of the document. This property is a simpler version of <see cref="Aspose::Words::DocumentBase::get_BackgroundShape">BackgroundShape</see>.
    /// 
    /// This property provides a simple way to specify a solid page color for the document.
    /// Setting this property creates and sets an appropriate <see cref="Aspose::Words::DocumentBase::get_BackgroundShape">BackgroundShape</see>.
    /// 
    /// If the page color is not set (e.g. there is no background shape in the document) returns
    /// <see cref="System::Drawing::Color::Empty">Empty</see>.
    /// 
    /// @sa Aspose::Words::DocumentBase::get_BackgroundShape
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_PageColor();
    /// Setter for Aspose::Words::DocumentBase::get_PageColor
    ASPOSE_WORDS_SHARED_API void set_PageColor(System::Drawing::Color value);

    /// Imports a node from another document to the current document.
    /// 
    /// This method uses the <see cref="Aspose::Words::ImportFormatMode::UseDestinationStyles">UseDestinationStyles</see> option to resolve formatting.
    /// 
    /// Importing a node creates a copy of the source node belonging to the importing document.
    /// The returned node has no parent. The source node is not altered or removed from the original document.
    /// 
    /// Before a node from another document can be inserted into this document, it must be imported.
    /// During import, document-specific properties such as references to styles and lists are translated
    /// from the original to the importing document. After the node was imported, it can be inserted
    /// into the appropriate place in the document using <see cref="Aspose::Words::CompositeNode::InsertBefore(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertBefore()</see> or
    /// <see cref="Aspose::Words::CompositeNode::InsertAfter(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertAfter()</see>.
    /// 
    /// If the source node already belongs to the destination document, then simply a deep clone
    /// of the source node is created.
    /// 
    /// @param srcNode The node being imported.
    /// @param isImportChildren True to import all child nodes recursively; otherwise, false.
    /// 
    /// @return The cloned node that belongs to the current document.
    /// 
    /// @sa Aspose::Words::NodeImporter
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren);
    /// Imports a node from another document to the current document with an option to control formatting.
    /// 
    /// This overload is useful to control how styles and list formatting are imported.
    /// 
    /// Importing a node creates a copy of the source node belonging to the importing document.
    /// The returned node has no parent. The source node is not altered or removed from the original document.
    /// 
    /// Before a node from another document can be inserted into this document, it must be imported.
    /// During import, document-specific properties such as references to styles and lists are translated
    /// from the original to the importing document. After the node was imported, it can be inserted
    /// into the appropriate place in the document using <see cref="Aspose::Words::CompositeNode::InsertBefore(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertBefore()</see> or
    /// <see cref="Aspose::Words::CompositeNode::InsertAfter(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertAfter()</see>.
    /// 
    /// If the source node already belongs to the destination document, then simply a deep clone
    /// of the source node is created.
    /// 
    /// @param srcNode The node to imported.
    /// @param isImportChildren True to import all child nodes recursively; otherwise, false.
    /// @param importFormatMode Specifies how to merge style formatting that clashes.
    /// 
    /// @return The cloned, imported node. The node belongs to the destination document, but has no parent.
    /// 
    /// @sa Aspose::Words::ImportFormatMode
    /// @sa Aspose::Words::NodeImporter
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren, Aspose::Words::ImportFormatMode importFormatMode);

protected:

    virtual bool get_IsTrackRevisionsEnabled() = 0;
    int32_t get_TreeChangeCount() const;
    System::SharedPtr<Aspose::Words::Node> get_NullNode() const;
    System::SharedPtr<CsToCppPorter::MemoryManagement::ObjectsBag> get_HangingNodesCollection() const;
    System::SharedPtr<Aspose::Words::HtmlBlockCollection> get_HtmlBlockCollection() const;
    System::SharedPtr<Aspose::Words::PersonCollection> get_People() const;
    System::SharedPtr<Aspose::Words::Settings::DocPr> get_DocPr() const;
    System::SharedPtr<Aspose::Words::VariableCollection> get_Variables() const;
    System::SharedPtr<Aspose::Words::FootnoteSeparatorCollection> get_FootnoteSeparators() const;
    System::SharedPtr<Aspose::Words::Markup::SdtPlaceholderManager> get_SdtPlaceholderManager();
    System::SharedPtr<Aspose::Words::UniqueIdManager> get_SdtIdManager();
    bool get_IsIgnoreLastSectionBreak() const;
    void set_IsIgnoreLastSectionBreak(bool value);
    virtual System::SharedPtr<Aspose::Words::Fonts::DocumentFontProvider> get_FontProvider() = 0;

    System::SharedPtr<Aspose::Words::Formatting::Intern::InternManager> InternManager;
    static const int32_t MapDivider;

    ASPOSE_WORDS_SHARED_API DocumentBase();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener);
    System::SharedPtr<Aspose::Words::NodeChangingArgs> InternalEvent(System::SharedPtr<Aspose::Words::Node> node, System::SharedPtr<Aspose::Words::Node> oldParent, System::SharedPtr<Aspose::Words::Node> newParent, Aspose::Words::NodeChangingAction action);
    void MarkChanged();
    void BeforeEvent(System::SharedPtr<Aspose::Words::NodeChangingArgs> args);
    void AfterEvent(System::SharedPtr<Aspose::Words::NodeChangingArgs> args);
    void AddInternalNodeChangingCallback(System::SharedPtr<Aspose::Words::INodeChangingCallback> callback);
    bool RemoveInternalNodeChangingCallback(System::SharedPtr<Aspose::Words::INodeChangingCallback> callback);
    virtual void SuspendTrackRevisions() = 0;
    virtual void ResumeTrackRevisions() = 0;
    void RemoveHangingNode(System::SharedPtr<Aspose::Words::Node> node);
    void AddHangingNode(System::SharedPtr<Aspose::Words::Node> node);
    void Warn(Aspose::Words::WarningType type, Aspose::Words::WarningSource source, System::String description);
    int32_t GetNextShapeId();
    virtual ASPOSE_WORDS_SHARED_API int32_t GetNextShapeId(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> shape);
    void SetNextShapeId(int32_t value);
    virtual ASPOSE_WORDS_SHARED_API int32_t MapShapeToRange(int32_t currentId, int32_t lnkItemsCount);
    static bool HaveSameIdMap(int32_t id1, int32_t id2);
    static int32_t GetNearestMapId(int32_t id);
    int32_t GetNextDmlTextBoxId();
    void SetNextDmlTextBoxId(int32_t value);
    void ResetNextAnnotationId();
    int32_t GetNextAnnotationId();
    void SetBackgroundShapeSafe(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);
    System::SharedPtr<Aspose::Words::Document> CreateTempDocument();
    virtual System::SharedPtr<Aspose::Words::Themes::Theme> GetThemeInternal() = 0;

    virtual ASPOSE_WORDS_SHARED_API ~DocumentBase();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Node> mNullNode;
    System::SharedPtr<CsToCppPorter::MemoryManagement::ObjectsBag> mHangingNodesCollection;
    System::SharedPtr<Aspose::Words::NodeChangingCallbackChain> mNodeChangingCallbackChain;
    int32_t mTreeChangeCount;
    int32_t mNextShapeId;
    int32_t mNextDmlTextBoxId;
    int32_t mNextAnnotationId;
    System::SharedPtr<Aspose::Words::UniqueIdManager> mSdtIdManager;
    System::SharedPtr<Aspose::Words::Markup::SdtPlaceholderManager> mSdtPlaceholderManager;
    bool mIsIgnoreLastSectionBreak;
    System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> mFontInfos;
    System::SharedPtr<Aspose::Words::StyleCollection> mStyles;
    System::SharedPtr<Aspose::Words::Lists::ListCollection> mLists;
    System::SharedPtr<Aspose::Words::HtmlBlockCollection> mHtmlBlockCollection;
    System::SharedPtr<Aspose::Words::PersonCollection> mPeople;
    System::SharedPtr<Aspose::Words::Settings::DocPr> mDocPr;
    System::SharedPtr<Aspose::Words::VariableCollection> mVariables;
    System::SharedPtr<Aspose::Words::FootnoteSeparatorCollection> mFootnoteSeparators;
    System::SharedPtr<Aspose::Words::Drawing::Shape> mBackgroundShape;
    System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> mResourceLoadingCallback;
    System::SharedPtr<Aspose::Words::IWarningCallback> mWarningCallback;

    void RestoreSdtIds(System::SharedPtr<Aspose::Words::DocumentBase> lhs);
    System::SharedPtr<Aspose::Words::Node> ImportNode(System::SharedPtr<Aspose::Words::Node> srcNode, bool isImportChildren, Aspose::Words::ImportFormatMode importFormatMode, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener);

};

}
}
