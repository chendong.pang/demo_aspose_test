//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/CleanupOptions.h
#pragma once

#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Validation { class DocumentCleaner; } } }

namespace Aspose {

namespace Words {

/// Allows to specify options for document cleaning.
class ASPOSE_WORDS_SHARED_CLASS CleanupOptions : public System::Object
{
    typedef CleanupOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Validation::DocumentCleaner;

public:

    /// Specifies whether unused styles should be removed from document.
    /// Default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_UnusedStyles() const;
    /// Specifies whether unused styles should be removed from document.
    /// Default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_UnusedStyles(bool value);
    /// Specifies whether unused list and list definitions should be removed from document.
    /// Default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_UnusedLists() const;
    /// Specifies whether unused list and list definitions should be removed from document.
    /// Default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_UnusedLists(bool value);
    /// Gets/sets a flag indicating whether duplicate styles should be removed from document.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_DuplicateStyle() const;
    /// Gets/sets a flag indicating whether duplicate styles should be removed from document.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_DuplicateStyle(bool value);

    ASPOSE_WORDS_SHARED_API CleanupOptions();

protected:

    bool get_UnusedBuiltinStyles() const;
    void set_UnusedBuiltinStyles(bool value);

private:

    bool mUnusedStyles;
    bool mUnusedBuiltinStyles;
    bool mUnusedLists;
    bool mDuplicateStyle;

};

}
}
