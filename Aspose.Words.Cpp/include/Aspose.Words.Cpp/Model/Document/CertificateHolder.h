//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/CertificateHolder.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/security/secure_string.h>
#include <system/object.h>
#include <system/array.h>
#include <security/cryptography/x509_certificates/x509_certificate_2.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Saving { class PdfDigitalSignatureDetails; } } }
namespace Aspose { namespace Words { class DigitalSignature; } }
namespace Aspose { namespace Crypto { class CertificateHolderInternal; } }

namespace Aspose {

namespace Words {

/// Represents a holder of <b>X509Certificate2</b> instance.
/// 
/// <b>CertificateHolder</b> can be created by static factory methods only.
/// It contains an instance of <b>X509Certificate2</b> which is used to introduce private, public keys and certificate chains into the system.
/// This class is applied in <see cref="Aspose::Words::DigitalSignatureUtil">DigitalSignatureUtil</see> and <see cref="Aspose::Words::Saving::PdfDigitalSignatureDetails">PdfDigitalSignatureDetails</see> instead of obsolete methods with
/// <see cref="System::Security::Cryptography::X509Certificates::X509Certificate2">X509Certificate2</see> as parameters.
class ASPOSE_WORDS_SHARED_CLASS CertificateHolder : public System::Object
{
    typedef CertificateHolder ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Saving::PdfDigitalSignatureDetails;
    friend class Aspose::Words::DigitalSignature;

public:

    /// Returns the instance of <b>X509Certificate2</b> which holds private, public keys and certificate chain.
    /// 
    /// @return <see cref="System::Security::Cryptography::X509Certificates::X509Certificate2">X509Certificate2</see>
    ///     instance
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Security::Cryptography::X509Certificates::X509Certificate2> get_Certificate();

    /// Creates CertificateHolder object using byte array of PKCS12 store and its password.
    /// 
    /// @param certBytes A byte array that contains data from an X.509 certificate.
    /// @param password The password required to access the X.509 certificate data.
    /// 
    /// @return An instance of CertificateHolder
    /// 
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>certBytes</b> is null
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>password</b> is null
    /// @exception System::Security::SecurityException Thrown if PKCS12 store contains no aliases
    /// @exception System::IO::IOException Thrown if there is wrong password or corrupted file.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CertificateHolder> Create(System::ArrayPtr<uint8_t> certBytes, System::SharedPtr<System::Security::SecureString> password);
    /// Creates CertificateHolder object using byte array of PKCS12 store and its password.
    /// 
    /// @param certBytes A byte array that contains data from an X.509 certificate.
    /// @param password The password required to access the X.509 certificate data.
    /// 
    /// @return An instance of CertificateHolder
    /// 
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>certBytes</b> is null
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>password</b> is null
    /// @exception System::Security::SecurityException Thrown if PKCS12 store contains no aliases
    /// @exception System::IO::IOException Thrown if there is wrong password or corrupted file.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CertificateHolder> Create(System::ArrayPtr<uint8_t> certBytes, System::String password);
    /// Creates CertificateHolder object using path to PKCS12 store and its password.
    /// 
    /// @param fileName The name of a certificate file.
    /// @param password The password required to access the X.509 certificate data.
    /// 
    /// @return An instance of CertificateHolder
    /// 
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>fileName</b> is null
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>password</b> is null
    /// @exception System::Security::SecurityException Thrown if PKCS12 store contains no aliases
    /// @exception System::IO::IOException Thrown if there is wrong password or corrupted file.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CertificateHolder> Create(System::String fileName, System::String password);
    /// Creates CertificateHolder object using path to PKCS12 store, its password and the alias by using which private key and certificate will be found.
    /// 
    /// @param fileName The name of a certificate file.
    /// @param password The password required to access the X.509 certificate data.
    /// @param alias The associated alias for a certificate and its private key
    /// 
    /// @return An instance of CertificateHolder
    /// 
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>fileName</b> is null
    /// @exception Org::BouncyCastle::Security::InvalidParameterException Thrown if <b>password</b> is null
    /// @exception System::Security::SecurityException Thrown if PKCS12 store contains no aliases
    /// @exception System::IO::IOException Thrown if there is wrong password or corrupted file.
    /// @exception System::Security::SecurityException Thrown if there is no private key with the given alias
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CertificateHolder> Create(System::String fileName, System::String password, System::String alias);

protected:

    System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> ToInternal();

    CertificateHolder(System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> holderInternal);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> mCertificateHolder;

};

}
}
