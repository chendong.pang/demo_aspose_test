//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/ComHelper.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }

namespace Aspose {

namespace Words {

/// Provides methods for COM clients to load a document into Aspose.Words.
/// 
/// Use the <see cref="Aspose::Words::ComHelper">ComHelper</see> class to load a document from a file or stream into a
/// <see cref="Aspose::Words::Document">Document</see> object in a COM application.
/// 
/// The <see cref="Aspose::Words::Document">Document</see> class provides a default constructor to create a new document
/// and also provides overloaded constructors to load a document from a file or stream.
/// If you are using Aspose.Words from a .NET application, you can use all of the <see cref="Aspose::Words::Document">Document</see>
/// constructors directly, but if you are using Aspose.Words from a COM application,
/// only the default <see cref="Aspose::Words::Document">Document</see> constructor is available.
class ASPOSE_WORDS_SHARED_CLASS ComHelper : public System::Object
{
    typedef ComHelper ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Initializes a new instance of this class.
    ASPOSE_WORDS_SHARED_API ComHelper();

    /// Allows a COM application to load a <see cref="Aspose::Words::Document">Document</see> from a file.
    /// 
    /// This method is same as calling the <see cref="Aspose::Words::Document">Document</see> constructor with a file name parameter.
    /// 
    /// @param fileName Filename of the document to load.
    /// 
    /// @return A <see cref="Aspose::Words::Document">Document</see> object that represents a Word document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> Open(System::String fileName);
    /// Allows a COM application to load <see cref="Aspose::Words::Document">Document</see> from a stream.
    /// 
    /// This method is same as calling the <see cref="Aspose::Words::Document">Document</see> constructor with a stream parameter.
    /// 
    /// @param stream A .NET stream object that contains the document to load.
    /// 
    /// @return A <see cref="Aspose::Words::Document">Document</see> object that represents a Word document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> Open(System::SharedPtr<System::IO::Stream> stream);

};

}
}
