//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/FileCorruptedException.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/runtime/serialization/streaming_context.h>
#include <system/runtime/serialization/serialization_info.h>
#include <system/object.h>
#include <system/exceptions.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Factories { class FileFormatDetector; } } } }
namespace Aspose { namespace Words { class FileFormatUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class Ecma376AgileEncryption; } } } }

namespace Aspose {

namespace Words {

class Details_FileCorruptedException;
/// Thrown during document load, when the document appears to be corrupted and impossible to load.
using FileCorruptedException = System::ExceptionWrapper<Details_FileCorruptedException>;

/// Thrown during document load, when the document appears to be corrupted and impossible to load.
class ASPOSE_WORDS_SHARED_CLASS Details_FileCorruptedException : public System::Details_Exception
{
    typedef Details_FileCorruptedException ThisType;
    typedef System::Details_Exception BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Factories::FileFormatDetector;
    friend class Aspose::Words::FileFormatUtil;
    friend class Aspose::Words::RW::OfficeCrypto::Ecma376AgileEncryption;
    friend class System::ExceptionWrapperHelper;

protected:

    [[noreturn]] void DoThrow(const System::ExceptionPtr& self) const override;

    Details_FileCorruptedException(System::String message);
    Details_FileCorruptedException(System::String message, System::Exception innerException);
    Details_FileCorruptedException(System::Exception innerException);
    Details_FileCorruptedException(System::SharedPtr<System::Runtime::Serialization::SerializationInfo> info, System::Runtime::Serialization::StreamingContext context);
    ASPOSE_WORDS_SHARED_API Details_FileCorruptedException();

};

}
}
