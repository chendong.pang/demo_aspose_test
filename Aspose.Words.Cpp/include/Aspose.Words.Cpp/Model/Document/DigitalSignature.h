//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/DigitalSignature.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/guid.h>
#include <system/date_time.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Document/DigitalSignatureType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class SignOptions; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class SignatureLineRenderingHelper; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerMsFormatBase; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerBase; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerOdt; } } } }
namespace Aspose { namespace Words { class DigitalSignatureUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class OpcSignatureWriterBase; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Signatures { class CertificateInfoWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class SignatureLine; } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerOoxml; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerXps; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Signatures { class CertificateInfoReader; } } } } }
namespace Aspose { namespace Words { class DigitalSignatureCollection; } }
namespace Aspose { namespace Words { class CertificateHolder; } }
namespace Aspose { namespace Crypto { class CertificateHolderInternal; } }

namespace Aspose {

namespace Words {

/// Represents a digital signature on a document and the result of its verification.
class ASPOSE_WORDS_SHARED_CLASS DigitalSignature : public System::Object
{
    typedef DigitalSignature ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::SignOptions;
    friend class Aspose::Words::Drawing::Core::SignatureLineRenderingHelper;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerMsFormatBase;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerBase;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerOdt;
    friend class Aspose::Words::DigitalSignatureUtil;
    friend class Aspose::Words::RW::OfficeCrypto::OpcSignatureWriterBase;
    friend class Aspose::Words::RW::Doc::Signatures::CertificateInfoWriter;
    friend class Aspose::Words::Drawing::SignatureLine;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerOoxml;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigReader;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerXps;
    friend class Aspose::Words::RW::Doc::Signatures::CertificateInfoReader;
    friend class Aspose::Words::DigitalSignatureCollection;

public:

    /// Gets the type of the digital signature.
    ASPOSE_WORDS_SHARED_API Aspose::Words::DigitalSignatureType get_SignatureType() const;
    /// Gets the time the document was signed.
    ASPOSE_WORDS_SHARED_API System::DateTime get_SignTime() const;
    /// Gets the signing purpose comment.
    ASPOSE_WORDS_SHARED_API System::String get_Comments() const;
    /// Returns the subject distinguished name of the certificate that was used to sign the document.
    ASPOSE_WORDS_SHARED_API System::String get_SubjectName();
    /// Returns the subject distinguished name of the certificate isuuer.
    ASPOSE_WORDS_SHARED_API System::String get_IssuerName();
    /// Returns true if this digital signature is valid and the document has not been tampered with.
    ASPOSE_WORDS_SHARED_API bool get_IsValid() const;
    /// Returns the certificate holder object that contains the certificate was used to sign the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::CertificateHolder> get_CertificateHolder() const;

    /// Returns a user-friendly string that displays the value of this object.
    ASPOSE_WORDS_SHARED_API System::String ToString() const override;

protected:

    System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> get_CertificateHolderInternal() const;
    System::ArrayPtr<uint8_t> get_ImageBytes() const;
    void set_ImageBytes(System::ArrayPtr<uint8_t> value);
    System::ArrayPtr<uint8_t> get_ImageBytesValid() const;
    void set_ImageBytesValid(System::ArrayPtr<uint8_t> value);
    System::ArrayPtr<uint8_t> get_ImageBytesInvalid() const;
    void set_ImageBytesInvalid(System::ArrayPtr<uint8_t> value);
    bool get_Visible() const;
    void set_Visible(bool value);
    System::String get_Text() const;
    void set_Text(System::String value);
    System::Guid get_SetupId() const;
    void set_SetupId(System::Guid value);
    System::Guid get_ProviderId() const;
    void set_ProviderId(System::Guid value);

    DigitalSignature(System::SharedPtr<Aspose::Words::CertificateHolder> holder);
    DigitalSignature(Aspose::Words::DigitalSignatureType sigType);

    void SetSignTime(System::DateTime signTime);
    void SetComments(System::String comments);
    void SetIsValid(bool result);
    void SetCertificate(System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> certificate);
    static System::String ExtractCn(System::String cn);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::DigitalSignatureType mSignatureType;
    System::SharedPtr<Aspose::Words::CertificateHolder> mCertificateHolder;
    System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> mCertificateHolderInternal;
    System::DateTime mSignTime;
    System::String mComments;
    bool mIsValid;
    System::ArrayPtr<uint8_t> mImage;
    System::ArrayPtr<uint8_t> mImageValid;
    System::ArrayPtr<uint8_t> mImageInvalid;
    System::String mText;
    System::Guid mSetupId;
    System::Guid mProviderId;
    bool mVisible;

    DigitalSignature(System::SharedPtr<Aspose::Crypto::CertificateHolderInternal> holder);

};

}
}
