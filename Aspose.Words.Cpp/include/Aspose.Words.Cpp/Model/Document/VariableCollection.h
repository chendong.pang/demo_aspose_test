//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/VariableCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/keyvalue_pair.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedStringListGeneric; } } }

namespace Aspose {

namespace Words {

/// A collection of document variables.
/// 
/// Variable names and values are strings.
/// 
/// Variable names are case-insensitive.
class ASPOSE_WORDS_SHARED_CLASS VariableCollection : public System::Collections::Generic::IEnumerable<System::Collections::Generic::KeyValuePair<System::String, System::String>>
{
    typedef VariableCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::Collections::Generic::KeyValuePair<System::String, System::String>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::DocumentBase;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets or a sets a document variable by the case-insensitive name.
    /// null values are not allowed as a right hand side of the assignment and will be replaced by empty string.
    ASPOSE_WORDS_SHARED_API System::String idx_get(System::String name);
    /// Gets or a sets a document variable by the case-insensitive name.
    /// null values are not allowed as a right hand side of the assignment and will be replaced by empty string.
    ASPOSE_WORDS_SHARED_API void idx_set(System::String name, System::String value);
    /// Gets or sets a document variable at the specified index.
    /// null values are not allowed as a right hand side of the assignment and will be replaced by empty string.
    /// 
    /// @param index Zero-based index of the document variable.
    ASPOSE_WORDS_SHARED_API System::String idx_get(int32_t index);
    /// Gets or sets a document variable at the specified index.
    /// null values are not allowed as a right hand side of the assignment and will be replaced by empty string.
    /// 
    /// @param index Zero-based index of the document variable.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::String value);

    /// Returns an enumerator object that can be used to iterate over all variable in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::Collections::Generic::KeyValuePair<System::String, System::String>>> GetEnumerator() override;
    /// Adds a document variable to the collection.
    /// 
    /// @param name The case-insensitive name of the variable to add.
    /// @param value The value of the variable. The value cannot be null, if value is null empty string will be used instead.
    ASPOSE_WORDS_SHARED_API void Add(System::String name, System::String value);
    /// Determines whether the collection contains a document variable with the given name.
    /// 
    /// @param name Case-insensitive name of the document variable to locate.
    /// 
    /// @return True if item is found in the collection; otherwise, false.
    ASPOSE_WORDS_SHARED_API bool Contains(System::String name);
    /// Returns the zero-based index of the specified document variable in the collection.
    /// 
    /// @param name The case-insensitive name of the variable.
    /// 
    /// @return The zero based index. Negative value if not found.
    ASPOSE_WORDS_SHARED_API int32_t IndexOfKey(System::String name);
    /// Removes a document variable with the specified name from the collection.
    /// 
    /// @param name The case-insensitive name of the variable.
    ASPOSE_WORDS_SHARED_API void Remove(System::String name);
    /// Removes a document variable at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all elements from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();

protected:

    VariableCollection();

    System::SharedPtr<Aspose::Words::VariableCollection> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~VariableCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::SortedStringListGeneric<System::String>> mItems;

};

}
}
