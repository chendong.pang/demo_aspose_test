//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/RtfLoadOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Document/LoadOptions.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }

namespace Aspose {

namespace Words {

/// Allows to specify additional options when loading <see cref="Aspose::Words::LoadFormat::Rtf">Rtf</see> document into a <see cref="Aspose::Words::Document">Document</see> object.
class ASPOSE_WORDS_SHARED_CLASS RtfLoadOptions : public Aspose::Words::LoadOptions
{
    typedef RtfLoadOptions ThisType;
    typedef Aspose::Words::LoadOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;

public:

    /// When set to true, <see cref="Aspose::Charset::CharsetDetector">CharsetDetector</see> will try to detect UTF8 characters,
    /// they will be preserved during import.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_RecognizeUtf8Text() const;
    /// When set to true, <see cref="Aspose::Charset::CharsetDetector">CharsetDetector</see> will try to detect UTF8 characters,
    /// they will be preserved during import.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_RecognizeUtf8Text(bool value);

    /// Initializes a new instance of this class with default values.
    ASPOSE_WORDS_SHARED_API RtfLoadOptions();

protected:

    RtfLoadOptions(System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::LoadOptions> Clone() override;

    virtual ASPOSE_WORDS_SHARED_API ~RtfLoadOptions();

private:

    bool mRecognizeUtf8Text;

};

}
}
