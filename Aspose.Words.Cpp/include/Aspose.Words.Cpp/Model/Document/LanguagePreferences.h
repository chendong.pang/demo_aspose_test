//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/LanguagePreferences.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Document/EditingLanguage.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxStylesReader; } } } } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class HashSetGeneric; } } }
namespace Aspose { namespace Words { class DocumentBase; } }

namespace Aspose {

namespace Words {

/// Allows to set up language preferences.
class ASPOSE_WORDS_SHARED_CLASS LanguagePreferences : public System::Object
{
    typedef LanguagePreferences ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::RW::Docx::Reader::DocxStylesReader;

public:

    /// Gets default editing language.
    /// The default value is <see cref="Aspose::Words::EditingLanguage::EnglishUS">EnglishUS</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::EditingLanguage get_DefaultEditingLanguage() const;
    /// Sets default editing language.
    /// The default value is <see cref="Aspose::Words::EditingLanguage::EnglishUS">EnglishUS</see>.
    ASPOSE_WORDS_SHARED_API void set_DefaultEditingLanguage(Aspose::Words::EditingLanguage value);

    /// Adds additional editing language.
    ASPOSE_WORDS_SHARED_API void AddEditingLanguage(Aspose::Words::EditingLanguage language);
    /// Adds additional editing languages.
    ASPOSE_WORDS_SHARED_API void AddEditingLanguages(System::ArrayPtr<Aspose::Words::EditingLanguage> languages);

    ASPOSE_WORDS_SHARED_API LanguagePreferences();

protected:

    bool get_IsDefaultChineseOrJapanese();
    static bool get_TestMode();
    static void set_TestMode(bool value);

    void Apply(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::EditingLanguage get_LocaleId();
    Aspose::Words::EditingLanguage get_LocaleIdFarEast();
    Aspose::Words::EditingLanguage get_LocaleIdFarEastByDefaultLanguage();
    Aspose::Words::EditingLanguage get_LocaleIdFarEastByEditingLanguagesCollection();
    Aspose::Words::EditingLanguage get_LocaleIdBi();
    bool get_ContainsChineseSimplified();
    bool get_ContainsChineseTraditional();
    bool get_ContainsHebrew();
    bool get_ContainsArabic();
    bool get_ContainsJapanese();

    System::SharedPtr<Aspose::Collections::Generic::HashSetGeneric<Aspose::Words::EditingLanguage>> mEditingLanguages;
    Aspose::Words::EditingLanguage mDefaultEditingLanguage;

    static bool& gTestMode();

};

}
}
