//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/FileFormatInfo.h
#pragma once

#include <system/text/encoding.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Document/LoadFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Chm { namespace Reader { class ChmFormatDetector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace FormatDetector { class MarkdownFormatDetector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class OpenXmlDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocDocumentReader; } } } } }
namespace Aspose { namespace Words { class DigitalSignatureUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Factories { class FileFormatDetector; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlFormatDetector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Reader { class MhtmlFormatDetector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtFormatDetector; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }
namespace Aspose { namespace Words { enum class DigitalSignatureType; } }
namespace Aspose { namespace Ss { class FileSystem; } }

namespace Aspose {

namespace Words {

/// Contains data returned by <see cref="Aspose::Words::FileFormatUtil">FileFormatUtil</see> document format detection methods.
/// 
/// You do not create instances of this class directly. Objects of this class are returned by
/// <see cref="Aspose::Words::FileFormatUtil::DetectFileFormat(System::SharedPtr<System::IO::Stream>)">DetectFileFormat()</see> methods.
class ASPOSE_WORDS_SHARED_CLASS FileFormatInfo : public System::Object
{
    typedef FileFormatInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Chm::Reader::ChmFormatDetector;
    friend class Aspose::Words::RW::Markdown::FormatDetector::MarkdownFormatDetector;
    friend class Aspose::Words::RW::Docx::Reader::OpenXmlDocumentReader;
    friend class Aspose::Words::RW::Doc::Reader::DocDocumentReader;
    friend class Aspose::Words::DigitalSignatureUtil;
    friend class Aspose::Words::RW::Factories::FileFormatDetector;
    friend class Aspose::Words::RW::Html::Reader::HtmlFormatDetector;
    friend class Aspose::Words::RW::Mhtml::Reader::MhtmlFormatDetector;
    friend class Aspose::Words::RW::Txt::Reader::TxtFormatDetector;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;

public:

    /// Gets the detected document format.
    /// 
    /// When an OOXML document is encrypted, it is not possible to ascertained whether it is
    /// an Excel, Word or PowerPoint document without decrypting it first so for an encrypted OOXML
    /// document this property will always return <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.
    /// 
    /// @sa Aspose::Words::FileFormatInfo::get_IsEncrypted
    ASPOSE_WORDS_SHARED_API Aspose::Words::LoadFormat get_LoadFormat() const;
    /// Returns true if the document is encrypted and requires a password to open.
    /// 
    /// This property exists to help you sort documents that are encrypted from those that are not.
    /// If you attempt to load an encrypted document using Aspose.Words without supplying a password an
    /// exception will be thrown. You can use this property to detect whether a document requires a password
    /// and take some action before loading a document, for example, prompt the user for a password.
    /// 
    /// @sa Aspose::Words::FileFormatInfo::get_LoadFormat
    ASPOSE_WORDS_SHARED_API bool get_IsEncrypted() const;
    /// Returns true if this document contains a digital signature.
    /// This property merely informs that a digital signature is present on a document,
    /// but it does not  specify whether the signature is valid or not.
    /// 
    /// This property exists to help you sort documents that are digitally signed from those that are not.
    /// If you use Aspose.Words to modify and save a document that is digitally signed, then the digital signature will
    /// be lost. This is by design because a digital signature exists to guard the authenticity of a document.
    /// Using this property you can detect digitally signed documents before processing them in the same way as normal
    /// documents and take some action to avoid losing the digital signature, for example notify the user.
    ASPOSE_WORDS_SHARED_API bool get_HasDigitalSignature() const;
    /// Gets the detected encoding if applicable to the current document format.
    /// At the moment detects encoding only for HTML documents.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Text::Encoding> get_Encoding() const;

protected:

    Aspose::Words::DigitalSignatureType get_DigitalSignatureType() const;
    void set_DigitalSignatureType(Aspose::Words::DigitalSignatureType value);
    System::SharedPtr<Aspose::Ss::FileSystem> get_FileSystem() const;
    void set_FileSystem(System::SharedPtr<Aspose::Ss::FileSystem> value);
    bool get_HasRtlScript() const;
    void set_HasRtlScript(bool value);

    FileFormatInfo();

    void SetLoadFormat(Aspose::Words::LoadFormat loadFormat);
    void SetIsEncrypted(bool isEncrypted);
    void SetIsDigitalSignaturePresent(bool isDigitalSignaturePresent);
    void SetEncoding(System::SharedPtr<System::Text::Encoding> encoding);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::LoadFormat mLoadFormat;
    bool mIsEncrypted;
    bool mHasDigitalSignature;
    System::SharedPtr<System::Text::Encoding> mEncoding;
    bool mHasRtlScript;
    Aspose::Words::DigitalSignatureType mDigitalSignatureType;
    System::SharedPtr<Aspose::Ss::FileSystem> mFileSystem;

};

}
}
