//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/SubDocument.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Doc { class FileSubDocuments; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class SubDocumentProcessor; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlInlineReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxInlineReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfSpecialCharacterReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Represents a <b>SubDocument</b> - which is a reference to an externally stored document.
/// 
/// In this version of Aspose.Words, <see cref="Aspose::Words::SubDocument">SubDocument</see> nodes do not provide public methods
/// and properties to create or modify a subdocument. In this version you are not able to instantiate
/// SubDocument nodes or modify existing except deleting them.
/// 
/// <see cref="Aspose::Words::SubDocument">SubDocument</see> can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
class ASPOSE_WORDS_SHARED_CLASS SubDocument : public Aspose::Words::Node
{
    typedef SubDocument ThisType;
    typedef Aspose::Words::Node BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Doc::FileSubDocuments;
    friend class Aspose::Words::RW::Doc::SubDocumentProcessor;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Wml::Reader::WmlInlineReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxInlineReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfSpecialCharacterReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Returns <b>NodeType.SubDocument</b>
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    System::String get_FileName() const;
    void set_FileName(System::String value);

    SubDocument(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::String fileName);

    virtual ASPOSE_WORDS_SHARED_API ~SubDocument();

private:

    System::String mFileName;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
