//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/DigitalSignatureCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Document/DigitalSignature.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { class SignatureLine; } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Signatures { class SignatureReader; } } } } }
namespace Aspose { namespace Collections { template<typename> class GuidToObjDictionary; } }

namespace Aspose {

namespace Words {

/// Provides a read-only collection of digital signatures attached to a document.
class ASPOSE_WORDS_SHARED_CLASS DigitalSignatureCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::DigitalSignature>>
{
    typedef DigitalSignatureCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::DigitalSignature>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Drawing::SignatureLine;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigReader;
    friend class Aspose::Words::RW::Doc::Signatures::SignatureReader;

public:

    /// Returns <c>true</c> if all digital signatures in this collection are valid and the document has not been tampered with
    /// Also returns <c>true</c> if there are no digital signatures.
    /// Returns <c>false</c> if at least one digital signature is invalid.
    ASPOSE_WORDS_SHARED_API bool get_IsValid();
    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets a document signature at the specified index.
    /// 
    /// @param index Zero-based index of the signature.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DigitalSignature> idx_get(int32_t index);

    /// Returns a dictionary enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::DigitalSignature>>> GetEnumerator() override;

    ASPOSE_WORDS_SHARED_API DigitalSignatureCollection();

protected:

    void Add(System::SharedPtr<Aspose::Words::DigitalSignature> signature);
    System::SharedPtr<Aspose::Words::DigitalSignature> GetBySetupId(System::String setupId);

    virtual ASPOSE_WORDS_SHARED_API ~DigitalSignatureCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::DigitalSignature>>> mItems;
    System::SharedPtr<Aspose::Collections::GuidToObjDictionary<System::SharedPtr<Aspose::Words::DigitalSignature>>> mSignatureBySetupId;

};

}
}
