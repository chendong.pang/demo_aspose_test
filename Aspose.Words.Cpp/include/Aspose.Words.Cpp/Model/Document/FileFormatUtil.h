//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/FileFormatUtil.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <system/exceptions.h>
#include <system/collections/dictionary.h>
#include <mutex>
#include <memory>

#include "Aspose.Words.Cpp/Model/Drawing/ImageType.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"
#include "Aspose.Words.Cpp/Model/Document/LoadFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class FileFormatInfo; } }
namespace Aspose { namespace Words { namespace Saving { class SaveOptions; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class ImageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class OfficeMathToShapeConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlEquationXmlConverter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ImageDataCore; } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace RW { namespace Svg { namespace Reader { class SvgImage; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsBlipBitmap; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsBlipMetafile; } } } } }
namespace Aspose { enum class FileFormat; }

namespace Aspose {

namespace Words {

/// Provides utility methods for working with file formats, such as detecting file format
/// or converting file extensions to/from file format enums.
class ASPOSE_WORDS_SHARED_CLASS FileFormatUtil
{
    typedef FileFormatUtil ThisType;

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Saving::SaveOptions;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::ImageShapeWriter;
    friend class Aspose::Words::Validation::OfficeMathToShapeConverter;
    friend class Aspose::Words::RW::Wml::Reader::WmlEquationXmlConverter;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Drawing::Core::ImageDataCore;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::RW::Svg::Reader::SvgImage;
    friend class Aspose::Words::RW::Doc::Escher::EsBlipBitmap;
    friend class Aspose::Words::RW::Doc::Escher::EsBlipMetafile;

public:

    /// Detects and returns the information about a format of a document stored in a disk file.
    /// 
    /// Even if this method detects the document format, it does not guarantee
    /// that the specified document is valid. This method only detects the document format by
    /// reading data that is sufficient for detection. To fully verify that a document is valid
    /// you need to load the document into a <see cref="Aspose::Words::Document">Document</see> object.
    /// 
    /// This method throws <see cref="Aspose::Words::FileCorruptedException">FileCorruptedException</see> when the format is
    /// recognized, but the detection cannot complete because of corruption.
    /// 
    /// @param fileName The file name.
    /// 
    /// @return A <see cref="Aspose::Words::FileFormatInfo">FileFormatInfo</see> object that contains the detected information.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::FileFormatInfo> DetectFileFormat(System::String fileName);
    /// Detects and returns the information about a format of a document stored in a stream.
    /// 
    /// The stream must be positioned at the beginning of the document.
    /// 
    /// When this method returns, the position in the stream is restored to the original position.
    /// 
    /// Even if this method detects the document format, it does not guarantee
    /// that the specified document is valid. This method only detects the document format by
    /// reading data that is sufficient for detection. To fully verify that a document is valid
    /// you need to load the document into a <see cref="Aspose::Words::Document">Document</see> object.
    /// 
    /// This method throws <see cref="Aspose::Words::FileCorruptedException">FileCorruptedException</see> when the format is
    /// recognized, but the detection cannot complete because of corruption.
    /// 
    /// @param stream The stream.
    /// 
    /// @return A <see cref="Aspose::Words::FileFormatInfo">FileFormatInfo</see> object that contains the detected information.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::FileFormatInfo> DetectFileFormat(System::SharedPtr<System::IO::Stream> stream);
    /// Converts IANA content type into a load format enumerated value.
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API Aspose::Words::LoadFormat ContentTypeToLoadFormat(System::String contentType);
    /// Converts IANA content type into a save format enumerated value.
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat ContentTypeToSaveFormat(System::String contentType);
    /// Converts a load format enumerated value into a file extension. The returned extension is a lower-case string with a leading dot.
    /// 
    /// The <see cref="Aspose::Words::SaveFormat::WordML">WordML</see> value is converted to ".wml".
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API System::String LoadFormatToExtension(Aspose::Words::LoadFormat loadFormat);
    /// Converts a <see cref="Aspose::Words::SaveFormat">SaveFormat</see> value to a <see cref="Aspose::Words::LoadFormat">LoadFormat</see> value if possible.
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API Aspose::Words::LoadFormat SaveFormatToLoadFormat(Aspose::Words::SaveFormat saveFormat);
    /// Converts a <see cref="Aspose::Words::LoadFormat">LoadFormat</see> value to a <see cref="Aspose::Words::SaveFormat">SaveFormat</see> value if possible.
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat LoadFormatToSaveFormat(Aspose::Words::LoadFormat loadFormat);
    /// Converts a save format enumerated value into a file extension. The returned extension is a lower-case string with a leading dot.
    /// 
    /// The <see cref="Aspose::Words::SaveFormat::WordML">WordML</see> value is converted to ".wml".
    /// 
    /// The <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see> value is converted to ".fopc".
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API System::String SaveFormatToExtension(Aspose::Words::SaveFormat saveFormat);
    /// Converts a file name extension into a <see cref="Aspose::Words::SaveFormat">SaveFormat</see> value.
    /// 
    /// If the extension cannot be recognized, returns <see cref="Aspose::Words::SaveFormat::Unknown">Unknown</see>.
    /// 
    /// @param extension The file extension. Can be with or without a leading dot. Case-insensitive.
    /// 
    /// @exception System::ArgumentNullException Throws if the parameter is null.
    static ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat ExtensionToSaveFormat(System::String extension);
    /// Converts an Aspose.Words image type enumerated value into a file extension. The returned extension is a lower-case string with a leading dot.
    /// 
    /// @exception System::ArgumentException Throws when cannot convert.
    static ASPOSE_WORDS_SHARED_API System::String ImageTypeToExtension(Aspose::Words::Drawing::ImageType imageType);

    ASPOSE_WORDS_SHARED_API FileFormatUtil();

protected:

    static System::Exception ConvertLoadException(System::Exception e);
    static void ConvertAndRethrowLoadException(System::Exception const &e);
    static System::String SaveFormatToString(Aspose::Words::SaveFormat saveFormat);
    static Aspose::Words::SaveFormat ToSaveFormat(Aspose::FileFormat fileFormat);
    static Aspose::Words::Drawing::ImageType ToImageType(Aspose::FileFormat fileFormat);
    static bool IsXmlBasedFormat(Aspose::Words::LoadFormat format);
    static Aspose::FileFormat FromSaveFormat(Aspose::Words::SaveFormat saveFormat);

private:

    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::Words::LoadFormat, Aspose::FileFormat>>& gFromLoadFormat();
    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::Words::SaveFormat, Aspose::FileFormat>>& gFromSaveFormat();
    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::Words::Drawing::ImageType, Aspose::FileFormat>>& gFromImageType();
    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::FileFormat, Aspose::Words::LoadFormat>>& gToLoadFormat();
    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::FileFormat, Aspose::Words::SaveFormat>>& gToSaveFormat();
    static System::SharedPtr<System::Collections::Generic::Dictionary<Aspose::FileFormat, Aspose::Words::Drawing::ImageType>>& gToImageType();
    static Aspose::FileFormat FromLoadFormat(Aspose::Words::LoadFormat loadFormat);
    static Aspose::FileFormat FromImageType(Aspose::Words::Drawing::ImageType imageType);
    static Aspose::Words::LoadFormat ToLoadFormat(Aspose::FileFormat fileFormat);
    static void AddMap(Aspose::FileFormat fileFormat, Aspose::Words::SaveFormat saveFormat, Aspose::Words::LoadFormat loadFormat, Aspose::Words::Drawing::ImageType imageType);

    static void __StaticConstructor__();

};

}
}
