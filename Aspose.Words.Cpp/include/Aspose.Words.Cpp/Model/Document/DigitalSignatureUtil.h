//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/DigitalSignatureUtil.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/io/stream.h>
#include <system/io/memory_stream.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class OpenXmlDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerMsFormatBase; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class XmlDsigSignerOdt; } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class Reference; } } } }
namespace Aspose { namespace Words { class CertificateHolder; } }
namespace Aspose { namespace Words { class SignOptions; } }
namespace Aspose { namespace Words { class DigitalSignatureCollection; } }
namespace Aspose { namespace Ss { class FileSystem; } }
namespace Aspose { namespace Crypto { enum class DigestAlgorithm; } }
namespace Aspose { namespace Crypto { enum class CryptoAlgorithm : uint8_t; } }
namespace Aspose { namespace Words { class DigitalSignature; } }

namespace Aspose {

namespace Words {

/// Provides methods for signing document.
/// 
/// Since digital signature works with file content rather than Document Object Model these methods are put into a separate class.
/// 
/// Supported formats are <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> and <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.
class ASPOSE_WORDS_SHARED_CLASS DigitalSignatureUtil
{
    typedef DigitalSignatureUtil ThisType;

    friend class Aspose::Words::RW::Docx::Reader::OpenXmlDocumentReader;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerMsFormatBase;
    friend class Aspose::Words::RW::OfficeCrypto::XmlDsigSignerOdt;
    friend class Aspose::Words::RW::OfficeCrypto::Reference;

public:

    /// Signs source document using given <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> and <see cref="Aspose::Words::SignOptions">SignOptions</see>
    /// with digital signature and writes signed document to destination stream.
    /// Document should be either <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> or <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.<b>Output will be written to the start of stream and stream size will be updated with content length.</b>
    /// 
    /// @param srcStream The stream which contains the document to sign.
    /// @param dstStream The stream that signed document will be written to.
    /// @param certHolder <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> object with certificate that used to sign file.
    ///     The certificate in holder MUST contain private keys and have the X509KeyStorageFlags.Exportable flag set.
    /// @param signOptions <see cref="Aspose::Words::SignOptions">SignOptions</see> object with various signing options.
    static ASPOSE_WORDS_SHARED_API void Sign(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::CertificateHolder> certHolder, System::SharedPtr<Aspose::Words::SignOptions> signOptions);
    /// Signs source document using given <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> and <see cref="Aspose::Words::SignOptions">SignOptions</see>
    /// with digital signature and writes signed document to destination file.
    /// Document should be either <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> or <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.
    /// 
    /// @param srcFileName The file name of the document to sign.
    /// @param dstFileName The file name of the signed document output.
    /// @param certHolder <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> object with certificate that used to sign file.
    ///     The certificate in holder MUST contain private keys and have the X509KeyStorageFlags.Exportable flag set.
    /// @param signOptions <see cref="Aspose::Words::SignOptions">SignOptions</see> object with various signing options.
    static ASPOSE_WORDS_SHARED_API void Sign(System::String srcFileName, System::String dstFileName, System::SharedPtr<Aspose::Words::CertificateHolder> certHolder, System::SharedPtr<Aspose::Words::SignOptions> signOptions);
    /// Signs source document using given <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> with digital signature
    /// and writes signed document to destination stream.
    /// Document should be either <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> or <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.<b>Output will be written to the start of stream and stream size will be updated with content length.</b>
    /// 
    /// @param srcStream The stream which contains the document to sign.
    /// @param dstStream The stream that signed document will be written to.
    /// @param certHolder <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> object with certificate that used to sign file.
    ///     The certificate in holder MUST contain private keys and have the X509KeyStorageFlags.Exportable flag set.
    static ASPOSE_WORDS_SHARED_API void Sign(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::CertificateHolder> certHolder);
    /// Signs source document using given <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> with digital signature
    /// and writes signed document to destination file.
    /// Document should be either <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> or <see cref="Aspose::Words::LoadFormat::Docx">Docx</see>.
    /// 
    /// @param srcFileName The file name of the document to sign.
    /// @param dstFileName The file name of the signed document output.
    /// @param certHolder <see cref="Aspose::Words::CertificateHolder">CertificateHolder</see> object with certificate that used to sign file.
    ///     The certificate in holder MUST contain private keys and have the X509KeyStorageFlags.Exportable flag set.
    static ASPOSE_WORDS_SHARED_API void Sign(System::String srcFileName, System::String dstFileName, System::SharedPtr<Aspose::Words::CertificateHolder> certHolder);
    /// Removes all digital signatures from source file and writes unsigned file to destination file.
    static ASPOSE_WORDS_SHARED_API void RemoveAllSignatures(System::String srcFileName, System::String dstFileName);
    /// Removes all digital signatures from document in source stream and writes unsigned document to destination stream.
    /// <b>Output will be written to the start of stream and stream size will be updated with content length.</b>
    static ASPOSE_WORDS_SHARED_API void RemoveAllSignatures(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream);
    /// Loads digital signatures from document.
    /// 
    /// @param fileName Path to the document.
    /// 
    /// @return Collection of digital signatures. Returns empty collection if file is not signed.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DigitalSignatureCollection> LoadSignatures(System::String fileName);
    /// Loads digital signatures from document using stream.
    /// 
    /// @param stream Stream with the document.
    /// 
    /// @return Collection of digital signatures. Returns empty collection if file is not signed.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DigitalSignatureCollection> LoadSignatures(System::SharedPtr<System::IO::Stream> stream);

protected:

    static System::SharedPtr<System::IO::MemoryStream> DecryptFileSystem(System::SharedPtr<Aspose::Ss::FileSystem> fileSystem, System::String password);
    static System::String GetDigestMethod(Aspose::Crypto::DigestAlgorithm digestAlgorithm);
    static System::String GetDigestMethod(Aspose::Crypto::CryptoAlgorithm cryptoAlgorithm);

private:

    static const System::String& NotSupportedByFileFormat();
    static void SignStreams(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::CertificateHolder> certHolder, System::SharedPtr<Aspose::Words::SignOptions> signOptions);
    static void SignOpcPackage(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::DigitalSignature> signature, bool isXps);
    static void SignOdtPackage(System::SharedPtr<System::IO::Stream> srcStream, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::DigitalSignature> signature);
    static void SignEncryptedOpcPackage(System::SharedPtr<Aspose::Ss::FileSystem> srcDocFileSystem, System::SharedPtr<System::IO::Stream> dstStream, System::SharedPtr<Aspose::Words::DigitalSignature> signature, System::String srcPassword);
    static System::SharedPtr<Aspose::Words::DigitalSignatureCollection> LoadFromXps(System::SharedPtr<System::IO::Stream> stream);

};

}
}
