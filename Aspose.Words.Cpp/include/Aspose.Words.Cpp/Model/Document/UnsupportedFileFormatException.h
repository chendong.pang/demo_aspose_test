//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/UnsupportedFileFormatException.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/runtime/serialization/streaming_context.h>
#include <system/runtime/serialization/serialization_info.h>
#include <system/object.h>
#include <system/exceptions.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Factories { class ReaderFactory; } } } }
namespace Aspose { namespace Words { class DigitalSignatureUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Mobi { namespace Reader { class MobiParser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace OfficeCrypto { class DataSpaces; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReaderCore; } } } } }

namespace Aspose {

namespace Words {

class Details_UnsupportedFileFormatException;
/// Thrown during document load, when the document format is not recognized or not supported by Aspose.Words.
using UnsupportedFileFormatException = System::ExceptionWrapper<Details_UnsupportedFileFormatException>;

/// Thrown during document load, when the document format is not recognized or not supported by Aspose.Words.
class ASPOSE_WORDS_SHARED_CLASS Details_UnsupportedFileFormatException : public System::Details_Exception
{
    typedef Details_UnsupportedFileFormatException ThisType;
    typedef System::Details_Exception BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Factories::ReaderFactory;
    friend class Aspose::Words::DigitalSignatureUtil;
    friend class Aspose::Words::RW::Mobi::Reader::MobiParser;
    friend class Aspose::Words::RW::OfficeCrypto::DataSpaces;
    friend class Aspose::Words::RW::Doc::Reader::DocReaderCore;
    friend class System::ExceptionWrapperHelper;

protected:

    [[noreturn]] void DoThrow(const System::ExceptionPtr& self) const override;

    Details_UnsupportedFileFormatException(System::String message);
    Details_UnsupportedFileFormatException(System::String message, System::Exception innerException);
    Details_UnsupportedFileFormatException(System::SharedPtr<System::Runtime::Serialization::SerializationInfo> info, System::Runtime::Serialization::StreamingContext context);
    ASPOSE_WORDS_SHARED_API Details_UnsupportedFileFormatException();

};

}
}
