//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Document/Document.h
#pragma once

#include <system/io/stream.h>
#include <system/io/memory_stream.h>
#include <system/dummy_classes.h>
#include <system/date_time.h>
#include <system/collections/ilist.h>
#include <system/collections/idictionary.h>
#include <system/collections/dictionary.h>
#include <mutex>
#include <memory>
#include <drawing/size_f.h>
#include <drawing/graphics.h>

#include "Aspose.Words.Cpp/Model/Saving/OoxmlCompliance.h"
#include "Aspose.Words.Cpp/Model/Formatting/ISectionAttrSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Watermark/IWatermarkProvider.h"
#include "Aspose.Words.Cpp/Model/Document/RevisionsView.h"
#include "Aspose.Words.Cpp/Model/Document/ProtectionType.h"
#include "Aspose.Words.Cpp/Model/Document/LoadFormat.h"
#include "Aspose.Words.Cpp/Model/Document/DocumentBase.h"

namespace Aspose { namespace Words { namespace Saving { class SaveOutputParameters; } } }
namespace Aspose { namespace Words { namespace Rendering { class PageInfo; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramTextBoxFitCache; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathContext; } } } }
namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { class Watermark; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlNodeCustomizer; } } } } }
namespace Aspose { namespace Words { namespace Fields { class AutoTextEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace RW { namespace Chm { namespace Reader { class ChmReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace Fields { class ToaEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Validation { class CustomXmlValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlFontNameResolver; } } } }
namespace Aspose { namespace Words { namespace RW { class RtfThemeWriter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Svg { namespace Reader { class SvgToEmfConverter; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class OpenXmlDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxReaderFactory; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlEquationXmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class StyleFormatterHtmlRules; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlIFrameWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Reader { class MhtmlReader; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReader; } } } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { class PlainTextDocument; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Saving { class PclSaveOptions; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlPictureRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace ShapeEffects { class DmlShapeEffectsRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class BackgroundShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanFieldContextMainText; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { namespace Layout { class LayoutCollector; } } }
namespace Aspose { namespace Words { namespace Layout { class LayoutEnumerator; } } }
namespace Aspose { namespace Words { class DigitalSignatureUtil; } }
namespace Aspose { namespace Words { class OoxmlComplianceInfo; } }
namespace Aspose { namespace Words { class RangeDocumentBuilder; } }
namespace Aspose { namespace Words { class XmlNamespace; } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntryPageNumberInfo; } } }
namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { namespace Saving { class PsSaveOptions; } } }
namespace Aspose { namespace Words { namespace Saving { class HtmlFixedSaveOptions; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class HtmlShapeLayout; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlFramesetReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlVmlShapeReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlFramesetWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStyleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStylesheet; } } } } }
namespace Aspose { namespace Words { namespace Saving { class MetafileRenderingOptions; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxAltChunkReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ImageDataCore; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ImageDataUtil; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFileName; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldDate; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldPageRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTime; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldXE; } } }
namespace Aspose { namespace Words { namespace Fields { class ExternalActionUpdateLayout; } } }
namespace Aspose { namespace Words { namespace Markup { class SdtContentHelper; } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class LayoutOptionsCore; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutSpanPicture; } } } }
namespace Aspose { namespace Words { namespace BuildingBlocks { class GlossaryDocument; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }
namespace Aspose { namespace Words { namespace Saving { class SvgSaveOptions; } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { namespace Saving { class XamlFixedSaveOptions; } } }
namespace Aspose { namespace Words { namespace Saving { class PdfSaveOptions; } } }
namespace Aspose { namespace Words { namespace Saving { class XpsSaveOptions; } } }
namespace Aspose { namespace Words { namespace Themes { class Theme; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { class SaveInfo; } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { class LayoutApsBuilder; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class DmlShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { class FixedPageWriterBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtOleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlEquationXmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCustomizationsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxVbaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxCustomizationsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxVbaWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Customizations { class TcgReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Customizations { class TcgWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxFramesetWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumUtil; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace TableLayout { class ParagraphMeasurer; } } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace Rendering { class AsposeWordsPrintDocument; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Imaging { namespace Writer { class ImagingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtBinaryObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtImageWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Svg { namespace Writer { class SvgWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfLochHichDbchGroupWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Reader { class TxtReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFieldWriter; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxWebSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPrFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReaderContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfRunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfRunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriterContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFontsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlRunWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlWriter; } } } } }
namespace Aspose { namespace Words { enum class VbaDocumentEvents; } }
namespace Aspose { namespace Words { namespace Properties { class BuiltInDocumentProperties; } } }
namespace Aspose { namespace Words { namespace WebExtensions { class TaskPaneCollection; } } }
namespace Aspose { namespace Words { namespace Properties { class CustomDocumentProperties; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { namespace Settings { class DocumentProtection; } } }
namespace Aspose { namespace Words { class SectionCollection; } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace Settings { class ViewOptions; } } }
namespace Aspose { namespace Words { namespace Settings { class WriteProtection; } } }
namespace Aspose { namespace Words { namespace Settings { class CompatibilityOptions; } } }
namespace Aspose { namespace Words { namespace Settings { class MailMergeSettings; } } }
namespace Aspose { namespace Words { namespace Settings { class HyphenationOptions; } } }
namespace Aspose { namespace Words { namespace Markup { class CustomXmlPartCollection; } } }
namespace Aspose { namespace Words { namespace Markup { class CustomPartCollection; } } }
namespace Aspose { namespace Words { class AllocatedCommand; } }
namespace Aspose { namespace Words { class KeyMap; } }
namespace Aspose { namespace Words { class VariableCollection; } }
namespace Aspose { namespace Words { class DigitalSignatureCollection; } }
namespace Aspose { namespace Words { namespace Fonts { class FontSettings; } } }
namespace Aspose { namespace Words { namespace Frames { class FrameSet; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class RouteSlip; } } } }
namespace Aspose { namespace Words { class XmlNamespaceCollection; } }
namespace Aspose { namespace Words { class XmlSchemaReferenceCollection; } }
namespace Aspose { namespace Words { namespace Revisions { class EditSession; } } }
namespace Aspose { namespace Words { namespace Licensing { class VentureLicense; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class PageLayout; } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class CanvasInfo; } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace Layout { class LayoutOptions; } } }
namespace Aspose { namespace Words { class FootnoteOptions; } }
namespace Aspose { namespace Words { class EndnoteOptions; } }
namespace Aspose { namespace Words { namespace Fields { class FieldOptions; } } }
namespace Aspose { namespace Words { class VbaProject; } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabel; } } }
namespace Aspose { namespace Words { class LoadOptions; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { class ImportFormatOptions; } }
namespace Aspose { namespace Words { namespace Saving { class SaveOptions; } } }
namespace Aspose { namespace Words { class CleanupOptions; } }
namespace Aspose { namespace Words { class CompareOptions; } }
namespace Aspose { namespace Words { namespace Loading { class IResourceLoadingCallback; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class BookletSignature; } } } }
namespace Aspose { namespace Rendering { namespace Aps { class ApsPage; } } }
namespace Aspose { namespace Words { namespace Rendering { class ThumbnailGeneratingOptions; } } }
namespace Aspose { namespace Ss { class MemoryStorage; } }

namespace Aspose {

namespace Words {

/// Represents a Word document.
/// 
/// The <b>Document</b> is a central object in the Aspose.Words library.
/// 
/// To load an existing document in any of the <see cref="Aspose::Words::LoadFormat">LoadFormat</see> formats, pass a file name
/// or a stream into one of the <b>Document</b> constructors. To create a blank document, call the
/// constructor without parameters.
/// 
/// Use one of the Save method overloads to save the document in any of the
/// <see cref="Aspose::Words::SaveFormat">SaveFormat</see> formats.
/// 
/// To draw document pages directly onto a <b>Graphics</b> object use
/// <see cref="Aspose::Words::Document::RenderToScale(int32_t, System::SharedPtr<System::Drawing::Graphics>, float, float, float)">RenderToScale()</see> or <see cref="Aspose::Words::Document::RenderToSize(int32_t, System::SharedPtr<System::Drawing::Graphics>, float, float, float, float)">RenderToSize()</see> method.
/// 
/// To print the document, use one of the <see cref="Aspose::Words::Document::Print(System::String)">Print()</see> methods.
/// 
/// <see cref="Aspose::Words::Document::get_MailMerge">MailMerge</see> is the Aspose.Words's reporting engine that allows to populate
/// reports designed in Microsoft Word with data from various data sources quickly and easily.
/// The data can be from a DataSet, DataTable, DataView, IDataReader or an array of values.
/// <b>MailMerge</b> will go through the records found in the data source and insert them into
/// mail merge fields in the document growing it as necessary.
/// 
/// <b>Document</b> stores document-wide information such as <see cref="Aspose::Words::DocumentBase::get_Styles">Styles</see>,
/// <see cref="Aspose::Words::Document::get_BuiltInDocumentProperties">BuiltInDocumentProperties</see>, <see cref="Aspose::Words::Document::get_CustomDocumentProperties">CustomDocumentProperties</see>, lists and macros.
/// Most of these objects are accessible via the corresponding properties of the <b>Document</b>.
/// 
/// The <b>Document</b> is a root node of a tree that contains all other nodes of the document.
/// The tree is a Composite design pattern and in many ways similar to XmlDocument.
/// The content of the document can be manipulated freely programmatically:
/// 
/// - The nodes of the document can be accessed via typed collections, for example <see cref="Aspose::Words::Document::get_Sections">Sections</see>,
///   <see cref="Aspose::Words::ParagraphCollection">ParagraphCollection</see> etc.
/// - The nodes of the document can be selected by their node type using
///   <see cref="Aspose::Words::CompositeNode::GetChildNodes(Aspose::Words::NodeType, bool)">GetChildNodes()</see>
///   or using an XPath query with <see cref="Aspose::Words::CompositeNode::SelectNodes(System::String)">SelectNodes()</see> or <see cref="Aspose::Words::CompositeNode::SelectSingleNode(System::String)">SelectSingleNode()</see>.
/// - Content nodes can be added or removed from anywhere in the document using
///   <see cref="Aspose::Words::CompositeNode::InsertBefore(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertBefore()</see>, <see cref="Aspose::Words::CompositeNode::InsertAfter(System::SharedPtr<Aspose::Words::Node>, System::SharedPtr<Aspose::Words::Node>)">InsertAfter()</see>,
///   <see cref="Aspose::Words::CompositeNode::RemoveChild(System::SharedPtr<Aspose::Words::Node>)">RemoveChild()</see> and other
///   methods provided by the base class <see cref="Aspose::Words::CompositeNode">CompositeNode</see>.
/// - The formatting attributes of each node can be changed via the properties of that node.
/// 
/// Consider using <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see> that simplifies the task of programmatically creating
/// or populating the document tree.
/// 
/// The <b>Document</b> can contain only <see cref="Aspose::Words::Section">Section</see> objects.
/// 
/// In Microsoft Word, a valid document needs to have at least one section.
class ASPOSE_WORDS_SHARED_CLASS Document : public Aspose::Words::DocumentBase, public Aspose::Words::ISectionAttrSource, public Aspose::Words::IWatermarkProvider
{
    typedef Document ThisType;
    typedef Aspose::Words::DocumentBase BaseType;
    typedef Aspose::Words::ISectionAttrSource BaseType1;
    typedef Aspose::Words::IWatermarkProvider BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramTextBoxFitCache;
    friend class Aspose::Words::ApsBuilder::Math::MathContext;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Watermark;
    friend class Aspose::Words::Drawing::Core::Dml::DmlNodeCustomizer;
    friend class Aspose::Words::Fields::AutoTextEntryExtractor;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::RW::Chm::Reader::ChmReader;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::Fields::ToaEntryExtractor;
    friend class Aspose::Words::Validation::CustomXmlValidator;
    friend class Aspose::Words::RW::Html::HtmlFontNameResolver;
    friend class Aspose::Words::RW::RtfThemeWriter;
    friend class Aspose::Words::RW::Svg::Reader::SvgToEmfConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Docx::Reader::OpenXmlDocumentReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxReaderFactory;
    friend class Aspose::Words::RW::Wml::Reader::WmlEquationXmlConverter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Html::Css::New::StyleFormatterHtmlRules;
    friend class Aspose::Words::RW::Html::Writer::HtmlIFrameWriter;
    friend class Aspose::Words::RW::Mhtml::Reader::MhtmlReader;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReader;
    friend class Aspose::Words::MailMerging::MailMergeRegion;
    friend class Aspose::Words::PlainTextDocument;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Saving::PclSaveOptions;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::ApsBuilder::Dml::DmlPictureRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::ShapeEffects::DmlShapeEffectsRenderer;
    friend class Aspose::Words::ApsBuilder::Shapes::BackgroundShapeApsBuilder;
    friend class Aspose::Words::Layout::Core::SpanFieldContextMainText;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::Layout::LayoutCollector;
    friend class Aspose::Words::Layout::LayoutEnumerator;
    friend class Aspose::Words::DigitalSignatureUtil;
    friend class Aspose::Words::OoxmlComplianceInfo;
    friend class Aspose::Words::RangeDocumentBuilder;
    friend class Aspose::Words::XmlNamespace;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::IndexEntryPageNumberInfo;
    friend class Aspose::Words::Fonts::DocumentFontProvider;
    friend class Aspose::Words::Saving::PsSaveOptions;
    friend class Aspose::Words::Saving::HtmlFixedSaveOptions;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;
    friend class Aspose::Words::RW::Html::HtmlShapeLayout;
    friend class Aspose::Words::RW::Html::Reader::HtmlFramesetReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlVmlShapeReaderContext;
    friend class Aspose::Words::RW::Html::Writer::HtmlFramesetWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlStyleWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlStylesheet;
    friend class Aspose::Words::Saving::MetafileRenderingOptions;
    friend class Aspose::Words::RW::Docx::Reader::DocxAltChunkReader;
    friend class Aspose::Words::Drawing::Core::ImageDataCore;
    friend class Aspose::Words::Drawing::Core::ImageDataUtil;
    friend class Aspose::Words::Fields::FieldFileName;
    friend class Aspose::Words::Fields::FieldDate;
    friend class Aspose::Words::Fields::FieldPageRef;
    friend class Aspose::Words::Fields::FieldTime;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::FieldXE;
    friend class Aspose::Words::Fields::ExternalActionUpdateLayout;
    friend class Aspose::Words::Markup::SdtContentHelper;
    friend class Aspose::Words::Markup::XmlMapping;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::LayoutOptionsCore;
    friend class Aspose::Words::Layout::PreAps::LayoutSpanPicture;
    friend class Aspose::Words::BuildingBlocks::GlossaryDocument;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Fields::FieldUpdater;
    friend class Aspose::Words::Saving::SvgSaveOptions;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::Saving::XamlFixedSaveOptions;
    friend class Aspose::Words::Saving::PdfSaveOptions;
    friend class Aspose::Words::Saving::XpsSaveOptions;
    friend class Aspose::Words::Themes::Theme;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::SaveInfo;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::LayoutApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::DmlShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::RW::FixedPageWriterBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtOleWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlEquationXmlWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxCustomizationsReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxVbaReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxCustomizationsWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxVbaWriter;
    friend class Aspose::Words::RW::Doc::Customizations::TcgReader;
    friend class Aspose::Words::RW::Doc::Customizations::TcgWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxFramesetWriter;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::Fields::FieldNumUtil;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::TableLayout::ParagraphMeasurer;
    friend class Aspose::Words::Run;
    friend class Aspose::Words::Rendering::AsposeWordsPrintDocument;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Imaging::Writer::ImagingWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtBinaryObjectReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtImageWriter;
    friend class Aspose::Words::RW::Svg::Writer::SvgWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfLochHichDbchGroupWriter;
    friend class Aspose::Words::RW::Txt::Reader::TxtReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtSettingsReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFieldWriter;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Docx::Reader::DocxReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxWebSettingsReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxWriter;
    friend class Aspose::Words::RW::Doc::DocPrFiler;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtSettingsWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReaderContext;
    friend class Aspose::Words::RW::Rtf::Reader::RtfRunPrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfRunPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriterContext;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlFontsWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlRunWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlWriter;

public:
    using Aspose::Words::DocumentBase::Clone;

public:

    /// Gets or sets the full path of the template attached to the document.
    /// 
    /// Empty string means the document is attached to the Normal template.
    /// 
    /// @exception System::ArgumentNullException Throws if you attempt to set to a null value.
    /// 
    /// @sa Aspose::Words::Properties::BuiltInDocumentProperties::get_Template
    ASPOSE_WORDS_SHARED_API System::String get_AttachedTemplate();
    /// Setter for Aspose::Words::Document::get_AttachedTemplate
    ASPOSE_WORDS_SHARED_API void set_AttachedTemplate(System::String value);
    /// Gets a flag indicating whether the styles in the document are updated to match the styles in the
    /// attached template each time the document is opened in MS Word.
    ASPOSE_WORDS_SHARED_API bool get_AutomaticallyUpdateStyles();
    /// Sets a flag indicating whether the styles in the document are updated to match the styles in the
    /// attached template each time the document is opened in MS Word.
    ASPOSE_WORDS_SHARED_API void set_AutomaticallyUpdateStyles(bool value);
    /// Specifies whether to turn on the gray shading on form fields.
    ASPOSE_WORDS_SHARED_API bool get_ShadeFormData();
    /// Specifies whether to turn on the gray shading on form fields.
    ASPOSE_WORDS_SHARED_API void set_ShadeFormData(bool value);
    /// <b>True</b> if changes are tracked when this document is edited in Microsoft Word.
    /// 
    /// Setting this option only instructs Microsoft Word whether the track changes
    /// is turned on or off. This property has no effect on changes to the document that you make
    /// programmatically via Aspose.Words.
    /// 
    /// If you want to automatically track changes as they are made programmatically by Aspose.Words
    /// to this document use the <see cref="Aspose::Words::Document::StartTrackRevisions(System::String, System::DateTime)">StartTrackRevisions()</see> method.
    ASPOSE_WORDS_SHARED_API bool get_TrackRevisions();
    /// Setter for Aspose::Words::Document::get_TrackRevisions
    ASPOSE_WORDS_SHARED_API void set_TrackRevisions(bool value);
    /// Specifies whether to display grammar errors in this document.
    ASPOSE_WORDS_SHARED_API bool get_ShowGrammaticalErrors();
    /// Specifies whether to display grammar errors in this document.
    ASPOSE_WORDS_SHARED_API void set_ShowGrammaticalErrors(bool value);
    /// Specifies whether to display spelling errors in this document.
    ASPOSE_WORDS_SHARED_API bool get_ShowSpellingErrors();
    /// Specifies whether to display spelling errors in this document.
    ASPOSE_WORDS_SHARED_API void set_ShowSpellingErrors(bool value);
    /// Returns <b>NodeType.Document</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns a collection that represents all the built-in document properties of the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::BuiltInDocumentProperties> get_BuiltInDocumentProperties() const;
    /// Returns a collection that represents a list of task pane add-ins.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::WebExtensions::TaskPaneCollection> get_WebExtensionTaskPanes() const;
    /// Returns a collection that represents all the custom document properties of the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::CustomDocumentProperties> get_CustomDocumentProperties();
    /// Returns a <b>MailMerge</b> object that represents the mail merge functionality for the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::MailMerging::MailMerge> get_MailMerge();
    /// Gets the currently active document protection type.
    /// 
    /// This property allows to retrieve the currently set document protection type.
    /// To change the document protection type use the <see cref="Aspose::Words::Document::Protect(Aspose::Words::ProtectionType, System::String)">Protect()</see>
    /// and <see cref="Aspose::Words::Document::Unprotect">Unprotect</see> methods.
    /// 
    /// When a document is protected, the user can make only limited changes,
    /// such as adding annotations, making revisions, or completing a form.
    /// 
    /// Note that document protection is different from write protection.
    /// Write protection is specified using the <see cref="Aspose::Words::Document::get_WriteProtection">WriteProtection</see>
    /// 
    /// @sa Aspose::Words::Document::Protect(Aspose::Words::ProtectionType, System::String)
    /// @sa Aspose::Words::Document::Unprotect
    /// @sa Aspose::Words::Document::get_WriteProtection
    ASPOSE_WORDS_SHARED_API Aspose::Words::ProtectionType get_ProtectionType();
    /// Returns a collection that represents all sections in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::SectionCollection> get_Sections();
    /// Gets the first section in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> get_FirstSection();
    /// Gets the last section in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> get_LastSection();
    /// Provides options to control how the document is displayed in Microsoft Word.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::ViewOptions> get_ViewOptions();
    /// Provides access to the document write protection options.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::WriteProtection> get_WriteProtection();
    /// Provides access to document compatibility options (that is, the user preferences entered on the <b>Compatibility</b>
    /// tab of the <b>Options</b> dialog in Word).
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::CompatibilityOptions> get_CompatibilityOptions();
    /// Gets or sets the object that contains all of the mail merge information for a document.
    /// 
    /// You can use this object to specify a mail merge data source for a document and this information
    /// (along with the available data fields) will appear in Microsoft Word when the user opens this document.
    /// Or you can use this object to query mail merge settings that the user has specified in Microsoft Word
    /// for this document.
    /// 
    /// This object is never null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::MailMergeSettings> get_MailMergeSettings();
    /// Setter for Aspose::Words::Document::get_MailMergeSettings
    ASPOSE_WORDS_SHARED_API void set_MailMergeSettings(System::SharedPtr<Aspose::Words::Settings::MailMergeSettings> value);
    /// Provides access to document hyphenation options.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::HyphenationOptions> get_HyphenationOptions();
    /// Returns <b>true</b> if the document has any tracked changes.
    ASPOSE_WORDS_SHARED_API bool get_HasRevisions();
    /// Returns <b>true</b> if the document has a VBA project (macros).
    /// 
    /// @sa Aspose::Words::Document::RemoveMacros
    ASPOSE_WORDS_SHARED_API bool get_HasMacros();
    /// Provides access to the document watermark.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Watermark> get_Watermark();
    /// Gets the number of document versions that was stored in the DOC document.
    /// 
    /// Versions in Microsoft Word are accessed via the File/Versions menu. Microsoft Word supports
    /// versions only for DOC files.
    /// 
    /// This property allows to detect if there were document versions stored in this document
    /// before it was opened in Aspose.Words. Aspose.Words provides no other support for document versions.
    /// If you save this document using Aspose.Words, the document will be saved without versions.
    ASPOSE_WORDS_SHARED_API int32_t get_VersionsCount();
    /// Gets or sets the interval (in points) between the default tab stops.
    /// 
    /// @sa Aspose::Words::TabStopCollection
    /// @sa Aspose::Words::TabStop
    ASPOSE_WORDS_SHARED_API double get_DefaultTabStop();
    /// Setter for Aspose::Words::Document::get_DefaultTabStop
    ASPOSE_WORDS_SHARED_API void set_DefaultTabStop(double value);
    /// Gets the <see cref="Aspose::Words::Document::get_Theme">Theme</see> object for this document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::Theme> get_Theme();
    /// Gets or sets the collection of Custom XML Data Storage Parts.
    /// 
    /// Aspose.Words loads and saves Custom XML Parts into OOXML and DOC documents only.
    /// 
    /// This property cannot be <c>null</c>.
    /// 
    /// @sa Aspose::Words::Markup::CustomXmlPart
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomXmlPartCollection> get_CustomXmlParts() const;
    /// Setter for Aspose::Words::Document::get_CustomXmlParts
    ASPOSE_WORDS_SHARED_API void set_CustomXmlParts(System::SharedPtr<Aspose::Words::Markup::CustomXmlPartCollection> value);
    /// Gets or sets the collection of custom parts (arbitrary content) that are linked to the OOXML package using "unknown relationships".
    /// 
    /// Do not confuse these custom parts with Custom XML Data. If you need to access Custom XML parts,
    /// use the <see cref="Aspose::Words::Document::get_CustomXmlParts">CustomXmlParts</see> property.
    /// 
    /// This collection contains OOXML parts whose parent is the OOXML package and they targets are of an "unknown relationship".
    /// For more information see <see cref="Aspose::Words::Markup::CustomPart">CustomPart</see>.
    /// 
    /// Aspose.Words loads and saves custom parts into OOXML documents only.
    /// 
    /// This property cannot be <c>null</c>.
    /// 
    /// @sa Aspose::Words::Markup::CustomPart
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Markup::CustomPartCollection> get_PackageCustomParts() const;
    /// Setter for Aspose::Words::Document::get_PackageCustomParts
    ASPOSE_WORDS_SHARED_API void set_PackageCustomParts(System::SharedPtr<Aspose::Words::Markup::CustomPartCollection> value);
    /// Returns the collection of variables added to a document or template.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VariableCollection> get_Variables();
    /// Gets or sets the glossary document within this document or template. A glossary document is a storage
    /// for AutoText, AutoCorrect and Building Block entries defined in a document.
    /// 
    /// This property returns <c>null</c> if the document does not have a glossary document.
    /// 
    /// You can add a glossary document to a document by creating a
    /// <see cref="Aspose::Words::BuildingBlocks::GlossaryDocument">GlossaryDocument</see> object and assigning to this property.
    /// 
    /// @sa Aspose::Words::BuildingBlocks::GlossaryDocument
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BuildingBlocks::GlossaryDocument> get_GlossaryDocument() const;
    /// Setter for Aspose::Words::Document::get_GlossaryDocument
    ASPOSE_WORDS_SHARED_API void set_GlossaryDocument(System::SharedPtr<Aspose::Words::BuildingBlocks::GlossaryDocument> value);
    /// Gets the original file name of the document.
    /// 
    /// Returns null if the document was loaded from a stream or created blank.
    ASPOSE_WORDS_SHARED_API System::String get_OriginalFileName() const;
    /// Gets the format of the original document that was loaded into this object.
    /// 
    /// If you created a new blank document, returns the <see cref="Aspose::Words::LoadFormat::Doc">Doc</see> value.
    ASPOSE_WORDS_SHARED_API Aspose::Words::LoadFormat get_OriginalLoadFormat() const;
    /// Gets the OOXML compliance version determined from the loaded document content.
    /// Makes sense only for OOXML documents.
    /// 
    /// If you created a new blank document or load non OOXML document
    /// returns the <see cref="Aspose::Words::Saving::OoxmlCompliance::Ecma376_2006">Ecma376_2006</see> value.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::OoxmlCompliance get_Compliance();
    /// Gets the collection of digital signatures for this document and their validation results.
    /// 
    /// This collection contains digital signatures that were loaded from the original document.
    /// These digital signatures will not be saved when you save this <see cref="Aspose::Words::Document">Document</see> object
    /// into a file or stream because saving or converting will produce a document that is different from the
    /// original and the original digital signatures will no longer be valid.
    /// 
    /// This collection is never null. If the document is not signed, it will contain zero elements.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DigitalSignatureCollection> get_DigitalSignatures() const;
    /// Gets or sets document font settings.
    /// 
    /// This property allows to specify font settings per document. If set to null, default static font settings
    /// <see cref="Aspose::Words::Fonts::FontSettings::get_DefaultInstance">DefaultInstance</see> will be used.
    /// 
    /// The default value is null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::FontSettings> get_FontSettings() const;
    /// Setter for Aspose::Words::Document::get_FontSettings
    ASPOSE_WORDS_SHARED_API void set_FontSettings(System::SharedPtr<Aspose::Words::Fonts::FontSettings> value);
    /// Gets the number of pages in the document as calculated by the most recent page layout operation.
    /// 
    /// @sa Aspose::Words::Document::UpdatePageLayout
    ASPOSE_WORDS_SHARED_API int32_t get_PageCount();
    /// Gets a collection of revisions (tracked changes) that exist in this document.
    /// 
    /// The returned collection is a "live" collection, which means if you remove parts of a document that contain
    /// revisions, the deleted revisions will automatically disappear from this collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RevisionCollection> get_Revisions();
    /// Gets a <b>LayoutOptions</b> object that represents options to control the layout process of this document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Layout::LayoutOptions> get_LayoutOptions() const;
    /// Gets a value indicating whether to work with the original or revised version of a document.
    ASPOSE_WORDS_SHARED_API Aspose::Words::RevisionsView get_RevisionsView() const;
    /// Sets a value indicating whether to work with the original or revised version of a document.
    ASPOSE_WORDS_SHARED_API void set_RevisionsView(Aspose::Words::RevisionsView value);
    /// Provides options that control numbering and positioning of footnotes in this document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::FootnoteOptions> get_FootnoteOptions();
    /// Provides options that control numbering and positioning of endnotes in this document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::EndnoteOptions> get_EndnoteOptions();
    /// Gets a <b>FieldOptions</b> object that represents options to control field handling in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldOptions> get_FieldOptions();
    /// Gets a flag indicating that Microsoft Word will remove all user information from comments, revisions and
    /// document properties upon saving the document.
    ASPOSE_WORDS_SHARED_API bool get_RemovePersonalInformation();
    /// Sets a flag indicating that Microsoft Word will remove all user information from comments, revisions and
    /// document properties upon saving the document.
    ASPOSE_WORDS_SHARED_API void set_RemovePersonalInformation(bool value);
    /// Gets a <see cref="Aspose::Words::Document::get_VbaProject">VbaProject</see>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaProject> get_VbaProject() const;
    /// Sets a <see cref="Aspose::Words::Document::get_VbaProject">VbaProject</see>.
    ASPOSE_WORDS_SHARED_API void set_VbaProject(System::SharedPtr<Aspose::Words::VbaProject> value);

    /// Creates a blank Word document.
    /// 
    /// The document paper size is Letter by default. If you want to change page setup, use
    /// <see cref="Aspose::Words::Section::get_PageSetup">Section.PageSetup</see>.
    /// 
    /// After creation, you can use <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see> to add document content easily.
    ASPOSE_WORDS_SHARED_API Document();
    /// Opens an existing document from a file. Automatically detects the file format.
    /// 
    /// @param fileName File name of the document to open.
    /// 
    /// @exception Aspose::Words::UnsupportedFileFormatException The document format is not recognized or not supported.
    /// @exception Aspose::Words::FileCorruptedException The document appears to be corrupted and cannot be loaded.
    /// @exception System::Exception There is a problem with the document and it should be reported to Aspose.Words developers.
    /// @exception System::IO::IOException There is an input/output exception.
    /// @exception Aspose::Words::IncorrectPasswordException The document is encrypted and requires a password to open, but you supplied an incorrect password.
    /// @exception System::ArgumentException The name of the file cannot be null or empty string.
    ASPOSE_WORDS_SHARED_API Document(System::String fileName);
    /// Opens an existing document from a file. Allows to specify additional options such as an encryption password.
    /// 
    /// @param fileName File name of the document to open.
    /// @param loadOptions Additional options to use when loading a document. Can be null.
    /// 
    /// @exception Aspose::Words::UnsupportedFileFormatException The document format is not recognized or not supported.
    /// @exception Aspose::Words::FileCorruptedException The document appears to be corrupted and cannot be loaded.
    /// @exception System::Exception There is a problem with the document and it should be reported to Aspose.Words developers.
    /// @exception System::IO::IOException There is an input/output exception.
    /// @exception Aspose::Words::IncorrectPasswordException The document is encrypted and requires a password to open, but you supplied an incorrect password.
    /// @exception System::ArgumentException The name of the file cannot be null or empty string.
    ASPOSE_WORDS_SHARED_API Document(System::String fileName, System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);
    /// Opens an existing document from a stream. Automatically detects the file format.
    /// 
    /// The document must be stored at the beginning of the stream. The stream must support random positioning.
    /// 
    /// @param stream Stream where to load the document from.
    /// 
    /// @exception Aspose::Words::UnsupportedFileFormatException The document format is not recognized or not supported.
    /// @exception Aspose::Words::FileCorruptedException The document appears to be corrupted and cannot be loaded.
    /// @exception System::Exception There is a problem with the document and it should be reported to Aspose.Words developers.
    /// @exception System::IO::IOException There is an input/output exception.
    /// @exception Aspose::Words::IncorrectPasswordException The document is encrypted and requires a password to open, but you supplied an incorrect password.
    /// @exception System::ArgumentNullException The stream cannot be null.
    /// @exception System::NotSupportedException The stream does not support reading or seeking.
    /// @exception System::ObjectDisposedException The stream is a disposed object.
    ASPOSE_WORDS_SHARED_API Document(System::SharedPtr<System::IO::Stream> stream);
    /// Opens an existing document from a stream. Allows to specify additional options such as an encryption password.
    /// 
    /// The document must be stored at the beginning of the stream. The stream must support random positioning.
    /// 
    /// @param stream The stream where to load the document from.
    /// @param loadOptions Additional options to use when loading a document. Can be null.
    /// 
    /// @exception Aspose::Words::UnsupportedFileFormatException The document format is not recognized or not supported.
    /// @exception Aspose::Words::FileCorruptedException The document appears to be corrupted and cannot be loaded.
    /// @exception System::Exception There is a problem with the document and it should be reported to Aspose.Words developers.
    /// @exception System::IO::IOException There is an input/output exception.
    /// @exception Aspose::Words::IncorrectPasswordException The document is encrypted and requires a password to open, but you supplied an incorrect password.
    /// @exception System::ArgumentNullException The stream cannot be null.
    /// @exception System::NotSupportedException The stream does not support reading or seeking.
    /// @exception System::ObjectDisposedException The stream is a disposed object.
    ASPOSE_WORDS_SHARED_API Document(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);

    /// Performs a deep copy of the <see cref="Aspose::Words::Document">Document</see>.
    /// 
    /// @return The cloned document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> Clone();
    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Appends the specified document to the end of this document.
    /// 
    /// @param srcDoc The document to append.
    /// @param importFormatMode Specifies how to merge style formatting that clashes.
    ASPOSE_WORDS_SHARED_API void AppendDocument(System::SharedPtr<Aspose::Words::Document> srcDoc, Aspose::Words::ImportFormatMode importFormatMode);
    /// Appends the specified document to the end of this document.
    /// 
    /// @param srcDoc The document to append.
    /// @param importFormatMode Specifies how to merge style formatting that clashes.
    /// @param importFormatOptions Allows to specify options that affect formatting of a result document.
    ASPOSE_WORDS_SHARED_API void AppendDocument(System::SharedPtr<Aspose::Words::Document> srcDoc, Aspose::Words::ImportFormatMode importFormatMode, System::SharedPtr<Aspose::Words::ImportFormatOptions> importFormatOptions);
    /// Saves the document to a file. Automatically determines the save format from the extension.
    /// 
    /// @param fileName The name for the document. If a document with the
    ///     specified file name already exists, the existing document is overwritten.
    /// 
    /// @return Additional information that you can optionally use.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::String fileName);
    /// Saves the document to a file in the specified format.
    /// 
    /// @param fileName The name for the document. If a document with the
    ///     specified file name already exists, the existing document is overwritten.
    /// @param saveFormat The format in which to save the document.
    /// 
    /// @return Additional information that you can optionally use.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::String fileName, Aspose::Words::SaveFormat saveFormat);
    /// Saves the document to a file using the specified save options.
    /// 
    /// @param fileName The name for the document. If a document with the
    ///     specified file name already exists, the existing document is overwritten.
    /// @param saveOptions Specifies the options that control how the document is saved. Can be null.
    /// 
    /// @return Additional information that you can optionally use.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::String fileName, System::SharedPtr<Aspose::Words::Saving::SaveOptions> saveOptions);
    /// Saves the document to a stream using the specified format.
    /// 
    /// @param stream Stream where to save the document.
    /// @param saveFormat The format in which to save the document.
    /// 
    /// @return Additional information that you can optionally use.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::SharedPtr<System::IO::Stream> stream, Aspose::Words::SaveFormat saveFormat);
    /// Saves the document to a stream using the specified save options.
    /// 
    /// @param stream Stream where to save the document.
    /// @param saveOptions Specifies the options that control how the document is saved. Can be null.
    ///     If this is null, the document will be saved in the binary DOC format.
    /// 
    /// @return Additional information that you can optionally use.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::Saving::SaveOptions> saveOptions);
    /// If the document contains no sections, creates one section with one paragraph.
    ASPOSE_WORDS_SHARED_API void EnsureMinimum();
    /// Accepts all tracked changes in the document.
    ASPOSE_WORDS_SHARED_API void AcceptAllRevisions();
    /// Protects the document from changes without changing the existing password or assigns a random password.
    /// 
    /// When a document is protected, the user can make only limited changes,
    /// such as adding annotations, making revisions, or completing a form.
    /// 
    /// When you protect a document, and the document already has a protection password,
    /// the existing protection password is not changed.
    /// 
    /// When you protect a document, and the document does not have a protection password,
    /// this method assigns a random password that makes it impossible to unprotect the document
    /// in Microsoft Word, but you still can unprotect the document in Aspose.Words as it does not
    /// require a password when unprotecting.
    /// 
    /// @param type Specifies the protection type for the document.
    ASPOSE_WORDS_SHARED_API void Protect(Aspose::Words::ProtectionType type);
    /// Protects the document from changes and optionally sets a protection password.
    /// 
    /// When a document is protected, the user can make only limited changes,
    /// such as adding annotations, making revisions, or completing a form.
    /// 
    /// Note that document protection is different from write protection.
    /// Write protection is specified using the <see cref="Aspose::Words::Document::get_WriteProtection">WriteProtection</see>.
    /// 
    /// @param type Specifies the protection type for the document.
    /// @param password The password to protect the document with.
    ///     Specify null or empty string if you want to protect the document without a password.
    ASPOSE_WORDS_SHARED_API void Protect(Aspose::Words::ProtectionType type, System::String password);
    /// Removes protection from the document regardless of the password.
    /// 
    /// This method unprotects the document even if it has a protection password.
    /// 
    /// Note that document protection is different from write protection.
    /// Write protection is specified using the <see cref="Aspose::Words::Document::get_WriteProtection">WriteProtection</see>.
    ASPOSE_WORDS_SHARED_API void Unprotect();
    /// Removes protection from the document if a correct password is specified.
    /// 
    /// This method unprotects the document only if a correct password is specified.
    /// 
    /// Note that document protection is different from write protection.
    /// Write protection is specified using the <see cref="Aspose::Words::Document::get_WriteProtection">WriteProtection</see>.
    /// 
    /// @param password The password to unprotect the document with.
    /// 
    /// @return True if a correct password was specified and the document was unprotected.
    ASPOSE_WORDS_SHARED_API bool Unprotect(System::String password);
    /// Updates word count properties of the document.
    /// 
    /// <b>UpdateWordCount</b> recalculates and updates Characters, Words and Paragraphs
    /// properties in the <see cref="Aspose::Words::Document::get_BuiltInDocumentProperties">BuiltInDocumentProperties</see> collection of the <b>Document</b>.
    /// 
    /// Note that <b>UpdateWordCount</b> does not update number of lines and pages properties.
    /// Use the <see cref="Aspose::Words::Document::UpdateWordCount">UpdateWordCount</see> overload and pass True value as a parameter to do that.
    /// 
    /// When you use an evaluation version, the evaluation watermark will also be included
    /// in the word count.
    ASPOSE_WORDS_SHARED_API void UpdateWordCount();
    /// Updates word count properties of the document, optionally updates <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_Lines">Lines</see> property.
    /// 
    /// @param updateLinesCount True if number of lines in the document shall be calculated.
    ASPOSE_WORDS_SHARED_API void UpdateWordCount(bool updateLinesCount);
    /// Updates widths of cells and tables in the document according to their preferred widths and content.
    /// You do not need to call this method if the tables appear correct in the output document.
    /// 
    /// You do not normally need to call this method as cell and table widths are maintained automatically.
    /// You can call this method before exporting to PDF (or any other fixed-page format), only in rare cases
    /// where you confirmed that tables appear incorrectly laid out in the output document. Calling this method
    /// might help to correct the output.
    ASPOSE_WORDS_SHARED_API void UpdateTableLayout();
    /// Updates list labels for all list items in the document.
    /// 
    /// This method updates list label properties such as <see cref="Aspose::Words::Lists::ListLabel::get_LabelValue">LabelValue</see> and
    /// <see cref="Aspose::Words::Lists::ListLabel::get_LabelString">LabelString</see> for each <see cref="Aspose::Words::Paragraph::get_ListLabel">ListLabel</see> object in the document.
    /// 
    /// Also, this method is sometimes implicitly called when updating fields in the document. This is required
    /// because some fields that may reference list numbers (such as TOC or REF) need them be up-to-date.
    ASPOSE_WORDS_SHARED_API void UpdateListLabels();
    /// Removes all macros (the VBA project) as well as toolbars and command customizations from the document.
    /// 
    /// By removing all macros from a document you can ensure the document contains no macro viruses.
    ASPOSE_WORDS_SHARED_API void RemoveMacros();
    /// Updates the values of fields in the whole document.
    /// 
    /// When you open, modify and then save a document, Aspose.Words does not update fields automatically, it keeps them intact.
    /// Therefore, you would usually want to call this method before saving if you have modified the document
    /// programmatically and want to make sure the proper (calculated) field values appear in the saved document.
    /// 
    /// There is no need to update fields after executing a mail merge because mail merge is a kind of field update
    /// and automatically updates all fields in the document.
    /// 
    /// This method does not update all field types. For the detailed list of supported field types, see the Programmers Guide.
    /// 
    /// This method does not update fields that are related to the page layout algorithms (e.g. PAGE, PAGES, PAGEREF).
    /// The page layout-related fields are updated when you render a document or call <see cref="Aspose::Words::Document::UpdatePageLayout">UpdatePageLayout</see>.
    /// 
    /// Use the <see cref="Aspose::Words::Document::NormalizeFieldTypes">NormalizeFieldTypes</see> method before fields updating if there were document changes that affected field types.
    /// 
    /// To update fields in a specific part of the document use <see cref="Aspose::Words::Range::UpdateFields">UpdateFields</see>.
    ASPOSE_WORDS_SHARED_API void UpdateFields();
    /// Unlinks fields in the whole document.
    /// 
    /// Replaces all the fields in the whole document with their most recent results.
    /// 
    /// To unlink fields in a specific part of the document use <see cref="Aspose::Words::Range::UnlinkFields">UnlinkFields</see>.
    ASPOSE_WORDS_SHARED_API void UnlinkFields();
    /// Changes field type values <see cref="Aspose::Words::Fields::FieldChar::get_FieldType">FieldType</see> of <see cref="Aspose::Words::Fields::FieldStart">FieldStart</see>, <see cref="Aspose::Words::Fields::FieldSeparator">FieldSeparator</see>, <see cref="Aspose::Words::Fields::FieldEnd">FieldEnd</see>
    /// in the whole document so that they correspond to the field types contained in the field codes.
    /// 
    /// Use this method after document changes that affect field types.
    /// 
    /// To change field type values in a specific part of the document use <see cref="Aspose::Words::Range::NormalizeFieldTypes">NormalizeFieldTypes</see>.
    ASPOSE_WORDS_SHARED_API void NormalizeFieldTypes();
    /// Joins runs with same formatting in all paragraphs of the document.
    /// 
    /// This is an optimization method. Some documents contain adjacent runs with same formatting.
    /// Usually this occurs if a document was intensively edited manually.
    /// You can reduce the document size and speed up further processing by joining these runs.
    /// 
    /// The operation checks every <see cref="Aspose::Words::Paragraph">Paragraph</see> node in the document for adjacent <see cref="Aspose::Words::Run">Run</see>
    /// nodes having identical properties. It ignores unique identifiers used to track editing sessions of run
    /// creation and modification. First run in every joining sequence accumulates all text. Remaining
    /// runs are deleted from the document.
    /// 
    /// @return Number of joins performed. When <b>N</b> adjacent runs are being joined they count as <b>N - 1</b> joins.
    ASPOSE_WORDS_SHARED_API int32_t JoinRunsWithSameFormatting();
    /// Converts formatting specified in table styles into direct formatting on tables in the document.
    /// 
    /// This method exists because this version of Aspose.Words provides only limited support for
    /// table styles (see below). This method might be useful when you load a DOCX or WordprocessingML
    /// document that contains tables formatted with table styles and you need to query formatting of
    /// tables, cells, paragraphs or text.
    /// 
    /// This version of Aspose.Words provides limited support for table styles as follows:
    /// 
    /// - Table styles defined in DOCX or WordprocessingML documents are preserved as table styles
    ///   when saving the document as DOCX or WordprocessingML.
    /// - Table styles defined in DOCX or WordprocessingML documents are automatically converted
    ///   to direct formatting on tables when saving the document into any other format,
    ///   rendering or printing.
    /// - Table styles defined in DOC documents are preserved as table styles when
    ///   saving the document as DOC only.
    ASPOSE_WORDS_SHARED_API void ExpandTableStylesToDirectFormatting();
    /// Cleans unused styles and lists from the document.
    ASPOSE_WORDS_SHARED_API void Cleanup();
    /// Cleans unused styles and lists from the document depending on given <see cref="Aspose::Words::CleanupOptions">CleanupOptions</see>.
    ASPOSE_WORDS_SHARED_API void Cleanup(System::SharedPtr<Aspose::Words::CleanupOptions> options);
    /// Removes external XML schema references from this document.
    ASPOSE_WORDS_SHARED_API void RemoveExternalSchemaReferences();
    /// Starts automatically marking all further changes you make to the document programmatically as revision changes.
    /// 
    /// If you call this method and then make some changes to the document programmatically,
    /// save the document and later open the document in MS Word you will see these changes as revisions.
    /// 
    /// Currently Aspose.Words supports tracking of node insertions and deletions only. Formatting changes are not
    /// recorded as revisions.
    /// 
    /// Automatic tracking of changes is supported both when modifying this document through node manipulations
    /// as well as when using <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see>
    /// 
    /// This method does not change the <see cref="Aspose::Words::Document::get_TrackRevisions">TrackRevisions</see> option and does not use its value
    /// for the purposes of revision tracking.
    /// 
    /// @param author Initials of the author to use for revisions.
    /// @param dateTime The date and time to use for revisions.
    /// 
    /// @sa Aspose::Words::Document::StopTrackRevisions
    ASPOSE_WORDS_SHARED_API void StartTrackRevisions(System::String author, System::DateTime dateTime);
    /// Starts automatically marking all further changes you make to the document programmatically as revision changes.
    /// 
    /// If you call this method and then make some changes to the document programmatically,
    /// save the document and later open the document in MS Word you will see these changes as revisions.
    /// 
    /// Currently Aspose.Words supports tracking of node insertions and deletions only. Formatting changes are not
    /// recorded as revisions.
    /// 
    /// Automatic tracking of changes is supported both when modifying this document through node manipulations
    /// as well as when using <see cref="Aspose::Words::DocumentBuilder">DocumentBuilder</see>
    /// 
    /// This method does not change the <see cref="Aspose::Words::Document::get_TrackRevisions">TrackRevisions</see> option and does not use its value
    /// for the purposes of revision tracking.
    /// 
    /// @param author Initials of the author to use for revisions.
    /// 
    /// @sa Aspose::Words::Document::StopTrackRevisions
    ASPOSE_WORDS_SHARED_API void StartTrackRevisions(System::String author);
    /// Stops automatic marking of document changes as revisions.
    /// 
    /// @sa Aspose::Words::Document::StartTrackRevisions(System::String, System::DateTime)
    ASPOSE_WORDS_SHARED_API void StopTrackRevisions();
    /// Compares this document with another document producing changes as number of edit and format revisions <see cref="Aspose::Words::Revision">Revision</see>.
    /// 
    /// The following document nodes are not compared at the moment:
    /// 
    /// - <see cref="Aspose::Words::Markup::StructuredDocumentTag">StructuredDocumentTag</see>
    /// - Item3
    /// 
    /// @note Documents must not have revisions before comparison.
    /// 
    /// @param document Document to compare.
    /// @param author Initials of the author to use for revisions.
    /// @param dateTime The date and time to use for revisions.
    ASPOSE_WORDS_SHARED_API void Compare(System::SharedPtr<Aspose::Words::Document> document, System::String author, System::DateTime dateTime);
    /// Compares this document with another document producing changes as a number of edit and format revisions <see cref="Aspose::Words::Revision">Revision</see>.
    /// Allows to specify comparison options using <see cref="Aspose::Words::CompareOptions">CompareOptions</see>.
    ASPOSE_WORDS_SHARED_API void Compare(System::SharedPtr<Aspose::Words::Document> document, System::String author, System::DateTime dateTime, System::SharedPtr<Aspose::Words::CompareOptions> options);
    /// Copies styles from the specified template to a document.
    ASPOSE_WORDS_SHARED_API void CopyStylesFromTemplate(System::String template_);
    /// Copies styles from the specified template to a document.
    ASPOSE_WORDS_SHARED_API void CopyStylesFromTemplate(System::SharedPtr<Aspose::Words::Document> template_);
    /// Rebuilds the page layout of the document.
    /// 
    /// This method formats a document into pages and updates the page number related fields in the document such
    /// as PAGE, PAGES, PAGEREF and REF. The up-to-date page layout information is required for a correct rendering of the document
    /// to fixed-page formats.
    /// 
    /// This method is automatically invoked when you first convert a document to PDF, XPS, image or print it.
    /// However, if you modify the document after rendering and then attempt to render it again - Aspose.Words will not
    /// update the page layout automatically. In this case you should call <see cref="Aspose::Words::Document::UpdatePageLayout">UpdatePageLayout</see> before
    /// rendering again.
    ASPOSE_WORDS_SHARED_API void UpdatePageLayout();
    /// Renders a document page into a <see cref="System::Drawing::Graphics">Graphics</see> object to a specified scale.
    /// 
    /// @param pageIndex The 0-based page index.
    /// @param graphics The object where to render to.
    /// @param x The X coordinate (in world units) of the top left corner of the rendered page.
    /// @param y The Y coordinate (in world units) of the top left corner of the rendered page.
    /// @param scale The scale for rendering the page (1.0 is 100\%).
    /// 
    /// @return The width and height (in world units) of the rendered page.
    ASPOSE_WORDS_SHARED_API System::Drawing::SizeF RenderToScale(int32_t pageIndex, System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float scale);
    /// Renders a document page into a <see cref="System::Drawing::Graphics">Graphics</see> object to a specified size.
    /// 
    /// @param pageIndex The 0-based page index.
    /// @param graphics The object where to render to.
    /// @param x The X coordinate (in world units) of the top left corner of the rendered page.
    /// @param y The Y coordinate (in world units) of the top left corner of the rendered page.
    /// @param width The maximum width (in world units) that can be occupied by the rendered page.
    /// @param height The maximum height (in world units) that can be occupied by the rendered page.
    /// 
    /// @return The scale that was automatically calculated for the rendered page to fit the specified size.
    ASPOSE_WORDS_SHARED_API float RenderToSize(int32_t pageIndex, System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float width, float height);
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::Drawing::Shape> watermark) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Shape> Get() override;
    /// Removes itself from the parent.
    ASPOSE_WORDS_SHARED_API void Remove() override;
    /// Gets the page size, orientation and other information about a page that might be useful for printing or rendering.
    /// 
    /// @param pageIndex The 0-based page index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Rendering::PageInfo> GetPageInfo(int32_t pageIndex);
    /// Updates <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_Thumbnail">Thumbnail</see> of the document according to the specified options.
    /// 
    /// @param options The generating options to use.
    ASPOSE_WORDS_SHARED_API void UpdateThumbnail(System::SharedPtr<Aspose::Words::Rendering::ThumbnailGeneratingOptions> options);
    /// Updates <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_Thumbnail">Thumbnail</see> of the document using default options.
    ASPOSE_WORDS_SHARED_API void UpdateThumbnail();
    /// Returns the <see cref="Aspose::Words::Document">Document</see> object representing specified range of pages.
    /// 
    /// @param index The zero-based index of the first page to extract.
    /// @param count Number of pages to be extracted.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> ExtractPages(int32_t index, int32_t count);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchSectionAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetSectionAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearSectionAttrs() override;

protected:

    System::SharedPtr<Aspose::Words::Settings::DocumentProtection> get_DocumentProtection();
    bool get_HasCustomizations();
    bool get_HasAttachedToolbars();
    bool get_HasAllocatedCommands();
    bool get_HasKeyMaps();
    System::ArrayPtr<uint8_t> get_DropdownStrings() const;
    void set_DropdownStrings(System::ArrayPtr<uint8_t> value);
    System::SharedPtr<System::IO::MemoryStream> get_CompObj() const;
    void set_CompObj(System::SharedPtr<System::IO::MemoryStream> value);
    System::ArrayPtr<uint8_t> get_MsoEnvelope() const;
    void set_MsoEnvelope(System::ArrayPtr<uint8_t> value);
    System::ArrayPtr<uint8_t> get_AttachedToolbars() const;
    void set_AttachedToolbars(System::ArrayPtr<uint8_t> value);
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::AllocatedCommand>>> get_AllocatedCommands() const;
    void set_AllocatedCommands(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::AllocatedCommand>>> value);
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::KeyMap>>> get_KeyMaps() const;
    void set_KeyMaps(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::KeyMap>>> value);
    Aspose::Words::VbaDocumentEvents get_VbaDocumentEvents() const;
    void set_VbaDocumentEvents(Aspose::Words::VbaDocumentEvents value);
    System::String get_BaseUri() const;
    void set_BaseUri(System::String value);
    System::String get_SavedFileName() const;
    System::SharedPtr<Aspose::Words::Fonts::FontSettings> get_EffectiveFontSettings();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::DocumentFontProvider> get_FontProvider() override;
    System::SharedPtr<Aspose::Words::Frames::FrameSet> get_FrameSet() const;
    void set_FrameSet(System::SharedPtr<Aspose::Words::Frames::FrameSet> value);
    System::SharedPtr<Aspose::Words::RW::Doc::RouteSlip> get_RouteSlip() const;
    void set_RouteSlip(System::SharedPtr<Aspose::Words::RW::Doc::RouteSlip> value);
    System::SharedPtr<Aspose::Words::XmlNamespaceCollection> get_XmlNamespaces();
    System::SharedPtr<Aspose::Words::XmlSchemaReferenceCollection> get_XmlSchemaReferences();
    System::SharedPtr<Aspose::Words::Revisions::EditSession> get_EditSession() const;
    void set_EditSession(System::SharedPtr<Aspose::Words::Revisions::EditSession> value);
    System::SharedPtr<Aspose::Words::Licensing::VentureLicense> get_VentureLicense() const;
    void set_VentureLicense(System::SharedPtr<Aspose::Words::Licensing::VentureLicense> value);
    ASPOSE_WORDS_SHARED_API bool get_IsTrackRevisionsEnabled() override;
    System::SharedPtr<Aspose::Words::OoxmlComplianceInfo> get_ComplianceInfo() const;
    void set_ComplianceInfo(System::SharedPtr<Aspose::Words::OoxmlComplianceInfo> value);
    bool get_HasPageLayout();
    System::SharedPtr<Aspose::Words::Layout::PreAps::PageLayout> get_PageLayout();
    System::SharedPtr<Aspose::Words::Layout::LayoutOptionsCore> get_LayoutOptionsCore() const;
    System::SharedPtr<System::Collections::Generic::IDictionary<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>, System::SharedPtr<Aspose::Words::ApsBuilder::Shapes::CanvasInfo>>> get_CanvasCache();
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<System::Object>>> get_OpenGLGraphicsContextCache();
    System::DateTime get_CurrentDateTimeCache();

    static const int32_t VbaSignaturePrefixLength;
    int64_t FixedPageFormatStoringMilliseconds;

    Document(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::LoadOptions> loadOptions, bool increaseCredit);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::Theme> GetThemeInternal() override;
    void SetThemeInternal(System::SharedPtr<Aspose::Words::Themes::Theme> theme);
    int32_t GetNextTocEntryBookmarkIndex();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    virtual ASPOSE_WORDS_SHARED_API void WriteDocToHttResponse(System::SharedPtr<System::Web::HttpResponse> response, System::ArrayPtr<uint8_t> docData, int32_t dataLength, System::String headerName, System::String headerValue);
    System::String MapFileName(System::String fileName);
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    void Load(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);
    static System::SharedPtr<System::IO::Stream> OpenDocumentStream(System::String pathOrUri, System::SharedPtr<Aspose::Words::Loading::IResourceLoadingCallback> resourceLoadingCallback);
    void LoadBlank();
    ASPOSE_WORDS_SHARED_API void SuspendTrackRevisions() override;
    ASPOSE_WORDS_SHARED_API void ResumeTrackRevisions() override;
    void UpdateStyles(System::SharedPtr<Aspose::Words::Saving::SaveOptions> so);
    System::SharedPtr<Aspose::Words::Node> GetNodeById(System::String id);
    System::SharedPtr<Aspose::Words::Layout::PreAps::PageLayout> GetPageLayout(bool build);
    void InvalidateLayoutDocument();
    System::Drawing::SizeF RenderBookletSignatureToScale(System::SharedPtr<Aspose::Words::Layout::Core::BookletSignature> bookletSignature, System::Drawing::SizeF bookletPageSize, int32_t paperTray, System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float scale);
    float RenderBookletSignatureToSize(System::SharedPtr<Aspose::Words::Layout::Core::BookletSignature> bookletSignature, System::Drawing::SizeF bookletPageSize, int32_t paperTray, System::SharedPtr<System::Drawing::Graphics> graphics, float x, float y, float width, float height);
    System::SharedPtr<Aspose::Rendering::Aps::ApsPage> LayoutPageToAps(int32_t pageIndex);
    System::SharedPtr<Aspose::Rendering::Aps::ApsPage> LayoutPageToAps(int32_t pageIndex, System::SharedPtr<Aspose::Words::ApsBuilder::LayoutApsBuilder> apsBuilder);
    System::SharedPtr<Aspose::Rendering::Aps::ApsPage> GetBookletPageAps(System::SharedPtr<Aspose::Words::Layout::Core::BookletSignature> bookletSheet, System::Drawing::SizeF bookletPageSize, int32_t paperTray);
    System::SharedPtr<Aspose::Rendering::Aps::ApsPage> GetBookletPageAps(System::SharedPtr<Aspose::Words::Layout::Core::BookletSignature> bookletSheet, System::Drawing::SizeF bookletPageSize, int32_t paperTray, System::SharedPtr<Aspose::Words::ApsBuilder::LayoutApsBuilder> apsBuilder);
    void CheckPageIndexAndCount(int32_t pageIndex, int32_t pageCount);
    void ReadVbaProject(System::SharedPtr<Aspose::Ss::MemoryStorage> storage, System::ArrayPtr<uint8_t> signature);
    System::SharedPtr<Aspose::Words::Fields::FieldNumListLabel> GetFieldNumListLabel(System::SharedPtr<Aspose::Words::Fields::FieldStart> fieldStart);
    void SetFieldNumListLabel(System::SharedPtr<Aspose::Words::Fields::FieldStart> fieldStart, System::SharedPtr<Aspose::Words::Fields::FieldNumListLabel> label);
    void ClearFieldNumListLabels();

    virtual ASPOSE_WORDS_SHARED_API ~Document();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    static System::ArrayPtr<char16_t>& gDirectoryDelimiters();

    System::SharedPtr<Aspose::Words::Properties::BuiltInDocumentProperties> mBuiltInDocumentProperties;
    System::SharedPtr<Aspose::Words::Properties::CustomDocumentProperties> mCustomDocumentProperties;
    System::SharedPtr<Aspose::Words::BuildingBlocks::GlossaryDocument> mGlossary;
    System::SharedPtr<Aspose::Words::Themes::Theme> mTheme;
    System::SharedPtr<Aspose::Words::Markup::CustomXmlPartCollection> mCustomXmlParts;
    System::SharedPtr<Aspose::Words::Markup::CustomPartCollection> mPackageCustomParts;
    System::SharedPtr<Aspose::Words::DigitalSignatureCollection> mDigitalSignatures;
    System::SharedPtr<Aspose::Words::Frames::FrameSet> mFrame;
    System::ArrayPtr<uint8_t> mDropdownStrings;
    System::SharedPtr<System::IO::MemoryStream> mCompObj;
    System::ArrayPtr<uint8_t> mMsoEnvelope;
    System::ArrayPtr<uint8_t> mAttachedToolbars;
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::AllocatedCommand>>> mAllocatedCommands;
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::KeyMap>>> mKeyMaps;
    Aspose::Words::VbaDocumentEvents mVbaDocumentEvents;
    System::SharedPtr<Aspose::Words::VbaProject> mVbaProject;
    System::String mOriginalFileName;
    System::String mSavedFileName;
    Aspose::Words::LoadFormat mOriginalLoadFormat;
    System::String mBaseUri;
    System::SharedPtr<Aspose::Words::Licensing::VentureLicense> mVentureLicense;
    System::SharedPtr<Aspose::Words::Layout::LayoutOptionsCore> mLayoutOptionsCore;
    System::SharedPtr<Aspose::Words::Layout::LayoutOptions> mLayoutOptions;
    System::SharedPtr<Aspose::Words::SectionCollection> mSectionsCache;
    System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mMailMergeCache;
    System::SharedPtr<Aspose::Words::Layout::PreAps::PageLayout> mLayoutCache;
    System::SharedPtr<System::Collections::Generic::IDictionary<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>, System::SharedPtr<Aspose::Words::ApsBuilder::Shapes::CanvasInfo>>> mCanvasCache;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<System::Object>>> mOpenGLGraphicsContextCache;
    System::SharedPtr<Aspose::Words::FootnoteOptions> mFootnoteOptionsCache;
    System::SharedPtr<Aspose::Words::EndnoteOptions> mEndnoteOptionsCache;
    System::SharedPtr<Aspose::Words::RevisionCollection> mRevisionsCache;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::SharedPtr<Aspose::Words::Fields::FieldStart>, System::SharedPtr<Aspose::Words::Fields::FieldNumListLabel>>> mFieldNumListLabels;
    int32_t mTocEntryBookmarkIndex;
    System::SharedPtr<Aspose::Words::Fields::FieldOptions> mFieldOptionsCache;
    System::SharedPtr<Aspose::Words::RW::Doc::RouteSlip> mRouteSlip;
    System::SharedPtr<Aspose::Words::OoxmlComplianceInfo> mComplianceInfo;
    bool mTrackRevisionsEnabled;
    int32_t mIsTrackRevisionsLockCount;
    System::SharedPtr<Aspose::Words::Revisions::EditSession> mEditSession;
    System::DateTime mCurrentDateTimeCache;
    System::SharedPtr<Aspose::Words::Fonts::FontSettings> mFontSettings;
    System::SharedPtr<Aspose::Words::Fonts::DocumentFontProvider> mFontProvider;
    Aspose::Words::RevisionsView mRevisionsView;
    System::SharedPtr<Aspose::Words::WebExtensions::TaskPaneCollection> mWebExtensionTaskPanes;
    System::SharedPtr<Aspose::Words::Watermark> mWatermark;
    System::SharedPtr<Aspose::Words::PageExtractor> mPageExtractor;

    static void __StaticConstructor__();

    void ResolveSdtXmlMapping();
    System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> Save(System::SharedPtr<System::IO::Stream> stream, System::String fileName, System::SharedPtr<Aspose::Words::Saving::SaveOptions> saveOptions);
    void SetupLoadWarningCallback(System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);
    void AutoUpdateTrackRevisions();
    void LoadCore(System::SharedPtr<System::IO::Stream> stream, System::SharedPtr<Aspose::Words::LoadOptions> loadOptions);
    System::SharedPtr<Aspose::Words::Saving::SaveOutputParameters> SaveCore(System::SharedPtr<Aspose::Words::SaveInfo> saveInfo);
    void UpdateFieldsBeforeSave();
    void SetLocaleDefaultsForNewDocument();
    void RemoveSectionUnlock(Aspose::Words::ProtectionType type);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
