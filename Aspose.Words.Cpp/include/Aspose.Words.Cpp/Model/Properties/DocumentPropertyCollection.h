//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Properties/DocumentPropertyCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Properties/DocumentProperty.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Properties { class CustomDocumentProperties; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtMetaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCustomPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPropertiesFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfUserPropertiesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlDocPropertiesReader; } } } } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedStringListGeneric; } } }

namespace Aspose {

namespace Words {

namespace Properties {

/// Base class for <see cref="Aspose::Words::Properties::BuiltInDocumentProperties">BuiltInDocumentProperties</see> and <see cref="Aspose::Words::Properties::CustomDocumentProperties">CustomDocumentProperties</see> collections.
/// 
/// The names of the properties are case-insensitive.
/// 
/// The properties in the collection are sorted alphabetically by name.
/// 
/// @sa Aspose::Words::Properties::BuiltInDocumentProperties
/// @sa Aspose::Words::Properties::CustomDocumentProperties
class ASPOSE_WORDS_SHARED_CLASS DocumentPropertyCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Properties::DocumentProperty>>
{
    typedef DocumentPropertyCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Properties::DocumentProperty>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Properties::CustomDocumentProperties;
    friend class Aspose::Words::RW::Odt::Reader::OdtMetaReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxCustomPropertiesReader;
    friend class Aspose::Words::RW::Doc::DocPropertiesFiler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfUserPropertiesHandler;
    friend class Aspose::Words::RW::Wml::Reader::WmlDocPropertiesReader;

public:

    /// Gets number of items in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Returns a <see cref="Aspose::Words::Properties::DocumentProperty">DocumentProperty</see> object by the name of the property.
    /// 
    /// Returns null if a property with the specified name is not found.
    /// 
    /// @param name The case-insensitive name of the property to retrieve.
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> idx_get(System::String name);
    /// Returns a <see cref="Aspose::Words::Properties::DocumentProperty">DocumentProperty</see> object by index.
    /// 
    /// @param index Zero-based index of the <see cref="Aspose::Words::Properties::DocumentProperty">DocumentProperty</see> to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> idx_get(int32_t index);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Properties::DocumentProperty>>> GetEnumerator() override;
    /// Returns true if a property with the specified name exists in the collection.
    /// 
    /// @param name The case-insensitive name of the property.
    /// 
    /// @return True if the property exists in the collection; false otherwise.
    ASPOSE_WORDS_SHARED_API bool Contains(System::String name);
    /// Gets the index of a property by name.
    /// 
    /// @param name The case-insensitive name of the property.
    /// 
    /// @return The zero based index. Negative value if not found.
    ASPOSE_WORDS_SHARED_API int32_t IndexOf(System::String name);
    /// Removes a property with the specified name from the collection.
    /// 
    /// @param name The case-insensitive name of the property.
    ASPOSE_WORDS_SHARED_API void Remove(System::String name);
    /// Removes a property at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all properties from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();

protected:

    DocumentPropertyCollection();

    System::SharedPtr<Aspose::Words::Properties::DocumentProperty> AddSafe(System::String name, System::SharedPtr<System::Object> value);
    System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, System::SharedPtr<System::Object> value);
    System::SharedPtr<Aspose::Words::Properties::DocumentPropertyCollection> Clone();
    virtual System::SharedPtr<Aspose::Words::Properties::DocumentPropertyCollection> Create() = 0;

    virtual ASPOSE_WORDS_SHARED_API ~DocumentPropertyCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::Generic::SortedStringListGeneric<System::SharedPtr<Aspose::Words::Properties::DocumentProperty>>> mItems;

};

}
}
}
