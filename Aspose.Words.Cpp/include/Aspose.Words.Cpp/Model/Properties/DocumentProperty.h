//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Properties/DocumentProperty.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/date_time.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Properties/PropertyType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Properties { class BuiltInDocumentProperties; } } }
namespace Aspose { namespace Words { namespace Properties { class DocumentPropertyCollection; } } }
namespace Aspose { namespace Words { namespace Properties { class CustomDocumentProperties; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxCustomPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxCustomPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPropertiesFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtMetaWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfUserPropertiesHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfInfoGroupWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlDocPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxDocPropertiesWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Properties {

/// Represents a custom or built-in document property.
/// 
/// @sa Aspose::Words::Properties::DocumentPropertyCollection
class ASPOSE_WORDS_SHARED_CLASS DocumentProperty : public System::Object
{
    typedef DocumentProperty ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Properties::BuiltInDocumentProperties;
    friend class Aspose::Words::Properties::DocumentPropertyCollection;
    friend class Aspose::Words::Properties::CustomDocumentProperties;
    friend class Aspose::Words::RW::Docx::Reader::DocxCustomPropertiesReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxCustomPropertiesWriter;
    friend class Aspose::Words::RW::Doc::DocPropertiesFiler;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtMetaWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfUserPropertiesHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfInfoGroupWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlDocPropertiesReader;
    friend class Aspose::Words::RW::Nrx::Writer::NrxDocPropertiesWriter;

public:

    /// Returns the name of the property.
    /// 
    /// Cannot be null and cannot be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Gets the value of the property.
    /// 
    /// Cannot be null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> get_Value();
    /// Sets the value of the property.
    /// 
    /// Cannot be null.
    ASPOSE_WORDS_SHARED_API void set_Value(System::SharedPtr<System::Object> value);
    /// Gets the data type of the property.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Properties::PropertyType get_Type() const;
    /// Gets the source of a linked custom document property.
    ASPOSE_WORDS_SHARED_API System::String get_LinkSource() const;
    /// Shows whether this property is linked to content or not.
    ASPOSE_WORDS_SHARED_API bool get_IsLinkToContent();

    /// Returns the property value as a string formatted according to the current locale.
    /// 
    /// Converts a boolean property into "Y" or "N".
    /// Converts a date property into a short date string.
    /// For all other types converts a property using Object.ToString().
    ASPOSE_WORDS_SHARED_API System::String ToString() const override;
    /// Returns the property value as integer.
    ASPOSE_WORDS_SHARED_API int32_t ToInt();
    /// Returns the property value as double.
    ASPOSE_WORDS_SHARED_API double ToDouble();
    /// Returns the property value as DateTime in UTC.
    /// 
    /// Throws an exception if the property type is not <see cref="Aspose::Words::Properties::PropertyType::DateTime">DateTime</see>.
    /// 
    /// Microsoft Word stores only the date part (no time) for custom date properties.
    ASPOSE_WORDS_SHARED_API System::DateTime ToDateTime();
    /// Returns the property value as bool.
    /// 
    /// Throws an exception if the property type is not <see cref="Aspose::Words::Properties::PropertyType::Boolean">Boolean</see>.
    ASPOSE_WORDS_SHARED_API bool ToBool();
    /// Returns the property value as byte array.
    /// 
    /// Throws an exception if the property type is not <see cref="Aspose::Words::Properties::PropertyType::ByteArray">ByteArray</see>.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> ToByteArray();

protected:

    System::SharedPtr<System::Object> get_DefaultValueInternal() const;
    void set_DefaultValueInternal(System::SharedPtr<System::Object> value);
    System::SharedPtr<System::Object> get_ValueInternal() const;
    void set_ValueInternal(System::SharedPtr<System::Object> value);
    System::String get_LinkTarget() const;
    void set_LinkTarget(System::String value);

    static const int32_t MinUserPropId;

    DocumentProperty(System::String name, System::SharedPtr<System::Object> value);

    void FromString(System::String value);
    void FromInt(int32_t value);
    void FromDouble(double value);
    void FromDateTime(System::DateTime value);
    void FromBool(bool value);
    void FromByteArray(System::ArrayPtr<uint8_t> data);
    System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Clone();
    static Aspose::Words::Properties::PropertyType GetPropertyTypeFromValue(System::SharedPtr<System::Object> value);
    static System::SharedPtr<System::Object> GetDefaultValueForPropertyType(Aspose::Words::Properties::PropertyType propType);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mName;
    System::SharedPtr<System::Object> mValue;
    System::String mLinkTarget;
    System::SharedPtr<System::Object> mDefaultValueInternal;

};

}
}
}
