//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Properties/CustomDocumentProperties.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/date_time.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Properties/DocumentPropertyCollection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Properties { class DocumentProperty; } } }

namespace Aspose {

namespace Words {

namespace Properties {

/// A collection of custom document properties.
/// 
/// Each <see cref="Aspose::Words::Properties::DocumentProperty">DocumentProperty</see> object represents a custom property of a container document.
/// 
/// The names of the properties are case-insensitive.
/// 
/// The properties in the collection are sorted alphabetically by name.
/// 
/// @sa Aspose::Words::Document
/// @sa Aspose::Words::Document::get_BuiltInDocumentProperties
/// @sa Aspose::Words::Document::get_CustomDocumentProperties
class ASPOSE_WORDS_SHARED_CLASS CustomDocumentProperties : public Aspose::Words::Properties::DocumentPropertyCollection
{
    typedef CustomDocumentProperties ThisType;
    typedef Aspose::Words::Properties::DocumentPropertyCollection BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Document;

protected:
    using Aspose::Words::Properties::DocumentPropertyCollection::Add;

public:
    using Aspose::Words::Properties::DocumentPropertyCollection::idx_get;

public:

    /// Creates a new custom document property of the <b>PropertyType.String</b> data type.
    /// 
    /// @param name The name of the property.
    /// @param value The value of the property.
    /// 
    /// @return The newly created property object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, System::String value);
    /// Creates a new custom document property of the <b>PropertyType.Number</b> data type.
    /// 
    /// @param name The name of the property.
    /// @param value The value of the property.
    /// 
    /// @return The newly created property object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, int32_t value);
    /// Creates a new custom document property of the <b>PropertyType.DateTime</b> data type.
    /// 
    /// @param name The name of the property.
    /// @param value The value of the property.
    /// 
    /// @return The newly created property object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, System::DateTime value);
    /// Creates a new custom document property of the <b>PropertyType.Boolean</b> data type.
    /// 
    /// @param name The name of the property.
    /// @param value The value of the property.
    /// 
    /// @return The newly created property object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, bool value);
    /// Creates a new custom document property of the <b>PropertyType.Float</b> data type.
    /// 
    /// @param name The name of the property.
    /// @param value The value of the property.
    /// 
    /// @return The newly created property object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> Add(System::String name, double value);
    /// Creates a new linked to content custom document property.
    /// 
    /// @param name The name of the property.
    /// @param linkSource The source of the property.
    /// 
    /// @return The newly created property object or null when the linkSource is invalid.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentProperty> AddLinkToContent(System::String name, System::String linkSource);

protected:

    CustomDocumentProperties(System::SharedPtr<Aspose::Words::Document> document);
    CustomDocumentProperties();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Properties::DocumentPropertyCollection> Create() override;
    void SetDocument(System::SharedPtr<Aspose::Words::Document> document);

    virtual ASPOSE_WORDS_SHARED_API ~CustomDocumentProperties();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Document> mDocument;

    static System::String GetLinkedContent(System::SharedPtr<Aspose::Words::Document> document, System::String linkTarget);

};

}
}
}
