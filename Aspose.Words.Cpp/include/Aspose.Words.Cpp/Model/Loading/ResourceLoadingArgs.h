//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Loading/ResourceLoadingArgs.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Loading/ResourceType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ImageDataUtil; } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlCommon { class HtmlResourceLoader; } } } }

namespace Aspose {

namespace Words {

namespace Loading {

/// Provides data for the <see cref="Aspose::Words::Loading::IResourceLoadingCallback::ResourceLoading(System::SharedPtr<Aspose::Words::Loading::ResourceLoadingArgs>)">ResourceLoading()</see> method.
class ASPOSE_WORDS_SHARED_CLASS ResourceLoadingArgs : public System::Object
{
    typedef ResourceLoadingArgs ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Core::ImageDataUtil;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::RW::HtmlCommon::HtmlResourceLoader;

public:

    /// Type of resource.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Loading::ResourceType get_ResourceType() const;
    /// URI of the resource which is used for downloading
    /// if <see cref="Aspose::Words::Loading::IResourceLoadingCallback::ResourceLoading(System::SharedPtr<Aspose::Words::Loading::ResourceLoadingArgs>)">ResourceLoading()</see>
    /// returns <see cref="Aspose::Words::Loading::ResourceLoadingAction::Default">Default</see>.
    /// Initially it's set to absolute URI of the resource,
    /// but user can redefine it to any value.
    ASPOSE_WORDS_SHARED_API System::String get_Uri() const;
    /// Setter for Aspose::Words::Loading::ResourceLoadingArgs::get_Uri
    ASPOSE_WORDS_SHARED_API void set_Uri(System::String value);
    /// Original URI of the resource as specified in imported document.
    ASPOSE_WORDS_SHARED_API System::String get_OriginalUri() const;

    /// Sets user provided data of the resource which is used
    /// if <see cref="Aspose::Words::Loading::IResourceLoadingCallback::ResourceLoading(System::SharedPtr<Aspose::Words::Loading::ResourceLoadingArgs>)">ResourceLoading()</see>
    /// returns <see cref="Aspose::Words::Loading::ResourceLoadingAction::UserProvided">UserProvided</see>.
    ASPOSE_WORDS_SHARED_API void SetData(System::ArrayPtr<uint8_t> data);

protected:

    bool get_IsDataEmtpy();

    ResourceLoadingArgs(System::String absoluteUri, System::String originalUri, Aspose::Words::Loading::ResourceType resourceType);

    System::ArrayPtr<uint8_t> GetData();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Loading::ResourceType mResourceType;
    System::ArrayPtr<uint8_t> mData;
    System::String mUri;
    System::String mOriginalUri;

};

}
}
}
