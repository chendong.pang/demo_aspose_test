//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontInfoSubstitutionRule.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <drawing/font_style.h>

#include "Aspose.Words.Cpp/Model/Fonts/FontSubstitutionRule.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class FontSubstitutionSettings; } } }
namespace Aspose { namespace Fonts { namespace TrueType { class TTFont; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfo; } } }
namespace Aspose { namespace Fonts { class ExternalFontCache; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Font info substitution rule.
class ASPOSE_WORDS_SHARED_CLASS FontInfoSubstitutionRule : public Aspose::Words::Fonts::FontSubstitutionRule
{
    typedef FontInfoSubstitutionRule ThisType;
    typedef Aspose::Words::Fonts::FontSubstitutionRule BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::FontSubstitutionSettings;

protected:

    FontInfoSubstitutionRule(System::SharedPtr<System::Object> syncRoot);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Fonts::TrueType::TTFont> PerformSubstitutionCore(System::String familyName, System::Drawing::FontStyle style, System::SharedPtr<Aspose::Words::Fonts::FontInfo> info, System::SharedPtr<Aspose::Fonts::ExternalFontCache> fontCache) override;

};

}
}
}
