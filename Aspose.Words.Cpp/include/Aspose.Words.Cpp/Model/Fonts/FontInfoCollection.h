//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontInfoCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/sorted_list.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fonts/FontInfo.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { namespace Word60 { class Word60ListHelper; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class RunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class StyleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class RunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class StyleWriter; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFontFaceDeclsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxFontTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFontTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPrFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class FontInfoFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFontCodeResolver; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfHeaderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfRunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriterContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlFontsReader; } } } } }
namespace Aspose { namespace Collections { class StringToIntDictionary; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class ISetGeneric; } } }
namespace Aspose { namespace Words { namespace Settings { class DocPr; } } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Represents a collection of fonts used in a document.
/// 
/// Items are <see cref="Aspose::Words::Fonts::FontInfo">FontInfo</see> objects.
/// 
/// You do not create instances of this class directly.
/// Use the <see cref="Aspose::Words::DocumentBase::get_FontInfos">FontInfos</see> property to access the collection of fonts
/// defined in the document.
/// 
/// @sa Aspose::Words::Fonts::FontInfo
/// @sa Aspose::Words::DocumentBase::get_FontInfos
class ASPOSE_WORDS_SHARED_CLASS FontInfoCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Fonts::FontInfo>>
{
    typedef FontInfoCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Fonts::FontInfo>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::RW::Doc::Reader::Word60::Word60ListHelper;
    friend class Aspose::Words::RW::Doc::Reader::RunPrReader;
    friend class Aspose::Words::RW::Doc::Reader::StyleReader;
    friend class Aspose::Words::RW::Doc::Writer::RunPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::StyleWriter;
    friend class Aspose::Words::DocumentBase;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::RW::Odt::Reader::OdtFontFaceDeclsReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxFontTableReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxFontTableWriter;
    friend class Aspose::Words::RW::Doc::DocPrFiler;
    friend class Aspose::Words::RW::Doc::FontInfoFiler;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFontCodeResolver;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfHeaderWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfRunPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriterContext;
    friend class Aspose::Words::RW::Wml::Reader::WmlFontsReader;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    /// Specifies whether or not to embed TrueType fonts in a document when it is saved.
    /// Default value for this property is <b>false</b>.
    /// 
    /// Embedding TrueType fonts allows others to view the document with the same fonts that were used to create it,
    /// but may substantially increase the document size.
    /// 
    /// This option works for DOC, DOCX and RTF formats only.
    ASPOSE_WORDS_SHARED_API bool get_EmbedTrueTypeFonts() const;
    /// Setter for Aspose::Words::Fonts::FontInfoCollection::get_EmbedTrueTypeFonts
    ASPOSE_WORDS_SHARED_API void set_EmbedTrueTypeFonts(bool value);
    /// Specifies whether or not to embed System fonts into the document.
    /// Default value for this property is <b>false</b>.
    /// This option works only when <see cref="Aspose::Words::Fonts::FontInfoCollection::get_EmbedTrueTypeFonts">EmbedTrueTypeFonts</see> option is set to <b>true</b>.
    /// 
    /// Setting this property to <c>True</c> is useful if the user is on an East Asian system
    /// and wants to create a document that is readable by others who do not have fonts for that
    /// language on their system. For example, a user on a Japanese system could choose to embed the
    /// fonts in a document so that the Japanese document would be readable on all systems.
    /// 
    /// This option works for DOC, DOCX and RTF formats only.
    ASPOSE_WORDS_SHARED_API bool get_EmbedSystemFonts() const;
    /// Setter for Aspose::Words::Fonts::FontInfoCollection::get_EmbedSystemFonts
    ASPOSE_WORDS_SHARED_API void set_EmbedSystemFonts(bool value);
    /// Specifies whether or not to save a subset of the embedded TrueType fonts with the document.
    /// Default value for this property is <b>false</b>.
    /// This option works only when <see cref="Aspose::Words::Fonts::FontInfoCollection::get_EmbedTrueTypeFonts">EmbedTrueTypeFonts</see> property is set to <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_SaveSubsetFonts() const;
    /// Specifies whether or not to save a subset of the embedded TrueType fonts with the document.
    /// Default value for this property is <b>false</b>.
    /// This option works only when <see cref="Aspose::Words::Fonts::FontInfoCollection::get_EmbedTrueTypeFonts">EmbedTrueTypeFonts</see> property is set to <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_SaveSubsetFonts(bool value);

    /// Gets a font with the specified name.
    /// 
    /// @param name Case-insensitive name of the font to locate.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::FontInfo> idx_get(System::String name);
    /// Gets a font at the specified index.
    /// 
    /// @param index Zero-based index of the font.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fonts::FontInfo> idx_get(int32_t index);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Fonts::FontInfo>>> GetEnumerator() override;
    /// Determines whether the collection contains a font with the given name.
    /// 
    /// @param name Case-insensitive name of the font to locate.
    /// 
    /// @return True if the item is found in the collection; otherwise, false.
    ASPOSE_WORDS_SHARED_API bool Contains(System::String name);

protected:

    FontInfoCollection();

    int32_t NameToCode(System::String name);
    System::String CodeToName(int32_t fontCode);
    int32_t Merge(System::SharedPtr<Aspose::Words::Fonts::FontInfo> fontInfo);
    void Merge(System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> srcFontInfoCollection);
    void UpdateToUsedFonts(System::SharedPtr<Aspose::Collections::Generic::ISetGeneric<System::String>> usedFontNames);
    System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> Clone();
    System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> CloneWithoutEmbeddedFonts();
    void RemoveEmbeddedFonts();
    bool HasEmbeddedFonts();
    void RemoveUnusedFonts(System::SharedPtr<System::Collections::Generic::SortedList<int32_t, int32_t>> validFontIndexes);
    void UpdateEmbedFontOptions(System::SharedPtr<Aspose::Words::Settings::DocPr> docPr);

    virtual ASPOSE_WORDS_SHARED_API ~FontInfoCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Fonts::FontInfo>>> mItems;
    System::SharedPtr<Aspose::Collections::StringToIntDictionary> mFontNameToItemIndex;
    System::SharedPtr<System::Collections::Generic::List<System::String>> mExternalCodeToFontName;
    System::SharedPtr<Aspose::Collections::StringToIntDictionary> mFontAltNameToItemIndex;
    bool mEmbedTrueTypeFonts;
    bool mEmbedSystemFonts;
    bool mSaveSubsetFonts;

    void UpdateEmbedFontOptions(System::SharedPtr<Aspose::Words::Fonts::FontInfoCollection> fontInfos);
    void Clear();
    void FindNewAndUsedExistingFonts(System::SharedPtr<Aspose::Collections::Generic::ISetGeneric<System::String>> usedFontNames, System::SharedPtr<System::Collections::Generic::List<System::String>> newFontNames, System::SharedPtr<System::Collections::Generic::SortedList<int32_t, int32_t>> validFontIndexes);
    void AddNewFonts(System::SharedPtr<System::Collections::Generic::List<System::String>> newFontNames);

};

}
}
}
