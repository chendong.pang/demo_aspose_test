//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/SystemFontSource.h
#pragma once

#include <system/string.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Fonts/FontSourceBase.h"

namespace Aspose { namespace Fonts { class IFontData; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Represents all TrueType fonts installed to the system.
class ASPOSE_WORDS_SHARED_CLASS SystemFontSource : public Aspose::Words::Fonts::FontSourceBase
{
    typedef SystemFontSource ThisType;
    typedef Aspose::Words::Fonts::FontSourceBase BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Returns the type of the font source.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontSourceType get_Type() override;

    /// Ctor.
    ASPOSE_WORDS_SHARED_API SystemFontSource();
    /// Ctor.
    /// 
    /// @param priority Font source priority. See the <see cref="Aspose::Words::Fonts::FontSourceBase::get_Priority">Priority</see> property description for more information.
    ASPOSE_WORDS_SHARED_API SystemFontSource(int32_t priority);

    /// Returns system font folders or empty array if folders are not accessible.
    static ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::String> GetSystemFontFolders();

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Fonts::IFontData>>> GetFontDataInternalInternal() override;

};

}
}
}
