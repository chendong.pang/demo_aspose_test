//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontSourceBase.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ilist.h>
#include <system/collections/ienumerable.h>
#include "Aspose.Words.Cpp/Fonts/IFontSource.h"
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fonts/FontSourceType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class StreamFontSource; } } }
namespace Aspose { namespace Words { namespace Fonts { class FileFontSource; } } }
namespace Aspose { namespace Words { namespace Fonts { class FolderFontSource; } } }
namespace Aspose { namespace Words { namespace Fonts { class MemoryFontSource; } } }
namespace Aspose { namespace Words { namespace Fonts { class SystemFontSource; } } }
namespace Aspose { namespace Words { namespace Fonts { class PhysicalFontInfo; } } }
namespace Aspose { namespace Fonts { class IFontData; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// This is an abstract base class for the classes that allow the user to specify various font sources.
class ASPOSE_WORDS_SHARED_CLASS FontSourceBase : public Aspose::Fonts::IFontSource
{
    typedef FontSourceBase ThisType;
    typedef Aspose::Fonts::IFontSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::StreamFontSource;
    friend class Aspose::Words::Fonts::FileFontSource;
    friend class Aspose::Words::Fonts::FolderFontSource;
    friend class Aspose::Words::Fonts::MemoryFontSource;
    friend class Aspose::Words::Fonts::SystemFontSource;

public:

    /// Returns the type of the font source.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontSourceType get_Type() = 0;
    /// Returns the font source priority.
    /// 
    /// This value is used when there are fonts with the same family name and style in different font sources.
    /// In this case Aspose.Words selects the font from the source with the higher priority value.
    /// 
    /// The default value is 0.
    ASPOSE_WORDS_SHARED_API int32_t get_Priority() const;
    ASPOSE_WORDS_SHARED_API int32_t get_PriorityInternal() override;

    /// Returns list of fonts available via this source.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fonts::PhysicalFontInfo>>> GetAvailableFonts();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Fonts::IFontData>>> GetFontDataInternal() override;

protected:

    ASPOSE_WORDS_SHARED_API FontSourceBase();
    ASPOSE_WORDS_SHARED_API FontSourceBase(int32_t priority);

    virtual System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Fonts::IFontData>>> GetFontDataInternalInternal() = 0;

    virtual ASPOSE_WORDS_SHARED_API ~FontSourceBase();

private:

    int32_t mPriority;

};

}
}
}
