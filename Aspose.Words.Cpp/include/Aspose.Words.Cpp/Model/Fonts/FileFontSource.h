//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FileFontSource.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fonts/FontSourceType.h"
#include "Aspose.Words.Cpp/Model/Fonts/FontSourceBase.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Fonts { class IFontData; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Represents the single TrueType font file stored in the file system.
class ASPOSE_WORDS_SHARED_CLASS FileFontSource : public Aspose::Words::Fonts::FontSourceBase
{
    typedef FileFontSource ThisType;
    typedef Aspose::Words::Fonts::FontSourceBase BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Path to the font file.
    ASPOSE_WORDS_SHARED_API System::String get_FilePath() const;
    /// Returns the type of the font source.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontSourceType get_Type() override;

    /// Ctor.
    /// 
    /// @param filePath Path to font file.
    ASPOSE_WORDS_SHARED_API FileFontSource(System::String filePath);
    /// Ctor.
    /// 
    /// @param filePath Path to font file.
    /// @param priority Font source priority. See the <see cref="Aspose::Words::Fonts::FontSourceBase::get_Priority">Priority</see> property description for more information.
    ASPOSE_WORDS_SHARED_API FileFontSource(System::String filePath, int32_t priority);

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Fonts::IFontData>>> GetFontDataInternalInternal() override;

private:

    System::String mFilePath;

};

}
}
}
