//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontFallbackSettings.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontSettings; } } }
namespace Aspose { namespace Fonts { namespace FontFallback { class FontFallbackSettingsCore; } } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Specifies font fallback mechanism settings.
class ASPOSE_WORDS_SHARED_CLASS FontFallbackSettings : public System::Object
{
    typedef FontFallbackSettings ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::DocumentFontProvider;
    friend class Aspose::Words::Fonts::FontSettings;

public:

    /// Loads font fallback settings from XML file.
    /// 
    /// @param fileName Input file name.
    ASPOSE_WORDS_SHARED_API void Load(System::String fileName);
    /// Loads fallback settings from XML stream.
    /// 
    /// @param stream Input stream.
    ASPOSE_WORDS_SHARED_API void Load(System::SharedPtr<System::IO::Stream> stream);
    /// Loads predefined fallback settings which mimics the Microsoft Word fallback and uses Microsoft office fonts.
    ASPOSE_WORDS_SHARED_API void LoadMsOfficeFallbackSettings();
    /// Loads predefined fallback settings which uses Google Noto fonts.
    ASPOSE_WORDS_SHARED_API void LoadNotoFallbackSettings();
    /// Saves the current fallback settings to stream.
    /// 
    /// @param outputStream Output stream.
    ASPOSE_WORDS_SHARED_API void Save(System::SharedPtr<System::IO::Stream> outputStream);
    /// Saves the current fallback settings to file.
    /// 
    /// @param fileName Output file name.
    ASPOSE_WORDS_SHARED_API void Save(System::String fileName);
    /// Automatically builds the fallback settings by scanning available fonts.
    ASPOSE_WORDS_SHARED_API void BuildAutomatic();

protected:

    System::SharedPtr<Aspose::Fonts::FontFallback::FontFallbackSettingsCore> get_CoreSettings();

    FontFallbackSettings(System::SharedPtr<System::Object> syncRoot, System::SharedPtr<Aspose::Words::Fonts::FontSettings> fontSettings);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Fonts::FontFallback::FontFallbackSettingsCore> mCoreSettings;
    System::SharedPtr<System::Object> mSyncRoot;
    System::WeakPtr<Aspose::Words::Fonts::FontSettings> mFontSettings;

};

}
}
}
