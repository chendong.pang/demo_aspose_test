//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/PhysicalFontInfo.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class FontSourceBase; } } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Specifies information about physical font available to Aspose.Words font engine.
class ASPOSE_WORDS_SHARED_CLASS PhysicalFontInfo : public System::Object
{
    typedef PhysicalFontInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::FontSourceBase;

public:

    /// Family name of the font.
    ASPOSE_WORDS_SHARED_API System::String get_FontFamilyName() const;
    /// Full name of the font.
    ASPOSE_WORDS_SHARED_API System::String get_FullFontName() const;
    /// Version string of the font.
    ASPOSE_WORDS_SHARED_API System::String get_Version() const;
    /// Path to the font file if any.
    ASPOSE_WORDS_SHARED_API System::String get_FilePath() const;

protected:

    PhysicalFontInfo(System::String fontFamilyName, System::String fullFontName, System::String version, System::String filePath);

private:

    System::String mFontFamilyName;
    System::String mFullFontName;
    System::String mVersion;
    System::String mFilePath;

};

}
}
}
