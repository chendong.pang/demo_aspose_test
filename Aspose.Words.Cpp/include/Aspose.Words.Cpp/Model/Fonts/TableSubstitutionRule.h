//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/TableSubstitutionRule.h
#pragma once

#include <system/io/stream.h>
#include <system/collections/ienumerable.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Fonts/FontSubstitutionRule.h"

namespace Aspose { namespace Words { namespace Fonts { class FontSubstitutionSettings; } } }
namespace Aspose { namespace Fonts { class TableSubstitutionRuleCore; } }
namespace Aspose { namespace Fonts { namespace TrueType { class TTFont; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfo; } } }
namespace Aspose { namespace Fonts { class ExternalFontCache; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Table font substitution rule.
class ASPOSE_WORDS_SHARED_CLASS TableSubstitutionRule : public Aspose::Words::Fonts::FontSubstitutionRule
{
    typedef TableSubstitutionRule ThisType;
    typedef Aspose::Words::Fonts::FontSubstitutionRule BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::FontSubstitutionSettings;

public:

    /// Loads table substitution settings from XML file.
    /// 
    /// @param fileName Input file name.
    ASPOSE_WORDS_SHARED_API void Load(System::String fileName);
    /// Loads table substitution settings from XML stream.
    /// 
    /// @param stream Input stream.
    ASPOSE_WORDS_SHARED_API void Load(System::SharedPtr<System::IO::Stream> stream);
    /// Loads predefined table substitution settings for Windows platform.
    ASPOSE_WORDS_SHARED_API void LoadWindowsSettings();
    /// Loads predefined table substitution settings for Linux platform.
    ASPOSE_WORDS_SHARED_API void LoadLinuxSettings();
    /// Loads predefined table substitution settings for Linux platform.
    ASPOSE_WORDS_SHARED_API void LoadAndroidSettings();
    /// Saves the current table substitution settings to file.
    /// 
    /// @param fileName Output file name.
    ASPOSE_WORDS_SHARED_API void Save(System::String fileName);
    /// Saves the current table substitution settings to stream.
    /// 
    /// @param outputStream Output stream.
    ASPOSE_WORDS_SHARED_API void Save(System::SharedPtr<System::IO::Stream> outputStream);
    /// Returns array containing substitute font names for the specified original font name.
    /// 
    /// @param originalFontName Original font name.
    /// 
    /// @return List of alternative font names.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerable<System::String>> GetSubstitutes(System::String originalFontName);
    ASPOSE_WORDS_SHARED_API void SetSubstitutes(System::String originalFontName, const System::ArrayPtr<System::String>& substituteFontNames);
    ASPOSE_WORDS_SHARED_API void AddSubstitutes(System::String originalFontName, const System::ArrayPtr<System::String>& substituteFontNames);

protected:

    TableSubstitutionRule(System::SharedPtr<System::Object> syncRoot);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Fonts::TrueType::TTFont> PerformSubstitutionCore(System::String familyName, System::Drawing::FontStyle style, System::SharedPtr<Aspose::Words::Fonts::FontInfo> info, System::SharedPtr<Aspose::Fonts::ExternalFontCache> fontCache) override;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Fonts::TableSubstitutionRuleCore> mRuleCore;

};

}
}
}
