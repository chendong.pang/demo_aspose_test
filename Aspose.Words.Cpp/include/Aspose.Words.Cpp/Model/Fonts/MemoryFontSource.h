//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/MemoryFontSource.h
#pragma once

#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Fonts/FontSourceBase.h"

namespace Aspose { namespace Fonts { class IFontData; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Represents the single TrueType font file stored in memory.
class ASPOSE_WORDS_SHARED_CLASS MemoryFontSource : public Aspose::Words::Fonts::FontSourceBase
{
    typedef MemoryFontSource ThisType;
    typedef Aspose::Words::Fonts::FontSourceBase BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Binary font data.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> get_FontData() const;
    /// Returns the type of the font source.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontSourceType get_Type() override;

    /// Ctor.
    /// 
    /// @param fontData Binary font data.
    ASPOSE_WORDS_SHARED_API MemoryFontSource(System::ArrayPtr<uint8_t> fontData);
    /// Ctor.
    /// 
    /// @param fontData Binary font data.
    /// @param priority Font source priority. See the <see cref="Aspose::Words::Fonts::FontSourceBase::get_Priority">Priority</see> property description for more information.
    ASPOSE_WORDS_SHARED_API MemoryFontSource(System::ArrayPtr<uint8_t> fontData, int32_t priority);

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Fonts::IFontData>>> GetFontDataInternalInternal() override;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::ArrayPtr<uint8_t> mFontData;

};

}
}
}
