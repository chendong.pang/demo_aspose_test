//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontSubstitutionRule.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <drawing/font_style.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class DefaultFontSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontNameSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontConfigSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfoSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class TableSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontSettings; } } }
namespace Aspose { namespace Fonts { namespace TrueType { class TTFont; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfo; } } }
namespace Aspose { namespace Fonts { class ExternalFontCache; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// This is an abstract base class for the font substitution rule.
class ASPOSE_WORDS_SHARED_CLASS FontSubstitutionRule : public System::Object
{
    typedef FontSubstitutionRule ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::DefaultFontSubstitutionRule;
    friend class Aspose::Words::Fonts::FontNameSubstitutionRule;
    friend class Aspose::Words::Fonts::FontConfigSubstitutionRule;
    friend class Aspose::Words::Fonts::FontInfoSubstitutionRule;
    friend class Aspose::Words::Fonts::TableSubstitutionRule;
    friend class Aspose::Words::Fonts::FontSettings;

public:

    /// Specifies whether the rule is enabled or not.
    virtual ASPOSE_WORDS_SHARED_API bool get_Enabled();
    /// Specifies whether the rule is enabled or not.
    virtual ASPOSE_WORDS_SHARED_API void set_Enabled(bool value);

protected:

    System::SharedPtr<System::Object> SyncRoot;

    FontSubstitutionRule(System::SharedPtr<System::Object> syncRoot);

    System::SharedPtr<Aspose::Fonts::TrueType::TTFont> PerformSubstitution(System::String familyName, System::Drawing::FontStyle style, System::SharedPtr<Aspose::Words::Fonts::FontInfo> info, System::SharedPtr<Aspose::Fonts::ExternalFontCache> fontCache);
    virtual System::SharedPtr<Aspose::Fonts::TrueType::TTFont> PerformSubstitutionCore(System::String familyName, System::Drawing::FontStyle style, System::SharedPtr<Aspose::Words::Fonts::FontInfo> info, System::SharedPtr<Aspose::Fonts::ExternalFontCache> fontCache) = 0;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mEnabled;

};

}
}
}
