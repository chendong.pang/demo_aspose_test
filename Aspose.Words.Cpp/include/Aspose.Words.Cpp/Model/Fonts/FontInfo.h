//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fonts/FontInfo.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/collections/ilist.h>
#include <system/array.h>
#include <drawing/font_style.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fonts/FontPitch.h"
#include "Aspose.Words.Cpp/Model/Fonts/FontFamily.h"
#include "Aspose.Words.Cpp/Model/Fonts/EmbeddedFontStyle.h"
#include "Aspose.Words.Cpp/Model/Fonts/EmbeddedFontFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fonts { class FontConfigSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfoSubstitutionRule; } } }
namespace Aspose { namespace Words { namespace Validation { class ComplexScriptRunUpdater; } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace Fonts { class DocumentFontProvider; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeFonts; } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfoCollection; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFontFaceDeclsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxFontTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFontTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class FontInfoFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFontCodeResolver; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFontTableHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfFontTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfRunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriterContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlFontsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFontsWriter; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollection; } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFont; } } }
namespace Aspose { namespace Fonts { namespace TrueType { class TTFont; } } }
namespace Aspose { namespace Fonts { namespace TrueType { class FontSubsetBase; } } }
namespace Aspose { namespace Fonts { namespace TrueType { class FontUnicodeRanges; } } }
namespace Aspose { namespace Fonts { class FontSubstitutionInfo; } }
namespace Aspose { namespace Fonts { enum class FontFamilyCore; } }
namespace Aspose { namespace Fonts { enum class FontPitchCore; } }

namespace Aspose {

namespace Words {

namespace Fonts {

/// Specifies information about a font used in the document.
/// 
/// You do not create instances of this class directly.
/// Use the <see cref="Aspose::Words::DocumentBase::get_FontInfos">FontInfos</see> property to access the collection of fonts
/// defined in a document.
/// 
/// @sa Aspose::Words::Fonts::FontInfoCollection
/// @sa Aspose::Words::DocumentBase::get_FontInfos
class ASPOSE_WORDS_SHARED_CLASS FontInfo : public System::Object
{
    typedef FontInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fonts::FontConfigSubstitutionRule;
    friend class Aspose::Words::Fonts::FontInfoSubstitutionRule;
    friend class Aspose::Words::Validation::ComplexScriptRunUpdater;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::Fonts::DocumentFontProvider;
    friend class Aspose::Words::Themes::ThemeFonts;
    friend class Aspose::Words::Fonts::FontInfoCollection;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFontFaceDeclsReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxFontTableReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxFontTableWriter;
    friend class Aspose::Words::RW::Doc::FontInfoFiler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFontCodeResolver;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFontTableHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfFontTableWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfRunPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriterContext;
    friend class Aspose::Words::RW::Wml::Reader::WmlFontsReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlFontsWriter;

public:

    /// The pitch indicates if the font is fixed pitch, proportionally spaced, or relies on a default setting.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontPitch get_Pitch() const;
    /// The pitch indicates if the font is fixed pitch, proportionally spaced, or relies on a default setting.
    ASPOSE_WORDS_SHARED_API void set_Pitch(Aspose::Words::Fonts::FontPitch value);
    /// Indicates that this font is a TrueType or OpenType font as opposed to a raster or vector font.
    /// Default is true.
    ASPOSE_WORDS_SHARED_API bool get_IsTrueType() const;
    /// Indicates that this font is a TrueType or OpenType font as opposed to a raster or vector font.
    /// Default is true.
    ASPOSE_WORDS_SHARED_API void set_IsTrueType(bool value);
    /// Gets the font family this font belongs to.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fonts::FontFamily get_Family() const;
    /// Sets the font family this font belongs to.
    ASPOSE_WORDS_SHARED_API void set_Family(Aspose::Words::Fonts::FontFamily value);
    /// Gets the character set for the font.
    ASPOSE_WORDS_SHARED_API int32_t get_Charset();
    /// Sets the character set for the font.
    ASPOSE_WORDS_SHARED_API void set_Charset(int32_t value);
    /// Gets or sets the PANOSE typeface classification number.
    /// 
    /// PANOSE is a compact 10-byte description of a fonts critical visual characteristics,
    /// such as contrast, weight, and serif style. The digits represent Family Kind, Serif Style,
    /// Weight, Proportion, Contrast, Stroke Variation, Arm Style, Letterform, Midline, and X-Height.
    /// 
    /// Can be <c>null</c>.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> get_Panose() const;
    /// Setter for Aspose::Words::Fonts::FontInfo::get_Panose
    ASPOSE_WORDS_SHARED_API void set_Panose(System::ArrayPtr<uint8_t> value);
    /// Gets the name of the font.
    /// 
    /// Cannot be <c>null</c>. Can be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Gets the alternate name for the font.
    /// 
    /// Cannot be <c>null</c>. Can be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_AltName() const;
    /// Sets the alternate name for the font.
    /// 
    /// Cannot be <c>null</c>. Can be an empty string.
    ASPOSE_WORDS_SHARED_API void set_AltName(System::String value);

    /// Gets a specific embedded font file.
    /// 
    /// @param format Specifies the font format to retrieve.
    /// @param style Specifies the font style to retrieve.
    /// 
    /// @return Returns <c>null</c> if the specified font is not embedded.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> GetEmbeddedFont(Aspose::Words::Fonts::EmbeddedFontFormat format, Aspose::Words::Fonts::EmbeddedFontStyle style);
    /// Gets an embedded font file in OpenType format. Fonts in Embedded OpenType format are converted to OpenType.
    /// 
    /// @param style Specifies the font style to retrieve.
    /// 
    /// @return Returns <c>null</c> if the specified font is not embedded.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> GetEmbeddedFontAsOpenType(Aspose::Words::Fonts::EmbeddedFontStyle style);

protected:

    int32_t get_Weight() const;
    void set_Weight(int32_t value);
    System::String get_IanaCharset();
    void set_IanaCharset(System::String value);
    System::ArrayPtr<uint8_t> get_Sig() const;
    void set_Sig(System::ArrayPtr<uint8_t> value);
    bool get_IsCharsetDefined();

    static const int32_t PanoseLength;
    static const int32_t SigLength;

    FontInfo();
    FontInfo(System::String name);

    void SetName(System::String name);
    System::SharedPtr<Aspose::Words::Fonts::FontInfo> Clone();
    void Merge(System::SharedPtr<Aspose::Words::Fonts::FontInfo> srcFontInfo);
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Fonts::EmbeddedFont>> GetEmbeddedFonts(Aspose::Words::Fonts::EmbeddedFontFormat format);
    System::SharedPtr<Aspose::Words::Fonts::EmbeddedFont> GetEmbeddedFontWithData(System::Drawing::FontStyle style);
    static Aspose::Words::Fonts::EmbeddedFontStyle FontStyleToEmbeddedFontStyle(System::Drawing::FontStyle style);
    System::SharedPtr<Aspose::Fonts::TrueType::TTFont> GetEmbeddedFontParsed(System::Drawing::FontStyle style);
    System::SharedPtr<Aspose::Fonts::TrueType::TTFont> GetEmbeddedFontParsedAnyStyle();
    void AddEmbeddedFont(System::ArrayPtr<uint8_t> fontData, Aspose::Words::Fonts::EmbeddedFontFormat fontFormat, Aspose::Words::Fonts::EmbeddedFontStyle fontStyle, bool isSubsetted);
    void AddEmbeddedFont(System::SharedPtr<Aspose::Fonts::TrueType::TTFont> ttFont);
    void AddEmbeddedFont(System::SharedPtr<Aspose::Fonts::TrueType::FontSubsetBase> fontSubset);
    void AddEmbeddedFont(System::SharedPtr<Aspose::Words::Fonts::EmbeddedFont> embeddedFont);
    System::SharedPtr<System::Collections::Generic::IList<System::String>> GetAltNameList();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Fonts::EmbeddedFont>> GetEmbeddedFonts();
    bool HasEmbeddedFonts();
    bool HasEmbeddedFontsByFormat(Aspose::Words::Fonts::EmbeddedFontFormat fontFormat);
    bool HasEmbeddedFontsByStyle(System::Drawing::FontStyle fontStyle);
    void RemoveEmbeddedFonts();
    int32_t GetEmbeddedFontsCount(Aspose::Words::Fonts::EmbeddedFontFormat fontFormat);
    System::SharedPtr<Aspose::Fonts::TrueType::FontUnicodeRanges> GetUnicodeRanges();
    System::SharedPtr<Aspose::Fonts::FontSubstitutionInfo> GetSubstitutionInfo();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Fonts::FontPitch mPitch;
    bool mIsTrueType;
    Aspose::Words::Fonts::FontFamily mFamily;
    int32_t mWeight;
    int32_t mCharset;
    System::String mIanaCharset;
    System::ArrayPtr<uint8_t> mPanose;
    System::ArrayPtr<uint8_t> mSig;
    System::String mName;
    System::String mAltName;
    System::SharedPtr<Aspose::Words::Fonts::EmbeddedFontCollection> mEmbeddedFonts;

    void MergeAttributes(System::SharedPtr<Aspose::Words::Fonts::FontInfo> srcFontInfo);
    static Aspose::Fonts::FontFamilyCore FontFamilyToCore(Aspose::Words::Fonts::FontFamily value);
    static Aspose::Fonts::FontPitchCore FontPitchToCore(Aspose::Words::Fonts::FontPitch value);

};

}
}
}
