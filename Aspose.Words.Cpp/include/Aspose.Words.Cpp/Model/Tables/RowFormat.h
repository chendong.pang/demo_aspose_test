//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Tables/RowFormat.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/sorted_list.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Text/HeightRule.h"
#include "Aspose.Words.Cpp/Model/Formatting/IBorderAttrSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { class BorderCollection; } }
namespace Aspose { namespace Words { enum class BorderType; } }
namespace Aspose { namespace Words { class IRowAttrSource; } }

namespace Aspose {

namespace Words {

namespace Tables {

/// Represents all formatting for a table row.
class ASPOSE_WORDS_SHARED_CLASS RowFormat : public Aspose::Words::IBorderAttrSource
{
    typedef RowFormat ThisType;
    typedef Aspose::Words::IBorderAttrSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Tables::Row;

public:

    /// Gets the collection of default cell borders for the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BorderCollection> get_Borders();
    /// Gets the height of the table row in points.
    ASPOSE_WORDS_SHARED_API double get_Height();
    /// Sets the height of the table row in points.
    ASPOSE_WORDS_SHARED_API void set_Height(double value);
    /// Gets the rule for determining the height of the table row.
    ASPOSE_WORDS_SHARED_API Aspose::Words::HeightRule get_HeightRule();
    /// Sets the rule for determining the height of the table row.
    ASPOSE_WORDS_SHARED_API void set_HeightRule(Aspose::Words::HeightRule value);
    /// True if the text in a table row is allowed to split across a page break.
    ASPOSE_WORDS_SHARED_API bool get_AllowBreakAcrossPages();
    /// True if the text in a table row is allowed to split across a page break.
    ASPOSE_WORDS_SHARED_API void set_AllowBreakAcrossPages(bool value);
    /// True if the row is repeated as a table heading on every page when the table spans more than one page.
    ASPOSE_WORDS_SHARED_API bool get_HeadingFormat();
    /// True if the row is repeated as a table heading on every page when the table spans more than one page.
    ASPOSE_WORDS_SHARED_API void set_HeadingFormat(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::SortedList<Aspose::Words::BorderType, int32_t>> get_PossibleBorderKeys() override;

    /// Resets to default row formatting.
    ASPOSE_WORDS_SHARED_API void ClearFormatting();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedBorderAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetBorderAttr(int32_t key, System::SharedPtr<System::Object> value) override;

protected:

    RowFormat(System::SharedPtr<Aspose::Words::IRowAttrSource> parent);

    virtual ASPOSE_WORDS_SHARED_API ~RowFormat();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::IRowAttrSource> mParent;
    System::SharedPtr<Aspose::Words::BorderCollection> mBorders;

    System::SharedPtr<System::Object> FetchAttr(int32_t key);
    System::SharedPtr<System::Object> FetchOrCreateComplexRowAttr(int32_t key);

};

}
}
}
