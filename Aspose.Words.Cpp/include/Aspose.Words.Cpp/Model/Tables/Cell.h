//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Tables/Cell.h
#pragma once

#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Formatting/ICellAttrSource.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorRemover; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class RowComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class TableComparer; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { class DocumentInserter; } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace Tables { class DocRtfGridHandler; } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxCellReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeSizeValidationHelper; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class FieldContext; } } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Tables { class CellSpan; } } }
namespace Aspose { namespace Words { namespace Tables { class RowSpan; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class OneDimensionCellRangeEnumerator; } } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class UpDownCellRangeEnumerator; } } } }
namespace Aspose { namespace Words { namespace Markup { class StructuredDocumentTag; } } }
namespace Aspose { namespace Words { class NodeUtil; } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class CellPosition; } } } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { class TableBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBackgroundColorPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageTableWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageDocumentTreeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { class MarkupResolver; } } }
namespace Aspose { namespace Words { namespace Lists { class ListLabelUtil; } } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { namespace Tables { class CellCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableGrid; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableCellReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Celler { class CellerTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTextPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtCellPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtParagraphPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtTableBuilder; } } } } }
namespace Aspose { namespace Words { namespace Tables { class TableGridFromCellWidth; } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfTableDefWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRunPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class CellFormat; } } }
namespace Aspose { namespace Words { class ParagraphCollection; } }
namespace Aspose { namespace Words { namespace Tables { class TableCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }

namespace Aspose {

namespace Words {

namespace Tables {

/// Represents a table cell.
/// 
/// <b>Cell</b> can only be a child of a <b>Row</b>.
/// 
/// <b>Cell</b> can contain block-level nodes <b>Paragraph</b> and <b>Table</b>.
/// 
/// A minimal valid cell needs to have at least one <b>Paragraph</b>.
class ASPOSE_WORDS_SHARED_CLASS Cell : public Aspose::Words::CompositeNode, public Aspose::Words::ICellAttrSource, public Aspose::Words::Revisions::ITrackableNode
{
    typedef Cell ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::ICellAttrSource BaseType1;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Themes::ThemeColorRemover;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Comparison::RowComparer;
    friend class Aspose::Words::Comparison::TableComparer;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::DocumentInserter;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::Tables::DocRtfGridHandler;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownTableWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxCellReaderBase;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RevisionGroupCollection;
    friend class Aspose::Words::Drawing::ShapeSizeValidationHelper;
    friend class Aspose::Words::Fields::Expressions::FieldContext;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Tables::CellSpan;
    friend class Aspose::Words::Tables::RowSpan;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::Fields::Expressions::OneDimensionCellRangeEnumerator;
    friend class Aspose::Words::Fields::Expressions::UpDownCellRangeEnumerator;
    friend class Aspose::Words::Markup::StructuredDocumentTag;
    friend class Aspose::Words::NodeUtil;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Fields::Expressions::CellPosition;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::TableBuilder;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::RW::Html::Css::New::CssBackgroundColorPropertyDef;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageTableWriterBase;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageDocumentTreeWriter;
    friend class Aspose::Words::RW::MarkupResolver;
    friend class Aspose::Words::Lists::ListLabelUtil;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::Tables::CellCollection;
    friend class Aspose::Words::Tables::Row;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Nrx::NrxTableGrid;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableCellReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Celler::CellerTable;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Html::CssTable;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtTextPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtCellPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtParagraphPropertiesWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtTableBuilder;
    friend class Aspose::Words::Tables::TableGridFromCellWidth;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfTableDefWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlRunPrReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Returns <b>NodeType.Cell</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns the parent row of the cell.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Row> get_ParentRow();
    /// Gets the first paragraph among the immediate children.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_FirstParagraph();
    /// Gets the last paragraph among the immediate children.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_LastParagraph();
    /// True if this is the first cell inside a row; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsFirstCell();
    /// True if this is the last cell inside a row; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsLastCell();
    /// Provides access to the formatting properties of the cell.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::CellFormat> get_CellFormat();
    /// Gets a collection of paragraphs that are immediate children of the cell.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphCollection> get_Paragraphs();
    /// Gets a collection of tables that are immediate children of the cell.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::TableCollection> get_Tables();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    /// Initializes a new instance of the <b>Cell</b> class.
    /// 
    /// When <b>Cell</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Cell</b> to the document use InsertAfter or InsertBefore
    /// on the row where you want the cell inserted.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Cell(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// If the last child is not a paragraph, creates and appends one empty paragraph.
    ASPOSE_WORDS_SHARED_API void EnsureMinimum();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedCellAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetCellAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearCellAttrs() override;

protected:

    System::SharedPtr<Aspose::Words::Tables::Table> get_ParentTable();
    System::SharedPtr<Aspose::Words::Tables::Cell> get_NextCell();
    System::SharedPtr<Aspose::Words::Tables::Cell> get_PreviousCell();
    int32_t get_ColumnIndex();
    int32_t get_RowIndex();
    System::SharedPtr<Aspose::Words::Tables::CellPr> get_CellPr() const;
    void set_CellPr(System::SharedPtr<Aspose::Words::Tables::CellPr> value);

    Cell(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Tables::CellPr> cellPr);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    System::SharedPtr<System::Object> GetInheritedCellAttr(int32_t key);
    System::SharedPtr<Aspose::Words::Tables::CellPr> GetExpandedCellPr();

    virtual ASPOSE_WORDS_SHARED_API ~Cell();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Tables::CellPr> mCellPr;
    System::SharedPtr<Aspose::Words::Tables::CellFormat> mCellFormat;
    System::SharedPtr<Aspose::Words::ParagraphCollection> mParagraphs;
    System::SharedPtr<Aspose::Words::Tables::TableCollection> mTables;

    void ExpandAttr(int32_t key, System::SharedPtr<Aspose::Words::Tables::CellPr> dstCellPr);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
