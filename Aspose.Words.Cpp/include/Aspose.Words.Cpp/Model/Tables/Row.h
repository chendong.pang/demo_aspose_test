//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Tables/Row.h
#pragma once

#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Formatting/IRowAttrSource.h"

namespace Aspose { namespace Words { namespace Themes { class ThemeColorRemover; } } }
namespace Aspose { namespace Words { namespace Layout { class ParagraphPrConverter; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class TableComparer; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColorUpdater; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { namespace Formatting { namespace Intern { class InternManager; } } } }
namespace Aspose { namespace Words { namespace Tables { class DocRtfGridHandler; } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlBlockWriter; } } } } }
namespace Aspose { namespace Words { class RevisionGroupCollection; } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridConstructor; } } }
namespace Aspose { namespace Words { namespace Tables { class RowSpan; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { class TableStyle; } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { class TableBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Tables { class DocTableContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBackgroundColorPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlI18nWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTableWriter; } } } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableGrid; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Celler { class CellerTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxRowReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtCellPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class TableGridFromCellWidth; } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfTableDefWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRowReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class CellCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class RowFormat; } } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }

namespace Aspose {

namespace Words {

namespace Tables {

/// Represents a table row.
/// 
/// <b>Row</b> can only be a child of a <b>Table</b>.
/// 
/// <b>Row</b> can contain one or more <b>Cell</b> nodes.
/// 
/// A minimal valid row needs to have at least one <b>Cell</b>.
class ASPOSE_WORDS_SHARED_CLASS Row : public Aspose::Words::CompositeNode, public Aspose::Words::IRowAttrSource, public Aspose::Words::Revisions::ITrackableNode
{
    typedef Row ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::IRowAttrSource BaseType1;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Themes::ThemeColorRemover;
    friend class Aspose::Words::Layout::ParagraphPrConverter;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Comparison::FormattingComparer;
    friend class Aspose::Words::Comparison::TableComparer;
    friend class Aspose::Words::Themes::ThemeColorUpdater;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::Formatting::Intern::InternManager;
    friend class Aspose::Words::Tables::DocRtfGridHandler;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::Html::Writer::HtmlBlockWriter;
    friend class Aspose::Words::RevisionGroupCollection;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Tables::FixedGridConstructor;
    friend class Aspose::Words::Tables::RowSpan;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::TableStyle;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::TableBuilder;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::RW::Doc::Tables::DocTableContext;
    friend class Aspose::Words::RW::Html::Css::New::CssBackgroundColorPropertyDef;
    friend class Aspose::Words::RW::Html::Writer::HtmlI18nWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlTableWriter;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::Tables::Cell;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Nrx::NrxTableGrid;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableReader;
    friend class Aspose::Words::RW::Celler::CellerTable;
    friend class Aspose::Words::RW::Docx::Reader::DocxRowReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Html::CssTable;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtCellPropertiesWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::Tables::TableGridFromCellWidth;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfTableDefWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlRowReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Returns <b>NodeType.Row</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns the immediate parent table of the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Table> get_ParentTable();
    /// True if this is the first row in a table; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsFirstRow();
    /// True if this is the last row in a table; false otherwise.
    ASPOSE_WORDS_SHARED_API bool get_IsLastRow();
    /// Returns the first <b>Cell</b> in the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Cell> get_FirstCell();
    /// Returns the last <b>Cell</b> in the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Cell> get_LastCell();
    /// Provides typed access to the <b>Cell</b> child nodes of the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::CellCollection> get_Cells();
    /// Provides access to the formatting properties of the row.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::RowFormat> get_RowFormat();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;

    /// Initializes a new instance of the <b>Row</b> class.
    /// 
    /// When <b>Row</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Row</b> to the document use InsertAfter or InsertBefore
    /// on the table where you want the row inserted.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Row(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// Gets the text of all cells in this row including the end of row character.
    /// 
    /// Returns concatenated text of all child nodes with the end of row character
    /// <see cref="Aspose::Words::ControlChar::Cell">ControlChar.Cell</see> appended at the end.
    /// 
    /// The returned string includes all control and special characters as described in <see cref="Aspose::Words::ControlChar">ControlChar</see>.
    ASPOSE_WORDS_SHARED_API System::String GetText() override;
    /// If the <b>Row</b> has no cells, creates and appends one <b>Cell</b>.
    ASPOSE_WORDS_SHARED_API void EnsureMinimum();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRowAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void SetRowAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void ClearRowAttrs() override;
    ASPOSE_WORDS_SHARED_API void ResetToDefaultAttrs() override;

protected:

    bool get_IsHeadingRow();
    System::SharedPtr<Aspose::Words::Tables::Row> get_NextRow();
    System::SharedPtr<Aspose::Words::Tables::Row> get_PreviousRow();
    int32_t get_RowIndex();
    System::SharedPtr<Aspose::Words::Tables::TablePr> get_TablePr() const;
    void set_TablePr(System::SharedPtr<Aspose::Words::Tables::TablePr> value);
    int32_t get_ParaId() const;
    void set_ParaId(int32_t value);
    int32_t get_TextId() const;
    void set_TextId(int32_t value);

    Row(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::Tables::TablePr> rowPr);

    void EnsureTableStyleIsValid();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    bool IsSameRowFloatingPositioning(System::SharedPtr<Aspose::Words::Tables::Row> rhs);
    void BreakTableBefore();
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API System::String GetEndText() override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    void CopyCellPrTo(System::SharedPtr<Aspose::Words::Tables::CellCollection> cells);
    void RemoveParagraphFloatingAttributes();
    bool HasAttribute(int32_t key);

    virtual ASPOSE_WORDS_SHARED_API ~Row();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    int32_t mParaId;
    int32_t mTextId;
    System::SharedPtr<Aspose::Words::Tables::TablePr> mTablePr;
    System::SharedPtr<Aspose::Words::Tables::RowFormat> mRowFormat;
    System::SharedPtr<Aspose::Words::Tables::CellCollection> mCells;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
