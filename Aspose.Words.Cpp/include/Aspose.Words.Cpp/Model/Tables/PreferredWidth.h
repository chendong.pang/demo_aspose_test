//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Tables/PreferredWidth.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Tables/PreferredWidthType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class AutoFitGridConstructor; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class AutoFitGridResizer; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class AutoFitGridCalculator; } } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxCellPrReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeSizeValidationHelper; } } }
namespace Aspose { namespace Words { namespace Tables { class CellSpan; } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridConstructor; } } }
namespace Aspose { namespace Words { namespace Tables { class RowSpan; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class TableWidths; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class GridTable; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class TableCalculator; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class TableLayout; } } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace Tables { class TableGridColumn; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class CellPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class TablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class CellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class TablePrWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssWidthStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxCellPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxRowPrAttrContainer; } } } } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { namespace Tables { class CellFormat; } } }
namespace Aspose { namespace Words { namespace Tables { class CellPr; } } }
namespace Aspose { namespace Words { namespace Tables { class Table; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxXmlBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTablePropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTable; } } } } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxRowPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxTablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfCellPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfTablePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlRowPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlTablePrReader; } } } } }

namespace Aspose {

namespace Words {

namespace Tables {

/// Represents a value and its unit of measure that is used to specify the preferred width of a table or a cell.
/// 
/// Preferred width can be specified as a percentage, number of points or a special "none/auto" value.
/// 
/// The instances of this class are immutable.
/// 
/// @sa Aspose::Words::Tables::Table::get_PreferredWidth
/// @sa Aspose::Words::Tables::CellFormat::get_PreferredWidth
class ASPOSE_WORDS_SHARED_CLASS PreferredWidth FINAL : public System::Object
{
    typedef PreferredWidth ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Layout::Core::AutoFitGridConstructor;
    friend class Aspose::Words::Layout::Core::AutoFitGridResizer;
    friend class Aspose::Words::Layout::Core::AutoFitGridCalculator;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::RW::Nrx::Reader::NrxCellPrReaderBase;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::Drawing::ShapeSizeValidationHelper;
    friend class Aspose::Words::Tables::CellSpan;
    friend class Aspose::Words::Tables::FixedGridConstructor;
    friend class Aspose::Words::Tables::RowSpan;
    friend class Aspose::Words::Layout::Core::TableWidths;
    friend class Aspose::Words::Layout::Core::GridTable;
    friend class Aspose::Words::Layout::Core::TableCalculator;
    friend class Aspose::Words::Layout::Core::TableLayout;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::Tables::TableGridColumn;
    friend class Aspose::Words::RW::Doc::Reader::CellPrReader;
    friend class Aspose::Words::RW::Doc::Reader::TablePrReader;
    friend class Aspose::Words::RW::Doc::Writer::CellPrWriter;
    friend class Aspose::Words::RW::Doc::Writer::TablePrWriter;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::RW::Html::Css::New::CssWidthStyleConverter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxCellPrWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxRowPrAttrContainer;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::Tables::CellFormat;
    friend class Aspose::Words::Tables::CellPr;
    friend class Aspose::Words::Tables::Table;
    friend class Aspose::Words::RW::Nrx::Writer::NrxXmlBuilder;
    friend class Aspose::Words::RW::Odt::Reader::OdtTablePropertiesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTableReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTable;
    friend class Aspose::Words::Model::Nrx::NrxXmlReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxRowPrReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxTablePrReader;
    friend class Aspose::Words::RW::Html::CssTable;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfCellPrReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfTablePrReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfBuilder;
    friend class Aspose::Words::RW::Wml::Reader::WmlRowPrReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlTablePrReader;

public:

    /// Returns an instance that represents the "preferred width is not specified" value.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::PreferredWidth>& Auto();

    /// Gets the unit of measure used for this preferred width value.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Tables::PreferredWidthType get_Type() const;
    /// Gets the preferred width value. The unit of measure is specified in the <see cref="Aspose::Words::Tables::PreferredWidth::get_Type">Type</see> property.
    ASPOSE_WORDS_SHARED_API double get_Value() const;

    /// A creation method that returns a new instance that represents a preferred width specified as a percentage.
    /// 
    /// @param percent The value must be from 0 to 100.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromPercent(double percent);
    /// A creation method that returns a new instance that represents a preferred width specified using a number of points.
    /// 
    /// @param points The value must be from 0 to 22 inches (22 * 72 points).
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromPoints(double points);
    /// Determines whether the specified PreferredWidth is equal in value to the current PreferredWidth.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<Aspose::Words::Tables::PreferredWidth> other);
    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Serves as a hash function for this type.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    /// Returns a user-friendly string that displays the value of this object.
    ASPOSE_WORDS_SHARED_API System::String ToString() const override;

protected:

    Aspose::Words::Tables::PreferredWidthType get_TypeOrig() const;
    int32_t get_ValueRaw() const;
    int32_t get_ValueTwips() const;
    bool get_IsPositive();
    bool get_IsAuto();
    bool get_IsFixed() const;
    bool get_IsPercent();

    static const int32_t PercentFactor;

    static System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromRawSafe(Aspose::Words::Tables::PreferredWidthType type, int32_t rawValue);
    static System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromTwipsSafe(int32_t twips);
    static System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromPercentSafe(double percent);
    static System::SharedPtr<Aspose::Words::Tables::PreferredWidth> FromPointsSafe(double points);
    static double LimitPercentsToSafeRange(double percent);
    static double LimitPointsToSafeRange(double points);

    virtual ASPOSE_WORDS_SHARED_API ~PreferredWidth();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Tables::PreferredWidthType mType;
    Aspose::Words::Tables::PreferredWidthType mTypeOrig;
    int32_t mValueRaw;
    static const int32_t MaxPercent;
    static const int32_t MaxPoints;

    static System::SharedPtr<Aspose::Words::Tables::PreferredWidth>& ZeroTwips();

    PreferredWidth(Aspose::Words::Tables::PreferredWidthType type, int32_t rawValue);

};

}
}
}
