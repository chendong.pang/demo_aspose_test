//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Tables/Table.h
#pragma once

#include <drawing/color.h>

#include "Aspose.Words.Cpp/Model/Tables/TextWrapping.h"
#include "Aspose.Words.Cpp/Model/Tables/TableStyleOptions.h"
#include "Aspose.Words.Cpp/Model/Tables/TableAlignment.h"
#include "Aspose.Words.Cpp/Model/Tables/AutoFitBehavior.h"
#include "Aspose.Words.Cpp/Model/Styles/StyleIdentifier.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Drawing/VerticalAlignment.h"
#include "Aspose.Words.Cpp/Model/Drawing/RelativeVerticalPosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/RelativeHorizontalPosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/HorizontalAlignment.h"
#include "Aspose.Words.Cpp/Model/Borders/TextureIndex.h"
#include "Aspose.Words.Cpp/Model/Borders/LineStyle.h"
#include "Aspose.Words.Cpp/Model/Borders/BorderType.h"

namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { namespace Tables { class DocRtfGridHandler; } } }
namespace Aspose { namespace Words { namespace Tables { class TableMerger; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class TableLayout; } } } }
namespace Aspose { namespace Words { namespace Tables { class FixedGridCalculator; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanGenerator; } } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class CellPosition; } } } }
namespace Aspose { namespace Words { namespace Fields { class NewResultEnumerator; } } }
namespace Aspose { namespace Words { namespace TableLayout { class Extensions; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class TableValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Tables { class DocTableContext; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlTableWriter; } } } } }
namespace Aspose { namespace Words { namespace TableLayout { class TableLayouter; } } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { class NrxTableGrid; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Celler { class CellerTable; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTableWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfTableDefWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlTableReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { namespace Tables { class RowCollection; } } }
namespace Aspose { namespace Words { namespace Tables { class PreferredWidth; } } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Tables { class TablePr; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { namespace TableLayout { class LayoutWidth; } } }
namespace Aspose { namespace Words { enum class RevisionsView; } }
namespace Aspose { namespace Words { namespace Tables { enum class CellMerge; } } }
namespace Aspose { namespace Words { namespace Tables { class TableGridFromCellWidth; } } }

namespace Aspose {

namespace Words {

namespace Tables {

/// Represents a table in a Word document.
/// 
/// <b>Table</b> is a block-level node and can be a child of classes derived from <b>Story</b> or
/// <b>InlineStory</b>.
/// 
/// <b>Table</b> can contain one or more <b>Row</b> nodes.
/// 
/// A minimal valid table needs to have at least one <b>Row</b>.
class ASPOSE_WORDS_SHARED_CLASS Table : public Aspose::Words::CompositeNode
{
    typedef Table ThisType;
    typedef Aspose::Words::CompositeNode BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::Tables::DocRtfGridHandler;
    friend class Aspose::Words::Tables::TableMerger;
    friend class Aspose::Words::Layout::Core::TableLayout;
    friend class Aspose::Words::Tables::FixedGridCalculator;
    friend class Aspose::Words::Layout::Core::SpanGenerator;
    friend class Aspose::Words::Fields::Expressions::CellPosition;
    friend class Aspose::Words::Fields::NewResultEnumerator;
    friend class Aspose::Words::TableLayout::Extensions;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::TableValidator;
    friend class Aspose::Words::RW::Doc::Tables::DocTableContext;
    friend class Aspose::Words::RW::Html::Writer::HtmlTableWriter;
    friend class Aspose::Words::TableLayout::TableLayouter;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::Paragraph;
    friend class Aspose::Words::RW::Docx::Reader::DocxDocumentReaderBase;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Nrx::NrxTableGrid;
    friend class Aspose::Words::RW::Celler::CellerTable;
    friend class Aspose::Words::RW::Docx::Reader::DocxTableReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Html::Reader::HtmlTableReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtTableWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfTableDefWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlTableReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    /// Returns <b>NodeType.Table</b>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns the first <b>Row</b> node in the table.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Row> get_FirstRow();
    /// Returns the last <b>Row</b> node in the table.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::Row> get_LastRow();
    /// Provides typed access to the rows of the table.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::RowCollection> get_Rows();
    /// Specifies how an inline table is aligned in the document.
    /// 
    /// The default value is <see cref="Aspose::Words::Tables::TableAlignment::Left">Left</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Tables::TableAlignment get_Alignment();
    /// Specifies how an inline table is aligned in the document.
    /// 
    /// The default value is <see cref="Aspose::Words::Tables::TableAlignment::Left">Left</see>.
    ASPOSE_WORDS_SHARED_API void set_Alignment(Aspose::Words::Tables::TableAlignment value);
    /// Allows Microsoft Word and Aspose.Words to automatically resize cells in a table to fit their contents.
    /// 
    /// The default value is <c>true</c>.
    /// 
    /// @sa Aspose::Words::Tables::Table::AutoFit(Aspose::Words::Tables::AutoFitBehavior)
    ASPOSE_WORDS_SHARED_API bool get_AllowAutoFit();
    /// Setter for Aspose::Words::Tables::Table::get_AllowAutoFit
    ASPOSE_WORDS_SHARED_API void set_AllowAutoFit(bool value);
    /// Gets the table preferred width.
    /// 
    /// The default value is <see cref="Aspose::Words::Tables::PreferredWidth::Auto">Auto</see>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Tables::PreferredWidth> get_PreferredWidth();
    /// Sets the table preferred width.
    /// 
    /// The default value is <see cref="Aspose::Words::Tables::PreferredWidth::Auto">Auto</see>.
    ASPOSE_WORDS_SHARED_API void set_PreferredWidth(System::SharedPtr<Aspose::Words::Tables::PreferredWidth> value);
    /// Gets or sets whether this is a right-to-left table.
    /// 
    /// When <c>true</c>, the cells in this row are laid out right to left.
    /// 
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool get_Bidi();
    /// Setter for Aspose::Words::Tables::Table::get_Bidi
    ASPOSE_WORDS_SHARED_API void set_Bidi(bool value);
    /// Gets the amount of space (in points) to add to the left of the contents of cells.
    ASPOSE_WORDS_SHARED_API double get_LeftPadding();
    /// Sets the amount of space (in points) to add to the left of the contents of cells.
    ASPOSE_WORDS_SHARED_API void set_LeftPadding(double value);
    /// Gets the amount of space (in points) to add to the right of the contents of cells.
    ASPOSE_WORDS_SHARED_API double get_RightPadding();
    /// Sets the amount of space (in points) to add to the right of the contents of cells.
    ASPOSE_WORDS_SHARED_API void set_RightPadding(double value);
    /// Gets the amount of space (in points) to add above the contents of cells.
    ASPOSE_WORDS_SHARED_API double get_TopPadding();
    /// Sets the amount of space (in points) to add above the contents of cells.
    ASPOSE_WORDS_SHARED_API void set_TopPadding(double value);
    /// Gets the amount of space (in points) to add below the contents of cells.
    ASPOSE_WORDS_SHARED_API double get_BottomPadding();
    /// Sets the amount of space (in points) to add below the contents of cells.
    ASPOSE_WORDS_SHARED_API void set_BottomPadding(double value);
    /// Gets the amount of space (in points) between the cells.
    ASPOSE_WORDS_SHARED_API double get_CellSpacing();
    /// Sets the amount of space (in points) between the cells.
    ASPOSE_WORDS_SHARED_API void set_CellSpacing(double value);
    /// Gets the "Allow spacing between cells" option.
    ASPOSE_WORDS_SHARED_API bool get_AllowCellSpacing();
    /// Sets the "Allow spacing between cells" option.
    ASPOSE_WORDS_SHARED_API void set_AllowCellSpacing(bool value);
    /// Gets the value that represents the left indent of the table.
    ASPOSE_WORDS_SHARED_API double get_LeftIndent();
    /// Sets the value that represents the left indent of the table.
    ASPOSE_WORDS_SHARED_API void set_LeftIndent(double value);
    /// Gets bit flags that specify how a table style is applied to this table.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Tables::TableStyleOptions get_StyleOptions();
    /// Sets bit flags that specify how a table style is applied to this table.
    ASPOSE_WORDS_SHARED_API void set_StyleOptions(Aspose::Words::Tables::TableStyleOptions value);
    /// Gets the table style applied to this table.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Style> get_Style();
    /// Sets the table style applied to this table.
    ASPOSE_WORDS_SHARED_API void set_Style(System::SharedPtr<Aspose::Words::Style> value);
    /// Gets the name of the table style applied to this table.
    ASPOSE_WORDS_SHARED_API System::String get_StyleName();
    /// Sets the name of the table style applied to this table.
    ASPOSE_WORDS_SHARED_API void set_StyleName(System::String value);
    /// Gets the locale independent style identifier of the table style applied to this table.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StyleIdentifier get_StyleIdentifier();
    /// Sets the locale independent style identifier of the table style applied to this table.
    ASPOSE_WORDS_SHARED_API void set_StyleIdentifier(Aspose::Words::StyleIdentifier value);
    /// Gets <see cref="Aspose::Words::Tables::Table::get_TextWrapping">TextWrapping</see> for table.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Tables::TextWrapping get_TextWrapping();
    /// Sets <see cref="Aspose::Words::Tables::Table::get_TextWrapping">TextWrapping</see> for table.
    ASPOSE_WORDS_SHARED_API void set_TextWrapping(Aspose::Words::Tables::TextWrapping value);
    /// Gets or sets title of this table.
    /// It provides an alternative text representation of the information contained in the table.
    /// 
    /// The default value is an empty string.
    /// 
    /// This property is meaningful for ISO/IEC 29500 compliant DOCX documents
    /// (<see cref="Aspose::Words::Saving::OoxmlCompliance">OoxmlCompliance</see>).
    /// When saved to pre-ISO/IEC 29500 formats, the property is ignored.
    ASPOSE_WORDS_SHARED_API System::String get_Title();
    /// Setter for Aspose::Words::Tables::Table::get_Title
    ASPOSE_WORDS_SHARED_API void set_Title(System::String value);
    /// Gets or sets description of this table.
    /// It provides an alternative text representation of the information contained in the table.
    /// 
    /// The default value is an empty string.
    /// 
    /// This property is meaningful for ISO/IEC 29500 compliant DOCX documents
    /// (<see cref="Aspose::Words::Saving::OoxmlCompliance">OoxmlCompliance</see>).
    /// When saved to pre-ISO/IEC 29500 formats, the property is ignored.
    ASPOSE_WORDS_SHARED_API System::String get_Description();
    /// Setter for Aspose::Words::Tables::Table::get_Description
    ASPOSE_WORDS_SHARED_API void set_Description(System::String value);
    /// Gets distance between table left and the surrounding text, in points.
    ASPOSE_WORDS_SHARED_API double get_DistanceLeft();
    /// Gets distance between table right and the surrounding text, in points.
    ASPOSE_WORDS_SHARED_API double get_DistanceRight();
    /// Gets distance between table top and the surrounding text, in points.
    ASPOSE_WORDS_SHARED_API double get_DistanceTop();
    /// Gets distance between table bottom and the surrounding text, in points.
    ASPOSE_WORDS_SHARED_API double get_DistanceBottom();
    /// Gets floating table relative horizontal alignment.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::HorizontalAlignment get_RelativeHorizontalAlignment();
    /// Sets floating table relative horizontal alignment.
    ASPOSE_WORDS_SHARED_API void set_RelativeHorizontalAlignment(Aspose::Words::Drawing::HorizontalAlignment value);
    /// Gets floating table relative vertical alignment.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::VerticalAlignment get_RelativeVerticalAlignment();
    /// Sets floating table relative vertical alignment.
    ASPOSE_WORDS_SHARED_API void set_RelativeVerticalAlignment(Aspose::Words::Drawing::VerticalAlignment value);
    /// Gets the base object from which the horizontal positioning of floating table should be calculated.
    /// Default value is <see cref="Aspose::Words::Drawing::RelativeHorizontalPosition::Column">Column</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::RelativeHorizontalPosition get_HorizontalAnchor();
    /// Gets the base object from which the horizontal positioning of floating table should be calculated.
    /// Default value is <see cref="Aspose::Words::Drawing::RelativeHorizontalPosition::Column">Column</see>.
    ASPOSE_WORDS_SHARED_API void set_HorizontalAnchor(Aspose::Words::Drawing::RelativeHorizontalPosition value);
    /// Gets the base object from which the vertical positioning of floating table should be calculated.
    /// Default value is <see cref="Aspose::Words::Drawing::RelativeVerticalPosition::Margin">Margin</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::RelativeVerticalPosition get_VerticalAnchor();
    /// Gets the base object from which the vertical positioning of floating table should be calculated.
    /// Default value is <see cref="Aspose::Words::Drawing::RelativeVerticalPosition::Margin">Margin</see>.
    ASPOSE_WORDS_SHARED_API void set_VerticalAnchor(Aspose::Words::Drawing::RelativeVerticalPosition value);
    /// Gets absolute horizontal floating table position specified by the table properties, in points.
    /// Default value is 0.
    ASPOSE_WORDS_SHARED_API double get_AbsoluteHorizontalDistance();
    /// Sets absolute horizontal floating table position specified by the table properties, in points.
    /// Default value is 0.
    ASPOSE_WORDS_SHARED_API void set_AbsoluteHorizontalDistance(double value);
    /// Gets absolute vertical floating table position specified by the table properties, in points.
    /// Default value is 0.
    ASPOSE_WORDS_SHARED_API double get_AbsoluteVerticalDistance();
    /// Sets absolute vertical floating table position specified by the table properties, in points.
    /// Default value is 0.
    ASPOSE_WORDS_SHARED_API void set_AbsoluteVerticalDistance(double value);
    /// Gets whether a floating table shall allow other floating objects in the document
    /// to overlap its extents when displayed.
    /// Default value is <c>true</c>.
    ASPOSE_WORDS_SHARED_API bool get_AllowOverlap();

    /// Initializes a new instance of the <b>Table</b> class.
    /// 
    /// When <b>Table</b> is created, it belongs to the specified document, but is not
    /// yet part of the document and <b>ParentNode</b> is null.
    /// 
    /// To append <b>Table</b> to the document use InsertAfter or InsertBefore
    /// on the story where you want the table inserted.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API Table(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Converts cells horizontally merged by width to cells merged by <see cref="Aspose::Words::Tables::CellFormat::get_HorizontalMerge">HorizontalMerge</see>.
    ASPOSE_WORDS_SHARED_API void ConvertToHorizontallyMergedCells();
    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    /// If the table has no rows, creates and appends one <b>Row</b>.
    ASPOSE_WORDS_SHARED_API void EnsureMinimum();
    /// Sets all table borders to the specified line style, width and color.
    /// 
    /// @param lineStyle The line style to apply.
    /// @param lineWidth The line width to set (in points).
    /// @param color The color to use for the border.
    ASPOSE_WORDS_SHARED_API void SetBorders(Aspose::Words::LineStyle lineStyle, double lineWidth, System::Drawing::Color color);
    /// Sets the specified table border to the specified line style, width and color.
    /// 
    /// @param borderType The table border to change.
    /// @param lineStyle The line style to apply.
    /// @param lineWidth The line width to set (in points).
    /// @param color The color to use for the border.
    /// @param isOverrideCellBorders When <c>true</c>, causes all existing explicit cell borders to be removed.
    ASPOSE_WORDS_SHARED_API void SetBorder(Aspose::Words::BorderType borderType, Aspose::Words::LineStyle lineStyle, double lineWidth, System::Drawing::Color color, bool isOverrideCellBorders);
    /// Removes all table and cell borders on this table.
    ASPOSE_WORDS_SHARED_API void ClearBorders();
    /// Sets shading to the specified values on all cells in the table.
    /// 
    /// @param texture The texture to apply.
    /// @param foregroundColor The color of the texture.
    /// @param backgroundColor The color of the background fill.
    ASPOSE_WORDS_SHARED_API void SetShading(Aspose::Words::TextureIndex texture, System::Drawing::Color foregroundColor, System::Drawing::Color backgroundColor);
    /// Removes all shading on the table.
    ASPOSE_WORDS_SHARED_API void ClearShading();
    /// Resizes the table and cells according to the specified auto fit behavior.
    /// 
    /// This method mimics the commands available in the Auto Fit menu for a table in Microsoft Word.
    /// The commands available are "Auto Fit to Contents", "Auto Fit to Window" and "Fixed Column Width". In Microsoft Word
    /// these commands set relevant table properties and then update the table layout and Aspose.Words does the same for you.
    /// 
    /// @param behavior Specifies how to auto fit the table.
    ASPOSE_WORDS_SHARED_API void AutoFit(Aspose::Words::Tables::AutoFitBehavior behavior);

protected:

    bool get_HasCells();
    bool get_IsNested();
    int32_t get_Istd();
    void set_Istd(int32_t value);
    bool get_IsFloating();
    System::SharedPtr<Aspose::Words::Tables::TablePr> get_TablePr();
    bool get_HasRevision();

    static const int32_t MaxColumns;
    static const int32_t InitialCellSpacing;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    System::SharedPtr<Aspose::Words::Tables::Cell> GetCell(int32_t columnIndex, int32_t rowIndex);
    int32_t GetColumnCount();
    System::SharedPtr<Aspose::Words::TableLayout::LayoutWidth> UpdateLayout();
    void NormalizeHorizontalMerge();
    void NormalizeVerticalMerge();
    void FixNestedTableLayout();
    int32_t GetHeaderRowCount();
    int32_t GetTableLeft();
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    System::SharedPtr<Aspose::Words::Tables::Row> FindDifferentPositioningOrStyle();
    int32_t GetCellsWidth();
    void RemoveAttrFromAllRows(int32_t key);
    void SetAttrOnAllRows(int32_t key, System::SharedPtr<System::Object> value);
    void SetAttrOnAllRowsNoOverride(int32_t key, System::SharedPtr<System::Object> value);
    void CellWidthsToTableGrid();

    virtual ASPOSE_WORDS_SHARED_API ~Table();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Tables::RowCollection> mRowsCache;
    System::SharedPtr<Aspose::Words::Tables::TablePr> mTablePr;

    static int32_t GetColumnCount(System::SharedPtr<Aspose::Words::Tables::Row> row);
    void NormalizeVerticalMerge(Aspose::Words::RevisionsView view);
    void FixNestedTableAutoFit();
    void FixNestedTableNegativeLefIndent();
    static Aspose::Words::Tables::CellMerge GetVerticalMerge(System::SharedPtr<Aspose::Words::Tables::Cell> cell, Aspose::Words::RevisionsView view);
    static void SpecifyRowHeightRule(System::SharedPtr<Aspose::Words::Tables::Row> row);
    void RemoveTextWrapping();
    void SetTextWrapping();
    void RemoveCellBorders(Aspose::Words::BorderType borderType);
    void AutoFitToContents();
    void AutoFitToWindow();
    void RemoveAttrFromAllCells(int32_t key);
    void AutoFitFixedColumnWidths();
    static void UpdateRowGrid(System::SharedPtr<Aspose::Words::Tables::TablePr> rowPr, System::SharedPtr<Aspose::Words::Tables::TableGridFromCellWidth> tableGrid);
    void RemoveCalculatedGrid();
    void SetCellPreferredToCurrentWidth();
    void PrepareRowBeforSetIstd(System::SharedPtr<Aspose::Words::Tables::Row> row);
    System::SharedPtr<System::Object> FetchFirstRowAttr(int32_t key);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
