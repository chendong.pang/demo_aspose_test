//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Settings/WriteProtection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Settings { class DocPr; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfFileHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Settings { class PasswordHash; } } }

namespace Aspose {

namespace Words {

namespace Settings {

/// Specifies write protection settings for a document.
/// 
/// Write protection specifies whether the author has recommended that
/// the document is to be opened as read-only and/or require a password to modify a document.
/// 
/// Write protection is different from document protection. Write protection is specified in
/// Microsoft Word in the options of the Save As dialog box.
/// 
/// You do not create instances of this class directly. You access document protection settings
/// via the <see cref="Aspose::Words::Document::get_WriteProtection">WriteProtection</see> property.
class ASPOSE_WORDS_SHARED_CLASS WriteProtection : public System::Object
{
    typedef WriteProtection ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Settings::DocPr;
    friend class Aspose::Words::RW::Docx::Reader::DocxSettingsReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfFileHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;

public:

    /// Specifies whether the document author has recommended that the document be opened as read-only.
    ASPOSE_WORDS_SHARED_API bool get_ReadOnlyRecommended() const;
    /// Specifies whether the document author has recommended that the document be opened as read-only.
    ASPOSE_WORDS_SHARED_API void set_ReadOnlyRecommended(bool value);
    /// Returns true when a write protection password is set.
    ASPOSE_WORDS_SHARED_API bool get_IsWriteProtected();

    /// Sets the write protection password for the document.
    /// 
    /// If a password is set, Microsoft Word will require the user to enter it or open
    /// the document as read-only.
    /// 
    /// @param password The password to set. Cannot be null, but can be an empty string.
    ASPOSE_WORDS_SHARED_API void SetPassword(System::String password);
    /// Returns true if the specified password is the same as the write-protection password the document was protected with.
    /// If document is not write-protected with password then returns false.
    ASPOSE_WORDS_SHARED_API bool ValidatePassword(System::String password);

protected:

    System::SharedPtr<Aspose::Words::Settings::PasswordHash> get_PasswordHash() const;

    WriteProtection();

    System::SharedPtr<Aspose::Words::Settings::WriteProtection> Clone();
    System::String GetPassword();
    void UpdateDocxHash();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mReadOnlyRecommended;
    System::String mPassword;
    System::SharedPtr<Aspose::Words::Settings::PasswordHash> mPasswordHash;

};

}
}
}
