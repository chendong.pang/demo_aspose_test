//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Settings/HyphenationOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Settings { class DocPr; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxSettingsReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class DocPrFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfDocPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlDocPrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlDocPrWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Settings {

/// Allows to configure document hyphenation options.
class ASPOSE_WORDS_SHARED_CLASS HyphenationOptions : public System::Object
{
    typedef HyphenationOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Settings::DocPr;
    friend class Aspose::Words::RW::Docx::Reader::DocxSettingsReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;
    friend class Aspose::Words::RW::Doc::DocPrFiler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfDocPrReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlDocPrReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlDocPrWriter;

public:

    /// Gets value determining whether automatic hyphenation is turned on for the document.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_AutoHyphenation() const;
    /// Sets value determining whether automatic hyphenation is turned on for the document.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_AutoHyphenation(bool value);
    /// Gets or sets the maximum number of consecutive lines that can end with hyphens.
    /// Default value for this property is 0.
    /// 
    /// If value of this property is set to 0, any number of consecutive lines can end with hyphens.
    /// 
    /// The property does not have effect when saving to fixed page formats e.g. PDF.
    ASPOSE_WORDS_SHARED_API int32_t get_ConsecutiveHyphenLimit() const;
    /// Setter for Aspose::Words::Settings::HyphenationOptions::get_ConsecutiveHyphenLimit
    ASPOSE_WORDS_SHARED_API void set_ConsecutiveHyphenLimit(int32_t value);
    /// Gets the distance in 1/20 of a point from the right margin within which you do not want
    /// to hyphenate words.
    /// Default value for this property is 360 (0.25 inch).
    ASPOSE_WORDS_SHARED_API int32_t get_HyphenationZone() const;
    /// Sets the distance in 1/20 of a point from the right margin within which you do not want
    /// to hyphenate words.
    /// Default value for this property is 360 (0.25 inch).
    ASPOSE_WORDS_SHARED_API void set_HyphenationZone(int32_t value);
    /// Gets value determining whether words written in all capital letters are hyphenated.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_HyphenateCaps() const;
    /// Sets value determining whether words written in all capital letters are hyphenated.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_HyphenateCaps(bool value);

    ASPOSE_WORDS_SHARED_API HyphenationOptions();

protected:

    static const int32_t ConsecutiveHyphenLimitDefault;
    static const int32_t HyphenationZoneDefault;
    static const bool HyphenateCapsDefault;

    System::SharedPtr<Aspose::Words::Settings::HyphenationOptions> Clone();
    void SetConsecutiveHyphenLimitSafe(int32_t value);
    void SetHyphenationZoneSafe(int32_t value);

private:

    bool mAutoHyphenation;
    int32_t mConsecutiveHyphenLimit;
    int32_t mHyphenationZone;
    bool mHyphenateCaps;

    static bool IsValidConsecutiveHyphenLimit(int32_t value);
    static bool IsValidHyphenationZone(int32_t value);

};

}
}
}
