//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Settings/OdsoFieldMapDataCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/Model/Settings/OdsoFieldMapData.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FormattableMergeFieldMerger; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class MailMergePrWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Settings {

/// A typed collection of the <see cref="Aspose::Words::Settings::OdsoFieldMapData">OdsoFieldMapData</see> objects.
/// 
/// @sa Aspose::Words::Settings::OdsoFieldMapData
/// @sa Aspose::Words::Settings::Odso::get_FieldMapDatas
class ASPOSE_WORDS_SHARED_CLASS OdsoFieldMapDataCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData>>
{
    typedef OdsoFieldMapDataCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FormattableMergeFieldMerger;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::Doc::Writer::MailMergePrWriter;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Gets or sets an item in this collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData> idx_get(int32_t index);
    /// Gets or sets an item in this collection.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData> value);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData>>> GetEnumerator() override;
    /// Adds an object to the end of this collection.
    /// 
    /// @param value The object to add. Cannot be null.
    ASPOSE_WORDS_SHARED_API int32_t Add(System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData> value);
    /// Removes all elements from this collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Removes the element at the specified index.
    /// 
    /// @param index The zero-based index of the element.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);

    ASPOSE_WORDS_SHARED_API OdsoFieldMapDataCollection();

protected:

    static const int32_t ValidMapLength;

    void MakeValid();

    virtual ASPOSE_WORDS_SHARED_API ~OdsoFieldMapDataCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Settings::OdsoFieldMapData>>> mItems;

};

}
}
}
