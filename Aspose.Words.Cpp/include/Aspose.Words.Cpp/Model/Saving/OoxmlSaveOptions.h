//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/OoxmlSaveOptions.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Saving/SaveOptions.h"
#include "Aspose.Words.Cpp/Model/Saving/OoxmlCompliance.h"
#include "Aspose.Words.Cpp/Model/Saving/CompressionLevel.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class OoxmlComplianceInfo; } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class Iso29500ComplianceEnforcer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace Saving { enum class OoxmlComplianceCore; } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Can be used to specify additional options when saving a document into the <see cref="Aspose::Words::SaveFormat::Docx">Docx</see>,
/// <see cref="Aspose::Words::SaveFormat::Docm">Docm</see>, <see cref="Aspose::Words::SaveFormat::Dotx">Dotx</see>, <see cref="Aspose::Words::SaveFormat::Dotm">Dotm</see> or
/// <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see> format.
class ASPOSE_WORDS_SHARED_CLASS OoxmlSaveOptions : public Aspose::Words::Saving::SaveOptions
{
    typedef OoxmlSaveOptions ThisType;
    typedef Aspose::Words::Saving::SaveOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::OoxmlComplianceInfo;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::Iso29500ComplianceEnforcer;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;

public:

    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can be <see cref="Aspose::Words::SaveFormat::Docx">Docx</see>, <see cref="Aspose::Words::SaveFormat::Docm">Docm</see>,
    /// <see cref="Aspose::Words::SaveFormat::Dotx">Dotx</see>, <see cref="Aspose::Words::SaveFormat::Dotm">Dotm</see> or <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat get_SaveFormat() override;
    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can be <see cref="Aspose::Words::SaveFormat::Docx">Docx</see>, <see cref="Aspose::Words::SaveFormat::Docm">Docm</see>,
    /// <see cref="Aspose::Words::SaveFormat::Dotx">Dotx</see>, <see cref="Aspose::Words::SaveFormat::Dotm">Dotm</see> or <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see>.
    ASPOSE_WORDS_SHARED_API void set_SaveFormat(Aspose::Words::SaveFormat value) override;
    /// Gets/sets a password to encrypt document using ECMA376 Standard encryption algorithm.
    /// 
    /// In order to save document without encryption this property should be null or empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Password() const;
    /// Gets/sets a password to encrypt document using ECMA376 Standard encryption algorithm.
    /// 
    /// In order to save document without encryption this property should be null or empty string.
    ASPOSE_WORDS_SHARED_API void set_Password(System::String value);
    /// Specifies the OOXML version for the output document.
    /// The default value is <see cref="Aspose::Words::Saving::OoxmlCompliance::Ecma376_2006">Ecma376_2006</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::OoxmlCompliance get_Compliance();
    /// Specifies the OOXML version for the output document.
    /// The default value is <see cref="Aspose::Words::Saving::OoxmlCompliance::Ecma376_2006">Ecma376_2006</see>.
    ASPOSE_WORDS_SHARED_API void set_Compliance(Aspose::Words::Saving::OoxmlCompliance value);
    /// Keeps original representation of legacy control characters.
    ASPOSE_WORDS_SHARED_API bool get_KeepLegacyControlChars() const;
    /// Keeps original representation of legacy control characters.
    ASPOSE_WORDS_SHARED_API void set_KeepLegacyControlChars(bool value);
    /// Specifies the compression level used to save document.
    /// The default value is <see cref="Aspose::Words::Saving::CompressionLevel::Normal">Normal</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::CompressionLevel get_CompressionLevel() const;
    /// Specifies the compression level used to save document.
    /// The default value is <see cref="Aspose::Words::Saving::CompressionLevel::Normal">Normal</see>.
    ASPOSE_WORDS_SHARED_API void set_CompressionLevel(Aspose::Words::Saving::CompressionLevel value);

    /// Initializes a new instance of this class that can be used to save a document in the <see cref="Aspose::Words::SaveFormat::Docx">Docx</see> format.
    ASPOSE_WORDS_SHARED_API OoxmlSaveOptions();
    /// Initializes a new instance of this class that can be used to save a document in the <see cref="Aspose::Words::SaveFormat::Docx">Docx</see>,
    /// <see cref="Aspose::Words::SaveFormat::Docm">Docm</see>, <see cref="Aspose::Words::SaveFormat::Dotx">Dotx</see>, <see cref="Aspose::Words::SaveFormat::Dotm">Dotm</see> or
    /// <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see> format.
    /// 
    /// @param saveFormat Can be <see cref="Aspose::Words::SaveFormat::Docx">Docx</see>, <see cref="Aspose::Words::SaveFormat::Docm">Docm</see>,
    ///     <see cref="Aspose::Words::SaveFormat::Dotx">Dotx</see>, <see cref="Aspose::Words::SaveFormat::Dotm">Dotm</see> or <see cref="Aspose::Words::SaveFormat::FlatOpc">FlatOpc</see>.
    ASPOSE_WORDS_SHARED_API OoxmlSaveOptions(Aspose::Words::SaveFormat saveFormat);

protected:

    Aspose::Words::Saving::OoxmlComplianceCore get_ComplianceCore() const;
    void set_ComplianceCore(Aspose::Words::Saving::OoxmlComplianceCore value);
    bool get_ConvertDmlPictureToVml() const;
    void set_ConvertDmlPictureToVml(bool value);
    bool get_WriteClrSchemeMapping() const;
    void set_WriteClrSchemeMapping(bool value);
    bool get_WriteWordCountOption() const;
    void set_WriteWordCountOption(bool value);
    bool get_WriteDoNotTrackMovesCorrectly() const;
    void set_WriteDoNotTrackMovesCorrectly(bool value);
    bool get_WriteExtendedIds() const;
    void set_WriteExtendedIds(bool value);
    bool get_UserSetCompliance() const;

    static System::SharedPtr<Aspose::Words::Saving::OoxmlSaveOptions> DocxWithCompliance(Aspose::Words::Saving::OoxmlComplianceCore compliance);
    ASPOSE_WORDS_SHARED_API void SetTestMode() override;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mUserSetCompliance;
    Aspose::Words::SaveFormat mSaveFormat;
    Aspose::Words::Saving::OoxmlComplianceCore mComplianceCore;
    bool mConvertDmlPictureToVml;
    bool mWriteClrSchemeMapping;
    bool mWriteWordCountOption;
    bool mWriteDoNotTrackMovesCorrectly;
    bool mWriteExtendedIds;
    bool mKeepLegacyControlChars;
    Aspose::Words::Saving::CompressionLevel mCompressionLevel;
    System::String mPassword;

    void SetSaveFormatCore(Aspose::Words::SaveFormat saveFormat);

};

}
}
}
