//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PdfDigitalSignatureTimestampSettings.h
#pragma once

#include <system/timespan.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Saving { class PdfDigitalSignatureDetails; } } }
namespace Aspose { namespace Rendering { namespace Pdf { namespace Signature { class PdfDigitalSignatureTimestampSettingsCore; } } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Contains settings of the digital signature timestamp.
class ASPOSE_WORDS_SHARED_CLASS PdfDigitalSignatureTimestampSettings : public System::Object
{
    typedef PdfDigitalSignatureTimestampSettings ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Saving::PdfDigitalSignatureDetails;

public:

    /// Timestamp server URL.
    ASPOSE_WORDS_SHARED_API System::String get_ServerUrl() const;
    /// Timestamp server URL.
    ASPOSE_WORDS_SHARED_API void set_ServerUrl(System::String value);
    /// Timestamp server user name.
    ASPOSE_WORDS_SHARED_API System::String get_UserName() const;
    /// Timestamp server user name.
    ASPOSE_WORDS_SHARED_API void set_UserName(System::String value);
    /// Timestamp server password.
    ASPOSE_WORDS_SHARED_API System::String get_Password() const;
    /// Timestamp server password.
    ASPOSE_WORDS_SHARED_API void set_Password(System::String value);
    /// Time-out value for accessing timestamp server.
    ASPOSE_WORDS_SHARED_API System::TimeSpan get_Timeout() const;
    /// Time-out value for accessing timestamp server.
    ASPOSE_WORDS_SHARED_API void set_Timeout(System::TimeSpan value);

    /// Initializes an instance of this class.
    ASPOSE_WORDS_SHARED_API PdfDigitalSignatureTimestampSettings();
    /// Initializes an instance of this class.
    /// 
    /// @param serverUrl Timestamp server URL.
    /// @param userName Timestamp server user name.
    /// @param password Timestamp server password.
    ASPOSE_WORDS_SHARED_API PdfDigitalSignatureTimestampSettings(System::String serverUrl, System::String userName, System::String password);
    /// Initializes an instance of this class.
    /// 
    /// @param serverUrl Timestamp server URL.
    /// @param userName Timestamp server user name.
    /// @param password Timestamp server password.
    /// @param timeout Time-out value for accessing timestamp server.
    ASPOSE_WORDS_SHARED_API PdfDigitalSignatureTimestampSettings(System::String serverUrl, System::String userName, System::String password, System::TimeSpan timeout);

protected:

    System::SharedPtr<Aspose::Rendering::Pdf::Signature::PdfDigitalSignatureTimestampSettingsCore> ToCore();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mServerUrl;
    System::String mUserName;
    System::String mPassword;
    System::TimeSpan mTimeout;

};

}
}
}
