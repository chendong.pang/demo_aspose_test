//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PageSet/PageSet.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Saving { class PageSavingCallbackCore; } } }
namespace Aspose { namespace Words { namespace RW { class FixedPageWriterBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Imaging { namespace Writer { class ImagingWriter; } } } } }
namespace Aspose { namespace Words { namespace Saving { class PageSetBase; } } }
namespace Aspose { namespace Words { namespace Saving { class PageRange; } } }
namespace Aspose { namespace Words { namespace Saving { enum class PageParity; } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Describes a random set of pages.
class ASPOSE_WORDS_SHARED_CLASS PageSet FINAL : public System::Object
{
    typedef PageSet ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Saving::PageSavingCallbackCore;
    friend class Aspose::Words::RW::FixedPageWriterBase;
    friend class Aspose::Words::RW::Imaging::Writer::ImagingWriter;

public:

    /// Gets a set with all the pages of the document in their original order.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::PageSet> get_All();
    /// Gets a set with all the even pages of the document in their original order.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::PageSet> get_Even();
    /// Gets a set with all the odd pages of the document in their original order.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::PageSet> get_Odd();

    /// Creates a page set based on exact page indices.
    /// @param pages Zero-based indices of pages
    /// If a page is encountered that is not in the document, an exception will be thrown during rendering.
    /// System::Int32::MaxValue means the last page in the document.
    ASPOSE_WORDS_SHARED_API PageSet(const System::ArrayPtr<int32_t>& pages);

    /// Creates a page set based on ranges.
    ///
    /// @param ranges Array of page ranges
    ///
    /// If a range is encountered that starts after the last page in the document, 
    /// an exception will be thrown during rendering.
    /// All ranges that end after the last page are truncated to fit in the document.
    ASPOSE_WORDS_SHARED_API PageSet(const System::ArrayPtr<System::SharedPtr<Aspose::Words::Saving::PageRange>>& ranges);

protected:

    System::SharedPtr<Aspose::Words::Saving::PageSetBase> get_Core() const;

    PageSet(System::String pages);
    PageSet(Aspose::Words::Saving::PageParity parity, const System::ArrayPtr<System::SharedPtr<Aspose::Words::Saving::PageRange>>& ranges);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Saving::PageSetBase> mSet;

};

}
}
}
