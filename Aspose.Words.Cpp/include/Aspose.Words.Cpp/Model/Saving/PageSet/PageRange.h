//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PageSet/PageRange.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Saving { class PageSetOnRangesEnumerator; } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Represents a continuous range of pages.
class ASPOSE_WORDS_SHARED_CLASS PageRange FINAL : public System::Object
{
    typedef PageRange ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Saving::PageSetOnRangesEnumerator;

public:

    /// Creates a new page range object.
    /// 
    /// @param from The starting page zero-based index.
    /// @param to The ending page zero-based index.
    ///     If it exceeds the index of the last page in the document,
    ///     it is truncated to fit in the document on rendering.
    ASPOSE_WORDS_SHARED_API PageRange(int32_t from, int32_t to);

protected:

    int32_t get_From() const;
    int32_t get_To() const;
    int32_t get_Count();
    int32_t get_CountEven();
    int32_t get_CountOdd();

    System::SharedPtr<Aspose::Words::Saving::PageRange> Initialize(int32_t documentPageCount);

private:

    int32_t mFrom;
    int32_t mTo;

    static const System::String& NegativePageIndexMessage();
    static const System::String& ToLessThanFromMessage();
    static const System::String& PageCountExceeded();

};

}
}
}
