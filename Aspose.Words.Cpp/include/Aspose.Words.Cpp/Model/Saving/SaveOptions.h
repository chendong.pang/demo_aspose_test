//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/SaveOptions.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Saving/DmlRenderingMode.h"
#include "Aspose.Words.Cpp/Model/Saving/DmlEffectsRenderingMode.h"
#include "Aspose.Words.Cpp/Model/Saving/Dml3DEffectsRenderingMode.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlStylesheetWriter; } } } } }
namespace Aspose { namespace Words { namespace Layout { class LayoutOptionsCore; } } }
namespace Aspose { namespace Words { namespace Saving { class HtmlSaveOptions; } } }
namespace Aspose { namespace Words { namespace Saving { class OoxmlSaveOptions; } } }
namespace Aspose { namespace Words { namespace Saving { class RtfSaveOptions; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace RW { class FixedPageWriterBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Epub { namespace Writer { class NcxBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Epub { namespace Writer { class OpfBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxSettingsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Writer { class MhtmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtMetaWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtContentWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfHeaderWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlDocPrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// This is an abstract base class for classes that allow the user to specify additional
/// options when saving a document into a particular format.
class ASPOSE_WORDS_SHARED_CLASS SaveOptions : public System::Object
{
    typedef SaveOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlStylesheetWriter;
    friend class Aspose::Words::Layout::LayoutOptionsCore;
    friend class Aspose::Words::Saving::HtmlSaveOptions;
    friend class Aspose::Words::Saving::OoxmlSaveOptions;
    friend class Aspose::Words::Saving::RtfSaveOptions;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::RW::FixedPageWriterBase;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Epub::Writer::NcxBuilder;
    friend class Aspose::Words::RW::Epub::Writer::OpfBuilder;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxSettingsWriter;
    friend class Aspose::Words::RW::Mhtml::Writer::MhtmlWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtMetaWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtContentWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfDocPrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfHeaderWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlDocPrWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Specifies the format in which the document will be saved if this save options object is used.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat get_SaveFormat() = 0;
    /// Specifies the format in which the document will be saved if this save options object is used.
    virtual ASPOSE_WORDS_SHARED_API void set_SaveFormat(Aspose::Words::SaveFormat value) = 0;
    /// Specifies the folder for temporary files used when saving to a DOC or DOCX file.
    /// By default this property is <c>null</c> and no temporary files are used.
    /// 
    /// When Aspose.Words saves a document, it needs to create temporary internal structures. By default,
    /// these internal structures are created in memory and the memory usage spikes for a short period while
    /// the document is being saved. When saving is complete, the memory is freed and reclaimed by the garbage collector.
    /// 
    /// If you are saving a very large document (thousands of pages) and/or processing many documents at the same time,
    /// then the memory spike during saving can be significant enough to cause the system to throw <see cref="System::OutOfMemoryException">OutOfMemoryException</see>.
    /// Specifying a temporary folder using <see cref="Aspose::Words::Saving::SaveOptions::get_TempFolder">TempFolder</see> will cause Aspose.Words to keep the internal structures in
    /// temporary files instead of memory. It reduces the memory usage during saving, but will decrease the save performance.
    /// 
    /// The folder must exist and be writable, otherwise an exception will be thrown.
    /// 
    /// Aspose.Words automatically deletes all temporary files when saving is complete.
    ASPOSE_WORDS_SHARED_API System::String get_TempFolder() const;
    /// Setter for Aspose::Words::Saving::SaveOptions::get_TempFolder
    ASPOSE_WORDS_SHARED_API void set_TempFolder(System::String value);
    /// When <c>true</c>, pretty formats output where applicable.
    /// Default value is <b>false</b>.
    /// 
    /// Set to <b>true</b> to make HTML, MHTML, EPUB, WordML, RTF, DOCX and ODT output human readable.
    /// Useful for testing or debugging.
    ASPOSE_WORDS_SHARED_API bool get_PrettyFormat() const;
    /// Setter for Aspose::Words::Saving::SaveOptions::get_PrettyFormat
    ASPOSE_WORDS_SHARED_API void set_PrettyFormat(bool value);
    /// Gets or sets a value determining whether or not to use anti-aliasing for rendering.
    /// 
    /// The default value is <c>false</c>. When this value is set to <c>true</c> anti-aliasing is
    /// used for rendering.
    /// 
    /// This property is used when the document is exported to the following formats:
    /// <see cref="Aspose::Words::SaveFormat::Tiff">Tiff</see>, <see cref="Aspose::Words::SaveFormat::Png">Png</see>, <see cref="Aspose::Words::SaveFormat::Bmp">Bmp</see>,
    /// <see cref="Aspose::Words::SaveFormat::Jpeg">Jpeg</see>, <see cref="Aspose::Words::SaveFormat::Emf">Emf</see>. When the document is exported to the
    /// <see cref="Aspose::Words::SaveFormat::Html">Html</see>, <see cref="Aspose::Words::SaveFormat::Mhtml">Mhtml</see> and <see cref="Aspose::Words::SaveFormat::Epub">Epub</see>
    /// formats this option is used for raster images.
    ASPOSE_WORDS_SHARED_API bool get_UseAntiAliasing() const;
    /// Setter for Aspose::Words::Saving::SaveOptions::get_UseAntiAliasing
    ASPOSE_WORDS_SHARED_API void set_UseAntiAliasing(bool value);
    /// Gets or sets a value determining whether or not to use high quality (i.e. slow) rendering algorithms.
    /// 
    /// The default value is <c>false</c>.
    /// 
    /// This property is used when the document is exported to image formats:
    /// <see cref="Aspose::Words::SaveFormat::Tiff">Tiff</see>, <see cref="Aspose::Words::SaveFormat::Png">Png</see>, <see cref="Aspose::Words::SaveFormat::Bmp">Bmp</see>,
    /// <see cref="Aspose::Words::SaveFormat::Jpeg">Jpeg</see>, <see cref="Aspose::Words::SaveFormat::Emf">Emf</see>.
    ASPOSE_WORDS_SHARED_API bool get_UseHighQualityRendering() const;
    /// Setter for Aspose::Words::Saving::SaveOptions::get_UseHighQualityRendering
    ASPOSE_WORDS_SHARED_API void set_UseHighQualityRendering(bool value);
    /// Gets value determining whether content of <see cref="Aspose::Words::Markup::StructuredDocumentTag">StructuredDocumentTag</see> is updated before saving.
    ASPOSE_WORDS_SHARED_API bool get_UpdateSdtContent() const;
    /// Sets value determining whether content of <see cref="Aspose::Words::Markup::StructuredDocumentTag">StructuredDocumentTag</see> is updated before saving.
    ASPOSE_WORDS_SHARED_API void set_UpdateSdtContent(bool value);
    /// Gets or sets a value determining how DrawingML shapes are rendered.
    /// 
    /// The default value is <see cref="Aspose::Words::Saving::DmlRenderingMode::Fallback">Fallback</see>.
    /// 
    /// This property is used when the document is exported to fixed page formats.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::DmlRenderingMode get_DmlRenderingMode() const;
    /// Setter for Aspose::Words::Saving::SaveOptions::get_DmlRenderingMode
    ASPOSE_WORDS_SHARED_API void set_DmlRenderingMode(Aspose::Words::Saving::DmlRenderingMode value);
    /// Gets or sets a value determining how DrawingML effects are rendered.
    /// 
    /// The default value is <see cref="Aspose::Words::Saving::DmlEffectsRenderingMode::Simplified">Simplified</see>.
    /// 
    /// This property is used when the document is exported to fixed page formats.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::DmlEffectsRenderingMode get_DmlEffectsRenderingMode();
    /// Setter for Aspose::Words::Saving::SaveOptions::get_DmlEffectsRenderingMode
    virtual ASPOSE_WORDS_SHARED_API void set_DmlEffectsRenderingMode(Aspose::Words::Saving::DmlEffectsRenderingMode value);
    /// Gets path to default template (including filename).
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_DefaultTemplate() const;
    /// Sets path to default template (including filename).
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_DefaultTemplate(System::String value);
    /// Gets a value determining if fields of certain types should be updated before saving the document to a fixed page format.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_UpdateFields() const;
    /// Sets a value determining if fields of certain types should be updated before saving the document to a fixed page format.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_UpdateFields(bool value);
    /// Gets a value determining whether the <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_LastSavedTime">LastSavedTime</see> property is updated before saving.
    ASPOSE_WORDS_SHARED_API bool get_UpdateLastSavedTimeProperty() const;
    /// Sets a value determining whether the <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_LastSavedTime">LastSavedTime</see> property is updated before saving.
    ASPOSE_WORDS_SHARED_API void set_UpdateLastSavedTimeProperty(bool value);
    /// Gets a value determining whether the <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_LastPrinted">LastPrinted</see> property is updated before saving.
    ASPOSE_WORDS_SHARED_API bool get_UpdateLastPrintedProperty() const;
    /// Sets a value determining whether the <see cref="Aspose::Words::Properties::BuiltInDocumentProperties::get_LastPrinted">LastPrinted</see> property is updated before saving.
    ASPOSE_WORDS_SHARED_API void set_UpdateLastPrintedProperty(bool value);
    /// Gets value determining if memory optimization should be performed before saving the document.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_MemoryOptimization() const;
    /// Sets value determining if memory optimization should be performed before saving the document.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_MemoryOptimization(bool value);
    /// Gets a value determining how 3D effects are rendered.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::Dml3DEffectsRenderingMode get_Dml3DEffectsRenderingMode() const;
    /// Sets a value determining how 3D effects are rendered.
    ASPOSE_WORDS_SHARED_API void set_Dml3DEffectsRenderingMode(Aspose::Words::Saving::Dml3DEffectsRenderingMode value);

    /// Creates a save options object of a class suitable for the specified save format.
    /// 
    /// @param saveFormat The save format for which to create a save options object.
    /// 
    /// @return An object of a class that derives from <see cref="Aspose::Words::Saving::SaveOptions">SaveOptions</see>.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOptions> CreateSaveOptions(Aspose::Words::SaveFormat saveFormat);
    /// Creates a save options object of a class suitable for the file extension specified in the given file name.
    /// 
    /// @param fileName The extension of this file name determines the class of the save options object to create.
    /// 
    /// @return An object of a class that derives from <see cref="Aspose::Words::Saving::SaveOptions">SaveOptions</see>.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::SaveOptions> CreateSaveOptions(System::String fileName);

protected:

    bool get_IsTestMode() const;
    void set_IsTestMode(bool value);
    virtual ASPOSE_WORDS_SHARED_API bool get_IsMultipleMainPartsAllowed();
    virtual ASPOSE_WORDS_SHARED_API bool get_IsFlowFormat();
    bool get_ExportGeneratorName() const;
    void set_ExportGeneratorName(bool value);
    bool get_WriteRsidTable() const;
    void set_WriteRsidTable(bool value);
    bool get_SetBuiltInThemeIfNull() const;
    void set_SetBuiltInThemeIfNull(bool value);

    ASPOSE_WORDS_SHARED_API SaveOptions();

    virtual ASPOSE_WORDS_SHARED_API void SetTestMode();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mSetBuiltInThemeIfNull;
    bool mMemoryOptimization;
    bool mIsTestMode;
    bool mExportGeneratorName;
    bool mPrettyFormat;
    System::String mTempFolder;
    bool mUseAntiAliasing;
    bool mUseHighQualityRendering;
    bool mUpdateSdtContent;
    Aspose::Words::Saving::DmlRenderingMode mDmlRenderingMode;
    Aspose::Words::Saving::DmlEffectsRenderingMode mDmlEffectsRenderingMode;
    System::String mDefaultTemplate;
    bool mUpdateFields;
    bool mWriteRsidTable;
    bool mUpdateLastSavedTimeProperty;
    bool mUpdateLastPrintedProperty;
    Aspose::Words::Saving::Dml3DEffectsRenderingMode mDml3DEffectsRenderingMode;

};

}
}
}
