//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PclSaveOptions.h
#pragma once

#include <system/string.h>

#include "Aspose.Words.Cpp/Model/Saving/FixedPageSaveOptions.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"

namespace Aspose { namespace Words { namespace RW { namespace Pcl { namespace Writer { class PclWriter; } } } } }
namespace Aspose { namespace Collections { class StringToStringDictionary; } }
namespace Aspose { namespace Rendering { namespace Pcl { class PclOptionsCore; } } }
namespace Aspose { namespace Words { class Document; } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Can be used to specify additional options when saving a document into the <see cref="Aspose::Words::SaveFormat::Pcl">Pcl</see> format.
class ASPOSE_WORDS_SHARED_CLASS PclSaveOptions : public Aspose::Words::Saving::FixedPageSaveOptions
{
    typedef PclSaveOptions ThisType;
    typedef Aspose::Words::Saving::FixedPageSaveOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Pcl::Writer::PclWriter;

public:

    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can only be <see cref="Aspose::Words::SaveFormat::Pcl">Pcl</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat get_SaveFormat() override;
    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can only be <see cref="Aspose::Words::SaveFormat::Pcl">Pcl</see>.
    ASPOSE_WORDS_SHARED_API void set_SaveFormat(Aspose::Words::SaveFormat value) override;
    /// Gets a value determining whether or not complex transformed elements
    /// should be rasterized before saving to PCL document.
    /// Default is <c>true</c>.
    ASPOSE_WORDS_SHARED_API bool get_RasterizeTransformedElements() const;
    /// Sets a value determining whether or not complex transformed elements
    /// should be rasterized before saving to PCL document.
    /// Default is <c>true</c>.
    ASPOSE_WORDS_SHARED_API void set_RasterizeTransformedElements(bool value);
    /// Name of the font that will be used
    /// if no expected font is found in printer and built-in fonts collections.
    ASPOSE_WORDS_SHARED_API System::String get_FallbackFontName() const;
    /// Name of the font that will be used
    /// if no expected font is found in printer and built-in fonts collections.
    ASPOSE_WORDS_SHARED_API void set_FallbackFontName(System::String value);

    /// Adds information about font that is uploaded to the printer by manufacturer.
    /// 
    /// @param fontFullName Full name of the font (e.g. "Times New Roman Bold Italic").
    /// @param fontPclName Name of the font that is used in Pcl document.
    ASPOSE_WORDS_SHARED_API void AddPrinterFont(System::String fontFullName, System::String fontPclName);

    ASPOSE_WORDS_SHARED_API PclSaveOptions();

protected:

    System::SharedPtr<Aspose::Rendering::Pcl::PclOptionsCore> ToCore(System::SharedPtr<Aspose::Words::Document> document);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mRasterizeTransformedElements;
    System::SharedPtr<Aspose::Collections::StringToStringDictionary> mPrinterFonts;
    System::String mFallbackFontName;

};

}
}
}
