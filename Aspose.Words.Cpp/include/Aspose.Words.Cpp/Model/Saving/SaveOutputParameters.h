//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/SaveOutputParameters.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlFixed { namespace Writer { class HtmlFixedWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Pcl { namespace Writer { class PclWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Ps { namespace Writer { class PsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Epub { namespace Writer { class EpubWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Imaging { namespace Writer { class ImagingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Pdf { namespace DirectWriter { class PdfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Svg { namespace Writer { class SvgWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Mhtml { namespace Writer { class MhtmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace XamlFixed { namespace Writer { class XamlFixedWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xaml { namespace Writer { class XamlWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Xps { namespace Writer { class XpsWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// This object is returned to the caller after a document is saved and contains additional information that
/// has been generated or calculated during the save operation. The caller can use or ignore this object.
class ASPOSE_WORDS_SHARED_CLASS SaveOutputParameters : public System::Object
{
    typedef SaveOutputParameters ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriterBase;
    friend class Aspose::Words::RW::HtmlFixed::Writer::HtmlFixedWriter;
    friend class Aspose::Words::RW::Html::Writer::HtmlWriter;
    friend class Aspose::Words::RW::Pcl::Writer::PclWriter;
    friend class Aspose::Words::RW::Ps::Writer::PsWriter;
    friend class Aspose::Words::RW::Epub::Writer::EpubWriter;
    friend class Aspose::Words::RW::Imaging::Writer::ImagingWriter;
    friend class Aspose::Words::RW::Pdf::DirectWriter::PdfWriter;
    friend class Aspose::Words::RW::Svg::Writer::SvgWriter;
    friend class Aspose::Words::RW::Docx::Writer::DocxWriter;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Mhtml::Writer::MhtmlWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::RW::XamlFixed::Writer::XamlFixedWriter;
    friend class Aspose::Words::RW::Xaml::Writer::XamlWriter;
    friend class Aspose::Words::RW::Xps::Writer::XpsWriter;

public:

    /// Returns the Content-Type string (Internet Media Type) that identifies the type of the saved document.
    ASPOSE_WORDS_SHARED_API System::String get_ContentType() const;

protected:

    SaveOutputParameters(System::String contentType);

    void SetContentType(System::String contentType);

private:

    System::String mContentType;

};

}
}
}
