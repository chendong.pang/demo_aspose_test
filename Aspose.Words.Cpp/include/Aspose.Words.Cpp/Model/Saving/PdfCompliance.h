//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PdfCompliance.h
#pragma once

#include <system/object_ext.h>
#include <system/enum.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Saving {

/// Specifies the PDF standards compliance level.
enum class PdfCompliance
{
    /// The output file will comply with the PDF 1.7 standard.
    Pdf17,
    /// The output file will comply with the PDF 1.5 standard.
    Pdf15,
    /// The output file will comply with the PDF/A-1a standard.
    /// This level includes all the requirements of PDF/A-1b and additionally requires
    /// that document structure be included (also known as being "tagged"),
    /// with the objective of ensuring that document content can be searched and repurposed.
    PdfA1a,
    /// The output file will comply with the PDF/A-1b standard.
    /// PDF/A-1b has the objective of ensuring reliable reproduction of the
    /// visual appearance of the document.
    PdfA1b
};

}
}
}

template<>
struct EnumMetaInfo<Aspose::Words::Saving::PdfCompliance>
{
    static const ASPOSE_WORDS_SHARED_API std::array<std::pair<Aspose::Words::Saving::PdfCompliance, const char_t*>, 4>& values();
};
