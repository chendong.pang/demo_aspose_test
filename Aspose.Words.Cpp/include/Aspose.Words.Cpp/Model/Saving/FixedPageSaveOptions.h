//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/FixedPageSaveOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Saving/SaveOptions.h"
#include "Aspose.Words.Cpp/Model/Saving/NumeralFormat.h"
#include "Aspose.Words.Cpp/Model/Saving/ColorMode.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Saving { class PageSavingCallbackCore; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayout; } } } }
namespace Aspose { namespace Words { namespace Layout { class LayoutOptionsCore; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace RW { class FixedPageWriterBase; } } }
namespace Aspose { namespace Words { namespace RW { namespace Imaging { namespace Writer { class ImagingWriter; } } } } }
namespace Aspose { namespace Words { namespace Saving { class PageSet; } } }
namespace Aspose { namespace Words { namespace Saving { class IPageSavingCallback; } } }
namespace Aspose { namespace Words { namespace Saving { class MetafileRenderingOptions; } } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Contains common options that can be specified when saving a document into fixed page formats (PDF, XPS,
/// images etc).
class ASPOSE_WORDS_SHARED_CLASS FixedPageSaveOptions : public Aspose::Words::Saving::SaveOptions
{
    typedef FixedPageSaveOptions ThisType;
    typedef Aspose::Words::Saving::SaveOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Saving::PageSavingCallbackCore;
    friend class Aspose::Words::Layout::Core::DocumentLayout;
    friend class Aspose::Words::Layout::LayoutOptionsCore;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::RW::FixedPageWriterBase;
    friend class Aspose::Words::RW::Imaging::Writer::ImagingWriter;

public:

    /// Gets the pages to render.
    /// Default is all the pages in the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::PageSet> get_PageSet() const;
    /// Sets the pages to render.
    /// Default is all the pages in the document.
    ASPOSE_WORDS_SHARED_API void set_PageSet(System::SharedPtr<Aspose::Words::Saving::PageSet> value);
    /// Gets the 0-based index of the first page to save.
    /// Default is 0.
    ASPOSE_WORDS_SHARED_API int32_t get_PageIndex() const;
    /// Sets the 0-based index of the first page to save.
    /// Default is 0.
    ASPOSE_WORDS_SHARED_API void set_PageIndex(int32_t value);
    /// Gets the number of pages to save.
    /// Default is <see cref="System::Int32::MaxValue">MaxValue</see> which means all pages of the document will be rendered.
    ASPOSE_WORDS_SHARED_API int32_t get_PageCount() const;
    /// Sets the number of pages to save.
    /// Default is <see cref="System::Int32::MaxValue">MaxValue</see> which means all pages of the document will be rendered.
    ASPOSE_WORDS_SHARED_API void set_PageCount(int32_t value);
    /// Allows to control how separate pages are saved when a document is exported to fixed page format.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::IPageSavingCallback> get_PageSavingCallback() const;
    /// Allows to control how separate pages are saved when a document is exported to fixed page format.
    ASPOSE_WORDS_SHARED_API void set_PageSavingCallback(System::SharedPtr<Aspose::Words::Saving::IPageSavingCallback> value);
    /// Gets <see cref="Aspose::Words::Saving::NumeralFormat">NumeralFormat</see> used for rendering of numerals.
    /// European numerals are used by default.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::NumeralFormat get_NumeralFormat() const;
    /// Sets <see cref="Aspose::Words::Saving::NumeralFormat">NumeralFormat</see> used for rendering of numerals.
    /// European numerals are used by default.
    ASPOSE_WORDS_SHARED_API void set_NumeralFormat(Aspose::Words::Saving::NumeralFormat value);
    /// Allows to specify metafile rendering options.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Saving::MetafileRenderingOptions> get_MetafileRenderingOptions() const;
    /// Allows to specify metafile rendering options.
    ASPOSE_WORDS_SHARED_API void set_MetafileRenderingOptions(System::SharedPtr<Aspose::Words::Saving::MetafileRenderingOptions> value);
    /// Gets or sets a value determining the quality of the JPEG images inside Html document.
    /// 
    /// Has effect only when a document contains JPEG images.
    /// 
    /// Use this property to get or set the quality of the images inside a document when saving in fixed page format.
    /// The value may vary from 0 to 100 where 0 means worst quality but maximum compression and 100
    /// means best quality but minimum compression.
    /// 
    /// The default value is 95.
    ASPOSE_WORDS_SHARED_API int32_t get_JpegQuality() const;
    /// Setter for Aspose::Words::Saving::FixedPageSaveOptions::get_JpegQuality
    ASPOSE_WORDS_SHARED_API void set_JpegQuality(int32_t value);
    /// Gets a value determining how colors are rendered.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::ColorMode get_ColorMode() const;
    /// Sets a value determining how colors are rendered.
    ASPOSE_WORDS_SHARED_API void set_ColorMode(Aspose::Words::Saving::ColorMode value);
    /// Flag indicates whether it is required to optimize output.
    /// If this flag is set redundant nested canvases and empty canvases are removed,
    /// also neighbor glyphs with the same formatting are concatenated.
    /// Note: The accuracy of the content display may be affected if this property is set to true.
    /// Default is false.
    virtual ASPOSE_WORDS_SHARED_API bool get_OptimizeOutput();
    /// Setter for Aspose::Words::Saving::FixedPageSaveOptions::get_OptimizeOutput
    virtual ASPOSE_WORDS_SHARED_API void set_OptimizeOutput(bool value);

    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;

protected:

    ASPOSE_WORDS_SHARED_API bool get_IsFlowFormat() override;
    System::SharedPtr<System::IO::Stream> get_LayoutExportStream() const;
    void set_LayoutExportStream(System::SharedPtr<System::IO::Stream> value);
    virtual ASPOSE_WORDS_SHARED_API bool get_CreateOutlinesForHeadingsInTables();

    ASPOSE_WORDS_SHARED_API FixedPageSaveOptions();

    System::SharedPtr<Aspose::Words::Saving::IPageSavingCallback> GetIPageSavingCallback();
    Aspose::Words::Saving::NumeralFormat GetNumeralFormatInternal();
    void UpdatePageSet();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::IO::Stream> mLayoutExportStream;
    int32_t mPageCount;
    int32_t mPageIndex;
    System::SharedPtr<Aspose::Words::Saving::IPageSavingCallback> mPageSavingCallback;
    Aspose::Words::Saving::NumeralFormat mNumeralFormat;
    System::SharedPtr<Aspose::Words::Saving::MetafileRenderingOptions> mMetafileRenderingOptions;
    int32_t mJpegQuality;
    Aspose::Words::Saving::ColorMode mColorMode;
    bool mOptimizeOutput;
    System::SharedPtr<Aspose::Words::Saving::PageSet> mPageSet;
    bool mIsPageSetUpdateFromIndexAndCountNeeded;

};

}
}
}
