//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/PsSaveOptions.h
#pragma once

#include "Aspose.Words.Cpp/Model/Saving/FixedPageSaveOptions.h"
#include "Aspose.Words.Cpp/Model/Document/SaveFormat.h"

namespace Aspose { namespace Words { namespace RW { namespace Ps { namespace Writer { class PsWriter; } } } } }
namespace Aspose { namespace Rendering { namespace Ps { class PsOptionsCore; } } }
namespace Aspose { namespace Words { class Document; } }

namespace Aspose {

namespace Words {

namespace Saving {

/// Can be used to specify additional options when saving a document into the <see cref="Aspose::Words::SaveFormat::Ps">Ps</see> format.
class ASPOSE_WORDS_SHARED_CLASS PsSaveOptions : public Aspose::Words::Saving::FixedPageSaveOptions
{
    typedef PsSaveOptions ThisType;
    typedef Aspose::Words::Saving::FixedPageSaveOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Ps::Writer::PsWriter;

public:

    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can only be <see cref="Aspose::Words::SaveFormat::Ps">Ps</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::SaveFormat get_SaveFormat() override;
    /// Specifies the format in which the document will be saved if this save options object is used.
    /// Can only be <see cref="Aspose::Words::SaveFormat::Ps">Ps</see>.
    ASPOSE_WORDS_SHARED_API void set_SaveFormat(Aspose::Words::SaveFormat value) override;
    /// Gets or sets a boolean value indicating whether the document should be saved using a booklet printing layout,
    /// if it is specified via <see cref="Aspose::Words::PageSetup::get_MultiplePages">MultiplePages</see>.
    /// 
    /// If this option is specified, <see cref="Aspose::Words::Saving::FixedPageSaveOptions::get_PageSet">PageSet</see>, <see cref="Aspose::Words::Saving::FixedPageSaveOptions::get_PageIndex">PageIndex</see>
    /// and <see cref="Aspose::Words::Saving::FixedPageSaveOptions::get_PageCount">PageCount</see> are ignored when saving.
    /// This behavior matches MS Word.
    /// If book fold printing settings are not specified in page setup, this option will have no effect.
    ASPOSE_WORDS_SHARED_API bool get_UseBookFoldPrintingSettings() const;
    /// Setter for Aspose::Words::Saving::PsSaveOptions::get_UseBookFoldPrintingSettings
    ASPOSE_WORDS_SHARED_API void set_UseBookFoldPrintingSettings(bool value);

    ASPOSE_WORDS_SHARED_API PsSaveOptions();

protected:

    System::SharedPtr<Aspose::Rendering::Ps::PsOptionsCore> ToCore(System::SharedPtr<Aspose::Words::Document> doc);

private:

    bool mUseBookFoldPrintingSettings;

};

}
}
}
