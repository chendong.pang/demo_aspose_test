//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Saving/TxtSaveOptionsBase.h
#pragma once

#include <system/text/encoding.h>
#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Saving/TxtExportHeadersFootersMode.h"
#include "Aspose.Words.Cpp/Model/Saving/SaveOptions.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Saving {

/// The base class for specifying additional options when saving a document into a text based formats.
class ASPOSE_WORDS_SHARED_CLASS TxtSaveOptionsBase : public Aspose::Words::Saving::SaveOptions
{
    typedef TxtSaveOptionsBase ThisType;
    typedef Aspose::Words::Saving::SaveOptions BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Specifies the encoding to use when exporting in text formats.
    /// Default value is <b>Encoding.UTF8</b>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Text::Encoding> get_Encoding() const;
    /// Specifies the encoding to use when exporting in text formats.
    /// Default value is <b>Encoding.UTF8</b>.
    ASPOSE_WORDS_SHARED_API void set_Encoding(System::SharedPtr<System::Text::Encoding> value);
    /// Specifies the string to use as a paragraph break when exporting in text formats.
    /// 
    /// The default value is <see cref="Aspose::Words::ControlChar::CrLf">CrLf</see>.
    ASPOSE_WORDS_SHARED_API System::String get_ParagraphBreak() const;
    /// Specifies the string to use as a paragraph break when exporting in text formats.
    /// 
    /// The default value is <see cref="Aspose::Words::ControlChar::CrLf">CrLf</see>.
    ASPOSE_WORDS_SHARED_API void set_ParagraphBreak(System::String value);
    /// Allows to specify whether the page breaks should be preserved during export.
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ForcePageBreaks() const;
    /// Allows to specify whether the page breaks should be preserved during export.
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ForcePageBreaks(bool value);
    /// Specifies the way headers and footers are exported to the text formats.
    /// Default value is <see cref="Aspose::Words::Saving::TxtExportHeadersFootersMode::PrimaryOnly">PrimaryOnly</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Saving::TxtExportHeadersFootersMode get_ExportHeadersFootersMode() const;
    /// Specifies the way headers and footers are exported to the text formats.
    /// Default value is <see cref="Aspose::Words::Saving::TxtExportHeadersFootersMode::PrimaryOnly">PrimaryOnly</see>.
    ASPOSE_WORDS_SHARED_API void set_ExportHeadersFootersMode(Aspose::Words::Saving::TxtExportHeadersFootersMode value);

    ASPOSE_WORDS_SHARED_API TxtSaveOptionsBase();

protected:

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Text::Encoding> mEncoding;
    System::String mParagraphBreak;
    bool mForcePageBreaks;
    Aspose::Words::Saving::TxtExportHeadersFootersMode mExportHeadersFootersMode;

};

}
}
}
