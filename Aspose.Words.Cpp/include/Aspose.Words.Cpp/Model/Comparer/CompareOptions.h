//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Comparer/CompareOptions.h
#pragma once

#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Comparer/Granularity.h"
#include "Aspose.Words.Cpp/Model/Comparer/ComparisonTargetType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Comparison { class FormattingComparer; } } }

namespace Aspose {

namespace Words {

/// Allows to choose advanced options for document comparison operation.
class ASPOSE_WORDS_SHARED_CLASS CompareOptions : public System::Object
{
    typedef CompareOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Comparison::FormattingComparer;

public:

    /// True indicates that documents comparison is case insensitive.
    /// By default comparison is case sensitive.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreCaseChanges() const;
    /// True indicates that documents comparison is case insensitive.
    /// By default comparison is case sensitive.
    ASPOSE_WORDS_SHARED_API void set_IgnoreCaseChanges(bool value);
    /// Specifies whether to compare the differences in data contained in tables.
    /// By default tables are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreTables() const;
    /// Specifies whether to compare the differences in data contained in tables.
    /// By default tables are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreTables(bool value);
    /// Specifies whether to compare differences in fields.
    /// By default fields are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreFields() const;
    /// Specifies whether to compare differences in fields.
    /// By default fields are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreFields(bool value);
    /// Specifies whether to compare differences in footnotes and endnotes.
    /// By default footnotes are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreFootnotes() const;
    /// Specifies whether to compare differences in footnotes and endnotes.
    /// By default footnotes are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreFootnotes(bool value);
    /// Specifies whether to compare differences in comments.
    /// By default comments are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreComments() const;
    /// Specifies whether to compare differences in comments.
    /// By default comments are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreComments(bool value);
    /// Specifies whether to compare differences in the data contained within text boxes.
    /// By default textboxes are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreTextboxes() const;
    /// Specifies whether to compare differences in the data contained within text boxes.
    /// By default textboxes are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreTextboxes(bool value);
    /// True indicates that formatting is ignored.
    /// By default document formatting is not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreFormatting() const;
    /// True indicates that formatting is ignored.
    /// By default document formatting is not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreFormatting(bool value);
    /// True indicates that headers and footers content is ignored.
    /// By default headers and footers are not ignored.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreHeadersAndFooters() const;
    /// True indicates that headers and footers content is ignored.
    /// By default headers and footers are not ignored.
    ASPOSE_WORDS_SHARED_API void set_IgnoreHeadersAndFooters(bool value);
    /// Specifies which document shall be used as a target during comparison.
    ASPOSE_WORDS_SHARED_API Aspose::Words::ComparisonTargetType get_Target() const;
    /// Specifies which document shall be used as a target during comparison.
    ASPOSE_WORDS_SHARED_API void set_Target(Aspose::Words::ComparisonTargetType value);
    /// Specifies whether changes are tracked by character or by word.
    /// Default value is <see cref="Aspose::Words::Granularity::WordLevel">WordLevel</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Granularity get_Granularity() const;
    /// Specifies whether changes are tracked by character or by word.
    /// Default value is <see cref="Aspose::Words::Granularity::WordLevel">WordLevel</see>.
    ASPOSE_WORDS_SHARED_API void set_Granularity(Aspose::Words::Granularity value);
    /// Specifies whether to ignore difference in DrawingML unique Id.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreDmlUniqueId() const;
    /// Specifies whether to ignore difference in DrawingML unique Id.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_IgnoreDmlUniqueId(bool value);

    ASPOSE_WORDS_SHARED_API CompareOptions();

protected:

    bool get_UseOriginalFormatting();
    bool get_UseFinalFormatting();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mIgnoreComments;
    bool mIgnoreTextboxes;
    bool mIgnoreFootnotes;
    bool mIgnoreFields;
    bool mIgnoreTables;
    bool mIgnoreCaseChanges;
    bool mIgnoreFormatting;
    bool mIgnoreHeadersAndFooters;
    Aspose::Words::ComparisonTargetType mTargetType;
    Aspose::Words::Granularity mGranularity;
    bool mIgnoreDmlUniqueId;

};

}
}
