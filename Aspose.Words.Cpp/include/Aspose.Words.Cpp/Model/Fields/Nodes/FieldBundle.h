//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Nodes/FieldBundle.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/exceptions.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"

namespace Aspose { namespace Words { namespace Fields { class FieldChar; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// \cond
class FieldBundle : public System::Object
{
    typedef FieldBundle ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:

    System::SharedPtr<Aspose::Words::Fields::FieldChar> get_FieldCodeEnd();
    Aspose::Words::Fields::FieldType get_FieldType();
    System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() const;
    void set_Start(System::SharedPtr<Aspose::Words::Fields::FieldStart> value);
    System::SharedPtr<Aspose::Words::Fields::FieldSeparator> get_Separator() const;
    void set_Separator(System::SharedPtr<Aspose::Words::Fields::FieldSeparator> value);
    bool get_HasSeparator();
    System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() const;
    void set_End(System::SharedPtr<Aspose::Words::Fields::FieldEnd> value);
    bool get_IsLocked();
    bool get_IsDirty();

    FieldBundle(System::SharedPtr<Aspose::Words::Fields::FieldStart> start, System::SharedPtr<Aspose::Words::Fields::FieldSeparator> separator, System::SharedPtr<Aspose::Words::Fields::FieldEnd> end);

    System::String GetFieldCode();
    System::String GetFieldCode(bool includeChildFieldCodes);
    void SetFieldType(Aspose::Words::Fields::FieldType fieldType);
    Aspose::Words::Fields::FieldType ParseFieldType();
    void NormalizeFieldTypes();
    void RemoveFieldNodes();
    void LockField(bool isLocked);
    void DirtyField(bool isDirty);
    void UpdateDirtyLocked();
    static Aspose::Words::Fields::FieldBundle GetFieldBundle(System::SharedPtr<Aspose::Words::Fields::FieldChar> fieldChar);
    static Aspose::Words::Fields::FieldBundle GetFieldBundleNoSeparatorCheck(System::SharedPtr<Aspose::Words::Fields::FieldChar> fieldChar);
    static Aspose::Words::Fields::FieldBundle FillFieldBundleOneWay(Aspose::Words::Fields::FieldBundle fieldBundle, System::SharedPtr<Aspose::Words::Fields::FieldChar> fieldChar, bool isForward);

    FieldBundle();

protected:

    System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldSeparator> pr_Separator;
    System::SharedPtr<Aspose::Words::Fields::FieldEnd> pr_End;
    System::WeakPtr<Aspose::Words::Fields::FieldStart> mStart;

    System::String GetFieldCodeLimited(bool allowHiddenText);
    static void ValidateFieldChar(System::SharedPtr<Aspose::Words::Fields::FieldChar> fieldChar, System::SharedPtr<Aspose::Words::Fields::FieldChar> referenceFieldChar);
    static System::InvalidOperationException CreateInvalidModelException();

};/// \endcond

/// \cond
class FieldNestingLevel : public System::Object
{
    typedef FieldNestingLevel ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:

    bool get_IsTop();

    FieldNestingLevel(bool isForward, bool startFromTop);

    bool AcceptFieldStart();
    bool AcceptFieldEnd();

private:

    int32_t get_TopValue();

    bool mIsForward;
    int32_t mValue;

};/// \endcond

}
}
}
