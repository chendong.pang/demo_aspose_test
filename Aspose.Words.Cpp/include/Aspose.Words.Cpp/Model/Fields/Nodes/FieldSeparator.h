//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Nodes/FieldSeparator.h
#pragma once

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Fields/Nodes/FieldChar.h"

namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEndFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { class ShapeFieldRemover; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldSimpleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { class MiscRwUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class OleObject; } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldType; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents a Word field separator that separates the field code from the field result.
/// 
/// <see cref="Aspose::Words::Fields::FieldSeparator">FieldSeparator</see> is an inline-level node and represented
/// by the <see cref="Aspose::Words::ControlChar::FieldSeparatorChar">FieldSeparatorChar</see> control character in the document.
/// 
/// <see cref="Aspose::Words::Fields::FieldSeparator">FieldSeparator</see> can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
/// 
/// A complete field in a Microsoft Word document is a complex structure consisting of
/// a field start character, field code, field separator character, field result
/// and field end character. Some fields only have field start, field code and field end.
/// 
/// To easily insert a new field into a document, use the <see cref="Aspose::Words::DocumentBuilder::InsertField(System::String)">InsertField()</see>
/// method.
class ASPOSE_WORDS_SHARED_CLASS FieldSeparator : public Aspose::Words::Fields::FieldChar
{
    typedef FieldSeparator ThisType;
    typedef Aspose::Words::Fields::FieldChar BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::Fields::FieldAppender;
    friend class Aspose::Words::Fields::FieldEndFieldBuildingBlock;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::ShapeFieldRemover;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldSimpleReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::MiscRwUtil;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlHyperlinkReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxHyperlinkReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;

public:

    /// Returns <see cref="Aspose::Words::NodeType::FieldSeparator">FieldSeparator</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitFieldSeparator(System::SharedPtr<Aspose::Words::Fields::FieldSeparator>)">VisitFieldSeparator()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return <b>False</b> if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> get_OleObject() const;

    FieldSeparator(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr, Aspose::Words::Fields::FieldType type);
    FieldSeparator(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr, Aspose::Words::Fields::FieldType type, System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> oleObject);

    virtual ASPOSE_WORDS_SHARED_API ~FieldSeparator();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> pr_OleObject;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
