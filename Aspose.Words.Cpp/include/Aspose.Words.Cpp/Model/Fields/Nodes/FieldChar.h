//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Nodes/FieldChar.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Text/SpecialChar.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class PageLayout; } } } }
namespace Aspose { namespace Words { class PageExtractor; } }
namespace Aspose { namespace Words { class FieldRemoverRetainCertainFieldsFilter; } }
namespace Aspose { namespace Words { class FieldRemoverRetainFwrFieldsFilter; } }
namespace Aspose { namespace Words { namespace Fields { class FieldExtractor; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlParagraphArranger; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownHyperlinkWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class EmbeddedFontCollector; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUnknown; } } }
namespace Aspose { namespace Words { namespace Fields { class FakeResultHelper; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFakeResultAppender; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlImageWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtTrackedChangesWriter; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFactory; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFieldFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { class ShapeFieldRemover; } }
namespace Aspose { namespace Words { class NodeTextCollector; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanField; } } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Fields { class NodeRangeFieldCodeTokenizer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBundle; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Tables { class TableFormattingExpander; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class FieldValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlFieldWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageFieldWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFieldWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Txt { namespace Writer { class TxtWriter; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Base class for nodes that represent field characters in a document.
/// 
/// A complete field in a Microsoft Word document is a complex structure consisting of
/// a field start character, field code, field separator character, field result
/// and field end character. Some fields only have field start, field code and field end.
/// 
/// To easily insert a new field into a document, use the <see cref="Aspose::Words::DocumentBuilder::InsertField(System::String)">InsertField()</see>
/// method.
/// 
/// @sa Aspose::Words::Fields::FieldStart
/// @sa Aspose::Words::Fields::FieldSeparator
/// @sa Aspose::Words::Fields::FieldEnd
class ASPOSE_WORDS_SHARED_CLASS FieldChar : public Aspose::Words::SpecialChar
{
    typedef FieldChar ThisType;
    typedef Aspose::Words::SpecialChar BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Layout::PreAps::PageLayout;
    friend class Aspose::Words::PageExtractor;
    friend class Aspose::Words::FieldRemoverRetainCertainFieldsFilter;
    friend class Aspose::Words::FieldRemoverRetainFwrFieldsFilter;
    friend class Aspose::Words::Fields::FieldExtractor;
    friend class Aspose::Words::RW::Docx::Writer::DocxFieldsWriter;
    friend class Aspose::Words::RW::Nrx::Writer::NrxFieldsWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlParagraphArranger;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownHyperlinkWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlFieldsWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::Fonts::EmbeddedFontCollector;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::Fields::FieldUnknown;
    friend class Aspose::Words::Fields::FakeResultHelper;
    friend class Aspose::Words::Fields::FieldFakeResultAppender;
    friend class Aspose::Words::RW::Html::Writer::HtmlImageWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtTrackedChangesWriter;
    friend class Aspose::Words::Fields::FieldFactory;
    friend class Aspose::Words::Fields::MergeFieldFinder;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::Fields::TocEntryExtractor;
    friend class Aspose::Words::Fields::FieldUtil;
    friend class Aspose::Words::ShapeFieldRemover;
    friend class Aspose::Words::NodeTextCollector;
    friend class Aspose::Words::Layout::Core::SpanField;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Fields::NodeRangeFieldCodeTokenizer;
    friend class Aspose::Words::Fields::FieldBundle;
    friend class Aspose::Words::Fields::FieldEnd;
    friend class Aspose::Words::Fields::FieldSeparator;
    friend class Aspose::Words::Fields::FieldStart;
    friend class Aspose::Words::Tables::TableFormattingExpander;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::FieldValidator;
    friend class Aspose::Words::RW::Html::Writer::HtmlFieldWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageFieldWriterBase;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;
    friend class Aspose::Words::RW::Odt::Writer::OdtFieldWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Odt::OdtUtil;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Txt::Writer::TxtWriter;

public:

    /// Returns the type of the field.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldType get_FieldType() const;
    /// Gets whether the parent field is locked (should not recalculate its result).
    ASPOSE_WORDS_SHARED_API bool get_IsLocked() const;
    /// Sets whether the parent field is locked (should not recalculate its result).
    ASPOSE_WORDS_SHARED_API void set_IsLocked(bool value);
    /// Gets whether the current result of the field is no longer correct (stale) due to other modifications
    /// made to the document.
    ASPOSE_WORDS_SHARED_API bool get_IsDirty() const;
    /// Sets whether the current result of the field is no longer correct (stale) due to other modifications
    /// made to the document.
    ASPOSE_WORDS_SHARED_API void set_IsDirty(bool value);

    /// Returns a field for the field char.
    /// 
    /// @return A field for the field char.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> GetField();

protected:

    ASPOSE_WORDS_SHARED_API void set_FieldType(Aspose::Words::Fields::FieldType value);
    bool get_IsPrivate() const;
    void set_IsPrivate(bool value);

    FieldChar(System::SharedPtr<Aspose::Words::DocumentBase> doc, char16_t fieldChar, System::SharedPtr<Aspose::Words::RunPr> runPr, Aspose::Words::Fields::FieldType type);

    virtual ASPOSE_WORDS_SHARED_API ~FieldChar();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Fields::FieldType pr_FieldType;
    bool pr_IsLocked;
    bool pr_IsDirty;
    bool pr_IsPrivate;

};

}
}
}
