//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Nodes/FieldEnd.h
#pragma once

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Fields/Nodes/FieldChar.h"

namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEndFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class FieldValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldSimpleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { class MiscRwUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextIndexesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldType; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents an end of a Word field in a document.
/// 
/// <see cref="Aspose::Words::Fields::FieldEnd">FieldEnd</see> is an inline-level node and represented
/// by the <see cref="Aspose::Words::ControlChar::FieldEndChar">FieldEndChar</see> control character in the document.
/// 
/// <see cref="Aspose::Words::Fields::FieldEnd">FieldEnd</see> can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
/// 
/// A complete field in a Microsoft Word document is a complex structure consisting of
/// a field start character, field code, field separator character, field result
/// and field end character. Some fields only have field start, field code and field end.
/// 
/// To easily insert a new field into a document, use the <see cref="Aspose::Words::DocumentBuilder::InsertField(System::String)">InsertField()</see>
/// method.
class ASPOSE_WORDS_SHARED_CLASS FieldEnd : public Aspose::Words::Fields::FieldChar
{
    typedef FieldEnd ThisType;
    typedef Aspose::Words::Fields::FieldChar BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::Fields::FieldAppender;
    friend class Aspose::Words::Fields::FieldEndFieldBuildingBlock;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::FieldValidator;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldSimpleReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::MiscRwUtil;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextIndexesReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Wml::Reader::WmlHyperlinkReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxHyperlinkReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;

public:

    /// Returns <see cref="Aspose::Words::NodeType::FieldEnd">FieldEnd</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns <b>true</b> if this field has a separator.
    ASPOSE_WORDS_SHARED_API bool get_HasSeparator() const;

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitFieldEnd(System::SharedPtr<Aspose::Words::Fields::FieldEnd>)">VisitFieldEnd()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return <b>False</b> if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    FieldEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr, Aspose::Words::Fields::FieldType type, bool hasSeparator);

    void SetHasSeparator(bool value);

    virtual ASPOSE_WORDS_SHARED_API ~FieldEnd();

private:

    bool pr_HasSeparator;

    ASPOSE_WORDS_SHARED_API void set_HasSeparator(bool value);

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
