//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Nodes/FieldStart.h
#pragma once

#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Fields/Nodes/FieldChar.h"

namespace Aspose { namespace Words { namespace Comparison { class FieldComparer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldCharReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStartFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFakeResultAppender; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormDropDown; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormText; } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxFldSimpleReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { class MiscRwUtil; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtFieldWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxHyperlinkReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldType; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents a start of a Word field in a document.
/// 
/// <see cref="Aspose::Words::Fields::FieldStart">FieldStart</see> is an inline-level node and represented by the
/// <see cref="Aspose::Words::ControlChar::FieldStartChar">FieldStartChar</see> control character in the document.
/// 
/// <see cref="Aspose::Words::Fields::FieldStart">FieldStart</see> can only be a child of <see cref="Aspose::Words::Paragraph">Paragraph</see>.
/// 
/// A complete field in a Microsoft Word document is a complex structure consisting of
/// a field start character, field code, field separator character, field result
/// and field end character. Some fields only have field start, field code and field end.
/// 
/// To easily insert a new field into a document, use the <see cref="Aspose::Words::DocumentBuilder::InsertField(System::String)">InsertField()</see>
/// method.
class ASPOSE_WORDS_SHARED_CLASS FieldStart : public Aspose::Words::Fields::FieldChar
{
    typedef FieldStart ThisType;
    typedef Aspose::Words::Fields::FieldChar BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comparison::FieldComparer;
    friend class Aspose::Words::RW::Docx::Writer::DocxFieldsWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlFieldsWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldCharReaderBase;
    friend class Aspose::Words::Fields::FieldAppender;
    friend class Aspose::Words::Fields::FieldStartFieldBuildingBlock;
    friend class Aspose::Words::Fields::FieldFakeResultAppender;
    friend class Aspose::Words::Fields::FieldFormDropDown;
    friend class Aspose::Words::Fields::FieldUtil;
    friend class Aspose::Words::Fields::FieldFormText;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxFldSimpleReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::MiscRwUtil;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtFieldWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlHyperlinkReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxHyperlinkReader;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;

public:

    /// Returns <see cref="Aspose::Words::NodeType::FieldStart">FieldStart</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitFieldStart(System::SharedPtr<Aspose::Words::Fields::FieldStart>)">VisitFieldStart()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return <b>False</b> if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    System::SharedPtr<Aspose::Words::Fields::FormField> get_FormField();
    System::ArrayPtr<uint8_t> get_FieldData() const;
    void set_FieldData(System::ArrayPtr<uint8_t> value);

    FieldStart(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::SharedPtr<Aspose::Words::RunPr> runPr, Aspose::Words::Fields::FieldType type);

    virtual ASPOSE_WORDS_SHARED_API ~FieldStart();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::ArrayPtr<uint8_t> pr_FieldData;

    System::SharedPtr<Aspose::Words::Fields::FormField> FindFormField();
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
