//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/FieldOptions.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/globalization/culture_info.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldUpdateCultureSource.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/Index/FieldIndexFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldToa; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUserAddress; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUserInitials; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUserName; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class IFieldUpdateCultureProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldUserPromptRespondent; } } }
namespace Aspose { namespace Words { namespace Fields { class IBarcodeGenerator; } } }
namespace Aspose { namespace Words { namespace Fields { class UserInformation; } } }
namespace Aspose { namespace Words { namespace Fields { class ToaCategories; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldResultFormatter; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents options to control field handling in a document.
class ASPOSE_WORDS_SHARED_CLASS FieldOptions : public System::Object
{
    typedef FieldOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldToa;
    friend class Aspose::Words::Fields::FieldUserAddress;
    friend class Aspose::Words::Fields::FieldUserInitials;
    friend class Aspose::Words::Fields::FieldUserName;
    friend class Aspose::Words::Document;

public:

    /// Specifies what culture to use to format the field result.
    /// 
    /// By default, the culture of the current thread is used.
    /// 
    /// The setting affects only date/time fields with \\\\\@ format switch.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateCultureSource get_FieldUpdateCultureSource() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_FieldUpdateCultureSource
    ASPOSE_WORDS_SHARED_API void set_FieldUpdateCultureSource(Aspose::Words::Fields::FieldUpdateCultureSource value);
    /// Gets or sets a provider that returns a culture object specific for each particular field.
    /// 
    /// The provider is requested when the value of <see cref="Aspose::Words::Fields::FieldOptions::get_FieldUpdateCultureSource">FieldUpdateCultureSource</see> is <b>FieldUpdateCultureSource.FieldCode</b>.
    /// 
    /// If the provider is present, then the culture object it returns is used for the field update. Otherwise, a system culture is used.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::IFieldUpdateCultureProvider> get_FieldUpdateCultureProvider() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_FieldUpdateCultureProvider
    ASPOSE_WORDS_SHARED_API void set_FieldUpdateCultureProvider(System::SharedPtr<Aspose::Words::Fields::IFieldUpdateCultureProvider> value);
    /// Gets or sets the value indicating whether bidirectional text is fully supported during field update or not.
    /// 
    /// When this property is set to <b>true</b>, additional steps are performed to produce Right-To-Left language
    /// (i.e. Arabic or Hebrew) compatible field result during its update.
    /// 
    /// When this property is set to <b>false</b> and Right-To-Left language is used, correctness of field result
    /// after its update is not guaranteed.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_IsBidiTextSupportedOnUpdate() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_IsBidiTextSupportedOnUpdate
    ASPOSE_WORDS_SHARED_API void set_IsBidiTextSupportedOnUpdate(bool value);
    /// Gets or sets the respondent to user prompts during field update.
    /// 
    /// If the value of this property is set to <b>null</b>, the fields that require user response on prompting
    /// (such as ASK or FILLIN) are not updated.
    /// 
    /// The default value is <b>null</b>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::IFieldUserPromptRespondent> get_UserPromptRespondent() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_UserPromptRespondent
    ASPOSE_WORDS_SHARED_API void set_UserPromptRespondent(System::SharedPtr<Aspose::Words::Fields::IFieldUserPromptRespondent> value);
    /// Gets default document author's name. If author's name is already specified in built-in document properties,
    /// this option is not considered.
    ASPOSE_WORDS_SHARED_API System::String get_DefaultDocumentAuthor() const;
    /// Sets default document author's name. If author's name is already specified in built-in document properties,
    /// this option is not considered.
    ASPOSE_WORDS_SHARED_API void set_DefaultDocumentAuthor(System::String value);
    /// Gets custom style separator for the \\t switch in TOC field.
    ASPOSE_WORDS_SHARED_API System::String get_CustomTocStyleSeparator() const;
    /// Sets custom style separator for the \\t switch in TOC field.
    ASPOSE_WORDS_SHARED_API void set_CustomTocStyleSeparator(System::String value);
    /// Gets or sets the value indicating whether legacy (early than AW 13.10) number format for fields is enabled or not.
    /// 
    /// When this property is set to <b>true</b>, template symbol "#" worked as in .net:
    /// Replaces the pound sign with the corresponding digit if one is present; otherwise, no symbols appears in the result string.
    /// 
    /// When this property is set to <b>false</b>, template symbol "#" works as MS Word:
    /// This format item specifies the requisite numeric places to display in the result.
    /// If the result does not include a digit in that place, MS Word displays a space. For example, { = 9 + 6 \\\# \$\#\#\# } displays \$ 15.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_LegacyNumberFormat() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_LegacyNumberFormat
    ASPOSE_WORDS_SHARED_API void set_LegacyNumberFormat(bool value);
    /// Gets or sets the value indicating that number format is parsed using invariant culture or not
    /// 
    /// When this property is set to <b>true</b>, number format is taken from an invariant culture.
    /// 
    /// When this property is set to <b>false</b>, number format is taken from the current thread's culture.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_UseInvariantCultureNumberFormat() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_UseInvariantCultureNumberFormat
    ASPOSE_WORDS_SHARED_API void set_UseInvariantCultureNumberFormat(bool value);
    /// Gets or set custom barcode generator.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::IBarcodeGenerator> get_BarcodeGenerator() const;
    /// Gets or set custom barcode generator.
    ASPOSE_WORDS_SHARED_API void set_BarcodeGenerator(System::SharedPtr<Aspose::Words::Fields::IBarcodeGenerator> value);
    /// Gets or sets the culture to preprocess field values.
    /// 
    /// Currently this property only affects value of the DOCPROPERTY field.
    /// 
    /// The default value is <b>null</b>. When this property is set to <b>null</b>, the DOCPROPERTY field's value is preprocessed
    /// with the culture controlled by the <see cref="Aspose::Words::Fields::FieldOptions::get_FieldUpdateCultureSource">FieldUpdateCultureSource</see> property.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Globalization::CultureInfo> get_PreProcessCulture() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_PreProcessCulture
    ASPOSE_WORDS_SHARED_API void set_PreProcessCulture(System::SharedPtr<System::Globalization::CultureInfo> value);
    /// Gets the current user information.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::UserInformation> get_CurrentUser() const;
    /// Sets the current user information.
    ASPOSE_WORDS_SHARED_API void set_CurrentUser(System::SharedPtr<Aspose::Words::Fields::UserInformation> value);
    /// Gets the table of authorities categories.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::ToaCategories> get_ToaCategories() const;
    /// Sets the table of authorities categories.
    ASPOSE_WORDS_SHARED_API void set_ToaCategories(System::SharedPtr<Aspose::Words::Fields::ToaCategories> value);
    /// Gets a <see cref="Aspose::Words::Fields::FieldOptions::get_FieldIndexFormat">FieldIndexFormat</see> that represents
    /// the formatting for the <see cref="Aspose::Words::Fields::FieldIndex">FieldIndex</see> fields in the document.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldIndexFormat get_FieldIndexFormat();
    /// Sets a <see cref="Aspose::Words::Fields::FieldOptions::get_FieldIndexFormat">FieldIndexFormat</see> that represents
    /// the formatting for the <see cref="Aspose::Words::Fields::FieldIndex">FieldIndex</see> fields in the document.
    ASPOSE_WORDS_SHARED_API void set_FieldIndexFormat(Aspose::Words::Fields::FieldIndexFormat value);
    /// Gets the file name of the document.
    /// 
    /// This property is used by the FILENAME field with higher priority than the <see cref="Aspose::Words::Document::get_OriginalFileName">OriginalFileName</see> property.
    ASPOSE_WORDS_SHARED_API System::String get_FileName() const;
    /// Sets the file name of the document.
    /// 
    /// This property is used by the FILENAME field with higher priority than the <see cref="Aspose::Words::Document::get_OriginalFileName">OriginalFileName</see> property.
    ASPOSE_WORDS_SHARED_API void set_FileName(System::String value);
    /// Allows to control how the field result is formatted.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatter> get_ResultFormatter() const;
    /// Allows to control how the field result is formatted.
    ASPOSE_WORDS_SHARED_API void set_ResultFormatter(System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatter> value);
    /// Gets or sets paths of MS Word built-in templates.
    /// 
    /// This property is used by the AUTOTEXT and GLOSSARY fields, if referenced auto text entry is not found in the <see cref="Aspose::Words::Document::get_AttachedTemplate">AttachedTemplate</see> template.
    /// 
    /// By defalut MS Word stores built-in templates in c:\\Users\\\<username\>\\AppData\\Roaming\\Microsoft\\Document Building Blocks\\1033\\16\\Built-In Building Blocks.dotx and
    /// C:\\Users\\\<username\>\\AppData\\Roaming\\Microsoft\\Templates\\Normal.dotm files.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::String> get_BuiltInTemplatesPaths() const;
    /// Setter for Aspose::Words::Fields::FieldOptions::get_BuiltInTemplatesPaths
    ASPOSE_WORDS_SHARED_API void set_BuiltInTemplatesPaths(System::ArrayPtr<System::String> value);

protected:

    System::SharedPtr<Aspose::Words::Fields::UserInformation> get_EffectiveUser();
    System::SharedPtr<Aspose::Words::Fields::ToaCategories> get_EffectiveToaCategories();

    FieldOptions(System::SharedPtr<Aspose::Words::Document> document);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Fields::FieldUpdateCultureSource pr_FieldUpdateCultureSource;
    System::SharedPtr<Aspose::Words::Fields::IFieldUpdateCultureProvider> pr_FieldUpdateCultureProvider;
    bool pr_IsBidiTextSupportedOnUpdate;
    System::SharedPtr<Aspose::Words::Fields::IFieldUserPromptRespondent> pr_UserPromptRespondent;
    System::String pr_DefaultDocumentAuthor;
    System::String pr_CustomTocStyleSeparator;
    bool pr_LegacyNumberFormat;
    bool pr_UseInvariantCultureNumberFormat;
    System::SharedPtr<Aspose::Words::Fields::IBarcodeGenerator> pr_BarcodeGenerator;
    System::SharedPtr<System::Globalization::CultureInfo> pr_PreProcessCulture;
    System::SharedPtr<Aspose::Words::Fields::UserInformation> pr_CurrentUser;
    System::SharedPtr<Aspose::Words::Fields::ToaCategories> pr_ToaCategories;
    System::String pr_FileName;
    System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatter> pr_ResultFormatter;
    System::ArrayPtr<System::String> mBuiltInTemplatesPaths;
    System::SharedPtr<Aspose::Words::Document> mDocument;

};

}
}
}
