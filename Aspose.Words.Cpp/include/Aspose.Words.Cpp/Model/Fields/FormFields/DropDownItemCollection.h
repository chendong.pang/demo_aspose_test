//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/FormFields/DropDownItemCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Formatting/IComplexAttr.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlControlAsFormFieldReader; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FormFieldPr; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFieldReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class FfDataFiler; } } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// A collection of strings that represent all the items in a drop-down form field.
/// 
/// @sa Aspose::Words::Fields::FormField
/// @sa Aspose::Words::Fields::FormField::get_DropDownItems
class ASPOSE_WORDS_SHARED_CLASS DropDownItemCollection : public System::Collections::Generic::IEnumerable<System::String>, public Aspose::Words::IComplexAttr
{
    typedef DropDownItemCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::String> BaseType;
    typedef Aspose::Words::IComplexAttr BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Html::Reader::HtmlControlAsFormFieldReader;
    friend class Aspose::Words::Fields::FormFieldPr;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::RW::Odt::Reader::OdtFieldReader;
    friend class Aspose::Words::RW::Doc::FfDataFiler;

public:

    /// Gets the number of elements contained in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    ASPOSE_WORDS_SHARED_API bool get_IsInheritedComplexAttr() override;

    /// Gets or sets the element at the specified index.
    ASPOSE_WORDS_SHARED_API System::String idx_get(int32_t index);
    /// Gets or sets the element at the specified index.
    ASPOSE_WORDS_SHARED_API void idx_set(int32_t index, System::String value);

    /// Returns an enumerator object that can be used to iterate over all items in the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::String>> GetEnumerator() override;
    /// Adds a string to the end of the collection.
    /// 
    /// @param value The string to add to the end of the collection.
    /// 
    /// @return The zero-based index at which the new element is inserted.
    ASPOSE_WORDS_SHARED_API int32_t Add(System::String value);
    /// Determines whether the collection contains the specified value.
    /// 
    /// @param value Case-sensitive value to locate.
    /// 
    /// @return True if the item is found in the collection; otherwise, false.
    ASPOSE_WORDS_SHARED_API bool Contains(System::String value);
    /// Returns the zero-based index of the specified value in the collection.
    /// 
    /// @param value The case-sensitive value to locate.
    /// 
    /// @return The zero based index. Negative value if not found.
    ASPOSE_WORDS_SHARED_API int32_t IndexOf(System::String value);
    /// Inserts a string into the collection at the specified index.
    /// 
    /// @param index The zero-based index at which value is inserted.
    /// @param value The string to insert.
    ASPOSE_WORDS_SHARED_API void Insert(int32_t index, System::String value);
    /// Removes the specified value from the collection.
    /// 
    /// @param name The case-sensitive value to remove.
    ASPOSE_WORDS_SHARED_API void Remove(System::String name);
    /// Removes a value at the specified index.
    /// 
    /// @param index The zero based index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all elements from the collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::IComplexAttr> DeepCloneComplexAttr() override;

protected:

    static const int32_t MaxItemsCount;

    DropDownItemCollection();

    System::SharedPtr<Aspose::Words::Fields::DropDownItemCollection> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~DropDownItemCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::String>> mItems;

};

}
}
}
