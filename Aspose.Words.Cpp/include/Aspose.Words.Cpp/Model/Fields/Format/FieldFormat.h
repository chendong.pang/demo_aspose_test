//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Format/FieldFormat.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Lists/ListLabelUtil.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class GeneralFormatCollection; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionApplyResult; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionFormatResult; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldResultApplier; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { class RichString; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormattingResult; } } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { enum class ChapterPageSeparator; } }
namespace Aspose { namespace Words { namespace Fields { enum class GeneralFormat; } } }
namespace Aspose { enum class CharCase; }
namespace Aspose { namespace Words { namespace Fields { class IFieldResultFormatter; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldResultFormatProvider; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Provides typed access to field's numeric, date and time, and general formatting.
class ASPOSE_WORDS_SHARED_CLASS FieldFormat : public System::Object
{
    typedef FieldFormat ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::Fields::FieldUtil;
    friend class Aspose::Words::Fields::GeneralFormatCollection;
    friend class Aspose::Words::Fields::FieldUpdateActionApplyResult;
    friend class Aspose::Words::Fields::FieldUpdateActionFormatResult;
    friend class Aspose::Words::Fields::FieldResultApplier;
    friend class Aspose::Words::DocumentBuilder;

private:

    class FieldFormatListLabelBuildBehaviour : public Aspose::Words::Lists::IListLabelBuildBehaviour
    {
        typedef FieldFormatListLabelBuildBehaviour ThisType;
        typedef Aspose::Words::Lists::IListLabelBuildBehaviour BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        void NotifyListNumberAppended(int32_t listLabelLength) override;
        bool ShouldAppendNotListNumberChar(char16_t c) override;
        int32_t FinilazeListLabelLength(int32_t listLabelLength) override;

        FieldFormatListLabelBuildBehaviour();

    private:

        int32_t mListLabelLengthAtLastListNumber;

    };

public:

    /// Gets a formatting that is applied to a numeric field result. Corresponds to the \\\# switch.
    ASPOSE_WORDS_SHARED_API System::String get_NumericFormat();
    /// Sets a formatting that is applied to a numeric field result. Corresponds to the \\\# switch.
    ASPOSE_WORDS_SHARED_API void set_NumericFormat(System::String value);
    /// Gets a formatting that is applied to a date and time field result. Corresponds to the \\\@ switch.
    ASPOSE_WORDS_SHARED_API System::String get_DateTimeFormat();
    /// Sets a formatting that is applied to a date and time field result. Corresponds to the \\\@ switch.
    ASPOSE_WORDS_SHARED_API void set_DateTimeFormat(System::String value);
    /// Gets a collection of general formats that are applied to a numeric, text or any field result.
    /// Corresponds to the \\* switches.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::GeneralFormatCollection> get_GeneralFormats();

protected:

    System::SharedPtr<Aspose::Words::Fields::RichString> get_RichNumericFormat();

    static const System::String& NumericFormatSwitch();
    static const System::String& DateTimeFormatSwitch();
    static const System::String& GeneralFormatSwitch();

    FieldFormat(System::SharedPtr<Aspose::Words::Fields::Field> field);

    bool FormatResult(System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> value, System::SharedPtr<Aspose::Words::Fields::FieldFormattingResult>& formattingResult);
    System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatProvider> GetFieldResultFormatProvider();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldCode> get_FieldCode();

    System::WeakPtr<Aspose::Words::Fields::Field> mField;
    System::SharedPtr<Aspose::Words::Fields::GeneralFormatCollection> mGeneralFormats;

    bool TryFormatDateTime(System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> value, System::SharedPtr<Aspose::Words::Fields::RichString>& result);
    System::SharedPtr<Aspose::Words::Fields::RichString> TryApplyNumericFormatToDateTime(System::SharedPtr<Aspose::Words::Fields::RichString> value);
    bool TryFormatNumber(System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> value, System::SharedPtr<Aspose::Words::Fields::RichString>& result, bool& preserveRichFormatting);
    bool TryConvertNumber(System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> value, System::SharedPtr<Aspose::Words::Fields::RichString>& result);
    bool TryFormatPageNumber(System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> value, System::SharedPtr<Aspose::Words::Fields::RichString>& result);
    System::String GetChapterNumber(System::SharedPtr<Aspose::Words::Section> formatSection);
    static char16_t GetChapterPageSeparatorChar(Aspose::Words::ChapterPageSeparator separator);
    bool TryFormatString(System::SharedPtr<Aspose::Words::Fields::RichString> value, System::SharedPtr<Aspose::Words::Fields::RichString>& result);
    static System::SharedPtr<Aspose::Words::Fields::RichString> FormatString(System::SharedPtr<Aspose::Words::Fields::RichString> value, Aspose::Words::Fields::GeneralFormat format, Aspose::CharCase charCase, System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatter> resultFormatter);

};

}
}
}
