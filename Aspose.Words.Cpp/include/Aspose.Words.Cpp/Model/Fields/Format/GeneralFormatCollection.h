//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Format/GeneralFormatCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ienumerator_ng.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Format/GeneralFormat.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldPageRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeq; } } }
namespace Aspose { namespace Words { namespace Fields { class CharFormatProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormat; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSwitch; } } }
namespace Aspose { template<typename> class EnumeratorWrapperPalGeneric; }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents a typed collection of general formats.
class ASPOSE_WORDS_SHARED_CLASS GeneralFormatCollection : public System::Collections::Generic::IEnumerable<Aspose::Words::Fields::GeneralFormat>
{
    typedef GeneralFormatCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<Aspose::Words::Fields::GeneralFormat> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::Field;
    friend class Aspose::Words::Fields::FieldPageRef;
    friend class Aspose::Words::Fields::FieldSeq;
    friend class Aspose::Words::Fields::CharFormatProvider;
    friend class Aspose::Words::Fields::FieldUpdater;
    friend class Aspose::Words::Fields::FieldFormat;

private:

    class GeneralFormatEnumerator FINAL : public System::Collections::Generic::IEnumerator<Aspose::Words::Fields::GeneralFormat>
    {
        typedef GeneralFormatEnumerator ThisType;
        typedef System::Collections::Generic::IEnumerator<Aspose::Words::Fields::GeneralFormat> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        Aspose::Words::Fields::GeneralFormat get_Current() const override;

        GeneralFormatEnumerator(System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Fields::FieldSwitch>>> switchEnumerator);

        void Dispose() override;
        bool MoveNext() override;
        void Reset() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::EnumeratorWrapperPalGeneric<System::SharedPtr<Aspose::Words::Fields::FieldSwitch>>> mSwitchEnumerator;

    };

public:

    /// Gets the total number of the items in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Adds a general format to the collection.
    /// 
    /// @param item A general format.
    ASPOSE_WORDS_SHARED_API void Add(Aspose::Words::Fields::GeneralFormat item);
    /// Removes all occurrences of the specified general format from the collection.
    /// 
    /// @param item A general format.
    ASPOSE_WORDS_SHARED_API void Remove(Aspose::Words::Fields::GeneralFormat item);
    /// Removes a general format occurrence at the specified index.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);

    /// Gets a general format at the specified index.
    /// 
    /// @param index The index of a general format.
    /// 
    /// @return A general format.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::GeneralFormat idx_get(int32_t index);

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<Aspose::Words::Fields::GeneralFormat>> GetEnumerator() override;

protected:

    GeneralFormatCollection(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode);

    bool HasFormat(Aspose::Words::Fields::GeneralFormat format);
    void AddOrRemove(Aspose::Words::Fields::GeneralFormat format, bool isAdd);
    Aspose::Words::Fields::GeneralFormat GetNumericFormat();
    System::String GetNumericFormatString();

    virtual ASPOSE_WORDS_SHARED_API ~GeneralFormatCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldCode> mFieldCode;

    static bool IsValidSwitch(System::SharedPtr<Aspose::Words::Fields::FieldSwitch> fieldSwitch);

};

}
}
}
