//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/FieldBuilder/FieldBuilder.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldBuilder/Blocks/IFieldBuildingBlock.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldStartFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEndFieldBuildingBlock; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldArgumentBuilder; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Builds a field from field code tokens (arguments and switches).
class ASPOSE_WORDS_SHARED_CLASS FieldBuilder : public Aspose::Words::Fields::IFieldBuildingBlock
{
    typedef FieldBuilder ThisType;
    typedef Aspose::Words::Fields::IFieldBuildingBlock BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Initializes an instance of the <see cref="Aspose::Words::Fields::FieldBuilder">FieldBuilder</see> class.
    /// 
    /// @param fieldType The type of the field to build.
    ASPOSE_WORDS_SHARED_API FieldBuilder(Aspose::Words::Fields::FieldType fieldType);

    /// Adds a field's argument.
    /// 
    /// @param argument The argument value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddArgument(System::String argument);
    /// Adds a field's argument.
    /// 
    /// @param argument The argument value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddArgument(int32_t argument);
    /// Adds a field's argument.
    /// 
    /// @param argument The argument value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddArgument(double argument);
    /// Adds a child field represented by another <see cref="Aspose::Words::Fields::FieldBuilder">FieldBuilder</see> to the field's code.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddArgument(System::SharedPtr<Aspose::Words::Fields::FieldBuilder> argument);
    /// Adds a field's argument represented by <see cref="Aspose::Words::Fields::FieldArgumentBuilder">FieldArgumentBuilder</see> to the field's code.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddArgument(System::SharedPtr<Aspose::Words::Fields::FieldArgumentBuilder> argument);
    /// Adds a field's switch.
    /// 
    /// @param switchName The switch name.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddSwitch(System::String switchName);
    /// Adds a field's switch.
    /// 
    /// @param switchName The switch name.
    /// @param switchArgument The switch value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddSwitch(System::String switchName, System::String switchArgument);
    /// Adds a field's switch.
    /// 
    /// @param switchName The switch name.
    /// @param switchArgument The switch value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddSwitch(System::String switchName, int32_t switchArgument);
    /// Adds a field's switch.
    /// 
    /// @param switchName The switch name.
    /// @param switchArgument The switch value.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldBuilder> AddSwitch(System::String switchName, double switchArgument);
    /// Builds and inserts a field into the document before the specified inline node.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the inserted field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> BuildAndInsert(System::SharedPtr<Aspose::Words::Inline> refNode);
    /// Builds and inserts a field into the document to the end of the specified paragraph.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::Field">Field</see> object that represents the inserted field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::Field> BuildAndInsert(System::SharedPtr<Aspose::Words::Paragraph> refNode);
    ASPOSE_WORDS_SHARED_API void BuildBlock(System::SharedPtr<Aspose::Words::DocumentBuilder> documentBuilder) override;

protected:

    virtual ASPOSE_WORDS_SHARED_API ~FieldBuilder();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Fields::IFieldBuildingBlock>>> mBuildingBlocks;
    System::SharedPtr<Aspose::Words::Fields::FieldStartFieldBuildingBlock> mStart;
    System::SharedPtr<Aspose::Words::Fields::FieldEndFieldBuildingBlock> mEnd;

    System::SharedPtr<Aspose::Words::Fields::Field> BuildAndInsertInternal(System::SharedPtr<Aspose::Words::Node> refNode);
    void AppendBuildingBlock(System::SharedPtr<Aspose::Words::Fields::IFieldBuildingBlock> buildingBlock);
    void AppendBuildingBlockWithDelimiter(System::SharedPtr<Aspose::Words::Fields::IFieldBuildingBlock> buildingBlock);
    void AppendBuildingBlockWithQuotesAndDelimiter(System::SharedPtr<Aspose::Words::Fields::IFieldBuildingBlock> buildingBlock);

};

}
}
}
