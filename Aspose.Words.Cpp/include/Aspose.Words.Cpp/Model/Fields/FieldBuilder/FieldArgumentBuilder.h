//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/FieldBuilder/FieldArgumentBuilder.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/list.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldBuilder/Blocks/IFieldBuildingBlock.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { namespace Fields { class FieldBuilder; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Builds a complex field argument consisting of fields, nodes, and plain text.
class ASPOSE_WORDS_SHARED_CLASS FieldArgumentBuilder : public Aspose::Words::Fields::IFieldBuildingBlock
{
    typedef FieldArgumentBuilder ThisType;
    typedef Aspose::Words::Fields::IFieldBuildingBlock BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Initializes an instance of the <see cref="Aspose::Words::Fields::FieldArgumentBuilder">FieldArgumentBuilder</see> class.
    ASPOSE_WORDS_SHARED_API FieldArgumentBuilder();

    /// Adds a plain text to the argument.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldArgumentBuilder> AddText(System::String text);
    /// Adds a node to the argument.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldArgumentBuilder> AddNode(System::SharedPtr<Aspose::Words::Inline> node);
    /// Adds a field represented by a <see cref="Aspose::Words::Fields::FieldBuilder">FieldBuilder</see> to the argument.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldArgumentBuilder> AddField(System::SharedPtr<Aspose::Words::Fields::FieldBuilder> fieldBuilder);
    ASPOSE_WORDS_SHARED_API void BuildBlock(System::SharedPtr<Aspose::Words::DocumentBuilder> documentBuilder) override;

protected:

    virtual ASPOSE_WORDS_SHARED_API ~FieldArgumentBuilder();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Fields::IFieldBuildingBlock>>> mBuildingBlocks;

};

}
}
}
