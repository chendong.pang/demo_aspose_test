//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Field.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Nodes/FieldBundle.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStrategy; } } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStage; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionInsertShape; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldAutoTextUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldQuoteUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludePictureUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { namespace Comparison { class ComparerNodeUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUnlinker; } } }
namespace Aspose { namespace Words { namespace Fields { class UnlinkFieldReplaceListener; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntriesBookmarkDeferredRemover; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBarcodeUtil; } } }
namespace Aspose { namespace Words { namespace Revisions { class RevisionUtil; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanFieldContext; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class OfficeMathToApsConverter; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldOldResultNodeCollection; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldReplacer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntryPageNumberInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUserPromptingUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFieldUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTextHelper; } } }
namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionSetBookmarkValue; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeDataSourceResettableDecorator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFactory; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldHyperlink; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIf; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldMergeField; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTC; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class StyleFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class CharFormatProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFormatProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionApplyResult; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionFormatResult; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionInsertErrorMessage; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionInsertImage; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanField; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeTC; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldArgument; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeEmbed; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeLink; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeParser; } } }
namespace Aspose { namespace Words { namespace Fields { class NodeRangeFieldCodeTokenizer; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldResultApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class TextResultApplier; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFormat; } } }
namespace Aspose { namespace Words { namespace Fields { class FormField; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateContext; } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageFieldWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Validation { class RubyConverter; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNumUtil; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { class Range; } }
namespace Aspose { namespace Words { namespace RW { class RunWriter; } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldChar; } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldArea; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldArgument; } } }
namespace Aspose { namespace Bidi { class IBidiParagraphLevelOverride; } }
namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { class Document; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents a Microsoft Word document field.
/// 
/// A field in a Word document is a complex structure consisting of multiple nodes that include field start,
/// field code, field separator, field result and field end. Fields can be nested, contain rich content and span
/// multiple paragraphs or sections in a document. The <see cref="Aspose::Words::Fields::Field">Field</see> class is a "facade" object that provides
/// properties and methods that allow to work with a field as a single object.
/// 
/// The <see cref="Aspose::Words::Fields::Field::get_Start">Start</see>, <see cref="Aspose::Words::Fields::Field::get_Separator">Separator</see> and <see cref="Aspose::Words::Fields::Field::get_End">End</see> properties point to the
/// field start, separator and end nodes of the field respectively.
/// 
/// The content between the field start and separator is the field code. The content between the
/// field separator and field end is the field result. The field code typically consists of one or more
/// <see cref="Aspose::Words::Run">Run</see> objects that specify instructions. The processing application is expected to execute
/// the field code to calculate the field result.
/// 
/// The process of calculating field results is called the field update. Aspose.Words can update field
/// results of most of the field types in exactly the same way as Microsoft Word does it. Most notably,
/// Aspose.Words can calculate results of even the most complex formula fields. To calculate the field
/// result of a single field use the <see cref="Aspose::Words::Fields::Field::Update">Update</see> method. To update fields in the whole document
/// use <see cref="Aspose::Words::Document::UpdateFields">UpdateFields</see>.
/// 
/// You can get the plain text version of the field code using the <see cref="Aspose::Words::Fields::Field::GetFieldCode(bool)">GetFieldCode()</see> method.
/// You can get and set the plain text version of the field result using the <see cref="Aspose::Words::Fields::Field::get_Result">Result</see> property.
/// Both the field code and field result can contain complex content, such as nested fields, paragraphs, shapes,
/// tables and in this case you might want to work with the field nodes directly if you need more control.
/// 
/// You do not create instances of the <see cref="Aspose::Words::Fields::Field">Field</see> class directly.
/// To create a new field use the <see cref="Aspose::Words::DocumentBuilder::InsertField(System::String)">InsertField()</see> method.
class ASPOSE_WORDS_SHARED_CLASS Field : public virtual System::Object
{
    typedef Field ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldUpdateActionInsertShape;
    friend class Aspose::Words::Fields::FieldUpdateAction;
    friend class Aspose::Words::Fields::FieldAutoTextUpdater;
    friend class Aspose::Words::Fields::FieldQuoteUpdater;
    friend class Aspose::Words::Fields::FieldIncludePictureUpdater;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::Comparison::ComparerNodeUtil;
    friend class Aspose::Words::Fields::FieldUnlinker;
    friend class Aspose::Words::Fields::UnlinkFieldReplaceListener;
    friend class Aspose::Words::Fields::TocEntriesBookmarkDeferredRemover;
    friend class Aspose::Words::MailMerging::MailMergeRegion;
    friend class Aspose::Words::Fields::FieldBarcodeUtil;
    friend class Aspose::Words::Revisions::RevisionUtil;
    friend class Aspose::Words::Layout::Core::SpanFieldContext;
    friend class Aspose::Words::ApsBuilder::Math::OfficeMathToApsConverter;
    friend class Aspose::Words::Fields::FieldOldResultNodeCollection;
    friend class Aspose::Words::Fields::FieldParagraphFinder;
    friend class Aspose::Words::Fields::FieldReplacer;
    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Fields::IndexEntryPageNumberInfo;
    friend class Aspose::Words::Fields::FieldUserPromptingUtil;
    friend class Aspose::Words::Fields::MergeFieldUtil;
    friend class Aspose::Words::Fields::FieldTextHelper;
    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::Fields::FieldUpdateActionSetBookmarkValue;
    friend class Aspose::Words::MailMerging::MailMergeDataSourceResettableDecorator;
    friend class Aspose::Words::Fields::FieldCodeUpdater;
    friend class Aspose::Words::Fields::FieldFactory;
    friend class Aspose::Words::Fields::FieldHyperlink;
    friend class Aspose::Words::Fields::FieldIf;
    friend class Aspose::Words::Fields::FieldMergeField;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldTC;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::StyleFinder;
    friend class Aspose::Words::Fields::TocEntryExtractor;
    friend class Aspose::Words::Fields::FieldUtil;
    friend class Aspose::Words::Fields::CharFormatProvider;
    friend class Aspose::Words::Fields::MergeFormatProvider;
    friend class Aspose::Words::Fields::FieldUpdateActionApplyResult;
    friend class Aspose::Words::Fields::FieldUpdateActionFormatResult;
    friend class Aspose::Words::Fields::FieldUpdateActionInsertErrorMessage;
    friend class Aspose::Words::Fields::FieldUpdateActionInsertImage;
    friend class Aspose::Words::Layout::Core::SpanField;
    friend class Aspose::Words::Fields::FieldCodeTC;
    friend class Aspose::Words::Fields::FieldUpdater;
    friend class Aspose::Words::Fields::FieldArgument;
    friend class Aspose::Words::Fields::FieldCode;
    friend class Aspose::Words::Fields::FieldCode;
    friend class Aspose::Words::Fields::FieldCodeEmbed;
    friend class Aspose::Words::Fields::FieldCodeLink;
    friend class Aspose::Words::Fields::FieldCodeParser;
    friend class Aspose::Words::Fields::NodeRangeFieldCodeTokenizer;
    friend class Aspose::Words::Fields::NodeRangeFieldCodeTokenizer;
    friend class Aspose::Words::Fields::FieldResultApplier;
    friend class Aspose::Words::Fields::TextResultApplier;
    friend class Aspose::Words::Fields::FieldFormat;
    friend class Aspose::Words::Fields::FormField;
    friend class Aspose::Words::Fields::FieldUpdateContext;
    friend class Aspose::Words::Fields::FieldUpdateContext;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageFieldWriterBase;
    friend class Aspose::Words::Validation::RubyConverter;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Fields::FieldNumUtil;
    friend class Aspose::Words::MailMerging::MailMerge;
    friend class Aspose::Words::Range;
    friend class Aspose::Words::RW::RunWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;

public:

    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() const;
    /// Gets the node that represents the field separator. Can be null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldSeparator> get_Separator();
    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() const;
    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_FieldStart() const;
    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_FieldEnd() const;
    /// Gets the Microsoft Word field type.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldType get_Type();
    /// Gets text that is between the field separator and field end.
    ASPOSE_WORDS_SHARED_API System::String get_Result();
    /// Sets text that is between the field separator and field end.
    ASPOSE_WORDS_SHARED_API void set_Result(System::String value);
    /// Gets the text that represents the displayed field result.
    ASPOSE_WORDS_SHARED_API System::String get_DisplayResult();
    /// Gets whether the field is locked (should not recalculate its result).
    ASPOSE_WORDS_SHARED_API bool get_IsLocked();
    /// Sets whether the field is locked (should not recalculate its result).
    ASPOSE_WORDS_SHARED_API void set_IsLocked(bool value);
    /// Gets whether the current result of the field is no longer correct (stale) due to other modifications made to the document.
    ASPOSE_WORDS_SHARED_API bool get_IsDirty();
    /// Sets whether the current result of the field is no longer correct (stale) due to other modifications made to the document.
    ASPOSE_WORDS_SHARED_API void set_IsDirty(bool value);
    /// Gets a <see cref="Aspose::Words::Fields::FieldFormat">FieldFormat</see> object that provides typed access to field's formatting.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldFormat> get_Format();
    /// Gets the LCID of the field.
    /// 
    /// @sa Aspose::Words::Fields::FieldUpdateCultureSource::FieldCode
    ASPOSE_WORDS_SHARED_API int32_t get_LocaleId();
    /// Sets the LCID of the field.
    /// 
    /// @sa Aspose::Words::Fields::FieldUpdateCultureSource::FieldCode
    ASPOSE_WORDS_SHARED_API void set_LocaleId(int32_t value);

    /// Returns text between field start and field separator (or field end if there is no separator).
    /// Both field code and field result of child fields are included.
    ASPOSE_WORDS_SHARED_API System::String GetFieldCode();
    /// Returns text between field start and field separator (or field end if there is no separator).
    /// 
    /// @param includeChildFieldCodes <c>True</c> if child field codes should be included.
    ASPOSE_WORDS_SHARED_API System::String GetFieldCode(bool includeChildFieldCodes);
    /// Removes the field from the document. Returns a node right after the field. If the field's end is the last child
    /// of its parent node, returns its parent paragraph. If the field is already removed, returns <b>null</b>.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Remove();
    /// Performs the field update. Throws if the field is being updated already.
    ASPOSE_WORDS_SHARED_API void Update();
    /// Performs a field update. Throws if the field is being updated already.
    /// 
    /// @param ignoreMergeFormat If <c>true</c> then direct field result formatting is abandoned, regardless of the MERGEFORMAT switch, otherwise normal update is performed.
    ASPOSE_WORDS_SHARED_API void Update(bool ignoreMergeFormat);
    /// Performs the field unlink.
    /// 
    /// Replaces the field with its most recent result.
    /// 
    /// Some fields, such as XE (Index Entry) fields and SEQ (Sequence) fields, cannot be unlinked.
    /// 
    /// @return <c>True</c> if the field has been unlinked, otherwise <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool Unlink();

protected:

    System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>>> get_OldResultNodes();
    System::SharedPtr<Aspose::Words::Paragraph> get_OldResultStartParagraph() const;
    System::SharedPtr<Aspose::Words::Paragraph> get_OldResultEndParagraph() const;
    virtual ASPOSE_WORDS_SHARED_API bool get_SupportsConditionalUpdate();
    bool get_IsInHeaderFooter();
    System::SharedPtr<Aspose::Words::Fields::FieldChar> get_FieldCodeEnd();
    System::SharedPtr<Aspose::Words::Fields::FieldCode> get_FieldCodeCache();
    bool get_HasFieldCodeCache();
    bool get_HasSeparator();
    bool get_IsRemoved();
    System::SharedPtr<Aspose::Words::Fields::FieldUpdater> get_Updater();
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateContext> get_UpdateContext() const;
    bool get_IsUpdating();
    System::SharedPtr<Aspose::Words::DocumentBase> get_Document();

    Field();
    Field(Aspose::Words::Fields::FieldBundle bundle);

    virtual ASPOSE_WORDS_SHARED_API void Initialize(System::SharedPtr<Aspose::Words::Fields::FieldStart> start, System::SharedPtr<Aspose::Words::Fields::FieldSeparator> separator, System::SharedPtr<Aspose::Words::Fields::FieldEnd> end);
    virtual ASPOSE_WORDS_SHARED_API void ParseFieldCode();
    void InvalidateFieldCodeCache();
    void EnsureFieldCodeCache();
    Aspose::Words::Fields::FieldType GetFieldTypeFromCode();
    void SetFieldCode(System::String fieldCode);
    void SetFieldType(Aspose::Words::Fields::FieldType fieldType);
    void NormalizeFieldTypes();
    void Remove(Aspose::Words::Fields::FieldArea area);
    void BeginUpdate(System::SharedPtr<Aspose::Words::Fields::FieldUpdateContext> context);
    virtual ASPOSE_WORDS_SHARED_API void EndUpdate();
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStrategy GetChildFieldsUpdateStrategyInArgument(System::SharedPtr<Aspose::Words::Fields::IFieldArgument> argument);
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStage GetUpdateStage();
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore();
    void UnlinkCore();
    virtual ASPOSE_WORDS_SHARED_API void BeforeUnlink();
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetFakeResult();
    System::SharedPtr<Aspose::Words::NodeRange> GetFieldRange();
    System::SharedPtr<Aspose::Words::NodeRange> GetRange(Aspose::Words::Fields::FieldArea area);
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Bidi::IBidiParagraphLevelOverride> GetBidiParagraphLevelOverride();
    void EnsureSeparator(bool force);
    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> GetPageNumberFormatSection();
    virtual ASPOSE_WORDS_SHARED_API System::String GetDefaultDateTimeFormat();
    System::SharedPtr<Aspose::Words::Document> FetchDocument();
    void StoreOldResultNodesIfNeeded();
    virtual ASPOSE_WORDS_SHARED_API bool NeedStoreOldResultNodes();
    void RemoveStoredOldResultNodes();
    System::String GetResult(bool isFieldResultMode);

    virtual ASPOSE_WORDS_SHARED_API ~Field();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsUnlinkable();

    System::SharedPtr<Aspose::Words::Paragraph> pr_OldResultStartParagraph;

    void set_OldResultStartParagraph(System::SharedPtr<Aspose::Words::Paragraph> value);

    System::SharedPtr<Aspose::Words::Paragraph> pr_OldResultEndParagraph;

    void set_OldResultEndParagraph(System::SharedPtr<Aspose::Words::Paragraph> value);

    Aspose::Words::Fields::FieldBundle mBundle;
    System::SharedPtr<Aspose::Words::Fields::FieldCode> mFieldCodeCache;
    System::SharedPtr<Aspose::Words::Fields::FieldFormat> mFieldFormatCache;
    System::WeakPtr<Aspose::Words::Fields::FieldUpdateContext> mUpdateContext;
    System::SharedPtr<Aspose::Words::Fields::FieldOldResultNodeCollection> mOldResultNodes;

    bool GetBidiEmbeddingLevel();
    bool GetPageAndNumPagesBidiEmbeddingLevelSource();
    static System::SharedPtr<Aspose::Words::Inline> FirstOrDefaultInlineNode(System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Node>>> nodes);

};

}
}
}
