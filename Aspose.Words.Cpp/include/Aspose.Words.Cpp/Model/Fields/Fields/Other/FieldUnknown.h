//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Other/FieldUnknown.h
#pragma once

#include <system/text/regularexpressions/regex.h>
#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/IMergeFieldSurrogate.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStage; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldChar; } } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldType; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements an unknown or unrecognized field.
class ASPOSE_WORDS_SHARED_CLASS FieldUnknown : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IMergeFieldSurrogate
{
    typedef FieldUnknown ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IMergeFieldSurrogate BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldUtil;

public:

    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() override;
    /// Gets the node that represents the field separator. Can be null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldSeparator> get_Separator() override;
    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() override;

    ASPOSE_WORDS_SHARED_API System::String GetMergeFieldName() override;
    ASPOSE_WORDS_SHARED_API bool CanWorkAsMergeField() override;
    ASPOSE_WORDS_SHARED_API bool IsMergeValueRequired() override;

protected:

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStage GetUpdateStage() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    bool IsBookmarkRef();
    static bool IsBookmarkRef(System::SharedPtr<Aspose::Words::Fields::FieldStart> fieldStart, System::SharedPtr<Aspose::Words::Fields::FieldChar> fieldCodeEnd);

    virtual ASPOSE_WORDS_SHARED_API ~FieldUnknown();

private:

    System::String get_BookmarkName();

    static System::SharedPtr<System::Text::RegularExpressions::Regex>& gBookmarkNameRegex();
    static bool IsBookmarkRef(System::String fieldCode, Aspose::Words::Fields::FieldType fieldType);

};

}
}
}
