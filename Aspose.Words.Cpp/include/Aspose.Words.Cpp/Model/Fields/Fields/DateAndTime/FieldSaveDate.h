//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DateAndTime/FieldSaveDate.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/Transitional/IFieldInfoResultProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class DateTimeConstant; } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldCode; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the SAVEDATE field.
class ASPOSE_WORDS_SHARED_CLASS FieldSaveDate : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldSaveDate ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldInfo;

private:

    class SaveDateInfoResultProvider : public Aspose::Words::Fields::IFieldInfoResultProvider
    {
        typedef SaveDateInfoResultProvider ThisType;
        typedef Aspose::Words::Fields::IFieldInfoResultProvider BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetResult(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode) override;

    };

public:

    /// Gets whether to use the Hijri Lunar or Hebrew Lunar calendar.
    ASPOSE_WORDS_SHARED_API bool get_UseLunarCalendar();
    /// Sets whether to use the Hijri Lunar or Hebrew Lunar calendar.
    ASPOSE_WORDS_SHARED_API void set_UseLunarCalendar(bool value);
    /// Gets whether to use the Saka Era calendar.
    ASPOSE_WORDS_SHARED_API bool get_UseSakaEraCalendar();
    /// Sets whether to use the Saka Era calendar.
    ASPOSE_WORDS_SHARED_API void set_UseSakaEraCalendar(bool value);
    /// Gets whether to use the Um-al-Qura calendar.
    ASPOSE_WORDS_SHARED_API bool get_UseUmAlQuraCalendar();
    /// Sets whether to use the Um-al-Qura calendar.
    ASPOSE_WORDS_SHARED_API void set_UseUmAlQuraCalendar(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    static System::SharedPtr<Aspose::Words::Fields::IFieldInfoResultProvider>& FieldInfoResultProvider();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldSaveDate();

private:

    static const System::String& UseLunarCalendarSwitch();
    static const System::String& UseSakaEraCalendarSwitch();
    static const System::String& UseUmAlQuraCalendarSwitch();
    static System::SharedPtr<Aspose::Words::Fields::Expressions::DateTimeConstant> GetResultInternal(System::SharedPtr<Aspose::Words::Document> document);

};

}
}
}
