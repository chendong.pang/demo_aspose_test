//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/FieldGreetingLine.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <mutex>
#include <memory>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/IFormattableMergeField.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class IMergeFormatContext; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFormatPlaceholder; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the GREETINGLINE field.
class ASPOSE_WORDS_SHARED_CLASS FieldGreetingLine : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider, public Aspose::Words::Fields::IFormattableMergeField
{
    typedef FieldGreetingLine ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;
    typedef Aspose::Words::Fields::IFormattableMergeField BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:

    /// Gets the text to include in the field if the name is blank.
    ASPOSE_WORDS_SHARED_API System::String get_AlternateText();
    /// Sets the text to include in the field if the name is blank.
    ASPOSE_WORDS_SHARED_API void set_AlternateText(System::String value);
    /// Gets the format of the name included in the field.
    ASPOSE_WORDS_SHARED_API System::String get_NameFormat();
    /// Sets the format of the name included in the field.
    ASPOSE_WORDS_SHARED_API void set_NameFormat(System::String value);
    /// Gets the language id used to format the name.
    ASPOSE_WORDS_SHARED_API System::String get_LanguageId();
    /// Sets the language id used to format the name.
    ASPOSE_WORDS_SHARED_API void set_LanguageId(System::String value);
    ASPOSE_WORDS_SHARED_API System::String get_MergeFormat() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::String>> get_PlaceholdersToFieldsMap() override;

    /// Returns a collection of mail merge field names used by the field.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<System::String> GetFieldNames() override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Document> IFormattableMergeField_FetchDocument() override;
    ASPOSE_WORDS_SHARED_API System::String GetPlaceholderValue(System::SharedPtr<Aspose::Words::Fields::IMergeFormatContext> context, System::SharedPtr<Aspose::Words::Fields::MergeFormatPlaceholder> placeholder) override;

    ASPOSE_WORDS_SHARED_API FieldGreetingLine();

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldGreetingLine();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    static System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::String>>& gPlaceholdersToFieldsMap();
    static System::ArrayPtr<System::String>& gValidFormats();

    System::String mTextBefore;
    System::String mTextAfter;
    System::SharedPtr<System::Text::StringBuilder> mFormatBuilder;

    static const System::String& AlternateTextSwitch();
    static const System::String& NameFormatSwitch();
    static const System::String& LanguageIdSwitch();
    bool IsValidFormat();

    static void __StaticConstructor__();

};

}
}
}
