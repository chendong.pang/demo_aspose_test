//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/FieldMergeField.h
#pragma once

#include <system/text/regularexpressions/regex.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegionUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldMergeFieldParamBag; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMergeDataSourceResettableDecorator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldFactory; } } }
namespace Aspose { namespace Words { namespace MailMerging { class TagReplacer; } } }
namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFieldImageDimension; } } }
namespace Aspose { namespace Words { namespace Fields { class IMergeFieldSurrogate; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace MailMerging { class ImageFieldMergingArgs; } } }
namespace Aspose { namespace Bidi { class IBidiParagraphLevelOverride; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the MERGEFIELD field.
class ASPOSE_WORDS_SHARED_CLASS FieldMergeField : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldMergeField ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::MailMerging::MailMergeRegion;
    friend class Aspose::Words::MailMerging::MailMergeRegionUtil;
    friend class Aspose::Words::Fields::FieldMergeFieldParamBag;
    friend class Aspose::Words::MailMerging::MailMergeDataSourceResettableDecorator;
    friend class Aspose::Words::Fields::FieldFactory;
    friend class Aspose::Words::MailMerging::TagReplacer;
    friend class Aspose::Words::MailMerging::MailMerge;

protected:

    /// MergeFieldType enumeration.
    enum class MergeFieldType
    {
        Common,
        Image,
        Barcode,
        RegionStart,
        RegionEnd
    };

public:

    /// Gets the Microsoft Word field type.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldType get_Type() override;
    /// Returns just the name of the data field. Any prefix is stripped to the prefix property.
    ASPOSE_WORDS_SHARED_API System::String get_FieldNameNoPrefix() const;
    /// Gets the name of a data field.
    ASPOSE_WORDS_SHARED_API System::String get_FieldName();
    /// Sets the name of a data field.
    ASPOSE_WORDS_SHARED_API void set_FieldName(System::String value);
    /// Gets the text to be inserted before the field if the field is not blank.
    ASPOSE_WORDS_SHARED_API System::String get_TextBefore();
    /// Sets the text to be inserted before the field if the field is not blank.
    ASPOSE_WORDS_SHARED_API void set_TextBefore(System::String value);
    /// Gets the text to be inserted after the field if the field is not blank.
    ASPOSE_WORDS_SHARED_API System::String get_TextAfter();
    /// Sets the text to be inserted after the field if the field is not blank.
    ASPOSE_WORDS_SHARED_API void set_TextAfter(System::String value);
    /// Gets whether this field is a mapped field.
    ASPOSE_WORDS_SHARED_API bool get_IsMapped();
    /// Sets whether this field is a mapped field.
    ASPOSE_WORDS_SHARED_API void set_IsMapped(bool value);
    /// Gets whether to enable character conversion for vertical formatting.
    ASPOSE_WORDS_SHARED_API bool get_IsVerticalFormatting();
    /// Sets whether to enable character conversion for vertical formatting.
    ASPOSE_WORDS_SHARED_API void set_IsVerticalFormatting(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    bool get_IsRegionStartMark();
    bool get_IsRegionEndMark();
    bool get_IsImageField();
    System::String get_Prefix() const;

    FieldMergeField();

    static System::SharedPtr<Aspose::Words::Fields::FieldMergeField> CreateFromSurrogate(System::SharedPtr<Aspose::Words::Fields::IMergeFieldSurrogate> surrogate);
    static System::SharedPtr<Aspose::Words::Fields::IMergeFieldSurrogate> GetMergeFieldSurrogate(System::SharedPtr<Aspose::Words::Fields::Field> field);
    ASPOSE_WORDS_SHARED_API void Initialize(System::SharedPtr<Aspose::Words::Fields::FieldStart> start, System::SharedPtr<Aspose::Words::Fields::FieldSeparator> separator, System::SharedPtr<Aspose::Words::Fields::FieldEnd> end) override;
    ASPOSE_WORDS_SHARED_API void ParseFieldCode() override;
    static System::SharedPtr<Aspose::Words::Fields::FieldMergeFieldParamBag> ParseFieldCodeToParamBag(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode, System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mailMerge);
    static System::SharedPtr<Aspose::Words::Fields::FieldMergeFieldParamBag> ParseFieldCodeToParamBag(System::String fieldCode, System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mailMerge);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API void EndUpdate() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Bidi::IBidiParagraphLevelOverride> GetBidiParagraphLevelOverride() override;
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> GetImageWidthCopy();
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> GetImageHeightCopy();
    static bool ContainsNamePrefixSeparator(System::String value);

    virtual ASPOSE_WORDS_SHARED_API ~FieldMergeField();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_HasTextBeforeOrTextAfter();

    System::String pr_Prefix;

    void set_Prefix(System::String value);

    System::String pr_FieldNameNoPrefix;

    ASPOSE_WORDS_SHARED_API void set_FieldNameNoPrefix(System::String value);

    bool mIsSurrogate;
    Aspose::Words::Fields::FieldMergeField::MergeFieldType mMergeFieldType;
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> mImageWidth;
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> mImageHeight;

    static System::SharedPtr<System::Text::RegularExpressions::Regex>& gImageSizeRegex();
    static System::ArrayPtr<char16_t>& gNamePrefixSeparators();
    static const System::String& TextBeforeSwitch();
    static const System::String& TextAfterSwitch();
    static const System::String& IsMappedSwitch();
    static const System::String& IsVerticalFormattingSwitch();

    FieldMergeField(bool isSurrogate);

    void ParseFieldCode(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode);
    static bool ProcessPossibleImageField(System::String prefix, System::SharedPtr<Aspose::Words::Fields::FieldMergeFieldParamBag> param);
    System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegion> GetMailMergeRegion();
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> MergeField(System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegion> region);
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> MergeTextField(System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegion> region, System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mailMerge);
    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> DataValueToConstant(System::SharedPtr<System::Object> dataValue);
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> MergeImageField(System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegion> region, System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mailMerge);
    static System::SharedPtr<System::IO::Stream> ResolveImageStreamFromDatasourceValue(System::SharedPtr<System::Object> dataValue, System::SharedPtr<Aspose::Words::MailMerging::ImageFieldMergingArgs> e);
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> ProcessMissingDataValue(System::SharedPtr<Aspose::Words::MailMerging::MailMerge> mailMerge);
    static System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> GetImageDimensionCopy(System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> source);

private:

    static constexpr int32_t ImageSizeGroupIndex = 3;
    static constexpr int32_t ImageWidthValueGroupIndex = 4;
    static constexpr int32_t ImageWidthUnitGroupIndex = 5;
    static constexpr int32_t ImageHeightValueGroupIndex = 6;
    static constexpr int32_t ImageHeightUnitGroupIndex = 7;
    static constexpr int32_t FieldNameArgumentIndex = 0;

};

}
}
}
