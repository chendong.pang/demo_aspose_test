//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/FieldNextIf.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the NEXTIF field.
class ASPOSE_WORDS_SHARED_CLASS FieldNextIf : public Aspose::Words::Fields::Field
{
    typedef FieldNextIf ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::MailMerging::MailMergeRegion;

public:

    /// Gets the left part of the comparison expression.
    ASPOSE_WORDS_SHARED_API System::String get_LeftExpression();
    /// Sets the left part of the comparison expression.
    ASPOSE_WORDS_SHARED_API void set_LeftExpression(System::String value);
    /// Gets the comparison operator.
    ASPOSE_WORDS_SHARED_API System::String get_ComparisonOperator();
    /// Sets the comparison operator.
    ASPOSE_WORDS_SHARED_API void set_ComparisonOperator(System::String value);
    /// Gets the right part of the comparison expression.
    ASPOSE_WORDS_SHARED_API System::String get_RightExpression();
    /// Sets the right part of the comparison expression.
    ASPOSE_WORDS_SHARED_API void set_RightExpression(System::String value);

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetExpressionResult();

    virtual ASPOSE_WORDS_SHARED_API ~FieldNextIf();

private:

    static const int32_t LeftExpressionArgumentIndex;
    static const int32_t ComparisonOperatorArgumentIndex;
    static const int32_t RightExpressionArgumentIndex;

};

}
}
}
