//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/MergeFieldImageDimension.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <mutex>
#include <memory>

#include "Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/MergeFieldImageDimensionUnit.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldMergeField; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionInsertImage; } } }
namespace Aspose { namespace Collections { class StringToIntDictionary; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Represents an image dimension (i.e. the width or the height) used across a mail merge process.
/// 
/// @sa Aspose::Words::Fields::MergeFieldImageDimensionUnit
class ASPOSE_WORDS_SHARED_CLASS MergeFieldImageDimension : public System::Object
{
    typedef MergeFieldImageDimension ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldMergeField;
    friend class Aspose::Words::Fields::FieldUpdateActionInsertImage;

public:

    /// The value.
    ASPOSE_WORDS_SHARED_API double get_Value() const;
    /// The value.
    ASPOSE_WORDS_SHARED_API void set_Value(double value);
    /// The unit.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::MergeFieldImageDimensionUnit get_Unit() const;
    /// The unit.
    ASPOSE_WORDS_SHARED_API void set_Unit(Aspose::Words::Fields::MergeFieldImageDimensionUnit value);

    /// Creates an image dimension instance with the given value in points.
    /// 
    /// @param value The value.
    ASPOSE_WORDS_SHARED_API MergeFieldImageDimension(double value);
    /// Creates an image dimension instance with the given value and the given unit.
    /// 
    /// @param value The value.
    /// @param unit The unit.
    ASPOSE_WORDS_SHARED_API MergeFieldImageDimension(double value, Aspose::Words::Fields::MergeFieldImageDimensionUnit unit);

protected:

    MergeFieldImageDimension();

    static System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> TryParse(System::String valueString, System::String unitString);
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> Clone();
    static double ToPoints(System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> imageDimension, double originalValueInPoints);
    static bool HasValue(System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> imageDimension);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    double pr_Value;
    Aspose::Words::Fields::MergeFieldImageDimensionUnit mUnit;

    static System::SharedPtr<Aspose::Collections::StringToIntDictionary>& gUnitNameToValueMap();

    static void __StaticConstructor__();

};

}
}
}
