//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/EquationsAndFormulas/FieldAdvance.h
#pragma once

#include <system/string.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the ADVANCE field.
class ASPOSE_WORDS_SHARED_CLASS FieldAdvance : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldAdvance ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the number of points by which the text that follows the field should be moved down.
    ASPOSE_WORDS_SHARED_API System::String get_DownOffset();
    /// Sets the number of points by which the text that follows the field should be moved down.
    ASPOSE_WORDS_SHARED_API void set_DownOffset(System::String value);
    /// Gets the number of points by which the text that follows the field should be moved left.
    ASPOSE_WORDS_SHARED_API System::String get_LeftOffset();
    /// Sets the number of points by which the text that follows the field should be moved left.
    ASPOSE_WORDS_SHARED_API void set_LeftOffset(System::String value);
    /// Gets the number of points by which the text that follows the field should be moved right.
    ASPOSE_WORDS_SHARED_API System::String get_RightOffset();
    /// Sets the number of points by which the text that follows the field should be moved right.
    ASPOSE_WORDS_SHARED_API void set_RightOffset(System::String value);
    /// Gets the number of points by which the text that follows the field should be moved up.
    ASPOSE_WORDS_SHARED_API System::String get_UpOffset();
    /// Sets the number of points by which the text that follows the field should be moved up.
    ASPOSE_WORDS_SHARED_API void set_UpOffset(System::String value);
    /// Gets the number of points by which the text that follows the field should be moved horizontally
    /// from the left edge of the column, frame, or text box.
    ASPOSE_WORDS_SHARED_API System::String get_HorizontalPosition();
    /// Sets the number of points by which the text that follows the field should be moved horizontally
    /// from the left edge of the column, frame, or text box.
    ASPOSE_WORDS_SHARED_API void set_HorizontalPosition(System::String value);
    /// Gets the number of points by which the text that follows the field should be moved vertically
    /// from the top edge of the page.
    ASPOSE_WORDS_SHARED_API System::String get_VerticalPosition();
    /// Sets the number of points by which the text that follows the field should be moved vertically
    /// from the top edge of the page.
    ASPOSE_WORDS_SHARED_API void set_VerticalPosition(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

private:

    static const System::String& DownOffsetSwitch();
    static const System::String& LeftOffsetSwitch();
    static const System::String& RightOffsetSwitch();
    static const System::String& UpOffsetSwitch();
    static const System::String& HorizontalPositionSwitch();
    static const System::String& VerticalPositionSwitch();

};

}
}
}
