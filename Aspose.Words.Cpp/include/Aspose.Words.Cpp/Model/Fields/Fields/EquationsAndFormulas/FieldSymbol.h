//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/EquationsAndFormulas/FieldSymbol.h
#pragma once

#include <system/text/encoding.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class Inline; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements a SYMBOL field.
class ASPOSE_WORDS_SHARED_CLASS FieldSymbol : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldSymbol ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the character's code point value in decimal or hexadecimal.
    ASPOSE_WORDS_SHARED_API System::String get_CharacterCode();
    /// Sets the character's code point value in decimal or hexadecimal.
    ASPOSE_WORDS_SHARED_API void set_CharacterCode(System::String value);
    /// Gets the name of the font of the character retrieved by the field.
    ASPOSE_WORDS_SHARED_API System::String get_FontName();
    /// Sets the name of the font of the character retrieved by the field.
    ASPOSE_WORDS_SHARED_API void set_FontName(System::String value);
    /// Gets the size in points of the font of the character retrieved by the field.
    ASPOSE_WORDS_SHARED_API System::String get_FontSize();
    /// Sets the size in points of the font of the character retrieved by the field.
    ASPOSE_WORDS_SHARED_API void set_FontSize(System::String value);
    /// Gets whether the character code is interpreted as the value of an ANSI character.
    ASPOSE_WORDS_SHARED_API bool get_IsAnsi();
    /// Sets whether the character code is interpreted as the value of an ANSI character.
    ASPOSE_WORDS_SHARED_API void set_IsAnsi(bool value);
    /// Gets whether the character code is interpreted as the value of a Unicode character.
    ASPOSE_WORDS_SHARED_API bool get_IsUnicode();
    /// Sets whether the character code is interpreted as the value of a Unicode character.
    ASPOSE_WORDS_SHARED_API void set_IsUnicode(bool value);
    /// Gets whether the character code is interpreted as the value of a SHIFT-JIS character.
    ASPOSE_WORDS_SHARED_API bool get_IsShiftJis();
    /// Sets whether the character code is interpreted as the value of a SHIFT-JIS character.
    ASPOSE_WORDS_SHARED_API void set_IsShiftJis(bool value);
    /// Gets whether the character retrieved by the field affects the line spacing of the paragraph.
    ASPOSE_WORDS_SHARED_API bool get_DontAffectsLineSpacing();
    /// Sets whether the character retrieved by the field affects the line spacing of the paragraph.
    ASPOSE_WORDS_SHARED_API void set_DontAffectsLineSpacing(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetFakeResult() override;

private:

    bool get_IsHex();
    double get_FontSizeAsDouble();

    static const int32_t CharacterCodeArgumentIndex;

    static const System::String& IsAnsiSwitch();
    static const System::String& FontNameSwitch();
    static const System::String& DontAffectsLineSpacingSwitch();
    static const System::String& IsShiftJisSwitch();
    static const System::String& FontSizeSwitch();
    static const System::String& IsUnicodeSwitch();
    static const System::String& ErroneousResult();
    static System::SharedPtr<System::Text::Encoding>& gAnsiEncoding();
    System::String GetResultText();
    System::SharedPtr<Aspose::Words::Inline> GetResultFormatSourceNode();
    int32_t GetIntCharacterCode();

};

}
}
}
