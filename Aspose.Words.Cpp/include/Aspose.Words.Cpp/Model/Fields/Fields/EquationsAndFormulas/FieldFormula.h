//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/EquationsAndFormulas/FieldFormula.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldFormText; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the = (formula) field.
class ASPOSE_WORDS_SHARED_CLASS FieldFormula : public Aspose::Words::Fields::Field
{
    typedef FieldFormula ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldFormText;

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> Evaluate();

    virtual ASPOSE_WORDS_SHARED_API ~FieldFormula();

private:

    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> Evaluate(System::String expression);

};

}
}
}
