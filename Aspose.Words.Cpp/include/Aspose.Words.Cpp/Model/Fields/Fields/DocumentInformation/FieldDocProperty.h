//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentInformation/FieldDocProperty.h
#pragma once

#include <system/text/regularexpressions/regex.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace Properties { class DocumentProperty; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the DOCPROPERTY field.
class ASPOSE_WORDS_SHARED_CLASS FieldDocProperty : public Aspose::Words::Fields::Field
{
    typedef FieldDocProperty ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

protected:

    System::String get_PropertyName();
    void set_PropertyName(System::String value);

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldDocProperty();

private:

    static const System::String& DateTimeFormatFormat();
    static const System::String& DefaultTimeSeparator();
    static const System::String& DefaultHourFormat();

    static const int32_t HourFormatGroupIndex;

    static System::SharedPtr<System::Text::RegularExpressions::Regex>& gHourFormatRegex();
    static System::SharedPtr<System::Text::RegularExpressions::Regex>& gYearFormatRegex();
    static const System::String& YearFormatReplacePattern();

    static const int32_t PropertyNameArgumentIndex;

    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetUnformattedResult(System::SharedPtr<Aspose::Words::Properties::DocumentProperty> property, bool isCustomProperty);
    System::String GetUnformattedDateTimeResult(System::SharedPtr<Aspose::Words::Properties::DocumentProperty> property, bool isCustomProperty);

};

}
}
}
