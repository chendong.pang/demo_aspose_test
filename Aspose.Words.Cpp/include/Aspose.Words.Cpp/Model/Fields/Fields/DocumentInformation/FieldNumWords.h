//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentInformation/FieldNumWords.h
#pragma once

#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/Fields/Transitional/IFieldInfoResultProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Int32Constant; } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldCode; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the NUMWORDS field.
class ASPOSE_WORDS_SHARED_CLASS FieldNumWords : public Aspose::Words::Fields::Field
{
    typedef FieldNumWords ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldInfo;

private:

    class NumWordsInfoResultProvider : public Aspose::Words::Fields::IFieldInfoResultProvider
    {
        typedef NumWordsInfoResultProvider ThisType;
        typedef Aspose::Words::Fields::IFieldInfoResultProvider BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetResult(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode) override;

    };

protected:

    static System::SharedPtr<Aspose::Words::Fields::IFieldInfoResultProvider>& FieldInfoResultProvider();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldNumWords();

private:

    static System::SharedPtr<Aspose::Words::Fields::Expressions::Int32Constant> GetResultInternal(System::SharedPtr<Aspose::Words::Document> document);

};

}
}
}
