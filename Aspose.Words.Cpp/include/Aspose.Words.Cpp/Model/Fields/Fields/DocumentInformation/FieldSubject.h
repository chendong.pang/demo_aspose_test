//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentInformation/FieldSubject.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Fields/Transitional/IFieldInfoResultProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class IFieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the SUBJECT field.
class ASPOSE_WORDS_SHARED_CLASS FieldSubject : public Aspose::Words::Fields::Field
{
    typedef FieldSubject ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldInfo;

private:

    class SubjectInfoResultProvider : public Aspose::Words::Fields::IFieldInfoResultProvider
    {
        typedef SubjectInfoResultProvider ThisType;
        typedef Aspose::Words::Fields::IFieldInfoResultProvider BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetResult(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode) override;

    };

public:

    /// Gets the text of the subject.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Sets the text of the subject.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);

protected:

    static System::SharedPtr<Aspose::Words::Fields::IFieldInfoResultProvider>& FieldInfoResultProvider();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldSubject();

private:

    static const int32_t TextArgumentIndex;

    static System::String GetResultFieldSubject(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode);

};

}
}
}
