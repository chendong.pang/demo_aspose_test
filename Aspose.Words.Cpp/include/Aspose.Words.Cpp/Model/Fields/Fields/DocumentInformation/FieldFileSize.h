//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentInformation/FieldFileSize.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/Transitional/IFieldInfoResultProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Int32Constant; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class IFieldCode; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the FILESIZE field.
/// 
/// Retrieves the size of the current document's file or 0 if the size cannot be determined.
/// 
/// In the current implementation, uses the <see cref="Aspose::Words::Document::get_OriginalFileName">OriginalFileName</see> property to retrieve
/// the file name used to determine the file size.
class ASPOSE_WORDS_SHARED_CLASS FieldFileSize : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldFileSize ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldInfo;

private:

    class FileSizeInfoResultProvider : public Aspose::Words::Fields::IFieldInfoResultProvider
    {
        typedef FileSizeInfoResultProvider ThisType;
        typedef Aspose::Words::Fields::IFieldInfoResultProvider BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> GetResult(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode) override;

    };

public:

    /// Gets whether to display the file size in kilobytes.
    ASPOSE_WORDS_SHARED_API bool get_IsInKilobytes();
    /// Sets whether to display the file size in kilobytes.
    ASPOSE_WORDS_SHARED_API void set_IsInKilobytes(bool value);
    /// Gets whether to display the file size in megabytes.
    ASPOSE_WORDS_SHARED_API bool get_IsInMegabytes();
    /// Sets whether to display the file size in megabytes.
    ASPOSE_WORDS_SHARED_API void set_IsInMegabytes(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    static System::SharedPtr<Aspose::Words::Fields::IFieldInfoResultProvider>& FieldInfoResultProvider();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    static Aspose::Words::Fields::FieldSwitchType GetSwitchTypeInternal(System::String switchName);

    virtual ASPOSE_WORDS_SHARED_API ~FieldFileSize();

private:

    static const System::String& IsInKilobytesSwitch();
    static const System::String& IsInMegabytesSwitch();
    static System::SharedPtr<Aspose::Words::Fields::Expressions::Int32Constant> GetResultInternal(System::SharedPtr<Aspose::Words::Document> document, System::SharedPtr<Aspose::Words::Fields::IFieldCode> fieldCode);

};

}
}
}
