//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldPageRef.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class ChapterTitleParagraphFinder; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanField; } } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Int32Constant; } } } }
namespace Aspose { namespace Words { class Section; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the PAGEREF field.
class ASPOSE_WORDS_SHARED_CLASS FieldPageRef : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldPageRef ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::ChapterTitleParagraphFinder;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Layout::Core::SpanField;

public:

    /// Gets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets whether to insert a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertHyperlink();
    /// Sets whether to insert a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertHyperlink(bool value);
    /// Gets whether to insert a relative position of the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertRelativePosition();
    /// Sets whether to insert a relative position of the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertRelativePosition(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    System::SharedPtr<Aspose::Words::Node> get_BookmarkNode();
    void set_BookmarkNode(System::SharedPtr<Aspose::Words::Node> value);

    bool CanBeUpdated();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Section> GetPageNumberFormatSection() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldPageRef();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_BookmarkNameHasChars();

    System::SharedPtr<Aspose::Words::Node> mBookmarkNode;
    static const int32_t BookmarkNameArgumentIndex;

    static const System::String& InsertHyperlinkSwitch();
    static const System::String& InsertRelativePositionSwitch();
    System::String GetRelativePosition(System::SharedPtr<Aspose::Words::Fields::Expressions::Int32Constant> bookmarkPage);
    static System::String GetOnPageNString(int32_t pageNumber);

};

}
}
}
