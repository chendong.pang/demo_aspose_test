//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldNoteRef.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { class Footnote; } }
namespace Aspose { namespace Words { class Run; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the NOTEREF field.
class ASPOSE_WORDS_SHARED_CLASS FieldNoteRef : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldNoteRef ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Inserts the reference mark with the same character formatting as the Footnote Reference
    /// or Endnote Reference style.
    ASPOSE_WORDS_SHARED_API bool get_InsertReferenceMark();
    /// Inserts the reference mark with the same character formatting as the Footnote Reference
    /// or Endnote Reference style.
    ASPOSE_WORDS_SHARED_API void set_InsertReferenceMark(bool value);
    /// Gets whether to insert a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertHyperlink();
    /// Sets whether to insert a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertHyperlink(bool value);
    /// Gets whether to insert a relative position of the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertRelativePosition();
    /// Sets whether to insert a relative position of the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertRelativePosition(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldNoteRef();

private:

    static const int32_t BookmarkNameArgumentIndex;

    static const System::String& InsertReferenceMarkSwitch();
    static const System::String& InsertHyperlinkSwitch();
    static const System::String& InsertRelativePositionSwitch();
    static const System::String& NoBookmarkNameGivenErrorMessage();
    static const System::String& BookmarkNotDefinedErrorMessage();
    System::SharedPtr<Aspose::Words::NodeRange> GetResultRange(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Footnote> footnote);
    System::SharedPtr<Aspose::Words::Run> GetReferenceMarkRun(System::SharedPtr<Aspose::Words::Footnote> footnote);
    System::SharedPtr<Aspose::Words::Run> GetRelativePositionRun(System::SharedPtr<Aspose::Words::Bookmark> bookmark);

};

}
}
}
