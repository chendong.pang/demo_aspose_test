//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldSet.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldArgument; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the SET field.
class ASPOSE_WORDS_SHARED_CLASS FieldSet : public Aspose::Words::Fields::Field
{
    typedef FieldSet ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets the new text of the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkText();
    /// Sets the new text of the bookmark.
    ASPOSE_WORDS_SHARED_API void set_BookmarkText(System::String value);

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldSet();

private:

    System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_BookmarkTextArgument();

    static const int32_t BookmarkNameArgumentIndex;
    static const int32_t BookmarkTextArgumentIndex;

};

}
}
}
