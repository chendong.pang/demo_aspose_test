//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldRef.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ilist.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Lists/ListLabelUtil.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/IMergeFieldSurrogate.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldStyleRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStage; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Lists { class ListNumberState; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionApplyResult; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the REF field.
class ASPOSE_WORDS_SHARED_CLASS FieldRef : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider, public Aspose::Words::Fields::IMergeFieldSurrogate
{
    typedef FieldRef ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;
    typedef Aspose::Words::Fields::IMergeFieldSurrogate BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldStyleRef;

private:

    class FieldRefListLabelBuildBehaviour : public Aspose::Words::Lists::IListLabelBuildBehaviour
    {
        typedef FieldRefListLabelBuildBehaviour ThisType;
        typedef Aspose::Words::Lists::IListLabelBuildBehaviour BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static System::SharedPtr<Aspose::Words::Fields::FieldRef::FieldRefListLabelBuildBehaviour>& Instance();
        void NotifyListNumberAppended(int32_t listLabelLength) override;
        bool ShouldAppendNotListNumberChar(char16_t c) override;
        int32_t FinilazeListLabelLength(int32_t listLabelLength) override;

    private:

        FieldRefListLabelBuildBehaviour();

        static bool IsDelimiterChar(char16_t c);

    };

public:

    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() override;
    /// Gets the node that represents the field separator. Can be null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldSeparator> get_Separator() override;
    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() override;
    /// Gets the referenced bookmark's name.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the referenced bookmark's name.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API System::String get_NumberSeparator();
    /// Sets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API void set_NumberSeparator(System::String value);
    /// Gets whether to increment footnote, endnote, and annotation numbers that are
    /// marked by the bookmark, and insert the corresponding footnote, endnote, and comment text.
    ASPOSE_WORDS_SHARED_API bool get_IncludeNoteOrComment();
    /// Sets whether to increment footnote, endnote, and annotation numbers that are
    /// marked by the bookmark, and insert the corresponding footnote, endnote, and comment text.
    ASPOSE_WORDS_SHARED_API void set_IncludeNoteOrComment(bool value);
    /// Gets whether to create a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertHyperlink();
    /// Sets whether to create a hyperlink to the bookmarked paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertHyperlink(bool value);
    /// Gets whether to insert the paragraph number of the referenced paragraph exactly as it appears in the document.
    ASPOSE_WORDS_SHARED_API bool get_InsertParagraphNumber();
    /// Sets whether to insert the paragraph number of the referenced paragraph exactly as it appears in the document.
    ASPOSE_WORDS_SHARED_API void set_InsertParagraphNumber(bool value);
    /// Gets whether to insert the relative position of the referenced paragraph.
    ASPOSE_WORDS_SHARED_API bool get_InsertRelativePosition();
    /// Sets whether to insert the relative position of the referenced paragraph.
    ASPOSE_WORDS_SHARED_API void set_InsertRelativePosition(bool value);
    /// Gets whether to insert the paragraph number of the referenced paragraph in relative context.
    ASPOSE_WORDS_SHARED_API bool get_InsertParagraphNumberInRelativeContext();
    /// Sets whether to insert the paragraph number of the referenced paragraph in relative context.
    ASPOSE_WORDS_SHARED_API void set_InsertParagraphNumberInRelativeContext(bool value);
    /// Gets whether to suppress non-delimiter characters.
    ASPOSE_WORDS_SHARED_API bool get_SuppressNonDelimiters();
    /// Sets whether to suppress non-delimiter characters.
    ASPOSE_WORDS_SHARED_API void set_SuppressNonDelimiters(bool value);
    /// Gets whether to insert the paragraph number of the referenced paragraph in full context.
    ASPOSE_WORDS_SHARED_API bool get_InsertParagraphNumberInFullContext();
    /// Sets whether to insert the paragraph number of the referenced paragraph in full context.
    ASPOSE_WORDS_SHARED_API void set_InsertParagraphNumberInFullContext(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;
    ASPOSE_WORDS_SHARED_API System::String GetMergeFieldName() override;
    ASPOSE_WORDS_SHARED_API bool CanWorkAsMergeField() override;
    ASPOSE_WORDS_SHARED_API bool IsMergeValueRequired() override;

protected:

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStage GetUpdateStage() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    static System::String GetLabelStringInRelativeContext(System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, System::SharedPtr<Aspose::Words::Paragraph> relativeParagraph, bool suppressNonDelimiters, System::String numberSeparator);
    static System::String GetLabelStringInParagraphNumberContext(System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, bool suppressNonDelimiters);
    static System::String GetTrimmedLabelStringInParagraphNumberContext(System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph);
    static System::String GetLabelStringInFullContext(System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, bool suppressNonDelimiters, System::String numberSeparator);

    virtual ASPOSE_WORDS_SHARED_API ~FieldRef();

private:

    bool get_InsertParagraphNumberInAnyContext();

    static const int32_t BookmarkNameArgumentIndex;

    static const System::String& NumberSeparatorSwitch();
    static const System::String& IncludeNoteOrCommentSwitch();
    static const System::String& InsertHyperlinkSwitch();
    static const System::String& InsertParagraphNumberSwitch();
    static const System::String& InsertRelativePositionSwitch();
    static const System::String& InsertParagraphNumberInRelativeContextSwitch();
    static const System::String& SuppressNonDelimitersSwitch();
    static const System::String& InsertParagraphNumberInFullContextSwitch();
    static System::String GetLabelStringInRelativeContext(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, System::SharedPtr<Aspose::Words::Paragraph> relativeParagraph, bool suppressNonDelimiters, System::String numberSeparator);
    static System::SharedPtr<Aspose::Words::Lists::ListNumberState> GetLastNumberStateBeforeParagraph(System::SharedPtr<Aspose::Words::Lists::ListNumberState> refNumberState, System::SharedPtr<Aspose::Words::Paragraph> fieldParagraph);
    static System::String GetLabelStringInParagraphNumberContext(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, bool suppressNonDelimiters);
    static System::String GetLabelStringInFullContext(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Paragraph> sourceParagraph, bool suppressNonDelimiters, System::String numberSeparator);
    static System::SharedPtr<Aspose::Words::Lists::IListLabelBuildBehaviour> GetListLabelBuildBehaviour(bool suppressNonDelimiters);
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateActionApplyResult> AppendRelativeBookmarkPosition(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::String fieldResult);
    static System::String TrimTrailingDot(System::String source);
    static System::SharedPtr<Aspose::Words::Lists::ListNumberState> GetParagraphListNumberState(System::SharedPtr<Aspose::Words::Paragraph> paragraph);
    static System::String GetListLabel(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Paragraph> paragraph, bool suppressNonDelimiters);
    static System::SharedPtr<Aspose::Words::Lists::ListNumberState> GetListNumberState(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<Aspose::Words::Paragraph> paragraph);
    static System::SharedPtr<Aspose::Words::Lists::ListNumberState> GetFieldListNumberState(System::SharedPtr<Aspose::Words::Fields::Field> field);
    static System::SharedPtr<Aspose::Words::Lists::ListNumberState> GetParagraphListNumberStateFromListNumField(System::SharedPtr<Aspose::Words::Paragraph> paragraph);
    static System::SharedPtr<Aspose::Words::Fields::Field> GetListNumFieldFromParagraph(System::SharedPtr<Aspose::Words::Paragraph> paragraph);
    static System::SharedPtr<Aspose::Words::Fields::Field> GetFirstField(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::Field>>> fields);
    static System::SharedPtr<Aspose::Words::Fields::Field> GetListNumFieldFromParagraphWithBookmark(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::Field>>> fields);
    static System::SharedPtr<Aspose::Words::Fields::Field> GetListNumFieldFromParagraphInsideBookmark(System::SharedPtr<Aspose::Words::Bookmark> bookmark, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::Field>>> fields);
    static System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::Field>>> ExtractListNumFields(System::SharedPtr<Aspose::Words::Paragraph> paragraph);

};

}
}
}
