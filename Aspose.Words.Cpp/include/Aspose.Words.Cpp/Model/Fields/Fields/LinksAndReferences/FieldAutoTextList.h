//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldAutoTextList.h
#pragma once

#include <system/string.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the AUTOTEXTLIST field.
class ASPOSE_WORDS_SHARED_CLASS FieldAutoTextList : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldAutoTextList ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the name of the AutoText entry.
    ASPOSE_WORDS_SHARED_API System::String get_EntryName();
    /// Sets the name of the AutoText entry.
    ASPOSE_WORDS_SHARED_API void set_EntryName(System::String value);
    /// Gets the name of the style on which the list to contain entries is based.
    ASPOSE_WORDS_SHARED_API System::String get_ListStyle();
    /// Sets the name of the style on which the list to contain entries is based.
    ASPOSE_WORDS_SHARED_API void set_ListStyle(System::String value);
    /// Gets the text of the ScreenTip to show.
    ASPOSE_WORDS_SHARED_API System::String get_ScreenTip();
    /// Sets the text of the ScreenTip to show.
    ASPOSE_WORDS_SHARED_API void set_ScreenTip(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

private:

    static const int32_t EntryNameArgumentIndex;

    static const System::String& ListStyleSwitch();
    static const System::String& ScreenTipSwitch();

};

}
}
}
