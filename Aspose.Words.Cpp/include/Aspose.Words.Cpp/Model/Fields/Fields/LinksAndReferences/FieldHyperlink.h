//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldHyperlink.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/Format/ResultFormat/IFieldResultFormatProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Format/ResultFormat/IFieldResultFormatApplier.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanField; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeHyperlink; } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageFieldWriterBase; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class Inline; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class RunPr; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the HYPERLINK field
class ASPOSE_WORDS_SHARED_CLASS FieldHyperlink : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider, public Aspose::Words::Fields::IFieldResultFormatProvider
{
    typedef FieldHyperlink ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;
    typedef Aspose::Words::Fields::IFieldResultFormatProvider BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Layout::Core::SpanField;
    friend class Aspose::Words::Fields::FieldCodeHyperlink;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageFieldWriterBase;

private:

    class HyperlinkStyleFormatApplier : public Aspose::Words::Fields::IFieldResultFormatApplier
    {
        typedef HyperlinkStyleFormatApplier ThisType;
        typedef Aspose::Words::Fields::IFieldResultFormatApplier BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static System::SharedPtr<Aspose::Words::Fields::FieldHyperlink::HyperlinkStyleFormatApplier>& Instance();
        void ApplyFormat(System::SharedPtr<Aspose::Words::NodeRange> result) override;

    private:

        HyperlinkStyleFormatApplier();

    };

    class RetainingFormatApplier : public Aspose::Words::Fields::IFieldResultFormatApplier
    {
        typedef RetainingFormatApplier ThisType;
        typedef Aspose::Words::Fields::IFieldResultFormatApplier BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static System::SharedPtr<Aspose::Words::Fields::FieldHyperlink::RetainingFormatApplier> GetFormatApplier(System::SharedPtr<Aspose::Words::Fields::Field> field);
        void ApplyFormat(System::SharedPtr<Aspose::Words::NodeRange> result) override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::RunPr> mRunPr;

        RetainingFormatApplier(System::SharedPtr<Aspose::Words::RunPr> runPr);

    };

public:

    /// Gets the target to which the link should be redirected.
    ASPOSE_WORDS_SHARED_API System::String get_Target();
    /// Sets the target to which the link should be redirected.
    ASPOSE_WORDS_SHARED_API void set_Target(System::String value);
    /// Gets a location where this hyperlink jumps.
    ASPOSE_WORDS_SHARED_API System::String get_Address();
    /// Sets a location where this hyperlink jumps.
    ASPOSE_WORDS_SHARED_API void set_Address(System::String value);
    /// Gets a location in the file, such as a bookmark, where this hyperlink jumps.
    ASPOSE_WORDS_SHARED_API System::String get_SubAddress();
    /// Sets a location in the file, such as a bookmark, where this hyperlink jumps.
    ASPOSE_WORDS_SHARED_API void set_SubAddress(System::String value);
    /// Gets whether to append coordinates to the hyperlink for a server-side image map.
    ASPOSE_WORDS_SHARED_API bool get_IsImageMap();
    /// Sets whether to append coordinates to the hyperlink for a server-side image map.
    ASPOSE_WORDS_SHARED_API void set_IsImageMap(bool value);
    /// Gets whether to open the destination site in a new web browser window.
    ASPOSE_WORDS_SHARED_API bool get_OpenInNewWindow();
    /// Sets whether to open the destination site in a new web browser window.
    ASPOSE_WORDS_SHARED_API void set_OpenInNewWindow(bool value);
    /// Gets the ScreenTip text for the hyperlink.
    ASPOSE_WORDS_SHARED_API System::String get_ScreenTip();
    /// Sets the ScreenTip text for the hyperlink.
    ASPOSE_WORDS_SHARED_API void set_ScreenTip(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Inline> GetSourceNode() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::IFieldResultFormatApplier> GetFormatApplier() override;

protected:

    System::String get_HRef();

    static const int32_t AddressArgumentIndex;

    static const System::String& SubAddressSwitch();
    static const System::String& IsImageMapSwitch();
    static const System::String& OpenInNewWindowSwitch();
    static const System::String& ScreenTipSwitch();
    static const System::String& TargetSwitch();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API void BeforeUnlink() override;
    ASPOSE_WORDS_SHARED_API bool NeedStoreOldResultNodes() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldHyperlink();

private:

    bool get_HasResult();
    bool get_IsDirectResultUpdate();

    System::String GetHyperlinkText();

};

}
}
}
