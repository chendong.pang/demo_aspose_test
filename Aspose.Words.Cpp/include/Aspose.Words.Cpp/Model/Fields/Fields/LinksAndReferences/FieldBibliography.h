//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/LinksAndReferences/FieldBibliography.h
#pragma once

#include <system/string.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the BIBLIOGRAPHY field.
class ASPOSE_WORDS_SHARED_CLASS FieldBibliography : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldBibliography ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the language ID that is used to format the bibliographic sources in the document.
    ASPOSE_WORDS_SHARED_API System::String get_FormatLanguageId();
    /// Sets the language ID that is used to format the bibliographic sources in the document.
    ASPOSE_WORDS_SHARED_API void set_FormatLanguageId(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    System::String get_FilterLanguageId();
    void set_FilterLanguageId(System::String value);
    System::String get_SourceTag();
    void set_SourceTag(System::String value);

private:

    static const System::String& FormatLanguageIdSwitch();
    static const System::String& FilterLanguageIdSwitch();
    static const System::String& SourceTagSwitch();

};

}
}
}
