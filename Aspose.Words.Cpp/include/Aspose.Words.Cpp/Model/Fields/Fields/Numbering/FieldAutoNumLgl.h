//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Numbering/FieldAutoNumLgl.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { class NodeRange; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the AUTONUMLGL field.
class ASPOSE_WORDS_SHARED_CLASS FieldAutoNumLgl : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldAutoNumLgl ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;

public:

    /// Gets whether to display the number without a trailing period.
    ASPOSE_WORDS_SHARED_API bool get_RemoveTrailingPeriod();
    /// Sets whether to display the number without a trailing period.
    ASPOSE_WORDS_SHARED_API void set_RemoveTrailingPeriod(bool value);
    /// Gets the separator character to be used.
    ASPOSE_WORDS_SHARED_API System::String get_SeparatorCharacter();
    /// Sets the separator character to be used.
    ASPOSE_WORDS_SHARED_API void set_SeparatorCharacter(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    char16_t get_SeparatorCharacterCore();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetFakeResult() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldAutoNumLgl();

private:

    static const System::String& RemoveTrailingPeriodSwitch();
    static const System::String& SeparatorCharacterSwitch();

};

}
}
}
