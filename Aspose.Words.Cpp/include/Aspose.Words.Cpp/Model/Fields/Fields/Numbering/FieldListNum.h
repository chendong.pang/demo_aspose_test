//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Numbering/FieldListNum.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldNumListLabelUpdater; } } }
namespace Aspose { namespace Words { class NodeRange; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the LISTNUM field.
class ASPOSE_WORDS_SHARED_CLASS FieldListNum : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldListNum ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldNumListLabelUpdater;

public:

    /// Gets the name of the abstract numbering definition used for the numbering.
    ASPOSE_WORDS_SHARED_API System::String get_ListName();
    /// Sets the name of the abstract numbering definition used for the numbering.
    ASPOSE_WORDS_SHARED_API void set_ListName(System::String value);
    /// Returns a value indicating whether the name of an abstract numbering definition
    /// is provided by the field's code.
    ASPOSE_WORDS_SHARED_API bool get_HasListName();
    /// Gets the level in the list, overriding the default behavior of the field.
    ASPOSE_WORDS_SHARED_API System::String get_ListLevel();
    /// Sets the level in the list, overriding the default behavior of the field.
    ASPOSE_WORDS_SHARED_API void set_ListLevel(System::String value);
    /// Gets the starting value for this field.
    ASPOSE_WORDS_SHARED_API System::String get_StartingNumber();
    /// Sets the starting value for this field.
    ASPOSE_WORDS_SHARED_API void set_StartingNumber(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    int32_t get_ListLevelAsInt32();
    int32_t get_ListLevelCore();
    int32_t get_StartingNumberAsInt32();
    int32_t get_StartingNumberCore();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetFakeResult() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldListNum();

private:

    static const int32_t ListNameArgumentIndex;

    static const System::String& ListLevelSwitch();
    static const System::String& StartingNumberSwitch();
    static int32_t ParseIntSwitchValue(System::String switchValue);

};

}
}
}
