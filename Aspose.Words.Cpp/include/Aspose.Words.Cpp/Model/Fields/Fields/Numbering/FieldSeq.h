//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Numbering/FieldSeq.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class TocEntryExtractor; } } }
namespace Aspose { namespace Common { class NullableInt32; } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the SEQ field.
class ASPOSE_WORDS_SHARED_CLASS FieldSeq : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldSeq ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::TocEntryExtractor;

public:

    /// Gets the name assigned to the series of items that are to be numbered.
    ASPOSE_WORDS_SHARED_API System::String get_SequenceIdentifier();
    /// Sets the name assigned to the series of items that are to be numbered.
    ASPOSE_WORDS_SHARED_API void set_SequenceIdentifier(System::String value);
    /// Gets a bookmark name that refers to an item elsewhere in the document rather than in the current location.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets a bookmark name that refers to an item elsewhere in the document rather than in the current location.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets whether to insert the next sequence number for the specified item.
    ASPOSE_WORDS_SHARED_API bool get_InsertNextNumber();
    /// Sets whether to insert the next sequence number for the specified item.
    ASPOSE_WORDS_SHARED_API void set_InsertNextNumber(bool value);
    /// Gets an integer number to reset the sequence number to. Returns -1 if the number is absent.
    ASPOSE_WORDS_SHARED_API System::String get_ResetNumber();
    /// Sets an integer number to reset the sequence number to. Returns -1 if the number is absent.
    ASPOSE_WORDS_SHARED_API void set_ResetNumber(System::String value);
    /// Gets an integer number representing a heading level to reset the sequence number to.
    /// Returns -1 if the number is absent.
    ASPOSE_WORDS_SHARED_API System::String get_ResetHeadingLevel();
    /// Sets an integer number representing a heading level to reset the sequence number to.
    /// Returns -1 if the number is absent.
    ASPOSE_WORDS_SHARED_API void set_ResetHeadingLevel(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    bool get_HasBookmarkName();
    bool get_InsertClosestPrecedingNumber();
    void set_InsertClosestPrecedingNumber(bool value);
    bool get_HideFieldResult();
    void set_HideFieldResult(bool value);
    bool get_HasResetNumberSwitch();
    Aspose::Common::NullableInt32 get_ResetNumberAsInt32();
    bool get_HasResetHeadingLevelSwitch();
    Aspose::Common::NullableInt32 get_ResetHeadingLevelAsInt32();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    bool IsError(System::SharedPtr<Aspose::Words::Fields::FieldUpdater> updater);

    virtual ASPOSE_WORDS_SHARED_API ~FieldSeq();

private:

    bool get_IsOutOfMainTextAndNotValid();
    bool get_IsSequenceIdentifierMissing();

    static const int32_t SequenceIdentifierArgumentIndex;
    static const int32_t BookmarkNameArgumentIndex;

    static const System::String& InsertClosestPrecedingNumberSwitch();
    static const System::String& HideFieldResultSwitch();
    static const System::String& InsertNextNumberSwitch();
    static const System::String& ResetNumberSwitch();
    static const System::String& ResetHeadingLevelSwitch();
    bool IsBookmarkMissing(System::SharedPtr<Aspose::Words::Fields::FieldUpdater> updater);

};

}
}
}
