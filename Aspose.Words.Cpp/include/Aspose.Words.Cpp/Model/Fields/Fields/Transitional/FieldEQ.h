//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Transitional/FieldEQ.h
#pragma once

#include <system/text/string_builder.h>
#include <system/text/regularexpressions/regex.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ilist.h>
#include <system/collections/icollection.h>

#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { namespace Fields { class RichString; } } }
namespace Aspose { namespace Words { namespace Fields { class RichChar; } } }
namespace Aspose { namespace Words { namespace Fields { class RichStringBuilder; } } }
namespace Aspose { namespace Words { namespace Math { class OfficeMath; } } }
namespace Aspose { namespace Words { class CompositeNode; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the EQ field.
class ASPOSE_WORDS_SHARED_CLASS FieldEQ : public Aspose::Words::Fields::Field
{
    typedef FieldEQ ThisType;
    typedef Aspose::Words::Fields::Field BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetFakeResult() override;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    static char16_t get_DataSeparator();

    static const char16_t EmptyChart;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;

    static System::SharedPtr<System::Text::RegularExpressions::Regex>& gTokenRegex();
    System::SharedPtr<Aspose::Words::Node> EqFieldCodeToOfficeMath(System::SharedPtr<Aspose::Words::Fields::RichString> fieldCode);
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Node>>> ReadMathNodes(System::SharedPtr<Aspose::Words::Fields::RichString> fieldCode);
    static void ReadTokenStart(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Text::StringBuilder> currentToken, System::SharedPtr<System::Collections::Generic::ICollection<System::String>> tokens);
    static void ReadDataStart(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Text::StringBuilder> currentToken, System::SharedPtr<System::Collections::Generic::ICollection<System::String>> tokens, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Fields::RichChar>>> openBrackets, System::SharedPtr<Aspose::Words::Fields::RichStringBuilder> currentData);
    void ReadDataEnd(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Text::StringBuilder> currentToken, System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichChar>>> openBrackets, System::SharedPtr<Aspose::Words::Fields::RichStringBuilder> currentData, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Node>>> mathNodes);
    static void ReadDataSeparator(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Fields::RichChar>>> openBrackets, System::SharedPtr<Aspose::Words::Fields::RichStringBuilder> currentData, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    void ReadText(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Text::StringBuilder> currentToken, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Fields::RichChar>>> openBrackets, System::SharedPtr<Aspose::Words::Fields::RichStringBuilder> currentData, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Node>>> mathNodes);
    void AddText(System::SharedPtr<Aspose::Words::Fields::RichChar> c, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Node>>> mathNodes);
    void AddText(System::SharedPtr<Aspose::Words::Fields::RichChar> openBracket, System::SharedPtr<Aspose::Words::Fields::RichString> text, System::SharedPtr<Aspose::Words::Fields::RichChar> closeBracket, System::SharedPtr<System::Collections::Generic::ICollection<System::SharedPtr<Aspose::Words::Node>>> mathNodes);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> EqToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> ArrayToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> BracketToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> DisplaceToOfficeMath();
    System::SharedPtr<Aspose::Words::Math::OfficeMath> FractionToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> IntegralToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> ListToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> OverstrikeToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> RadicalToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> ScriptToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    static System::SharedPtr<Aspose::Words::RunPr> GetArgumentRunPr(System::SharedPtr<Aspose::Words::Math::OfficeMath> math);
    System::SharedPtr<Aspose::Words::Math::OfficeMath> BoxToOfficeMath(System::SharedPtr<System::Collections::Generic::IList<System::String>> tokens, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::RichString>>> data);
    static System::SharedPtr<Aspose::Words::Math::OfficeMath> CreateMatrixRow(System::SharedPtr<Aspose::Words::Math::OfficeMath> parentMatrix, System::SharedPtr<Aspose::Words::RunPr> runPr, bool isOverstrikeRow);
    static void AddChildren(System::SharedPtr<Aspose::Words::CompositeNode> parent, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Node>>> children);
    static char16_t GetClosingBracket(char16_t openBracket);
    static bool IsTokenStart(char16_t c);
    static bool IsDataStart(char16_t c);
    static bool IsDataEnd(char16_t c);
    static bool IsDataSeparator(char16_t c);

};

}
}
}
