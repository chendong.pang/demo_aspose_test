//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/Transitional/FieldInfo.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCode.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldInfoResultProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the INFO field.
class ASPOSE_WORDS_SHARED_CLASS FieldInfo : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldInfo ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

private:

    class FieldCodeDecorator : public Aspose::Words::Fields::IFieldCode
    {
        typedef FieldCodeDecorator ThisType;
        typedef Aspose::Words::Fields::IFieldCode BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        FieldCodeDecorator(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode);

        System::String GetArgumentAsString(int32_t authorNameArgumentIndex) override;
        bool HasSwitch(System::String switchName) override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::Fields::FieldCode> mFieldCode;

    };

public:

    /// Gets the type of the document property to insert.
    ASPOSE_WORDS_SHARED_API System::String get_InfoType();
    /// Sets the type of the document property to insert.
    ASPOSE_WORDS_SHARED_API void set_InfoType(System::String value);
    /// Gets an optional value that updates the property.
    ASPOSE_WORDS_SHARED_API System::String get_NewValue();
    /// Sets an optional value that updates the property.
    ASPOSE_WORDS_SHARED_API void set_NewValue(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldInfo();

private:

    static const int32_t InfoTypeArgumentIndex;
    static const int32_t NewValueArgumentIndex;

    static const System::String& UnknownInfoTypeError();
    System::SharedPtr<Aspose::Words::Fields::IFieldInfoResultProvider> GetProvider();

};

}
}
}
