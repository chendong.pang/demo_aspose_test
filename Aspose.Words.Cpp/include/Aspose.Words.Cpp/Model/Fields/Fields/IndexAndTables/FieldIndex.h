//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/FieldIndex.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/Index/FieldIndexValidateResult.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldCodeIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUtil; } } }
namespace Aspose { namespace Common { class NullableInt32; } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndexWriteContext; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdater; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntry; } } }
namespace Aspose { namespace Words { enum class StyleIdentifier; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntryPageNumberInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeXE; } } }
namespace Aspose { namespace Words { class IRunAttrSource; } }
namespace Aspose { namespace Bidi { class IBidiParagraphLevelOverride; } }
namespace Aspose { namespace Words { class Run; } }
namespace Aspose { namespace Words { namespace Tables { class Row; } } }
namespace Aspose { namespace Words { class INodeModifier; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class Paragraph; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the INDEX field.
class ASPOSE_WORDS_SHARED_CLASS FieldIndex : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldIndex ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldCodeIndex;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldUtil;

private:

    class LinearNodeCopier : public System::Object
    {
        typedef LinearNodeCopier ThisType;
        typedef System::Object BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

        FRIEND_FUNCTION_System_MakeObject;

    public:

        static void Copy(System::SharedPtr<Aspose::Words::NodeRange> range, System::SharedPtr<Aspose::Words::INodeModifier> compositeModifier, System::SharedPtr<Aspose::Words::Node> refNode);

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::Run> mParagraphSeparator;
        System::SharedPtr<Aspose::Words::Run> mTableRowSeparator;
        System::SharedPtr<Aspose::Words::Tables::Row> mLastTableRow;
        System::SharedPtr<Aspose::Words::Node> mLastInline;
        System::SharedPtr<Aspose::Words::Node> mVirtualDomRefNode;
        System::SharedPtr<Aspose::Words::NodeRange> mRange;
        System::SharedPtr<Aspose::Words::INodeModifier> mCompositeModifier;
        System::SharedPtr<Aspose::Words::Node> mRefNode;
        System::SharedPtr<Aspose::Words::DocumentBase> mDocument;

        static const System::String& Separator();

        LinearNodeCopier(System::SharedPtr<Aspose::Words::NodeRange> range, System::SharedPtr<Aspose::Words::INodeModifier> compositeModifier, System::SharedPtr<Aspose::Words::Node> refNode);

        void Copy();
        void VisitParagraph(System::SharedPtr<Aspose::Words::Paragraph> paragraph);
        void VisitTableRow(System::SharedPtr<Aspose::Words::Tables::Row> row);
        void VisitInline(System::SharedPtr<Aspose::Words::Node> node);
        bool NeedSkipNode(System::SharedPtr<Aspose::Words::Node> node);
        void AppendParagraphSeparator();
        void AppendTableRowSeparator();
        void AppendResultNode(System::SharedPtr<Aspose::Words::Node> node);

    };

public:

    /// Gets the name of the bookmark that marks the portion of the document used to build the index.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the name of the bookmark that marks the portion of the document used to build the index.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets the number of columns per page used when building the index.
    ASPOSE_WORDS_SHARED_API System::String get_NumberOfColumns();
    /// Sets the number of columns per page used when building the index.
    ASPOSE_WORDS_SHARED_API void set_NumberOfColumns(System::String value);
    /// Gets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API System::String get_SequenceSeparator();
    /// Sets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API void set_SequenceSeparator(System::String value);
    /// Gets the character sequence that is used to separate an index entry and its page number.
    ASPOSE_WORDS_SHARED_API System::String get_PageNumberSeparator();
    /// Sets the character sequence that is used to separate an index entry and its page number.
    ASPOSE_WORDS_SHARED_API void set_PageNumberSeparator(System::String value);
    /// Gets a value indicating whether a page number separator is overridden through the field's code.
    ASPOSE_WORDS_SHARED_API bool get_HasPageNumberSeparator();
    /// Gets an index entry type used to build the index.
    ASPOSE_WORDS_SHARED_API System::String get_EntryType();
    /// Sets an index entry type used to build the index.
    ASPOSE_WORDS_SHARED_API void set_EntryType(System::String value);
    /// Gets the character sequence that is used to separate the start and end of a page range.
    ASPOSE_WORDS_SHARED_API System::String get_PageRangeSeparator();
    /// Sets the character sequence that is used to separate the start and end of a page range.
    ASPOSE_WORDS_SHARED_API void set_PageRangeSeparator(System::String value);
    /// Gets a heading that appears at the start of each set of entries for any given letter.
    ASPOSE_WORDS_SHARED_API System::String get_Heading();
    /// Sets a heading that appears at the start of each set of entries for any given letter.
    ASPOSE_WORDS_SHARED_API void set_Heading(System::String value);
    /// Gets the character sequence that is used to separate cross references and other entries.
    ASPOSE_WORDS_SHARED_API System::String get_CrossReferenceSeparator();
    /// Sets the character sequence that is used to separate cross references and other entries.
    ASPOSE_WORDS_SHARED_API void set_CrossReferenceSeparator(System::String value);
    /// Gets the character sequence that is used to separate two page numbers in a page number list.
    ASPOSE_WORDS_SHARED_API System::String get_PageNumberListSeparator();
    /// Sets the character sequence that is used to separate two page numbers in a page number list.
    ASPOSE_WORDS_SHARED_API void set_PageNumberListSeparator(System::String value);
    /// Gets a range of letters to which limit the index.
    ASPOSE_WORDS_SHARED_API System::String get_LetterRange();
    /// Sets a range of letters to which limit the index.
    ASPOSE_WORDS_SHARED_API void set_LetterRange(System::String value);
    /// Gets whether run subentries into the same line as the main entry.
    ASPOSE_WORDS_SHARED_API bool get_RunSubentriesOnSameLine();
    /// Sets whether run subentries into the same line as the main entry.
    ASPOSE_WORDS_SHARED_API void set_RunSubentriesOnSameLine(bool value);
    /// Gets the name of a sequence whose number is included with the page number.
    ASPOSE_WORDS_SHARED_API System::String get_SequenceName();
    /// Sets the name of a sequence whose number is included with the page number.
    ASPOSE_WORDS_SHARED_API void set_SequenceName(System::String value);
    /// Gets a value indicating whether a sequence should be used while the field's result building.
    ASPOSE_WORDS_SHARED_API bool get_HasSequenceName();
    /// Gets whether to enable the use of yomi text for index entries.
    ASPOSE_WORDS_SHARED_API bool get_UseYomi();
    /// Sets whether to enable the use of yomi text for index entries.
    ASPOSE_WORDS_SHARED_API void set_UseYomi(bool value);
    /// Gets the language ID used to generate the index.
    ASPOSE_WORDS_SHARED_API System::String get_LanguageId();
    /// Sets the language ID used to generate the index.
    ASPOSE_WORDS_SHARED_API void set_LanguageId(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    bool get_HasNumberOfColumnsSwitch();
    Aspose::Common::NullableInt32 get_NumberOfColumnsAsInt32();

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Bidi::IBidiParagraphLevelOverride> GetBidiParagraphLevelOverride() override;
    bool IsError(System::SharedPtr<Aspose::Words::Fields::FieldUpdater> updater);

    virtual ASPOSE_WORDS_SHARED_API ~FieldIndex();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldIndexWriteContext> mWriteContext;

    static const System::String& BookmarkNameSwitch();
    static const System::String& NumberOfColumnsSwitch();
    static const System::String& SequenceSeparatorSwitch();
    static const System::String& PageNumberSeparatorSwitch();
    static const System::String& EntryTypeSwitch();
    static const System::String& PageRangeSeparatorSwitch();
    static const System::String& HeadingSwitch();
    static const System::String& CrossReferenceSeparatorSwitch();
    static const System::String& PageNumberListSeparatorSwitch();
    static const System::String& LetterRangeSwitch();
    static const System::String& RunSubentriesOnSameLineSwitch();
    static const System::String& SequenceNameSwitch();
    static const System::String& UseYomiSwitch();
    static const System::String& LanguageIdSwitch();
    Aspose::Words::Fields::FieldIndexValidateResult Validate(System::SharedPtr<Aspose::Words::Fields::FieldUpdater> updater);
    static bool HasBodyAsStoryAncestor(System::SharedPtr<Aspose::Words::Node> node);
    void SetFieldSeqDataProviderContextInfo(System::SharedPtr<Aspose::Words::Fields::FieldCodeIndex> fieldCode);
    static void InsertSection(System::SharedPtr<Aspose::Words::DocumentBuilder> builder, int32_t columnsCount, int32_t columnsSpacing);
    void WriteSubentries(System::SharedPtr<Aspose::Words::Node> dummyRefNode, System::SharedPtr<Aspose::Words::Fields::IndexEntry> entry, int32_t subentryLevel);
    static Aspose::Words::StyleIdentifier GetStyleIdentifier(int32_t subentryLevel);
    void WriteHeading(char16_t startChar);
    void WriteSubentry(System::SharedPtr<Aspose::Words::Node> dummyRefNode, System::SharedPtr<Aspose::Words::Fields::IndexEntry> entry, int32_t subentryLevel, int32_t subentryIndex, Aspose::Words::StyleIdentifier styleIdentifier);
    void CopySubentryNodes(System::SharedPtr<Aspose::Words::NodeRange> range, System::SharedPtr<Aspose::Words::Node> refNode, Aspose::Words::StyleIdentifier styleIdentifier);
    static void TrimSubentryNodes(System::SharedPtr<Aspose::Words::Node> startNode, System::SharedPtr<Aspose::Words::Node> endNode, bool isForward);
    void WritePageNumbers(System::SharedPtr<Aspose::Words::Node> refNode, System::SharedPtr<Aspose::Words::Fields::IndexEntry> subentry, Aspose::Words::StyleIdentifier styleIdentifier);
    void WriteSinglePageNumber(int32_t sequenceValue);
    void WritePageNumberRange(System::SharedPtr<Aspose::Words::Fields::IndexEntryPageNumberInfo> pageNumberInfo, int32_t firstSequenceValue, int32_t lastSequenceValue);
    void WritePageRangeBookmarkError(System::SharedPtr<Aspose::Words::Fields::IndexEntryPageNumberInfo> pageNumberInfo, int32_t sequenceValue);
    System::SharedPtr<System::Text::StringBuilder> GetPageNumberBuilder();
    void EndWritePageNumber(System::SharedPtr<System::Text::StringBuilder> pageNumberBuilder, System::SharedPtr<Aspose::Words::Fields::IndexEntryPageNumberInfo> pageNumberInfo);
    void BuildPageNumberBase(System::SharedPtr<System::Text::StringBuilder> pageNumberBuilder, System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> lastFieldCodeXE, System::SharedPtr<Aspose::Words::Fields::IndexEntryPageNumberInfo> pageNumberInfo, int32_t pageNumber, int32_t sequenceValue);
    void BuildPageNumberPart(System::SharedPtr<System::Text::StringBuilder> pageNumberBuilder, System::String part, System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> lastFieldCodeXE, System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> currentFieldCodeXE);
    void WritePageNumberPart(System::SharedPtr<System::Text::StringBuilder> pageNumberBuilder, System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCodeXE);
    static bool NeedInvertBold(System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCodeXE);
    static bool NeedInvertItalic(System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCodeXE);
    static void InvertAttrBoolEx(System::SharedPtr<Aspose::Words::IRunAttrSource> runAttrSource, int32_t key);
    void WritePageNumberReplacements(System::SharedPtr<Aspose::Words::Node> refNode, System::SharedPtr<Aspose::Words::Fields::IndexEntry> subentry, Aspose::Words::StyleIdentifier styleIdentifier);
    void WritePageNumberReplacement(System::SharedPtr<Aspose::Words::Node> refNode, System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCodeXE, Aspose::Words::StyleIdentifier styleIdentifier);

};

}
}
}
