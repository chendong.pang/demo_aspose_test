//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/Index/FieldIndexValidateResult.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

namespace Aspose { namespace Words { namespace Fields { class FieldCodeIndex; } } }
namespace Aspose { namespace Words { class Bookmark; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// \cond
class FieldIndexValidateResult : public System::Object
{
    typedef FieldIndexValidateResult ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:

    bool get_IsError();
    System::String get_ErrorMessage() const;
    System::SharedPtr<Aspose::Words::Fields::FieldCodeIndex> get_FieldCode() const;
    System::SharedPtr<Aspose::Words::Bookmark> get_Bookmark() const;

    FieldIndexValidateResult(System::String errorMessage, System::SharedPtr<Aspose::Words::Fields::FieldCodeIndex> fieldCode, System::SharedPtr<Aspose::Words::Bookmark> bookmark);
    FieldIndexValidateResult();

protected:

    System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String pr_ErrorMessage;
    System::SharedPtr<Aspose::Words::Fields::FieldCodeIndex> pr_FieldCode;
    System::SharedPtr<Aspose::Words::Bookmark> pr_Bookmark;

};/// \endcond

}
}
}
