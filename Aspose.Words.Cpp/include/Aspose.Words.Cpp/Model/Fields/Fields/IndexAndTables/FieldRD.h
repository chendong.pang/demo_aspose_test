//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/FieldRD.h
#pragma once

#include <system/string.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the RD field.
class ASPOSE_WORDS_SHARED_CLASS FieldRD : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldRD ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the name of the file to include when generating a table of contents, table of authorities, or index.
    ASPOSE_WORDS_SHARED_API System::String get_FileName();
    /// Sets the name of the file to include when generating a table of contents, table of authorities, or index.
    ASPOSE_WORDS_SHARED_API void set_FileName(System::String value);
    /// Gets whether the path is relative to the current document.
    ASPOSE_WORDS_SHARED_API bool get_IsPathRelative();
    /// Sets whether the path is relative to the current document.
    ASPOSE_WORDS_SHARED_API void set_IsPathRelative(bool value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

private:

    static const int32_t FileNameArgumentIndex;

    static const System::String& IsPathRelativeSwitch();

};

}
}
}
