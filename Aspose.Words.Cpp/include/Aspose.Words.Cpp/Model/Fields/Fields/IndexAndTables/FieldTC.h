//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/FieldTC.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/Toc/ITocEntry.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeTC; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Common { class NullableInt32; } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { class Node; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the TC field.
class ASPOSE_WORDS_SHARED_CLASS FieldTC FINAL : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider, public Aspose::Words::Fields::ITocEntry
{
    typedef FieldTC ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;
    typedef Aspose::Words::Fields::ITocEntry BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::FieldCodeTC;

public:

    /// Gets the text of the entry.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Sets the text of the entry.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);
    /// Gets a type identifier for this field (which is typically a letter).
    ASPOSE_WORDS_SHARED_API System::String get_TypeIdentifier();
    /// Sets a type identifier for this field (which is typically a letter).
    ASPOSE_WORDS_SHARED_API void set_TypeIdentifier(System::String value);
    /// Gets the level of the entry.
    ASPOSE_WORDS_SHARED_API System::String get_EntryLevel();
    /// Sets the level of the entry.
    ASPOSE_WORDS_SHARED_API void set_EntryLevel(System::String value);
    /// Gets whether page number in TOC should be omitted for this field.
    ASPOSE_WORDS_SHARED_API bool get_OmitPageNumber() override;
    /// Sets whether page number in TOC should be omitted for this field.
    ASPOSE_WORDS_SHARED_API void set_OmitPageNumber(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_Paragraph() override;
    ASPOSE_WORDS_SHARED_API int32_t get_Level() override;
    ASPOSE_WORDS_SHARED_API bool get_IsInFieldCode() override;

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> InsertBookmark(System::String bookmarkName) override;
    ASPOSE_WORDS_SHARED_API System::String GetDocumentOutlineTitle() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::NodeRange> GetLabelRange() override;

    ASPOSE_WORDS_SHARED_API FieldTC();

protected:

    System::SharedPtr<Aspose::Words::NodeRange> get_TextRange() const;
    System::SharedPtr<Aspose::Words::NodeRange> get_Range();
    bool get_HasEntryLevelSwitch();
    Aspose::Common::NullableInt32 get_EntryLevelAsInt32();

    ASPOSE_WORDS_SHARED_API void Initialize(System::SharedPtr<Aspose::Words::Fields::FieldStart> start, System::SharedPtr<Aspose::Words::Fields::FieldSeparator> separator, System::SharedPtr<Aspose::Words::Fields::FieldEnd> end) override;
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::NodeRange> pr_TextRange;

    void set_TextRange(System::SharedPtr<Aspose::Words::NodeRange> value);

    int32_t mLevel;
    bool mShowInToc;
    static const int32_t TextArgumentIndex;

    static const System::String& TypeIdentifierSwitch();
    static const System::String& EntryLevelSwitch();
    static const System::String& OmitPageNumberSwitch();
    void Parse();
    static void InsertNode(System::SharedPtr<Aspose::Words::Node> newChild, System::SharedPtr<Aspose::Words::Node> refChild, bool insertAfter);
    System::SharedPtr<Aspose::Words::NodeRange> GetEntryRange();

};

}
}
}
