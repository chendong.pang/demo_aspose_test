//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/FieldXE.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntry; } } }
namespace Aspose { namespace Words { namespace Fields { class IndexEntryPageNumberInfo; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeqDataProvider; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeXE; } } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanBookmark; } } } }
namespace Aspose { namespace Words { enum class NodeType; } }
namespace Aspose { namespace Words { class Node; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the XE field.
class ASPOSE_WORDS_SHARED_CLASS FieldXE : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldXE ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::IndexEntry;
    friend class Aspose::Words::Fields::IndexEntryPageNumberInfo;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldSeqDataProvider;
    friend class Aspose::Words::Fields::FieldCodeXE;

public:

    /// Gets the text of the entry.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Sets the text of the entry.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);
    /// Gets whether to apply bold formatting to the entry's page number.
    ASPOSE_WORDS_SHARED_API bool get_IsBold();
    /// Sets whether to apply bold formatting to the entry's page number.
    ASPOSE_WORDS_SHARED_API void set_IsBold(bool value);
    /// Gets an index entry type.
    ASPOSE_WORDS_SHARED_API System::String get_EntryType();
    /// Sets an index entry type.
    ASPOSE_WORDS_SHARED_API void set_EntryType(System::String value);
    /// Gets whether to apply italic formatting to the entry's page number.
    ASPOSE_WORDS_SHARED_API bool get_IsItalic();
    /// Sets whether to apply italic formatting to the entry's page number.
    ASPOSE_WORDS_SHARED_API void set_IsItalic(bool value);
    /// Gets the name of the bookmark that marks a range of pages that is inserted as the entry's page number.
    ASPOSE_WORDS_SHARED_API System::String get_PageRangeBookmarkName();
    /// Sets the name of the bookmark that marks a range of pages that is inserted as the entry's page number.
    ASPOSE_WORDS_SHARED_API void set_PageRangeBookmarkName(System::String value);
    /// Gets a value indicating whether a page range bookmark name is provided through the field's code.
    ASPOSE_WORDS_SHARED_API bool get_HasPageRangeBookmarkName();
    /// Gets text used in place of a page number.
    ASPOSE_WORDS_SHARED_API System::String get_PageNumberReplacement();
    /// Sets text used in place of a page number.
    ASPOSE_WORDS_SHARED_API void set_PageNumberReplacement(System::String value);
    /// Gets the yomi (first phonetic character for sorting indexes) for the index entry
    ASPOSE_WORDS_SHARED_API System::String get_Yomi();
    /// Sets the yomi (first phonetic character for sorting indexes) for the index entry
    ASPOSE_WORDS_SHARED_API void set_Yomi(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

protected:

    System::SharedPtr<Aspose::Words::NodeRange> get_TextRange();
    System::SharedPtr<Aspose::Words::NodeRange> get_PageNumberReplacementRange();

    bool ShouldBeProcessed();
    bool ShouldBeProcessed(System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCode);
    System::SharedPtr<Aspose::Words::Layout::Core::SpanBookmark> GetPageRangeSpanBookmark();
    System::SharedPtr<Aspose::Words::Layout::Core::SpanBookmark> GetPageRangeSpanBookmark(System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCode);
    System::String FormatPageNumber(int32_t pageNumber);

private:

    static const int32_t TextArgumentIndex;

    static const System::String& IsBoldSwitch();
    static const System::String& EntryTypeSwitch();
    static const System::String& IsItalicSwitch();
    static const System::String& PageRangeBookmarkNameSwitch();
    static const System::String& PageNumberReplacementSwitch();
    static const System::String& YomiSwitch();
    bool IsValid();
    bool HasValidText(System::SharedPtr<Aspose::Words::Fields::FieldCodeXE> fieldCode);
    static Aspose::Words::NodeType GetStoryAncestorType(System::SharedPtr<Aspose::Words::Node> node);

};

}
}
}
