//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/FieldToc.h
#pragma once

#include <system/string.h>
#include <system/idisposable.h>
#include <system/collections/ilist.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Nodes/Manipulation/NodeReplacer.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/IFieldFakeResultNodeModifier.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/IndexAndTables/Toc/ITocEntryExtractorOptions.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldIndexAndTablesUtil; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class LevelRange; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class Style; } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStage; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Fields { class ITocEntry; } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class INodeModifier; } }
namespace Aspose { namespace Words { enum class StyleIdentifier; } }
namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Math { class OfficeMath; } } }
namespace Aspose { namespace Collections { class StringToIntDictionary; } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the TOC field.
class ASPOSE_WORDS_SHARED_CLASS FieldToc : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider, public Aspose::Words::Fields::ITocEntryExtractorOptions
{
    typedef FieldToc ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;
    typedef Aspose::Words::Fields::ITocEntryExtractorOptions BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Fields::FieldIndexAndTablesUtil;
    friend class Aspose::Words::Document;

private:

    class FieldFakeResultNodeModifierAddapter : public Aspose::Words::Fields::IFieldFakeResultNodeModifier
    {
        typedef FieldFakeResultNodeModifierAddapter ThisType;
        typedef Aspose::Words::Fields::IFieldFakeResultNodeModifier BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        FieldFakeResultNodeModifierAddapter(System::SharedPtr<Aspose::Words::INodeModifier> attributeModifier);

        void Modify(System::SharedPtr<Aspose::Words::Node> node) override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::INodeModifier> mAttributeModifier;

    };

    class OfficeMathReplacer : public Aspose::Words::NodeReplacer
    {
        typedef OfficeMathReplacer ThisType;
        typedef Aspose::Words::NodeReplacer BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        OfficeMathReplacer();

    protected:

        void CollectClone(System::SharedPtr<Aspose::Words::Node> source, System::SharedPtr<Aspose::Words::Node> clone) override;
        void ReplaceCollectedNode(System::SharedPtr<Aspose::Words::Node> node) override;

    private:

        static void LinearizeOfficeMath(System::SharedPtr<Aspose::Words::Math::OfficeMath> officeMath);

    };

    class ParsedFieldToc FINAL : public System::IDisposable
    {
        typedef ParsedFieldToc ThisType;
        typedef System::IDisposable BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Fields::LevelRange> get_HeadingLevelRange() const;
        System::SharedPtr<Aspose::Words::Fields::LevelRange> get_EntryLevelRange() const;
        System::SharedPtr<Aspose::Words::Fields::LevelRange> get_PageNumberOmittingLevelRange() const;
        bool get_LevelRangeParsingErrorOccured() const;
        void set_LevelRangeParsingErrorOccured(bool value);
        int32_t get_EntryTypeCore() const;
        void set_EntryTypeCore(int32_t value);
        bool get_PreserveLineBreaks() const;
        void set_PreserveLineBreaks(bool value);
        System::SharedPtr<Aspose::Collections::StringToIntDictionary> get_CustomStylesToLevelsMap() const;

        ParsedFieldToc(System::SharedPtr<Aspose::Words::Fields::FieldToc> field);

        void Dispose() override;

    protected:

        virtual ~ParsedFieldToc();

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::Fields::LevelRange> pr_HeadingLevelRange;
        System::SharedPtr<Aspose::Words::Fields::LevelRange> pr_EntryLevelRange;
        System::SharedPtr<Aspose::Words::Fields::LevelRange> pr_PageNumberOmittingLevelRange;
        bool pr_LevelRangeParsingErrorOccured;
        int32_t pr_EntryTypeCore;
        bool pr_PreserveLineBreaks;
        System::SharedPtr<Aspose::Collections::StringToIntDictionary> pr_CustomStylesToLevelsMap;
        System::SharedPtr<Aspose::Words::Fields::FieldToc> mField;

    };

public:

    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() override;
    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() override;
    ASPOSE_WORDS_SHARED_API bool get_SkipTables() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::LevelRange> get_HeadingLevelRangeParsed() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::LevelRange> get_TocEntryLevelRange() override;
    ASPOSE_WORDS_SHARED_API bool get_IncludeTocEntryFields() override;
    ASPOSE_WORDS_SHARED_API int32_t get_EntryTypeCore() override;
    ASPOSE_WORDS_SHARED_API bool get_IsHeadingLevelRangeSpecified() override;
    ASPOSE_WORDS_SHARED_API bool get_IsPageNumberOmittingLevelRangeSpecified();
    ASPOSE_WORDS_SHARED_API bool get_IsEntryLevelRangeSpecified() override;
    ASPOSE_WORDS_SHARED_API bool get_AreCustomStylesSpecified() override;
    ASPOSE_WORDS_SHARED_API bool get_IsTableOfFigures() override;
    /// Gets the name of the bookmark that marks the portion of the document used to build the table.
    ASPOSE_WORDS_SHARED_API System::String get_BookmarkName();
    /// Sets the name of the bookmark that marks the portion of the document used to build the table.
    ASPOSE_WORDS_SHARED_API void set_BookmarkName(System::String value);
    /// Gets the name of the sequence identifier used when building a table of figures.
    ASPOSE_WORDS_SHARED_API System::String get_TableOfFiguresLabel() override;
    /// Sets the name of the sequence identifier used when building a table of figures.
    ASPOSE_WORDS_SHARED_API void set_TableOfFiguresLabel(System::String value);
    /// Gets the name of the sequence identifier used when building a table of figures that does not include caption's
    /// label and number.
    ASPOSE_WORDS_SHARED_API System::String get_CaptionlessTableOfFiguresLabel() override;
    /// Sets the name of the sequence identifier used when building a table of figures that does not include caption's
    /// label and number.
    ASPOSE_WORDS_SHARED_API void set_CaptionlessTableOfFiguresLabel(System::String value);
    ASPOSE_WORDS_SHARED_API bool get_IsBookmarkRangeSpecified() override;
    /// Gets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API System::String get_SequenceSeparator();
    /// Sets the character sequence that is used to separate sequence numbers and page numbers.
    ASPOSE_WORDS_SHARED_API void set_SequenceSeparator(System::String value);
    /// Gets a string that should match type identifiers of TC fields being included.
    ASPOSE_WORDS_SHARED_API System::String get_EntryIdentifier();
    /// Sets a string that should match type identifiers of TC fields being included.
    ASPOSE_WORDS_SHARED_API void set_EntryIdentifier(System::String value);
    /// Gets whether to make the table of contents entries hyperlinks.
    ASPOSE_WORDS_SHARED_API bool get_InsertHyperlinks();
    /// Sets whether to make the table of contents entries hyperlinks.
    ASPOSE_WORDS_SHARED_API void set_InsertHyperlinks(bool value);
    /// Gets a range of levels of the table of contents entries to be included.
    ASPOSE_WORDS_SHARED_API System::String get_EntryLevelRange();
    /// Sets a range of levels of the table of contents entries to be included.
    ASPOSE_WORDS_SHARED_API void set_EntryLevelRange(System::String value);
    /// Gets a range of levels of the table of contents entries from which to omits page numbers.
    ASPOSE_WORDS_SHARED_API System::String get_PageNumberOmittingLevelRange();
    /// Sets a range of levels of the table of contents entries from which to omits page numbers.
    ASPOSE_WORDS_SHARED_API void set_PageNumberOmittingLevelRange(System::String value);
    /// Gets a range of heading levels to include.
    ASPOSE_WORDS_SHARED_API System::String get_HeadingLevelRange();
    /// Sets a range of heading levels to include.
    ASPOSE_WORDS_SHARED_API void set_HeadingLevelRange(System::String value);
    /// Gets a sequence of characters that separate an entry and its page number.
    ASPOSE_WORDS_SHARED_API System::String get_EntrySeparator();
    /// Sets a sequence of characters that separate an entry and its page number.
    ASPOSE_WORDS_SHARED_API void set_EntrySeparator(System::String value);
    /// Gets the identifier of a sequence for which a prefix should be added to the entry's page number.
    ASPOSE_WORDS_SHARED_API System::String get_PrefixedSequenceIdentifier();
    /// Sets the identifier of a sequence for which a prefix should be added to the entry's page number.
    ASPOSE_WORDS_SHARED_API void set_PrefixedSequenceIdentifier(System::String value);
    /// Gets a list of styles other than the built-in heading styles to include in the table of contents.
    ASPOSE_WORDS_SHARED_API System::String get_CustomStyles();
    /// Sets a list of styles other than the built-in heading styles to include in the table of contents.
    ASPOSE_WORDS_SHARED_API void set_CustomStyles(System::String value);
    /// Gets whether to use the applied paragraph outline level.
    ASPOSE_WORDS_SHARED_API bool get_UseParagraphOutlineLevel() override;
    /// Sets whether to use the applied paragraph outline level.
    ASPOSE_WORDS_SHARED_API void set_UseParagraphOutlineLevel(bool value);
    /// Gets whether to preserve tab entries within table entries.
    ASPOSE_WORDS_SHARED_API bool get_PreserveTabs();
    /// Sets whether to preserve tab entries within table entries.
    ASPOSE_WORDS_SHARED_API void set_PreserveTabs(bool value);
    /// Gets whether to preserve newline characters within table entries.
    ASPOSE_WORDS_SHARED_API bool get_PreserveLineBreaks();
    /// Sets whether to preserve newline characters within table entries.
    ASPOSE_WORDS_SHARED_API void set_PreserveLineBreaks(bool value);
    /// Gets whether to hide tab leader and page numbers in Web layout view.
    ASPOSE_WORDS_SHARED_API bool get_HideInWebLayout();
    /// Sets whether to hide tab leader and page numbers in Web layout view.
    ASPOSE_WORDS_SHARED_API void set_HideInWebLayout(bool value);

    /// Updates the page numbers for items in this table of contents.
    /// 
    /// @return True if the operation is successful. If any of the related TOC bookmarks was removed, false will be returned.
    ASPOSE_WORDS_SHARED_API bool UpdatePageNumbers();
    ASPOSE_WORDS_SHARED_API int32_t GetLevelForCustomStyle(System::SharedPtr<Aspose::Words::Paragraph> paragraph, System::SharedPtr<Aspose::Words::Style> style) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Bookmark> GetRangeBookmark() override;

    ASPOSE_WORDS_SHARED_API FieldToc();

protected:

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStage GetUpdateStage() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API void BeforeUnlink() override;
    System::String GetUniqueTocBookmarkName(System::ArrayPtr<System::String> bookmarkNames);
    static Aspose::Words::StyleIdentifier GetStyleIdentifierForLevel(int32_t entryLevel, bool isTableOfFigures);
    static void RevertClonedHyperlinkStyle(System::SharedPtr<Aspose::Words::Node> parent);

    virtual ASPOSE_WORDS_SHARED_API ~FieldToc();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> mParsedFieldToc;
    bool mIsExtraParaWasAdded;
    bool mIsExtraParaAlreadyAdded;

    static const System::String& CaptionlessTableOfFiguresLabelSwitch();
    static const System::String& BookmarkNameSwitch();
    static const System::String& TableOfFiguresLabelSwitch();
    static const System::String& SequenceSeparatorSwitch();
    static const System::String& EntryIdentifierSwitch();
    static const System::String& InsertHyperlinksSwitch();
    static const System::String& EntryLevelRangeSwitch();
    static const System::String& PageNumberOmittingLevelRangeSwitch();
    static const System::String& HeadingLevelRangeSwitch();
    static const System::String& EntrySeparatorSwitch();
    static const System::String& PrefixedSequenceIdentifierSwitch();
    static const System::String& CustomStylesSwitch();
    static const System::String& UseParagraphOutlineLevelSwitch();
    static const System::String& PreserveTabsSwitch();
    static const System::String& PreserveLineBreaksSwitch();
    static const System::String& HideInWebLayoutSwitch();
    static System::SharedPtr<System::IDisposable> Parse(System::SharedPtr<Aspose::Words::Fields::FieldToc> field);
    static void SetLevelRangeFromSwitchArgument(System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> result, System::SharedPtr<Aspose::Words::Fields::Field> field, System::SharedPtr<Aspose::Words::Fields::LevelRange> levelRange, System::String switchName);
    static void ParseCustomStyles(System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> result, System::SharedPtr<Aspose::Words::Fields::FieldToc> field);
    static void ParseCustomStyles(System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> result, System::SharedPtr<Aspose::Words::Fields::Field> field, System::ArrayPtr<System::String> customStylesParts);
    static void MapCustomStyleToLevel(System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> result, System::String styleName, System::String invariantStyleName, int32_t styleLevel);
    static void MapCustomStyleToLevel(System::SharedPtr<Aspose::Words::Fields::FieldToc::ParsedFieldToc> result, System::String styleName, int32_t styleLevel);
    System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateParsed();
    void RemoveOldResultBookmarks();
    static System::String GetFieldBookmarkName(System::SharedPtr<Aspose::Words::Fields::Field> field);
    bool BuildTocEntries(System::SharedPtr<Aspose::Words::DocumentBuilder> builder, System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::ITocEntry>>> entries);
    System::ArrayPtr<System::String> GetDocumentBookmarkNames();
    bool BuildTocEntry(System::SharedPtr<Aspose::Words::DocumentBuilder> builder, System::ArrayPtr<System::String> bookmarkNames, System::SharedPtr<Aspose::Words::Fields::ITocEntry> entry);
    void InsertExtraParagraph(System::SharedPtr<Aspose::Words::DocumentBuilder> builder);
    void RemoveExtraParagraph();
    System::SharedPtr<Aspose::Words::Node> InsertHyperlink(System::SharedPtr<Aspose::Words::Node> refNode, System::SharedPtr<Aspose::Words::DocumentBuilder> builder, System::String sourceEntryBookmarkName);
    void BuildTocEntry(System::SharedPtr<Aspose::Words::Fields::ITocEntry> entry, System::SharedPtr<Aspose::Words::NodeRange> bookmarkRange, System::SharedPtr<Aspose::Words::Node> refNode);
    void BuildEntryPageNumber(System::SharedPtr<Aspose::Words::DocumentBuilder> builder, System::SharedPtr<Aspose::Words::Fields::ITocEntry> entry, System::String sourceEntryBookmarkName);
    System::String GetNoEntriesErrorMessage();
    System::SharedPtr<Aspose::Words::INodeModifier> GetAttributeModifier(System::SharedPtr<Aspose::Words::Fields::ITocEntry> entry);
    void RevertClonedHyperlinkStyle();

};

}
}
}
