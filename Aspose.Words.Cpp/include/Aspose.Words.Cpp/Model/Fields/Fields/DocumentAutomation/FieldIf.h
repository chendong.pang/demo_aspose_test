//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentAutomation/FieldIf.h
#pragma once

#include <system/text/regularexpressions/regex.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/Fields/MailMerge/IMergeFieldSurrogate.h"
#include "Aspose.Words.Cpp/Model/Fields/Fields/DocumentAutomation/FieldIfComparisonResult.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/Model/Fields/Expressions/IComparisonExpression.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldStart; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeparator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldEnd; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldArgument; } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class Constant; } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateAction; } } }
namespace Aspose { namespace Words { namespace Fields { enum class FieldUpdateStrategy; } } }
namespace Aspose { namespace Words { namespace Fields { class IFieldArgument; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldCode; } } }

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the IF field.
/// 
/// Compares the values designated by the expressions <see cref="Aspose::Words::Fields::FieldIf::get_LeftExpression">LeftExpression</see> and <see cref="Aspose::Words::Fields::FieldIf::get_RightExpression">RightExpression</see>
/// in comparison using the operator designated by <see cref="Aspose::Words::Fields::FieldIf::get_ComparisonOperator">ComparisonOperator</see>.
/// 
/// A field in the following format will be used as a mail merge source: { IF 0 = 0 "{PatientsNameFML}" "" \\* MERGEFORMAT }
class ASPOSE_WORDS_SHARED_CLASS FieldIf : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IMergeFieldSurrogate
{
    typedef FieldIf ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IMergeFieldSurrogate BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

private:

    class FieldIfComparisonExpression : public Aspose::Words::Fields::Expressions::IComparisonExpression
    {
        typedef FieldIfComparisonExpression ThisType;
        typedef Aspose::Words::Fields::Expressions::IComparisonExpression BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    private:

        class FieldIfArgumentsBundle : public System::Object
        {
            typedef FieldIfArgumentsBundle ThisType;
            typedef System::Object BaseType;

            typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
            RTTI_INFO_DECL();

        public:

            System::String get_LeftExpression() const;
            System::String get_ComparisonOperator() const;
            System::String get_RightExpression() const;
            System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_TrueResultArgument() const;
            System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_FalseResultArgument() const;

            FieldIfArgumentsBundle(System::String leftExpression, System::String comparisonOperator, System::String rightExpression, System::SharedPtr<Aspose::Words::Fields::FieldArgument> trueResultArgument, System::SharedPtr<Aspose::Words::Fields::FieldArgument> falseResultArgument);

        protected:

            System::Object::shared_members_type GetSharedMembers() override;

        private:

            System::String pr_LeftExpression;
            System::String pr_ComparisonOperator;
            System::String pr_RightExpression;
            System::SharedPtr<Aspose::Words::Fields::FieldArgument> pr_TrueResultArgument;
            System::SharedPtr<Aspose::Words::Fields::FieldArgument> pr_FalseResultArgument;

        };

    public:

        System::String get_LeftExpression() override;
        System::String get_ComparisonOperator() override;
        System::String get_RightExpression() override;
        System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_TrueResultArgument();
        System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_FalseResultArgument();

        FieldIfComparisonExpression(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode);

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression::FieldIfArgumentsBundle> mArguments;

        static System::SharedPtr<System::Text::RegularExpressions::Regex>& gMatchNumber();
        static System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression::FieldIfArgumentsBundle> TryMissedLeftExpression(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode, System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression::FieldIfArgumentsBundle> defaultArguments);
        static System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression::FieldIfArgumentsBundle> TryMissedOperatorAndRightExpression(System::SharedPtr<Aspose::Words::Fields::FieldCode> fieldCode, System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression::FieldIfArgumentsBundle> defaultArguments);

    };

public:

    /// Gets the node that represents the start of the field.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldStart> get_Start() override;
    /// Gets the node that represents the field separator. Can be null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldSeparator> get_Separator() override;
    /// Gets the node that represents the field end.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldEnd> get_End() override;
    /// Gets the left part of the comparison expression.
    ASPOSE_WORDS_SHARED_API System::String get_LeftExpression();
    /// Sets the left part of the comparison expression.
    ASPOSE_WORDS_SHARED_API void set_LeftExpression(System::String value);
    /// Gets the comparison operator.
    ASPOSE_WORDS_SHARED_API System::String get_ComparisonOperator();
    /// Sets the comparison operator.
    ASPOSE_WORDS_SHARED_API void set_ComparisonOperator(System::String value);
    /// Gets the right part of the comparison expression.
    ASPOSE_WORDS_SHARED_API System::String get_RightExpression();
    /// Sets the right part of the comparison expression.
    ASPOSE_WORDS_SHARED_API void set_RightExpression(System::String value);
    /// Gets the text displayed if the comparison expression is true.
    ASPOSE_WORDS_SHARED_API System::String get_TrueText();
    /// Sets the text displayed if the comparison expression is true.
    ASPOSE_WORDS_SHARED_API void set_TrueText(System::String value);
    /// Gets the text displayed if the comparison expression is false.
    ASPOSE_WORDS_SHARED_API System::String get_FalseText();
    /// Sets the text displayed if the comparison expression is false.
    ASPOSE_WORDS_SHARED_API void set_FalseText(System::String value);

    ASPOSE_WORDS_SHARED_API System::String GetMergeFieldName() override;
    ASPOSE_WORDS_SHARED_API bool CanWorkAsMergeField() override;
    ASPOSE_WORDS_SHARED_API bool IsMergeValueRequired() override;
    /// Evaluates the condition.
    /// 
    /// @return A <see cref="Aspose::Words::Fields::FieldIfComparisonResult">FieldIfComparisonResult</see> value that represents the result of the condition evaluation.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldIfComparisonResult EvaluateCondition();

protected:

    ASPOSE_WORDS_SHARED_API bool get_SupportsConditionalUpdate() override;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldUpdateAction> UpdateCore() override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldUpdateStrategy GetChildFieldsUpdateStrategyInArgument(System::SharedPtr<Aspose::Words::Fields::IFieldArgument> argument) override;
    ASPOSE_WORDS_SHARED_API void ParseFieldCode() override;

    virtual ASPOSE_WORDS_SHARED_API ~FieldIf();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_ResultArgument();
    System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_TrueResultArgument();
    System::SharedPtr<Aspose::Words::Fields::FieldArgument> get_FalseResultArgument();

    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> mComparisonResult;
    static const int32_t TrueResultArgumentIndex;
    static const int32_t FalseResultArgumentIndex;
    static const int32_t LeftExpressionArgumentIndex;
    static const int32_t ComparisonOperatorArgumentIndex;
    static const int32_t RightExpressionArgumentIndex;
    static const int32_t TrueTextArgumentIndex;
    static const int32_t FalseTextArgumentIndex;

    void UpdateComparisonResult();
    System::SharedPtr<Aspose::Words::Fields::Expressions::Constant> EvaluateComparisonResult();
    System::SharedPtr<Aspose::Words::Fields::FieldIf::FieldIfComparisonExpression> GetComparisonExpression();

};

}
}
}
