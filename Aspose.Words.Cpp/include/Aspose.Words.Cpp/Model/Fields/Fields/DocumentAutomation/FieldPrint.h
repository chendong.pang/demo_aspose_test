//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/Fields/DocumentAutomation/FieldPrint.h
#pragma once

#include <system/string.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Fields/FieldCode/FieldSwitchType.h"
#include "Aspose.Words.Cpp/Model/Fields/FieldCode/IFieldCodeTokenInfoProvider.h"
#include "Aspose.Words.Cpp/Model/Fields/Field.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Implements the PRINT field.
class ASPOSE_WORDS_SHARED_CLASS FieldPrint : public Aspose::Words::Fields::Field, public Aspose::Words::Fields::IFieldCodeTokenInfoProvider
{
    typedef FieldPrint ThisType;
    typedef Aspose::Words::Fields::Field BaseType;
    typedef Aspose::Words::Fields::IFieldCodeTokenInfoProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Gets the printer-specific control code characters or PostScript instructions.
    ASPOSE_WORDS_SHARED_API System::String get_PrinterInstructions();
    /// Sets the printer-specific control code characters or PostScript instructions.
    ASPOSE_WORDS_SHARED_API void set_PrinterInstructions(System::String value);
    /// Gets the drawing rectangle that the PostScript instructions operate on.
    ASPOSE_WORDS_SHARED_API System::String get_PostScriptGroup();
    /// Sets the drawing rectangle that the PostScript instructions operate on.
    ASPOSE_WORDS_SHARED_API void set_PostScriptGroup(System::String value);

    ASPOSE_WORDS_SHARED_API Aspose::Words::Fields::FieldSwitchType GetSwitchType(System::String switchName) override;

private:

    static const int32_t PrinterInstructionsArgumentIndex;

    static const System::String& PostScriptGroupSwitch();

};

}
}
}
