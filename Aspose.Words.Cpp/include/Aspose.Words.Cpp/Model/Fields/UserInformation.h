//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Fields/UserInformation.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <mutex>
#include <memory>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace Fields {

/// Specifies information about the user.
class ASPOSE_WORDS_SHARED_CLASS UserInformation : public System::Object
{
    typedef UserInformation ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:

    /// Gets the user's name.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Sets the user's name.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Gets the user's initials.
    ASPOSE_WORDS_SHARED_API System::String get_Initials() const;
    /// Sets the user's initials.
    ASPOSE_WORDS_SHARED_API void set_Initials(System::String value);
    /// Gets the user's postal address.
    ASPOSE_WORDS_SHARED_API System::String get_Address() const;
    /// Sets the user's postal address.
    ASPOSE_WORDS_SHARED_API void set_Address(System::String value);
    /// Default user information.
    static ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::UserInformation> get_DefaultUser();

    ASPOSE_WORDS_SHARED_API UserInformation();

private:

    System::String pr_Name;
    System::String pr_Initials;
    System::String pr_Address;
    static System::SharedPtr<Aspose::Words::Fields::UserInformation> pr_DefaultUser;

    static void __StaticConstructor__();

};

}
}
}
