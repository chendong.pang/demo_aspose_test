//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Core/Dml/Themes/ThemeColor.h
#pragma once

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Core {

namespace Dml {

namespace Themes {

/// ThemeColor enumeration.
enum class ThemeColor
{
    Dark1,
    Light1,
    Dark2,
    Light2,
    Accent1,
    Accent2,
    Accent3,
    Accent4,
    Accent5,
    Accent6,
    Hyperlink,
    FollowedHyperlink,
    Text1,
    Text2,
    Background1,
    Background2
};

}
}
}
}
}
}
