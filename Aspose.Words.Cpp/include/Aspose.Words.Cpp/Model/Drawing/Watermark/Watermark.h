//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Watermark/Watermark.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <drawing/size_f.h>
#include <drawing/image.h>

#include "Aspose.Words.Cpp/Model/Drawing/Watermark/WatermarkType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class IWatermarkProvider; } }
namespace Aspose { namespace Words { class TextWatermarkOptions; } }
namespace Aspose { namespace Words { class ImageWatermarkOptions; } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }

namespace Aspose {

namespace Words {

/// Represents class to work with document watermark.
class ASPOSE_WORDS_SHARED_CLASS Watermark FINAL : public System::Object
{
    typedef Watermark ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Document;

public:

    /// Gets the watermark type.
    ASPOSE_WORDS_SHARED_API Aspose::Words::WatermarkType get_Type();

    /// Adds Text watermark into the document.
    /// 
    /// @param text Text that is displayed as a watermark.
    /// 
    /// @exception System::ArgumentOutOfRangeException Throws when the text length is out of range or the text contains only whitespaces.
    /// @exception System::ArgumentNullException Throws when the text is null.
    ASPOSE_WORDS_SHARED_API void SetText(System::String text);
    /// Adds Text watermark into the document.
    /// 
    /// @param text Text that is displayed as a watermark.
    /// @param options Defines additional options for the text watermark.
    /// 
    /// @exception System::ArgumentOutOfRangeException Throws when the text length is out of range or the text contain only whitespaces.
    /// @exception System::ArgumentNullException Throws when the text is null.
    ASPOSE_WORDS_SHARED_API void SetText(System::String text, System::SharedPtr<Aspose::Words::TextWatermarkOptions> options);
    /// Adds Image watermark into the document.
    /// 
    /// @param image Image that is displayed as a watermark.
    /// 
    /// @exception System::ArgumentNullException Throws when the image is null.
    ASPOSE_WORDS_SHARED_API void SetImage(System::SharedPtr<System::Drawing::Image> image);
    /// Adds Image watermark into the document.
    /// 
    /// @param image Image that is displayed as a watermark.
    /// @param options Defines additional options for the image watermark.
    /// 
    /// @exception System::ArgumentNullException Throws when the image is null.
    ASPOSE_WORDS_SHARED_API void SetImage(System::SharedPtr<System::Drawing::Image> image, System::SharedPtr<Aspose::Words::ImageWatermarkOptions> options);
    /// Removes the watermark.
    ASPOSE_WORDS_SHARED_API void Remove();

protected:

    Watermark(System::SharedPtr<Aspose::Words::Document> doc, System::SharedPtr<Aspose::Words::IWatermarkProvider> watermarkProvider);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Document> mDoc;
    System::SharedPtr<Aspose::Words::IWatermarkProvider> mWatermarkProvider;

    void Add(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);
    void ValidateText(System::String text);
    void ValidateImage(System::SharedPtr<System::Drawing::Image> image);
    System::SharedPtr<Aspose::Words::Drawing::Shape> CreateWatermark(System::SharedPtr<System::Drawing::Image> image, System::SharedPtr<Aspose::Words::ImageWatermarkOptions> options, System::SharedPtr<Aspose::Words::Document> doc);
    void SetImageSize(System::SharedPtr<Aspose::Words::Drawing::Shape> watermark, System::SharedPtr<Aspose::Words::ImageWatermarkOptions> options, System::SharedPtr<Aspose::Words::Document> doc);
    System::SharedPtr<Aspose::Words::Drawing::Shape> CreateWatermark(System::String text, System::SharedPtr<Aspose::Words::TextWatermarkOptions> options, System::SharedPtr<Aspose::Words::Document> doc);
    void SetTextSize(System::SharedPtr<Aspose::Words::Drawing::Shape> watermark, System::SharedPtr<Aspose::Words::TextWatermarkOptions> options, System::SharedPtr<Aspose::Words::Document> doc);
    System::Drawing::SizeF GetTextAutoSize(System::Drawing::SizeF textSize, System::SharedPtr<Aspose::Words::TextWatermarkOptions> options, System::SharedPtr<Aspose::Words::Document> doc);
    void SetGeneralAttributes(System::SharedPtr<Aspose::Words::Drawing::Shape> watermark, System::String name);

};

}
}
