//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Ole/OleFormat.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/stream.h>
#include <system/io/memory_stream.h>
#include <system/guid.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Comparison { class ShapeComparer; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class VmlNode; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverterUtil; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { class ShapeFieldRemover; } }
namespace Aspose { namespace Words { namespace Fields { class FieldCodeLink; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeIdGenerator; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtOleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtBinaryObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlOleReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfOleHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfOleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class OleLinkType; } } } }
namespace Aspose { namespace Words { namespace Drawing { class OlePackage; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class EmbeddedObjectBase; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class OleObject; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class OoxmlObject; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Ole { class OleControl; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Ole { class MetafileIcon; } } } }
namespace Aspose { namespace Words { class IShapeAttrSource; } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Provides access to the data of an OLE object or ActiveX control.
/// 
/// Use the <see cref="Aspose::Words::Drawing::Shape::get_OleFormat">OleFormat</see> property to access the data of an OLE object.
/// You do not create instances of the <see cref="Aspose::Words::Drawing::OleFormat">OleFormat</see> class directly.
/// 
/// @sa Aspose::Words::Drawing::Shape::get_OleFormat
class ASPOSE_WORDS_SHARED_CLASS OleFormat : public System::Object
{
    typedef OleFormat ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Comparison::ShapeComparer;
    friend class Aspose::Words::Drawing::Core::VmlNode;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverterUtil;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::ShapeFieldRemover;
    friend class Aspose::Words::Fields::FieldCodeLink;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::ShapeIdGenerator;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::RW::Odt::Writer::OdtOleWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtObjectReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtBinaryObjectReader;
    friend class Aspose::Words::RW::Vml::VmlOleReader;
    friend class Aspose::Words::RW::Doc::Writer::ShapeWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfOleHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfOleWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Gets icon caption of OLE object.
    /// In case of OLE object is not embedded as icon or caption couldn't be retrieved returns empty string.
    ASPOSE_WORDS_SHARED_API System::String get_IconCaption();
    /// Gets the file extension suggested for the current embedded object if you want to save it into a file.
    ASPOSE_WORDS_SHARED_API System::String get_SuggestedExtension();
    /// Gets the file name suggested for the current embedded object if you want to save it into a file.
    ASPOSE_WORDS_SHARED_API System::String get_SuggestedFileName();
    /// Gets or sets the ProgID of the OLE object.
    /// 
    /// The ProgID property is not always present in Microsoft Word documents and cannot be relied upon.
    /// 
    /// Cannot be null.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_ProgId();
    /// Setter for Aspose::Words::Drawing::OleFormat::get_ProgId
    ASPOSE_WORDS_SHARED_API void set_ProgId(System::String value);
    /// Returns true if the OLE object is linked (when <see cref="Aspose::Words::Drawing::OleFormat::get_SourceFullName">SourceFullName</see> is specified).
    ASPOSE_WORDS_SHARED_API bool get_IsLink();
    /// Gets or sets the path and name of the source file for the linked OLE object.
    /// 
    /// The default value is an empty string.
    /// 
    /// If <see cref="Aspose::Words::Drawing::OleFormat::get_SourceFullName">SourceFullName</see> is not an empty string, the OLE object is linked.
    ASPOSE_WORDS_SHARED_API System::String get_SourceFullName();
    /// Setter for Aspose::Words::Drawing::OleFormat::get_SourceFullName
    ASPOSE_WORDS_SHARED_API void set_SourceFullName(System::String value);
    /// Gets or sets a string that is used to identify the portion of the source file that is being linked.
    /// 
    /// The default value is an empty string.
    /// 
    /// For example, if the source file is a Microsoft Excel workbook, the <see cref="Aspose::Words::Drawing::OleFormat::get_SourceItem">SourceItem</see>
    /// property might return "Workbook1!R3C1:R4C2" if the OLE object contains only a few cells from
    /// the worksheet.
    ASPOSE_WORDS_SHARED_API System::String get_SourceItem();
    /// Setter for Aspose::Words::Drawing::OleFormat::get_SourceItem
    ASPOSE_WORDS_SHARED_API void set_SourceItem(System::String value);
    /// Specifies whether the link to the OLE object is automatically updated or not in Microsoft Word.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_AutoUpdate();
    /// Specifies whether the link to the OLE object is automatically updated or not in Microsoft Word.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_AutoUpdate(bool value);
    /// Gets the draw aspect of the OLE object. When <b>true</b>, the OLE object is displayed as an icon.
    /// When <b>false</b>, the OLE object is displayed as content.
    /// 
    /// Aspose.Words does not allow to set this property to avoid confusion. If you were able to change
    /// the draw aspect in Aspose.Words, Microsoft Word would still display the OLE object in its original
    /// draw aspect until you edit or update the OLE object in Microsoft Word.
    ASPOSE_WORDS_SHARED_API bool get_OleIcon();
    /// Specifies whether the link to the OLE object is locked from updates.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_IsLocked();
    /// Specifies whether the link to the OLE object is locked from updates.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_IsLocked(bool value);
    /// Gets the CLSID of the OLE object.
    ASPOSE_WORDS_SHARED_API System::Guid get_Clsid();
    /// Provide access to <see cref="Aspose::Words::Drawing::OlePackage">OlePackage</see> if OLE object is an OLE Package.
    /// Returns null otherwise.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::OlePackage> get_OlePackage();
    /// Gets <see cref="Aspose::Words::Drawing::OleFormat::get_OleControl">OleControl</see> objects if this OLE object is an ActiveX control. Otherwise this property is null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Ole::OleControl> get_OleControl();

    /// Saves the data of the embedded object into the specified stream.
    /// 
    /// It is the responsibility of the caller to dispose the stream.
    /// 
    /// @param stream Where to save the object data.
    /// 
    /// @exception System::InvalidOperationException Throws if you attempt to save a linked object.
    ASPOSE_WORDS_SHARED_API void Save(System::SharedPtr<System::IO::Stream> stream);
    /// Saves the data of the embedded object into a file with the specified name.
    /// 
    /// @param fileName Name of the file to save the OLE object data.
    /// 
    /// @exception System::InvalidOperationException Throws if you attempt to save a linked object.
    ASPOSE_WORDS_SHARED_API void Save(System::String fileName);
    /// Gets OLE object data entry.
    /// 
    /// @param oleEntryName Case-sensitive name of the OLE data stream.
    /// 
    /// @return An OLE data stream or null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::IO::MemoryStream> GetOleEntry(System::String oleEntryName);
    /// Gets OLE object raw data.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> GetRawData();

protected:

    bool get_IsLinkNoData();
    bool get_NeedEmbeddedPart();
    System::SharedPtr<Aspose::Words::Drawing::Core::EmbeddedObjectBase> get_EmbeddedObject();
    void set_EmbeddedObject(System::SharedPtr<Aspose::Words::Drawing::Core::EmbeddedObjectBase> value);
    System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> get_OleObject();
    System::SharedPtr<Aspose::Words::Drawing::Core::OoxmlObject> get_OoxmlObject();
    int32_t get_OleTxid();
    void set_OleTxid(int32_t value);
    Aspose::Words::Drawing::Core::OleLinkType get_OleLinkType();
    void set_OleLinkType(Aspose::Words::Drawing::Core::OleLinkType value);
    int32_t get_FormatUpdateType();
    void set_FormatUpdateType(int32_t value);
    System::SharedPtr<Aspose::Words::RW::Ole::MetafileIcon> get_MetafileIcon();

    OleFormat(System::SharedPtr<Aspose::Words::IShapeAttrSource> parent);

    void SetOleIcon(bool isIcon);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::IShapeAttrSource> mParent;
    System::SharedPtr<Aspose::Words::RW::Ole::MetafileIcon> mMetafileIconCache;
    System::SharedPtr<Aspose::Words::Drawing::OlePackage> mOlePackageCache;
    System::SharedPtr<Aspose::Words::Drawing::Ole::OleControl> mOleControlCache;

    void UpdateDrawAspectFromOleObject();
    System::SharedPtr<System::Object> FetchAttr(int32_t key);
    void SetAttr(int32_t key, System::SharedPtr<System::Object> value);

};

}
}
}
