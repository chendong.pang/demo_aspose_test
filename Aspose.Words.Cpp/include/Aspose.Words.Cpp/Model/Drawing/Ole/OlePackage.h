//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Ole/OlePackage.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/binary_writer.h>
#include <system/io/binary_reader.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { class OleFormat; } } }
namespace Aspose { namespace Words { namespace RW { namespace Ole { class OleUtil; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class OleObject; } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Allows to access OLE Package properties.
class ASPOSE_WORDS_SHARED_CLASS OlePackage : public System::Object
{
    typedef OlePackage ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::OleFormat;
    friend class Aspose::Words::RW::Ole::OleUtil;

public:

    /// Gets OLE Package file name.
    ASPOSE_WORDS_SHARED_API System::String get_FileName() const;
    /// Sets OLE Package file name.
    ASPOSE_WORDS_SHARED_API void set_FileName(System::String value);
    /// Gets OLE Package display name.
    ASPOSE_WORDS_SHARED_API System::String get_DisplayName() const;
    /// Sets OLE Package display name.
    ASPOSE_WORDS_SHARED_API void set_DisplayName(System::String value);

protected:

    bool get_IsLink();

    System::String TempFileName;
    System::ArrayPtr<uint8_t> Data;
    static const int32_t DefaultCodePage;

    OlePackage();
    OlePackage(System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> oleObject);

    void Read(System::SharedPtr<System::IO::BinaryReader> reader);
    void Write(System::SharedPtr<System::IO::BinaryWriter> writer);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mFileName;
    System::String mDisplayName;
    System::SharedPtr<Aspose::Words::Drawing::Core::OleObject> mOleObject;

    void ReadUnicodeProperties(System::SharedPtr<System::IO::BinaryReader> reader);
    static System::String ReadUnicodeProperty(System::SharedPtr<System::IO::BinaryReader> reader);
    void UpdateOleObjectData();
    static System::String ReadNullTerminatedString(System::SharedPtr<System::IO::BinaryReader> reader);
    static void WriteNullTerminatedString(System::SharedPtr<System::IO::BinaryWriter> writer, System::String text);
    static void WriteLengthPrefixedUnicodeString(System::SharedPtr<System::IO::BinaryWriter> writer, System::String text);
    static System::String ReadLengthPrefixedUnicodeString(System::SharedPtr<System::IO::BinaryReader> reader, int32_t len);

};

}
}
}
