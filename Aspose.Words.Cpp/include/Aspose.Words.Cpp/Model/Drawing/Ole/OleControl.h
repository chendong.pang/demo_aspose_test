//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Ole/OleControl.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Ole { namespace Core { class HtmlOleControl; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Ole { class OleControlFactory; } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeWriter; } } } } }
namespace Aspose { namespace Ss { class MemoryStorage; } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Ole {

/// Represents OLE ActiveX control.
class ASPOSE_WORDS_SHARED_CLASS OleControl : public System::Object
{
    typedef OleControl ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Ole::Core::HtmlOleControl;
    friend class Aspose::Words::Drawing::Ole::OleControlFactory;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeWriter;

public:

    /// Returns true if the control is a <see cref="Aspose::Words::Drawing::Ole::Forms2OleControl">Forms2OleControl</see>.
    virtual ASPOSE_WORDS_SHARED_API bool get_IsForms2OleControl();
    /// Gets name of the ActiveX control.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;

protected:

    virtual ASPOSE_WORDS_SHARED_API bool get_IsHtmlOleControl();

    ASPOSE_WORDS_SHARED_API OleControl();
    ASPOSE_WORDS_SHARED_API OleControl(System::String name);
    OleControl(System::SharedPtr<Aspose::Ss::MemoryStorage> storage);

    static bool IsOleControl(System::SharedPtr<Aspose::Ss::MemoryStorage> storage);

private:

    System::String mName;

};

}
}
}
}
