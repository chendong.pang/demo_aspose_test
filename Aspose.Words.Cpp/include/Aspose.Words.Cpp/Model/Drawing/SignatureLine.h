//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/SignatureLine.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/guid.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Core { class SignatureLineRenderingHelper; } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { class DigitalSignature; } }
namespace Aspose { namespace Words { class SignatureLineOptions; } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Provides access to signature line properties.
class ASPOSE_WORDS_SHARED_CLASS SignatureLine : public System::Object
{
    typedef SignatureLine ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Core::SignatureLineRenderingHelper;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;

public:

    /// Gets suggested signer of the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Signer();
    /// Sets suggested signer of the signature line.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Signer(System::String value);
    /// Gets suggested signer's title (for example, Manager).
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_SignerTitle();
    /// Sets suggested signer's title (for example, Manager).
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_SignerTitle(System::String value);
    /// Gets suggested signer's e-mail address.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Email();
    /// Sets suggested signer's e-mail address.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Email(System::String value);
    /// Gets a value indicating that default instructions is shown in the Sign dialog.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_DefaultInstructions();
    /// Sets a value indicating that default instructions is shown in the Sign dialog.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_DefaultInstructions(bool value);
    /// Gets instructions to the signer that are displayed on signing the signature line.
    /// This property is ignored if <see cref="Aspose::Words::Drawing::SignatureLine::get_DefaultInstructions">DefaultInstructions</see> is set.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API System::String get_Instructions();
    /// Sets instructions to the signer that are displayed on signing the signature line.
    /// This property is ignored if <see cref="Aspose::Words::Drawing::SignatureLine::get_DefaultInstructions">DefaultInstructions</see> is set.
    /// Default value for this property is <b>empty string</b> (<see cref="System::String::Empty">Empty</see>).
    ASPOSE_WORDS_SHARED_API void set_Instructions(System::String value);
    /// Gets a value indicating that the signer can add comments in the Sign dialog.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_AllowComments();
    /// Sets a value indicating that the signer can add comments in the Sign dialog.
    /// Default value for this property is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_AllowComments(bool value);
    /// Gets a value indicating that sign date is shown in the signature line.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowDate();
    /// Sets a value indicating that sign date is shown in the signature line.
    /// Default value for this property is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowDate(bool value);
    /// Gets identifier for this signature line.
    /// This identifier can be associated with a digital signature, when signing document using <see cref="Aspose::Words::DigitalSignatureUtil">DigitalSignatureUtil</see>.
    /// This value must be unique and by default it is randomly generated new Guid (<see cref="System::Guid::NewGuid">NewGuid</see>).
    ASPOSE_WORDS_SHARED_API System::Guid get_Id();
    /// Sets identifier for this signature line.
    /// This identifier can be associated with a digital signature, when signing document using <see cref="Aspose::Words::DigitalSignatureUtil">DigitalSignatureUtil</see>.
    /// This value must be unique and by default it is randomly generated new Guid (<see cref="System::Guid::NewGuid">NewGuid</see>).
    ASPOSE_WORDS_SHARED_API void set_Id(System::Guid value);
    /// Gets or sets signature provider identifier for this signature line.
    /// Default value is "{00000000-0000-0000-0000-000000000000}".
    /// 
    /// The cryptographic service provider (CSP) is an independent software module that actually performs
    /// cryptography algorithms for authentication, encoding, and encryption. MS Office reserves the value
    /// of {00000000-0000-0000-0000-000000000000} for its default signature provider.
    /// 
    /// The GUID of the additionally installed provider should be obtained from the documentation shipped with the provider.
    /// 
    /// In addition, all the installed cryptographic providers are enumerated in windows registry.
    /// It can be found in the following path: HKLM\\SOFTWARE\\Microsoft\\Cryptography\\Defaults\\Provider.
    /// There is a key name "CP Service UUID" which corresponds to a GUID of signature provider.
    ASPOSE_WORDS_SHARED_API System::Guid get_ProviderId();
    /// Setter for Aspose::Words::Drawing::SignatureLine::get_ProviderId
    ASPOSE_WORDS_SHARED_API void set_ProviderId(System::Guid value);
    /// Indicates that signature line is signed by digital signature.
    ASPOSE_WORDS_SHARED_API bool get_IsSigned();
    /// Indicates that signature line is signed by digital signature and this digital signature is valid.
    ASPOSE_WORDS_SHARED_API bool get_IsValid();

protected:

    System::SharedPtr<Aspose::Words::DigitalSignature> get_DigitalSignature();
    System::ArrayPtr<uint8_t> get_ImageBytes();
    System::SharedPtr<Aspose::Words::Drawing::Shape> get_Parent() const;

    SignatureLine(System::SharedPtr<Aspose::Words::Drawing::Shape> parent);

    void LoadProperties(System::SharedPtr<Aspose::Words::SignatureLineOptions> options);
    void UpdateShapeImage();

    virtual ASPOSE_WORDS_SHARED_API ~SignatureLine();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Drawing::Shape> mParent;
    int32_t mImageUpdateLock;

    void LockImageUpdate();
    void UnlockImageUpdate();

};

}
}
}
