//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/GroupShape.h
#pragma once

#include "Aspose.Words.Cpp/Model/Drawing/ShapeBase.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramRenderer; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlToDmlComplexShapeConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlOutlineToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlCompositeNodeReader; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class EditAs; } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Drawing { enum class ShapeMarkupLanguage : uint8_t; } } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { class Node; } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Represents a group of shapes in a document.
/// 
/// A <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see> is a composite node and can have <see cref="Aspose::Words::Drawing::Shape">Shape</see> and
/// <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see> nodes as children.
/// 
/// Each <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see> defines a new coordinate system for its child shapes.
/// The coordinate system is defined using the <see cref="Aspose::Words::Drawing::ShapeBase::get_CoordSize">CoordSize</see> and
/// <see cref="Aspose::Words::Drawing::ShapeBase::get_CoordOrigin">CoordOrigin</see> properties.
/// 
/// @sa Aspose::Words::Drawing::ShapeBase
/// @sa Aspose::Words::Drawing::Shape
class ASPOSE_WORDS_SHARED_CLASS GroupShape : public Aspose::Words::Drawing::ShapeBase
{
    typedef GroupShape ThisType;
    typedef Aspose::Words::Drawing::ShapeBase BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramRenderer;
    friend class Aspose::Words::Validation::VmlToDml::VmlToDmlComplexShapeConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlOutlineToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlCompositeNodeReader;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;

public:

    /// Returns <see cref="Aspose::Words::NodeType::GroupShape">GroupShape</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;

    /// Creates a new group shape.
    /// 
    /// By default, the shape is floating and has default location and size.
    /// 
    /// You should specify desired shape properties after you created a shape.
    /// 
    /// @param doc The owner document.
    ASPOSE_WORDS_SHARED_API GroupShape(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Aspose::Words::Drawing::Core::EditAs get_EditAs();
    void set_EditAs(Aspose::Words::Drawing::Core::EditAs value);
    ASPOSE_WORDS_SHARED_API bool get_IsPercentWidthInapplicable() override;

    GroupShape(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::Drawing::ShapeMarkupLanguage markupLanguage);

    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;

    virtual ASPOSE_WORDS_SHARED_API ~GroupShape();

private:

    static bool CanInsertBase(System::SharedPtr<Aspose::Words::Node> newChild);
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
