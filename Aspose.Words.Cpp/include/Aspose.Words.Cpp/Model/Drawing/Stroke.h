//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Stroke.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <drawing/color.h>

#include "Aspose.Words.Cpp/Model/Drawing/ShapeLineStyle.h"
#include "Aspose.Words.Cpp/Model/Drawing/JoinStyle.h"
#include "Aspose.Words.Cpp/Model/Drawing/EndCap.h"
#include "Aspose.Words.Cpp/Model/Drawing/DashStyle.h"
#include "Aspose.Words.Cpp/Model/Drawing/ArrowWidth.h"
#include "Aspose.Words.Cpp/Model/Drawing/ArrowType.h"
#include "Aspose.Words.Cpp/Model/Drawing/ArrowLength.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class PenFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class InternalColorResolver; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlImageWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeBoundsFinder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeInfo; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBorderNew; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class LineFillType; } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Drawing { class DrLineCapsStyle; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class IStroke; } } } }
namespace Aspose { namespace Drawing { enum class DrLineEndType; } }
namespace Aspose { namespace Drawing { enum class DrLineEndSize; } }
namespace Aspose { namespace Drawing { enum class DrLineEndingCapType; } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Defines a stroke for a shape.
/// 
/// Use the <see cref="Aspose::Words::Drawing::Shape::get_Stroke">Stroke</see> property to access stroke properties of a shape.
/// You do not create instances of the <see cref="Aspose::Words::Drawing::Stroke">Stroke</see> class directly.
/// 
/// @sa Aspose::Words::Drawing::Shape::get_Stroke
class ASPOSE_WORDS_SHARED_CLASS Stroke : public System::Object
{
    typedef Stroke ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Shapes::PenFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::InternalColorResolver;
    friend class Aspose::Words::RW::Html::Writer::HtmlImageWriter;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeBoundsFinder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeInfo;
    friend class Aspose::Words::RW::Html::Css::New::CssBorderNew;

public:

    /// Defines whether the path will be stroked.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_On();
    /// Defines whether the path will be stroked.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_On(bool value);
    /// Defines the brush thickness that strokes the path of a shape in points.
    /// 
    /// The default value is 0.75.
    ASPOSE_WORDS_SHARED_API double get_Weight();
    /// Defines the brush thickness that strokes the path of a shape in points.
    /// 
    /// The default value is 0.75.
    ASPOSE_WORDS_SHARED_API void set_Weight(double value);
    /// Defines the color of a stroke.
    /// 
    /// The default value is
    /// <see cref="System::Drawing::Color::get_Black">Black</see>.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color();
    /// Setter for Aspose::Words::Drawing::Stroke::get_Color
    ASPOSE_WORDS_SHARED_API void set_Color(System::Drawing::Color value);
    /// Defines a second color for a stroke.
    /// 
    /// The default value is
    /// <see cref="System::Drawing::Color::get_White">White</see>.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color2();
    /// Setter for Aspose::Words::Drawing::Stroke::get_Color2
    ASPOSE_WORDS_SHARED_API void set_Color2(System::Drawing::Color value);
    /// Specifies the dot and dash pattern for a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::DashStyle::Solid">Solid</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::DashStyle get_DashStyle();
    /// Specifies the dot and dash pattern for a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::DashStyle::Solid">Solid</see>.
    ASPOSE_WORDS_SHARED_API void set_DashStyle(Aspose::Words::Drawing::DashStyle value);
    /// Defines the join style of a polyline.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::JoinStyle::Round">Round</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::JoinStyle get_JoinStyle();
    /// Defines the join style of a polyline.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::JoinStyle::Round">Round</see>.
    ASPOSE_WORDS_SHARED_API void set_JoinStyle(Aspose::Words::Drawing::JoinStyle value);
    /// Defines the cap style for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::EndCap::Flat">Flat</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::EndCap get_EndCap();
    /// Defines the cap style for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::EndCap::Flat">Flat</see>.
    ASPOSE_WORDS_SHARED_API void set_EndCap(Aspose::Words::Drawing::EndCap value);
    /// Defines the line style of the stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ShapeLineStyle::Single">Single</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ShapeLineStyle get_LineStyle();
    /// Defines the line style of the stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ShapeLineStyle::Single">Single</see>.
    ASPOSE_WORDS_SHARED_API void set_LineStyle(Aspose::Words::Drawing::ShapeLineStyle value);
    /// Defines the arrowhead for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowType::None">None</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowType get_StartArrowType();
    /// Defines the arrowhead for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowType::None">None</see>.
    ASPOSE_WORDS_SHARED_API void set_StartArrowType(Aspose::Words::Drawing::ArrowType value);
    /// Defines the arrowhead for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowType::None">None</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowType get_EndArrowType();
    /// Defines the arrowhead for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowType::None">None</see>.
    ASPOSE_WORDS_SHARED_API void set_EndArrowType(Aspose::Words::Drawing::ArrowType value);
    /// Defines the arrowhead width for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowWidth::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowWidth get_StartArrowWidth();
    /// Defines the arrowhead width for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowWidth::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API void set_StartArrowWidth(Aspose::Words::Drawing::ArrowWidth value);
    /// Defines the arrowhead length for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowLength::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowLength get_StartArrowLength();
    /// Defines the arrowhead length for the start of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowLength::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API void set_StartArrowLength(Aspose::Words::Drawing::ArrowLength value);
    /// Defines the arrowhead width for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowWidth::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowWidth get_EndArrowWidth();
    /// Defines the arrowhead width for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowWidth::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API void set_EndArrowWidth(Aspose::Words::Drawing::ArrowWidth value);
    /// Defines the arrowhead length for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowLength::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ArrowLength get_EndArrowLength();
    /// Defines the arrowhead length for the end of a stroke.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::ArrowLength::Medium">Medium</see>.
    ASPOSE_WORDS_SHARED_API void set_EndArrowLength(Aspose::Words::Drawing::ArrowLength value);
    /// Defines the amount of transparency of a stroke. Valid range is from 0 to 1.
    /// 
    /// The default value is 1.
    ASPOSE_WORDS_SHARED_API double get_Opacity();
    /// Defines the amount of transparency of a stroke. Valid range is from 0 to 1.
    /// 
    /// The default value is 1.
    ASPOSE_WORDS_SHARED_API void set_Opacity(double value);
    /// Defines the image for a stroke image or pattern fill.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> get_ImageBytes();

protected:

    System::SharedPtr<Aspose::Drawing::DrColor> get_ColorInternal();
    void set_ColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_Color2Internal();
    void set_Color2Internal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    Aspose::Words::Drawing::Core::LineFillType get_LineFillType();
    void set_LineFillType(Aspose::Words::Drawing::Core::LineFillType value);
    System::SharedPtr<Aspose::Drawing::DrLineCapsStyle> get_LineCapsStyle();
    bool get_IsLineCapEnabled();

    Stroke(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Core::IStroke> get_StrokeCore();
    bool get_IsVmlOutline();

    System::WeakPtr<Aspose::Words::Drawing::Shape> mShape;
    System::SharedPtr<Aspose::Words::Drawing::Core::IStroke> mStrokeCoreCache;
    System::SharedPtr<Aspose::Drawing::DrLineCapsStyle> mCapStyle;

    void BuildCapsStyle();
    static Aspose::Drawing::DrLineEndType ToDrLineEndType(Aspose::Words::Drawing::ArrowType type);
    static Aspose::Drawing::DrLineEndSize ToDrLineEndSizeFromWidth(Aspose::Words::Drawing::ArrowWidth width);
    static Aspose::Drawing::DrLineEndSize ToDrLineEndSizeFromLength(Aspose::Words::Drawing::ArrowLength length);
    static Aspose::Drawing::DrLineEndingCapType ToDrLineEndingCapType(Aspose::Words::Drawing::EndCap endCap);

};

}
}
}
