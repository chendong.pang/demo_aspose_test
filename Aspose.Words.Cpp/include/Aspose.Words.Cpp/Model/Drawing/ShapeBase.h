//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/ShapeBase.h
#pragma once

#include <drawing/size_f.h>
#include <drawing/size.h>
#include <drawing/rectangle_f.h>
#include <drawing/point_f.h>
#include <drawing/point.h>

#include "Aspose.Words.Cpp/Model/Formatting/RunPrExpandFlags.h"
#include "Aspose.Words.Cpp/Model/Text/IInline.h"
#include "Aspose.Words.Cpp/Model/Revisions/ITrackableNode.h"
#include "Aspose.Words.Cpp/Model/Nodes/CompositeNode.h"
#include "Aspose.Words.Cpp/Model/Formatting/IShapeAttrSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/WrapType.h"
#include "Aspose.Words.Cpp/Model/Drawing/WrapSide.h"
#include "Aspose.Words.Cpp/Model/Drawing/VerticalAlignment.h"
#include "Aspose.Words.Cpp/Model/Drawing/ShapeType.h"
#include "Aspose.Words.Cpp/Model/Drawing/ShapeMarkupLanguage.h"
#include "Aspose.Words.Cpp/Model/Drawing/RelativeVerticalPosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/RelativeHorizontalPosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/HorizontalAlignment.h"
#include "Aspose.Words.Cpp/Model/Drawing/FlipOrientation.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/IShape.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramTextBoxFitCache; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlCompositeNodeRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Content { class TextBoxMetrics; } } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LayoutShapeDrawingMLAdapter; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class LayoutShapeVMLAdapter; } } } }
namespace Aspose { namespace Words { class Watermark; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlNodeCustomizer; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlShapeInserter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlWrapPolygonUtils; } } } } }
namespace Aspose { namespace Words { namespace Fields { class FieldIncludePictureUpdater; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRendererBase; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Extrusion { class ExtrusionParametersBuilder; } } } } }
namespace Aspose { namespace Words { namespace Comparison { class ShapeComparer; } } }
namespace Aspose { namespace Words { namespace Comparison { class StoryComparer; } } }
namespace Aspose { namespace Words { namespace Validation { class IstdVisitor; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class ChartInserter; } } } } }
namespace Aspose { namespace Words { class VideoInserter; } }
namespace Aspose { namespace Words { namespace Math { class OfficeMathUtil; } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplaceIndexer; } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxFieldsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class ImageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class SvgShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlFormulaToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlFillToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlGeometryToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlGradientToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlToDmlComplexShapeConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlOutlineToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlGraphicFrameWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxBackgroundReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxBackgroundWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace HtmlCommon { class HtmlImageUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeAdaptor; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlNode; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class DrawingMLImageDataSource; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class VmlNode; } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeSizeValidationHelper; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldBarcodeUtil; } } }
namespace Aspose { namespace Words { namespace Fields { class ImageResultFieldMergeFormatApplier; } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverterUtil; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class UnsupportedShapeDetector; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlGeometryToVmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlShapeToShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlContentPartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlDiagramReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartShapesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlCompositeNodeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlPictureReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlShapeReader; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartSpaceRenderer; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlCompositeNode; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlFillableNode; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlGroupShape; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlChartRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramDrawingRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlNodeRendererFactory; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlShapeBase; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlShape; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlToApsConverter; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Fills { class DmlFill; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Outlines { class DmlOutline; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { class DmlShapeRenderer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilderBase; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxSizesCalculator; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class DocumentLayoutBuilder; } } } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class WarningGenerator; } } } }
namespace Aspose { namespace Words { namespace Layout { class RunWrapFinder; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlShapePrFiller; } } } }
namespace Aspose { namespace Words { namespace Validation { class DrawingMLIdValidator; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlCanvasWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartSpaceWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlContentPartWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlDiagramDrawingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlGraphicWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlGroupShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlPictureWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapePrReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlImageWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtCaptionReader; } } } } }
namespace Aspose { namespace Words { class RevisionCollection; } }
namespace Aspose { namespace Words { namespace Fields { class TocHyperlinkEntryAttributeModifier; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldUpdateActionInsertImage; } } }
namespace Aspose { namespace Words { namespace Markup { class XmlMapping; } } }
namespace Aspose { namespace Words { class ShapeFieldRemover; } }
namespace Aspose { namespace Words { namespace Layout { namespace Core { class SpanShape; } } } }
namespace Aspose { namespace Words { namespace Layout { class AttributeConverter; } } }
namespace Aspose { namespace Words { namespace Layout { class DocumentSpanConverter; } } }
namespace Aspose { namespace Words { namespace Layout { namespace PreAps { class LayoutSpanPicture; } } } }
namespace Aspose { namespace Words { namespace Validation { class BookmarkValidator; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace Validation { class DocumentValidator; } } }
namespace Aspose { namespace Words { namespace Validation { class RunPrCollectorBase; } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeImageOptimizer; } } }
namespace Aspose { namespace Words { class SaveInfo; } }
namespace Aspose { namespace Words { namespace Validation { class ShapeIdGenerator; } } }
namespace Aspose { namespace Words { namespace Drawing { class Fill; } } }
namespace Aspose { namespace Words { namespace Drawing { class GroupShape; } } }
namespace Aspose { namespace Words { namespace Drawing { class ImageData; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Spans { class LineApsBuilder; } } } }
namespace Aspose { namespace Words { namespace Rendering { class ShapeRenderer; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class DmlShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilderContext; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeBoundsFinder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class BrushFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeInfo; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeTransformer; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssHRuleHeightStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBorderStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssWidthPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Writer { class NrxShapeDefaultsWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfLegacyShapeHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeResourceWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlFillReader; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlPictureToShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlDrawingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtPathReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapeBounds; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class Stroke; } } }
namespace Aspose { namespace Words { namespace Drawing { class TextBox; } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { class Section; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class GeometryResolver; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlDrawingReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxDocumentReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { class OdtEnum; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtCustomShapeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtGReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtEllipseReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtRectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtBinaryObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtConnectorReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtLineReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtListLevelPropertiesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtEnhancedGeometryWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtGraphicPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlOleReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtTextBoxReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFrameReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxNumberingWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsDrawingFiler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapeFiler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapePrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class ModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Shapes { class Fspa; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Shapes { class Picf; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class ShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlListReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtAutoStylesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfPictureHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfModelBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfOleHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfShapeHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfShapePropertyHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfOleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfShapePrWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeWriter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Model { namespace Nrx { class NrxXmlUtil; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class RelativeWidth; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class RelativeHeight; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class ConnectorType; } } } }
namespace Aspose { namespace Words { namespace Drawing { enum class LayoutFlow; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { enum class DmlChartUserShapeAnchorType; } } } } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { namespace Revisions { class EditRevision; } } }
namespace Aspose { namespace Words { namespace Revisions { class MoveRevision; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class Locks; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class ShapePr; } } } }
namespace Aspose { namespace Words { namespace Themes { class Theme; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class Graphic; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { enum class DmlNodeType; } } } } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Images { class ImageSizeCore; } }
namespace Aspose { namespace Words { class PageSetup; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class PathPoint; } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Base class for objects in the drawing layer, such as an AutoShape, freeform, OLE object, ActiveX control, or picture.
/// 
/// This is an abstract class. The two derived classes that you can instantiate
/// are <see cref="Aspose::Words::Drawing::Shape">Shape</see> and <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see>.
/// 
/// A shape is a node in the document tree.
/// 
/// If the shape is a child of a <see cref="Aspose::Words::Paragraph">Paragraph</see> object, then the shape is said to be "top-level".
/// Top-level shapes are measured and positioned in points.
/// 
/// A shape can also occur as a child of a <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see> object when several shapes
/// are grouped. Child shapes of a group shape are positioned in the coordinate space and units
/// defined by the <see cref="Aspose::Words::Drawing::ShapeBase::get_CoordSize">CoordSize</see> and <see cref="Aspose::Words::Drawing::ShapeBase::get_CoordOrigin">CoordOrigin</see> properties of the parent
/// group shape.
/// 
/// A shape can be positioned inline with text or floating. The positioning method is controlled
/// using the <see cref="Aspose::Words::Drawing::ShapeBase::get_WrapType">WrapType</see> property.
/// 
/// When a shape is floating, it is positioned relative to something (e.g the current paragraph,
/// the margin or the page). The relative positioning of the shape is specified using the
/// <see cref="Aspose::Words::Drawing::ShapeBase::get_RelativeHorizontalPosition">RelativeHorizontalPosition</see> and <see cref="Aspose::Words::Drawing::ShapeBase::get_RelativeVerticalPosition">RelativeVerticalPosition</see> properties.
/// 
/// A floating shape be positioned explicitly using the <see cref="Aspose::Words::Drawing::ShapeBase::get_Left">Left</see> and <see cref="Aspose::Words::Drawing::ShapeBase::get_Top">Top</see>
/// properties or aligned relative to some other object using the <see cref="Aspose::Words::Drawing::ShapeBase::get_HorizontalAlignment">HorizontalAlignment</see>
/// and <see cref="Aspose::Words::Drawing::ShapeBase::get_VerticalAlignment">VerticalAlignment</see> properties.
/// 
/// @sa Aspose::Words::Drawing::Shape
/// @sa Aspose::Words::Drawing::GroupShape
class ASPOSE_WORDS_SHARED_CLASS ShapeBase : public Aspose::Words::CompositeNode, public Aspose::Words::IInline, public Aspose::Words::Drawing::Core::IShape, public Aspose::Words::IShapeAttrSource, public Aspose::Words::Revisions::ITrackableNode
{
    typedef ShapeBase ThisType;
    typedef Aspose::Words::CompositeNode BaseType;
    typedef Aspose::Words::IInline BaseType1;
    typedef Aspose::Words::Drawing::Core::IShape BaseType2;
    typedef Aspose::Words::IShapeAttrSource BaseType3;
    typedef Aspose::Words::Revisions::ITrackableNode BaseType4;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2, BaseType3, BaseType4> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramTextBoxFitCache;
    friend class Aspose::Words::ApsBuilder::Dml::DmlCompositeNodeRenderer;
    friend class Aspose::Words::ApsBuilder::Shapes::Content::TextBoxMetrics;
    friend class Aspose::Words::Layout::Core::LayoutShapeDrawingMLAdapter;
    friend class Aspose::Words::Layout::Core::LayoutShapeVMLAdapter;
    friend class Aspose::Words::Watermark;
    friend class Aspose::Words::Drawing::Core::Dml::DmlNodeCustomizer;
    friend class Aspose::Words::Drawing::Core::Dml::DmlShapeInserter;
    friend class Aspose::Words::Drawing::Core::Dml::DmlWrapPolygonUtils;
    friend class Aspose::Words::Fields::FieldIncludePictureUpdater;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRendererBase;
    friend class Aspose::Words::ApsBuilder::Shapes::Extrusion::ExtrusionParametersBuilder;
    friend class Aspose::Words::Comparison::ShapeComparer;
    friend class Aspose::Words::Comparison::StoryComparer;
    friend class Aspose::Words::Validation::IstdVisitor;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::RW::Docx::Reader::ChartInserter;
    friend class Aspose::Words::VideoInserter;
    friend class Aspose::Words::Math::OfficeMathUtil;
    friend class Aspose::Words::Replacing::FindReplaceIndexer;
    friend class Aspose::Words::RW::Nrx::Writer::NrxFieldsWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::ImageShapeWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::SvgShapeWriter;
    friend class Aspose::Words::Validation::VmlToDml::VmlFormulaToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlFillToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlGeometryToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlGradientToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlToDmlComplexShapeConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlOutlineToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Dml::Writer::DmlGraphicFrameWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxBackgroundReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxBackgroundWriter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::RW::HtmlCommon::HtmlImageUtil;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::Drawing::ShapeAdaptor;
    friend class Aspose::Words::Drawing::Core::Dml::DmlNode;
    friend class Aspose::Words::Drawing::Core::DrawingMLImageDataSource;
    friend class Aspose::Words::Drawing::Core::VmlNode;
    friend class Aspose::Words::Drawing::ShapeSizeValidationHelper;
    friend class Aspose::Words::Fields::FieldBarcodeUtil;
    friend class Aspose::Words::Fields::ImageResultFieldMergeFormatApplier;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverterUtil;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Validation::UnsupportedShapeDetector;
    friend class Aspose::Words::Validation::DmlToVml::DmlGeometryToVmlConverter;
    friend class Aspose::Words::Validation::DmlToVml::DmlShapeToShapeConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlContentPartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlDiagramReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartShapesReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlCompositeNodeReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlPictureReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlShapeReader;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartSpaceRenderer;
    friend class Aspose::Words::Drawing::Core::Dml::DmlCompositeNode;
    friend class Aspose::Words::Drawing::Core::Dml::DmlFillableNode;
    friend class Aspose::Words::Drawing::Core::Dml::DmlGroupShape;
    friend class Aspose::Words::ApsBuilder::Dml::DmlChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramDrawingRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::DmlNodeRendererFactory;
    friend class Aspose::Words::Drawing::Core::Dml::DmlShapeBase;
    friend class Aspose::Words::Drawing::Core::Dml::DmlShape;
    friend class Aspose::Words::ApsBuilder::Dml::DmlToApsConverter;
    friend class Aspose::Words::Drawing::Core::Dml::Fills::DmlFill;
    friend class Aspose::Words::Drawing::Core::Dml::Outlines::DmlOutline;
    friend class Aspose::Words::Drawing::Core::Dml::Outlines::DmlOutline;
    friend class Aspose::Words::ApsBuilder::Dml::DmlShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilderBase;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxSizesCalculator;
    friend class Aspose::Words::Layout::Core::DocumentLayoutBuilder;
    friend class Aspose::Words::Layout::Core::WarningGenerator;
    friend class Aspose::Words::Layout::RunWrapFinder;
    friend class Aspose::Words::Validation::DmlToVml::DmlShapePrFiller;
    friend class Aspose::Words::Validation::DrawingMLIdValidator;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;
    friend class Aspose::Words::RW::Dml::Writer::DmlCanvasWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartSpaceWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlContentPartWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlDiagramDrawingWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlGraphicWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlGroupShapeWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlPictureWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeWriter;
    friend class Aspose::Words::RW::Doc::Escher::EsShapePrReader;
    friend class Aspose::Words::RW::Html::Writer::HtmlImageWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtCaptionReader;
    friend class Aspose::Words::RevisionCollection;
    friend class Aspose::Words::Fields::TocHyperlinkEntryAttributeModifier;
    friend class Aspose::Words::Fields::FieldUpdateActionInsertImage;
    friend class Aspose::Words::Markup::XmlMapping;
    friend class Aspose::Words::ShapeFieldRemover;
    friend class Aspose::Words::Layout::Core::SpanShape;
    friend class Aspose::Words::Layout::AttributeConverter;
    friend class Aspose::Words::Layout::DocumentSpanConverter;
    friend class Aspose::Words::Layout::PreAps::LayoutSpanPicture;
    friend class Aspose::Words::Validation::BookmarkValidator;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Validation::DocumentValidator;
    friend class Aspose::Words::Validation::RunPrCollectorBase;
    friend class Aspose::Words::Validation::ShapeImageOptimizer;
    friend class Aspose::Words::SaveInfo;
    friend class Aspose::Words::Validation::ShapeIdGenerator;
    friend class Aspose::Words::Drawing::Fill;
    friend class Aspose::Words::Drawing::GroupShape;
    friend class Aspose::Words::Drawing::ImageData;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::Spans::LineApsBuilder;
    friend class Aspose::Words::Rendering::ShapeRenderer;
    friend class Aspose::Words::ApsBuilder::Shapes::DmlShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilderContext;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeBoundsFinder;
    friend class Aspose::Words::ApsBuilder::Shapes::BrushFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeInfo;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeTransformer;
    friend class Aspose::Words::RW::Html::Css::New::CssHRuleHeightStyleConverter;
    friend class Aspose::Words::RW::Html::Css::New::CssBorderStyleConverter;
    friend class Aspose::Words::RW::Html::Css::New::CssWidthPropertyDef;
    friend class Aspose::Words::RW::Nrx::Writer::NrxShapeDefaultsWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfLegacyShapeHandler;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeResourceWriter;
    friend class Aspose::Words::RW::Vml::VmlFillReader;
    friend class Aspose::Words::Validation::DmlToVml::DmlPictureToShapeConverter;
    friend class Aspose::Words::RW::Dml::Writer::DmlDrawingWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtObjectReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtPathReader;
    friend class Aspose::Words::RW::Doc::Escher::EsShapeBounds;
    friend class Aspose::Words::Drawing::Stroke;
    friend class Aspose::Words::Drawing::TextBox;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::Section;
    friend class Aspose::Words::ApsBuilder::Shapes::GeometryResolver;
    friend class Aspose::Words::RW::Dml::Reader::DmlDrawingReader;
    friend class Aspose::Words::RW::Nrx::Reader::NrxDocumentReaderBase;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Odt::OdtEnum;
    friend class Aspose::Words::RW::Odt::Reader::OdtCustomShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtGReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtEllipseReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtRectReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtBinaryObjectReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtConnectorReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtLineReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtListLevelPropertiesReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtEnhancedGeometryWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtShapeWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtGraphicPropertiesWriter;
    friend class Aspose::Words::RW::Vml::VmlOleReader;
    friend class Aspose::Words::RW::Vml::VmlShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtTextBoxReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFrameReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxNumberingWriter;
    friend class Aspose::Words::RW::Doc::Escher::EsDrawingFiler;
    friend class Aspose::Words::RW::Doc::Escher::EsShapeFiler;
    friend class Aspose::Words::RW::Doc::Escher::EsShapePrWriter;
    friend class Aspose::Words::RW::Doc::Reader::ModelBuilder;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Doc::Shapes::Fspa;
    friend class Aspose::Words::RW::Doc::Shapes::Picf;
    friend class Aspose::Words::RW::Doc::Writer::ShapeWriter;
    friend class Aspose::Words::RW::Html::Reader::HtmlListReader;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtAutoStylesWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfPictureHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfModelBuilder;
    friend class Aspose::Words::RW::Rtf::Reader::RtfOleHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfShapeHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfShapePropertyHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfOleWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfShapePrWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfShapeWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfWriter;
    friend class Aspose::Words::RW::Vml::VmlShapeWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;
    friend class Aspose::Words::Model::Nrx::NrxXmlUtil;

public:
    using Aspose::Words::CompositeNode::Clone;

public:

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document_IInline() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> get_RunPr_IInline() override;
    ASPOSE_WORDS_SHARED_API void set_RunPr_IInline(System::SharedPtr<Aspose::Words::RunPr> value) override;
    ASPOSE_WORDS_SHARED_API int32_t get_ZOrder_IShape() override;
    ASPOSE_WORDS_SHARED_API void set_ZOrder_IShape(int32_t value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_InsertRevision() override;
    ASPOSE_WORDS_SHARED_API void set_InsertRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::EditRevision> get_DeleteRevision() override;
    ASPOSE_WORDS_SHARED_API void set_DeleteRevision(System::SharedPtr<Aspose::Words::Revisions::EditRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveFromRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveFromRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Revisions::MoveRevision> get_MoveToRevision() override;
    ASPOSE_WORDS_SHARED_API void set_MoveToRevision(System::SharedPtr<Aspose::Words::Revisions::MoveRevision> value) override;
    /// Defines the text displayed when the mouse pointer moves over the shape.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_ScreenTip();
    /// Defines the text displayed when the mouse pointer moves over the shape.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API void set_ScreenTip(System::String value);
    /// Gets or sets the full hyperlink address for a shape.
    /// 
    /// The default value is an empty string.
    /// 
    /// Below are examples of valid values for this property:
    /// 
    /// Full URI: <c>https://www.aspose.com/</c>.
    /// 
    /// Full file name: <c>C:\\\\My Documents\\\\SalesReport.doc</c>.
    /// 
    /// Relative URI: <c>../../../resource.txt</c>
    /// 
    /// Relative file name: <c>..\\\\My Documents\\\\SalesReport.doc</c>.
    /// 
    /// Bookmark within another document: <c>https://www.aspose.com/Products/Default.aspx#Suites</c>
    /// 
    /// Bookmark within this document: <c>\#BookmakName</c>.
    ASPOSE_WORDS_SHARED_API System::String get_HRef();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_HRef
    ASPOSE_WORDS_SHARED_API void set_HRef(System::String value);
    /// Gets the target frame for the shape hyperlink.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Target();
    /// Sets the target frame for the shape hyperlink.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API void set_Target(System::String value);
    /// Defines alternative text to be displayed instead of a graphic.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_AlternativeText();
    /// Defines alternative text to be displayed instead of a graphic.
    /// 
    /// The default value is an empty string.
    ASPOSE_WORDS_SHARED_API void set_AlternativeText(System::String value);
    /// Gets or sets the title (caption) of the current shape object.
    /// 
    /// Default is empty string.
    /// 
    /// Cannot be null, but can be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Title();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Title
    ASPOSE_WORDS_SHARED_API void set_Title(System::String value);
    /// Gets or sets the optional shape name.
    /// 
    /// Default is empty string.
    /// 
    /// Cannot be null, but can be an empty string.
    ASPOSE_WORDS_SHARED_API System::String get_Name();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Name
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Returns true if this object was inserted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsInsertRevision();
    /// Returns true if this object was deleted in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsDeleteRevision();
    /// Returns <b>true</b> if this object was moved (deleted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveFromRevision();
    /// Returns <b>true</b> if this object was moved (inserted) in Microsoft Word while change tracking was enabled.
    ASPOSE_WORDS_SHARED_API bool get_IsMoveToRevision();
    /// Returns true if this shape is not a child of a group shape.
    ASPOSE_WORDS_SHARED_API bool get_IsTopLevel();
    /// Returns true if this is a group shape.
    ASPOSE_WORDS_SHARED_API bool get_IsGroup();
    /// Returns true if this shape is an image shape.
    ASPOSE_WORDS_SHARED_API bool get_IsImage();
    /// Returns true if this shape is a horizontal rule.
    ASPOSE_WORDS_SHARED_API bool get_IsHorizontalRule();
    /// Returns true if this shape is a WordArt object.
    ASPOSE_WORDS_SHARED_API bool get_IsWordArt();
    /// Returns true if the shape type allows the shape to have an image.
    /// 
    /// Although Microsoft Word has a special shape type for images, it appears that in Microsoft Word documents any shape
    /// except a group shape can have an image, therefore this property returns true for all shapes except <see cref="Aspose::Words::Drawing::GroupShape">GroupShape</see>.
    ASPOSE_WORDS_SHARED_API bool get_CanHaveImage();
    /// Specifies whether the shape's anchor is locked.
    /// 
    /// The default value is <b>false</b>.
    /// 
    /// Has effect only for top level shapes.
    /// 
    /// This property affects behavior of the shape's anchor in Microsoft Word.
    /// When the anchor is not locked, moving the shape in Microsoft Word can move
    /// the shape's anchor too.
    ASPOSE_WORDS_SHARED_API bool get_AnchorLocked();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_AnchorLocked
    ASPOSE_WORDS_SHARED_API void set_AnchorLocked(bool value);
    /// Specifies whether the shape's aspect ratio is locked.
    /// 
    /// The default value depends on the <see cref="Aspose::Words::Drawing::ShapeBase::get_ShapeType">ShapeType</see>, for the ShapeType.Image it is <b>true</b>
    /// but for the other shape types it is <b>false</b>.
    /// 
    /// Has effect for top level shapes only.
    ASPOSE_WORDS_SHARED_API bool get_AspectRatioLocked();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_AspectRatioLocked
    ASPOSE_WORDS_SHARED_API void set_AspectRatioLocked(bool value);
    /// Gets or sets a value that specifies whether this shape can overlap other shapes.
    /// 
    /// This property affects behavior of the shape in Microsoft Word.
    /// Aspose.Words ignores the value of this property.
    /// 
    /// This property is applicable only to top level shapes.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_AllowOverlap();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_AllowOverlap
    ASPOSE_WORDS_SHARED_API void set_AllowOverlap(bool value);
    /// Specifies whether the shape is below or above text.
    /// 
    /// Has effect only for top level shapes.
    /// 
    /// The default value is <b>false</b>.
    /// 
    /// @sa Aspose::Words::Drawing::ShapeBase::get_ZOrder
    ASPOSE_WORDS_SHARED_API bool get_BehindText();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_BehindText
    ASPOSE_WORDS_SHARED_API void set_BehindText(bool value);
    /// A quick way to determine if this shape is positioned inline with text.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API bool get_IsInline();
    /// Gets or sets the position of the left edge of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points and relative to the shape anchor.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    /// 
    /// The default value is 0.
    /// 
    /// Has effect only for floating shapes.
    ASPOSE_WORDS_SHARED_API double get_Left();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Left
    ASPOSE_WORDS_SHARED_API void set_Left(double value);
    /// Gets or sets the position of the top edge of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points and relative to the shape anchor.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    /// 
    /// The default value is 0.
    /// 
    /// Has effect only for floating shapes.
    ASPOSE_WORDS_SHARED_API double get_Top();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Top
    ASPOSE_WORDS_SHARED_API void set_Top(double value);
    /// Gets the position of the right edge of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points and relative to the shape anchor.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    ASPOSE_WORDS_SHARED_API double get_Right();
    /// Gets the position of the bottom edge of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points and relative to the shape anchor.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    ASPOSE_WORDS_SHARED_API double get_Bottom();
    /// Gets or sets the width of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    /// 
    /// The default value is 0.
    ASPOSE_WORDS_SHARED_API double get_Width();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Width
    ASPOSE_WORDS_SHARED_API void set_Width(double value);
    /// Gets or sets the height of the containing block of the shape.
    /// 
    /// For a top-level shape, the value is in points.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    /// 
    /// The default value is 0.
    ASPOSE_WORDS_SHARED_API double get_Height();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Height
    ASPOSE_WORDS_SHARED_API void set_Height(double value);
    /// Returns or sets the distance (in points) between the document text and the top edge of the shape.
    /// 
    /// The default value is 0.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API double get_DistanceTop();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_DistanceTop
    ASPOSE_WORDS_SHARED_API void set_DistanceTop(double value);
    /// Returns or sets the distance (in points) between the document text and the bottom edge of the shape.
    /// 
    /// The default value is 0.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API double get_DistanceBottom();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_DistanceBottom
    ASPOSE_WORDS_SHARED_API void set_DistanceBottom(double value);
    /// Returns or sets the distance (in points) between the document text and the left edge of the shape.
    /// 
    /// The default value is 1/8 inch.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API double get_DistanceLeft();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_DistanceLeft
    ASPOSE_WORDS_SHARED_API void set_DistanceLeft(double value);
    /// Returns or sets the distance (in points) between the document text and the right edge of the shape.
    /// 
    /// The default value is 1/8 inch.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API double get_DistanceRight();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_DistanceRight
    ASPOSE_WORDS_SHARED_API void set_DistanceRight(double value);
    /// Defines the angle (in degrees) that a shape is rotated.
    /// Positive value corresponds to clockwise rotation angle.
    /// 
    /// The default value is 0.
    ASPOSE_WORDS_SHARED_API double get_Rotation();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Rotation
    ASPOSE_WORDS_SHARED_API void set_Rotation(double value);
    /// Determines the display order of overlapping shapes.
    /// 
    /// Has effect only for top level shapes.
    /// 
    /// The default value is 0.
    /// 
    /// The number represents the stacking precedence. A shape with a higher number will be displayed
    /// as if it were overlapping (in "front" of) a shape with a lower number.
    /// 
    /// The order of overlapping shapes is independent for shapes in the header and in the main
    /// text of the document.
    /// 
    /// The display order of child shapes in a group shape is determined by their order
    /// inside the group shape.
    /// 
    /// @sa Aspose::Words::Drawing::ShapeBase::get_BehindText
    ASPOSE_WORDS_SHARED_API int32_t get_ZOrder();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_ZOrder
    ASPOSE_WORDS_SHARED_API void set_ZOrder(int32_t value);
    /// Returns the immediate parent paragraph.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_ParentParagraph();
    /// Gets or sets the location and size of the containing block of the shape.
    /// 
    /// Ignores aspect ratio lock upon setting.
    /// 
    /// For a top-level shape, the value is in points and relative to the shape anchor.
    /// 
    /// For shapes in a group, the value is in the coordinate space and units of the parent group.
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF get_Bounds();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_Bounds
    ASPOSE_WORDS_SHARED_API void set_Bounds(System::Drawing::RectangleF value);
    /// Gets the location and size of the containing block of the shape in points, relative to the anchor of the topmost shape.
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF get_BoundsInPoints();
    /// Gets final extent that this shape object has after applying drawing effects.
    /// Value is measured in points.
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF get_BoundsWithEffects();
    /// Gets the shape type.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ShapeType get_ShapeType();
    /// Gets MarkupLanguage used for this graphic object.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ShapeMarkupLanguage get_MarkupLanguage() const;
    /// Gets the size of the shape in points.
    ASPOSE_WORDS_SHARED_API System::Drawing::SizeF get_SizeInPoints();
    /// Switches the orientation of a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::FlipOrientation::None">None</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::FlipOrientation get_FlipOrientation();
    /// Switches the orientation of a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::FlipOrientation::None">None</see>.
    ASPOSE_WORDS_SHARED_API void set_FlipOrientation(Aspose::Words::Drawing::FlipOrientation value);
    /// Specifies relative to what the shape is positioned horizontally.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::RelativeHorizontalPosition::Column">Column</see>.
    /// 
    /// Has effect only for top level floating shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::RelativeHorizontalPosition get_RelativeHorizontalPosition();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_RelativeHorizontalPosition
    ASPOSE_WORDS_SHARED_API void set_RelativeHorizontalPosition(Aspose::Words::Drawing::RelativeHorizontalPosition value);
    /// Specifies relative to what the shape is positioned vertically.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::RelativeVerticalPosition::Paragraph">Paragraph</see>.
    /// 
    /// Has effect only for top level floating shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::RelativeVerticalPosition get_RelativeVerticalPosition();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_RelativeVerticalPosition
    ASPOSE_WORDS_SHARED_API void set_RelativeVerticalPosition(Aspose::Words::Drawing::RelativeVerticalPosition value);
    /// Specifies how the shape is positioned horizontally.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::HorizontalAlignment::None">None</see>.
    /// 
    /// Has effect only for top level floating shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::HorizontalAlignment get_HorizontalAlignment();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_HorizontalAlignment
    ASPOSE_WORDS_SHARED_API void set_HorizontalAlignment(Aspose::Words::Drawing::HorizontalAlignment value);
    /// Specifies how the shape is positioned vertically.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::VerticalAlignment::None">None</see>.
    /// 
    /// Has effect only for top level floating shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::VerticalAlignment get_VerticalAlignment();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_VerticalAlignment
    ASPOSE_WORDS_SHARED_API void set_VerticalAlignment(Aspose::Words::Drawing::VerticalAlignment value);
    /// Defines whether the shape is inline or floating. For floating shapes defines the wrapping mode for text around the shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::WrapType::None">None</see>.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::WrapType get_WrapType();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_WrapType
    ASPOSE_WORDS_SHARED_API void set_WrapType(Aspose::Words::Drawing::WrapType value);
    /// Specifies how the text is wrapped around the shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::WrapSide::Both">Both</see>.
    /// 
    /// Has effect only for top level shapes.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::WrapSide get_WrapSide();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_WrapSide
    ASPOSE_WORDS_SHARED_API void set_WrapSide(Aspose::Words::Drawing::WrapSide value);
    /// The coordinates at the top-left corner of the containing block of this shape.
    /// 
    /// The default value is (0,0).
    ASPOSE_WORDS_SHARED_API System::Drawing::Point get_CoordOrigin();
    /// The coordinates at the top-left corner of the containing block of this shape.
    /// 
    /// The default value is (0,0).
    ASPOSE_WORDS_SHARED_API void set_CoordOrigin(System::Drawing::Point value);
    /// The width and height of the coordinate space inside the containing block of this shape.
    /// 
    /// The default value is (1000, 1000).
    ASPOSE_WORDS_SHARED_API System::Drawing::Size get_CoordSize();
    /// The width and height of the coordinate space inside the containing block of this shape.
    /// 
    /// The default value is (1000, 1000).
    ASPOSE_WORDS_SHARED_API void set_CoordSize(System::Drawing::Size value);
    /// Provides access to the font formatting of this object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_Font();
    /// Indicates that shape is a SignatureLine.
    ASPOSE_WORDS_SHARED_API bool get_IsSignatureLine();
    /// Gets or sets a flag indicating whether the shape is displayed inside a table or outside of it.
    /// 
    /// The default value is <b>true</b>.
    /// 
    /// Has effect only for top level shapes, the property <see cref="Aspose::Words::Drawing::ShapeBase::get_WrapType">WrapType</see> of which is set to value
    /// other than <see cref="Aspose::Words::Inline">Inline</see>.
    ASPOSE_WORDS_SHARED_API bool get_IsLayoutInCell();
    /// Setter for Aspose::Words::Drawing::ShapeBase::get_IsLayoutInCell
    ASPOSE_WORDS_SHARED_API void set_IsLayoutInCell(bool value);

    /// Creates and returns an object that can be used to render this shape into an image.
    /// 
    /// This method just invokes the <see cref="Aspose::Words::Rendering::ShapeRenderer">ShapeRenderer</see> constructor and passes
    /// this object as a parameter.
    /// 
    /// @return The renderer object for this shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Rendering::ShapeRenderer> GetShapeRenderer();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr_IInline(Aspose::Words::RunPrExpandFlags flags) override;
    /// Reserved for system use. IShapeAttrSource.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectShapeAttr(int32_t key) override;
    /// Reserved for system use. IShapeAttrSource.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedShapeAttr(int32_t key) override;
    /// Reserved for system use. IShapeAttrSource.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchShapeAttr(int32_t key) override;
    /// Reserved for system use. IShapeAttrSource.
    ASPOSE_WORDS_SHARED_API void SetShapeAttr(int32_t key, System::SharedPtr<System::Object> value) override;
    /// Reserved for system use. IShapeAttrSource.
    ASPOSE_WORDS_SHARED_API void RemoveShapeAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> GetDirectRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Object> FetchInheritedRunAttr(int32_t fontAttr) override;
    ASPOSE_WORDS_SHARED_API void SetRunAttr(int32_t fontAttr, System::SharedPtr<System::Object> value) override;
    ASPOSE_WORDS_SHARED_API void RemoveRunAttr(int32_t key) override;
    ASPOSE_WORDS_SHARED_API void ClearRunAttrs() override;
    /// Converts a value from the local coordinate space into the coordinate space of the parent shape.
    ASPOSE_WORDS_SHARED_API System::Drawing::PointF LocalToParent(System::Drawing::PointF value);
    /// Adds to the source rectangle values of the effect extent and returns the final rectangle.
    ASPOSE_WORDS_SHARED_API System::Drawing::RectangleF AdjustWithEffects(System::Drawing::RectangleF source);

protected:

    virtual bool get_IsPercentWidthInapplicable() = 0;
    bool get_IsTextBox();
    bool get_IsEffectExtentEmpty();
    Aspose::Words::Drawing::FlipOrientation get_ParentFlipOrientation();
    System::SharedPtr<Aspose::Words::Drawing::Core::Locks> get_Locks();
    int32_t get_CoordOriginX();
    void set_CoordOriginX(int32_t value);
    int32_t get_CoordOriginY();
    void set_CoordOriginY(int32_t value);
    int32_t get_Id();
    void set_Id(int32_t value);
    bool get_IsOleObject();
    bool get_IsOleControl();
    bool get_IsOle();
    bool get_IsStandardHorizontalRule();
    bool get_IsNeedDummyWhenInline();
    bool get_IsPictureBullet();
    void set_IsPictureBullet(bool value);
    System::Drawing::SizeF get_Size();
    System::SharedPtr<System::Object> get_LeftPercent();
    System::SharedPtr<System::Object> get_TopPercent();
    System::SharedPtr<System::Object> get_WidthPercent();
    System::SharedPtr<System::Object> get_HeightPercent();
    Aspose::Words::Drawing::Core::RelativeWidth get_RelativeWidth();
    Aspose::Words::Drawing::Core::RelativeHeight get_RelativeHeight();
    int32_t get_CoordSizeWidth();
    int32_t get_CoordSizeHeight();
    double get_TotalRotation();
    bool get_HasHyperlink();
    bool get_IsHyperlinkedInline();
    System::String get_HyperlinkAddress();
    System::String get_HyperlinkSubAddress();
    System::SharedPtr<Aspose::Words::Drawing::Core::ShapePr> get_ShapePr() const;
    void set_ShapePr(System::SharedPtr<Aspose::Words::Drawing::Core::ShapePr> value);
    System::SharedPtr<Aspose::Words::RunPr> get_RunPr() const;
    void set_RunPr(System::SharedPtr<Aspose::Words::RunPr> value);
    Aspose::Words::Drawing::Core::ConnectorType get_ConnectorType();
    void set_ConnectorType(Aspose::Words::Drawing::Core::ConnectorType value);
    bool get_IsConnector();
    int32_t get_Txid();
    void set_Txid(int32_t value);
    int32_t get_TextboxNextShapeId();
    void set_TextboxNextShapeId(int32_t value);
    bool get_Hidden();
    void set_Hidden(bool value);
    System::ArrayPtr<uint8_t> get_InkData();
    bool get_InkAnnotation();
    bool get_IsPseudoInline();
    System::ArrayPtr<System::Drawing::PointF> get_WrapPolygonVertices();
    bool get_IsCalloutShapeType();
    bool get_IsConnectorShapeType();
    bool get_IsBracket();
    bool get_IsBrace();
    bool get_IsDmlShape();
    Aspose::Words::Drawing::LayoutFlow get_LayoutFlow();
    int32_t get_TextboxId() const;
    void set_TextboxId(int32_t value);
    int32_t get_LinkedTextboxId() const;
    void set_LinkedTextboxId(int32_t value);
    int32_t get_LinkedTextboxSeq() const;
    void set_LinkedTextboxSeq(int32_t value);
    bool get_HasTextbox();
    System::SharedPtr<Aspose::Words::Themes::Theme> get_DocumentTheme();
    System::SharedPtr<Aspose::Words::Drawing::ShapeBase> get_FallbackShape();
    bool get_SupportRendering();
    bool get_IsGridCalculationSupported();
    Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartUserShapeAnchorType get_AnchorType() const;
    void set_AnchorType(Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartUserShapeAnchorType value);
    System::Drawing::PointF get_From() const;
    void set_From(System::Drawing::PointF value);
    System::Drawing::PointF get_To() const;
    void set_To(System::Drawing::PointF value);
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlNode> get_DmlNode() const;
    void set_DmlNode(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlNode> value);
    System::SharedPtr<Aspose::Words::Drawing::Core::Graphic> get_GraphicData();
    bool get_HasDrawingExtensions();
    bool get_IsFallback() const;
    void set_IsFallback(bool value);
    System::Drawing::Size get_EffectiveCoordSize();
    int32_t get_EffectiveCoordSizeWidth();
    int32_t get_EffectiveCoordSizeHeight();
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlShape> get_DmlShape();
    bool get_CanHaveFillImage();
    bool get_IsWatermark();
    bool get_CanBeTextWatermark();
    bool get_CanBeImageWatermark();
    System::Drawing::SizeF get_BoundsForRelativeSize() const;

    static double& DefaultShapeSize();

    static const int32_t MainDrawingPatriarchShapeId;
    static const int32_t ShapeIdClusterSize;

    ASPOSE_WORDS_SHARED_API ShapeBase(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::Drawing::ShapeMarkupLanguage markupLanguage);

    System::Drawing::RectangleF ConvertLocalToTopmostAnchor(System::Drawing::RectangleF rect);
    System::Drawing::PointF ParentToPoints(System::Drawing::PointF value);
    bool HasTextBoxes();
    bool HasFields();
    bool HasDmlNodes(Aspose::Words::Drawing::Core::Dml::DmlNodeType dmlNodeType);
    void SetShapeType(Aspose::Words::Drawing::ShapeType shapeType);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    System::SharedPtr<Aspose::Words::RunPr> GetExpandedRunPr(Aspose::Words::RunPrExpandFlags flags);
    void SetSizeSafe(double width, double height);
    void SetWidthSafe(double value);
    void SetHeightSafe(double value);
    void MakeSizeValid();
    System::SharedPtr<Aspose::Words::Drawing::ShapeBase> GetTopLevelParentShape();
    bool RequiresBoundsRecalculation();
    void UpdateSizeAndPosition();
    void AdjustHorizontalRuleHeight();
    void UpdateSizeAndPositionFromRelative();
    void SetCoordSizeSafe(System::Drawing::Size value);
    void SetCoordSizeWidthSafe(int32_t width);
    void SetCoordSizeHeightSafe(int32_t height);
    bool IsChangedSinceLoad();
    void MarkLoaded();
    void UpdateFallBackParent();
    System::SharedPtr<Aspose::Images::ImageSizeCore> GetShapeImageSize();
    void SetIsSignatureLine(bool value);
    static System::String GenerateAltText(System::String title, System::String description);
    static bool IsAllowedShapeTypes(Aspose::Words::Drawing::ShapeType shapeType);
    System::Drawing::RectangleF GetsSizeBoxForBounds(double rotation);

    virtual ASPOSE_WORDS_SHARED_API ~ShapeBase();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsCustomShape();
    bool get_SwitchPercentDimensions();
    System::SharedPtr<Aspose::Words::CompositeNode> get_FallBack();

    System::SharedPtr<Aspose::Words::Drawing::Core::ShapePr> mShapePr;
    System::SharedPtr<Aspose::Words::RunPr> mRunPr;
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlShape> mDmlShape;
    System::SharedPtr<Aspose::Words::Drawing::Core::Locks> mLocksCache;
    System::SharedPtr<Aspose::Words::Font> mFontCache;
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlNode> mDmlNode;
    System::SharedPtr<Aspose::Words::Drawing::Core::Graphic> mGraphicData;
    Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartUserShapeAnchorType mAnchorType;
    System::Drawing::PointF mFrom;
    System::Drawing::PointF mTo;
    Aspose::Words::Drawing::ShapeMarkupLanguage mMarkupLanguage;
    int32_t mTextboxId;
    int32_t mLinkedTextboxId;
    int32_t mLinkedTextboxSeq;
    int64_t mHashCodeCache;
    static const int32_t DefaultPercentValue;
    bool mIsFallback;
    System::Drawing::Size mEffectiveCoordSize;
    System::Drawing::SizeF mBoundsForRelativeSize;

    void UpdateLeftFromPercent(System::SharedPtr<Aspose::Words::PageSetup> ps);
    void UpdateTopFromPercent(System::SharedPtr<Aspose::Words::PageSetup> ps);
    void UpdateWidthFromPercent(System::SharedPtr<Aspose::Words::PageSetup> ps);
    void UpdateHeightFromPercent(System::SharedPtr<Aspose::Words::PageSetup> ps);
    void SetPercentWidth(double value);
    void SetPercentHeight(double value);
    void SetWidthLockAspectRatioSensitive(double value, bool isThrow);
    void SetHeightLockAspectRatioSensitive(double value, bool isThrow);
    void ResetRelSizeValue(int32_t key);
    void SetWidthCore(double value, bool isThrow);
    void SetHeightCore(double value, bool isThrow);
    void ValidateZeroSizeShape();
    System::Drawing::SizeF CalculateNotZeroSize();
    int64_t CalculateHashCode();
    static System::ArrayPtr<System::Drawing::PointF> ConvertPathPointToPointF(System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::PathPoint>> pathPoints);
    System::Drawing::RectangleF GetPathBoundingRect();
    System::Drawing::RectangleF GetsBoundsWithEffects();
    float GetIntegerNumberOfTwips(float value);
    float GetExtentEffectValue(int32_t attr);

};

}
}
}
