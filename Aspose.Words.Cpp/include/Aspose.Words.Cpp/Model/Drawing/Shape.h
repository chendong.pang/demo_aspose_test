//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Shape.h
#pragma once

#include <drawing/color.h>

#include "Aspose.Words.Cpp/Model/Sections/StoryType.h"
#include "Aspose.Words.Cpp/Model/Drawing/TextBoxWrapMode.h"
#include "Aspose.Words.Cpp/Model/Drawing/ShapeType.h"
#include "Aspose.Words.Cpp/Model/Drawing/ShapeMarkupLanguage.h"
#include "Aspose.Words.Cpp/Model/Drawing/ShapeBase.h"
#include "Aspose.Words.Cpp/Model/Drawing/LayoutFlow.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/ITextBox.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class LimoMapBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Content { class TextBoxMetrics; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlShapeInserter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class HorizontalRuleFormat; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Diagram { class DmlDiagramRenderingServiceLocator; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Math { class MathTextElement; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class GeometryApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Extrusion { class ExtrusionParametersBuilder; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class ChartInserter; } } } } }
namespace Aspose { namespace Words { class VideoInserter; } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownParagraphWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Markdown { namespace Writer { class MarkdownWriter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class OfficeMathToShapeConverter; } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlFormulaToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlGeometryToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlShapeToDmlShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxBackgroundWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Nrx { namespace Reader { class NrxRunReaderBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverterUtil; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlShapeToShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlContentPartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlDiagramReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlPictureReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlShapeReader; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Extrusion { class ExtrusionBoundsCalculator; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { namespace Shadow { class ShadowBuilderParameters; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class InternalColorResolver; } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlUtil; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlImageWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtCaptionReader; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ListValidator; } } }
namespace Aspose { namespace Words { class DocumentBuilder; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilderContext; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class BrushFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeTransformer; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssHRuleAlignmentStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssHRuleNoShadeStyleConverter; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssWidthPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { namespace Validation { namespace DmlToVml { class DmlPictureToShapeConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtPathReader; } } } } }
namespace Aspose { namespace Words { namespace Lists { class ListCollection; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class GeometryResolver; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriterBase; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtCustomShapeReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtEllipseReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtRectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtConnectorReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtLineReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtEnhancedGeometryWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtShapeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Writer { class OdtGraphicPropertiesWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlOleReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlShapeReader; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtFrameReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { class WordUtil; } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Escher { class EsShapeFiler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class Parser; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Reader { class HtmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfPictureHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfOleHandler; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfOleWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfPictureData; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Vml { class VmlGeoWriter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class ConnectorType; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class OleObjectType; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class ShadowType; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class ThreeDRenderMode; } } } }
namespace Aspose { namespace Words { namespace Drawing { class Stroke; } } }
namespace Aspose { namespace Words { namespace Drawing { class Fill; } } }
namespace Aspose { namespace Words { namespace Drawing { class ImageData; } } }
namespace Aspose { namespace Words { namespace Drawing { class OleFormat; } } }
namespace Aspose { namespace Words { namespace Drawing { class TextBox; } } }
namespace Aspose { namespace Words { namespace Drawing { class TextPath; } } }
namespace Aspose { namespace Words { class Paragraph; } }
namespace Aspose { namespace Words { namespace Drawing { class SignatureLine; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class HorizontalRule; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class PathInfo; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class PathPoint; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class Formula; } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class Handle; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class PathRectangle; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class Chart; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class ChartCollection; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }
namespace Aspose { namespace Words { enum class VisitorAction; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class INodeCloningListener; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { enum class DmlNodeType; } } } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Rendering { namespace Aps { class ApsNode; } } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Represents an object in the drawing layer, such as an AutoShape, textbox, freeform, OLE object, ActiveX control, or picture.
/// 
/// Using the <see cref="Aspose::Words::Drawing::Shape">Shape</see> class you can create or modify shapes in a Microsoft Word document.
/// 
/// An important property of a shape is its <see cref="Aspose::Words::Drawing::ShapeBase::get_ShapeType">ShapeType</see>. Shapes of different
/// types can have different capabilities in a Word document. For example, only image and OLE shapes
/// can have images inside them. Most of the shapes can have text, but not all.
/// 
/// Shapes that can have text, can contain <see cref="Aspose::Words::Paragraph">Paragraph</see> and
/// <see cref="Aspose::Words::Tables::Table">Table</see> nodes as children.
/// 
/// @sa Aspose::Words::Drawing::ShapeBase
/// @sa Aspose::Words::Drawing::GroupShape
class ASPOSE_WORDS_SHARED_CLASS Shape FINAL : public Aspose::Words::Drawing::ShapeBase, public Aspose::Words::Drawing::Core::ITextBox
{
    typedef Shape ThisType;
    typedef Aspose::Words::Drawing::ShapeBase BaseType;
    typedef Aspose::Words::Drawing::Core::ITextBox BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Shapes::LimoMapBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::Content::TextBoxMetrics;
    friend class Aspose::Words::Drawing::Core::Dml::DmlShapeInserter;
    friend class Aspose::Words::Drawing::HorizontalRuleFormat;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Diagram::DmlDiagramRenderingServiceLocator;
    friend class Aspose::Words::ApsBuilder::Math::MathTextElement;
    friend class Aspose::Words::ApsBuilder::Shapes::GeometryApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::Extrusion::ExtrusionParametersBuilder;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::RW::Docx::Reader::ChartInserter;
    friend class Aspose::Words::VideoInserter;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownParagraphWriter;
    friend class Aspose::Words::RW::Markdown::Writer::MarkdownWriter;
    friend class Aspose::Words::Validation::OfficeMathToShapeConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlFormulaToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlGeometryToDmlConverter;
    friend class Aspose::Words::Validation::VmlToDml::VmlShapeToDmlShapeConverter;
    friend class Aspose::Words::RW::Docx::Writer::DocxBackgroundWriter;
    friend class Aspose::Words::RW::Nrx::Reader::NrxRunReaderBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverterUtil;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::Validation::DmlToVml::DmlShapeToShapeConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlContentPartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlDiagramReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlPictureReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlShapeReader;
    friend class Aspose::Words::ApsBuilder::Shapes::Extrusion::ExtrusionBoundsCalculator;
    friend class Aspose::Words::ApsBuilder::Shapes::Shadow::ShadowBuilderParameters;
    friend class Aspose::Words::ApsBuilder::Shapes::InternalColorResolver;
    friend class Aspose::Words::Validation::DmlToVml::DmlUtil;
    friend class Aspose::Words::RW::Html::Writer::HtmlImageWriter;
    friend class Aspose::Words::RW::Odt::Reader::OdtCaptionReader;
    friend class Aspose::Words::Validation::ListValidator;
    friend class Aspose::Words::DocumentBuilder;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::Drawing::ShapeBase;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilderContext;
    friend class Aspose::Words::ApsBuilder::Shapes::BrushFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeTransformer;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;
    friend class Aspose::Words::RW::Html::Css::New::CssHRuleAlignmentStyleConverter;
    friend class Aspose::Words::RW::Html::Css::New::CssHRuleNoShadeStyleConverter;
    friend class Aspose::Words::RW::Html::Css::New::CssWidthPropertyDef;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::Validation::DmlToVml::DmlPictureToShapeConverter;
    friend class Aspose::Words::RW::Odt::Reader::OdtPathReader;
    friend class Aspose::Words::Lists::ListCollection;
    friend class Aspose::Words::ApsBuilder::Shapes::GeometryResolver;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriterBase;
    friend class Aspose::Words::RW::Odt::Reader::OdtCustomShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtEllipseReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtRectReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtConnectorReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtLineReader;
    friend class Aspose::Words::RW::Odt::Writer::OdtEnhancedGeometryWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtShapeWriter;
    friend class Aspose::Words::RW::Odt::Writer::OdtGraphicPropertiesWriter;
    friend class Aspose::Words::RW::Vml::VmlOleReader;
    friend class Aspose::Words::RW::Vml::VmlShapeReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtFrameReader;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::WordUtil;
    friend class Aspose::Words::RW::Doc::Escher::EsShapeFiler;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::Reader::Parser;
    friend class Aspose::Words::RW::Html::Reader::HtmlReader;
    friend class Aspose::Words::RW::Rtf::Reader::RtfPictureHandler;
    friend class Aspose::Words::RW::Rtf::Reader::RtfOleHandler;
    friend class Aspose::Words::RW::Rtf::Writer::RtfOleWriter;
    friend class Aspose::Words::RW::Rtf::Writer::RtfPictureData;
    friend class Aspose::Words::RW::Vml::VmlGeoWriter;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:
    using Aspose::Words::Drawing::ShapeBase::Clone;

public:

    /// Returns <see cref="Aspose::Words::NodeType::Shape">Shape</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    /// Returns <see cref="Aspose::Words::StoryType::Textbox">Textbox</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::StoryType get_StoryType();
    /// Returns true if an extrusion effect is enabled.
    ASPOSE_WORDS_SHARED_API bool get_ExtrusionEnabled();
    /// Returns true if a shadow effect is enabled.
    ASPOSE_WORDS_SHARED_API bool get_ShadowEnabled();
    /// Defines a stroke for a shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Stroke> get_Stroke();
    /// Defines whether the path will be stroked.
    /// 
    /// This is a shortcut to the <see cref="Aspose::Words::Drawing::Stroke::get_On">On</see> property.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_Stroked();
    /// Setter for Aspose::Words::Drawing::Shape::get_Stroked
    ASPOSE_WORDS_SHARED_API void set_Stroked(bool value);
    /// Defines the brush thickness that strokes the path of a shape in points.
    /// 
    /// This is a shortcut to the <see cref="Aspose::Words::Drawing::Stroke::get_Weight">Weight</see> property.
    /// 
    /// The default value is 0.75.
    ASPOSE_WORDS_SHARED_API double get_StrokeWeight();
    /// Setter for Aspose::Words::Drawing::Shape::get_StrokeWeight
    ASPOSE_WORDS_SHARED_API void set_StrokeWeight(double value);
    /// Defines the color of a stroke.
    /// 
    /// This is a shortcut to the <see cref="Aspose::Words::Drawing::Stroke::get_Color">Color</see> property.
    /// 
    /// The default value is
    /// <see cref="System::Drawing::Color::get_Black">Black</see>.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_StrokeColor();
    /// Setter for Aspose::Words::Drawing::Shape::get_StrokeColor
    ASPOSE_WORDS_SHARED_API void set_StrokeColor(System::Drawing::Color value);
    /// Defines a fill for a shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Fill> get_Fill();
    /// Determines whether the closed path of the shape will be filled.
    /// 
    /// This is a shortcut to the <see cref="Aspose::Words::Drawing::Fill::get_On">On</see> property.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_Filled();
    /// Setter for Aspose::Words::Drawing::Shape::get_Filled
    ASPOSE_WORDS_SHARED_API void set_Filled(bool value);
    /// Defines the brush color that fills the closed path of the shape.
    /// 
    /// This is a shortcut to the <see cref="Aspose::Words::Drawing::Fill::get_Color">Color</see> property.
    /// 
    /// The default value is
    /// <see cref="System::Drawing::Color::get_White">White</see>.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_FillColor();
    /// Setter for Aspose::Words::Drawing::Shape::get_FillColor
    ASPOSE_WORDS_SHARED_API void set_FillColor(System::Drawing::Color value);
    /// Returns true if the shape has image bytes or links an image.
    ASPOSE_WORDS_SHARED_API bool get_HasImage();
    /// Provides access to the image of the shape.
    /// Returns null if the shape cannot have an image.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::ImageData> get_ImageData();
    /// Provides access to the OLE data of a shape. For a shape that is not an OLE object or ActiveX control, returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::OleFormat> get_OleFormat();
    /// Defines attributes that specify how text is displayed in a shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::TextBox> get_TextBox();
    /// Defines the text of the text path (of a WordArt object).
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::TextPath> get_TextPath();
    /// Gets the first paragraph in the shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_FirstParagraph();
    /// Gets the last paragraph in the shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Paragraph> get_LastParagraph();
    /// Provides access to the properties of the horizontal rule shape.
    /// For a shape that is not a horizontal rule, returns null.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::HorizontalRuleFormat> get_HorizontalRuleFormat();
    /// Gets <see cref="Aspose::Words::Drawing::Shape::get_SignatureLine">SignatureLine</see> object if the shape is a signature line. Returns <b>null</b> otherwise.
    /// 
    /// @sa Aspose::Words::DocumentBuilder::InsertSignatureLine(System::SharedPtr<Aspose::Words::SignatureLineOptions>, Aspose::Words::Drawing::RelativeHorizontalPosition, double, Aspose::Words::Drawing::RelativeVerticalPosition, double, Aspose::Words::Drawing::WrapType)
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::SignatureLine> get_SignatureLine();
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::TextBoxWrapMode get_TextBoxWrapMode_ITextBox() override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::LayoutFlow get_TextboxLayoutFlow_ITextBox() override;
    ASPOSE_WORDS_SHARED_API bool get_HasVerticalTextFlow_ITextBox() override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::ShapeMarkupLanguage get_MarkupLanguage_ITextBox() override;
    /// Returns true if this Shape has a <see cref="Aspose::Words::Drawing::Shape::get_Chart">Chart</see>.
    ASPOSE_WORDS_SHARED_API bool get_HasChart();
    /// Returns true if this Shape has a SmartArt object.
    ASPOSE_WORDS_SHARED_API bool get_HasSmartArt();
    /// Provides access to the chart properties if this shape has a Chart.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Chart> get_Chart();

    /// Creates a new shape object.
    /// 
    /// You should specify desired shape properties after you created a shape.
    /// 
    /// @param doc The owner document.
    /// @param shapeType The type of the shape to create.
    ASPOSE_WORDS_SHARED_API Shape(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::Drawing::ShapeType shapeType);

    /// Accepts a visitor.
    /// 
    /// Enumerates over this node and all of its children. Each node calls a corresponding method on DocumentVisitor.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the nodes.
    /// 
    /// @return True if all nodes were visited; false if DocumentVisitor stopped the operation before visiting all nodes.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API float GetHorizontalMargins_ITextBox() override;
    /// Updates SmartArt pre-rendered drawing by using Aspose.Words's SmartArt cold rendering engine.
    ASPOSE_WORDS_SHARED_API void UpdateSmartArtDrawing();

protected:

    ASPOSE_WORDS_SHARED_API bool get_IsPercentWidthInapplicable() override;
    System::String get_ScriptText();
    void set_ScriptText(System::String value);
    System::String get_ScriptType();
    void set_ScriptType(System::String value);
    bool get_HasImageBytes();
    bool get_IsOoxmlControl();
    System::SharedPtr<Aspose::Words::Drawing::Core::HorizontalRule> get_HorizontalRule();
    Aspose::Words::Drawing::Core::ConnectorType get_ConnectorType();
    void set_ConnectorType(Aspose::Words::Drawing::Core::ConnectorType value);
    Aspose::Words::Drawing::Core::OleObjectType get_OleObjectType();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::PathInfo>> get_SegmentInfo();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::PathPoint>> get_Vertices();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::Formula>> get_Formulas();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ImageRecolor();
    int32_t get_LimoX();
    void set_LimoX(int32_t value);
    int32_t get_LimoY();
    void set_LimoY(int32_t value);
    bool get_IsFitTextboxToText();
    void set_IsFitTextboxToText(bool value);
    int32_t get_ShadowOpacity();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ShadowColor();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ShadowHighlight();
    int32_t get_ShadowOffsetX();
    int32_t get_ShadowOffsetY();
    int32_t get_ShadowSecondOffsetX();
    int32_t get_ShadowSecondOffsetY();
    int32_t get_ShadowScaleXtoX();
    int32_t get_ShadowScaleYtoY();
    int32_t get_ShadowScaleXtoY();
    int32_t get_ShadowScaleYtoX();
    int32_t get_ShadowOriginX();
    int32_t get_ShadowOriginY();
    int32_t get_ShadowPerspectiveX();
    int32_t get_ShadowPerspectiveY();
    Aspose::Words::Drawing::Core::ShadowType get_ShadowType();
    bool get_IsParallelExtrusion();
    int32_t get_ExtrusionSkewAngle();
    int32_t get_ExtrusionForeDepth();
    int32_t get_ExtrusionBackDepth();
    System::SharedPtr<Aspose::Drawing::DrColor> get_ExtrusionColor();
    bool get_ShouldUpdateRelativeSizeConsideringExtrusion();
    int32_t get_ViewpointX();
    int32_t get_ViewpointY();
    int32_t get_ViewpointZ();
    int32_t get_ViewpointOriginX();
    int32_t get_ViewpointOriginY();
    int32_t get_TDRotationAngleX();
    int32_t get_TDRotationAngleY();
    int32_t get_TDLight1PositionX();
    int32_t get_TDLight1PositionY();
    int32_t get_TDLight1PositionZ();
    int32_t get_TDLight1Intensity();
    bool get_TDLight1Harsh();
    int32_t get_TDLight2PositionX();
    int32_t get_TDLight2PositionY();
    int32_t get_TDLight2PositionZ();
    int32_t get_TDLight2Intensity();
    bool get_TDLight2Harsh();
    int32_t get_TDAmbientIntensity();
    int32_t get_TDSpecularAmount();
    int32_t get_TDDiffuseAmount();
    bool get_TDMetallic();
    Aspose::Words::Drawing::Core::ThreeDRenderMode get_RenderMode();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::Handle>> get_Handles();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::PathRectangle>> get_TextBoxRects();
    void set_TextBoxRects(System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::PathRectangle>> value);
    bool get_IsTextureRotated();
    bool get_IgnoreFill();
    bool get_HasFillImage();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ChartCollection> get_Charts();

    Shape(System::SharedPtr<Aspose::Words::DocumentBase> doc);
    Shape(System::SharedPtr<Aspose::Words::DocumentBase> doc, Aspose::Words::Drawing::ShapeMarkupLanguage markupLanguage);

    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptStart(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::VisitorAction AcceptEnd(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> Clone(bool isCloneChildren, System::SharedPtr<Aspose::Words::INodeCloningListener> cloningListener) override;
    ASPOSE_WORDS_SHARED_API bool CanInsert(System::SharedPtr<Aspose::Words::Node> newChild) override;
    static System::SharedPtr<Aspose::Words::Drawing::Shape> CreateHorizontalRule(System::SharedPtr<Aspose::Words::Document> doc);
    static System::SharedPtr<Aspose::Words::Drawing::Shape> CreateShapeFromAps(System::SharedPtr<Aspose::Words::Document> doc, System::SharedPtr<Aspose::Rendering::Aps::ApsNode> apsNode, double width, double height);
    static bool IsVisibleAsBackground(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);
    void FixZeroSize(bool isFromDefault);
    void ReplaceVmlImageDataWithFill();
    int32_t GetAdjust(int32_t index);

    virtual ASPOSE_WORDS_SHARED_API ~Shape();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Stroke> mStrokeCache;
    System::SharedPtr<Aspose::Words::Drawing::Fill> mFillCache;
    System::SharedPtr<Aspose::Words::Drawing::TextBox> mTextBoxCache;
    System::SharedPtr<Aspose::Words::Drawing::ImageData> mImageDataCache;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ChartCollection> mChartCollectionCache;
    System::SharedPtr<Aspose::Words::Drawing::SignatureLine> mSignatureLineCache;
    System::SharedPtr<Aspose::Words::Drawing::HorizontalRuleFormat> mHorizontalRuleFormatCache;

    static bool CanInsertInWpCanvasAndWpGroupShape(Aspose::Words::Drawing::Core::Dml::DmlNodeType dmlNodeType);
    static bool CanInsertInLockedCanvasAndGroupShape(Aspose::Words::Drawing::Core::Dml::DmlNodeType dmlNodeType);
    static bool CanInsertInCompositeShapeExcludeGrFrame(Aspose::Words::Drawing::Core::Dml::DmlNodeType dmlNodeType);
    static bool CanInsertInChart(Aspose::Words::Drawing::Core::Dml::DmlNodeType dmlNodeType);
    bool HasVerticalTextFlow();
    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
}
