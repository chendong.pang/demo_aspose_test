//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/TextBox.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/TextBoxWrapMode.h"
#include "Aspose.Words.Cpp/Model/Drawing/TextBoxAnchor.h"
#include "Aspose.Words.Cpp/Model/Drawing/LayoutFlow.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Text { class DmlTextShapeToShapeConvertor; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlShapeValidator; } } } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace Drawing { class ShapeBase; } } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedIntegerListGeneric; } } }
namespace Aspose { namespace Words { enum class NodeType; } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Defines attributes that specify how a text is displayed inside a shape.
/// 
/// Use the <see cref="Aspose::Words::Drawing::Shape::get_TextBox">TextBox</see> property to access text properties of a shape.
/// You do not create instances of the <see cref="Aspose::Words::Drawing::TextBox">TextBox</see> class directly.
/// 
/// @sa Aspose::Words::Drawing::Shape::get_TextBox
class ASPOSE_WORDS_SHARED_CLASS TextBox : public System::Object
{
    typedef TextBox ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Text::DmlTextShapeToShapeConvertor;
    friend class Aspose::Words::RW::Dml::Writer::DmlShapeValidator;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;

public:

    /// Specifies the inner left margin in points for a shape.
    /// 
    /// The default value is 1/10 inch.
    ASPOSE_WORDS_SHARED_API double get_InternalMarginLeft();
    /// Specifies the inner left margin in points for a shape.
    /// 
    /// The default value is 1/10 inch.
    ASPOSE_WORDS_SHARED_API void set_InternalMarginLeft(double value);
    /// Specifies the inner right margin in points for a shape.
    /// 
    /// The default value is 1/10 inch.
    ASPOSE_WORDS_SHARED_API double get_InternalMarginRight();
    /// Specifies the inner right margin in points for a shape.
    /// 
    /// The default value is 1/10 inch.
    ASPOSE_WORDS_SHARED_API void set_InternalMarginRight(double value);
    /// Specifies the inner top margin in points for a shape.
    /// 
    /// The default value is 1/20 inch.
    ASPOSE_WORDS_SHARED_API double get_InternalMarginTop();
    /// Specifies the inner top margin in points for a shape.
    /// 
    /// The default value is 1/20 inch.
    ASPOSE_WORDS_SHARED_API void set_InternalMarginTop(double value);
    /// Specifies the inner bottom margin in points for a shape.
    /// 
    /// The default value is 1/20 inch.
    ASPOSE_WORDS_SHARED_API double get_InternalMarginBottom();
    /// Specifies the inner bottom margin in points for a shape.
    /// 
    /// The default value is 1/20 inch.
    ASPOSE_WORDS_SHARED_API void set_InternalMarginBottom(double value);
    /// Determines whether Microsoft Word will grow the shape to fit text.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_FitShapeToText();
    /// Determines whether Microsoft Word will grow the shape to fit text.
    /// 
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_FitShapeToText(bool value);
    /// Determines the flow of the text layout in a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::LayoutFlow::Horizontal">Horizontal</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::LayoutFlow get_LayoutFlow();
    /// Determines the flow of the text layout in a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::LayoutFlow::Horizontal">Horizontal</see>.
    ASPOSE_WORDS_SHARED_API void set_LayoutFlow(Aspose::Words::Drawing::LayoutFlow value);
    /// Determines how text wraps inside a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::TextBoxWrapMode::Square">Square</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::TextBoxWrapMode get_TextBoxWrapMode();
    /// Determines how text wraps inside a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::TextBoxWrapMode::Square">Square</see>.
    ASPOSE_WORDS_SHARED_API void set_TextBoxWrapMode(Aspose::Words::Drawing::TextBoxWrapMode value);
    /// Specifies the vertical alignment of the text within a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::TextBoxAnchor::Top">Top</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::TextBoxAnchor get_VerticalAnchor();
    /// Specifies the vertical alignment of the text within a shape.
    /// 
    /// The default value is <see cref="Aspose::Words::Drawing::TextBoxAnchor::Top">Top</see>.
    ASPOSE_WORDS_SHARED_API void set_VerticalAnchor(Aspose::Words::Drawing::TextBoxAnchor value);
    /// Returns a TextBox that represents the next TextBox in a sequence of shapes.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::TextBox> get_Next();
    /// Sets a TextBox that represents the next TextBox in a sequence of shapes.
    ASPOSE_WORDS_SHARED_API void set_Next(System::SharedPtr<Aspose::Words::Drawing::TextBox> value);
    /// Returns a TextBox that represents the previous TextBox in a sequence of shapes.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::TextBox> get_Previous();
    /// Gets a parent shape for the TextBox.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Shape> get_Parent() const;

    /// Determines whether this TextBox can be linked to the target Textbox.
    ASPOSE_WORDS_SHARED_API bool IsValidLinkTarget(System::SharedPtr<Aspose::Words::Drawing::TextBox> target);
    /// Breaks the link to the next TextBox.
    ASPOSE_WORDS_SHARED_API void BreakForwardLink();

protected:

    Aspose::Words::Drawing::TextBoxAnchor get_TextBoxAnchor();
    void set_TextBoxAnchor(Aspose::Words::Drawing::TextBoxAnchor value);

    TextBox(System::SharedPtr<Aspose::Words::Drawing::Shape> parent);

    static bool IsNext(System::SharedPtr<Aspose::Words::Drawing::Shape> sourceShape, System::SharedPtr<Aspose::Words::Drawing::Shape> targetShape);

    virtual ASPOSE_WORDS_SHARED_API ~TextBox();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Drawing::Shape> mParent;
    System::SharedPtr<Aspose::Words::Drawing::TextBox> mPrevious;
    System::SharedPtr<Aspose::Words::Drawing::TextBox> mNext;

    void CreateNewChain(System::SharedPtr<Aspose::Collections::Generic::SortedIntegerListGeneric<System::SharedPtr<Aspose::Words::Drawing::ShapeBase>>> linkedChain, int32_t firstIndex, int32_t lastIndex);
    static bool IsNext(System::SharedPtr<Aspose::Words::Drawing::TextBox> source, System::SharedPtr<Aspose::Words::Drawing::TextBox> target);
    void AddLink(System::SharedPtr<Aspose::Words::Drawing::TextBox> target);
    System::String ValidateLinkTarget(System::SharedPtr<Aspose::Words::Drawing::TextBox> target);
    static System::SharedPtr<Aspose::Words::Drawing::TextBox> GetFallBackTextBox(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);
    bool DifferentStoryTypes(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> targetShape, Aspose::Words::NodeType nodeType);
    static bool ShapeCanStoreText(System::SharedPtr<Aspose::Words::Drawing::ShapeBase> shape);

};

}
}
}
