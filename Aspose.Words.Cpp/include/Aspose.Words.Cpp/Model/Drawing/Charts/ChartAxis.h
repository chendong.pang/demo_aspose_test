//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartAxis.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <drawing/size_f.h>
#include <drawing/rectangle_f.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/SimpleTypes/TitlePosition.h"
#include "Aspose.Words.Cpp/Model/Text/ParagraphAlignment.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/INumberFormatProvider.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/IDmlChartTitleHolder.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/ChartAxisType.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisTimeUnit.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisTickMark.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisTickLabelPosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisCrosses.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisCategoryType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisNumericDataLogarithmic; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisData; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeriesCollection; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartAxisReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabel; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class AxisDisplayUnit; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisDateData; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisNumericData; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPlotArea; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeries; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderBase; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlBarChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataTableRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlane; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneCartesian; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneCartesian3D; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlanePolar; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneSurface; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlotAreaRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartSpaceRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlLineChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlSurfaceChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartAxisWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class LabelAlignment; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class AxisPosition; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class CrossBetween; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartNumberFormat; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class AxisScaling; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisPr; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartGridlines; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { class DoubleOrAutomatic; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartNumFormat; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTxPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartTitle; } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderingContext; } } } } }
namespace Aspose { namespace Drawing { class DrPen; } }
namespace Aspose { namespace Drawing { namespace Fonts { class DrFont; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTx; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartValue; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents the axis options of the chart.
class ASPOSE_WORDS_SHARED_CLASS ChartAxis : public Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder, public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource, public Aspose::Words::Drawing::Charts::Core::INumberFormatProvider
{
    typedef ChartAxis ThisType;
    typedef Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder BaseType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType1;
    typedef Aspose::Words::Drawing::Charts::Core::INumberFormatProvider BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisNumericDataLogarithmic;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisData;
    friend class Aspose::Words::Drawing::Charts::ChartSeriesCollection;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartAxisReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabel;
    friend class Aspose::Words::Drawing::Charts::AxisDisplayUnit;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisDateData;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisNumericData;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea;
    friend class Aspose::Words::Drawing::Charts::ChartSeries;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderBase;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlBarChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataTableRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlane;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneCartesian;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneCartesian3D;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlanePolar;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneSurface;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlotAreaRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartSpaceRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlLineChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlSurfaceChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartAxisWriter;

public:

    /// Returns type of the axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::ChartAxisType get_Type() const;
    /// Gets type of the category axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisCategoryType get_CategoryType();
    /// Sets type of the category axis.
    ASPOSE_WORDS_SHARED_API void set_CategoryType(Aspose::Words::Drawing::Charts::AxisCategoryType value);
    /// Specifies how this axis crosses the perpendicular axis.
    /// 
    /// Default value is <see cref="Aspose::Words::Drawing::Charts::AxisCrosses::Automatic">Automatic</see>.
    /// 
    /// The property is not supported by MS Office 2016 new charts.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisCrosses get_Crosses();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_Crosses
    ASPOSE_WORDS_SHARED_API void set_Crosses(Aspose::Words::Drawing::Charts::AxisCrosses value);
    /// Specifies where on the perpendicular axis the axis crosses.
    /// 
    /// The property has effect only if <see cref="Aspose::Words::Drawing::Charts::ChartAxis::get_Crosses">Crosses</see> are set to <see cref="Aspose::Words::Drawing::Charts::AxisCrosses::Custom">Custom</see>.
    /// It is not supported by MS Office 2016 new charts.
    /// 
    /// The units are determined by the type of axis. When the axis is a value axis, the value of the property
    /// is a decimal number on the value axis. When the axis is a time category axis, the value is defined as
    /// an integer number of days relative to the base date (30/12/1899). For a text category axis, the value is
    /// an integer category number, starting with 1 as the first category.
    ASPOSE_WORDS_SHARED_API double get_CrossesAt();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_CrossesAt
    ASPOSE_WORDS_SHARED_API void set_CrossesAt(double value);
    /// Returns a flag indicating whether values of axis should be displayed in reverse order, i.e.
    /// from max to min.
    ASPOSE_WORDS_SHARED_API bool get_ReverseOrder();
    /// Sets a flag indicating whether values of axis should be displayed in reverse order, i.e.
    /// from max to min.
    ASPOSE_WORDS_SHARED_API void set_ReverseOrder(bool value);
    /// Returns the major tick marks.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTickMark get_MajorTickMark();
    /// Sets the major tick marks.
    ASPOSE_WORDS_SHARED_API void set_MajorTickMark(Aspose::Words::Drawing::Charts::AxisTickMark value);
    /// Returns the minor tick marks for the axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTickMark get_MinorTickMark();
    /// Sets the minor tick marks for the axis.
    ASPOSE_WORDS_SHARED_API void set_MinorTickMark(Aspose::Words::Drawing::Charts::AxisTickMark value);
    /// Returns the position of the tick labels on the axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTickLabelPosition get_TickLabelPosition();
    /// Sets the position of the tick labels on the axis.
    ASPOSE_WORDS_SHARED_API void set_TickLabelPosition(Aspose::Words::Drawing::Charts::AxisTickLabelPosition value);
    /// Returns or sets the distance between major tick marks.
    /// 
    /// Valid range of a value is greater than zero. The property has effect for time category and
    /// value axes.
    /// 
    /// Setting this property sets the <see cref="Aspose::Words::Drawing::Charts::ChartAxis::get_MajorUnitIsAuto">MajorUnitIsAuto</see> property to <b>false</b>.
    ASPOSE_WORDS_SHARED_API double get_MajorUnit();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_MajorUnit
    ASPOSE_WORDS_SHARED_API void set_MajorUnit(double value);
    /// Gets a flag indicating whether default distance between major tick marks shall be used.
    ASPOSE_WORDS_SHARED_API bool get_MajorUnitIsAuto();
    /// Sets a flag indicating whether default distance between major tick marks shall be used.
    ASPOSE_WORDS_SHARED_API void set_MajorUnitIsAuto(bool value);
    /// Returns the scale value for major tick marks on the time category axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTimeUnit get_MajorUnitScale();
    /// Sets the scale value for major tick marks on the time category axis.
    ASPOSE_WORDS_SHARED_API void set_MajorUnitScale(Aspose::Words::Drawing::Charts::AxisTimeUnit value);
    /// Returns or sets the distance between minor tick marks.
    /// 
    /// Valid range of a value is greater than zero. The property has effect for time category and
    /// value axes.
    /// 
    /// Setting this property sets the <see cref="Aspose::Words::Drawing::Charts::ChartAxis::get_MinorUnitIsAuto">MinorUnitIsAuto</see> property to <b>false</b>.
    ASPOSE_WORDS_SHARED_API double get_MinorUnit();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_MinorUnit
    ASPOSE_WORDS_SHARED_API void set_MinorUnit(double value);
    /// Gets a flag indicating whether default distance between minor tick marks shall be used.
    ASPOSE_WORDS_SHARED_API bool get_MinorUnitIsAuto();
    /// Sets a flag indicating whether default distance between minor tick marks shall be used.
    ASPOSE_WORDS_SHARED_API void set_MinorUnitIsAuto(bool value);
    /// Returns the scale value for minor tick marks on the time category axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTimeUnit get_MinorUnitScale();
    /// Sets the scale value for minor tick marks on the time category axis.
    ASPOSE_WORDS_SHARED_API void set_MinorUnitScale(Aspose::Words::Drawing::Charts::AxisTimeUnit value);
    /// Returns the smallest time unit that is represented on the time category axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisTimeUnit get_BaseTimeUnit();
    /// Sets the smallest time unit that is represented on the time category axis.
    ASPOSE_WORDS_SHARED_API void set_BaseTimeUnit(Aspose::Words::Drawing::Charts::AxisTimeUnit value);
    /// Returns a <see cref="Aspose::Words::Drawing::Charts::ChartNumberFormat">ChartNumberFormat</see> object that allows defining number formats for the axis.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> get_NumberFormat();
    /// Gets or sets the distance of labels from the axis.
    /// 
    /// The property represents a percentage of the default label offset.
    /// 
    /// Valid range is from 0 to 1000 percent inclusive. Default value is 100\%.
    /// 
    /// The property has effect only for category axes. It is not supported by MS Office 2016 new charts.
    ASPOSE_WORDS_SHARED_API int32_t get_TickLabelOffset();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_TickLabelOffset
    ASPOSE_WORDS_SHARED_API void set_TickLabelOffset(int32_t value);
    /// Specifies the scaling value of the display units for the value axis.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::AxisDisplayUnit> get_DisplayUnit();
    /// Gets a flag indicating whether the value axis crosses the category axis between categories.
    ASPOSE_WORDS_SHARED_API bool get_AxisBetweenCategories();
    /// Sets a flag indicating whether the value axis crosses the category axis between categories.
    ASPOSE_WORDS_SHARED_API void set_AxisBetweenCategories(bool value);
    /// Provides access to the scaling options of the axis.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::AxisScaling> get_Scaling();
    /// Gets or sets the interval, at which tick labels are drawn.
    /// 
    /// The property has effect for text category and series axes. It is not supported by MS Office 2016
    /// new charts. Valid range of a value is greater than or equal to 1.
    /// 
    /// Setting this property sets the <see cref="Aspose::Words::Drawing::Charts::ChartAxis::get_TickLabelSpacingIsAuto">TickLabelSpacingIsAuto</see> property to <b>false</b>.
    ASPOSE_WORDS_SHARED_API int32_t get_TickLabelSpacing();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_TickLabelSpacing
    ASPOSE_WORDS_SHARED_API void set_TickLabelSpacing(int32_t value);
    /// Gets or sets a flag indicating whether automatic interval of drawing tick labels shall be used.
    /// 
    /// Default value is <b>true</b>.
    /// 
    /// The property has effect for text category and series axes. It is not supported by MS Office 2016
    /// new charts.
    ASPOSE_WORDS_SHARED_API bool get_TickLabelSpacingIsAuto();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_TickLabelSpacingIsAuto
    ASPOSE_WORDS_SHARED_API void set_TickLabelSpacingIsAuto(bool value);
    /// Gets or sets text alignment of axis tick labels.
    /// 
    /// This property has effect only for multi-line labels.
    /// 
    /// Default value is <see cref="Aspose::Words::ParagraphAlignment::Center">Center</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::ParagraphAlignment get_TickLabelAlignment();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_TickLabelAlignment
    ASPOSE_WORDS_SHARED_API void set_TickLabelAlignment(Aspose::Words::ParagraphAlignment value);
    /// Gets or sets the interval, at which tick marks are drawn.
    /// 
    /// The property has effect for text category and series axes. It is not supported by MS Office 2016
    /// new charts.
    /// 
    /// Valid range of a value is greater than or equal to 1.
    ASPOSE_WORDS_SHARED_API int32_t get_TickMarkSpacing();
    /// Setter for Aspose::Words::Drawing::Charts::ChartAxis::get_TickMarkSpacing
    ASPOSE_WORDS_SHARED_API void set_TickMarkSpacing(int32_t value);
    /// Gets a flag indicating whether this axis is hidden or not.
    ASPOSE_WORDS_SHARED_API bool get_Hidden();
    /// Sets a flag indicating whether this axis is hidden or not.
    ASPOSE_WORDS_SHARED_API void set_Hidden(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> get_Title() override;
    ASPOSE_WORDS_SHARED_API void set_Title(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> value) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::Core::SimpleTypes::TitlePosition get_TitlePosition() override;
    /// Returns the Document the title holder belongs.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() override;
    ASPOSE_WORDS_SHARED_API bool get_TitleDeleted() override;
    ASPOSE_WORDS_SHARED_API void set_TitleDeleted(bool value) override;
    ASPOSE_WORDS_SHARED_API bool get_IsVisible() override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt_INumberFormatProvider() override;
    ASPOSE_WORDS_SHARED_API void set_NumFmt_INumberFormatProvider(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> GenerateAutoTitle(System::SharedPtr<Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext> context) override;

protected:

    bool get_IsDateCategoryAxis();
    bool get_IsDate();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisPr> get_ChartAxisPr() const;
    void set_ChartAxisPr(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisPr> value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::LabelAlignment get_LblAlgn();
    void set_LblAlgn(Aspose::Words::Drawing::Charts::Core::SimpleTypes::LabelAlignment value);
    int32_t get_AxId();
    void set_AxId(int32_t value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::AxisPosition get_AxPos();
    void set_AxPos(Aspose::Words::Drawing::Charts::Core::SimpleTypes::AxisPosition value);
    int32_t get_CrossAx();
    void set_CrossAx(int32_t value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::CrossBetween get_CrossBetween();
    void set_CrossBetween(Aspose::Words::Drawing::Charts::Core::SimpleTypes::CrossBetween value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartGridlines> get_MajorGridlines();
    void set_MajorGridlines(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartGridlines> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartGridlines> get_MinorGridlines();
    void set_MinorGridlines(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartGridlines> value);
    bool get_NoMultiLvlLbl();
    void set_NoMultiLvlLbl(bool value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::SimpleTypes::DoubleOrAutomatic> get_MajorUnitPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::SimpleTypes::DoubleOrAutomatic> get_MinorUnitPr();
    bool get_TickMarkSpacingIsAuto();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt();
    void set_NumFmt(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value);
    System::String get_FormatCode();
    void set_FormatCode(System::String value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> get_TxPr();
    bool get_AreTickLabelsVisible();
    void set_AreTickLabelsVisible(bool value);
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_TickLabelExtensions();
    void set_TickLabelExtensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value);
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_MajorTickMarkExtensions();
    void set_MajorTickMarkExtensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value);
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_MinorTickMarkExtensions();
    void set_MinorTickMarkExtensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> get_TitleInternal();
    void set_TitleInternal(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea> get_PlotArea() const;
    void set_PlotArea(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea> value);
    System::Drawing::RectangleF get_TargetRectangle() const;
    void set_TargetRectangle(System::Drawing::RectangleF value);
    System::SharedPtr<Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext> get_RenderingContext() const;
    void set_RenderingContext(System::SharedPtr<Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext> value);
    float get_DefaultAddition();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisData> get_Data();
    void set_Data(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisData> value);
    bool get_IsVertical();
    bool get_IsCategory();
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> get_CrossAxis();
    void set_CrossAxis(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> value);
    bool get_AreLabelsBetween();
    bool get_HideTickLabels();
    bool get_AreLabelsBefore();
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::AxisPosition get_ActualAxisPosition();
    bool get_CanPlotAreaOverlapLabels();
    bool get_IsPercentStacked() const;
    void set_IsPercentStacked(bool value);
    bool get_Is3D() const;
    void set_Is3D(bool value);
    bool get_Is2DSurface() const;
    void set_Is2DSurface(bool value);
    float get_LabelHeight();
    float get_TickLineLength();
    float get_DistanceToLabel();
    System::SharedPtr<Aspose::Drawing::DrPen> get_Pen();
    double get_LabelsRotation();
    System::SharedPtr<Aspose::Drawing::Fonts::DrFont> get_Font();
    double get_Unit();
    bool get_IsLogScaled();
    bool get_IsStaked() const;
    void set_IsStaked(bool value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_Chart() const;

    ChartAxis(Aspose::Words::Drawing::Charts::ChartAxisType axisType, System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> Clone();
    System::Drawing::SizeF GetLabelSizeF(System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue>> values, int32_t stepIteration);
    void SetType(Aspose::Words::Drawing::Charts::ChartAxisType value);
    void SetDisplayUnit(System::SharedPtr<Aspose::Words::Drawing::Charts::AxisDisplayUnit> value);
    void SetChart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    virtual ASPOSE_WORDS_SHARED_API ~ChartAxis();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisPr> pr_ChartAxisPr;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;
    System::WeakPtr<Aspose::Words::Drawing::Charts::ChartAxis> mCrossAxis;
    Aspose::Words::Drawing::Charts::ChartAxisType mAxisType;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartAxisData> mData;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea> mPlotArea;
    System::Drawing::RectangleF mPlotAreaTargetRect;
    System::SharedPtr<Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext> mContext;
    bool mIsPercentStacked;
    bool mIsStacked;
    bool mIs3D;
    bool mIs2DSurface;
    System::String mFormatCode;
    float mLabelHiegth;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> mTitle;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> mNumberFormat;
    System::SharedPtr<Aspose::Drawing::DrPen> mPen;
    System::SharedPtr<Aspose::Drawing::Fonts::DrFont> mFont;

    static const System::String& FontArailNarrow();
    bool UseDummyNumString(System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue>> values, int32_t stepIteration);

};

}
}
}
}
