//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartSeries.h
#pragma once

#include <system/string.h>
#include <system/collections/list.h>
#include <system/collections/ilist.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/IChartDataPoint.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeriesCollection; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartSeriesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabel; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabelCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataPointCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartErrorBars; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTrendline; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlBubbleChart; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisDateData; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartFormat; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPlotArea; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPr; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderingContext; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlAreaChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderBase; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlBarChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlBubbleChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChart3DHelper; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataLabelRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataRowRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataTableRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartErrorBarsRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlane; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneCartesian3D; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderingUtil; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartTrendlineRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlLineChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlOfPieChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlPieChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlScatterChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlStockChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlSurfaceChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartSeriesWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class BarShape; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class SeriesLayout; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataPoint; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartMarker; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDataSource; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartValue; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartData; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTx; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartValueColors; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSeriesLayoutPr; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents chart series properties.
class ASPOSE_WORDS_SHARED_CLASS ChartSeries : public Aspose::Words::Drawing::Charts::IChartDataPoint, public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef ChartSeries ThisType;
    typedef Aspose::Words::Drawing::Charts::IChartDataPoint BaseType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChart;
    friend class Aspose::Words::Drawing::Charts::ChartSeriesCollection;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartSeriesReader;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabel;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabelCollection;
    friend class Aspose::Words::Drawing::Charts::ChartDataPointCollection;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTrendline;
    friend class Aspose::Words::Drawing::Charts::Core::DmlBubbleChart;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisDateData;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartFormat;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPr;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlAreaChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderBase;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlBarChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlBubbleChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChart3DHelper;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataLabelRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataRowRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataTableRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartErrorBarsRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlane;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneCartesian3D;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingUtil;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartTrendlineRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlLineChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlOfPieChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlPieChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlScatterChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlStockChartRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlSurfaceChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartSeriesWriter;

public:

    ASPOSE_WORDS_SHARED_API int32_t get_Explosion() override;
    ASPOSE_WORDS_SHARED_API void set_Explosion(int32_t value) override;
    ASPOSE_WORDS_SHARED_API bool get_InvertIfNegative() override;
    ASPOSE_WORDS_SHARED_API void set_InvertIfNegative(bool value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartMarker> get_Marker() override;
    ASPOSE_WORDS_SHARED_API bool get_Bubble3D() override;
    ASPOSE_WORDS_SHARED_API void set_Bubble3D(bool value) override;
    /// Returns a collection of formatting objects for all data points in this series.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPointCollection> get_DataPoints() const;
    /// Gets the name of the series, if name is not set explicitly it is generated using index.
    /// By default returns Series plus one based index.
    ASPOSE_WORDS_SHARED_API System::String get_Name();
    /// Sets the name of the series, if name is not set explicitly it is generated using index.
    /// By default returns Series plus one based index.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Allows to specify whether the line connecting the points on the chart shall be smoothed using Catmull-Rom splines.
    ASPOSE_WORDS_SHARED_API bool get_Smooth() const;
    /// Allows to specify whether the line connecting the points on the chart shall be smoothed using Catmull-Rom splines.
    ASPOSE_WORDS_SHARED_API void set_Smooth(bool value);
    /// Gets a flag indicating whether data labels are displayed for the series.
    ASPOSE_WORDS_SHARED_API bool get_HasDataLabels() const;
    /// Sets a flag indicating whether data labels are displayed for the series.
    ASPOSE_WORDS_SHARED_API void set_HasDataLabels(bool value);
    /// Specifies the settings for the data labels for the entire series.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> get_DataLabels();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> get_DefaultDataPoint();
    ASPOSE_WORDS_SHARED_API void set_DataLabels(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> value);
    int32_t get_Index() const;
    void set_Index(int32_t value);
    int32_t get_Order() const;
    void set_Order(int32_t value);
    bool get_SmoothExplicitlySet() const;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::BarShape get_Shape() const;
    void set_Shape(Aspose::Words::Drawing::Charts::Core::SimpleTypes::BarShape value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_XValuesInherited();
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue>> get_OrderedXValuesArray();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_XValues();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_YValues();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_SourceYValues();
    int32_t get_PointsCount();
    int32_t get_NotEmptyPointsCount();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_BubbleSize();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars> get_XErrorBars() const;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars> get_YErrorBars() const;
    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTrendline>>> get_Trendlines() const;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> get_Tx() const;
    void set_Tx(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_Chart() const;
    bool get_HasValues();
    bool get_HasDataLabelsRangeData();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> get_DataLabelsRangeData();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueColors> get_ValueColors() const;
    void set_ValueColors(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueColors> value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SeriesLayout get_LayoutType() const;
    void set_LayoutType(Aspose::Words::Drawing::Charts::Core::SimpleTypes::SeriesLayout value);
    bool get_Hidden() const;
    void set_Hidden(bool value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> get_Owner() const;
    void set_Owner(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> value);
    System::String get_UniqueId() const;
    void set_UniqueId(System::String value);
    int32_t get_FormatIndex() const;
    void set_FormatIndex(int32_t value);
    int32_t get_DataId() const;
    void set_DataId(int32_t value);
    int32_t get_AxisId() const;
    void set_AxisId(int32_t value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSeriesLayoutPr> get_LayoutPr();
    bool get_IsScatterChartSeries();
    bool get_IsBubbleChartSeries();
    bool get_IsPieChartSeries();

    ChartSeries(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    void SetChart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> Clone();
    void AddErrorBars(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars> errorBars);
    void AddTrendline(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTrendline> trendline);
    int32_t GetIndexInOrderedXValuesArray(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> xValue);
    int32_t GetIndexInOrderedXValuesArray(int32_t index);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> GetDataPoint(int32_t i);
    void EndReading();

    virtual ASPOSE_WORDS_SHARED_API ~ChartSeries();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool pr_HasDataLabels;

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartData> get_ChartExChartData();

    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue>> mOrderedXValuesArray;
    int32_t mIndex;
    int32_t mOrder;
    bool mSmooth;
    bool mSmoothExplicitlySet;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::BarShape mShape;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> mXVal;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> mInheritedXVal;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> mYVal;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> mSourceYVal;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> mBubbleSize;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> mDLbls;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTrendline>>> mTrendlines;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> mTx;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> mDefaultDPt;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPointCollection> mDPtCollection;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars> mXErrBars;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartErrorBars> mYErrBars;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueColors> mValueColors;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SeriesLayout mLayoutType;
    bool mHidden;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> mOwner;
    System::String mUniqueId;
    int32_t mFormatIndex;
    int32_t mDataId;
    int32_t mAxisId;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSeriesLayoutPr> mLayoutPr;

    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue>> GetOrderedXValuesArrayInternal();
    void AddDummyXData();
    void BackUpYValues();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> SearchForProperXValuesSource();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataSource> GetDmlChartDataSource();

};

}
}
}
}
