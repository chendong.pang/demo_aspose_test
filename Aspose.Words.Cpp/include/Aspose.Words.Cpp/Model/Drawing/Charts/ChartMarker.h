//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartMarker.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/MarkerSymbol.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeriesCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDataPointPr; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataPoint; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartPivotFormat; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartLegendMarkerRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartMarkerRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartMarkerPr; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents a chart data marker.
class ASPOSE_WORDS_SHARED_CLASS ChartMarker : public System::Object
{
    typedef ChartMarker ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Drawing::Charts::ChartSeriesCollection;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataPointPr;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::ChartDataPoint;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartPivotFormat;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartLegendMarkerRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartMarkerRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Gets chart marker symbol.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::MarkerSymbol get_Symbol();
    /// Sets chart marker symbol.
    ASPOSE_WORDS_SHARED_API void set_Symbol(Aspose::Words::Drawing::Charts::MarkerSymbol value);
    /// Gets chart marker size.
    /// Default value is 7.
    ASPOSE_WORDS_SHARED_API int32_t get_Size();
    /// Sets chart marker size.
    /// Default value is 7.
    ASPOSE_WORDS_SHARED_API void set_Size(int32_t value);

    ASPOSE_WORDS_SHARED_API ChartMarker();

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartMarkerPr> get_MarkerPr() const;
    bool get_IsDefaultSize();

    static const int32_t DefaultMarkerSize;

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartMarker> Clone();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartMarkerPr> mMarkerPr;

};

}
}
}
}
