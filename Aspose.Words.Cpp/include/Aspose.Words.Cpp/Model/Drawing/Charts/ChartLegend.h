//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartLegend.h
#pragma once

#include <system/shared_ptr.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/LegendPosition.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartFormat; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataLabelRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartLegendRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class SidePosition; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class PositionAlignment; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartManualLayout; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTxPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartLegendEntry; } } } } } }
namespace Aspose { namespace Collections { template<typename> class IntToObjDictionary; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents chart legend properties.
class ASPOSE_WORDS_SHARED_CLASS ChartLegend : public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef ChartLegend ThisType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartFormat;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataLabelRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartLegendRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Specifies the position of the legend on a chart.
    /// Default value is <see cref="Aspose::Words::Drawing::Charts::LegendPosition::Right">Right</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::LegendPosition get_Position() const;
    /// Specifies the position of the legend on a chart.
    /// Default value is <see cref="Aspose::Words::Drawing::Charts::LegendPosition::Right">Right</see>.
    ASPOSE_WORDS_SHARED_API void set_Position(Aspose::Words::Drawing::Charts::LegendPosition value);
    /// Determines whether other chart elements shall be allowed to overlap legend.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_Overlay() const;
    /// Determines whether other chart elements shall be allowed to overlap legend.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_Overlay(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

    ASPOSE_WORDS_SHARED_API ChartLegend();

protected:

    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition get_SidePosition() const;
    void set_SidePosition(Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment get_PositionAlignment() const;
    void set_PositionAlignment(Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> get_Layout() const;
    void set_Layout(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> get_TxPr();
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartLegendEntry>>> get_LegendEntries() const;

    void AddEntry(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartLegendEntry> entry);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartLegendEntry> GetEntry(int32_t index);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartLegend> Clone();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> mLayout;
    Aspose::Words::Drawing::Charts::LegendPosition mLegendPos;
    bool mOverlay;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> mSpPr;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> mTxPr;
    System::SharedPtr<Aspose::Collections::IntToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartLegendEntry>>> mLegendEntries;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition mSidePosition;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment mPositionAlignment;

};

}
}
}
}
