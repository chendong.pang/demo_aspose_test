//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/AxisDisplayUnit.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/SimpleTypes/TitlePosition.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/IDmlChartTitleHolder.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisBuiltInUnit.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisPr; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartAxisReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartAxis; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPlotArea; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartTitle; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDisplayUnitsLabel; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTx; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderingContext; } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Provides access to the scaling options of the display units for the value axis.
class ASPOSE_WORDS_SHARED_CLASS AxisDisplayUnit : public Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder, public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef AxisDisplayUnit ThisType;
    typedef Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder BaseType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisPr;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartAxisReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::ChartAxis;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Gets the scaling value of the display units as one of the predefined values.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisBuiltInUnit get_Unit() const;
    /// Sets the scaling value of the display units as one of the predefined values.
    ASPOSE_WORDS_SHARED_API void set_Unit(Aspose::Words::Drawing::Charts::AxisBuiltInUnit value);
    /// Gets or sets a user-defined divisor to scale display units on the value axis.
    /// 
    /// The property is not supported by MS Office 2016 new charts. Default value is 1.
    /// 
    /// Setting this property sets the <see cref="Aspose::Words::Drawing::Charts::AxisDisplayUnit::get_Unit">Unit</see> property to
    /// <see cref="Aspose::Words::Drawing::Charts::AxisBuiltInUnit::Custom">Custom</see>.
    ASPOSE_WORDS_SHARED_API double get_CustomUnit() const;
    /// Setter for Aspose::Words::Drawing::Charts::AxisDisplayUnit::get_CustomUnit
    ASPOSE_WORDS_SHARED_API void set_CustomUnit(double value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> get_Title() override;
    ASPOSE_WORDS_SHARED_API void set_Title(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> value) override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::Core::SimpleTypes::TitlePosition get_TitlePosition() override;
    /// Returns the Document the title holder belongs.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::DocumentBase> get_Document() override;
    ASPOSE_WORDS_SHARED_API bool get_TitleDeleted() override;
    ASPOSE_WORDS_SHARED_API void set_TitleDeleted(bool value) override;
    ASPOSE_WORDS_SHARED_API bool get_IsVisible() override;

    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> GenerateAutoTitle(System::SharedPtr<Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingContext> context) override;

    ASPOSE_WORDS_SHARED_API AxisDisplayUnit();

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> get_Label() const;
    bool get_IsDefined();

    void SetAxis(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> axis);
    void SetLabel(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDisplayUnitsLabel> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::AxisDisplayUnit> Clone();
    System::String GetDisplayUnitsLabel();
    System::String GetNumberLabel();
    double GetActualUnit();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Drawing::Charts::AxisBuiltInUnit mUnit;
    double mCustomUnit;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDisplayUnitsLabel> mLabel;
    System::WeakPtr<Aspose::Words::Drawing::Charts::ChartAxis> mAxis;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;

};

}
}
}
}
