//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataPoint.h
#pragma once

#include "Aspose.Words.Cpp/Model/Drawing/Charts/IChartDataPoint.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeriesCollection; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartSeriesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataPointCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeries; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartLegendMarkerRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartRenderingUtil; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlLineChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartSeriesWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartMarker; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartPictureOptions; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDataPointPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Allows to specify formatting of a single data point on the chart.
class ASPOSE_WORDS_SHARED_CLASS ChartDataPoint : public Aspose::Words::Drawing::Charts::IChartDataPoint
{
    typedef ChartDataPoint ThisType;
    typedef Aspose::Words::Drawing::Charts::IChartDataPoint BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::ChartSeriesCollection;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartSeriesReader;
    friend class Aspose::Words::Drawing::Charts::ChartDataPointCollection;
    friend class Aspose::Words::Drawing::Charts::ChartSeries;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartLegendMarkerRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartRenderingUtil;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlLineChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartSeriesWriter;

public:

    /// Index of the data point this object applies formatting to.
    ASPOSE_WORDS_SHARED_API int32_t get_Index();
    ASPOSE_WORDS_SHARED_API int32_t get_Explosion() override;
    ASPOSE_WORDS_SHARED_API void set_Explosion(int32_t value) override;
    ASPOSE_WORDS_SHARED_API bool get_InvertIfNegative() override;
    ASPOSE_WORDS_SHARED_API void set_InvertIfNegative(bool value) override;
    ASPOSE_WORDS_SHARED_API bool get_Bubble3D() override;
    ASPOSE_WORDS_SHARED_API void set_Bubble3D(bool value) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartMarker> get_Marker() override;

protected:

    ASPOSE_WORDS_SHARED_API void set_Index(int32_t value);
    bool get_HasMarker();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartPictureOptions> get_PictureOptions();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataPointPr> get_PointPr() const;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_Chart() const;

    ChartDataPoint(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    void SetParent(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> parent);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> Clone();
    void SetChart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);
    void SetSpPr(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> spPr);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataPointPr> mPointPr;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> mSpPr;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartMarker> mMarker;

    void SetMarker(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartMarker> marker);

};

}
}
}
}
