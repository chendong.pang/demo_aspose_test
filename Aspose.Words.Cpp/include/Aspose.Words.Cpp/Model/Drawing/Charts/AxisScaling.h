//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/AxisScaling.h
#pragma once

#include <system/shared_ptr.h>

#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/AxisScaleType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartAxisPr; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartAxis; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPlotArea; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneCartesian; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlaneCartesian3D; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartPlanePolar; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlSurfaceChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartAxisWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { enum class AxisOrientation; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class AxisBound; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { class DoubleOrAutomatic; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents the scaling options of the axis.
class ASPOSE_WORDS_SHARED_CLASS AxisScaling : public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef AxisScaling ThisType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Drawing::Charts::Core::DmlChartAxisPr;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::ChartAxis;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPlotArea;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneCartesian;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlaneCartesian3D;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartPlanePolar;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlSurfaceChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartAxisWriter;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Gets scaling type of the axis.
    ASPOSE_WORDS_SHARED_API Aspose::Words::Drawing::Charts::AxisScaleType get_Type() const;
    /// Sets scaling type of the axis.
    ASPOSE_WORDS_SHARED_API void set_Type(Aspose::Words::Drawing::Charts::AxisScaleType value);
    /// Gets or sets the logarithmic base for a logarithmic axis.
    /// 
    /// The property is not supported by MS Office 2016 new charts.
    /// 
    /// Valid range of a floating point value is greater than or equal to 2 and less than or
    /// equal to 1000. The property has effect only if <see cref="Aspose::Words::Drawing::Charts::AxisScaling::get_Type">Type</see> is set to
    /// <see cref="Aspose::Words::Drawing::Charts::AxisScaleType::Logarithmic">Logarithmic</see>.
    /// 
    /// Setting this property sets the <see cref="Aspose::Words::Drawing::Charts::AxisScaling::get_Type">Type</see> property to <see cref="Aspose::Words::Drawing::Charts::AxisScaleType::Logarithmic">Logarithmic</see>.
    ASPOSE_WORDS_SHARED_API double get_LogBase() const;
    /// Setter for Aspose::Words::Drawing::Charts::AxisScaling::get_LogBase
    ASPOSE_WORDS_SHARED_API void set_LogBase(double value);
    /// Gets minimum value of the axis.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> get_Minimum();
    /// Sets minimum value of the axis.
    ASPOSE_WORDS_SHARED_API void set_Minimum(System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> value);
    /// Gets the maximum value of the axis.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> get_Maximum();
    /// Sets the maximum value of the axis.
    ASPOSE_WORDS_SHARED_API void set_Maximum(System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

    ASPOSE_WORDS_SHARED_API AxisScaling();

protected:

    bool get_MinimumIsDefined();
    bool get_MaximumIsDefined();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::SimpleTypes::DoubleOrAutomatic> get_GapWidth() const;
    void set_GapWidth(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::SimpleTypes::DoubleOrAutomatic> value);
    Aspose::Words::Drawing::Charts::AxisOrientation get_Orientation() const;
    void set_Orientation(Aspose::Words::Drawing::Charts::AxisOrientation value);

    System::SharedPtr<Aspose::Words::Drawing::Charts::AxisScaling> Clone();
    void SetLogBaseWithoutCheck(double value);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::Drawing::Charts::AxisScaleType mType;
    double mLogBase;
    System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> mMinimum;
    System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound> mMaximum;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::SimpleTypes::DoubleOrAutomatic> mGapWidth;
    Aspose::Words::Drawing::Charts::AxisOrientation mOrientation;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;

};

}
}
}
}
