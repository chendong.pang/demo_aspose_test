//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/AxisBound.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/date_time.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class AxisScaling; } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents minimum or maximum bound of axis values.
/// 
/// Bound can be specified as a numeric, datetime or a special "auto" value.
/// 
/// The instances of this class are immutable.
/// 
/// @sa Aspose::Words::Drawing::Charts::AxisScaling::get_Minimum
/// @sa Aspose::Words::Drawing::Charts::AxisScaling::get_Maximum
class ASPOSE_WORDS_SHARED_CLASS AxisBound FINAL : public System::Object
{
    typedef AxisBound ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::AxisScaling;

public:

    /// Returns a flag indicating that axis bound should be determined automatically.
    ASPOSE_WORDS_SHARED_API bool get_IsAuto() const;
    /// Returns numeric value of axis bound.
    ASPOSE_WORDS_SHARED_API double get_Value() const;
    /// Returns value of axis bound represented as datetime.
    ASPOSE_WORDS_SHARED_API System::DateTime get_ValueAsDate();

    /// Creates a new instance indicating that axis bound should be determined automatically by a word-processing
    /// application.
    ASPOSE_WORDS_SHARED_API AxisBound();
    /// Creates an axis bound represented as a number.
    ASPOSE_WORDS_SHARED_API AxisBound(double value);
    /// Creates an axis bound represented as datetime value.
    ASPOSE_WORDS_SHARED_API AxisBound(System::DateTime datetime);

    /// Determines whether the specified object is equal in value to the current object.
    ASPOSE_WORDS_SHARED_API bool Equals(System::SharedPtr<System::Object> obj) override;
    /// Serves as a hash function for this type.
    ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const override;
    /// Returns a user-friendly string that displays the value of this object.
    ASPOSE_WORDS_SHARED_API System::String ToString() const override;

protected:

    static System::SharedPtr<Aspose::Words::Drawing::Charts::AxisBound>& Auto();

    virtual ASPOSE_WORDS_SHARED_API ~AxisBound();

private:

    double mValue;
    bool mIsAuto;

    static System::DateTime& MinExclusiveDate();
    static System::DateTime& MaxDate();

    static const double MinExclusiveDateValue;
    static const double MaxExclusiveDateValue;

};

}
}
}
}
