//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/Chart.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeriesCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartTitle; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartLegend; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartAxis; } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartSpace; } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Provides access to the chart shape properties.
class ASPOSE_WORDS_SHARED_CLASS Chart : public System::Object
{
    typedef Chart ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChart;

public:

    /// Provides access to series collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeriesCollection> get_Series();
    /// Provides access to the chart title properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> get_Title();
    /// Provides access to the chart legend properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartLegend> get_Legend();
    /// Provides access to properties of the X axis of the chart.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> get_AxisX();
    /// Provides access to properties of the Y axis of the chart.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> get_AxisY();
    /// Provides access to properties of the Z axis of the chart.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> get_AxisZ();

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_DmlChart() const;
    System::SharedPtr<Aspose::Words::DocumentBase> get_Document();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartSpace> get_ChartSpace() const;

    Chart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartSpace> dmlChartSpace, System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> dmlChart);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mDmlChart;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeriesCollection> mSeriesCollection;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChartSpace> mChartSpace;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> mAxisX;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> mAxisY;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartAxis> mAxisZ;

};

}
}
}
}
