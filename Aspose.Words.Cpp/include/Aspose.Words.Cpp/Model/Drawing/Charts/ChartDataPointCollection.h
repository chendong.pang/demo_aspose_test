//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataPointCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/collections/dictionary.h>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataPoint.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartSeriesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeries; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlAreaChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents collection of a <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see>.
class ASPOSE_WORDS_SHARED_CLASS ChartDataPointCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint>>
{
    typedef ChartDataPointCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChart;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartSeriesReader;
    friend class Aspose::Words::Drawing::Charts::ChartSeries;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlAreaChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Returns the number of <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see> in this collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Returns <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see> for the specified index.
    /// If there is no ChartDataPoint for the specified index,
    /// returns default series ChartDataPoint.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> idx_get(int32_t index);

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint>>> GetEnumerator() override;
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see> at the specified index.
    /// 
    /// @param index Target data point index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> Add(int32_t index);
    /// Removes a <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see> at the specified index.
    /// 
    /// @param index The zero-based index of the bookmark to remove.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all <see cref="Aspose::Words::Drawing::Charts::ChartDataPoint">ChartDataPoint</see> from this collection.
    ASPOSE_WORDS_SHARED_API void Clear();

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> get_FirstDataPoint();
    bool get_HasCustomDataPoints();
    System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint>>> get_DataPoints();
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> get_Series() const;

    ChartDataPointCollection(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);

    void AddDataPoint(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint> dpt);
    void ResolveInheritedProperties();

    virtual ASPOSE_WORDS_SHARED_API ~ChartDataPointCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::WeakPtr<Aspose::Words::Drawing::Charts::ChartSeries> mSeries;
    System::SharedPtr<System::Collections::Generic::Dictionary<int32_t, System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataPoint>>> mDataPoints;

};

}
}
}
}
