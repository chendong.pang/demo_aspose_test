//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartSeriesCollection.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/date_time.h>
#include <system/collections/ilist.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/array.h>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/ChartSeries.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class Chart; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { enum class MarkerSymbol; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartValueCollection; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents collection of a <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see>.
class ASPOSE_WORDS_SHARED_CLASS ChartSeriesCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries>>
{
    typedef ChartSeriesCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Chart;

public:

    /// Returns the number of <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> in this collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Returns a <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> at the specified index.
    /// 
    /// The index is zero-based.
    /// 
    /// Negative indexes are allowed and indicate access from the back of the collection.
    /// For example -1 means the last item, -2 means the second before last and so on.
    /// 
    /// If index is greater than or equal to the number of items in the list, this returns a null reference.
    /// 
    /// If index is negative and its absolute value is greater than the number of items in the list, this returns a null reference.
    /// 
    /// @param index An index into the collection.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> idx_get(int32_t index);

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries>>> GetEnumerator() override;
    /// Removes a <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> at the specified index.
    /// 
    /// @param index The zero-based index of the ChartSeries to remove.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Removes all <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> from this collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> to this collection.
    /// Use this method to add series to any type of Bar, Column, Line and Surface charts.
    /// 
    /// @return Recently added <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> Add(System::String seriesName, System::ArrayPtr<System::String> categories, System::ArrayPtr<double> values);
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> to this collection.
    /// Use this method to add series to any type of Scatter charts.
    /// 
    /// @return Recently added <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> Add(System::String seriesName, System::ArrayPtr<double> xValues, System::ArrayPtr<double> yValues);
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> to this collection.
    /// Use this method to add series to any type of Area, Radar and Stock charts.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> Add(System::String seriesName, System::ArrayPtr<System::DateTime> dates, System::ArrayPtr<double> values);
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> to this collection.
    /// Use this method to add series to any type of Bubble charts.
    /// 
    /// @return Recently added <see cref="Aspose::Words::Drawing::Charts::ChartSeries">ChartSeries</see> object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> Add(System::String seriesName, System::ArrayPtr<double> xValues, System::ArrayPtr<double> yValues, System::ArrayPtr<double> bubbleSizes);

protected:

    ChartSeriesCollection(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> dmlChart);

    virtual ASPOSE_WORDS_SHARED_API ~ChartSeriesCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries>>> get_ChartSeriesList();

    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mDmlChart;

    void SetXAxisDateCategoryAttribute(bool value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> AddCore(System::String seriesName, System::ArrayPtr<double> values);
    void SetSeriesTypedProperies(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    static void SetPieChartSpPrDefaults(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    static void SetScatterSpPrDefaults(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    static void SetMarkerDefaults(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series, Aspose::Words::Drawing::Charts::MarkerSymbol markerStyle);
    static System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueCollection> CollectNumValues(System::ArrayPtr<double> values);
    static System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueCollection> CollectDateValues(System::ArrayPtr<System::DateTime> dates);
    static System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValueCollection> CollectStringValues(System::ArrayPtr<System::String> categories);

};

}
}
}
}
