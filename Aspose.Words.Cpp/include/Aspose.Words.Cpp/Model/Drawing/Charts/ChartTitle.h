//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartTitle.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlTitleRenderer; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class Chart; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace Validation { class ShapeValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartAxisReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class AxisDisplayUnit; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDisplayUnitsLabel; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartAxis; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartFormat; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class SidePosition; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class PositionAlignment; } } } } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartManualLayout; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTx; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTxPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class IDmlChartTitleHolder; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Text { class DmlTextBody; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Text { class DmlParagraphProperties; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Provides access to the chart title properties.
class ASPOSE_WORDS_SHARED_CLASS ChartTitle : public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef ChartTitle ThisType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlTitleRenderer;
    friend class Aspose::Words::Drawing::Charts::Chart;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::Validation::ShapeValidator;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartAxisReader;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::AxisDisplayUnit;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDisplayUnitsLabel;
    friend class Aspose::Words::Drawing::Charts::ChartAxis;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartFormat;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Gets the text of the chart title.
    /// If null or empty value is specified, auto generated title will be shown.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Sets the text of the chart title.
    /// If null or empty value is specified, auto generated title will be shown.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);
    /// Determines whether other chart elements shall be allowed to overlap title.
    /// By default overlay is false.
    ASPOSE_WORDS_SHARED_API bool get_Overlay() const;
    /// Determines whether other chart elements shall be allowed to overlap title.
    /// By default overlay is false.
    ASPOSE_WORDS_SHARED_API void set_Overlay(bool value);
    /// Determines whether the title shall be shown for this chart.
    /// Default value is true.
    ASPOSE_WORDS_SHARED_API bool get_Show();
    /// Determines whether the title shall be shown for this chart.
    /// Default value is true.
    ASPOSE_WORDS_SHARED_API void set_Show(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

protected:

    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition get_SidePosition() const;
    void set_SidePosition(Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment get_PositionAlignment() const;
    void set_PositionAlignment(Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment value);
    System::SharedPtr<Aspose::Words::Font> get_Font();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> get_Layout() const;
    void set_Layout(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    void set_SpPr(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> get_Tx() const;
    void set_Tx(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> get_TxPr();

    ChartTitle(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder> chartTitleHolder);

    virtual ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> Clone();
    void SetTitleHolder(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder> value);
    ASPOSE_WORDS_SHARED_API void CopyComplexPropertiesTo(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartTitle> destination);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> mLayout;
    bool mOverlay;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> mSpPr;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> mTx;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> mTxPr;
    System::SharedPtr<Aspose::Words::Font> mFont;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::IDmlChartTitleHolder> mTitleHolder;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::SidePosition mSidePosition;
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::PositionAlignment mPositionAlignment;

    static System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Text::DmlTextBody> CreateEmptyReachTx();
    static void ApplyDefaultRunProperties(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Text::DmlParagraphProperties> props);
    virtual ThisType* CppMemberwiseClone() const { return new ThisType(*this); }

};

}
}
}
}
