//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataLabelCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/sorted_list.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/INumberFormatProvider.h"
#include "Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataLabel.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Collections { class IntList; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartPr; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeries; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataLabelRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartNumberFormat; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDataLabelPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartNumFormat; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartDataAnalyzer; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { enum class DmlChartDataLabelAttrs; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents a collection of <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see>.
class ASPOSE_WORDS_SHARED_CLASS ChartDataLabelCollection : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>, public Aspose::Words::Drawing::Charts::Core::INumberFormatProvider
{
    typedef ChartDataLabelCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>> BaseType;
    typedef Aspose::Words::Drawing::Charts::Core::INumberFormatProvider BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChart;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartPr;
    friend class Aspose::Words::Drawing::Charts::ChartSeries;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataLabelRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

private:

    class Enumerator FINAL : public System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>
    {
        typedef Enumerator ThisType;
        typedef System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:

        System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> get_Current() const override;

        Enumerator(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> collection);

        bool MoveNext() override;
        void Reset() override;
        void Dispose() override;

    protected:

        System::Object::shared_members_type GetSharedMembers() override;

    private:

        System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> mCollection;
        System::SharedPtr<Aspose::Collections::IntList> mLabelIndexes;
        int32_t mLastUsedIndex;
        int32_t mCurrentIndex;

    };

public:

    /// Returns the number of <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> in this collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();
    /// Allows to specify whether category name is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowCategoryName();
    /// Allows to specify whether category name is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowCategoryName(bool value);
    /// Allows to specify whether bubble size is to be displayed for the data labels of the entire series.
    /// Applies only to Bubble charts.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowBubbleSize();
    /// Allows to specify whether bubble size is to be displayed for the data labels of the entire series.
    /// Applies only to Bubble charts.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowBubbleSize(bool value);
    /// Allows to specify whether legend key is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowLegendKey();
    /// Allows to specify whether legend key is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowLegendKey(bool value);
    /// Allows to specify whether percentage value is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>. Applies only to Pie charts.
    ASPOSE_WORDS_SHARED_API bool get_ShowPercentage();
    /// Allows to specify whether percentage value is to be displayed for the data labels of the entire series.
    /// Default value is <b>false</b>. Applies only to Pie charts.
    ASPOSE_WORDS_SHARED_API void set_ShowPercentage(bool value);
    /// Returns a Boolean to indicate the series name display behavior for the data labels of the entire series.
    /// <b>True</b> to show the series name. <b>False</b> to hide. By default <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowSeriesName();
    /// Sets a Boolean to indicate the series name display behavior for the data labels of the entire series.
    /// <b>True</b> to show the series name. <b>False</b> to hide. By default <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowSeriesName(bool value);
    /// Allows to specify whether values are to be displayed in the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowValue();
    /// Allows to specify whether values are to be displayed in the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowValue(bool value);
    /// Allows to specify whether data label leader lines need be shown for the data labels of the entire series.
    /// Default value is <b>false</b>.
    /// 
    /// Applies to Pie charts only.
    /// Leader lines create a visual connection between a data label and its corresponding data point.
    /// 
    /// Value defined for this property can be overridden for an individual data label with using the
    /// <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel::get_ShowLeaderLines">ShowLeaderLines</see> property.
    ASPOSE_WORDS_SHARED_API bool get_ShowLeaderLines();
    /// Setter for Aspose::Words::Drawing::Charts::ChartDataLabelCollection::get_ShowLeaderLines
    ASPOSE_WORDS_SHARED_API void set_ShowLeaderLines(bool value);
    /// Allows to specify whether values from data labels range to be displayed in the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_ShowDataLabelsRange();
    /// Allows to specify whether values from data labels range to be displayed in the data labels of the entire series.
    /// Default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_ShowDataLabelsRange(bool value);
    /// Gets string separator used for the data labels of the entire series.
    /// The default is a comma, except for pie charts showing only category name and percentage, when a line break
    /// shall be used instead.
    ASPOSE_WORDS_SHARED_API System::String get_Separator();
    /// Sets string separator used for the data labels of the entire series.
    /// The default is a comma, except for pie charts showing only category name and percentage, when a line break
    /// shall be used instead.
    ASPOSE_WORDS_SHARED_API void set_Separator(System::String value);
    /// Gets an <see cref="Aspose::Words::Drawing::Charts::ChartNumberFormat">ChartNumberFormat</see> instance allowing to set number format for the data labels of the
    /// entire series.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> get_NumberFormat();
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt_INumberFormatProvider() override;
    ASPOSE_WORDS_SHARED_API void set_NumFmt_INumberFormatProvider(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value) override;

    /// Returns <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> for the specified index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> idx_get(int32_t index);

    /// Returns an enumerator object.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>> GetEnumerator() override;
    /// Adds new <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> at the specified index.
    /// 
    /// @param index Target data label index.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> Add(int32_t index);
    /// Clears format of a <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> at the specified index.
    /// 
    /// @param index The zero-based index of the chart data label to clear format.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    /// Clears format of all <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> in this collection.
    ASPOSE_WORDS_SHARED_API void Clear();
    /// Clears format of all <see cref="Aspose::Words::Drawing::Charts::ChartDataLabel">ChartDataLabel</see> in this collection.
    ASPOSE_WORDS_SHARED_API void ClearFormat();

protected:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> get_LabelPr() const;
    void set_LabelPr(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_Chart() const;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt();
    void set_NumFmt(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value);
    bool get_HasItems();
    bool get_HasItemWithNonDefaultFormatting();
    bool get_HasNonDefaultFormatting();

    ChartDataLabelCollection(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart, System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabelCollection> Clone();
    void SetChart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart, System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    void AddLabel(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> label);
    bool HasNonDefaultItemFormatting(int32_t index);
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>> GetLabelsToWrite();
    void NormalizeAfterLoad();

    virtual ASPOSE_WORDS_SHARED_API ~ChartDataLabelCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>> get_MaterializedDataLabels();

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> mLabelPr;
    System::SharedPtr<System::Collections::Generic::SortedList<int32_t, System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel>>> mDataLabels;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChartDataAnalyzer> mDataAnalyzer;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;
    System::WeakPtr<Aspose::Words::Drawing::Charts::ChartSeries> mSeries;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> mNumberFormat;

    static System::ArrayPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelAttrs>& gPropertiesToWriteWithShowDataLabelsRange();
    void GenerateTx(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> label);
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> MaterializeItem(int32_t index);
    int32_t GetChartExDataLabelCount(bool includeLabelsAfterLastUsedIndex);
    int32_t GetHistogramLabelCount();
    System::SharedPtr<Aspose::Collections::IntList> GetLabelIndexesDeterminedByData();
    System::SharedPtr<Aspose::Collections::IntList> GetCategorieIndexes();
    int32_t CountLabelsFrom(int32_t index);
    int32_t GetCount(bool includeLabelsAfterLastUsedIndex);

};

}
}
}
}
