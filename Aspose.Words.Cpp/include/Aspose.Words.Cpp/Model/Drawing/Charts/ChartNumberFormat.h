//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartNumberFormat.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabel; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabelCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartAxis; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartNumFormat; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class INumberFormatProvider; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { enum class WarningType; } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents number formatting of the parent element.
class ASPOSE_WORDS_SHARED_CLASS ChartNumberFormat : public System::Object
{
    typedef ChartNumberFormat ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabel;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabelCollection;
    friend class Aspose::Words::Drawing::Charts::ChartAxis;

public:

    /// Gets or sets the format code applied to a data label.
    /// 
    /// Number formatting is used to change the way a value appears in data label and can be used in some very creative ways.
    /// The examples of number formats:
    /// 
    /// Number - "#,##0.00"
    /// 
    /// Currency - "\"\$\\"#,##0.00"
    /// 
    /// Time - "[$-x-systime]h:mm:ss AM/PM"
    /// 
    /// Date - "d/mm/yyyy"
    /// 
    /// Percentage - "0.00%"
    /// 
    /// Fraction - "# ?/?"
    /// 
    /// Scientific - "0.00E+00"
    /// 
    /// Text - "@"
    /// 
    /// Accounting - "_-\"\$\\"* #,##0.00_-;-\"\$\\"* #,##0.00_-;_-\"\$\\"* \"-\\"??_-;_-@_-"
    /// 
    /// Custom with color - "[Red]-#,##0.0"
    ASPOSE_WORDS_SHARED_API System::String get_FormatCode();
    /// Setter for Aspose::Words::Drawing::Charts::ChartNumberFormat::get_FormatCode
    ASPOSE_WORDS_SHARED_API void set_FormatCode(System::String value);
    /// Specifies whether the format code is linked to a source cell.
    /// Default is true.
    ASPOSE_WORDS_SHARED_API bool get_IsLinkedToSource();
    /// Specifies whether the format code is linked to a source cell.
    /// Default is true.
    ASPOSE_WORDS_SHARED_API void set_IsLinkedToSource(bool value);

protected:

    ChartNumberFormat(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::INumberFormatProvider> numFmtProvider, System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    void Warn(Aspose::Words::WarningType warningType, System::String message);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt();
    void set_NumFmt(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value);

    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::INumberFormatProvider> mNumFmtProvider;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;

    static const System::String& GeneralFormatCode();
    bool LinkedToSource();

};

}
}
}
}
