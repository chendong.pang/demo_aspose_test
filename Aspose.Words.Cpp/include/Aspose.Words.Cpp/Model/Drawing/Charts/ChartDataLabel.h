//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Charts/ChartDataLabel.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/date_time.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Charts/Core/INumberFormatProvider.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartLeaderLineRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtChartToDmlConverter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Reader { class DmlChartComplexTypesReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartDataLabelCollection; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartPivotFormat; } } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlChartDataLabelRenderer; } } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Dml { namespace Charts { class DmlPieChartRenderer; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Dml { namespace Writer { class DmlChartCommonWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace SimpleTypes { enum class DataLabelPosition; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartNumberFormat; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartManualLayout; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartSpPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTx; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartTxPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartDataLabelPr; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartNumFormat; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChart; } } } } }
namespace Aspose { namespace Words { enum class WarningType; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { class ChartSeries; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { namespace ComplexTypes { class DmlChartValue; } } } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

namespace Charts {

/// Represents data label on a chart point or trendline.
class ASPOSE_WORDS_SHARED_CLASS ChartDataLabel : public Aspose::Words::Drawing::Charts::Core::INumberFormatProvider
{
    typedef ChartDataLabel ThisType;
    typedef Aspose::Words::Drawing::Charts::Core::INumberFormatProvider BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartLeaderLineRenderer;
    friend class Aspose::Words::RW::Odt::Reader::OdtChartToDmlConverter;
    friend class Aspose::Words::RW::Dml::Reader::DmlChartComplexTypesReader;
    friend class Aspose::Words::Drawing::Charts::ChartDataLabelCollection;
    friend class Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartPivotFormat;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlChartDataLabelRenderer;
    friend class Aspose::Words::ApsBuilder::Dml::Charts::DmlPieChartRenderer;
    friend class Aspose::Words::RW::Dml::Writer::DmlChartCommonWriter;

public:

    /// Specifies the index of the containing element.
    /// This index shall determine which of the parent's children collection this element applies to.
    /// Default value is 0.
    ASPOSE_WORDS_SHARED_API int32_t get_Index();
    /// Allows to specify if category name is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowCategoryName();
    /// Allows to specify if category name is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowCategoryName(bool value);
    /// Allows to specify if bubble size is to be displayed for the data labels on a chart.
    /// Applies only to Bubble charts.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowBubbleSize();
    /// Allows to specify if bubble size is to be displayed for the data labels on a chart.
    /// Applies only to Bubble charts.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowBubbleSize(bool value);
    /// Allows to specify if legend key is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowLegendKey();
    /// Allows to specify if legend key is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowLegendKey(bool value);
    /// Allows to specify if percentage value is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowPercentage();
    /// Allows to specify if percentage value is to be displayed for the data labels on a chart.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowPercentage(bool value);
    /// Returns a Boolean to indicate the series name display behavior for the data labels on a chart.
    /// True to show the series name. False to hide. By default false.
    ASPOSE_WORDS_SHARED_API bool get_ShowSeriesName();
    /// Sets a Boolean to indicate the series name display behavior for the data labels on a chart.
    /// True to show the series name. False to hide. By default false.
    ASPOSE_WORDS_SHARED_API void set_ShowSeriesName(bool value);
    /// Allows to specify if values are to be displayed in the data labels.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowValue();
    /// Allows to specify if values are to be displayed in the data labels.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowValue(bool value);
    /// Allows to specify if data label leader lines need be shown.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowLeaderLines();
    /// Allows to specify if data label leader lines need be shown.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowLeaderLines(bool value);
    /// Allows to specify if values from data labels range to be displayed in the data labels.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_ShowDataLabelsRange();
    /// Allows to specify if values from data labels range to be displayed in the data labels.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_ShowDataLabelsRange(bool value);
    /// Gets string separator used for the data labels on a chart.
    /// The default is a comma, except for pie charts showing only category name and percentage, when a line break
    /// shall be used instead.
    ASPOSE_WORDS_SHARED_API System::String get_Separator();
    /// Sets string separator used for the data labels on a chart.
    /// The default is a comma, except for pie charts showing only category name and percentage, when a line break
    /// shall be used instead.
    ASPOSE_WORDS_SHARED_API void set_Separator(System::String value);
    /// Returns true if this data label has something to display.
    ASPOSE_WORDS_SHARED_API bool get_IsVisible();
    /// Returns number format of the parent element.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> get_NumberFormat();
    /// Gets/sets a flag indicating whether this label is hidden.
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API bool get_IsHidden();
    /// Gets/sets a flag indicating whether this label is hidden.
    /// The default value is <b>false</b>.
    ASPOSE_WORDS_SHARED_API void set_IsHidden(bool value);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt_INumberFormatProvider() override;
    ASPOSE_WORDS_SHARED_API void set_NumFmt_INumberFormatProvider(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value) override;

    /// Clears format of this data label. The properties are set to the default values defined in the parent data
    /// label collection.
    ASPOSE_WORDS_SHARED_API void ClearFormat();

protected:

    ASPOSE_WORDS_SHARED_API void set_Index(int32_t value);
    Aspose::Words::Drawing::Charts::Core::SimpleTypes::DataLabelPosition get_DLblPos();
    void set_DLblPos(Aspose::Words::Drawing::Charts::Core::SimpleTypes::DataLabelPosition value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> get_Layout();
    void set_Layout(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_SpPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> get_Tx();
    bool get_IsParentRunPrUsed();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> get_TxPr();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> get_LeaderLines();
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> get_LabelPr() const;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> get_NumFmt();
    void set_NumFmt(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartNumFormat> value);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> get_Chart() const;
    int32_t get_SeriesElementIndex() const;
    void set_SeriesElementIndex(int32_t value);
    bool get_HasNonDefaultFormatting();

    ChartDataLabel(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> parentPr, System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);

    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartDataLabel> Clone();
    void Warn(Aspose::Words::WarningType warningType, System::String message);
    void SetChart(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> chart);
    void UpdateText(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);

    virtual ASPOSE_WORDS_SHARED_API ~ChartDataLabel();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsTxPrSpecified();

    int32_t pr_SeriesElementIndex;

    bool get_IsNumFmtUsed();

    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> mLayout;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartDataLabelPr> mLabelPr;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> mTx;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTxPr> mTxPr;
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartSpPr> mSpPr;
    System::WeakPtr<Aspose::Words::Drawing::Charts::Core::DmlChart> mChart;
    System::SharedPtr<Aspose::Words::Drawing::Charts::ChartNumberFormat> mNumberFormat;

    void UpdateFields(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartTx> tx, System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartManualLayout> GetActualLayout();
    System::String GenerateDataLabelText(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    void AddTextToDataLabel(System::SharedPtr<System::Text::StringBuilder> labelBuilder, System::String addedText);
    System::String GetPercentage(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    System::String GetCategoryName(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    void SetLabelTextColor(System::String seriesFormatCode, System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> dmlVal);
    System::String GetValue(System::SharedPtr<Aspose::Words::Drawing::Charts::ChartSeries> series);
    System::String GetValue(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> dmlVal);
    System::DateTime GetDate(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> dmlVal);
    static System::String GetStringValue(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> dmlVal, System::String formatCode);
    static float GetFloatValue(System::SharedPtr<Aspose::Words::Drawing::Charts::Core::ComplexTypes::DmlChartValue> dmlVal);
    void AppendSeparator(System::SharedPtr<System::Text::StringBuilder> labelBuilder);

};

}
}
}
}
