//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Drawing/Fill.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <drawing/color.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Validation { namespace VmlToDml { class VmlGradientToDmlConverter; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxBackgroundWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class DocumentFormatter; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class VmlFill; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class BackgroundShapeApsBuilder; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class InternalColorResolver; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { class CssBackground; } } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { class LayoutApsBuilder; } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilderContext; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class ShapeApsBuilderUtil; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class BrushFactory; } } } }
namespace Aspose { namespace Words { namespace ApsBuilder { namespace Shapes { class TextBoxApsBuilder; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Css { namespace New { class CssBackgroundColorPropertyDef; } } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Html { namespace Writer { class HtmlCoreWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace MarkupLanguage { namespace Writer { class MarkupLanguageShapeResourceWriter; } } } } }
namespace Aspose { namespace Words { class InlineHelper; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtBinaryObjectReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { enum class FillType; } } } }
namespace Aspose { namespace Drawing { class DrColor; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class GradientColor; } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { class IFill; } } } }

namespace Aspose {

namespace Words {

namespace Drawing {

/// Defines a fill for a shape.
/// 
/// Use the <see cref="Aspose::Words::Drawing::Shape::get_Fill">Fill</see> property to access fill properties of a shape.
/// You do not create instances of the <see cref="Aspose::Words::Drawing::Fill">Fill</see> class directly.
/// 
/// Although the <see cref="Aspose::Words::Drawing::Fill">Fill</see> class provides properties to specify solid color fill only,
/// all of the more complex fill types, including as gradient, pattern and texture are fully preserved
/// during document open-save cycles.
/// 
/// @sa Aspose::Words::Drawing::Shape::get_Fill
class ASPOSE_WORDS_SHARED_CLASS Fill : public System::Object
{
    typedef Fill ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Validation::VmlToDml::VmlGradientToDmlConverter;
    friend class Aspose::Words::RW::Docx::Writer::DocxBackgroundWriter;
    friend class Aspose::Words::RW::Html::Css::New::DocumentFormatter;
    friend class Aspose::Words::Drawing::Core::VmlFill;
    friend class Aspose::Words::ApsBuilder::Shapes::BackgroundShapeApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::InternalColorResolver;
    friend class Aspose::Words::RW::Html::CssBackground;
    friend class Aspose::Words::Drawing::Shape;
    friend class Aspose::Words::ApsBuilder::LayoutApsBuilder;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilderContext;
    friend class Aspose::Words::ApsBuilder::Shapes::ShapeApsBuilderUtil;
    friend class Aspose::Words::ApsBuilder::Shapes::BrushFactory;
    friend class Aspose::Words::ApsBuilder::Shapes::TextBoxApsBuilder;
    friend class Aspose::Words::RW::Html::Css::New::CssBackgroundColorPropertyDef;
    friend class Aspose::Words::RW::Html::Writer::HtmlCoreWriter;
    friend class Aspose::Words::RW::MarkupLanguage::Writer::MarkupLanguageShapeResourceWriter;
    friend class Aspose::Words::InlineHelper;
    friend class Aspose::Words::RW::Odt::Reader::OdtBinaryObjectReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Defines the color of a fill.
    /// 
    /// The default value is
    /// <see cref="System::Drawing::Color::get_White">White</see>.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Color();
    /// Setter for Aspose::Words::Drawing::Fill::get_Color
    ASPOSE_WORDS_SHARED_API void set_Color(System::Drawing::Color value);
    /// Defines the transparency of a fill. Valid range from 0 to 1, where 0 is fully transparent and 1 is fully opaque.
    /// 
    /// The default value is 1.
    ASPOSE_WORDS_SHARED_API double get_Opacity();
    /// Defines the transparency of a fill. Valid range from 0 to 1, where 0 is fully transparent and 1 is fully opaque.
    /// 
    /// The default value is 1.
    ASPOSE_WORDS_SHARED_API void set_Opacity(double value);
    /// Determines whether the shape will be filled.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API bool get_On();
    /// Determines whether the shape will be filled.
    /// 
    /// The default value is <b>true</b>.
    ASPOSE_WORDS_SHARED_API void set_On(bool value);
    /// Gets the raw bytes of the fill texture or pattern.
    /// 
    /// The default value is null.
    ASPOSE_WORDS_SHARED_API System::ArrayPtr<uint8_t> get_ImageBytes();

protected:

    System::Drawing::Color get_Color2();
    void set_Color2(System::Drawing::Color value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_Color2Internal();
    void set_Color2Internal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    System::SharedPtr<Aspose::Drawing::DrColor> get_ColorInternal();
    void set_ColorInternal(System::SharedPtr<Aspose::Drawing::DrColor> value);
    bool get_LockAspectRatio();
    void set_LockAspectRatio(bool value);
    double get_Opacity2();
    void set_Opacity2(double value);
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::GradientColor>> get_GradientColors();
    double get_FocusLeft();
    void set_FocusLeft(double value);
    double get_FocusTop();
    void set_FocusTop(double value);
    int32_t get_Focus();
    void set_Focus(int32_t value);
    Aspose::Words::Drawing::Core::FillType get_FillType();
    void set_FillType(Aspose::Words::Drawing::Core::FillType value);
    double get_Angle();
    void set_Angle(double value);
    bool get_HasImageBytes();
    System::SharedPtr<Aspose::Words::Drawing::Core::IFill> get_FillCore();

    Fill(System::SharedPtr<Aspose::Words::Drawing::Shape> shape);

    void SetImageBytes(System::ArrayPtr<uint8_t> imageBytes);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool get_IsVmlFill();

    System::SharedPtr<Aspose::Words::Drawing::Core::IFill> mFillCoreCash;
    System::WeakPtr<Aspose::Words::Drawing::Shape> mShape;

};

}
}
}
