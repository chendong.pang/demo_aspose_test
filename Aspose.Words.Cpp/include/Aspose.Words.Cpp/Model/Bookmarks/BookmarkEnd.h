//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Bookmarks/BookmarkEnd.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/Revisions/DisplacedByType.h"
#include "Aspose.Words.Cpp/Model/Nodes/NodeType.h"
#include "Aspose.Words.Cpp/Model/Nodes/Node.h"
#include "Aspose.Words.Cpp/Model/Nodes/IDisplaceableByCustomXml.h"
#include "Aspose.Words.Cpp/Model/Bookmarks/IBookmarkNode.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class Bookmark; } }
namespace Aspose { namespace Words { namespace Validation { class BookmarkValidator; } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfContentHandler; } } } } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { namespace RW { namespace Odt { namespace Reader { class OdtParagraphReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxBookmarkReader; } } } } }
namespace Aspose { namespace Words { class DocumentBase; } }
namespace Aspose { namespace Words { class DocumentVisitor; } }

namespace Aspose {

namespace Words {

/// Represents an end of a bookmark in a Word document.
/// 
/// A complete bookmark in a Word document consists of a <see cref="Aspose::Words::BookmarkStart">BookmarkStart</see>
/// and a matching <see cref="Aspose::Words::BookmarkEnd">BookmarkEnd</see> with the same bookmark name.
/// 
/// <see cref="Aspose::Words::BookmarkStart">BookmarkStart</see> and <see cref="Aspose::Words::BookmarkEnd">BookmarkEnd</see> are just markers inside a document
/// that specify where the bookmark starts and ends.
/// 
/// Use the <see cref="Aspose::Words::Bookmark">Bookmark</see> class as a "facade" to work with a bookmark
/// as a single object.
class ASPOSE_WORDS_SHARED_CLASS BookmarkEnd : public Aspose::Words::Node, public Aspose::Words::IBookmarkNode, public Aspose::Words::IDisplaceableByCustomXml
{
    typedef BookmarkEnd ThisType;
    typedef Aspose::Words::Node BaseType;
    typedef Aspose::Words::IBookmarkNode BaseType1;
    typedef Aspose::Words::IDisplaceableByCustomXml BaseType2;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1, BaseType2> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Bookmark;
    friend class Aspose::Words::Validation::BookmarkValidator;
    friend class Aspose::Words::RW::Rtf::Reader::RtfContentHandler;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::RW::Odt::Reader::OdtParagraphReader;
    friend class Aspose::Words::RW::Docx::Reader::DocxBookmarkReader;

public:

    /// Returns <see cref="Aspose::Words::NodeType::BookmarkEnd">BookmarkEnd</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::NodeType get_NodeType() const override;
    ASPOSE_WORDS_SHARED_API Aspose::Words::Revisions::DisplacedByType get_DisplacedByCustomXml() override;
    ASPOSE_WORDS_SHARED_API void set_DisplacedByCustomXml(Aspose::Words::Revisions::DisplacedByType value) override;
    /// Gets the bookmark name.
    /// 
    /// Cannot be null.
    ASPOSE_WORDS_SHARED_API System::String get_Name() override;
    /// Sets the bookmark name.
    /// 
    /// Cannot be null.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value) override;

    /// Initializes a new instance of the <see cref="Aspose::Words::BookmarkEnd">BookmarkEnd</see> class.
    /// 
    /// @param doc The owner document.
    /// @param name The name of the bookmark. Cannot be null.
    ASPOSE_WORDS_SHARED_API BookmarkEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc, System::String name);

    /// Accepts a visitor.
    /// 
    /// Calls <see cref="Aspose::Words::DocumentVisitor::VisitBookmarkEnd(System::SharedPtr<Aspose::Words::BookmarkEnd>)">VisitBookmarkEnd()</see>.
    /// 
    /// For more info see the Visitor design pattern.
    /// 
    /// @param visitor The visitor that will visit the node.
    /// 
    /// @return False if the visitor requested the enumeration to stop.
    ASPOSE_WORDS_SHARED_API bool Accept(System::SharedPtr<Aspose::Words::DocumentVisitor> visitor) override;

protected:

    Aspose::Words::Revisions::DisplacedByType get_DisplacedBy() const;
    void set_DisplacedBy(Aspose::Words::Revisions::DisplacedByType value);

    BookmarkEnd(System::SharedPtr<Aspose::Words::DocumentBase> doc);

    System::String GetNameInternal();
    void SetNameInternal(System::String name);

    virtual ASPOSE_WORDS_SHARED_API ~BookmarkEnd();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mName;
    Aspose::Words::Revisions::DisplacedByType mDisplacedBy;

    ThisType* CppMemberwiseClone() const override { return new ThisType(*this); }

};

}
}
