//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Bookmarks/Bookmark.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Fields { class FieldIncludeTextUpdater; } } }
namespace Aspose { namespace Words { namespace Revisions { class BookmarkDeleter; } } }
namespace Aspose { namespace Words { class BookmarkCache; } }
namespace Aspose { namespace Words { namespace Fields { class FieldIndex; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldNoteRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRef; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldSeq; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldTC; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldToc; } } }
namespace Aspose { namespace Words { namespace Fields { class ParagraphTocEntry; } } }
namespace Aspose { namespace Words { class BookmarkStart; } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class BookmarkReference; } } } }
namespace Aspose { namespace Words { namespace Fields { namespace Expressions { class ComparisonEvaluator; } } } }
namespace Aspose { namespace Words { namespace Validation { class BookmarkValidator; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldRefUtil; } } }
namespace Aspose { namespace Words { namespace Properties { class CustomDocumentProperties; } } }
namespace Aspose { namespace Words { class BookmarkEnd; } }
namespace Aspose { namespace Words { namespace Tables { class Cell; } } }
namespace Aspose { namespace Words { class RangeBound; } }
namespace Aspose { namespace Words { class Node; } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class NodeRange; } }
namespace Aspose { namespace Words { class CompositeNode; } }

namespace Aspose {

namespace Words {

/// Represents a single bookmark.
/// 
/// <see cref="Aspose::Words::Bookmark">Bookmark</see> is a "facade" object that encapsulates two nodes <see cref="Aspose::Words::Bookmark::get_BookmarkStart">BookmarkStart</see>
/// and <see cref="Aspose::Words::Bookmark::get_BookmarkEnd">BookmarkEnd</see> in a document tree and allows to work with a bookmark as a single object.
class ASPOSE_WORDS_SHARED_CLASS Bookmark : public System::Object
{
    typedef Bookmark ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Fields::FieldIncludeTextUpdater;
    friend class Aspose::Words::Revisions::BookmarkDeleter;
    friend class Aspose::Words::BookmarkCache;
    friend class Aspose::Words::Fields::FieldIndex;
    friend class Aspose::Words::Fields::FieldNoteRef;
    friend class Aspose::Words::Fields::FieldRef;
    friend class Aspose::Words::Fields::FieldSeq;
    friend class Aspose::Words::Fields::FieldTC;
    friend class Aspose::Words::Fields::FieldToc;
    friend class Aspose::Words::Fields::ParagraphTocEntry;
    friend class Aspose::Words::BookmarkStart;
    friend class Aspose::Words::Fields::Expressions::BookmarkReference;
    friend class Aspose::Words::Fields::Expressions::ComparisonEvaluator;
    friend class Aspose::Words::Validation::BookmarkValidator;
    friend class Aspose::Words::Fields::FieldRefUtil;
    friend class Aspose::Words::Properties::CustomDocumentProperties;

public:

    /// Gets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_Name();
    /// Sets the name of the bookmark.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Gets the text enclosed in the bookmark.
    ASPOSE_WORDS_SHARED_API System::String get_Text();
    /// Sets the text enclosed in the bookmark.
    ASPOSE_WORDS_SHARED_API void set_Text(System::String value);
    /// Gets the node that represents the start of the bookmark.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BookmarkStart> get_BookmarkStart() const;
    /// Gets the node that represents the end of the bookmark.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::BookmarkEnd> get_BookmarkEnd();
    /// Returns <b>true</b> if this bookmark is a table column bookmark.
    ASPOSE_WORDS_SHARED_API bool get_IsColumn();
    /// Gets the zero-based index of the first column of the table column range associated with the bookmark.
    ASPOSE_WORDS_SHARED_API int32_t get_FirstColumn();
    /// Gets the zero-based index of the last column of the table column range associated with the bookmark.
    ASPOSE_WORDS_SHARED_API int32_t get_LastColumn();

    /// Removes the bookmark from the document. Does not remove text inside the bookmark.
    ASPOSE_WORDS_SHARED_API void Remove();

protected:

    static const System::String& ErrorBookmarkNotDefined();

    static const int32_t MaxNameLength;

    static const System::String& GoBackBookmarkName();

    Bookmark(System::SharedPtr<Aspose::Words::BookmarkStart> bookmarkStart);
    Bookmark(System::SharedPtr<Aspose::Words::BookmarkStart> bookmarkStart, System::SharedPtr<Aspose::Words::BookmarkEnd> bookmarkEnd);

    System::String GetText(bool isFieldResultMode);
    System::SharedPtr<Aspose::Words::NodeRange> GetNodeRange();
    System::SharedPtr<Aspose::Words::NodeRange> GetMostWideNodeRange();
    static System::SharedPtr<Aspose::Words::NodeRange> GetNodeRange(System::SharedPtr<Aspose::Words::BookmarkStart> bookmarkStart, System::SharedPtr<Aspose::Words::BookmarkEnd> bookmarkEnd);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::BookmarkStart> mBookmarkStart;
    System::SharedPtr<Aspose::Words::BookmarkEnd> mBookmarkEnd;

    void SetTextInternal(System::String text);
    void SetColumnBookmarkText(System::String text);
    void PrepareCell(System::SharedPtr<Aspose::Words::Tables::Cell> cell);
    System::SharedPtr<Aspose::Words::RangeBound> GetRangeBound(bool forRangeStart);
    System::SharedPtr<Aspose::Words::RangeBound> GetColumnRangeBound(bool forRangeStart);
    System::SharedPtr<Aspose::Words::Tables::Cell> GetBookmarkEndCellBound();
    System::SharedPtr<Aspose::Words::Tables::Cell> GetBookmarkStartCellBound();
    void UntangleStart();
    void UntangleEnd();
    bool IsBookmarkNodeToSkip(System::SharedPtr<Aspose::Words::Node> node);
    System::SharedPtr<Aspose::Words::RunPr> GetSourceRunPr();
    static System::SharedPtr<Aspose::Words::NodeRange> CreateNodeRange(System::SharedPtr<Aspose::Words::RangeBound> startBound, System::SharedPtr<Aspose::Words::RangeBound> endBound, System::SharedPtr<Aspose::Words::BookmarkStart> bookmarkStart);
    static System::SharedPtr<Aspose::Words::Node> GetNodeRangeEndNode(System::SharedPtr<Aspose::Words::BookmarkStart> bookmarkStart, System::SharedPtr<Aspose::Words::BookmarkEnd> bookmarkEnd);
    static System::SharedPtr<Aspose::Words::Node> GetNodeRangeStartNode(System::SharedPtr<Aspose::Words::RangeBound> startBound, System::SharedPtr<Aspose::Words::Node> fieldCodeEnd);
    void RestoreRemovedStartEnd(System::SharedPtr<Aspose::Words::RangeBound> startOfRemovedRange, System::SharedPtr<Aspose::Words::RangeBound> endOfRemovedRange);
    static bool IsNodeRemoved(System::SharedPtr<Aspose::Words::Node> node);
    static void MoveAnnotationsToNextParagraph(System::SharedPtr<Aspose::Words::Node> annotation);
    System::SharedPtr<Aspose::Words::Node> PrepareTextInsertPosition();
    static void InsertAtDisplacedSdt(System::SharedPtr<Aspose::Words::Node> bookmarkNode, System::SharedPtr<Aspose::Words::CompositeNode> sdt, bool asChild);

};

}
}
