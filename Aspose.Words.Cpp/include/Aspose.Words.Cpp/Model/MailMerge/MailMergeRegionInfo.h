//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/MailMerge/MailMergeRegionInfo.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/list.h>
#include <system/collections/ilist.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace MailMerging { class MailMerge; } } }
namespace Aspose { namespace Words { namespace Fields { class Field; } } }
namespace Aspose { namespace Words { namespace Fields { class FieldMergeField; } } }

namespace Aspose {

namespace Words {

namespace MailMerging {

/// Contains information about a mail merge region.
class ASPOSE_WORDS_SHARED_CLASS MailMergeRegionInfo : public System::Object
{
    typedef MailMergeRegionInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::MailMerging::MailMerge;

public:

    /// Returns a list of child regions.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegionInfo>>> get_Regions() const;
    /// Returns a list of child fields.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IList<System::SharedPtr<Aspose::Words::Fields::Field>>> get_Fields() const;
    /// Returns the name of region.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Returns a start field for the region.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldMergeField> get_StartField() const;
    /// Returns an end field for the region.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::FieldMergeField> get_EndField() const;
    /// Returns the nesting level for the region.
    ASPOSE_WORDS_SHARED_API int32_t get_Level() const;

protected:

    System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegionInfo> get_ParentRegion() const;
    void set_ParentRegion(System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegionInfo> value);
    ASPOSE_WORDS_SHARED_API void set_EndField(System::SharedPtr<Aspose::Words::Fields::FieldMergeField> value);

    MailMergeRegionInfo();
    MailMergeRegionInfo(System::SharedPtr<Aspose::Words::Fields::FieldMergeField> field);

    void SetRegionLevel(int32_t val);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegionInfo> pr_ParentRegion;
    System::String pr_Name;
    System::SharedPtr<Aspose::Words::Fields::FieldMergeField> pr_StartField;
    System::SharedPtr<Aspose::Words::Fields::FieldMergeField> pr_EndField;
    int32_t pr_Level;

    ASPOSE_WORDS_SHARED_API void set_Level(int32_t value);

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::MailMerging::MailMergeRegionInfo>>> mChildRegions;
    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::Fields::Field>>> mChildFields;

};

}
}
}
