//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/MailMerge/ImageFieldMergingArgs.h
#pragma once

#include <system/io/stream.h>
#include <drawing/image.h>

#include "Aspose.Words.Cpp/Model/MailMerge/FieldMergingArgsBase.h"

namespace Aspose { namespace Words { namespace MailMerging { class MailMergeRegion; } } }
namespace Aspose { namespace Words { namespace Drawing { class Shape; } } }
namespace Aspose { namespace Words { namespace Fields { class MergeFieldImageDimension; } } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Fields { class FieldMergeField; } } }

namespace Aspose {

namespace Words {

namespace MailMerging {

/// Provides data for the <see cref="Aspose::Words::MailMerging::IFieldMergingCallback::ImageFieldMerging(System::SharedPtr<Aspose::Words::MailMerging::ImageFieldMergingArgs>)">ImageFieldMerging()</see> event.
/// 
/// This event occurs during mail merge when an image mail merge
/// field is encountered in the document. You can respond to this event to return a
/// file name, stream, or an <see cref="System::Drawing::Image">Image</see> object to the mail merge
/// engine so it is inserted into the document.
/// 
/// There are three properties available <see cref="Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageFileName">ImageFileName</see>,
/// <see cref="Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageStream">ImageStream</see> and <see cref="Aspose::Words::MailMerging::ImageFieldMergingArgs::get_Image">Image</see> to specify where the image must be taken from.
/// Set only one of these properties.
/// 
/// To insert an image mail merge field into a document in Word, select Insert/Field command,
/// then select MergeField and type Image:MyFieldName.
/// 
/// @sa Aspose::Words::MailMerging::IFieldMergingCallback
class ASPOSE_WORDS_SHARED_CLASS ImageFieldMergingArgs : public Aspose::Words::MailMerging::FieldMergingArgsBase
{
    typedef ImageFieldMergingArgs ThisType;
    typedef Aspose::Words::MailMerging::FieldMergingArgsBase BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::MailMerging::MailMergeRegion;

public:

    /// Sets the file name of the image that the mail merge engine must insert into the document.
    ASPOSE_WORDS_SHARED_API System::String get_ImageFileName() const;
    /// Sets the file name of the image that the mail merge engine must insert into the document.
    ASPOSE_WORDS_SHARED_API void set_ImageFileName(System::String value);
    /// Specifies the stream for the mail merge engine to read an image from.
    /// 
    /// Aspose.Words closes this stream after it merges the image into the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::IO::Stream> get_ImageStream() const;
    /// Specifies the stream for the mail merge engine to read an image from.
    /// 
    /// Aspose.Words closes this stream after it merges the image into the document.
    ASPOSE_WORDS_SHARED_API void set_ImageStream(System::SharedPtr<System::IO::Stream> value);
    /// Specifies the image that the mail merge engine must insert into the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Drawing::Image> get_Image() const;
    /// Specifies the image that the mail merge engine must insert into the document.
    ASPOSE_WORDS_SHARED_API void set_Image(System::SharedPtr<System::Drawing::Image> value);
    /// Specifies the shape that the mail merge engine must insert into the document.
    /// 
    /// When this property is specified, the mail merge engine ignores all other properties like <see cref="Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageFileName">ImageFileName</see> or <see cref="Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageStream">ImageStream</see>
    /// and simply inserts the shape into the document.
    /// 
    /// Use this property to fully control the process of merging an image merge field.
    /// For example, you can specify <see cref="Aspose::Words::Drawing::ShapeBase::get_WrapType">WrapType</see> or any other shape property to fine tune the resulting node. However, please note that
    /// you are responsible for providing the content of the shape.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Shape> get_Shape() const;
    /// Setter for Aspose::Words::MailMerging::ImageFieldMergingArgs::get_Shape
    ASPOSE_WORDS_SHARED_API void set_Shape(System::SharedPtr<Aspose::Words::Drawing::Shape> value);
    /// Specifies the image width for the image to insert into the document.
    /// 
    /// The value of this property initially comes from the corresponding MERGEFIELD's code, contained in the
    /// template document. To override the initial value, you should assign an instance of
    /// <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class to this property or set the properties for the instance
    /// of <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class, returned by this property.
    /// 
    /// To indicate that the original value of the image width should be applied, you should assign the <b>null</b>
    /// value to this property or set the <see cref="Aspose::Words::Fields::MergeFieldImageDimension::get_Value">Value</see> property for the instance
    /// of <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class, returned by this property, to a negative value.
    /// 
    /// @sa Aspose::Words::Fields::MergeFieldImageDimension
    /// @sa Aspose::Words::Fields::MergeFieldImageDimensionUnit
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> get_ImageWidth() const;
    /// Setter for Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageWidth
    ASPOSE_WORDS_SHARED_API void set_ImageWidth(System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> value);
    /// Specifies the image height for the image to insert into the document.
    /// 
    /// The value of this property initially comes from the corresponding MERGEFIELD's code, contained in the
    /// template document. To override the initial value, you should assign an instance of
    /// <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class to this property or set the properties for the instance
    /// of <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class, returned by this property.
    /// 
    /// To indicate that the original value of the image height should be applied, you should assign the <b>null</b>
    /// value to this property or set the <see cref="Aspose::Words::Fields::MergeFieldImageDimension::get_Value">Value</see> property for the instance
    /// of <see cref="Aspose::Words::Fields::MergeFieldImageDimension">MergeFieldImageDimension</see> class, returned by this property, to a negative value.
    /// 
    /// @sa Aspose::Words::Fields::MergeFieldImageDimension
    /// @sa Aspose::Words::Fields::MergeFieldImageDimensionUnit
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> get_ImageHeight() const;
    /// Setter for Aspose::Words::MailMerging::ImageFieldMergingArgs::get_ImageHeight
    ASPOSE_WORDS_SHARED_API void set_ImageHeight(System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> value);

protected:

    ImageFieldMergingArgs(System::SharedPtr<Aspose::Words::Document> document, System::String tableName, int32_t recordIndex, System::SharedPtr<Aspose::Words::Fields::FieldMergeField> field, System::String fieldName, System::String documentFieldName, System::SharedPtr<System::Object> fieldValue, System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> imageWidth, System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> imageHeight);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String pr_ImageFileName;
    System::SharedPtr<System::IO::Stream> pr_ImageStream;
    System::SharedPtr<System::Drawing::Image> pr_Image;
    System::SharedPtr<Aspose::Words::Drawing::Shape> pr_Shape;
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> pr_ImageWidth;
    System::SharedPtr<Aspose::Words::Fields::MergeFieldImageDimension> pr_ImageHeight;

};

}
}
}
