//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/FindReplace/ReplacingArgs.h
#pragma once

#include <system/text/regularexpressions/match.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Replacing { class FindReplaceLegacy; } } }
namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { class Node; } }

namespace Aspose {

namespace Words {

namespace Replacing {

/// Provides data for a custom replace operation.
/// 
/// @sa Aspose::Words::Replacing::IReplacingCallback
/// @sa Aspose::Words::Range
/// @sa Aspose::Words::Range::Replace(System::String, System::String, System::SharedPtr<Aspose::Words::Replacing::FindReplaceOptions>)
class ASPOSE_WORDS_SHARED_CLASS ReplacingArgs : public System::Object
{
    typedef ReplacingArgs ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Replacing::FindReplaceLegacy;
    friend class Aspose::Words::Replacing::FindReplace;

public:

    /// The <see cref="System::Text::RegularExpressions::Match">Match</see> resulting from a single regular
    /// expression match during a <b>Replace</b>.
    /// 
    /// <b>Match.Index"</b>
    /// gets the zero-based starting
    /// position of the match from the start of the find and replace range.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Text::RegularExpressions::Match> get_Match() const;
    /// Gets the node that contains the beginning of the match.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Node> get_MatchNode() const;
    /// Gets the zero-based starting position of the match from the start of
    /// the node that contains the beginning of the match.
    ASPOSE_WORDS_SHARED_API int32_t get_MatchOffset() const;
    /// Gets the replacement string.
    ASPOSE_WORDS_SHARED_API System::String get_Replacement() const;
    /// Sets the replacement string.
    ASPOSE_WORDS_SHARED_API void set_Replacement(System::String value);
    /// Identifies, by name, a captured group in the <see cref="Aspose::Words::Replacing::ReplacingArgs::get_Match">Match</see>
    /// that is to be replaced with the <see cref="Aspose::Words::Replacing::ReplacingArgs::get_Replacement">Replacement</see> string.
    /// 
    /// When group name is null, <see cref="Aspose::Words::Replacing::ReplacingArgs::get_GroupIndex">GroupIndex</see> is used to identify the group.
    /// 
    /// Default is null.
    ASPOSE_WORDS_SHARED_API System::String get_GroupName() const;
    /// Setter for Aspose::Words::Replacing::ReplacingArgs::get_GroupName
    ASPOSE_WORDS_SHARED_API void set_GroupName(System::String value);
    /// Identifies, by index, a captured group in the <see cref="Aspose::Words::Replacing::ReplacingArgs::get_Match">Match</see>
    /// that is to be replaced with the <see cref="Aspose::Words::Replacing::ReplacingArgs::get_Replacement">Replacement</see> string.
    /// 
    /// <see cref="Aspose::Words::Replacing::ReplacingArgs::get_GroupIndex">GroupIndex</see> has effect only when <see cref="Aspose::Words::Replacing::ReplacingArgs::get_GroupName">GroupName</see> is null.
    /// 
    /// Default is zero.
    ASPOSE_WORDS_SHARED_API int32_t get_GroupIndex() const;
    /// Setter for Aspose::Words::Replacing::ReplacingArgs::get_GroupIndex
    ASPOSE_WORDS_SHARED_API void set_GroupIndex(int32_t value);

protected:

    ReplacingArgs(System::SharedPtr<System::Text::RegularExpressions::Match> match, System::SharedPtr<Aspose::Words::Node> matchNode, int32_t matchOffset, System::String replacement);

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mGroupName;
    System::SharedPtr<System::Text::RegularExpressions::Match> mMatch;
    System::SharedPtr<Aspose::Words::Node> mMatchNode;
    int32_t mMatchOffset;
    System::String mReplacement;
    int32_t mGroupIndex;

};

}
}
}
