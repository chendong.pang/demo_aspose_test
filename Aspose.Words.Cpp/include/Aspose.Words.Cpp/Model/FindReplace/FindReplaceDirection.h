//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/FindReplace/FindReplaceDirection.h
#pragma once

#include <system/object_ext.h>
#include <system/enum.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

/// Specifies direction for replace operations.
enum class FindReplaceDirection
{
    /// Matched items are replaced from first to last.
    Forward,
    /// Matched items are replaced from last back to first.
    Backward
};

}
}

template<>
struct EnumMetaInfo<Aspose::Words::FindReplaceDirection>
{
    static const ASPOSE_WORDS_SHARED_API std::array<std::pair<Aspose::Words::FindReplaceDirection, const char_t*>, 2>& values();
};
