//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/FindReplace/FindReplaceOptions.h
#pragma once

#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/FindReplace/FindReplaceDirection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Replacing { class FindReplace; } } }
namespace Aspose { namespace Words { class Font; } }
namespace Aspose { namespace Words { class ParagraphFormat; } }
namespace Aspose { namespace Words { namespace Replacing { class IReplacingCallback; } } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { namespace Words { class ParaPr; } }

namespace Aspose {

namespace Words {

namespace Replacing {

/// Specifies options for find/replace operations.
class ASPOSE_WORDS_SHARED_CLASS FindReplaceOptions : public System::Object
{
    typedef FindReplaceOptions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::Replacing::FindReplace;

public:

    /// Text formatting applied to new content.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Font> get_ApplyFont() const;
    /// Paragraph formatting applied to new content.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::ParagraphFormat> get_ApplyParagraphFormat() const;
    /// Selects direction for replace. Default value is <see cref="Aspose::Words::FindReplaceDirection::Forward">Forward</see>.
    ASPOSE_WORDS_SHARED_API Aspose::Words::FindReplaceDirection get_Direction() const;
    /// Selects direction for replace. Default value is <see cref="Aspose::Words::FindReplaceDirection::Forward">Forward</see>.
    ASPOSE_WORDS_SHARED_API void set_Direction(Aspose::Words::FindReplaceDirection value);
    /// True indicates case-sensitive comparison, false indicates case-insensitive comparison.
    ASPOSE_WORDS_SHARED_API bool get_MatchCase() const;
    /// True indicates case-sensitive comparison, false indicates case-insensitive comparison.
    ASPOSE_WORDS_SHARED_API void set_MatchCase(bool value);
    /// True indicates the oldValue must be a standalone word.
    ASPOSE_WORDS_SHARED_API bool get_FindWholeWordsOnly() const;
    /// True indicates the oldValue must be a standalone word.
    ASPOSE_WORDS_SHARED_API void set_FindWholeWordsOnly(bool value);
    /// The user-defined method which is called before every replace occurrence.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Replacing::IReplacingCallback> get_ReplacingCallback() const;
    /// The user-defined method which is called before every replace occurrence.
    ASPOSE_WORDS_SHARED_API void set_ReplacingCallback(System::SharedPtr<Aspose::Words::Replacing::IReplacingCallback> value);
    /// True indicates that a text search is performed sequentially from top to bottom considering the text boxes.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API bool get_UseLegacyOrder() const;
    /// True indicates that a text search is performed sequentially from top to bottom considering the text boxes.
    /// Default value is false.
    ASPOSE_WORDS_SHARED_API void set_UseLegacyOrder(bool value);
    /// Gets a boolean value indicating either to ignore text inside delete revisions.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreDeleted() const;
    /// Sets a boolean value indicating either to ignore text inside delete revisions.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API void set_IgnoreDeleted(bool value);
    /// Gets a boolean value indicating either to ignore text inside insert revisions.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreInserted() const;
    /// Sets a boolean value indicating either to ignore text inside insert revisions.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API void set_IgnoreInserted(bool value);
    /// Gets a boolean value indicating either to ignore text inside fields.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool get_IgnoreFields() const;
    /// Sets a boolean value indicating either to ignore text inside fields.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API void set_IgnoreFields(bool value);
    /// Gets a boolean value indicating whether to recognize and use substitutions within replacement patterns.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API bool get_UseSubstitutions() const;
    /// Sets a boolean value indicating whether to recognize and use substitutions within replacement patterns.
    /// The default value is <c>false</c>.
    ASPOSE_WORDS_SHARED_API void set_UseSubstitutions(bool value);
    /// Gets a boolean value indicating that old find/replace algorithm is used.
    ASPOSE_WORDS_SHARED_API bool get_LegacyMode() const;
    /// Sets a boolean value indicating that old find/replace algorithm is used.
    ASPOSE_WORDS_SHARED_API void set_LegacyMode(bool value);

    ASPOSE_WORDS_SHARED_API FindReplaceOptions();
    ASPOSE_WORDS_SHARED_API FindReplaceOptions(Aspose::Words::FindReplaceDirection direction);
    ASPOSE_WORDS_SHARED_API FindReplaceOptions(System::SharedPtr<Aspose::Words::Replacing::IReplacingCallback> replacingCallback);
    ASPOSE_WORDS_SHARED_API FindReplaceOptions(Aspose::Words::FindReplaceDirection direction, System::SharedPtr<Aspose::Words::Replacing::IReplacingCallback> replacingCallback);

protected:

    System::SharedPtr<Aspose::Words::RunPr> get_ApplyRunPr() const;
    System::SharedPtr<Aspose::Words::ParaPr> get_ApplyParaPr() const;

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    Aspose::Words::FindReplaceDirection mDirection;
    System::SharedPtr<Aspose::Words::Font> mApplyFont;
    System::SharedPtr<Aspose::Words::ParagraphFormat> mApplyParagraphFormat;
    System::SharedPtr<Aspose::Words::RunPr> mApplyRunPr;
    System::SharedPtr<Aspose::Words::ParaPr> mApplyParaPr;
    bool mMatchCase;
    bool mFindWholeWordsOnly;
    System::SharedPtr<Aspose::Words::Replacing::IReplacingCallback> mReplacingCallback;
    bool mUseLegacyOrder;
    bool mIgnoreDeleted;
    bool mIgnoreInserted;
    bool mIgnoreFields;
    bool mUseSubstitutions;
    bool mLegacyMode;

};

}
}
}
