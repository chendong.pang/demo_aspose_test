//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Themes/ThemeFonts.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/idictionary.h>
#include <system/collections/dictionary.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace Themes { class Theme; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class ThemeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace Fonts { class FontInfo; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeSupplementalFont; } } }

namespace Aspose {

namespace Words {

namespace Themes {

/// Represents a collection of fonts in the font scheme, allowing to specify different fonts for different languages <see cref="Aspose::Words::Themes::ThemeFonts::get_Latin">Latin</see>, <see cref="Aspose::Words::Themes::ThemeFonts::get_EastAsian">EastAsian</see> and <see cref="Aspose::Words::Themes::ThemeFonts::get_ComplexScript">ComplexScript</see>.
class ASPOSE_WORDS_SHARED_CLASS ThemeFonts : public System::Object
{
    typedef ThemeFonts ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::Themes::Theme;
    friend class Aspose::Words::RW::Docx::Writer::ThemeWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;

public:

    /// Specifies font name for Latin characters.
    ASPOSE_WORDS_SHARED_API System::String get_Latin();
    /// Specifies font name for Latin characters.
    ASPOSE_WORDS_SHARED_API void set_Latin(System::String value);
    /// Specifies font name for EastAsian characters.
    ASPOSE_WORDS_SHARED_API System::String get_EastAsian();
    /// Specifies font name for EastAsian characters.
    ASPOSE_WORDS_SHARED_API void set_EastAsian(System::String value);
    /// Specifies font name for ComplexScript characters.
    ASPOSE_WORDS_SHARED_API System::String get_ComplexScript();
    /// Specifies font name for ComplexScript characters.
    ASPOSE_WORDS_SHARED_API void set_ComplexScript(System::String value);

protected:

    System::SharedPtr<Aspose::Words::Fonts::FontInfo> ComplexScriptFontInfo;
    System::SharedPtr<Aspose::Words::Fonts::FontInfo> EastAsianFontInfo;
    System::SharedPtr<Aspose::Words::Fonts::FontInfo> LatinFontInfo;

    System::SharedPtr<System::Collections::Generic::IDictionary<System::String, System::SharedPtr<Aspose::Words::Themes::ThemeSupplementalFont>>> get_SupplementalFonts() const;
    bool get_IsModified() const;

    ThemeFonts();

    System::SharedPtr<Aspose::Words::Themes::ThemeFonts> Clone();
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mIsModified;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::String, System::SharedPtr<Aspose::Words::Themes::ThemeSupplementalFont>>> mSupplementalFonts;

};

}
}
}
