//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Themes/Theme.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/collections/dictionary.h>
#include <cstdint>

#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/Themes/ThemeFont.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/Themes/ThemeColor.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/Themes/IThemeProvider.h"
#include "Aspose.Words.Cpp/Model/Drawing/Core/Dml/IDmlExtensionListSource.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class FormattingDifferenceCalculator; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Charts { namespace Core { class DmlChartSpace; } } } } }
namespace Aspose { namespace Words { class OfficeColor; } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace Validation { class DocumentPostLoader; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class ThemeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { class ImportContext; } }
namespace Aspose { namespace Words { class NodeImporter; } }
namespace Aspose { namespace Words { class StyleCollection; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxDocumentWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Reader { class RtfReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Rtf { namespace Writer { class RtfRunPrWriter; } } } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeFonts; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeColors; } } }
namespace Aspose { namespace OpcPackaging { class OpcPackagePart; } }
namespace Aspose { namespace Words { namespace Settings { class ThemeFontLanguages; } } }
namespace Aspose { namespace Words { namespace Themes { class FormatScheme; } } }
namespace Aspose { namespace Words { namespace Themes { class ThemeObjectDefaults; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { class DmlExtension; } } } } }
namespace Aspose { namespace Collections { template<typename> class StringToObjDictionary; } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Colors { class DmlColor; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Fills { class DmlFill; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Outlines { class DmlOutline; } } } } } }
namespace Aspose { namespace Words { namespace Themes { class EffectStyle; } } }
namespace Aspose { namespace Words { class RunPr; } }
namespace Aspose { enum class Language; }

namespace Aspose {

namespace Words {

namespace Themes {

/// Represents document Theme, and provides access to main theme parts including <see cref="Aspose::Words::Themes::Theme::get_MajorFonts">MajorFonts</see>, <see cref="Aspose::Words::Themes::Theme::get_MinorFonts">MinorFonts</see> and <see cref="Aspose::Words::Themes::Theme::get_Colors">Colors</see>
class ASPOSE_WORDS_SHARED_CLASS Theme : public Aspose::Words::Drawing::Core::Dml::Themes::IThemeProvider, public Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource
{
    typedef Theme ThisType;
    typedef Aspose::Words::Drawing::Core::Dml::Themes::IThemeProvider BaseType;
    typedef Aspose::Words::Drawing::Core::Dml::IDmlExtensionListSource BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    friend class Aspose::Words::FormattingDifferenceCalculator;
    friend class Aspose::Words::Drawing::Charts::Core::DmlChartSpace;
    friend class Aspose::Words::OfficeColor;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::Validation::DocumentPostLoader;
    friend class Aspose::Words::RW::Docx::Writer::ThemeWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;
    friend class Aspose::Words::ImportContext;
    friend class Aspose::Words::NodeImporter;
    friend class Aspose::Words::StyleCollection;
    friend class Aspose::Words::RW::Docx::Writer::DocxDocumentWriter;
    friend class Aspose::Words::RW::Rtf::Reader::RtfReader;
    friend class Aspose::Words::RW::Rtf::Writer::RtfRunPrWriter;

public:

    /// Allows to specify the set of major fonts for different languages.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::ThemeFonts> get_MajorFonts() const;
    /// Allows to specify the set of minor fonts for different languages.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::ThemeFonts> get_MinorFonts() const;
    /// Allows to specify the set of theme colors for the document.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::ThemeColors> get_Colors() const;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> get_Extensions() override;
    ASPOSE_WORDS_SHARED_API void set_Extensions(System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> value) override;

    ASPOSE_WORDS_SHARED_API System::String GetFontName(Aspose::Words::Drawing::Core::Dml::Themes::ThemeFont themeFont) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Colors::DmlColor> GetThemeColor(Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor color) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Fills::DmlFill> GetBackgroundFillStyle(int32_t index) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Fills::DmlFill> GetFillStyle(int32_t index) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Outlines::DmlOutline> GetLineStyle(int32_t index) override;
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::Themes::EffectStyle> GetEffectStyle(int32_t index) override;
    ASPOSE_WORDS_SHARED_API void OnChange() override;

    ASPOSE_WORDS_SHARED_API Theme();

protected:

    static System::SharedPtr<Aspose::Words::Themes::Theme> get_BuiltInTheme();
    System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart> get_ThemePart() const;
    System::String get_ThemeNamespace() const;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::String, System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart>>> get_RelatedParts() const;
    void set_RelatedParts(System::SharedPtr<System::Collections::Generic::Dictionary<System::String, System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart>>> value);
    System::String get_Name() const;
    void set_Name(System::String value);
    System::String get_FontSchemeName() const;
    void set_FontSchemeName(System::String value);
    System::SharedPtr<Aspose::Words::Settings::ThemeFontLanguages> get_ThemeFontLanguages() const;
    void set_ThemeFontLanguages(System::SharedPtr<Aspose::Words::Settings::ThemeFontLanguages> value);
    System::SharedPtr<Aspose::Words::Themes::ThemeColors> get_ColorScheme();
    System::SharedPtr<Aspose::Words::Themes::FormatScheme> get_FormatScheme();
    void set_FormatScheme(System::SharedPtr<Aspose::Words::Themes::FormatScheme> value);
    System::SharedPtr<Aspose::Words::Themes::ThemeObjectDefaults> get_ObjectDefaults() const;
    void set_ObjectDefaults(System::SharedPtr<Aspose::Words::Themes::ThemeObjectDefaults> value);
    bool get_IsModified();

    void SetMajorFonts(System::SharedPtr<Aspose::Words::Themes::ThemeFonts> majorFonts);
    void SetMinorFonts(System::SharedPtr<Aspose::Words::Themes::ThemeFonts> minorFonts);
    System::SharedPtr<Aspose::Words::Themes::Theme> Clone();
    static bool ThemeFontsEquals(System::SharedPtr<Aspose::Words::Themes::Theme> a, System::SharedPtr<Aspose::Words::Themes::Theme> b);
    static bool ThemeColorsEquals(System::SharedPtr<Aspose::Words::Themes::Theme> a, System::SharedPtr<Aspose::Words::Themes::Theme> b);
    static void Apply(System::SharedPtr<Aspose::Words::Themes::Theme> theme, System::SharedPtr<Aspose::Words::RunPr> runPr);
    System::SharedPtr<Aspose::Words::Themes::Theme> CreateThemeOverride();
    void SetThemePart(System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart> value, System::String themeNamespace);
    void Attach(System::SharedPtr<Aspose::Words::Document> document);

    virtual ASPOSE_WORDS_SHARED_API ~Theme();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::Themes::ThemeColors> mColorScheme;
    System::String mFontSchemeName;
    System::SharedPtr<Aspose::Words::Themes::FormatScheme> mFormatScheme;
    System::SharedPtr<Aspose::Words::Themes::ThemeFonts> mMajorFonts;
    System::SharedPtr<Aspose::Words::Themes::ThemeFonts> mMinorFonts;
    System::String mName;
    System::SharedPtr<System::Collections::Generic::Dictionary<System::String, System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart>>> mRelatedParts;
    System::SharedPtr<Aspose::Words::Themes::ThemeObjectDefaults> mObjectDefaults;
    System::SharedPtr<Aspose::Collections::StringToObjDictionary<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::DmlExtension>>> mExtensions;
    System::SharedPtr<Aspose::Words::Settings::ThemeFontLanguages> mThemeFontLanguages;
    System::SharedPtr<Aspose::OpcPackaging::OpcPackagePart> mThemePart;
    System::WeakPtr<Aspose::Words::Document> mDocument;
    System::String mThemeNamespace;

    static System::SharedPtr<Aspose::Words::Themes::Theme>& gDefaultThemeCache();
    static System::SharedPtr<System::Object>& gDefaultThemeSyncRoot();
    static System::String LanguageToScript(Aspose::Language lang);
    static bool UsesTraditionalChineseScript(Aspose::Language lang);
    void UpdateDocument();
    static System::SharedPtr<Aspose::Words::Themes::Theme> ReadBuiltInTheme();

};

}
}
}
