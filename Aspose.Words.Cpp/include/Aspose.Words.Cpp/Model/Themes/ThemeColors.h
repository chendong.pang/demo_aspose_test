//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/Themes/ThemeColors.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/array.h>
#include <drawing/color.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class OfficeColor; } }
namespace Aspose { namespace Words { namespace Themes { class Theme; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class ThemeWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxThemeReader; } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Colors { class DmlColor; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Themes { class IThemeProvider; } } } } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Themes { enum class ThemeColor; } } } } } }

namespace Aspose {

namespace Words {

namespace Themes {

/// Represents the color scheme of the document theme which contains twelve colors.
/// ThemeColors object contains six accent colors, two dark colors, two light colors
/// and a color for each of a hyperlink and followed hyperlink.
class ASPOSE_WORDS_SHARED_CLASS ThemeColors : public System::Object
{
    typedef ThemeColors ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::OfficeColor;
    friend class Aspose::Words::Themes::Theme;
    friend class Aspose::Words::RW::Docx::Writer::ThemeWriter;
    friend class Aspose::Words::RW::Docx::Reader::DocxThemeReader;

public:

    /// Specifies color Accent 1.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent1();
    /// Specifies color Accent 1.
    ASPOSE_WORDS_SHARED_API void set_Accent1(System::Drawing::Color value);
    /// Specifies color Accent 2.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent2();
    /// Specifies color Accent 2.
    ASPOSE_WORDS_SHARED_API void set_Accent2(System::Drawing::Color value);
    /// Specifies color Accent 3.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent3();
    /// Specifies color Accent 3.
    ASPOSE_WORDS_SHARED_API void set_Accent3(System::Drawing::Color value);
    /// Specifies color Accent 4.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent4();
    /// Specifies color Accent 4.
    ASPOSE_WORDS_SHARED_API void set_Accent4(System::Drawing::Color value);
    /// Specifies color Accent 5.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent5();
    /// Specifies color Accent 5.
    ASPOSE_WORDS_SHARED_API void set_Accent5(System::Drawing::Color value);
    /// Specifies color Accent 6.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Accent6();
    /// Specifies color Accent 6.
    ASPOSE_WORDS_SHARED_API void set_Accent6(System::Drawing::Color value);
    /// Specifies color Dark 1.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Dark1();
    /// Specifies color Dark 1.
    ASPOSE_WORDS_SHARED_API void set_Dark1(System::Drawing::Color value);
    /// Specifies color Dark 2.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Dark2();
    /// Specifies color Dark 2.
    ASPOSE_WORDS_SHARED_API void set_Dark2(System::Drawing::Color value);
    /// Specifies color for a clicked hyperlink.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_FollowedHyperlink();
    /// Specifies color for a clicked hyperlink.
    ASPOSE_WORDS_SHARED_API void set_FollowedHyperlink(System::Drawing::Color value);
    /// Specifies color for a hyperlink.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Hyperlink();
    /// Specifies color for a hyperlink.
    ASPOSE_WORDS_SHARED_API void set_Hyperlink(System::Drawing::Color value);
    /// Specifies color Light 1.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Light1();
    /// Specifies color Light 1.
    ASPOSE_WORDS_SHARED_API void set_Light1(System::Drawing::Color value);
    /// Specifies color Light 2.
    ASPOSE_WORDS_SHARED_API System::Drawing::Color get_Light2();
    /// Specifies color Light 2.
    ASPOSE_WORDS_SHARED_API void set_Light2(System::Drawing::Color value);

protected:

    System::String get_Name();
    void set_Name(System::String value);
    bool get_IsModified() const;

    ThemeColors(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Themes::IThemeProvider> theme);

    System::SharedPtr<Aspose::Words::Themes::ThemeColors> Clone();
    void SetTheme(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Themes::IThemeProvider> theme);
    void AddColor(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Colors::DmlColor> color, Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor colorName);
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Colors::DmlColor> GetColor(Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor colorName);
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Colors::DmlColor> GetColor(System::String themeColorStr);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    bool mIsModified;
    System::ArrayPtr<System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Colors::DmlColor>> mColors;
    System::String mName;
    System::WeakPtr<Aspose::Words::Drawing::Core::Dml::Themes::IThemeProvider> mTheme;

    static int32_t& gThemeColorCount();
    static Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor MapColors(Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor color);
    System::Drawing::Color GetNativeColor(Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor colorIndex);
    void SetNativeColor(Aspose::Words::Drawing::Core::Dml::Themes::ThemeColor colorIndex, System::Drawing::Color color);

};

}
}
}
