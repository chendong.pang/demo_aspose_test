//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/WebExtensions/WebExtension.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace WebExtensions { class TaskPane; } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxWebExtensionReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxWebExtensionWriter; } } } } }
namespace Aspose { namespace Words { namespace WebExtensions { class WebExtensionReference; } } }
namespace Aspose { namespace Words { namespace WebExtensions { class WebExtensionBindingCollection; } } }
namespace Aspose { namespace Words { namespace WebExtensions { class WebExtensionReferenceCollection; } } }
namespace Aspose { namespace Words { namespace WebExtensions { class WebExtensionPropertyCollection; } } }
namespace Aspose { namespace Words { namespace Drawing { namespace Core { namespace Dml { namespace Fills { class DmlBlip; } } } } } }

namespace Aspose {

namespace Words {

namespace WebExtensions {

/// Represents a web extension object.
class ASPOSE_WORDS_SHARED_CLASS WebExtension : public System::Object
{
    typedef WebExtension ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::WebExtensions::TaskPane;
    friend class Aspose::Words::RW::Docx::Reader::DocxWebExtensionReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxWebExtensionWriter;

public:

    /// Uniquely identifies the web extension instance in the current document.
    ASPOSE_WORDS_SHARED_API System::String get_Id() const;
    /// Uniquely identifies the web extension instance in the current document.
    ASPOSE_WORDS_SHARED_API void set_Id(System::String value);
    /// Specifies whether the user can interact with the web extension or not.
    ASPOSE_WORDS_SHARED_API bool get_IsFrozen() const;
    /// Specifies whether the user can interact with the web extension or not.
    ASPOSE_WORDS_SHARED_API void set_IsFrozen(bool value);
    /// Specifies the primary reference to an web extension.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionReference> get_Reference() const;
    /// Specifies a list of web extension bindings.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionBindingCollection> get_Bindings() const;
    /// Specifies alternate references to a web extension.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionReferenceCollection> get_AlternateReferences() const;
    /// Represents a set of web extension custom properties.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionPropertyCollection> get_Properties() const;

protected:

    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Fills::DmlBlip> get_Snapshot() const;
    void set_Snapshot(System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Fills::DmlBlip> value);

    WebExtension();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String pr_Id;
    bool pr_IsFrozen;
    System::SharedPtr<Aspose::Words::Drawing::Core::Dml::Fills::DmlBlip> mSnapshot;
    System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionReference> mReference;
    System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionBindingCollection> mBindings;
    System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionPropertyCollection> mProperties;
    System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionReferenceCollection> mAlternateReferences;

};

}
}
}
