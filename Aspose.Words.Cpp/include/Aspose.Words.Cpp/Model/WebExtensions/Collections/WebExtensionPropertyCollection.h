//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/WebExtensions/Collections/WebExtensionPropertyCollection.h
#pragma once

#include "Aspose.Words.Cpp/Model/WebExtensions/WebExtensionProperty.h"
#include "Aspose.Words.Cpp/Model/WebExtensions/Collections/BaseWebExtensionCollection.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace WebExtensions { class WebExtension; } } }

namespace Aspose {

namespace Words {

namespace WebExtensions {

/// Specifies a set of web extension custom properties.
class ASPOSE_WORDS_SHARED_CLASS WebExtensionPropertyCollection : public Aspose::Words::WebExtensions::BaseWebExtensionCollection<System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionProperty>>
{
    typedef WebExtensionPropertyCollection ThisType;
    typedef Aspose::Words::WebExtensions::BaseWebExtensionCollection<System::SharedPtr<Aspose::Words::WebExtensions::WebExtensionProperty>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::WebExtensions::WebExtension;

public:

    ASPOSE_WORDS_SHARED_API void SetTemplateWeakPtr(unsigned int argument) override;

protected:

    WebExtensionPropertyCollection();

    virtual ASPOSE_WORDS_SHARED_API ~WebExtensionPropertyCollection();

};

}
}
}
