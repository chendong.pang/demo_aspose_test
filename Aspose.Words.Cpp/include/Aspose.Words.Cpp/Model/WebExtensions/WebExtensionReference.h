//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/WebExtensions/WebExtensionReference.h
#pragma once

#include <system/string.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/WebExtensions/Enums/WebExtensionStoreType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Words {

namespace WebExtensions {

/// Represents the reference to a web extension. The reference is used to identify the provider location and version of the
/// extension.
class ASPOSE_WORDS_SHARED_CLASS WebExtensionReference : public System::Object
{
    typedef WebExtensionReference ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    /// Identifier associated with the web extension within a catalog provider.
    ASPOSE_WORDS_SHARED_API System::String get_Id() const;
    /// Identifier associated with the web extension within a catalog provider.
    ASPOSE_WORDS_SHARED_API void set_Id(System::String value);
    /// Specifies the version of the web extension.
    ASPOSE_WORDS_SHARED_API System::String get_Version() const;
    /// Specifies the version of the web extension.
    ASPOSE_WORDS_SHARED_API void set_Version(System::String value);
    /// Specifies the instance of the marketplace where the web extension is stored.
    ASPOSE_WORDS_SHARED_API System::String get_Store() const;
    /// Specifies the instance of the marketplace where the web extension is stored.
    ASPOSE_WORDS_SHARED_API void set_Store(System::String value);
    /// Specifies the type of marketplace.
    ASPOSE_WORDS_SHARED_API Aspose::Words::WebExtensions::WebExtensionStoreType get_StoreType() const;
    /// Specifies the type of marketplace.
    ASPOSE_WORDS_SHARED_API void set_StoreType(Aspose::Words::WebExtensions::WebExtensionStoreType value);

    ASPOSE_WORDS_SHARED_API WebExtensionReference();

protected:

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String pr_Id;
    System::String pr_Version;
    System::String pr_Store;
    Aspose::Words::WebExtensions::WebExtensionStoreType pr_StoreType;

};

}
}
}
