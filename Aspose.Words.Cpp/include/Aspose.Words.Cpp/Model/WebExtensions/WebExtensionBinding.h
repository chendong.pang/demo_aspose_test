//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Model/WebExtensions/WebExtensionBinding.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/Model/WebExtensions/Enums/WebExtensionBindingType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxWebExtensionReader; } } } } }

namespace Aspose {

namespace Words {

namespace WebExtensions {

/// Specifies a binding relationship between a web extension and the data in the document.
class ASPOSE_WORDS_SHARED_CLASS WebExtensionBinding : public System::Object
{
    typedef WebExtensionBinding ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::RW::Docx::Reader::DocxWebExtensionReader;

public:

    /// Specifies the binding identifier.
    ASPOSE_WORDS_SHARED_API System::String get_Id() const;
    /// Specifies the binding identifier.
    ASPOSE_WORDS_SHARED_API void set_Id(System::String value);
    /// Specifies the binding type.
    ASPOSE_WORDS_SHARED_API Aspose::Words::WebExtensions::WebExtensionBindingType get_BindingType() const;
    /// Specifies the binding type.
    ASPOSE_WORDS_SHARED_API void set_BindingType(Aspose::Words::WebExtensions::WebExtensionBindingType value);
    /// Specifies the binding key used to map the binding entry in this list with the bound data in the document.
    ASPOSE_WORDS_SHARED_API System::String get_AppRef() const;
    /// Specifies the binding key used to map the binding entry in this list with the bound data in the document.
    ASPOSE_WORDS_SHARED_API void set_AppRef(System::String value);

    /// Creates web extension binding with specified parameters.
    /// 
    /// @param id Binding identifier.
    /// @param bindingType Binding type.
    /// @param appRef Binding key used to map the binding entry in this list with the bound data in the document.
    ASPOSE_WORDS_SHARED_API WebExtensionBinding(System::String id, Aspose::Words::WebExtensions::WebExtensionBindingType bindingType, System::String appRef);

protected:

    WebExtensionBinding();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String pr_Id;
    Aspose::Words::WebExtensions::WebExtensionBindingType pr_BindingType;
    System::String pr_AppRef;

};

}
}
}
