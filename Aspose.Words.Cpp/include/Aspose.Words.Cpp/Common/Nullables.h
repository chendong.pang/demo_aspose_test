//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/Common/Nullables.h
#pragma once

#include <system/object_ext.h>
#include <system/object.h>
#include <system/decimal.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose {

namespace Common {

/// \cond
class NullableByte : public System::Object
{
    typedef NullableByte ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API uint8_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableByte(uint8_t value);

    ASPOSE_WORDS_SHARED_API uint8_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API uint8_t GetValueOrDefault(uint8_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableByte other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(uint8_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableByte();

private:

    uint8_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableSByte : public System::Object
{
    typedef NullableSByte ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API int8_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableSByte(int8_t value);

    ASPOSE_WORDS_SHARED_API int8_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API int8_t GetValueOrDefault(int8_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableSByte other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(int8_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableSByte();

private:

    int8_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableInt16 : public System::Object
{
    typedef NullableInt16 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API int16_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableInt16(int16_t value);

    ASPOSE_WORDS_SHARED_API int16_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API int16_t GetValueOrDefault(int16_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableInt16 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(int16_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableInt16();

private:

    int16_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableUInt16 : public System::Object
{
    typedef NullableUInt16 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API uint16_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableUInt16(uint16_t value);

    ASPOSE_WORDS_SHARED_API uint16_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API uint16_t GetValueOrDefault(uint16_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableUInt16 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(uint16_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableUInt16();

private:

    uint16_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableInt32 : public System::Object
{
    typedef NullableInt32 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API int32_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableInt32(int32_t value);

    ASPOSE_WORDS_SHARED_API int32_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API int32_t GetValueOrDefault(int32_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableInt32 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(int32_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableInt32();

private:

    int32_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableUInt32 : public System::Object
{
    typedef NullableUInt32 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API uint32_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableUInt32(uint32_t value);

    ASPOSE_WORDS_SHARED_API uint32_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API uint32_t GetValueOrDefault(uint32_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableUInt32 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(uint32_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableUInt32();

private:

    uint32_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableInt64 : public System::Object
{
    typedef NullableInt64 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API int64_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableInt64(int64_t value);

    ASPOSE_WORDS_SHARED_API int64_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API int64_t GetValueOrDefault(int64_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableInt64 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(int64_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableInt64();

private:

    int64_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableUInt64 : public System::Object
{
    typedef NullableUInt64 ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API uint64_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableUInt64(uint64_t value);

    ASPOSE_WORDS_SHARED_API uint64_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API uint64_t GetValueOrDefault(uint64_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableUInt64 other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(uint64_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableUInt64();

private:

    uint64_t mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableSingle : public System::Object
{
    typedef NullableSingle ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API float get_Value();

    ASPOSE_WORDS_SHARED_API NullableSingle(float value);

    ASPOSE_WORDS_SHARED_API float GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API float GetValueOrDefault(float defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableSingle other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(float other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableSingle();

private:

    float mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableDouble : public System::Object
{
    typedef NullableDouble ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API double get_Value();

    ASPOSE_WORDS_SHARED_API NullableDouble(double value);

    ASPOSE_WORDS_SHARED_API double GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API double GetValueOrDefault(double defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableDouble other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(double other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableDouble();

private:

    double mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableDecimal : public System::Object
{
    typedef NullableDecimal ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API System::Decimal get_Value();

    ASPOSE_WORDS_SHARED_API NullableDecimal(System::Decimal value);

    ASPOSE_WORDS_SHARED_API System::Decimal GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API System::Decimal GetValueOrDefault(System::Decimal defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableDecimal other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(System::Decimal other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableDecimal();

private:

    System::Decimal mValue;
    bool pr_HasValue;

};/// \endcond

/// \cond
class NullableChar : public System::Object
{
    typedef NullableChar ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

public:

    ASPOSE_WORDS_SHARED_API bool get_HasValue() const;

    ASPOSE_WORDS_SHARED_API char16_t get_Value();

    ASPOSE_WORDS_SHARED_API NullableChar(char16_t value);

    ASPOSE_WORDS_SHARED_API char16_t GetValueOrDefault() const;

    ASPOSE_WORDS_SHARED_API char16_t GetValueOrDefault(char16_t defaultValue) const;

    ASPOSE_WORDS_SHARED_API bool Equals(Aspose::Common::NullableChar other) const;
    ASPOSE_WORDS_SHARED_API bool Equals(char16_t other) const;

    virtual ASPOSE_WORDS_SHARED_API int32_t GetHashCode() const;

    ASPOSE_WORDS_SHARED_API NullableChar();

private:

    char16_t mValue;
    bool pr_HasValue;

};/// \endcond

}
}

namespace System { template<> struct IsBoxable<Aspose::Common::NullableByte> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableSByte> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableInt16> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableUInt16> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableInt32> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableUInt32> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableInt64> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableUInt64> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableSingle> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableDouble> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableDecimal> : std::true_type {}; }
namespace System { template<> struct IsBoxable<Aspose::Common::NullableChar> : std::true_type {}; }
