//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/RW/Ole/VbaModule.h
#pragma once

#include <system/text/encoding.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/memory_stream.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/RW/Ole/VbaModuleType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class VbaModuleCollection; } }
namespace Aspose { namespace Words { class VbaProject; } }
namespace Aspose { namespace Words { class VbaRecordReader; } }
namespace Aspose { namespace Ss { class MemoryStorage; } }
namespace Aspose { namespace Words { class VbaRecordWriter; } }

namespace Aspose {

namespace Words {

/// Provides access to VBA project module.
class ASPOSE_WORDS_SHARED_CLASS VbaModule : public System::Object
{
    typedef VbaModule ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::VbaModuleCollection;
    friend class Aspose::Words::VbaProject;

public:

    /// Gets VBA project module name.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Sets VBA project module name.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Gets VBA project module source code.
    ASPOSE_WORDS_SHARED_API System::String get_SourceCode() const;
    /// Sets VBA project module source code.
    ASPOSE_WORDS_SHARED_API void set_SourceCode(System::String value);
    /// Specifies whether the module is a procedural module, document module, class module, or designer module.
    ASPOSE_WORDS_SHARED_API Aspose::Words::VbaModuleType get_Type() const;
    /// Specifies whether the module is a procedural module, document module, class module, or designer module.
    ASPOSE_WORDS_SHARED_API void set_Type(Aspose::Words::VbaModuleType value);

    /// Creates an empty module.
    ASPOSE_WORDS_SHARED_API VbaModule();

    /// Performs a copy of the <see cref="Aspose::Words::VbaModule">VbaModule</see>.
    /// 
    /// @return The cloned VbaModule.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaModule> Clone();

protected:

    void set_Project(System::SharedPtr<Aspose::Words::VbaProject> value);
    System::String get_DocString() const;
    void set_DocString(System::String value);
    int32_t get_HelpContext() const;
    void set_HelpContext(int32_t value);

    VbaModule(System::String name);

    static System::SharedPtr<Aspose::Words::VbaModule> Read(System::SharedPtr<Aspose::Words::VbaRecordReader> reader, System::SharedPtr<Aspose::Ss::MemoryStorage> storage);
    static System::String ModuleTypeToString(Aspose::Words::VbaModuleType type);
    void Write(System::SharedPtr<Aspose::Words::VbaRecordWriter> writer);
    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::String mName;
    System::String mSourceCode;
    System::String mDocString;
    int32_t mHelpContext;
    Aspose::Words::VbaModuleType mType;
    System::WeakPtr<Aspose::Words::VbaProject> mProject;

    void ExtractModuleType(System::SharedPtr<System::IO::MemoryStream> stream, System::SharedPtr<System::Text::Encoding> encoding);
    static System::ArrayPtr<Aspose::Words::VbaModuleType> GetAllTypesOfVbaModule();
    void ExtractSourceCode(System::SharedPtr<System::IO::MemoryStream> moduleStream, int32_t offset, System::SharedPtr<System::Text::Encoding> encoding);

};

}
}
