//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/RW/Ole/VbaReferenceCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <cstdint>

#include "Aspose.Words.Cpp/RW/Ole/VbaReference.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class VbaProject; } }

namespace Aspose {

namespace Words {

/// Represents a collection of <see cref="Aspose::Words::VbaReference">VbaReference</see> objects.
class ASPOSE_WORDS_SHARED_CLASS VbaReferenceCollection FINAL : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::VbaReference>>
{
    typedef VbaReferenceCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::VbaReference>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::VbaProject;

public:

    /// Returns the number of VBA references in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Removes the first occurrence of a specified VbaReference item from the collection.
    ASPOSE_WORDS_SHARED_API void Remove(System::SharedPtr<Aspose::Words::VbaReference> item);
    /// Removes the VbaReference element at the specified index of the the collection.
    ASPOSE_WORDS_SHARED_API void RemoveAt(int32_t index);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::VbaReference>>> GetEnumerator() override;

    /// Gets <see cref="Aspose::Words::VbaReference">VbaReference</see> object at the specified index.
    /// 
    /// @param index The zero-based index of the reference to get.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaReference> idx_get(int32_t index);

protected:

    VbaReferenceCollection(System::SharedPtr<Aspose::Words::VbaProject> vbaProject);

    void Add(System::SharedPtr<Aspose::Words::VbaReference> vbaReference);
    System::SharedPtr<Aspose::Words::VbaReferenceCollection> Clone();

    virtual ASPOSE_WORDS_SHARED_API ~VbaReferenceCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::VbaReference>>> mReferences;
    System::WeakPtr<Aspose::Words::VbaProject> mVbaProject;

};

}
}
