//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/RW/Ole/VbaReference.h
#pragma once

#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>

#include "Aspose.Words.Cpp/RW/Ole/VbaReferenceType.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class VbaReferenceControl; } }
namespace Aspose { namespace Words { class VbaReferenceProject; } }
namespace Aspose { namespace Words { class VbaReferenceRegistered; } }
namespace Aspose { namespace Words { class VbaProject; } }
namespace Aspose { namespace Words { class VbaReferenceCollection; } }
namespace Aspose { namespace Words { class VbaRecordReader; } }
namespace Aspose { namespace Words { class VbaRecordWriter; } }

namespace Aspose {

namespace Words {

/// Implements a reference to an Automation type library or VBA project.
class ASPOSE_WORDS_SHARED_CLASS VbaReference : public System::Object
{
    typedef VbaReference ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::VbaReferenceControl;
    friend class Aspose::Words::VbaReferenceProject;
    friend class Aspose::Words::VbaReferenceRegistered;
    friend class Aspose::Words::VbaProject;
    friend class Aspose::Words::VbaReferenceCollection;

public:

    /// Gets <see cref="Aspose::Words::VbaReferenceType">VbaReferenceType</see> object that indicates the type of reference that a VbaReference object represents.
    virtual ASPOSE_WORDS_SHARED_API Aspose::Words::VbaReferenceType get_Type() = 0;
    /// Gets a string value containing the identifier of an Automation type library.
    /// 
    /// Depending on reference type, the value of this property can be:
    /// 
    /// - a LibidReference specified at 2.1.1.8 LibidReference of [MS-OVBA]:
    ///   https://docs.microsoft.com/en-us/openspecs/office_file_formats/ms-ovba/3737ef6e-d819-4186-a5f2-6e258ddf66a5
    /// - a ProjectReference specified at 2.1.1.12 ProjectReference of [MS-OVBA]:
    ///   https://docs.microsoft.com/en-us/openspecs/office_file_formats/ms-ovba/9a45ac1a-f1ff-4ebd-958e-537701aa8131
    virtual ASPOSE_WORDS_SHARED_API System::String get_LibId() const = 0;

protected:

    System::String get_Name() const;

    VbaReference(System::String name);

    System::SharedPtr<Aspose::Words::VbaReference> Clone();
    static System::SharedPtr<Aspose::Words::VbaReference> Read(System::SharedPtr<Aspose::Words::VbaRecordReader> reader);
    virtual void Write(System::SharedPtr<Aspose::Words::VbaRecordWriter> writer) = 0;
    virtual void ReadCore(System::SharedPtr<Aspose::Words::VbaRecordReader> reader) = 0;

private:

    System::String mName;

    static System::SharedPtr<Aspose::Words::VbaReference> Create(Aspose::Words::VbaReferenceType type, System::String name);
    virtual ThisType* CppMemberwiseClone() const = 0;

};

}
}
