//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/RW/Ole/VbaProject.h
#pragma once

#include <system/text/string_builder.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/io/memory_stream.h>
#include <system/collections/ilist.h>
#include <system/array.h>
#include <cstdint>

#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class VbaModule; } }
namespace Aspose { namespace Words { class VbaModuleCollection; } }
namespace Aspose { namespace Words { class VbaReferenceCollection; } }
namespace Aspose { namespace Words { class Document; } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Reader { class DocxVbaReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Docx { namespace Writer { class DocxVbaWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Customizations { class TcgWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Reader { class DocReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { class VarFiler; } } } }
namespace Aspose { namespace Words { namespace RW { namespace Doc { namespace Writer { class DocWriter; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Reader { class WmlReader; } } } } }
namespace Aspose { namespace Words { namespace RW { namespace Wml { namespace Writer { class WmlWriter; } } } } }
namespace Aspose { namespace Ss { class MemoryStorage; } }
namespace Aspose { namespace Words { class VbaRecordReader; } }

namespace Aspose {

namespace Words {

/// Provides access to VBA project information.
/// A VBA project inside the document is defined as a collection of VBA modules.
class ASPOSE_WORDS_SHARED_CLASS VbaProject : public System::Object
{
    typedef VbaProject ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::VbaModule;
    friend class Aspose::Words::VbaModuleCollection;
    friend class Aspose::Words::VbaReferenceCollection;
    friend class Aspose::Words::Document;
    friend class Aspose::Words::RW::Docx::Reader::DocxVbaReader;
    friend class Aspose::Words::RW::Docx::Writer::DocxVbaWriter;
    friend class Aspose::Words::RW::Doc::Customizations::TcgWriter;
    friend class Aspose::Words::RW::Doc::Reader::DocReader;
    friend class Aspose::Words::RW::Doc::VarFiler;
    friend class Aspose::Words::RW::Doc::Writer::DocWriter;
    friend class Aspose::Words::RW::Wml::Reader::WmlReader;
    friend class Aspose::Words::RW::Wml::Writer::WmlWriter;

public:

    /// Gets VBA project name.
    ASPOSE_WORDS_SHARED_API System::String get_Name() const;
    /// Sets VBA project name.
    ASPOSE_WORDS_SHARED_API void set_Name(System::String value);
    /// Returns collection of VBA project modules.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaModuleCollection> get_Modules();
    /// Returns the VBA project’s code page.
    ASPOSE_WORDS_SHARED_API int32_t get_CodePage() const;
    /// Shows whether the VbaProject is signed or not.
    ASPOSE_WORDS_SHARED_API bool get_IsSigned();
    /// Gets a collection of VBA project references.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaReferenceCollection> get_References();

    /// Creates a blank VbaProject.
    ASPOSE_WORDS_SHARED_API VbaProject();

    /// Performs a copy of the <see cref="Aspose::Words::VbaProject">VbaProject</see>.
    /// 
    /// @return The cloned VbaProject.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaProject> Clone();

protected:

    System::ArrayPtr<uint8_t> get_Signature() const;
    void set_Signature(System::ArrayPtr<uint8_t> value);
    System::SharedPtr<System::Collections::Generic::IList<System::String>> get_MacroNames() const;
    void set_MacroNames(System::SharedPtr<System::Collections::Generic::IList<System::String>> value);
    int32_t get_SysKind() const;
    int32_t get_Lcid() const;
    int32_t get_LcidInvoke() const;
    System::String get_Description() const;
    System::String get_HelpFilePath1() const;
    System::String get_HelpFilePath2() const;
    int32_t get_HelpContext() const;
    int32_t get_LibFlags() const;
    uint32_t get_VersionMajor() const;
    uint16_t get_VersionMinor() const;
    System::String get_Constants() const;
    System::SharedPtr<Aspose::Ss::MemoryStorage> get_Storage();

    VbaProject(System::SharedPtr<Aspose::Ss::MemoryStorage> vbaStorage);

    void AddMacroName(System::String macroName);
    void ClearStorage();

    virtual ASPOSE_WORDS_SHARED_API ~VbaProject();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<Aspose::Words::VbaRecordReader> mReader;
    int16_t mCodePage;
    System::String mName;
    System::SharedPtr<Aspose::Words::VbaModuleCollection> mModulesCollection;
    System::SharedPtr<Aspose::Words::VbaReferenceCollection> mReferences;
    int32_t mSysKind;
    int32_t mLcid;
    int32_t mLcidInvoke;
    System::String mDescription;
    System::String mHelpFilePath1;
    System::String mHelpFilePath2;
    int32_t mHelpContext;
    int32_t mLibFlags;
    uint32_t mVersionMajor;
    uint16_t mVersionMinor;
    System::String mConstants;
    System::SharedPtr<Aspose::Ss::MemoryStorage> mVbaStorage;
    System::ArrayPtr<uint8_t> mSignature;
    System::SharedPtr<System::Collections::Generic::IList<System::String>> mMacroNames;
    System::String mProtectionState;
    System::String mPassword;
    System::String mVisibilityState;
    System::String mId;

    void ParseProjectStream();
    System::String GetParamValueByName(System::String stringProj, System::String paramName);
    System::SharedPtr<System::IO::MemoryStream> StreamPROJECT();
    static void WriteParam(System::SharedPtr<System::Text::StringBuilder> sb, System::String paramName, System::String paramValue, bool useQuotes);
    static System::SharedPtr<System::IO::MemoryStream> Stream_VBA_PROJECT();
    void WriteDir(System::SharedPtr<System::IO::MemoryStream> compressedDir);

};

}
}
