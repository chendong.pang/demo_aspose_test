//////////////////////////////////////////////////////////////////////////
// Copyright 2001-2020 Aspose Pty Ltd. All Rights Reserved.
//
// This file is part of Aspose.Words. The source code in this file
// is only intended as a supplement to the documentation, and is provided
// "as is", without warranty of any kind, either expressed or implied.
//////////////////////////////////////////////////////////////////////////
/// \file Aspose.Words.Cpp/RW/Ole/VbaModuleCollection.h
#pragma once

#include <system/shared_ptr.h>
#include <system/collections/list.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>

#include "Aspose.Words.Cpp/RW/Ole/VbaModule.h"
#include "Aspose.Words.Cpp/aspose_words_api_defs.h"

namespace Aspose { namespace Words { class VbaProject; } }
namespace Aspose { namespace Collections { namespace Generic { template<typename> class SortedStringListGeneric; } } }

namespace Aspose {

namespace Words {

/// Represents a collection of <see cref="Aspose::Words::VbaModule">VbaModule</see> objects.
class ASPOSE_WORDS_SHARED_CLASS VbaModuleCollection FINAL : public System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::VbaModule>>
{
    typedef VbaModuleCollection ThisType;
    typedef System::Collections::Generic::IEnumerable<System::SharedPtr<Aspose::Words::VbaModule>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSE_WORDS_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class Aspose::Words::VbaProject;

public:

    /// Returns the number of VBA modules in the collection.
    ASPOSE_WORDS_SHARED_API int32_t get_Count();

    /// Adds a module to the collection.
    ASPOSE_WORDS_SHARED_API void Add(System::SharedPtr<Aspose::Words::VbaModule> vbaModule);

    /// Retrieves a <see cref="Aspose::Words::VbaModule">VbaModule</see> object by index.
    /// 
    /// @param index Zero-based index of the module to retrieve.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaModule> idx_get(int32_t index);
    /// Retrieves a <see cref="Aspose::Words::VbaModule">VbaModule</see> object by name, or Null if not found.
    ASPOSE_WORDS_SHARED_API System::SharedPtr<Aspose::Words::VbaModule> idx_get(System::String name);

    /// Removes the specified module from the collection.
    /// 
    /// @param module_ The module to remove.
    ASPOSE_WORDS_SHARED_API void Remove(System::SharedPtr<Aspose::Words::VbaModule> module_);
    ASPOSE_WORDS_SHARED_API System::SharedPtr<System::Collections::Generic::IEnumerator<System::SharedPtr<Aspose::Words::VbaModule>>> GetEnumerator() override;

protected:

    VbaModuleCollection(System::SharedPtr<Aspose::Words::VbaProject> project);

    void Clear();
    System::SharedPtr<Aspose::Words::VbaModuleCollection> Clone(System::SharedPtr<Aspose::Words::VbaProject> project);
    void AddInternal(System::SharedPtr<Aspose::Words::VbaModule> vbaModule);

    virtual ASPOSE_WORDS_SHARED_API ~VbaModuleCollection();

    ASPOSE_WORDS_SHARED_API System::Object::shared_members_type GetSharedMembers() override;

private:

    System::SharedPtr<System::Collections::Generic::List<System::SharedPtr<Aspose::Words::VbaModule>>> mModules;
    System::SharedPtr<Aspose::Collections::Generic::SortedStringListGeneric<System::SharedPtr<Aspose::Words::VbaModule>>> mModulesByName;
    System::WeakPtr<Aspose::Words::VbaProject> mProject;

};

}
}
