#include <Aspose.Words.Cpp/Model/Saving/ImageSaveOptions.h>
#include <Aspose.Words.Cpp/Licensing/License.h>
#include <Aspose.Words.Cpp/Model/Document/Document.h>
#include <Aspose.Words.Cpp/Model/Saving/DocSaveOptions.h>
#include <sstream>
#include <climits>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>




using namespace Aspose::Words;











bool file_to_buf(std::string fpath, std::vector<uint8_t> &fbuf)
{
    FILE * pf = fopen(fpath.c_str(),"rb");
    if ( NULL == pf )
    {
       return false;
    }

    fseek(pf,0L, SEEK_END);
    int size= ftell(pf);
    fseek(pf,0L,SEEK_SET);

    fbuf.resize(size);

    int readsize = fread(fbuf.data() ,size , 1 , pf);
    if ( 1 != readsize )
    {
        fclose ( pf);
        return false;
    }
    fclose ( pf);
    return true;
}




int memory_escape_test(const std::vector<uint8_t> &word_body)
{
    System::ArrayPtr<uint8_t> Bytes =  System::MakeArray<uint8_t>(word_body);
    auto byteStream = System::MakeObject<System::IO::MemoryStream>(Bytes);

    auto doc = System::MakeObject<Document>(byteStream);

    byteStream->Close();    


    auto options = System::MakeObject<Aspose::Words::Saving::ImageSaveOptions>(Aspose::Words::SaveFormat::Jpeg);
    //auto options = System::MakeObject<Aspose::Words::Saving::DocSaveOptions>(Aspose::Words::SaveFormat::Doc);
    
    auto dstStream = System::MakeObject<System::IO::MemoryStream>();

    options->set_Resolution(300);
    options->set_PageCount(1);


    dstStream->set_Position(0);


    doc->Save(dstStream, options);




    dstStream->Close();

    return 0;

}



int main(int argc, char *argv[])
{

    unsigned int pid=0;

    std::vector<uint8_t> fbuf;



    file_to_buf("../src.docx", fbuf);
	

    std::cout << "memory_escape_test begin!" << std::endl;

    std::cout << "you can use the command top to check memory!" << std::endl;


    while(true){
        memory_escape_test(fbuf);

    }
	

	return 0;

}
