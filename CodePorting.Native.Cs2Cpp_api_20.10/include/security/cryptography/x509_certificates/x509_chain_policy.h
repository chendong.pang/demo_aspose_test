#pragma once

#include <system/timespan.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/enum_helpers.h>
#include <system/date_time.h>
#include <security/cryptography/x509_certificates/x509_verification_flags.h>
#include <security/cryptography/x509_certificates/x509_revocation_flag.h>
#include <security/cryptography/x509_certificates/x509_certificate_2_collection.h>

namespace System { namespace Security { namespace Cryptography { namespace X509Certificates {

class X509ChainPolicy FINAL : public Object
{
    typedef X509ChainPolicy ThisType;
    typedef Object BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:
    SharedPtr<X509Certificate2Collection> get_ExtraStore();
    SharedPtr<X509Certificate2Collection> get_CustomTrustStore();
    X509RevocationFlag get_RevocationFlag();
    void set_RevocationFlag(X509RevocationFlag value);
    X509VerificationFlags get_VerificationFlags();
    void set_VerificationFlags(X509VerificationFlags value);
    DateTime get_VerificationTime();
    void set_VerificationTime(DateTime value);
    TimeSpan get_UrlRetrievalTimeout();
    void set_UrlRetrievalTimeout(TimeSpan value);

    X509ChainPolicy();

    void Reset();

protected:
    SharedPtr<X509Certificate2Collection> _extraStore;
    SharedPtr<X509Certificate2Collection> _customTrustStore;

    shared_members_type GetSharedMembers() override;

private:
    X509RevocationFlag _revocationFlag;
    X509VerificationFlags _verificationFlags;
    DateTime pr_VerificationTime;
    TimeSpan pr_UrlRetrievalTimeout;

};

}}}} // System::Security::Cryptography::X509Certificates
