#pragma once

#include <system/string.h>
#include <system/object.h>
#include <system/enum_helpers.h>

namespace System { namespace Security { namespace Cryptography { namespace X509Certificates { enum class X509ChainStatusFlags; } } } }

namespace System { namespace Security { namespace Cryptography { namespace X509Certificates {

class X509ChainStatus : public Object
{
    typedef X509ChainStatus ThisType;
    typedef Object BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:
    X509ChainStatusFlags get_Status();
    void set_Status(X509ChainStatusFlags value);
    String get_StatusInformation();
    void set_StatusInformation(String value);

    X509ChainStatus();

protected:
    shared_members_type GetSharedMembers() override;

private:
    X509ChainStatusFlags pr_Status;
    String _statusInformation;

};

}}}} // System::Security::Cryptography::X509Certificates
