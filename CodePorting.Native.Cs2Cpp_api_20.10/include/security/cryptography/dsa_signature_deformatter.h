/// @file security/cryptography/dsa_signature_deformatter.h
#pragma once

#include <security/cryptography/asymmetric_signature_deformatter.h>

namespace System { namespace Security { namespace Cryptography {

class ASPOSECPP_SHARED_CLASS DSASignatureDeformatter : public AsymmetricSignatureDeformatter
{
public:
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    ASPOSECPP_SHARED_API void SetHashAlgorithm(String strName) override;
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    ASPOSECPP_SHARED_API void SetKey(SharedPtr<AsymmetricAlgorithm> key) override;
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    ASPOSECPP_SHARED_API bool VerifySignature(ArrayPtr<uint8_t> rgbHash, ArrayPtr<uint8_t> rgbSignature) override;
};

}}} // System::Security::Cryptography
