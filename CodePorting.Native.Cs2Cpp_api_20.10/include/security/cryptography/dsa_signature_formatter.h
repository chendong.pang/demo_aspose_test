/// @file security/cryptography/dsa_signature_formatter.h
#pragma once

#include <security/cryptography/asymmetric_signature_formatter.h>

namespace System { namespace Security { namespace Cryptography {

class ASPOSECPP_SHARED_CLASS DSASignatureFormatter : public AsymmetricSignatureFormatter
{
public:
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    ArrayPtr<uint8_t> CreateSignature(ArrayPtr<uint8_t> rgbHash) override;
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    void SetHashAlgorithm(String strName) override;
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    void SetKey(SharedPtr<AsymmetricAlgorithm> key) override;

};

}}} // System::Security::Cryptography
