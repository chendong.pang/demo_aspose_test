/// @file security/cryptography/hmac.h
#pragma once

#include <security/cryptography/hash_algorithm.h>

namespace System { namespace Security { namespace Cryptography {

class ASPOSECPP_SHARED_CLASS HMAC : public HashAlgorithm
{
public:
    /// NOT IMPLEMENTED
    /// @throws NotImplementedException
    static ASPOSECPP_SHARED_API SharedPtr<HMAC> Create();
};

}}} // System::Security::Cryptography
