#pragma once

#include <system/type_info.h>
#include <system/shared_ptr.h>
#include <system/object.h>
#include <system/collections/ienumerator.h>
#include <cstdint>

namespace System { namespace Security { namespace Cryptography { namespace Xml { class CipherReference; } } } }
namespace System { namespace Security { namespace Cryptography { namespace Xml { class EncryptedReference; } } } }
namespace System { namespace Security { namespace Cryptography { namespace Xml { class Reference; } } } }
namespace System { namespace Security { namespace Cryptography { namespace Xml { class Transform; } } } }

namespace System { namespace Security { namespace Cryptography { namespace Xml {

namespace Details { struct ReferenceImpl; }
namespace Details { struct TransformChainImpl; }

// This class represents an ordered chain of transforms
class ASPOSECPP_SHARED_CLASS TransformChain : public Object
{
    typedef TransformChain ThisType;
    typedef Object BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    friend class CipherReference;
    friend class EncryptedReference;
    friend class Reference;
    friend struct Details::ReferenceImpl;

public:
    ASPOSECPP_SHARED_API int32_t get_Count();

    ASPOSECPP_SHARED_API TransformChain();

    ASPOSECPP_SHARED_API void Add(SharedPtr<Transform> transform);
    ASPOSECPP_SHARED_API SharedPtr<Collections::Generic::IEnumerator<SharedPtr<Transform>>> GetEnumerator();

    ASPOSECPP_SHARED_API SharedPtr<Transform> idx_get(int32_t index);

protected:
    ASPOSECPP_SHARED_API shared_members_type GetSharedMembers() override;

private:
    const std::unique_ptr<Details::TransformChainImpl> m_impl;
};

}}}} // System::Security::Cryptography::Xml
