#pragma once

#include <security/cryptography/xml/xml_dsig_c14_ntransform.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class ASPOSECPP_SHARED_CLASS XmlDsigC14NWithCommentsTransform : public XmlDsigC14NTransform
{
    typedef XmlDsigC14NWithCommentsTransform ThisType;
    typedef XmlDsigC14NTransform BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API XmlDsigC14NWithCommentsTransform();
};

}}}} // System::Security::Cryptography::Xml
