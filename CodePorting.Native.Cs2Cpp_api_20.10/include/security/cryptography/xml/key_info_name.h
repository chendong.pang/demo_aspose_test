#pragma once

#include <system/string.h>

#include <security/cryptography/xml/key_info_clause.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class KeyInfoName : public KeyInfoClause
{
    typedef KeyInfoName ThisType;
    typedef KeyInfoClause BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:

    ASPOSECPP_SHARED_API KeyInfoName();

protected:

    String get_Value();
    void set_Value(String value);

    KeyInfoName(String keyName);

    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml() override;
    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml(SharedPtr<System::Xml::XmlDocument> xmlDocument) override;
    ASPOSECPP_SHARED_API void LoadXml(SharedPtr<System::Xml::XmlElement> value) override;

private:

    String _keyName;

};

}}}} // System::Security::Cryptography::Xml
