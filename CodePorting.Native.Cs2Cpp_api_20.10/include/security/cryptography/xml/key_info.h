#pragma once

#include <xml/xml_element.h>
#include <xml/xml_document.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <system/collections/ilist.h>
#include <system/collections/ienumerator.h>
#include <system/collections/ienumerable.h>
#include <security/cryptography/xml/key_info_clause.h>
#include <cstdint>

namespace System { namespace Security { namespace Cryptography { namespace Xml { class Signature; } } } }

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class KeyInfo : public Collections::Generic::IEnumerable<SharedPtr<Xml::KeyInfoClause>>
{
    typedef KeyInfo ThisType;
    typedef IEnumerable<SharedPtr<Xml::KeyInfoClause>> BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    friend class Signature;

public:

    ASPOSECPP_SHARED_API int32_t get_Count();

    ASPOSECPP_SHARED_API KeyInfo();

    ASPOSECPP_SHARED_API void AddClause(SharedPtr<KeyInfoClause> clause);
    ASPOSECPP_SHARED_API SharedPtr<Collections::Generic::IEnumerator<SharedPtr<KeyInfoClause>>> GetEnumerator() override;

protected:

    SharedPtr<System::Xml::XmlElement> GetXml(SharedPtr<System::Xml::XmlDocument> xmlDocument);
    void LoadXml(SharedPtr<System::Xml::XmlElement> value);

    virtual ASPOSECPP_SHARED_API ~KeyInfo();

    ASPOSECPP_SHARED_API shared_members_type GetSharedMembers() override;

private:

    String _id;
    SharedPtr<Collections::Generic::IList<SharedPtr<KeyInfoClause>>> _keyInfoClauses;

};

}}}} // System::Security::Cryptography::Xml
