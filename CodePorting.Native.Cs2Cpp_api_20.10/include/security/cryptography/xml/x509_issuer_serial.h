#pragma once

#include <system/string.h>
#include <system/object.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class ASPOSECPP_SHARED_CLASS X509IssuerSerial : public Object
{
    typedef X509IssuerSerial ThisType;
    typedef Object BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API String get_IssuerName();
    ASPOSECPP_SHARED_API void set_IssuerName(String value);
    ASPOSECPP_SHARED_API String get_SerialNumber();
    ASPOSECPP_SHARED_API void set_SerialNumber(String value);

    ASPOSECPP_SHARED_API X509IssuerSerial(String issuerName, String serialNumber);
    ASPOSECPP_SHARED_API X509IssuerSerial();

private:
    String pr_IssuerName;
    String pr_SerialNumber;
};

}}}} // System::Security::Cryptography::Xml
