#pragma once

#include <xml/xml_element.h>
#include <xml/xml_document.h>
#include <system/shared_ptr.h>
#include <system/object.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml { class KeyInfo; } } } }

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class KeyInfoClause : public Object
{
    typedef KeyInfoClause ThisType;
    typedef Object BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class KeyInfo;

protected:

    ASPOSECPP_SHARED_API KeyInfoClause();

    virtual SharedPtr<System::Xml::XmlElement> GetXml() = 0;
    virtual ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml(SharedPtr<System::Xml::XmlDocument> xmlDocument);
    virtual void LoadXml(SharedPtr<System::Xml::XmlElement> element) = 0;

};

}}}} // System::Security::Cryptography::Xml
