#pragma once

#include <xml/xml_element.h>
#include <xml/xml_document.h>
#include <system/string.h>
#include <system/shared_ptr.h>
#include <security/cryptography/dsa.h>
#include <security/cryptography/xml/key_info_clause.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class DSAKeyValue : public KeyInfoClause
{
    typedef DSAKeyValue ThisType;
    typedef KeyInfoClause BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:

    ASPOSECPP_SHARED_API SharedPtr<DSA> get_Key();
    ASPOSECPP_SHARED_API void set_Key(SharedPtr<DSA> value);

    ASPOSECPP_SHARED_API DSAKeyValue();
    ASPOSECPP_SHARED_API DSAKeyValue(SharedPtr<DSA> key);

protected:

    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml() override;
    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml(SharedPtr<System::Xml::XmlDocument> xmlDocument) override;
    ASPOSECPP_SHARED_API void LoadXml(SharedPtr<System::Xml::XmlElement> value) override;
    ASPOSECPP_SHARED_API Object::shared_members_type GetSharedMembers() override;

private:

    SharedPtr<DSA> _key;
    static const String KeyValueElementName;
    static const String DSAKeyValueElementName;
    static const String PElementName;
    static const String QElementName;
    static const String GElementName;
    static const String JElementName;
    static const String YElementName;
    static const String SeedElementName;
    static const String PgenCounterElementName;

};

}}}} // System::Security::Cryptography::Xml
