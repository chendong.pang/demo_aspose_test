#pragma once

#include <xml/xml_namespace_manager.h>
#include <system/io/stream.h>

#include <security/cryptography/xml/transform.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml { class Reference; } } } }

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class ASPOSECPP_SHARED_CLASS XmlDsigEnvelopedSignatureTransform : public Transform
{
    typedef XmlDsigEnvelopedSignatureTransform ThisType;
    typedef Transform BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    friend class Reference;

public:
    ASPOSECPP_SHARED_API ArrayPtr<TypeInfo> get_InputTypes() override;
    ASPOSECPP_SHARED_API ArrayPtr<TypeInfo> get_OutputTypes() override;

    ASPOSECPP_SHARED_API XmlDsigEnvelopedSignatureTransform();

    ASPOSECPP_SHARED_API void LoadInnerXml(SharedPtr<System::Xml::XmlNodeList> nodeList) override;
    ASPOSECPP_SHARED_API void LoadInput(SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API SharedPtr<Object> GetOutput() override;
    ASPOSECPP_SHARED_API SharedPtr<Object> GetOutput(const TypeInfo& type) override;

protected:
    void set_SignaturePosition(int32_t value);

    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlNodeList> GetInnerXml() override;
    ASPOSECPP_SHARED_API shared_members_type GetSharedMembers() override;

private:
    ArrayPtr<TypeInfo> m_input_types;
    ArrayPtr<TypeInfo> m_output_types;
    SharedPtr<System::Xml::XmlNodeList> m_input_node_list;
    SharedPtr<System::Xml::XmlNamespaceManager> m_ns_manager;
    SharedPtr<System::Xml::XmlDocument> m_containing_document;
    int32_t m_signature_position;

    void LoadStreamInput(SharedPtr<IO::Stream> stream);
    void LoadXmlNodeListInput(SharedPtr<System::Xml::XmlNodeList> nodeList);
    void LoadXmlDocumentInput(SharedPtr<System::Xml::XmlDocument> doc);
};

}}}} // System::Security::Cryptography::Xml
