#pragma once

#include <system/string.h>
#include <security/cryptography/rsa.h>
#include <security/cryptography/xml/key_info_clause.h>

namespace System { namespace Security { namespace Cryptography { namespace Xml {

class ASPOSECPP_SHARED_CLASS RSAKeyValue : public KeyInfoClause
{
    typedef RSAKeyValue ThisType;
    typedef KeyInfoClause BaseType;

    typedef BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API SharedPtr<RSA> get_Key();
    ASPOSECPP_SHARED_API void set_Key(SharedPtr<RSA> value);

    ASPOSECPP_SHARED_API RSAKeyValue();
    ASPOSECPP_SHARED_API RSAKeyValue(SharedPtr<RSA> key);

protected:
    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml() override;
    ASPOSECPP_SHARED_API SharedPtr<System::Xml::XmlElement> GetXml(SharedPtr<System::Xml::XmlDocument> xmlDocument) override;
    ASPOSECPP_SHARED_API void LoadXml(SharedPtr<System::Xml::XmlElement> value) override;
    ASPOSECPP_SHARED_API Object::shared_members_type GetSharedMembers() override;

private:
    SharedPtr<RSA> _key;
    static const String KeyValueElementName;
    static const String RSAKeyValueElementName;
    static const String ModulusElementName;
    static const String ExponentElementName;
};

}}}} // System::Security::Cryptography::Xml
