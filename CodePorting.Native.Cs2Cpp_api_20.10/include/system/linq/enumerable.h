/// @file system/linq/enumerable.h
#pragma once

#include <system/collections/ienumerable.h>
#include <system/collections/list.h>
#include <system/array.h>
#include <system/constraints.h>
#include <system/object_ext.h>
#include <system/exceptions.h>
#include <system/details/equality_helper.h>
#include <type_traits>

namespace System { namespace Linq {

namespace Details {

template <typename T>
class EmptyEnumerator final : public Collections::Generic::IEnumerator<T>
{
public:
    EmptyEnumerator() = default;

    T get_Current() const override
    {
        throw InvalidOperationException(u"Collection is empty");
    }

    bool MoveNext() override
    {
        return false;
    }

    void Reset() override
    {
    }
};

template <typename T>
class EmptyEnumerable final : public Collections::Generic::IEnumerable<T>
{
public:
    EmptyEnumerable() = default;

    SharedPtr<Collections::Generic::IEnumerator<T>> GetEnumerator() override
    {
        static const auto enumerator = MakeObject<EmptyEnumerator<T>>();
        return enumerator;
    }
};

} // namespace Details

/// Provides static LINQ methods.
class Enumerable
{
public:
    /// Returns an empty IEnumerable object.
    template <typename T>
    static SharedPtr<Collections::Generic::IEnumerable<T>> Empty()
    {
        static const auto enumerable = MakeObject<Details::EmptyEnumerable<T>>();
        return enumerable;
    }
};

}} // namespace System::Linq

namespace System { namespace Collections { namespace Generic {

namespace Details {

template <typename T>
T TryGetFirst(IEnumerable<T>& enumerable, bool& found)
{
    if (IList<T>* list = dynamic_cast<IList<T>*>(&enumerable))
    {
        const auto count = list->get_Count();
        if (count > 0)
        {
            found = true;
            return list->idx_get(0);
        }
    }
    else
    {
        const auto enumerator = enumerable.GetEnumerator();
        if (enumerator->MoveNext())
        {
            found = true;
            return enumerator->get_Current();
        }
    }
    found = false;
    return T{};
}

template <typename T>
T TryGetFirst(IEnumerable<T>& enumerable, const Func<T, bool>& predicate, bool& found)
{
    if (predicate == nullptr)
        throw ArgumentNullException(u"predicate");

    const auto enumerator = enumerable.GetEnumerator();
    while (enumerator->MoveNext())
    {
        T value = enumerator->get_Current();
        if (predicate(value))
        {
            found = true;
            return value;
        }
    }
    found = false;
    return T{};
}

template <typename T>
T TryGetLast(IEnumerable<T>& enumerable, bool& found)
{
    if (IList<T>* list = dynamic_cast<IList<T>*>(&enumerable))
    {
        const auto count = list->get_Count();
        if (count > 0)
        {
            found = true;
            return list->idx_get(count - 1);
        }
    }
    else
    {
        const auto enumerator = enumerable.GetEnumerator();
        if (enumerator->MoveNext())
        {
            T value;
            do
            {
                value = enumerator->get_Current();
            } while (enumerator->MoveNext());

            found = true;
            return value;
        }
    }
    found = false;
    return T{};
}

namespace CastRules {

template <typename Source, typename Result>
struct CastType
{
    static constexpr bool None = std::is_same<Source, Result>::value;
    static constexpr bool Static = IsSmartPtr<Source>::value && IsSmartPtr<Result>::value && Constraints::IsBaseOf<Result, Source>::value && !None;
    static constexpr bool Dynamic = IsSmartPtr<Source>::value && IsSmartPtr<Result>::value && !None && !Static;
    static constexpr bool NullableBoxing = std::is_same<Result, Nullable<Source>>::value;
    static constexpr bool NullableUnboxing = std::is_same<Source, Nullable<Result>>::value;
    static constexpr bool Boxing = IsBoxable<Source>::value && std::is_same<Result, SmartPtr<Object>>::value;
    static constexpr bool Unboxing = std::is_same<Source, SmartPtr<Object>>::value && IsBoxable<Result>::value;
    static constexpr bool Invalid = !(None || Static || Dynamic || NullableBoxing || NullableUnboxing || Boxing || Unboxing);
};

// Cast() implementation

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::None, Result> Cast(Source value)
{
    return value;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Static, Result> Cast(Source value)
{
    return Result(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Dynamic, Result> Cast(Source value)
{
    return DynamicCast<typename Result::Pointee_>(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::NullableBoxing, Result> Cast(Source value)
{
    return Result(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::NullableUnboxing, Result> Cast(Source value)
{
    if (value == nullptr)
        throw NullReferenceException(u"Can not unbox null value");

    return static_cast<Result>(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Boxing, Result> Cast(Source value)
{
    return ObjectExt::Box(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Unboxing, Result> Cast(Source value)
{
    return ObjectExt::Unbox<Result>(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Invalid, Result> Cast(Source value)
{
    System::Details::ThrowInvalidCastException();
}

// CanCast() implementation

template <typename T>
bool IsNull(T value)
{
    return false;
}

template <typename T>
bool IsNull(SharedPtr<T> value)
{
    return value == nullptr;
}

template <typename T>
bool IsNull(Nullable<T> value)
{
    return value == nullptr;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::None, bool> CanCast(Source value)
{
    return !IsNull(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Static, bool> CanCast(Source value)
{
    return value != nullptr;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Dynamic, bool> CanCast(Source value)
{
    return ObjectExt::Is<Result>(value);
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::NullableBoxing, bool> CanCast(Source value)
{
    return true;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::NullableUnboxing, bool> CanCast(Source value)
{
    return value != nullptr;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Boxing, bool> CanCast(Source value)
{
    return true;
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Unboxing, bool> CanCast(Source value)
{
    if (value == nullptr)
        return false;

    return value->Is(ObjectExt::GetType<Result>());
}

template <typename Source, typename Result>
std::enable_if_t<CastType<Source, Result>::Invalid, bool> CanCast(Source value)
{
    return false;
}

} // namespace CastRules

// Enumerator used by the IEnumerable.Cast() extension method
template <typename Source, typename Result>
class EnumeratorCastAdapter final : public IEnumerator<Result>
{
public:
    EnumeratorCastAdapter(SharedPtr<IEnumerator<Source>> sourceEnumerator)
        : m_sourceEnumerator(sourceEnumerator)
    {}

    Result get_Current() const override
    {
        return CastRules::Cast<Source, Result>(m_sourceEnumerator->get_Current());
    }

    bool MoveNext() override
    {
        return m_sourceEnumerator->MoveNext();
    }

    void Reset() override
    {
        m_sourceEnumerator->Reset();
    }

private:
    SharedPtr<IEnumerator<Source>> m_sourceEnumerator;
};

// Enumerator used by the IEnumerable.OfType() extension method
template <typename Source, typename Result>
class EnumeratorOfTypeAdapter final : public IEnumerator<Result>
{
public:
    EnumeratorOfTypeAdapter(SharedPtr<IEnumerator<Source>> sourceEnumerator)
        : m_sourceEnumerator(sourceEnumerator)
    {}

    Result get_Current() const override
    {
        return CastRules::Cast<Source, Result>(m_sourceEnumerator->get_Current());
    }

    bool MoveNext() override
    {
        while (m_sourceEnumerator->MoveNext())
        {
            if (CastRules::CanCast<Source, Result>(m_sourceEnumerator->get_Current()))
                return true;
        }
        return false;
    }

    void Reset() override
    {
        m_sourceEnumerator->Reset();
    }

private:
    SharedPtr<IEnumerator<Source>> m_sourceEnumerator;
};

// Enumerator used by the IEnumerable.Select() extension method
template <typename Source, typename Result>
class EnumeratorSelectAdapter final : public IEnumerator<Result>
{
public:
    EnumeratorSelectAdapter(SharedPtr<IEnumerator<Source>> sourceEnumerator, const Func<Source, Result>& selector)
        : m_sourceEnumerator(sourceEnumerator)
        , m_selector(selector)
    {}

    Result get_Current() const override
    {
        return m_selector(m_sourceEnumerator->get_Current());
    }

    bool MoveNext() override
    {
        return m_sourceEnumerator->MoveNext();
    }

    void Reset() override
    {
        m_sourceEnumerator->Reset();
    }

private:
    SharedPtr<IEnumerator<Source>> m_sourceEnumerator;
    Func<Source, Result> m_selector;
};

// Enumerable used by the IEnumerable.Cast() and IEnumerable.OfType() extension methods
template <typename Source, typename Result, typename EnumeratorAdapter>
class EnumerableAdapter final : public IEnumerable<Result>
{
public:
    EnumerableAdapter(SharedPtr<IEnumerable<Source>> sourceEnumerable)
        : m_sourceEnumerable(sourceEnumerable)
    {}

    SharedPtr<IEnumerator<Result>> GetEnumerator() override
    {
        return MakeObject<EnumeratorAdapter>(m_sourceEnumerable->GetEnumerator());
    }

private:
    SharedPtr<IEnumerable<Source>> m_sourceEnumerable;
};

// Enumerable used by the IEnumerable.Select() extension method
template <typename Source, typename Result>
class EnumerableSelectAdapter final : public IEnumerable<Result>
{
public:
    EnumerableSelectAdapter(SharedPtr<IEnumerable<Source>> sourceEnumerable, const Func<Source, Result>& selector)
        : m_sourceEnumerable(sourceEnumerable)
        , m_selector(selector)
    {}

    SharedPtr<IEnumerator<Result>> GetEnumerator() override
    {
        return MakeObject<EnumeratorSelectAdapter<Source, Result>>(m_sourceEnumerable->GetEnumerator(), m_selector);
    }

private:
    SharedPtr<IEnumerable<Source>> m_sourceEnumerable;
    Func<Source, Result> m_selector;
};

} // namespace Details

template<typename T>
T IEnumerable<T>::LINQ_ElementAt(int index)
{
    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        if (index == 0)
        {
            return enumerator->Current();
        }
        index--;
    }
    return T();
}

template<typename T>
T IEnumerable<T>::LINQ_First()
{
    bool found = false;
    T first = Details::TryGetFirst(*this, found);
    if (!found)
        throw InvalidOperationException(u"The source sequence is empty");

    return first;
}

template<typename T>
T IEnumerable<T>::LINQ_First(const Func<T, bool>& predicate)
{
    bool found = false;
    T first = Details::TryGetFirst(*this, predicate, found);
    if (!found)
        throw InvalidOperationException(u"No element satisfies the specified condition");

    return first;
}

template<typename T>
T IEnumerable<T>::LINQ_FirstOrDefault()
{
    bool found = false;
    return Details::TryGetFirst(*this, found);
}

template<typename T>
T IEnumerable<T>::LINQ_Last()
{
    bool found = false;
    T last = Details::TryGetLast(*this, found);
    if (!found)
        throw InvalidOperationException(u"The source sequence is empty.");

    return last;
}

template<typename T>
T IEnumerable<T>::LINQ_LastOrDefault()
{
    bool found = false;
    return Details::TryGetLast(*this, found);
}

template<typename T>
System::SharedPtr<Collections::Generic::List<T>> IEnumerable<T>::LINQ_ToList()
{
    //return System::MakeObject<Collections::Generic::List<T>>(GetEnumerator());
    auto list = System::MakeObject<Collections::Generic::List<T>>();
    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        list->Add(enumerator->Current());
    }
    return list;
}

template<typename T>
int IEnumerable<T>::LINQ_Count()
{
    int count = 0;
    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        count++;
    }
    return count;
}

template<typename T>
int IEnumerable<T>::LINQ_Count(const Func<T, bool>& predicate)
{
    if (predicate == nullptr)
        throw ArgumentNullException(u"predicate");

    int count = 0;
    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        if (predicate(enumerator->get_Current()))
        {
            count++;
        }
    }
    return count;
}

template<typename T>
System::ArrayPtr<T> IEnumerable<T>::LINQ_ToArray()
{
    return LINQ_ToList()->ToArray();
}

template <typename T>
bool IEnumerable<T>::LINQ_All(std::function<bool(T)> predicate)
{
    if (!predicate)
        throw ArgumentNullException(u"predicate");

    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
        if (!predicate(enumerator->Current()))
            return false;
    return true;
}

template <typename T>
bool IEnumerable<T>::LINQ_Any()
{
    auto enumerator = GetEnumerator();
    if (enumerator->MoveNext())
        return true;
    return false;
}

template <typename T>
bool IEnumerable<T>::LINQ_Any(std::function<bool(T)> predicate)
{
    if (!predicate)
        throw ArgumentNullException(u"predicate");

    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
        if (predicate(enumerator->Current()))
            return true;
    return false;
}

template <typename T>
T IEnumerable<T>::LINQ_FirstOrDefault(std::function<bool(T)> predicate)
{
    if (!predicate)
        throw ArgumentNullException(u"predicate");

    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        T val = enumerator->Current();
        if (predicate(val))
            return val;
    }
    return T();
}

template <typename T>
SharedPtr<IEnumerable<T>> IEnumerable<T>::LINQ_Where(std::function<bool(T)> predicate)
{
    if (!predicate)
        throw ArgumentNullException(u"predicate");

    auto list = System::MakeObject<Collections::Generic::List<T>>();
    auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        T val = enumerator->Current();
        if (predicate(val))
            list->Add(val);
    }
    return static_cast<SharedPtr<IEnumerable<T>>>(list);
}

template <typename T>
bool IEnumerable<T>::LINQ_Contains(T value)
{
    const auto enumerator = GetEnumerator();
    while (enumerator->MoveNext())
    {
        if (System::Details::AreEqual(value, enumerator->get_Current()))
            return true;
    }
    return false;
}

template <typename Source>
template <typename Result>
SharedPtr<IEnumerable<Result>> IEnumerable<Source>::LINQ_Cast()
{
    using EnumeratorAdapter = Details::EnumeratorCastAdapter<Source, Result>;
    using EnumerableAdapter = Details::EnumerableAdapter<Source, Result, EnumeratorAdapter>;
    return MakeObject<EnumerableAdapter>(MakeSharedPtr(this));
}

template <typename Source>
template <typename Result>
SharedPtr<IEnumerable<Result>> IEnumerable<Source>::LINQ_OfType()
{
    using EnumeratorAdapter = Details::EnumeratorOfTypeAdapter<Source, Result>;
    using EnumerableAdapter = Details::EnumerableAdapter<Source, Result, EnumeratorAdapter>;
    return MakeObject<EnumerableAdapter>(MakeSharedPtr(this));
}

template <typename Source>
template <typename Result>
SharedPtr<IEnumerable<Result>> IEnumerable<Source>::LINQ_Select(const Func<Source, Result>& selector)
{
    if (selector == nullptr)
        throw ArgumentNullException(u"selector");

    using EnumerableAdapter = Details::EnumerableSelectAdapter<Source, Result>;
    return MakeObject<EnumerableAdapter>(MakeSharedPtr(this), selector);
}



template <typename Source>
template <typename Key>
SharedPtr<IEnumerable<Source>> IEnumerable<Source>::LINQ_OrderBy(const Func<Source, Key>& keySelector)
{
    auto keys = LINQ_Select(keySelector)->LINQ_ToArray();
    auto items = LINQ_ToArray();
    items->Sort(keys, items);
    return items;
}

template <typename Source>
SharedPtr<IEnumerable<Source>> IEnumerable<Source>::LINQ_Concat(SharedPtr<IEnumerable<Source>> sequesnce)
{
    auto first = LINQ_ToList();
    auto second = sequesnce->LINQ_ToList();

    first->AddRange(second);

    return first;
}


}}} // namespace System::Collections::Generic
