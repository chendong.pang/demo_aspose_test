/// @file system/memory_management.h
#pragma once

#include <system/object.h>
#include <system/smart_ptr.h>
#include <system/exceptions.h>
#include <system/details/objects_bag.h>
#include <type_traits>

namespace CsToCppPorter {

namespace Details {

template <size_t ObjectCount>
class ObjectsHolder final : public System::Object
{
public:
    template <typename ...Args, typename = std::enable_if_t<sizeof...(Args) == ObjectCount>>
    ObjectsHolder(const System::SmartPtr<Args>& ...args)
        : m_objects{ args... }
    {
    }

    ObjectsHolder(const ObjectsHolder&) = delete;
    ObjectsHolder& operator=(const ObjectsHolder&) = delete;

private:
    System::SmartPtr<System::Object> m_objects[ObjectCount];

    System::Object::shared_members_type GetSharedMembers() override
    {
        auto result = System::Object::GetSharedMembers();
        for (auto& object : m_objects)
            result.Add("m_objects[]", object);
        return result;
    }

    RTTI_INFO_TEMPLATE_CLASS(CsToCppPorter::Details::ObjectsHolder<ObjectCount>, System::BaseTypesInfo<System::Object>);
};

} // namespace Details

/// @brief Defines a methods that changes the lifetime of objects.
class MemoryManagement
{
public:
    /// Highly optimized container designed to extend objects lifetime.
    using ObjectsBag = System::Details::ObjectsBag;

    /// @brief Creates a smart pointer using the aliasing constructor.
    /// Creates a pointer to the @p target object which controls lifetime of the @p owner object.
    /// This pointer's expiration is bound to expiration of the @p owner object,
    /// just like this of any other pointer that owns @p owner object.
    /// If converted to weak type, it is still consistent with other pointers to @p owner and
    /// does not expire while there is at least one shared pointer to @p owner object.
    /// Also, the returned pointer does not ensure that @p target object is alive, so some other
    /// means (e. g. shared pointer stored in the @p owner object) should do so.
    /// @tparam T1 Type of smart pointer to the object that the new smart pointer will refer to.
    /// @tparam T2 Type of smart pointer to an object whose ownership is shared with a new smart pointer.
    /// @param target Smart pointer to the object that the new smart pointer will refer to.
    /// @param owner Smart pointer to an object whose ownership is shared with a new smart pointer.
    /// @return New smart pointer.
    template<typename T1, typename T2>
    static T1 BindLifetime(const T1& target, const T2& owner)
    {
        return T1(owner, target.get());
    }

    /// @brief Creates a smart pointer using the aliasing constructor.
    /// Creates a pointer to the @p target object which controls lifetime of the @p owner object.
    /// This pointer's expiration is bound to expiration of the @p owner object,
    /// just like this of any other pointer that owns @p owner object.
    /// If converted to weak type, it is still consistent with other pointers to @p owner and
    /// does not expire while there is at least one shared pointer to @p owner object.
    /// Also, the returned pointer does not ensure that @p target object is alive, so some other
    /// means (e. g. shared pointer stored in the @p owner object) should do so.
    /// @tparam T1 Type of smart pointer to the object that the new smart pointer will refer to.
    /// @tparam T2 Type of smart pointer to an object whose ownership is shared with a new smart pointer.
    /// @param target Smart pointer to the object that the new smart pointer will refer to.
    /// @param owner Smart pointer to an object whose ownership is shared with a new smart pointer.
    /// @return New smart pointer.
    template<typename T1, typename T2>
    static typename std::enable_if<!std::is_reference<T1>::value, T1>::type BindLifetime(T1&& target, const T2& owner)
    {
        if (target->SharedCount() == 1)
            throw System::InvalidOperationException(u"Can not bind lifetime for a temporary target object");

        return T1(owner, target.get());
    }

    /// Creates a smart pointer using the aliasing constructor and
    /// moves @p target and @p objects pointers to the "proxy" objects holder.
    /// Creates a pointer to the @p target and extends the lifetime of all objects to the lifetime of this pointer.
    /// The resulting pointer guarantees all parameters to remain alive, even if it is the only pointer that
    /// keeps track of them.
    /// The resulting pointer effectively owns all objects passed to this method, however, it has its own
    /// expiration track.
    /// This means, that converting its own copy to weak type will expire it, even if all objects it tracks
    /// are still alive.
    /// @tparam T Type of the object that the new smart pointer will refer to.
    /// @tparam Objects Types of the objects whose ownership is shared with a new smart pointer.
    /// @param target Smart pointer to the object that the new smart pointer will refer to.
    /// @param objects Smart pointers to the objects whose ownership is shared with a new smart pointer.
    /// @return New smart pointer.
    template<typename T, typename ...Objects>
    static T ExtendLifetime(const T& target, const Objects& ...objects)
    {
        System::SmartPtr<System::Object> holder(
            new Details::ObjectsHolder<1 + sizeof...(objects)>(target, objects...));
        return T(holder, target.get());
    }
};

} // namespace CsToCppPorter
