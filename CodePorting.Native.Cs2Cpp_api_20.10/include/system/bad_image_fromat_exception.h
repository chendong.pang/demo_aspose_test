/// @file system/bad_image_fromat_exception.h

#pragma once

#include "system/details_exception_with_filename.h"

namespace System {
/// The exception that is thrown when the file image of a dynamic link library (DLL) or an executable program is
/// invalid.
class ASPOSECPP_SHARED_CLASS Details_BadImageFormatException
    : public Details_ExceptionWithFilename<Details_SystemException>
{
    friend class System::ExceptionWrapperHelper;
    FRIEND_FUNCTION_System_MakeObject;
    RTTI_INFO_NAMED(System::Details_BadImageFormatException, "System::BadImageFormatException",
                    ::System::BaseTypesInfo<System::Object>);

protected:
    /// Initializes a new instance of the BadImageFormatException class.
    ASPOSECPP_SHARED_API Details_BadImageFormatException();
    /// Initializes a new instance of the BadImageFormatException class with a specified error message.
    /// @param message The message that describes the error.
    ASPOSECPP_SHARED_API Details_BadImageFormatException(const String& message);
    /// Initializes a new instance of the BadImageFormatException class with a specified error message and a reference
    /// to the inner exception that is the cause of this exception.
    /// @param message The error message that explains the reason for the exception.
    /// @param innerException The exception that is the cause of the current exception. If the inner parameter is not
    /// a null reference, the current exception is raised in a catch block that handles the inner exception.
    ASPOSECPP_SHARED_API Details_BadImageFormatException(const String& message, const Exception& innerException);
    /// Initializes a new instance of the BadImageFormatException class with a specified error message and file name.
    /// @param message A message that describes the error.
    /// @param fileName The full name of the file with the invalid image.
    ASPOSECPP_SHARED_API Details_BadImageFormatException(const String& message, const String& fileName);
    /// Initializes a new instance of the BadImageFormatException class with a specified error message and a reference
    /// to the inner exception that is the cause of this exception.
    /// @param message The error message that explains the reason for the exception.
    /// @param fileName The full name of the file with the invalid image.
    /// @param innerException The exception that is the cause of the current exception. If the inner parameter is not
    /// null, the current exception is raised in a catch block that handles the inner exception.
    ASPOSECPP_SHARED_API Details_BadImageFormatException(const String& message, const String& fileName,
                                                         const Exception& innerException);
    /// @see System::Exception::DoThrow
    [[noreturn]] void DoThrow(const System::ExceptionPtr& self) const override;
};

using BadImageFormatException = System::ExceptionWrapper<Details_BadImageFormatException>;
} // namespace System
