#pragma once
#include <system/collections/icollection_ng.h>

namespace System {
namespace Collections {

/// IList Represents a non-generic collection of objects that can be individually accessed by index.
class IList : virtual public System::Collections::ICollection
{
    /// RTTI information.
    RTTI_INFO_TEMPLATE_CLASS(System::Collections::IList, System::BaseTypesInfo<System::Collections::ICollection>);
public:
    /// Gets the element at the specified index.
    /// @param index index of the element
    /// @param dummy fake param to help resolve disambiguity
    virtual SharedPtr<System::Object>  idx_get(int index, int dummy = 0) const = 0;

    /// Adds element to the end of list.
    /// @param item Item to add.
    virtual int Add(SharedPtr<System::Object> item) = 0;

    /// Removes first instance of specific item from list.
    /// @param item Item to remove.
    /// @return True if item was found and removed, false otherwise.
    virtual void Remove(SharedPtr<System::Object> item) = 0;

    /// Deletes all elements.
    virtual void Clear() = 0;

    /// Checks if item is present in list.
    /// @param item Item to look for.
    /// @return True if item is found, false otherwise.
    virtual bool Contains(SharedPtr<System::Object> item) const = 0;

    /// Gets first index of specific item.
    /// @param item Item to look for.
    /// @return Index of first occurance of specified item or -1 if not found.
    virtual int IndexOf(SharedPtr<System::Object> item)  const = 0;

    /// Inserts item at specified position.
    /// @param index Index to insert item into.
    /// @param item Item to insert.
    virtual void Insert(int index, SharedPtr<System::Object> item) = 0;

    /// Removes item at specified position.
    /// @param index Index to delete item at.
    virtual void RemoveAt(int index) = 0;

    /// Gets a value indicating whether the IList has a fixed size.
    virtual bool get_IsFixedSize() const { return false; }
};

} // namespace Collection
} // namespace System


