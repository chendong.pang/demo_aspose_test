#ifndef _ASPOSE_VERSION_DEFINES_H
#define _ASPOSE_VERSION_DEFINES_H

/// Revision from which aspose_cpp library was taken and/or built.
#define ASPOSECPPLIB_REVISION         "d6722042f6d690affec9718cd1b968da6a1fd54a" 

/// aspose_cpp library major version.
#define ASPOSECPPLIB_VERSION_MAJOR    20
/// aspose_cpp library minor version.
#define ASPOSECPPLIB_VERSION_MINOR    10
/// aspose_cpp library revision.
#define ASPOSECPPLIB_VERSION_REVISION 0
/// aspose_cpp library build number.
#define ASPOSECPPLIB_VERSION_BUILD    207

/// Ported product major version.
#define PRODUCT_VERSION_MAJOR         0
/// Ported product minor version.
#define PRODUCT_VERSION_MINOR         0
/// Ported product revusion.
#define PRODUCT_VERSION_REVISION      0
/// Ported product build number.
#define PRODUCT_VERSION_BUILD         0

/// Ported product name.
#define PRODUCT_NAME                  ""
/// Original file name.
#define ORIGINAL_FILE_NAME            "aspose_cpp_vc14x86.dll"
/// Copyright string.
#define COPYRIGHT_STRING              "All rights reserved 2001-2020"

#endif // _ASPOSE_VERSION_DEFINES_H
