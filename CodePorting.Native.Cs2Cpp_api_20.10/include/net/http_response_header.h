﻿/// @file net/http_response_header.h
#pragma once

#include <system/array.h>
#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net {

enum class HttpResponseHeader
{
    CacheControl = 0,
    Connection = 1,
    Date = 2,
    KeepAlive = 3,
    Pragma = 4,
    Trailer = 5,
    TransferEncoding = 6,
    Upgrade = 7,
    Via = 8,
    Warning = 9,
    Allow = 10,
    ContentLength = 11,
    ContentType = 12,
    ContentEncoding = 13,
    ContentLanguage = 14,
    ContentLocation = 15,
    ContentMd5 = 16,
    ContentRange = 17,
    Expires = 18,
    LastModified = 19,
    AcceptRanges = 20,
    Age = 21,
    ETag = 22,
    Location = 23,
    ProxyAuthenticate = 24,
    RetryAfter = 25,
    Server = 26,
    SetCookie = 27,
    Vary = 28,
    WwwAuthenticate = 29
};

class HttpResponseHeaderExtensions : public System::Object
{
    typedef HttpResponseHeaderExtensions ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static String GetName(HttpResponseHeader header);

private:
    static System::ArrayPtr<String> s_names;

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}} // namespace System::Net
