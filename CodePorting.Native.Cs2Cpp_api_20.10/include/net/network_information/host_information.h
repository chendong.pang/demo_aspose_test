﻿/// @file net/network_information/host_information.h
#pragma once

#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net { namespace NetworkInformation {

class HostInformation : public System::Object
{
    typedef HostInformation ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static String get_HostName();
    static String get_DomainName();
};

}}} // namespace System::Net::NetworkInformation
