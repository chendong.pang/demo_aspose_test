﻿/// @file net/cookie_container.h
#pragma once

#include <system/collections/dictionary.h>
#include <system/collections/icomparer.h>
#include <system/collections/keyvalue_pair.h>
#include <system/collections/sorted_list.h>
#include <system/object.h>

#include <net/cookie.h>
#include <net/cookie_collection.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS HeaderVariantInfo : public System::Object
{
    typedef HeaderVariantInfo ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API String get_Name();
    ASPOSECPP_SHARED_API CookieVariant get_Variant();

    ASPOSECPP_SHARED_API HeaderVariantInfo(String name, CookieVariant variant);
    ASPOSECPP_SHARED_API HeaderVariantInfo();

private:
    String _name;
    CookieVariant _variant;
};

class ASPOSECPP_SHARED_CLASS PathList : public System::Object
{
    typedef PathList ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

private:
    class PathListComparer FINAL : public System::Collections::Generic::IComparer<System::String>
    {
        typedef PathListComparer ThisType;
        typedef System::Collections::Generic::IComparer<System::String> BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        RTTI_INFO_DECL();

    public:
        static System::SharedPtr<PathList::PathListComparer> StaticInstance;

    private:
        int32_t Compare(args_type x, args_type y) const override;
    };

public:
    ASPOSECPP_SHARED_API int32_t get_Count() const;
    ASPOSECPP_SHARED_API System::SharedPtr<Object> get_SyncRoot() const;

    ASPOSECPP_SHARED_API static System::SharedPtr<PathList> Create();
    ASPOSECPP_SHARED_API int32_t GetCookiesCount();

    ASPOSECPP_SHARED_API System::SharedPtr<CookieCollection> idx_get(String s);
    ASPOSECPP_SHARED_API void idx_set(String s, System::SharedPtr<CookieCollection> value);

    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::IEnumerator<
        Collections::Generic::KeyValuePair<String, System::SharedPtr<CookieCollection>>>>
        GetEnumerator();

private:
    System::SharedPtr<Collections::Generic::SortedList<String, System::SharedPtr<CookieCollection>>> _list;

    PathList(System::SharedPtr<Collections::Generic::SortedList<String, System::SharedPtr<CookieCollection>>> list);
};

// CookieContainer
// Manage cookies for a user (implicit). Based on RFC 2965.
class ASPOSECPP_SHARED_CLASS CookieContainer : public System::Object
{
    typedef CookieContainer ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API static const int32_t DefaultCookieLimit;
    ASPOSECPP_SHARED_API static const int32_t DefaultPerDomainCookieLimit;
    ASPOSECPP_SHARED_API static const int32_t DefaultCookieLengthLimit;

    ASPOSECPP_SHARED_API int32_t get_Capacity();
    ASPOSECPP_SHARED_API void set_Capacity(int32_t value);
    ASPOSECPP_SHARED_API int32_t get_Count();
    ASPOSECPP_SHARED_API int32_t get_MaxCookieSize();
    ASPOSECPP_SHARED_API void set_MaxCookieSize(int32_t value);
    ASPOSECPP_SHARED_API int32_t get_PerDomainCapacity();
    ASPOSECPP_SHARED_API void set_PerDomainCapacity(int32_t value);

    ASPOSECPP_SHARED_API CookieContainer();
    ASPOSECPP_SHARED_API CookieContainer(int32_t capacity);
    ASPOSECPP_SHARED_API CookieContainer(int32_t capacity, int32_t perDomainCapacity, int32_t maxCookieSize);

    ASPOSECPP_SHARED_API void Add(System::SharedPtr<Cookie> cookie);
    ASPOSECPP_SHARED_API void Add(System::SharedPtr<Cookie> cookie, bool throwOnError);
    ASPOSECPP_SHARED_API void Add(System::SharedPtr<CookieCollection> cookies);
    ASPOSECPP_SHARED_API bool IsLocalDomain(String host);
    ASPOSECPP_SHARED_API void Add(System::SharedPtr<Uri> uri, System::SharedPtr<Cookie> cookie);
    ASPOSECPP_SHARED_API void Add(System::SharedPtr<Uri> uri, System::SharedPtr<CookieCollection> cookies);
    ASPOSECPP_SHARED_API System::SharedPtr<CookieCollection> CookieCutter(System::SharedPtr<Uri> uri, String headerName,
                                                                          String setCookieHeader, bool isThrow);
    ASPOSECPP_SHARED_API System::SharedPtr<CookieCollection> GetCookies(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API System::SharedPtr<CookieCollection> InternalGetCookies(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API String GetCookieHeader(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API String GetCookieHeader(System::SharedPtr<Uri> uri, String& optCookie2);
    ASPOSECPP_SHARED_API void SetCookies(System::SharedPtr<Uri> uri, String cookieHeader);

private:
    static System::ArrayPtr<HeaderVariantInfo> s_headerInfo;
    System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<PathList>>> _domainTable;
    int32_t _maxCookieSize;
    int32_t _maxCookies;
    int32_t _maxCookiesPerDomain;
    int32_t _count;
    String _fqdnMyDomain;

    bool AgeCookies(String domain);
    int32_t ExpireCollection(System::SharedPtr<CookieCollection> cc);
    void BuildCookieCollectionFromDomainMatches(System::SharedPtr<Uri> uri, bool isSecure, int32_t port,
                                                System::SharedPtr<CookieCollection> cookies,
                                                System::SharedPtr<Collections::Generic::List<String>> domainAttribute,
                                                bool matchOnlyPlainCookie);
    void MergeUpdateCollections(System::SharedPtr<CookieCollection> destination,
                                System::SharedPtr<CookieCollection> source, int32_t port, bool isSecure,
                                bool isPlainOnly);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}} // namespace System::Net
