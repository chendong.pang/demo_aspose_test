﻿/// @file net/web/services/protocol/soap_header.h
#pragma once

#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <xml/xml_element.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ABSTRACT SoapHeader : public System::Object
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Actor();
    ASPOSECPP_SHARED_API void set_Actor(String value);
    ASPOSECPP_SHARED_API bool get_DidUnderstand();
    ASPOSECPP_SHARED_API void set_DidUnderstand(bool value);
    ASPOSECPP_SHARED_API String get_EncodedMustUnderstand();
    ASPOSECPP_SHARED_API void set_EncodedMustUnderstand(String value);
    ASPOSECPP_SHARED_API bool get_MustUnderstand();
    ASPOSECPP_SHARED_API void set_MustUnderstand(bool value);
    ASPOSECPP_SHARED_API String get_EncodedMustUnderstand12();
    ASPOSECPP_SHARED_API void set_EncodedMustUnderstand12(String value);
    ASPOSECPP_SHARED_API String get_EncodedRelay();
    ASPOSECPP_SHARED_API void set_EncodedRelay(String value);
    ASPOSECPP_SHARED_API bool get_Relay();
    ASPOSECPP_SHARED_API void set_Relay(bool value);
    ASPOSECPP_SHARED_API String get_Role();
    ASPOSECPP_SHARED_API void set_Role(String value);

    ASPOSECPP_SHARED_API SoapHeader(System::SharedPtr<Xml::XmlElement> elem);

protected:
    ASPOSECPP_SHARED_API SoapHeader();

private:
    String actor;
    bool didUnderstand;
    bool mustUnderstand;
    String role;
    bool relay;
};

}}}} // namespace System::Web::Services::Protocols
