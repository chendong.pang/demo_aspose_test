/// @file net/web/services/protocol/soap_service_routing_style.h
#pragma once
namespace System { namespace Web { namespace Services { namespace Protocols {

enum class SoapServiceRoutingStyle
{
    SoapAction = 0x0,
    RequestElement = 0x1
};
}}}} // namespace System::Web::Services::Protocols
