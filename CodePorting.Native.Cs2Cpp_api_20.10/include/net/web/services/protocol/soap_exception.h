﻿/// @file net/web/services/protocol/soap_exception.h
#pragma once

#include <system/exceptions.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <xml/xml_node.h>
#include <xml/xml_qualified_name.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class SoapFaultSubCode;

class Details_SoapException;
using SoapException = System::ExceptionWrapper<Details_SoapException>;

class Details_SoapException : public System::Details_SystemException
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    static System::SharedPtr<Xml::XmlQualifiedName> ClientFaultCode;
    static System::SharedPtr<Xml::XmlQualifiedName> DetailElementName;
    static System::SharedPtr<Xml::XmlQualifiedName> MustUnderstandFaultCode;
    static System::SharedPtr<Xml::XmlQualifiedName> ServerFaultCode;
    static System::SharedPtr<Xml::XmlQualifiedName> VersionMismatchFaultCode;

    String get_Actor();
    System::SharedPtr<Xml::XmlQualifiedName> get_Code();
    System::SharedPtr<Xml::XmlNode> get_Detail();
    String get_Lang();
    String get_Role();
    System::SharedPtr<SoapFaultSubCode> get_SubCode();
    String get_Node();

    Details_SoapException();
    Details_SoapException(std::nullptr_t);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, Exception innerException);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor,
                          Exception innerException);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor,
                          System::SharedPtr<Xml::XmlNode> detail);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor,
                          System::SharedPtr<Xml::XmlNode> detail, Exception innerException);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code,
                          System::SharedPtr<SoapFaultSubCode> subcode);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor, String role,
                          System::SharedPtr<Xml::XmlNode> detail, System::SharedPtr<SoapFaultSubCode> subcode,
                          Exception innerException);
    Details_SoapException(String message, System::SharedPtr<Xml::XmlQualifiedName> code, String actor, String role,
                          String lang, System::SharedPtr<Xml::XmlNode> detail,
                          System::SharedPtr<SoapFaultSubCode> subcode, Exception innerException);

    static bool IsClientFaultCode(System::SharedPtr<Xml::XmlQualifiedName> code);
    static bool IsMustUnderstandFaultCode(System::SharedPtr<Xml::XmlQualifiedName> code);
    static bool IsServerFaultCode(System::SharedPtr<Xml::XmlQualifiedName> code);
    static bool IsVersionMismatchFaultCode(System::SharedPtr<Xml::XmlQualifiedName> code);

private:
    String actor;
    System::SharedPtr<Xml::XmlQualifiedName> code;
    System::SharedPtr<Xml::XmlNode> detail;
    String lang;
    String role;
    System::SharedPtr<SoapFaultSubCode> subcode;
};

}}}} // namespace System::Web::Services::Protocols
