﻿/// @file net/web/services/protocol/soap_parameter_style.h
#pragma once

namespace System { namespace Web { namespace Services { namespace Protocols {

enum class SoapParameterStyle
{
    Default,
    Bare,
    Wrapped
};
}}}} // namespace System::Web::Services::Protocols
