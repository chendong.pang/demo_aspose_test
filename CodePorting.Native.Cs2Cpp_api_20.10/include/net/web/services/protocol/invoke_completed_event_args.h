﻿/// @file net/web/services/protocol/invoke_completed_event_args.h
#pragma once

#include <system/array.h>
#include <system/component_model/async_completed_event_args.h>
#include <system/exceptions.h>
#include <system/object.h>
#include <system/shared_ptr.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ASPOSECPP_SHARED_CLASS InvokeCompletedEventArgs : public System::ComponentModel::AsyncCompletedEventArgs
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::ArrayPtr<System::SharedPtr<Object>> get_Results();
    ASPOSECPP_SHARED_API InvokeCompletedEventArgs(Exception error, bool cancelled, System::SharedPtr<Object> userState,
                                                  System::ArrayPtr<System::SharedPtr<Object>> results);

private:
    System::ArrayPtr<System::SharedPtr<Object>> _results;
};

}}}} // namespace System::Web::Services::Protocols
