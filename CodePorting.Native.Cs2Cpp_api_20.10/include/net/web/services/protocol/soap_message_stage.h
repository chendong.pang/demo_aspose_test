﻿/// @file net/web/services/protocol/soap_message_stage.h
#pragma once
namespace System { namespace Web { namespace Services { namespace Protocols {

enum class SoapMessageStage
{
    AfterDeserialize = 0x8,
    AfterSerialize = 0x2,
    BeforeDeserialize = 0x4,
    BeforeSerialize = 0x1
};

DECLARE_ENUM_OPERATORS(System::Web::Services::Protocols::SoapMessageStage);
DECLARE_USING_GLOBAL_OPERATORS

}}}} // namespace System::Web::Services::Protocols

DECLARE_USING_ENUM_OPERATORS(System::Web::Services::Protocols);
