﻿/// @file net/web/services/protocol/soap_message.h
#pragma once

#include <net/web/services/protocol/soap_exception.h>
#include <net/web/services/protocol/soap_message_stage.h>
#include <net/web/services/protocol/soap_protocol_version.h>
#include <net/web/services/protocol/soap_header_collection.h>
#include <net/web/services/protocol/soap_header_direction.h>
#include <cstdint>
#include <system/array.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/type_info.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class SoapHttpClientProtocol;
class SoapHeaderCollection;
class SoapHeaderMapping;

class ABSTRACT ASPOSECPP_SHARED_CLASS SoapMessage : public System::Object
{

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::ArrayPtr<System::SharedPtr<Object>> get_InParameters();
    ASPOSECPP_SHARED_API void set_InParameters(System::ArrayPtr<System::SharedPtr<Object>> value);
    ASPOSECPP_SHARED_API System::ArrayPtr<System::SharedPtr<Object>> get_OutParameters();
    ASPOSECPP_SHARED_API void set_OutParameters(System::ArrayPtr<System::SharedPtr<Object>> value);
    ASPOSECPP_SHARED_API virtual String get_Action() = 0;
    ASPOSECPP_SHARED_API String get_ContentType();
    ASPOSECPP_SHARED_API void set_ContentType(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<SoapHeaderCollection> get_Headers();
    ASPOSECPP_SHARED_API SoapMessageStage get_Stage();
    ASPOSECPP_SHARED_API System::SharedPtr<System::IO::Stream> get_Stream();
    ASPOSECPP_SHARED_API void SetStream(System::SharedPtr<System::IO::Stream> stream);
    ASPOSECPP_SHARED_API virtual String get_Url() = 0;
    ASPOSECPP_SHARED_API String get_ContentEncoding();
    ASPOSECPP_SHARED_API void set_ContentEncoding(String value);
    ASPOSECPP_SHARED_API bool get_IsSoap12();
    ASPOSECPP_SHARED_API virtual SoapProtocolVersion get_SoapVersion();
    ASPOSECPP_SHARED_API void set_InternalStream(System::SharedPtr<System::IO::Stream> value);

    ASPOSECPP_SHARED_API SoapMessage();

    ASPOSECPP_SHARED_API void SetStage(SoapMessageStage stage);
    ASPOSECPP_SHARED_API System::SharedPtr<Object> GetInParameterValue(int32_t index);
    ASPOSECPP_SHARED_API System::SharedPtr<Object> GetOutParameterValue(int32_t index);
    ASPOSECPP_SHARED_API System::SharedPtr<Object> GetReturnValue();
    ASPOSECPP_SHARED_API void SetHeaders(System::SharedPtr<SoapHeaderCollection> headers);
    ASPOSECPP_SHARED_API void SetException(SoapException ex);
    ASPOSECPP_SHARED_API SoapException get_Exception();
    ASPOSECPP_SHARED_API void CollectHeaders(System::SharedPtr<Object> target,
                        System::ArrayPtr<System::SharedPtr<SoapHeaderMapping>> headers, SoapHeaderDirection direction);
    ASPOSECPP_SHARED_API void UpdateHeaderValues(System::SharedPtr<Object> target,
                            System::ArrayPtr<System::SharedPtr<SoapHeaderMapping>> headersInfo);
    ASPOSECPP_SHARED_API System::SharedPtr<SoapHeaderMapping> FindHeader(System::ArrayPtr<System::SharedPtr<SoapHeaderMapping>> headersInfo,
                                                    const TypeInfo& headerType);

protected:
    virtual void EnsureInStage() = 0;
    virtual void EnsureOutStage() = 0;
    void EnsureStage(SoapMessageStage stage);

private:
    String content_type;
    String content_encoding;
    SoapException exception = nullptr;
    System::SharedPtr<SoapHeaderCollection> headers;
    SoapMessageStage stage;
    System::SharedPtr<System::IO::Stream> stream;
    System::ArrayPtr<System::SharedPtr<Object>> inParameters;
    System::ArrayPtr<System::SharedPtr<Object>> outParameters;
    SoapProtocolVersion soapVersion;
};

}}}} // namespace System::Web::Services::Protocols
