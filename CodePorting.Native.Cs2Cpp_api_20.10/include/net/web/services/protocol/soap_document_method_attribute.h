/// @file net/web/services/protocol/soap_document_method_attribute.h
#pragma once
#include <system/attribute.h>

#include <net/web/services/description/soap_format_extensions.h>
#include <net/web/services/protocol/soap_parameter_style.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ASPOSECPP_SHARED_CLASS SoapDocumentMethodAttribute FINAL : public System::Attribute
{
    typedef SoapDocumentMethodAttribute ThisType;
    typedef System::Attribute BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API String get_Action();
    ASPOSECPP_SHARED_API void set_Action(String value);
    ASPOSECPP_SHARED_API String get_Binding();
    ASPOSECPP_SHARED_API void set_Binding(String value);
    ASPOSECPP_SHARED_API bool get_OneWay();
    ASPOSECPP_SHARED_API void set_OneWay(bool value);
    ASPOSECPP_SHARED_API SoapParameterStyle get_ParameterStyle();
    ASPOSECPP_SHARED_API void set_ParameterStyle(SoapParameterStyle value);
    ASPOSECPP_SHARED_API String get_RequestElementName();
    ASPOSECPP_SHARED_API void set_RequestElementName(String value);
    ASPOSECPP_SHARED_API String get_RequestNamespace();
    ASPOSECPP_SHARED_API void set_RequestNamespace(String value);
    ASPOSECPP_SHARED_API String get_ResponseElementName();
    ASPOSECPP_SHARED_API void set_ResponseElementName(String value);
    ASPOSECPP_SHARED_API String get_ResponseNamespace();
    ASPOSECPP_SHARED_API void set_ResponseNamespace(String value);
    ASPOSECPP_SHARED_API Description::SoapBindingUse get_Use();
    ASPOSECPP_SHARED_API void set_Use(Description::SoapBindingUse value);

    ASPOSECPP_SHARED_API SoapDocumentMethodAttribute();
    ASPOSECPP_SHARED_API SoapDocumentMethodAttribute(String action);

private:
    String action;
    String binding;
    bool oneWay;
    SoapParameterStyle parameterStyle;
    String requestElementName;
    String requestNamespace;
    String responseElementName;
    String responseNamespace;
    Description::SoapBindingUse use;
};

}}}} // namespace System::Web::Services::Protocols
