/// @file net/web/services/protocol/soap_header_attribute.h
#pragma once
#include <system/attribute.h>
#include <system/enum_helpers.h>
#include <system/string.h>

#include <net/web/services/protocol/soap_header_direction.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ASPOSECPP_SHARED_CLASS SoapHeaderAttribute FINAL : public System::Attribute
{
    typedef SoapHeaderAttribute ThisType;
    typedef System::Attribute BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API SoapHeaderDirection get_Direction();
    ASPOSECPP_SHARED_API void set_Direction(SoapHeaderDirection value);
    ASPOSECPP_SHARED_API String get_MemberName();
    ASPOSECPP_SHARED_API void set_MemberName(String value);
    ASPOSECPP_SHARED_API bool get_Required();
    ASPOSECPP_SHARED_API void set_Required(bool value);

    ASPOSECPP_SHARED_API SoapHeaderAttribute(String memberName);

private:
    SoapHeaderDirection direction;
    String memberName;
    bool required;
};

}}}} // namespace System::Web::Services::Protocols
