﻿/// @file net/web/services/protocol/soap_document_service_attribute.h
#pragma once
#include <system/attribute.h>

#include <net/web/services/description/soap_format_extensions.h>
#include <net/web/services/protocol/soap_parameter_style.h>
#include <net/web/services/protocol/soap_service_routing_style.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class SoapDocumentServiceAttribute FINAL : public System::Attribute
{
    typedef SoapDocumentServiceAttribute ThisType;
    typedef System::Attribute BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    SoapParameterStyle get_ParameterStyle();
    void set_ParameterStyle(SoapParameterStyle value);
    SoapServiceRoutingStyle get_RoutingStyle();
    void set_RoutingStyle(SoapServiceRoutingStyle value);
    Description::SoapBindingUse get_Use();
    void set_Use(Description::SoapBindingUse value);

    SoapDocumentServiceAttribute();
    SoapDocumentServiceAttribute(Description::SoapBindingUse use);
    SoapDocumentServiceAttribute(Description::SoapBindingUse use, SoapParameterStyle paramStyle);

private:
    SoapParameterStyle paramStyle;
    SoapServiceRoutingStyle routingStyle;
    Description::SoapBindingUse use;
};

}}}} // namespace System::Web::Services::Protocols
