﻿/// @file net/web/services/protocol/soap_http_client_protocol.h
#pragma once
#include <system/iasyncresult.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>
#include <xml/xml_writer.h>
#include <cstdint>
#include <net/web_request.h>
#include <system/array.h>
#include <system/async_callback.h>
#include <system/collections/dictionary.h>
#include <net/web/services/protocol/http_web_client_protocol.h>
#include <net/web/services/protocol/soap_protocol_version.h>
#include <system/threading/send_or_post_callback.h>
#include <xml/xml_reader.h>
#include <xml/xml_serializer_implementation.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class SoapTypeStubInfo;
class SoapClientMessage;
class ASPOSECPP_SHARED_CLASS SoapHttpClientProtocol : public System::Web::Services::Protocols::HttpWebClientProtocol
{
public:
    SoapProtocolVersion get_SoapVersion();
    void set_SoapVersion(SoapProtocolVersion value);

    ASPOSECPP_SHARED_API SoapHttpClientProtocol();

    void Discover();
    ASPOSECPP_SHARED_API void
        InitializeSerializers(const System::TypeInfo& serviceType,
                              System::SharedPtr<System::Xml::Serialization::XmlSerializerImplementation> impl,
                              String name /*, String ns*/);

protected:
    ASPOSECPP_SHARED_API System::SharedPtr<IAsyncResult>
        BeginInvoke(String methodName, System::ArrayPtr<System::SharedPtr<Object>> parameters, AsyncCallback callback,
                    System::SharedPtr<Object> asyncState);
    ASPOSECPP_SHARED_API System::ArrayPtr<System::SharedPtr<Object>>
        EndInvoke(System::SharedPtr<IAsyncResult> asyncResult);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Net::WebRequest> GetWebRequest(System::SharedPtr<System::Uri> uri);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Xml::XmlReader>
        GetReaderForMessage(System::SharedPtr<SoapClientMessage> message, int32_t bufferSize);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Xml::XmlWriter>
        GetWriterForMessage(System::SharedPtr<SoapClientMessage> message, int32_t bufferSize);
    ASPOSECPP_SHARED_API System::ArrayPtr<System::SharedPtr<Object>>
        Invoke(String method_name, System::ArrayPtr<System::SharedPtr<Object>> parameters);
    ASPOSECPP_SHARED_API void InvokeAsync(String methodName, System::ArrayPtr<System::SharedPtr<Object>> parameters,
                                          Threading::SendOrPostCallback callback);
    ASPOSECPP_SHARED_API void InvokeAsync(String methodName, System::ArrayPtr<System::SharedPtr<Object>> parameters,
                                          Threading::SendOrPostCallback callback, System::SharedPtr<Object> userState);

private:
    static System::SharedPtr<
        System::Collections::Generic::Dictionary<System::String, System::SharedPtr<SoapTypeStubInfo>>>
        soapTypeInfos;
    System::SharedPtr<SoapTypeStubInfo> soapTypeInfo;
    SoapProtocolVersion soapVersion;

    void AsyncGetRequestStreamDone(System::SharedPtr<IAsyncResult> ar);
    void AsyncGetResponseDone(System::SharedPtr<IAsyncResult> ar);
    System::SharedPtr<Net::WebRequest> GetRequestForMessage(System::SharedPtr<System::Uri> uri,
                                                            System::SharedPtr<SoapClientMessage> message);
    void SendRequest(System::SharedPtr<IO::Stream> s, System::SharedPtr<SoapClientMessage> message);
    System::ArrayPtr<System::SharedPtr<Object>> ReceiveResponse(System::SharedPtr<Net::WebResponse> response,
                                                                System::SharedPtr<SoapClientMessage> message);
    void InvokeAsyncCallback(System::SharedPtr<IAsyncResult> ar);
};

}}}} // namespace System::Web::Services::Protocols
