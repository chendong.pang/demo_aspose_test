/// @file net/web/services/protocol/soap_header_direction.h
#pragma once
#include <system/enum_helpers.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

enum class SoapHeaderDirection
{
    In = 0x1,
    InOut = 0x3,
    Out = 0x2,
    Fault = 0x4
};

DECLARE_ENUM_OPERATORS(System::Web::Services::Protocols::SoapHeaderDirection);
DECLARE_USING_GLOBAL_OPERATORS

}}}} // namespace System::Web::Services::Protocols

DECLARE_USING_ENUM_OPERATORS(System::Web::Services::Protocols);
