﻿/// @file net/web/services/protocol/http_web_client_protocol.h
#pragma once
#include <security/cryptography/x509_certificates/x509_certificate_collection.h>
#include <system/iasyncresult.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/type_info.h>

#include <net/cookie_container.h>
#include <net/http_web_response.h>
#include <net/iweb_proxy.h>
#include <net/web/services/protocol/web_client_protocol.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ASPOSECPP_SHARED_CLASS ABSTRACT HttpWebClientProtocol : public System::Web::Services::Protocols::WebClientProtocol
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API bool get_AllowAutoRedirect();
    ASPOSECPP_SHARED_API void set_AllowAutoRedirect(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Security::Cryptography::X509Certificates::X509CertificateCollection>
        get_ClientCertificates();
    ASPOSECPP_SHARED_API System::SharedPtr<System::Net::CookieContainer> get_CookieContainer();
    ASPOSECPP_SHARED_API void set_CookieContainer(System::SharedPtr<System::Net::CookieContainer> value);
    ASPOSECPP_SHARED_API bool get_EnableDecompression();
    ASPOSECPP_SHARED_API void set_EnableDecompression(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Net::IWebProxy> get_Proxy();
    ASPOSECPP_SHARED_API void set_Proxy(System::SharedPtr<Net::IWebProxy> value);
    ASPOSECPP_SHARED_API String get_UserAgent();
    ASPOSECPP_SHARED_API void set_UserAgent(String value);
    ASPOSECPP_SHARED_API bool get_UnsafeAuthenticatedConnectionSharing();
    ASPOSECPP_SHARED_API void set_UnsafeAuthenticatedConnectionSharing(bool value);

    ASPOSECPP_SHARED_API virtual void CheckForCookies(System::SharedPtr<Net::HttpWebResponse> response);
    void UnregisterMapping(System::SharedPtr<Object> userState);

protected:
    ASPOSECPP_SHARED_API HttpWebClientProtocol();

    ASPOSECPP_SHARED_API virtual System::SharedPtr<Net::WebRequest> GetWebRequest(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Net::WebResponse>
        GetWebResponse(System::SharedPtr<Net::WebRequest> request);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Net::WebResponse>
        GetWebResponse(System::SharedPtr<Net::WebRequest> request, System::SharedPtr<IAsyncResult> result);
    ASPOSECPP_SHARED_API void CancelAsync(System::SharedPtr<Object> userState);

private:
    bool allowAutoRedirect, enableDecompression;
    System::SharedPtr<Security::Cryptography::X509Certificates::X509CertificateCollection> clientCertificates;
    System::SharedPtr<System::Net::CookieContainer> cookieContainer;
    System::SharedPtr<Net::IWebProxy> proxy;
    String userAgent;
    bool _unsafeAuthenticated;
    //    System::SharedPtr<Collections::Hashtable> mappings;
};

}}}} // namespace System::Web::Services::Protocols
