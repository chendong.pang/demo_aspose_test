﻿/// @file net/web/services/protocol/soap_protocol_version.h
#pragma once
namespace System { namespace Web { namespace Services { namespace Protocols {

enum class SoapProtocolVersion
{
    Default,
    Soap11,
    Soap12
};
}}}} // namespace System::Web::Services::Protocols
