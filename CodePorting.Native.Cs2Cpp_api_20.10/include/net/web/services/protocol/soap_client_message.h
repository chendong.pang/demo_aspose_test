﻿/// @file net/web/services/protocol/soap_client_message.h
#pragma once
#include <system/object.h>
#include <system/shared_ptr.h>

#include "soap_message.h"

namespace System { namespace Web { namespace Services { namespace Protocols {

class SoapHttpClientProtocol;
class SoapClientMethod;
class SoapMethodStubInfo;

class ASPOSECPP_SHARED_CLASS SoapClientMessage FINAL : public System::Web::Services::Protocols::SoapMessage
{
    typedef SoapClientMessage ThisType;
    typedef System::Web::Services::Protocols::SoapMessage BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    System::SharedPtr<SoapMethodStubInfo> MethodStubInfo;
    System::ArrayPtr<System::SharedPtr<Object>> Parameters;

    ASPOSECPP_SHARED_API virtual String get_Action() override;
    ASPOSECPP_SHARED_API System::SharedPtr<SoapHttpClientProtocol> get_Client();
    ASPOSECPP_SHARED_API virtual bool get_OneWay();
    ASPOSECPP_SHARED_API virtual String get_Url() override;

    ASPOSECPP_SHARED_API virtual SoapProtocolVersion get_SoapVersion() override;
    ASPOSECPP_SHARED_API SoapClientMessage(System::SharedPtr<SoapHttpClientProtocol> client, System::SharedPtr<SoapMethodStubInfo> msi,
                      String url, System::ArrayPtr<System::SharedPtr<Object>> parameters);

protected:
    virtual void EnsureInStage() override;
    virtual void EnsureOutStage() override;

private:
    System::SharedPtr<SoapHttpClientProtocol> client;
    String url;
};

}}}} // namespace System::Web::Services::Protocols
