﻿/// @file net/web/services/protocol/web_client_protocol.h
#pragma once
#include <system/iasyncresult.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/text/encoding.h>
#include <system/type_info.h>
#include <system/uri.h>
#include <cstdint>
#include <net/web_request.h>

namespace System { namespace Web { namespace Services { namespace Protocols {

class ASPOSECPP_SHARED_CLASS ABSTRACT WebClientProtocol : public System::Object
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    System::SharedPtr<Uri> uri;
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_Uri();
    ASPOSECPP_SHARED_API void set_Uri(System::SharedPtr<Uri> uri);

    ASPOSECPP_SHARED_API String get_ConnectionGroupName();
    ASPOSECPP_SHARED_API void set_ConnectionGroupName(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Net::ICredentials> get_Credentials();
    ASPOSECPP_SHARED_API void set_Credentials(System::SharedPtr<Net::ICredentials> value);
    ASPOSECPP_SHARED_API bool get_PreAuthenticate();
    ASPOSECPP_SHARED_API void set_PreAuthenticate(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Text::Encoding> get_RequestEncoding();
    ASPOSECPP_SHARED_API void set_RequestEncoding(System::SharedPtr<Text::Encoding> value);
    ASPOSECPP_SHARED_API int32_t get_Timeout();
    ASPOSECPP_SHARED_API void set_Timeout(int32_t value);
    ASPOSECPP_SHARED_API String get_Url();
    ASPOSECPP_SHARED_API void set_Url(String value);
    ASPOSECPP_SHARED_API bool get_UseDefaultCredentials();
    ASPOSECPP_SHARED_API void set_UseDefaultCredentials(bool value);
    ASPOSECPP_SHARED_API virtual void Abort();

protected:
    WebClientProtocol();

    static void AddToCache(const TypeInfo& type, System::SharedPtr<Object> value);
    static System::SharedPtr<Object> GetFromCache(const TypeInfo& type);
    virtual System::SharedPtr<Net::WebRequest> GetWebRequest(System::SharedPtr<Uri> uri);
    virtual System::SharedPtr<Net::WebResponse> GetWebResponse(System::SharedPtr<Net::WebRequest> request);
    virtual System::SharedPtr<Net::WebResponse> GetWebResponse(System::SharedPtr<Net::WebRequest> request,
                                                               System::SharedPtr<IAsyncResult> result);

private:
    String connectionGroupName;
    System::SharedPtr<Net::ICredentials> credentials;
    bool preAuthenticate;
    System::SharedPtr<Text::Encoding> requestEncoding;
    int32_t timeout;
    System::SharedPtr<Net::WebRequest> current_request;
    //    static System::SharedPtr<Collections::Specialized::HybridDictionary> cache;

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}}}} // namespace System::Web::Services::Protocols
