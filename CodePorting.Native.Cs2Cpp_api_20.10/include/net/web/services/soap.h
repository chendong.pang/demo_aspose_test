﻿/// @file net/web/services/soap.h
#pragma once
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Web { namespace Services {

class Soap
{
public:
    class Attribute
    {
    public:
        static const String MustUnderstand;
        static const String Actor;
        static const String EncodingStyle;
        static const String Lang;
        static const String ConformsTo;

    private:
        Attribute();
    };

    class Element
    {
    public:
        static const String Envelope;
        static const String Header;
        static const String Body;
        static const String Fault;
        static const String FaultActor;
        static const String FaultCode;
        static const String FaultDetail;
        static const String FaultString;
        static const String StackTrace;
        static const String Message;
        static const String Claim;

    private:
        Element();
    };

    class Code
    {
    public:
        static const String Server;
        static const String VersionMismatch;
        static const String MustUnderstand;
        static const String Client;

    private:
        Code();
    };

public:
    static const String XmlNamespace;
    static const String Encoding;
    static const String Namespace;
    static const String ConformanceClaim;
    static const String BasicProfile1_1;
    static const String Action;
    static const String ArrayType;
    static const String Prefix;
    static const String ClaimPrefix;
    static const String DimeContentType;
    static const String SoapContentType;

private:
    Soap();
};

class Soap12 FINAL
{
public:
    class Attribute
    {
    public:
        static const String UpgradeEnvelopeQname;
        static const String Role;
        static const String Relay;

    private:
        Attribute();
    };

    class Element FINAL
    {
    public:
        static const String Upgrade;
        static const String UpgradeEnvelope;
        static const String FaultRole;
        static const String FaultReason;
        static const String FaultReasonText;
        static const String FaultCode;
        static const String FaultNode;
        static const String FaultCodeValue;
        static const String FaultSubcode;
        static const String FaultDetail;

    private:
        Element();
    };

    class Code FINAL
    {
    public:
        static const String VersionMismatch;
        static const String MustUnderstand;
        static const String DataEncodingUnknown;
        static const String Sender;
        static const String Receiver;
        static const String RpcProcedureNotPresentSubcode;
        static const String RpcBadArgumentsSubcode;
        static const String EncodingMissingIDFaultSubcode;
        static const String EncodingUntypedValueFaultSubcode;

    private:
        Code();
    };

public:
    static const String Namespace;
    static const String Encoding;
    static const String RpcNamespace;
    static const String Prefix;

private:
    Soap12();
};

}}} // namespace System::Web::Services
