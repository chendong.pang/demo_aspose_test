﻿/// @file net/web/services/description/soap_format_extensions.h
#pragma once
#include <system/array.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <xml/schema/xml_schema.h>
#include <xml/xml_qualified_name.h>

namespace System { namespace Web { namespace Services { namespace Description {

enum class SoapBindingStyle
{
    Default,
    Document,
    Rpc
};

enum class SoapBindingUse
{
    Default,
    Encoded,
    Literal
};

}}}} // namespace System::Web::Services::Description
