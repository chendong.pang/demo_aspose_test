/// @file net/web/services/web_service_attribute.h
#pragma once
#include <system/attribute.h>
#include <system/string.h>

namespace System { namespace Web { namespace Services {

class WebServiceAttribute FINAL : public System::Attribute
{
    typedef WebServiceAttribute ThisType;
    typedef System::Attribute BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static const String DefaultNamespace;

    String get_Description();
    void set_Description(String value);
    String get_Name();
    void set_Name(String value);
    String get_Namespace();
    void set_Namespace(String value);

    WebServiceAttribute();

private:
    String description;
    String name;
    String ns;
};
}}} // namespace System::Web::Services
