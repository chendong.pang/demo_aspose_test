﻿/// @file net/web/services/web_service_binding_attribute.h
#pragma once
#include <system/attribute.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Web { namespace Services {

class WsiProfiles;
class ASPOSECPP_SHARED_CLASS WebServiceBindingAttribute FINAL : public System::Attribute
{
    typedef WebServiceBindingAttribute ThisType;
    typedef System::Attribute BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API String get_Location();
    ASPOSECPP_SHARED_API void set_Location(String value);
    ASPOSECPP_SHARED_API String get_Name();
    ASPOSECPP_SHARED_API void set_Name(String value);
    ASPOSECPP_SHARED_API String get_Namespace();
    ASPOSECPP_SHARED_API void set_Namespace(String value);
    ASPOSECPP_SHARED_API bool get_EmitConformanceClaims();
    ASPOSECPP_SHARED_API void set_EmitConformanceClaims(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<WsiProfiles> get_ConformsTo();
    ASPOSECPP_SHARED_API void set_ConformsTo(System::SharedPtr<WsiProfiles> value);

    ASPOSECPP_SHARED_API WebServiceBindingAttribute();
    ASPOSECPP_SHARED_API WebServiceBindingAttribute(String name);
    ASPOSECPP_SHARED_API WebServiceBindingAttribute(String name, String ns);
    ASPOSECPP_SHARED_API WebServiceBindingAttribute(String name, String ns, String location);

private:
    String location;
    String name;
    String ns;
    bool emitConformanceClaims;
    System::SharedPtr<WsiProfiles> conformsTo;
};

}}} // namespace System::Web::Services
