﻿/// @file net/http_web_response.h
#pragma once

#include <system/collections/ienumerable.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/uri.h>

#include <net/cookie_collection.h>
#include <net/cookie_container.h>
#include <net/http/http_response_message.h>
#include <net/http_status_code.h>
#include <net/web_header_collection.h>
#include <net/web_response.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS HttpWebResponse : public System::Net::WebResponse
{
    typedef HttpWebResponse ThisType;
    typedef System::Net::WebResponse BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API virtual int64_t get_ContentLength() override;
    ASPOSECPP_SHARED_API virtual String get_ContentType() override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<CookieCollection> get_Cookies();
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebHeaderCollection> get_Headers() override;
    ASPOSECPP_SHARED_API virtual String get_Method();
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Uri> get_ResponseUri() override;
    ASPOSECPP_SHARED_API virtual HttpStatusCode get_StatusCode();
    ASPOSECPP_SHARED_API virtual String get_StatusDescription();
    ASPOSECPP_SHARED_API virtual bool get_SupportsHeaders() override;
    ASPOSECPP_SHARED_API String GetResponseHeader(String headerName);

    ASPOSECPP_SHARED_API HttpWebResponse(System::SharedPtr<Http::HttpResponseMessage> _message,
                                         System::SharedPtr<Uri> requestUri,
                                         System::SharedPtr<CookieContainer> cookieContainer);

    ASPOSECPP_SHARED_API virtual System::SharedPtr<IO::Stream> GetResponseStream() override;
    ASPOSECPP_SHARED_API virtual void Close() override;

    /// Not implemented.
    ASPOSECPP_SHARED_API System::String get_CharacterSet()
    {
        throw NotImplementedException(ASPOSE_CURRENT_FUNCTION);
    }

protected:
    virtual void Dispose(bool disposing) override;
    System::Object::shared_members_type GetSharedMembers() override;

private:
    System::SharedPtr<Http::HttpResponseMessage> _httpResponseMessage;
    System::SharedPtr<Uri> _requestUri;
    System::SharedPtr<CookieCollection> _cookies;
    System::SharedPtr<WebHeaderCollection> _webHeaderCollection;

    void CheckDisposed();
    String GetHeaderValueAsString(System::SharedPtr<Collections::Generic::IEnumerable<String>> values);
};

}} // namespace System::Net
