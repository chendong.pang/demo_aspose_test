/// @file net/cache/request_cache_policy.h
#pragma once

#include <system/date_time.h>
#include <system/enum.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/timespan.h>

namespace System { namespace Net { namespace Cache {

// The enum describes cache settings applicable for any webrequest
enum class RequestCacheLevel
{
    Default = 0,
    BypassCache = 1,
    CacheOnly = 2,
    CacheIfAvailable = 3,
    Revalidate = 4,
    Reload = 5,
    NoCacheNoStore = 6
};

// Common request cache policy used for caching of Http, FTP, etc.
class ASPOSECPP_SHARED_CLASS RequestCachePolicy : public System::Object
{
    typedef RequestCachePolicy ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API RequestCacheLevel get_Level();

    ASPOSECPP_SHARED_API RequestCachePolicy();
    ASPOSECPP_SHARED_API RequestCachePolicy(RequestCacheLevel level);

    ASPOSECPP_SHARED_API virtual String ToString() const override;

private:
    RequestCacheLevel m_Level;
};

// The enum describes cache settings for http
enum class HttpRequestCacheLevel
{
    Default = 0,
    BypassCache = 1,
    CacheOnly = 2,
    CacheIfAvailable = 3,
    Revalidate = 4,
    Reload = 5,
    NoCacheNoStore = 6,
    CacheOrNextCacheOnly = 7,
    Refresh = 8
};

// CacheAgeControl is used to specify preferences with respect of cached item age and freshness.
enum class HttpCacheAgeControl
{
    None = 0x0,
    MinFresh = 0x1,
    MaxAge = 0x2,
    MaxStale = 0x4,
    MaxAgeAndMinFresh = 0x3,
    MaxAgeAndMaxStale = 0x6
};

// HTTP cache policy that expresses RFC2616 HTTP caching semantic
class HttpRequestCachePolicy : public System::Net::Cache::RequestCachePolicy
{
    typedef HttpRequestCachePolicy ThisType;
    typedef System::Net::Cache::RequestCachePolicy BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API HttpRequestCacheLevel get_Level() const;
    ASPOSECPP_SHARED_API DateTime get_CacheSyncDate() const;
    ASPOSECPP_SHARED_API DateTime get_InternalCacheSyncDateUtc() const;
    ASPOSECPP_SHARED_API TimeSpan get_MaxAge() const;
    ASPOSECPP_SHARED_API TimeSpan get_MinFresh() const;
    ASPOSECPP_SHARED_API TimeSpan get_MaxStale() const;

    ASPOSECPP_SHARED_API HttpRequestCachePolicy();
    ASPOSECPP_SHARED_API HttpRequestCachePolicy(HttpRequestCacheLevel level);
    ASPOSECPP_SHARED_API HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan ageOrFreshOrStale);
    ASPOSECPP_SHARED_API HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan maxAge, TimeSpan freshOrStale);
    ASPOSECPP_SHARED_API HttpRequestCachePolicy(DateTime cacheSyncDate);
    ASPOSECPP_SHARED_API HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan maxAge, TimeSpan freshOrStale,
                           DateTime cacheSyncDate);

    ASPOSECPP_SHARED_API virtual String ToString() const override;

private:
    static System::SharedPtr<HttpRequestCachePolicy> BypassCache;
    HttpRequestCacheLevel m_Level;
    DateTime m_LastSyncDateUtc;
    TimeSpan m_MaxAge;
    TimeSpan m_MinFresh;
    TimeSpan m_MaxStale;

    static RequestCacheLevel MapLevel(HttpRequestCacheLevel level);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}}} // namespace System::Net::Cache

template <>
struct EnumMetaInfo<System::Net::Cache::RequestCacheLevel>
{
    static const std::array<std::pair<System::Net::Cache::RequestCacheLevel, const char_t*>, 7>& values();
};
template <>
struct EnumMetaInfo<System::Net::Cache::HttpRequestCacheLevel>
{
    static const std::array<std::pair<System::Net::Cache::HttpRequestCacheLevel, const char_t*>, 9>& values();
};
