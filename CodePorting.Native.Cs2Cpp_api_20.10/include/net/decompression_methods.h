﻿/// @file net/decompression_methods.h
#pragma once
#include <system/enum_helpers.h>

namespace System { namespace Net {

enum class DecompressionMethods
{
    None = 0,
    GZip = 1,
    Deflate = 2
};

DECLARE_ENUM_OPERATORS(System::Net::DecompressionMethods);
DECLARE_USING_GLOBAL_OPERATORS

}} // namespace System::Net

DECLARE_USING_ENUM_OPERATORS(System::Net);
