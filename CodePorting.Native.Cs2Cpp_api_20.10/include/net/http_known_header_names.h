﻿/// @file net/http_known_header_names.h
#pragma once

#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS HttpKnownHeaderNames
{
public:
    static ASPOSECPP_SHARED_API const String Accept;
    static ASPOSECPP_SHARED_API const String AcceptCharset;
    static ASPOSECPP_SHARED_API const String AcceptEncoding;
    static ASPOSECPP_SHARED_API const String AcceptLanguage;
    static ASPOSECPP_SHARED_API const String AcceptRanges;
    static ASPOSECPP_SHARED_API const String Age;
    static ASPOSECPP_SHARED_API const String Allow;
    static ASPOSECPP_SHARED_API const String Authorization;
    static ASPOSECPP_SHARED_API const String CacheControl;
    static ASPOSECPP_SHARED_API const String Connection;
    static ASPOSECPP_SHARED_API const String ContentDisposition;
    static ASPOSECPP_SHARED_API const String ContentEncoding;
    static ASPOSECPP_SHARED_API const String ContentLanguage;
    static ASPOSECPP_SHARED_API const String ContentLength;
    static ASPOSECPP_SHARED_API const String ContentLocation;
    static ASPOSECPP_SHARED_API const String ContentMD5;
    static ASPOSECPP_SHARED_API const String ContentRange;
    static ASPOSECPP_SHARED_API const String ContentType;
    static ASPOSECPP_SHARED_API const String Cookie;
    static ASPOSECPP_SHARED_API const String Cookie2;
    static ASPOSECPP_SHARED_API const String Date;
    static ASPOSECPP_SHARED_API const String ETag;
    static ASPOSECPP_SHARED_API const String Expect;
    static ASPOSECPP_SHARED_API const String Expires;
    static ASPOSECPP_SHARED_API const String From;
    static ASPOSECPP_SHARED_API const String Host;
    static ASPOSECPP_SHARED_API const String IfMatch;
    static ASPOSECPP_SHARED_API const String IfModifiedSince;
    static ASPOSECPP_SHARED_API const String IfNoneMatch;
    static ASPOSECPP_SHARED_API const String IfRange;
    static ASPOSECPP_SHARED_API const String IfUnmodifiedSince;
    static ASPOSECPP_SHARED_API const String KeepAlive;
    static ASPOSECPP_SHARED_API const String LastModified;
    static ASPOSECPP_SHARED_API const String Location;
    static ASPOSECPP_SHARED_API const String MaxForwards;
    static ASPOSECPP_SHARED_API const String Origin;
    static ASPOSECPP_SHARED_API const String P3P;
    static ASPOSECPP_SHARED_API const String Pragma;
    static ASPOSECPP_SHARED_API const String ProxyAuthenticate;
    static ASPOSECPP_SHARED_API const String ProxyAuthorization;
    static ASPOSECPP_SHARED_API const String ProxyConnection;
    static ASPOSECPP_SHARED_API const String Range;
    static ASPOSECPP_SHARED_API const String Referer;
    static ASPOSECPP_SHARED_API const String RetryAfter;
    static ASPOSECPP_SHARED_API const String SecWebSocketAccept;
    static ASPOSECPP_SHARED_API const String SecWebSocketExtensions;
    static ASPOSECPP_SHARED_API const String SecWebSocketKey;
    static ASPOSECPP_SHARED_API const String SecWebSocketProtocol;
    static ASPOSECPP_SHARED_API const String SecWebSocketVersion;
    static ASPOSECPP_SHARED_API const String Server;
    static ASPOSECPP_SHARED_API const String SetCookie;
    static ASPOSECPP_SHARED_API const String SetCookie2;
    static ASPOSECPP_SHARED_API const String TE;
    static ASPOSECPP_SHARED_API const String Trailer;
    static ASPOSECPP_SHARED_API const String TransferEncoding;
    static ASPOSECPP_SHARED_API const String Upgrade;
    static ASPOSECPP_SHARED_API const String UserAgent;
    static ASPOSECPP_SHARED_API const String Vary;
    static ASPOSECPP_SHARED_API const String Via;
    static ASPOSECPP_SHARED_API const String WWWAuthenticate;
    static ASPOSECPP_SHARED_API const String Warning;
    static ASPOSECPP_SHARED_API const String XAspNetVersion;
    static ASPOSECPP_SHARED_API const String XPoweredBy;
};

}} // namespace System::Net
