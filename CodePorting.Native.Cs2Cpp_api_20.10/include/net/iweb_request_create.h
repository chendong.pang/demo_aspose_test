﻿/// @file net/iweb_request_create.h
#pragma once

#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/uri.h>

namespace System { namespace Net {

class WebRequest;
class IWebRequestCreate : public System::Object
{
    typedef IWebRequestCreate ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    virtual System::SharedPtr<WebRequest> Create(System::SharedPtr<Uri> uri) = 0;
};

}} // namespace System::Net
