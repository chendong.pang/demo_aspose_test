﻿/// @file net/cookie.h
#pragma once
#include <cstdint>
#include <system/array.h>
#include <system/collections/icomparer.h>
#include <system/date_time.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>

namespace System { namespace Net {

enum class CookieVariant
{
    Unknown,
    Plain,
    Rfc2109,
    Rfc2965,
    Default = static_cast<int32_t>(Rfc2109)
};

class ASPOSECPP_SHARED_CLASS Cookie FINAL : public System::Object
{
    typedef Cookie ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API static const int32_t MaxSupportedVersion;
    ASPOSECPP_SHARED_API static const String MaxSupportedVersionString;
    ASPOSECPP_SHARED_API static const String CommentAttributeName;
    ASPOSECPP_SHARED_API static const String CommentUrlAttributeName;
    ASPOSECPP_SHARED_API static const String DiscardAttributeName;
    ASPOSECPP_SHARED_API static const String DomainAttributeName;
    ASPOSECPP_SHARED_API static const String ExpiresAttributeName;
    ASPOSECPP_SHARED_API static const String MaxAgeAttributeName;
    ASPOSECPP_SHARED_API static const String PathAttributeName;
    ASPOSECPP_SHARED_API static const String PortAttributeName;
    ASPOSECPP_SHARED_API static const String SecureAttributeName;
    ASPOSECPP_SHARED_API static const String VersionAttributeName;
    ASPOSECPP_SHARED_API static const String HttpOnlyAttributeName;
    ASPOSECPP_SHARED_API static const String SeparatorLiteral;
    ASPOSECPP_SHARED_API static const String EqualsLiteral;
    ASPOSECPP_SHARED_API static const String QuotesLiteral;
    ASPOSECPP_SHARED_API static const String SpecialAttributeLiteral;
    ASPOSECPP_SHARED_API static System::ArrayPtr<char16_t> PortSplitDelimiters;
    ASPOSECPP_SHARED_API static System::ArrayPtr<char16_t> ReservedToName;
    ASPOSECPP_SHARED_API static System::ArrayPtr<char16_t> ReservedToValue;
    bool IsQuotedVersion;
    bool IsQuotedDomain;

    ASPOSECPP_SHARED_API String get_Comment() const;
    ASPOSECPP_SHARED_API void set_Comment(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_CommentUri() const;
    ASPOSECPP_SHARED_API void set_CommentUri(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API bool get_HttpOnly() const;
    ASPOSECPP_SHARED_API void set_HttpOnly(bool value);
    ASPOSECPP_SHARED_API bool get_Discard() const;
    ASPOSECPP_SHARED_API void set_Discard(bool value);
    ASPOSECPP_SHARED_API String get_Domain() const;
    ASPOSECPP_SHARED_API void set_Domain(String value);
    ASPOSECPP_SHARED_API bool get_DomainImplicit();
    ASPOSECPP_SHARED_API void set_DomainImplicit(bool value);
    ASPOSECPP_SHARED_API bool get_Expired();
    ASPOSECPP_SHARED_API void set_Expired(bool value);
    ASPOSECPP_SHARED_API DateTime get_Expires();
    ASPOSECPP_SHARED_API void set_Expires(DateTime value);
    ASPOSECPP_SHARED_API String get_Name() const;
    ASPOSECPP_SHARED_API void set_Name(String value);
    ASPOSECPP_SHARED_API String get_Path() const;
    ASPOSECPP_SHARED_API void set_Path(String value);
    ASPOSECPP_SHARED_API bool get_Plain() const;
    ASPOSECPP_SHARED_API String get_Port() const;
    ASPOSECPP_SHARED_API void set_Port(String value);
    ASPOSECPP_SHARED_API System::ArrayPtr<int32_t> get_PortList() const;
    ASPOSECPP_SHARED_API bool get_Secure() const;
    ASPOSECPP_SHARED_API void set_Secure(bool value);
    ASPOSECPP_SHARED_API DateTime get_TimeStamp() const;
    ASPOSECPP_SHARED_API String get_Value() const;
    ASPOSECPP_SHARED_API void set_Value(String value);
    ASPOSECPP_SHARED_API CookieVariant get_Variant() const;
    ASPOSECPP_SHARED_API void set_Variant(CookieVariant value);
    ASPOSECPP_SHARED_API String get_DomainKey() const;
    ASPOSECPP_SHARED_API int32_t get_Version() const;
    ASPOSECPP_SHARED_API void set_Version(int32_t value);

    ASPOSECPP_SHARED_API Cookie();
    ASPOSECPP_SHARED_API Cookie(String name, String value);
    ASPOSECPP_SHARED_API Cookie(String name, String value, String path);
    ASPOSECPP_SHARED_API Cookie(String name, String value, String path, String domain);

    ASPOSECPP_SHARED_API bool InternalSetName(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Cookie> Clone();
    ASPOSECPP_SHARED_API bool VerifySetDefaults(CookieVariant variant, System::SharedPtr<Uri> uri, bool isLocalDomain, String localDomain,
                           bool setDefault, bool shouldThrow);
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> comparand) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API String ToServerString();

private:
    String _comment;
    System::SharedPtr<Uri> _commentUri;
    CookieVariant _cookieVariant;
    bool _discard;
    String _domain;
    bool _domainImplicit;
    DateTime _expires;
    String _name;
    String _path;
    bool _pathImplicit;
    String _port;
    bool _portImplicit;
    System::ArrayPtr<int32_t> _portList;
    bool _secure;
    bool _httpOnly;
    DateTime _timeStamp;
    String _value;
    int32_t _version;
    String _domainKey;

    String get__Domain() const;
    String get__Path() const;
    String get__Port() const;
    String get__Version() const;

    static bool IsDomainEqualToHost(String domain, String host);
    static bool DomainCharsTest(String name);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

class ASPOSECPP_SHARED_CLASS CookieComparer FINAL : public System::Collections::Generic::IComparer<System::SharedPtr<System::Net::Cookie>>
{
    typedef CookieComparer ThisType;
    typedef System::Collections::Generic::IComparer<System::SharedPtr<System::Net::Cookie>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API static System::SharedPtr<CookieComparer> get_Instance();

    ASPOSECPP_SHARED_API virtual int Compare(args_type left,
        args_type right) const override;

private:
    static System::SharedPtr<CookieComparer> s_instance;

    CookieComparer();
};

class CookieTokenizer;
// CookieParser
// Takes a cookie header, makes cookies.
class ASPOSECPP_SHARED_CLASS CookieParser : public System::Object
{
    typedef CookieParser ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API CookieParser(String cookieString);

    ASPOSECPP_SHARED_API String GetString();
    ASPOSECPP_SHARED_API System::SharedPtr<Cookie> Get();
    ASPOSECPP_SHARED_API System::SharedPtr<Cookie> GetServer();
    ASPOSECPP_SHARED_API static String CheckQuoted(String value);

private:
    System::SharedPtr<CookieTokenizer> _tokenizer;
    System::SharedPtr<Cookie> _savedCookie;
};


}} // namespace System::Net
