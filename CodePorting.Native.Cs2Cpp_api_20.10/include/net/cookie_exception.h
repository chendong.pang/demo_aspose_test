﻿/// @file net/cookie_exception.h
#pragma once

#include <system/exceptions.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net {

class Details_CookieException;
using CookieException = System::ExceptionWrapper<Details_CookieException>;


class ASPOSECPP_SHARED_CLASS Details_CookieException : public System::Details_FormatException
{
public:
    ASPOSECPP_SHARED_API virtual ~Details_CookieException();

    ASPOSECPP_SHARED_API Details_CookieException();
    ASPOSECPP_SHARED_API Details_CookieException(std::nullptr_t);
    ASPOSECPP_SHARED_API Details_CookieException(String message);
    ASPOSECPP_SHARED_API Details_CookieException(String message, Exception inner);
};

}} // namespace System::Net
