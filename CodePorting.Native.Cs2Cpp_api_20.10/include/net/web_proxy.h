/// @file net/web_proxy.h
#pragma once
#include <cstdint>
#include <system/array.h>
#include <system/collections/list.h>
#include <system/runtime/serialization/serialization_info.h>
#include <system/runtime/serialization/streaming_context.h>
#include <system/string.h>
#include <system/text/regularexpressions/regex.h>

#include <net/ip_address.h>
#include <net/iweb_proxy.h>

namespace System { namespace Net {

class WebProxy : public System::Net::IWebProxy
{
public:
    System::SharedPtr<Uri> get_Address();
    void set_Address(System::SharedPtr<Uri> value);
    bool get_BypassProxyOnLocal();
    void set_BypassProxyOnLocal(bool value);
    System::ArrayPtr<String> get_BypassList();
    void set_BypassList(System::ArrayPtr<String> value);
    System::SharedPtr<ICredentials> get_Credentials();
    void set_Credentials(System::SharedPtr<ICredentials> value);
    bool get_UseDefaultCredentials();
    void set_UseDefaultCredentials(bool value);

    WebProxy();
    WebProxy(System::SharedPtr<Uri> Address);
    WebProxy(System::SharedPtr<Uri> Address, bool BypassOnLocal);
    WebProxy(System::SharedPtr<Uri> Address, bool BypassOnLocal, System::ArrayPtr<String> BypassList);
    WebProxy(System::SharedPtr<Uri> Address, bool BypassOnLocal, System::ArrayPtr<String> BypassList,
             System::SharedPtr<ICredentials> Credentials);
    WebProxy(String Host, int32_t Port);
    WebProxy(String Address);
    WebProxy(String Address, bool BypassOnLocal);
    WebProxy(String Address, bool BypassOnLocal, System::ArrayPtr<String> BypassList);
    WebProxy(String Address, bool BypassOnLocal, System::ArrayPtr<String> BypassList,
             System::SharedPtr<ICredentials> Credentials);

    System::SharedPtr<Uri> GetProxy(System::SharedPtr<Uri> destination);
    bool IsBypassed(System::SharedPtr<Uri> host);
    static System::SharedPtr<WebProxy> GetDefaultProxy();

private:
    System::SharedPtr<Collections::Generic::List<String>> _bypassList;
    System::ArrayPtr<System::SharedPtr<Text::RegularExpressions::Regex>> _regExBypassList;
    System::SharedPtr<Uri> pr_Address;
    bool pr_BypassProxyOnLocal;
    System::SharedPtr<ICredentials> pr_Credentials;

    static System::SharedPtr<Uri> CreateProxyUri(String address);
    void UpdateRegExList(bool canThrow);
    bool IsMatchInBypassList(System::SharedPtr<Uri> input);
    bool IsLocal(System::SharedPtr<Uri> host);
    static bool IsAddressLocal(System::SharedPtr<IPAddress> ipAddress);
};

}} // namespace System::Net
