﻿/// @file net/web_request.h
#pragma once

#include <system/async_callback.h>
#include <system/collections/list.h>
#include <system/iasyncresult.h>
#include <system/io/stream.h>
#include <system/string.h>

#include <net/cache/request_cache_policy.h>
#include <net/iweb_proxy.h>
#include <net/iweb_request_create.h>
#include <net/web_header_collection.h>

namespace System { namespace Net { namespace {
class RequestCachePolicy;
}}} // namespace System::Net::

namespace System { namespace Net {

class HttpWebRequest;
class FileWebRequest;
class WebRequest;
class WebResponse;
class ASPOSECPP_SHARED_CLASS ABSTRACT WebRequest : public virtual System::Object
{
    FRIEND_FUNCTION_System_MakeObject;

public:
    class WebRequestPrefixElement : public System::Object
    {
    public:
        String Prefix;
        System::SharedPtr<IWebRequestCreate> Creator;

        WebRequestPrefixElement(String P, System::SharedPtr<IWebRequestCreate> C);
    };

    class HttpRequestCreator : public System::Net::IWebRequestCreate
    {
    public:
        System::SharedPtr<WebRequest> Create(System::SharedPtr<Uri> Uri);
    };

public:
    static System::SharedPtr<Collections::Generic::List<System::SharedPtr<WebRequest::WebRequestPrefixElement>>>
        get_PrefixList();
    static void set_PrefixList(
        System::SharedPtr<Collections::Generic::List<System::SharedPtr<WebRequest::WebRequestPrefixElement>>> value);
    virtual String get_Method() = 0;
    virtual void set_Method(String value) = 0;
    virtual System::SharedPtr<Uri> get_RequestUri() = 0;
    virtual System::SharedPtr<WebHeaderCollection> get_Headers() = 0;
    virtual void set_Headers(System::SharedPtr<WebHeaderCollection> value) = 0;
    virtual String get_ContentType() = 0;
    virtual void set_ContentType(String value) = 0;
    virtual System::SharedPtr<ICredentials> get_Credentials();
    virtual void set_Credentials(System::SharedPtr<ICredentials> value);
    virtual bool get_UseDefaultCredentials();
    virtual void set_UseDefaultCredentials(bool value);
    static System::SharedPtr<IWebProxy> get_DefaultWebProxy();
    static void set_DefaultWebProxy(System::SharedPtr<IWebProxy> value);
    virtual System::SharedPtr<IWebProxy> get_Proxy();
    virtual void set_Proxy(System::SharedPtr<IWebProxy> value);

    ASPOSECPP_SHARED_API static System::SharedPtr<WebRequest> Create(String requestUriString);
    ASPOSECPP_SHARED_API static System::SharedPtr<WebRequest> Create(System::SharedPtr<Uri> requestUri);
    static System::SharedPtr<WebRequest> CreateDefault(System::SharedPtr<Uri> requestUri);
    static System::SharedPtr<HttpWebRequest> CreateHttp(String requestUriString);
    static System::SharedPtr<HttpWebRequest> CreateHttp(System::SharedPtr<Uri> requestUri);
    static bool RegisterPrefix(String prefix, System::SharedPtr<IWebRequestCreate> creator);
    virtual System::SharedPtr<IAsyncResult> BeginGetResponse(AsyncCallback callback,
                                                             System::SharedPtr<Object> state) = 0;
    virtual System::SharedPtr<WebResponse> EndGetResponse(System::SharedPtr<IAsyncResult> asyncResult) = 0;
    virtual System::SharedPtr<IAsyncResult> BeginGetRequestStream(AsyncCallback callback,
                                                                  System::SharedPtr<Object> state) = 0;
    virtual System::SharedPtr<IO::Stream> EndGetRequestStream(System::SharedPtr<IAsyncResult> asyncResult) = 0;
    virtual void Abort() = 0;

    ASPOSECPP_SHARED_API virtual int64_t get_ContentLength();
    ASPOSECPP_SHARED_API virtual void set_ContentLength(int64_t value);

    ASPOSECPP_SHARED_API virtual System::SharedPtr<System::Net::Cache::RequestCachePolicy> get_CachePolicy();
    ASPOSECPP_SHARED_API virtual void set_CachePolicy(System::SharedPtr<System::Net::Cache::RequestCachePolicy> value);

    ASPOSECPP_SHARED_API virtual System::String get_ConnectionGroupName();
    ASPOSECPP_SHARED_API virtual void set_ConnectionGroupName(System::String value);

    ASPOSECPP_SHARED_API virtual bool get_PreAuthenticate();
    ASPOSECPP_SHARED_API virtual void set_PreAuthenticate(bool value);

    ASPOSECPP_SHARED_API virtual int32_t get_Timeout();
    ASPOSECPP_SHARED_API virtual void set_Timeout(int32_t timeout);

    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebResponse> GetResponse();
    ASPOSECPP_SHARED_API virtual System::SharedPtr<System::IO::Stream> GetRequestStream();

protected:
    ASPOSECPP_SHARED_API WebRequest();

private:
    static System::SharedPtr<Collections::Generic::List<System::SharedPtr<WebRequest::WebRequestPrefixElement>>>
        s_prefixList;
    static System::SharedPtr<Object> s_internalSyncObject;
    static System::SharedPtr<IWebProxy> s_DefaultWebProxy;
    static bool s_DefaultWebProxyInitialized;

    static System::SharedPtr<WebRequest> Create(System::SharedPtr<Uri> requestUri, bool useUriBase);
};

}} // namespace System::Net
