/// @file net/web_client.h
#pragma once

#include <system/uri.h>
#include <system/component_model/component.h>
#include <system/text/encoding.h>
#include <system/collections/specialized/name_value_collection.h>
#include <mutex>

namespace System { namespace Net {

class WebRequest;
class WebResponse;

/// WebClient provides common methods for sending and receiving data.
/// Objects of this class should only be allocated using System::MakeObject() function.
/// Never create instance of this type on stack or using operator new, as it will result in runtime errors and/or assertion faults.
/// Always wrap this class into System::SmartPtr pointer and use this pointer to pass it to functions as argument.
class ASPOSECPP_SHARED_CLASS WebClient : public ComponentModel::Component
{
    typedef WebClient ThisType;
    typedef ComponentModel::Component BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API WebClient();
    ASPOSECPP_SHARED_API ~WebClient();

    /// Gets encoding used to download or upload strings.
    ASPOSECPP_SHARED_API SharedPtr<Text::Encoding> get_Encoding() const;

    /// Sets encoding used to download or upload strings.
    /// @param encoding Encoding used to download or upload strings.
    ASPOSECPP_SHARED_API void set_Encoding(const SharedPtr<Text::Encoding>& encoding);

    /// NOT IMPLEMENTED.
    ASPOSECPP_SHARED_API void set_UseDefaultCredentials(bool);

    /// Downloads specified resource as a string.
    /// @param address URI to download.
    /// @return String containing the requested resource.
    ASPOSECPP_SHARED_API String DownloadString(const String& address) const;

    /// Downloads specified resource as a string.
    /// @param address URI to download.
    /// @return String containing the requested resource.
    ASPOSECPP_SHARED_API String DownloadString(const SharedPtr<Uri>& address) const;

    /// Downloads specified resource as a byte-array.
    /// @param address URI to download.
    /// @return Byte-array containing the requested resource.
    ASPOSECPP_SHARED_API ByteArrayPtr DownloadData(const String& address) const;

    /// Downloads specified resource as a byte-array.
    /// @param address URI to download.
    /// @return Byte-array containing the requested resource.
    ASPOSECPP_SHARED_API ByteArrayPtr DownloadData(const SharedPtr<Uri>& address) const;

private:
    SharedPtr<Text::Encoding> m_encoding;
    SharedPtr<Uri> m_base_address;
    SharedPtr<Collections::Specialized::NameValueCollection> m_query_parameters;
    mutable std::mutex m_mutex;

    Object::shared_members_type GetSharedMembers() override;

    /// Convert @p address to absolute URI.
    SharedPtr<Uri> GetAbsoluteUri(const String& address) const;

    /// Convert @p address to absolute URI.
    SharedPtr<Uri> GetAbsoluteUri(const SharedPtr<Uri>& address) const;

    /// Build string from m_query_parameters.
    String BuildQuery() const;

    /// Convert response data to string.
    String DecodeData(const SharedPtr<WebResponse>& response, const ByteArrayPtr& data) const;

    /// Download and decode data from the specified resource.
    String DownloadStringInternal(const SharedPtr<Uri>& address) const;

    /// Download data from the specified resource.
    ByteArrayPtr DownloadDataInternal(const SharedPtr<Uri>& address) const;

    /// Creates web-request to download data from the specified address.
    SharedPtr<WebRequest> CreateWebRequest(const SharedPtr<Uri>& address) const;
};

}} // namespace System::Net
