﻿/// @file net/uri_scheme.h
#pragma once

#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net {

class UriScheme : public System::Object
{
    typedef UriScheme ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static const String Http;
    static const String Https;
    static const String Ws;
    static const String Wss;
    static const String SchemeDelimiter;
};

}} // namespace System::Net
