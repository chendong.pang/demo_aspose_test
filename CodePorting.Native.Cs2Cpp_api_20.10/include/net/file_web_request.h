﻿/// @file net/file_web_request.h
#pragma once

#include <cstdint>
#include <system/async_callback.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>
#include <net/cookie_container.h>
#include <net/http/http_response_message.h>
#include <net/iweb_proxy.h>
#include <net/service_point.h>
#include <net/web_request.h>
#include "security/cryptography/x509_certificates/x509_certificate_collection.h"

namespace System { namespace Net {

class RequestStream;

class ASPOSECPP_SHARED_CLASS FileWebRequest : public System::Net::WebRequest
{
    typedef FileWebRequest ThisType;
    typedef System::Net::WebRequest BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API void set_Timeout(int timeout) override;

    ASPOSECPP_SHARED_API virtual String get_ContentType() override;
    ASPOSECPP_SHARED_API virtual void set_ContentType(String value) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebHeaderCollection> get_Headers() override;
    ASPOSECPP_SHARED_API virtual void set_Headers(System::SharedPtr<WebHeaderCollection> value) override;
    ASPOSECPP_SHARED_API virtual String get_Method() override;
    ASPOSECPP_SHARED_API virtual void set_Method(String value) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Uri> get_RequestUri() override;

    ASPOSECPP_SHARED_API FileWebRequest(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebResponse> GetResponse() override;
    ASPOSECPP_SHARED_API virtual void Abort() override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IAsyncResult> BeginGetRequestStream(AsyncCallback callback,
                                                                                       System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IO::Stream>
        EndGetRequestStream(System::SharedPtr<IAsyncResult> asyncResult) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IAsyncResult> BeginGetResponse(AsyncCallback callback,
                                                                                  System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebResponse>
        EndGetResponse(System::SharedPtr<IAsyncResult> asyncResult) override;

protected:
    System::Object::shared_members_type GetSharedMembers() override;

private:
    System::SharedPtr<Uri> _requestUri;
    int32_t _timeout;
};

}} // namespace System::Net
