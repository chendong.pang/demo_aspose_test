﻿/// @file net/sockets/network_stream.h
#pragma once

#include <cstdint>
#include <system/array.h>
#include <system/async_callback.h>
#include <system/iasyncresult.h>
#include <system/io/file_access.h>
#include <system/io/seekorigin.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/sockets/socket.h>

namespace System { namespace Net { namespace Sockets {

// Provides the underlying stream of data for network access.
class ASPOSECPP_SHARED_CLASS NetworkStream : public System::IO::Stream
{
    typedef NetworkStream ThisType;
    typedef System::IO::Stream BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    virtual ASPOSECPP_SHARED_API bool get_CanRead() const override;
    virtual ASPOSECPP_SHARED_API bool get_CanSeek() const override;
    virtual ASPOSECPP_SHARED_API bool get_CanWrite() const override;
    virtual ASPOSECPP_SHARED_API bool get_CanTimeout() const override;
    virtual ASPOSECPP_SHARED_API int32_t get_ReadTimeout() const override;
    virtual ASPOSECPP_SHARED_API void set_ReadTimeout(int32_t value) override;
    virtual ASPOSECPP_SHARED_API int32_t get_WriteTimeout() const override;
    virtual ASPOSECPP_SHARED_API void set_WriteTimeout(int32_t value) override;
    virtual ASPOSECPP_SHARED_API bool get_DataAvailable() const;
    virtual ASPOSECPP_SHARED_API int64_t get_Length() const override;
    virtual ASPOSECPP_SHARED_API int64_t get_Position() const override;
    virtual ASPOSECPP_SHARED_API void set_Position(int64_t value) override;

    ASPOSECPP_SHARED_API NetworkStream(System::SharedPtr<System::Net::Sockets::Socket> socket);
    ASPOSECPP_SHARED_API NetworkStream(System::SharedPtr<System::Net::Sockets::Socket> socket,
                                       System::IO::FileAccess access, bool ownsSocket);
    ASPOSECPP_SHARED_API NetworkStream(System::SharedPtr<System::Net::Sockets::Socket> socket, bool ownsSocket);

    virtual ASPOSECPP_SHARED_API int64_t Seek(int64_t offset, IO::SeekOrigin origin) override;
    virtual ASPOSECPP_SHARED_API int32_t Read(System::ArrayPtr<uint8_t> buffer, int32_t offset, int32_t size) override;
    virtual ASPOSECPP_SHARED_API void Write(System::ArrayPtr<uint8_t> buffer, int32_t offset, int32_t size) override;

    ASPOSECPP_SHARED_API void Close(int timeout);

    virtual ASPOSECPP_SHARED_API ~NetworkStream();

    ASPOSECPP_SHARED_API System::SharedPtr<IAsyncResult> BeginRead(System::ArrayPtr<uint8_t> buffer, int32_t offset,
                                                                   int32_t size, AsyncCallback callback,
                                                                   System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API int32_t EndRead(System::SharedPtr<IAsyncResult> asyncResult) override;
    ASPOSECPP_SHARED_API System::SharedPtr<IAsyncResult> BeginWrite(System::ArrayPtr<uint8_t> buffer, int32_t offset,
                                                                    int32_t size, AsyncCallback callback,
                                                                    System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API void EndWrite(System::SharedPtr<IAsyncResult> asyncResult) override;
    virtual ASPOSECPP_SHARED_API void Flush() override;
    virtual ASPOSECPP_SHARED_API void SetLength(int64_t value) override;

private:
    void InitNetworkStream(System::SharedPtr<System::Net::Sockets::Socket> socket, System::IO::FileAccess Access);

    System::SharedPtr<System::Net::Sockets::Socket> m_StreamSocket;

    bool m_Readable;

    bool m_Writeable;
    bool m_OwnsSocket;
};
}}} // namespace System::Net::Sockets
