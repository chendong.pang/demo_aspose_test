﻿/// @file net/http/http_client_handler.h
#pragma once
#include <system/idisposable.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/cookie_container.h>
#include <net/http/http_message_handler.h>
#include <net/iweb_proxy.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpClientHandler : public HttpMessageHandler
{
    typedef HttpClientHandler ThisType;
    typedef System::Net::Http::HttpMessageHandler BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;

    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API HttpClientHandler();
    ASPOSECPP_SHARED_API void Dispose() override;
    ASPOSECPP_SHARED_API System::SharedPtr<HttpResponseMessage> Send(System::SharedPtr<HttpRequestMessage> request) override;
    ASPOSECPP_SHARED_API System::SharedPtr<ICredentials> get_Credentials();
    ASPOSECPP_SHARED_API void set_Credentials(System::SharedPtr<ICredentials> value);
    ASPOSECPP_SHARED_API System::SharedPtr<System::Net::CookieContainer> get_CookieContainer();
    ASPOSECPP_SHARED_API void set_CookieContainer(System::SharedPtr<System::Net::CookieContainer> value);
    ASPOSECPP_SHARED_API void set_UseCookies(bool value);
    ASPOSECPP_SHARED_API void set_Proxy(System::SharedPtr<IWebProxy> value);
    ASPOSECPP_SHARED_API void set_UseProxy(bool value);

protected:
    System::SharedPtr<System::Net::CookieContainer> _cookieContainer;
    System::SharedPtr<ICredentials> _credentials;
    System::SharedPtr<IWebProxy> _proxy;

    void Dispose(bool disposing) override;
};

}}} // namespace System::Net::Http
