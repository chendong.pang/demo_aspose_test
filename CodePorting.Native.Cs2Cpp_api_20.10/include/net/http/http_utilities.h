﻿/// @file net/http/http_utilities.h
#pragma once

#include <system/action.h>
#include <system/diagnostics/debug.h>
#include <system/enum_helpers.h>
#include <system/exceptions.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/uri.h>
#include <system/version.h>

namespace System { namespace Net { namespace Http {

class HttpUtilities : public System::Object
{
    typedef HttpUtilities ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static Version DefaultRequestVersion;
    static Version DefaultResponseVersion;

    static bool IsHttpUri(System::SharedPtr<Uri> uri);
};

}}} // namespace System::Net::Http
