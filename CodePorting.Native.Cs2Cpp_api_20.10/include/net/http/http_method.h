﻿/// @file net/http/http_method.h
#pragma once

#include <cstdint>
#include <system/iequatable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpMethod : public System::IEquatable<System::SharedPtr<System::Net::Http::HttpMethod>>
{
    typedef HttpMethod ThisType;
    typedef System::IEquatable<System::SharedPtr<System::Net::Http::HttpMethod>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    friend bool operator==(System::SharedPtr<HttpMethod> left, System::SharedPtr<HttpMethod> right);
    friend bool operator!=(System::SharedPtr<HttpMethod> left, System::SharedPtr<HttpMethod> right);

public:
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Get();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Put();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Post();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Delete();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Head();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Options();
    ASPOSECPP_SHARED_API static System::SharedPtr<HttpMethod> get_Trace();
    ASPOSECPP_SHARED_API String get_Method();

    ASPOSECPP_SHARED_API HttpMethod(String method);

    ASPOSECPP_SHARED_API bool Equals(System::SharedPtr<HttpMethod> other) override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API virtual String ToString() const override;

private:
    String _method;
    mutable int32_t _hashcode;
    static System::SharedPtr<HttpMethod> s_getMethod;
    static System::SharedPtr<HttpMethod> s_putMethod;
    static System::SharedPtr<HttpMethod> s_postMethod;
    static System::SharedPtr<HttpMethod> s_deleteMethod;
    static System::SharedPtr<HttpMethod> s_headMethod;
    static System::SharedPtr<HttpMethod> s_optionsMethod;
    static System::SharedPtr<HttpMethod> s_traceMethod;

    static bool IsUpperAscii(String value);
};

bool operator==(System::SharedPtr<HttpMethod> left, System::SharedPtr<HttpMethod> right);
bool operator!=(System::SharedPtr<HttpMethod> left, System::SharedPtr<HttpMethod> right);

}}} // namespace System::Net::Http
