﻿/// @file net/http/headers/range_header_value.h
#pragma once

#include <system/collections/icollection.h>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

#include <net/http/headers/object_collection.h>
#include <net/http/headers/range_item_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS RangeHeaderValue : public System::ICloneable
{
    typedef RangeHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Unit();
    ASPOSECPP_SHARED_API void set_Unit(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<RangeItemHeaderValue>>>
        get_Ranges();

    ASPOSECPP_SHARED_API RangeHeaderValue();
    ASPOSECPP_SHARED_API RangeHeaderValue(Nullable<int64_t> from, Nullable<int64_t> to);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<RangeHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<RangeHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetRangeLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    String _unit;
    System::SharedPtr<ObjectCollection<System::SharedPtr<RangeItemHeaderValue>>> _ranges;

    RangeHeaderValue(System::SharedPtr<RangeHeaderValue> source);

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
