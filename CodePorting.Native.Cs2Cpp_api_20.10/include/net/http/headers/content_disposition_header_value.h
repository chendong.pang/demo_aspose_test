﻿/// @file net/http/headers/content_disposition_header_value.h
#pragma once

#include "system/date_time_offset.h"
#include <cstdint>
#include <system/collections/icollection.h>
#include <system/date_time_offset.h>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

#include <net/http/headers/name_value_header_value.h>
#include <net/http/headers/object_collection.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS ContentDispositionHeaderValue : public System::ICloneable
{
    typedef ContentDispositionHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_DispositionType();
    ASPOSECPP_SHARED_API void set_DispositionType(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<NameValueHeaderValue>>> get_Parameters();
    ASPOSECPP_SHARED_API String get_Name();
    ASPOSECPP_SHARED_API void set_Name(String value);
    ASPOSECPP_SHARED_API String get_FileName();
    ASPOSECPP_SHARED_API void set_FileName(String value);
    ASPOSECPP_SHARED_API String get_FileNameStar();
    ASPOSECPP_SHARED_API void set_FileNameStar(String value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_CreationDate();
    ASPOSECPP_SHARED_API void set_CreationDate(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_ModificationDate();
    ASPOSECPP_SHARED_API void set_ModificationDate(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_ReadDate();
    ASPOSECPP_SHARED_API void set_ReadDate(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API Nullable<int64_t> get_Size();
    ASPOSECPP_SHARED_API void set_Size(Nullable<int64_t> value);

    ASPOSECPP_SHARED_API ContentDispositionHeaderValue();
    ASPOSECPP_SHARED_API ContentDispositionHeaderValue(String dispositionType);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<ContentDispositionHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<ContentDispositionHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetDispositionTypeLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

protected:
    ASPOSECPP_SHARED_API ContentDispositionHeaderValue(System::SharedPtr<ContentDispositionHeaderValue> source);

private:
    static const String fileName;
    static const String name;
    static const String fileNameStar;
    static const String creationDate;
    static const String modificationDate;
    static const String readDate;
    static const String size;
    System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> _parameters;
    String _dispositionType;

    System::SharedPtr<Object> Clone() override;
    static int32_t GetDispositionTypeExpressionLength(String input, int32_t startIndex, String& dispositionType);
    static void CheckDispositionTypeFormat(String dispositionType, String parameterName);
    Nullable<DateTimeOffset> GetDate(String parameter);
    void SetDate(String parameter, Nullable<DateTimeOffset> date);
    String GetName(String parameter);
    void SetName(String parameter, String value);
    String EncodeAndQuoteMime(String input);
    bool IsQuoted(String value);
    bool RequiresEncoding(String input);
    String EncodeMime(String input);
    bool TryDecodeMime(String input, String& output);
    String Encode5987(String input);
    bool TryDecode5987(String input, String& output);
};

}}}} // namespace System::Net::Http::Headers
