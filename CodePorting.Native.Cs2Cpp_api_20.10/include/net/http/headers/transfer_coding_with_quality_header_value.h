﻿/// @file net/http/headers/transfer_coding_with_quality_header_value.h
#pragma once

#include <system/nullable.h>
#include <net/http/headers/transfer_coding_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS TransferCodingWithQualityHeaderValue FINAL
    : public System::Net::Http::Headers::TransferCodingHeaderValue
{
    typedef TransferCodingWithQualityHeaderValue ThisType;
    typedef System::Net::Http::Headers::TransferCodingHeaderValue BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API Nullable<double> get_Quality();
    ASPOSECPP_SHARED_API void set_Quality(Nullable<double> value);

    ASPOSECPP_SHARED_API TransferCodingWithQualityHeaderValue();
    ASPOSECPP_SHARED_API TransferCodingWithQualityHeaderValue(String value);
    ASPOSECPP_SHARED_API TransferCodingWithQualityHeaderValue(String value, double quality);

    ASPOSECPP_SHARED_API static System::SharedPtr<TransferCodingWithQualityHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<TransferCodingWithQualityHeaderValue>& parsedValue);

private:
    TransferCodingWithQualityHeaderValue(System::SharedPtr<TransferCodingWithQualityHeaderValue> source);

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
