﻿/// @file net/http/headers/product_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS ProductHeaderValue : public System::ICloneable
{
    typedef ProductHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Name();
    ASPOSECPP_SHARED_API String get_Version();

    ASPOSECPP_SHARED_API ProductHeaderValue(String name);
    ASPOSECPP_SHARED_API ProductHeaderValue(String name, String version);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<ProductHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<ProductHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetProductLength(String input, int32_t startIndex,
                                    System::SharedPtr<ProductHeaderValue>& parsedValue);

private:
    String _name;
    String _version;

    ProductHeaderValue(System::SharedPtr<ProductHeaderValue> source);
    ProductHeaderValue();

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
