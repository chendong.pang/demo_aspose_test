﻿/// @file net/http/headers/http_content_headers.h
#pragma once

#include <cstdint>
#include <system/array.h>
#include <system/collections/dictionary.h>
#include <system/collections/hashset.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>

#include <net/http/headers/content_disposition_header_value.h>
#include <net/http/headers/content_range_header_value.h>
//#include <net/http/headers/http_header_parser.h>
#include <net/http/headers/http_header_value_collection.h>
#include <net/http/headers/http_headers.h>
#include <net/http/headers/media_type_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS HttpContentHeaders FINAL : public System::Net::Http::Headers::HttpHeaders
{
    typedef HttpContentHeaders ThisType;
    typedef System::Net::Http::Headers::HttpHeaders BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<String>> get_Allow();
    ASPOSECPP_SHARED_API System::SharedPtr<ContentDispositionHeaderValue> get_ContentDisposition();
    ASPOSECPP_SHARED_API void set_ContentDisposition(System::SharedPtr<ContentDispositionHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<String>> get_ContentEncoding();
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<String>> get_ContentLanguage();
    ASPOSECPP_SHARED_API Nullable<int64_t> get_ContentLength();
    ASPOSECPP_SHARED_API void set_ContentLength(Nullable<int64_t> value);
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_ContentLocation();
    ASPOSECPP_SHARED_API void set_ContentLocation(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API System::ArrayPtr<uint8_t> get_ContentMD5();
    ASPOSECPP_SHARED_API void set_ContentMD5(System::ArrayPtr<uint8_t> value);
    ASPOSECPP_SHARED_API System::SharedPtr<ContentRangeHeaderValue> get_ContentRange();
    ASPOSECPP_SHARED_API void set_ContentRange(System::SharedPtr<ContentRangeHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<MediaTypeHeaderValue> get_ContentType();
    ASPOSECPP_SHARED_API void set_ContentType(System::SharedPtr<MediaTypeHeaderValue> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Expires();
    ASPOSECPP_SHARED_API void set_Expires(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_LastModified();
    ASPOSECPP_SHARED_API void set_LastModified(Nullable<DateTimeOffset> value);

    ASPOSECPP_SHARED_API HttpContentHeaders(HeaderFunc<Nullable<int64_t>> calculateLengthFunc);

    ASPOSECPP_SHARED_API static void AddKnownHeaders(System::SharedPtr<Collections::Generic::HashSet<String>> headerSet);

private:
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        s_parserStore;
    static System::SharedPtr<Collections::Generic::HashSet<String>> s_invalidHeaders;
    HeaderFunc<Nullable<int64_t>> _calculateLengthFunc;
    bool _contentLengthSet;
    System::SharedPtr<HttpHeaderValueCollection<String>> _allow;
    System::SharedPtr<HttpHeaderValueCollection<String>> _contentEncoding;
    System::SharedPtr<HttpHeaderValueCollection<String>> _contentLanguage;

    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        CreateParserStore();
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        GetParserStore();
    static System::SharedPtr<Collections::Generic::HashSet<String>> CreateInvalidHeaders();
    static System::SharedPtr<Collections::Generic::HashSet<String>> GetInvalidHeaders();

    void InitStatic();
};

}}}} // namespace System::Net::Http::Headers
