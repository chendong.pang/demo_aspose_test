﻿/// @file net/http/headers/range_item_header_value.h
#pragma once

#include <cstdint>
#include <system/collections/icollection.h>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS RangeItemHeaderValue : public System::ICloneable
{
    typedef RangeItemHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API Nullable<int64_t> get_From();
    ASPOSECPP_SHARED_API Nullable<int64_t> get_To();

    ASPOSECPP_SHARED_API RangeItemHeaderValue(Nullable<int64_t> from, Nullable<int64_t> to);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static int32_t GetRangeItemListLength(
        String input, int32_t startIndex,
        System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<RangeItemHeaderValue>>> rangeCollection);
    ASPOSECPP_SHARED_API static int32_t GetRangeItemLength(String input, int32_t startIndex,
                                      System::SharedPtr<RangeItemHeaderValue>& parsedValue);

private:
    Nullable<int64_t> _from;
    Nullable<int64_t> _to;

    RangeItemHeaderValue(System::SharedPtr<RangeItemHeaderValue> source);

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
