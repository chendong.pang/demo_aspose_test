﻿/// @file net/http/headers/warning_header_value.h
#pragma once

#include "system/date_time_offset.h"
#include <cstdint>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS WarningHeaderValue : public System::ICloneable
{
    typedef WarningHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API int32_t get_Code();
    ASPOSECPP_SHARED_API String get_Agent();
    ASPOSECPP_SHARED_API String get_Text();
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Date();

    ASPOSECPP_SHARED_API WarningHeaderValue(int32_t code, String agent, String text);
    ASPOSECPP_SHARED_API WarningHeaderValue(int32_t code, String agent, String text, DateTimeOffset date);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<WarningHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<WarningHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetWarningLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    int32_t _code;
    String _agent;
    String _text;
    Nullable<DateTimeOffset> _date;

    WarningHeaderValue();
    WarningHeaderValue(System::SharedPtr<WarningHeaderValue> source);

    static bool TryReadAgent(String input, int32_t startIndex, int32_t& current, String& agent);
    static bool TryReadCode(String input, int32_t& current, int32_t& code);
    static bool TryReadDate(String input, int32_t& current, Nullable<DateTimeOffset>& date);
    System::SharedPtr<Object> Clone() override;
    static void CheckCode(int32_t code);
    static void CheckAgent(String agent);
};

}}}} // namespace System::Net::Http::Headers
