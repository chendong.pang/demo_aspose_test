﻿/// @file net/http/headers/content_range_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS ContentRangeHeaderValue : public System::ICloneable
{
    typedef ContentRangeHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Unit();
    ASPOSECPP_SHARED_API void set_Unit(String value);
    ASPOSECPP_SHARED_API Nullable<int64_t> get_From();
    ASPOSECPP_SHARED_API Nullable<int64_t> get_To();
    ASPOSECPP_SHARED_API Nullable<int64_t> get_Length();
    ASPOSECPP_SHARED_API bool get_HasLength() const;
    ASPOSECPP_SHARED_API bool get_HasRange() const;

    ASPOSECPP_SHARED_API ContentRangeHeaderValue(int64_t from, int64_t to, int64_t length);
    ASPOSECPP_SHARED_API ContentRangeHeaderValue(int64_t length);
    ASPOSECPP_SHARED_API ContentRangeHeaderValue(int64_t from, int64_t to);

    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<ContentRangeHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<ContentRangeHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetContentRangeLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    String _unit;
    Nullable<int64_t> _from;
    Nullable<int64_t> _to;
    Nullable<int64_t> _length;

    ContentRangeHeaderValue();
    ContentRangeHeaderValue(System::SharedPtr<ContentRangeHeaderValue> source);

    static bool TryGetLengthLength(String input, int32_t& current, int32_t& lengthLength);
    static bool TryGetRangeLength(String input, int32_t& current, int32_t& fromLength, int32_t& toStartIndex,
                                  int32_t& toLength);
    static bool TryCreateContentRange(String input, String unit, int32_t fromStartIndex, int32_t fromLength,
                                      int32_t toStartIndex, int32_t toLength, int32_t lengthStartIndex,
                                      int32_t lengthLength, System::SharedPtr<Object>& parsedValue);
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
