﻿/// @file net/http/headers/name_value_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/text/string_builder.h>

#include <net/http/headers/http_headers.h>
#include <net/http/headers/object_collection.h>

namespace System {
class ObjectExt;
}

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS NameValueHeaderValue : public virtual System::ICloneable
{
    typedef NameValueHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;
    friend class System::ObjectExt;

public:
    ASPOSECPP_SHARED_API String get_Name();
    ASPOSECPP_SHARED_API String get_Value();
    ASPOSECPP_SHARED_API void set_Value(String value);

    ASPOSECPP_SHARED_API NameValueHeaderValue();
    ASPOSECPP_SHARED_API NameValueHeaderValue(String name);
    ASPOSECPP_SHARED_API NameValueHeaderValue(String name, String value);

    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API static System::SharedPtr<NameValueHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<NameValueHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API static void ToString(System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> values,
                         char16_t separator, bool leadingSeparator, System::SharedPtr<Text::StringBuilder> destination);
    ASPOSECPP_SHARED_API static String ToString(System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> values,
                           char16_t separator, bool leadingSeparator);
    ASPOSECPP_SHARED_API static int32_t GetHashCode(System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> values);
    ASPOSECPP_SHARED_API static int32_t GetNameValueLength(String input, int32_t startIndex,
                                      System::SharedPtr<NameValueHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetNameValueLength(String input, int32_t startIndex,
                                      HeaderFunc<System::SharedPtr<NameValueHeaderValue>> nameValueCreator,
                                      System::SharedPtr<NameValueHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetNameValueListLength(
        String input, int32_t startIndex, char16_t delimiter,
        System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> nameValueCollection);
    ASPOSECPP_SHARED_API static System::SharedPtr<NameValueHeaderValue>
        Find(System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> values, String name);
    ASPOSECPP_SHARED_API static int32_t GetValueLength(String input, int32_t startIndex);

protected:
    ASPOSECPP_SHARED_API NameValueHeaderValue(System::SharedPtr<NameValueHeaderValue> source);

private:
    static HeaderFunc<System::SharedPtr<NameValueHeaderValue>> s_defaultNameValueCreator;
    String _name;
    String _value;

    static void CheckNameValueFormat(String name, String value);
    static void CheckValueFormat(String value);
    static System::SharedPtr<NameValueHeaderValue> CreateNameValue();
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
