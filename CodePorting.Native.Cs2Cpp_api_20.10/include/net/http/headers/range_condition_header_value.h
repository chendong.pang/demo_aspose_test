﻿/// @file net/http/headers/range_condition_header_value.h
#pragma once

#include <system/date_time_offset.h>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/http/headers/entity_tag_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS RangeConditionHeaderValue : public System::ICloneable
{
    typedef RangeConditionHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Date();
    ASPOSECPP_SHARED_API System::SharedPtr<EntityTagHeaderValue> get_EntityTag();

    ASPOSECPP_SHARED_API RangeConditionHeaderValue(DateTimeOffset date);
    ASPOSECPP_SHARED_API RangeConditionHeaderValue(System::SharedPtr<EntityTagHeaderValue> entityTag);
    ASPOSECPP_SHARED_API RangeConditionHeaderValue(String entityTag);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<RangeConditionHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<RangeConditionHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetRangeConditionLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    Nullable<DateTimeOffset> _date;
    System::SharedPtr<EntityTagHeaderValue> _entityTag;

    RangeConditionHeaderValue(System::SharedPtr<RangeConditionHeaderValue> source);
    RangeConditionHeaderValue();

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
