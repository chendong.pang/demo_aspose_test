﻿/// @file net/http/headers/transfer_coding_header_value.h
#pragma once

#include <system/collections/icollection.h>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/http/headers/name_value_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS TransferCodingHeaderValue : public virtual System::ICloneable
{
    typedef TransferCodingHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Value();
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Parameters();

    ASPOSECPP_SHARED_API TransferCodingHeaderValue();
    ASPOSECPP_SHARED_API TransferCodingHeaderValue(String value);

    ASPOSECPP_SHARED_API static System::SharedPtr<TransferCodingHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<TransferCodingHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t
        GetTransferCodingLength(String input, int32_t startIndex,
                                const HeaderFunc<System::SharedPtr<TransferCodingHeaderValue>>& transferCodingCreator,
                                System::SharedPtr<TransferCodingHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;

protected:
    ASPOSECPP_SHARED_API TransferCodingHeaderValue(System::SharedPtr<TransferCodingHeaderValue> source);

private:
    System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> _parameters;
    String _value;

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
