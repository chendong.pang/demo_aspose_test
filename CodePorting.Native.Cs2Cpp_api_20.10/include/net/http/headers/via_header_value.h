﻿/// @file net/http/headers/via_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS ViaHeaderValue : public System::ICloneable
{
    typedef ViaHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_ProtocolName();
    ASPOSECPP_SHARED_API String get_ProtocolVersion();
    ASPOSECPP_SHARED_API String get_ReceivedBy();
    ASPOSECPP_SHARED_API String get_Comment();

    ASPOSECPP_SHARED_API ViaHeaderValue(String protocolVersion, String receivedBy);
    ASPOSECPP_SHARED_API ViaHeaderValue(String protocolVersion, String receivedBy, String protocolName);
    ASPOSECPP_SHARED_API ViaHeaderValue(String protocolVersion, String receivedBy, String protocolName, String comment);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<ViaHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<ViaHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetViaLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    String _protocolName;
    String _protocolVersion;
    String _receivedBy;
    String _comment;

    ViaHeaderValue();
    ViaHeaderValue(System::SharedPtr<ViaHeaderValue> source);

    static int32_t GetProtocolEndIndex(String input, int32_t startIndex, String& protocolName, String& protocolVersion);
    System::SharedPtr<Object> Clone() override;
    static void CheckReceivedBy(String receivedBy);
};

}}}} // namespace System::Net::Http::Headers
