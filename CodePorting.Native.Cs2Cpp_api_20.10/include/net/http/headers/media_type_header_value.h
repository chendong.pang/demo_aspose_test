﻿/// @file net/http/headers/media_type_header_value.h
#pragma once

#include <cstdint>
#include <system/collections/icollection.h>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

#include <net/http/headers/name_value_header_value.h>
#include <net/http/headers/object_collection.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS MediaTypeHeaderValue : public virtual System::ICloneable
{
    typedef MediaTypeHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_CharSet();
    ASPOSECPP_SHARED_API void set_CharSet(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Parameters();
    ASPOSECPP_SHARED_API String get_MediaType();
    ASPOSECPP_SHARED_API void set_MediaType(String value);

    ASPOSECPP_SHARED_API MediaTypeHeaderValue();
    ASPOSECPP_SHARED_API MediaTypeHeaderValue(String mediaType);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<MediaTypeHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<MediaTypeHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetMediaTypeLength(String input, int32_t startIndex,
                                      HeaderFunc<System::SharedPtr<MediaTypeHeaderValue>> mediaTypeCreator,
                                      System::SharedPtr<MediaTypeHeaderValue>& parsedValue);

protected:
    MediaTypeHeaderValue(System::SharedPtr<MediaTypeHeaderValue> source);

private:
    static const String charSet;
    System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> _parameters;
    String _mediaType;

    static int32_t GetMediaTypeExpressionLength(String input, int32_t startIndex, String& mediaType);
    static void CheckMediaTypeFormat(String mediaType, String parameterName);
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
