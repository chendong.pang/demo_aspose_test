﻿/// @file net/http/headers/media_type_with_quality_header_value.h
#pragma once

#include <system/nullable.h>

#include <net/http/headers/media_type_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS MediaTypeWithQualityHeaderValue FINAL
    : public System::Net::Http::Headers::MediaTypeHeaderValue
{
    typedef MediaTypeWithQualityHeaderValue ThisType;
    typedef System::Net::Http::Headers::MediaTypeHeaderValue BaseType;
    typedef System::ICloneable BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API Nullable<double> get_Quality();
    ASPOSECPP_SHARED_API void set_Quality(Nullable<double> value);

    ASPOSECPP_SHARED_API MediaTypeWithQualityHeaderValue();
    ASPOSECPP_SHARED_API MediaTypeWithQualityHeaderValue(String mediaType);
    ASPOSECPP_SHARED_API MediaTypeWithQualityHeaderValue(String mediaType, double quality);

    ASPOSECPP_SHARED_API static System::SharedPtr<MediaTypeWithQualityHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<MediaTypeWithQualityHeaderValue>& parsedValue);

private:
    MediaTypeWithQualityHeaderValue(System::SharedPtr<MediaTypeWithQualityHeaderValue> source);

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
