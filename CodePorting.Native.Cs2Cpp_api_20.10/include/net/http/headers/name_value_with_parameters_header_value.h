﻿/// @file net/http/headers/name_value_with_parameters_header_value.h
#pragma once

#include <system/collections/icollection.h>

#include <net/http/headers/name_value_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS NameValueWithParametersHeaderValue
    : public System::Net::Http::Headers::NameValueHeaderValue
{
    typedef NameValueWithParametersHeaderValue ThisType;
    typedef System::Net::Http::Headers::NameValueHeaderValue BaseType;
    typedef System::ICloneable BaseType1;

    typedef ::System::BaseTypesInfo<BaseType, BaseType1> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Parameters();

    ASPOSECPP_SHARED_API NameValueWithParametersHeaderValue(String name);
    ASPOSECPP_SHARED_API NameValueWithParametersHeaderValue(String name, String value);
    ASPOSECPP_SHARED_API NameValueWithParametersHeaderValue();

    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;

    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<NameValueWithParametersHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<NameValueWithParametersHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetNameValueWithParametersLength(String input, int32_t startIndex,
                                                    System::SharedPtr<Object>& parsedValue);

protected:
    ASPOSECPP_SHARED_API NameValueWithParametersHeaderValue(System::SharedPtr<NameValueWithParametersHeaderValue> source);

private:
    static HeaderFunc<System::SharedPtr<NameValueHeaderValue>> s_nameValueCreator;
    System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> _parameters;

    static System::SharedPtr<NameValueHeaderValue> CreateNameValue();
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
