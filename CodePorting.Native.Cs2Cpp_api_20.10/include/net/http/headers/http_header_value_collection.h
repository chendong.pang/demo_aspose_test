﻿/// @file net/http/headers/http_header_value_collection.h
#pragma once

#include <system/action.h>
#include <system/collections/icollection.h>
#include <system/constraints.h>
#include <system/details/pointer_collection_helpers.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/special_casts.h>
#include <system/string.h>

#include <net/http/headers/http_headers.h>

namespace System { namespace Net { namespace Http { namespace Headers {

template <typename T>
class HttpHeaderValueCollection FINAL : public System::Collections::Generic::ICollection<T>
{
    assert_is_cs_class(T);

    typedef HttpHeaderValueCollection<T> ThisType;
    typedef System::Collections::Generic::ICollection<T> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_TEMPLATE_CLASS(ThisType, ThisTypeBaseTypesInfo);

    FRIEND_FUNCTION_System_MakeObject;

public:
    int32_t get_Count() const override
    {
        return GetCount();
    }

    bool get_IsReadOnly()
    {
        return false;
    }

    bool get_IsSpecialValueSet()
    {
        // If this collection instance has a "special value", then check whether that value was already set.
        if (_specialValue == nullptr)
        {
            return false;
        }
        return _store->ContainsParsedValue(_headerName, _specialValue);
    }

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store)
        : HttpHeaderValueCollection(headerName, store, nullptr,
                                    static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T>>(nullptr))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store,
                              Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T> validator)
        : HttpHeaderValueCollection(headerName, store, nullptr,
                                    static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T>>(validator))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store, T specialValue)
        : HttpHeaderValueCollection(headerName, store, specialValue,
                                    static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T>>(nullptr))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store, T specialValue,
                              Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T> validator)
        : _specialValue(T())
    {
        _store = store;
        _headerName = headerName;
        _specialValue = specialValue;
        _validator = validator;
    }

    void Add(const T& item) override
    {
        CheckValue(item);
        _store->AddParsedValue(_headerName, item);
    }

    void ParseAdd(String input)
    {
        _store->Add(_headerName, input);
    }

    bool TryParseAdd(String input)
    {
        return _store->TryParseAndAddValue(_headerName, input);
    }

    void Clear() override
    {
        _store->Remove(_headerName);
    }

    bool Contains(const T& item) const override
    {
        CheckValue(item);
        return _store->ContainsParsedValue(_headerName, item);
    }

    void CopyTo(System::ArrayPtr<T> array, int32_t arrayIndex) override
    {
        if (array == nullptr)
        {
            throw ArgumentNullException(u"array");
        }
        // Allow arrayIndex == array.Length in case our own collection is empty
        if ((arrayIndex < 0) || (arrayIndex > array->get_Length()))
        {
            throw ArgumentOutOfRangeException(u"arrayIndex");
        }

        System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

        if (storeValue == nullptr)
        {
            return;
        }

        System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
            System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

        if (storeValues == nullptr)
        {
            // We only have 1 value: If it is the "special value" just return, otherwise add the value to the
            // array and return.
            if (arrayIndex == array->get_Length())
            {
                throw ArgumentException(u"net_http_copyto_array_too_small");
            }
            array[arrayIndex] = nullptr;
        }
        else
        {
            storeValues->CopyTo(System::StaticCastArray<System::SharedPtr<Object>>(array), arrayIndex);
        }
    }

    bool Remove(const T& item) override
    {
        CheckValue(item);
        return _store->RemoveParsedValue(_headerName, item);
    }

    System::SharedPtr<Collections::Generic::IEnumerator<T>> GetEnumerator() override
    {
        auto list = System::MakeObject<Collections::Generic::List<T>>();
        while (true)
        {

            System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

            if (storeValue == nullptr)
            {
                // yield break;
                break;
            }

            System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
                System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

            if (storeValues == nullptr)
            {
                Diagnostics::Debug::Assert(System::ObjectExt::Is<T>(storeValue));
                // T result = storeValue;// System::StaticCast<T>(storeValue);
                T result = System::DynamicCast<typename T::Pointee_>(storeValue);
                list->Add(result);
            }
            else
            {
                // We have multiple values. Iterate through the values and return them.
                auto item_enumerator = (storeValues)->GetEnumerator();
                System::SharedPtr<Object> item;
                while (item_enumerator->MoveNext() && (item = item_enumerator->get_Current(), true))
                {
                    Diagnostics::Debug::Assert(System::ObjectExt::Is<T>(item));
                    // T result = item;// System::StaticCast<T>(item);
                    T result = System::DynamicCast<typename T::Pointee_>(item);
                    // T result = System::StaticCast_noexcept<typename T::Pointee_>(item);
                    // yield return item as T;
                    list->Add(result);
                }
            }
            break;
        }

        return list->GetEnumerator();
    }

    virtual String ToString() const override
    {
        return _store->GetHeaderString(_headerName);
    }

    String GetHeaderStringWithoutSpecial()
    {
        if (!get_IsSpecialValueSet())
        {
            return ToString();
        }
        return _store->GetHeaderString(_headerName, _specialValue);
    }

    void SetSpecialValue()
    {

        if (!_store->ContainsParsedValue(_headerName, _specialValue))
        {
            _store->AddParsedValue(_headerName, _specialValue);
        }
    }

    void RemoveSpecialValue()
    {

        // We're not interested in the return value. It's OK if the "special value" wasn't in the store
        // before calling RemoveParsedValue().
        _store->RemoveParsedValue(_headerName, _specialValue);
    }

    void SetTemplateWeakPtr(unsigned int argument) override
    {
        switch (argument)
        {
        case 0:
            System::Details::CollectionHelpers::SetWeakPointer(_specialValue);
            System::Details::CollectionHelpers::SetWeakPointer(0, _validator);
            break;
        }
    }

private:
    String _headerName;
    System::WeakPtr<HttpHeaders> _store;
    T _specialValue;
    Action<System::SharedPtr<HttpHeaderValueCollection<T>>, T> _validator;

    void CheckValue(const T& item) const
    {
        if (item == nullptr)
        {
            throw ArgumentNullException(u"item");
        }

        // If this instance has a custom validator for validating arguments, call it now.
        if (_validator != nullptr)
        {
            _validator(System::MakeSharedPtr(this), item);
        }
    }

    int32_t GetCount() const
    {
        // This is an O(n) operation.

        System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

        if (storeValue == nullptr)
        {
            return 0;
        }

        System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
            System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

        if (storeValues == nullptr)
        {
            return 1;
        }
        else
        {
            return storeValues->get_Count();
        }
    }
};

// partial specializatio for String type

template <>
class HttpHeaderValueCollection<System::String> : public System::Collections::Generic::ICollection<System::String>
{
    typedef HttpHeaderValueCollection<String> ThisType;
    typedef System::Collections::Generic::ICollection<String> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_TEMPLATE_CLASS(ThisType, ThisTypeBaseTypesInfo);

    FRIEND_FUNCTION_System_MakeObject;

public:
    int32_t get_Count() const override
    {
        return GetCount();
    }

    bool get_IsReadOnly()
    {
        return false;
    }

    bool get_IsSpecialValueSet()
    {
        // If this collection instance has a "special value", then check whether that value was already set.
        if (_specialValue == nullptr)
        {
            return false;
        }
        return _store->ContainsParsedValue(_headerName, _specialValue);
    }

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store)
        : HttpHeaderValueCollection(
              headerName, store, nullptr,
              static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String>>(nullptr))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store,
                              Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String> validator)
        : HttpHeaderValueCollection(
              headerName, store, nullptr,
              static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String>>(validator))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store, String specialValue)
        : HttpHeaderValueCollection(
              headerName, store, specialValue,
              static_cast<Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String>>(nullptr))
    {}

    HttpHeaderValueCollection(String headerName, System::SharedPtr<HttpHeaders> store, String specialValue,
                              Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String> validator)
    //: _specialValue(nullptr)
    {
        _store = store;
        _headerName = headerName;
        _specialValue = ObjectExt::Box(specialValue);
        _validator = validator;
    }

    void Add(const String& item) override
    {
        CheckValue(item);
        _store->AddParsedValue(_headerName, ObjectExt::Box(item));
    }

    void ParseAdd(String input)
    {
        _store->Add(_headerName, input);
    }

    bool TryParseAdd(String input)
    {
        return _store->TryParseAndAddValue(_headerName, input);
    }

    void Clear() override
    {
        _store->Remove(_headerName);
    }

    bool Contains(const String& item) const override
    {
        CheckValue(item);
        return _store->ContainsParsedValue(_headerName, ObjectExt::Box(item));
    }

    void CopyTo(System::ArrayPtr<String> array, int32_t arrayIndex) override
    {
        if (array == nullptr)
        {
            throw ArgumentNullException(u"array");
        }
        // Allow arrayIndex == array.Length in case our own collection is empty
        if ((arrayIndex < 0) || (arrayIndex > array->get_Length()))
        {
            throw ArgumentOutOfRangeException(u"arrayIndex");
        }

        System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

        if (storeValue == nullptr)
        {
            return;
        }

        System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
            System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

        if (storeValues == nullptr)
        {
            // We only have 1 value: If it is the "special value" just return, otherwise add the value to the
            // array and return.
            if (arrayIndex == array->get_Length())
            {
                throw ArgumentException(u"net_http_copyto_array_too_small");
            }
            array[arrayIndex] = nullptr;
        }
        else
        {
            //    storeValues->CopyTo(System::StaticCastArray<System::SharedPtr<Object>>(array), arrayIndex);
        }
    }

    bool Remove(const String& item) override
    {
        CheckValue(item);
        return _store->RemoveParsedValue(_headerName, ObjectExt::Box(item));
    }

    System::SharedPtr<Collections::Generic::IEnumerator<String>> GetEnumerator() override
    {
        auto list = System::MakeObject<Collections::Generic::List<String>>();
        while (true)
        {

            System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

            if (storeValue == nullptr)
            {
                // yield break;
                break;
            }

            System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
                System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

            if (storeValues == nullptr)
            {
                Diagnostics::Debug::Assert(System::ObjectExt::Is<String>(storeValue));
                String result = ObjectExt::Unbox<String>(storeValue);
                // T result = System::StaticCast<T>(storeValue);
                list->Add(result);
            }
            else
            {
                // We have multiple values. Iterate through the values and return them.
                auto item_enumerator = (storeValues)->GetEnumerator();
                decltype(item_enumerator->get_Current()) item;
                while (item_enumerator->MoveNext() && (item = item_enumerator->get_Current(), true))
                {
                    Diagnostics::Debug::Assert(System::ObjectExt::Is<String>(item));
                    // T result = System::StaticCast<T>(item);
                    String result = ObjectExt::Unbox<String>(item);
                    // yield return item as T;
                    list->Add(result);
                }
            }
            break;
        }

        return list->GetEnumerator();
    }

    virtual String ToString() const override
    {
        return _store->GetHeaderString(_headerName);
    }

    String GetHeaderStringWithoutSpecial()
    {
        if (!get_IsSpecialValueSet())
        {
            return ToString();
        }
        return _store->GetHeaderString(_headerName, _specialValue);
    }

    void SetSpecialValue()
    {

        if (!_store->ContainsParsedValue(_headerName, _specialValue))
        {
            _store->AddParsedValue(_headerName, _specialValue);
        }
    }

    void RemoveSpecialValue()
    {

        // We're not interested in the return value. It's OK if the "special value" wasn't in the store
        // before calling RemoveParsedValue().
        _store->RemoveParsedValue(_headerName, _specialValue);
    }

    void SetTemplateWeakPtr(unsigned int argument) override
    {
        switch (argument)
        {
        case 0:
            System::Details::CollectionHelpers::SetWeakPointer(_specialValue);
            System::Details::CollectionHelpers::SetWeakPointer(0, _validator);
            break;
        }
    }

private:
    String _headerName;
    System::WeakPtr<HttpHeaders> _store;
    System::SharedPtr<Object> _specialValue;
    Action<System::SharedPtr<HttpHeaderValueCollection<String>>, String> _validator;

    void CheckValue(String item) const
    {
        if (item == nullptr)
        {
            throw ArgumentNullException(u"item");
        }

        // If this instance has a custom validator for validating arguments, call it now.
        if (_validator != nullptr)
        {
            _validator(System::MakeSharedPtr(this), item);
        }
    }

    int32_t GetCount() const
    {
        // This is an O(n) operation.

        System::SharedPtr<Object> storeValue = _store->GetParsedValues(_headerName);

        if (storeValue == nullptr)
        {
            return 0;
        }

        System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> storeValues =
            System::DynamicCast_noexcept<System::Collections::Generic::List<System::SharedPtr<Object>>>(storeValue);

        if (storeValues == nullptr)
        {
            return 1;
        }
        else
        {
            return storeValues->get_Count();
        }
    }
};

}}}} // namespace System::Net::Http::Headers
