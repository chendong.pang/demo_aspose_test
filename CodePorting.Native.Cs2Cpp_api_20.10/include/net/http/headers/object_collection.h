﻿/// @file net/http/headers/object_collection.h
#pragma once

#include <cstdint>
#include <system/action.h>
#include <system/collections/ilist.h>
#include <system/collections/list.h>
#include <system/collections/object_model/collection.h>
#include <system/details/pointer_collection_helpers.h>
#include <system/exceptions.h>
#include <system/object.h>
#include <system/shared_ptr.h>

namespace System { namespace Net { namespace Http { namespace Headers {

template <typename T>
class ObjectCollection FINAL : public System::Collections::ObjectModel::Collection<T>
{
    typedef ObjectCollection<T> ThisType;
    typedef System::Collections::ObjectModel::Collection<T> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    RTTI_INFO_TEMPLATE_CLASS(ThisType, ThisTypeBaseTypesInfo);

public:
    ObjectCollection() : ObjectCollection(static_cast<Action<T>>(s_defaultValidator))
    {}

    ObjectCollection(Action<T> validator)
        : System::Collections::ObjectModel::Collection<T>(System::MakeObject<Collections::Generic::List<T>>())
    {
        _validator = validator;
    }

    void SetTemplateWeakPtr(unsigned int argument) override
    {
        switch (argument)
        {
        case 0:
            System::Details::CollectionHelpers::SetWeakPointer(0, _validator);
            BaseType::SetTemplateWeakPtr(0);
            break;
        }
    }

protected:
    virtual void InsertItem(int32_t index, T item)
    {
        if (_validator != nullptr)
        {
            _validator(item);
        }
        Collections::ObjectModel::Collection<T>::InsertItem(index, item);
    }

    virtual void SetItem(int32_t index, T item)
    {
        if (_validator != nullptr)
        {
            _validator(item);
        }
        Collections::ObjectModel::Collection<T>::SetItem(index, item);
    }

private:
    static Action<T> s_defaultValidator;
    Action<T> _validator;

    static void CheckNotNull(T item)
    {
        // Null values cannot be added to the collection.
        if (item == nullptr)
        {
            throw ArgumentNullException(u"item");
        }
    }
};

template <typename T>
Action<T> ObjectCollection<T>::s_defaultValidator = CheckNotNull;

}}}} // namespace System::Net::Http::Headers
