﻿/// @file net/http/headers/string_with_quality_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS StringWithQualityHeaderValue : public System::ICloneable
{
    typedef StringWithQualityHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Value();
    ASPOSECPP_SHARED_API Nullable<double> get_Quality();

    ASPOSECPP_SHARED_API StringWithQualityHeaderValue(String value);
    ASPOSECPP_SHARED_API StringWithQualityHeaderValue(String value, double quality);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<StringWithQualityHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<StringWithQualityHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetStringWithQualityLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    String _value;
    Nullable<double> _quality;

    StringWithQualityHeaderValue(System::SharedPtr<StringWithQualityHeaderValue> source);
    StringWithQualityHeaderValue();

    static bool TryReadQuality(String input, System::SharedPtr<StringWithQualityHeaderValue> result, int32_t& index);
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
