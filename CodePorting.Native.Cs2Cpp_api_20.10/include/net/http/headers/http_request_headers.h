﻿/// @file net/http/headers/http_request_headers.h
#pragma once

#include <system/uri.h>

#include <net/http/headers/authentication_header_value.h>
#include <net/http/headers/entity_tag_header_value.h>
#include <net/http/headers/http_headers.h>
#include <net/http/headers/media_type_with_quality_header_value.h>
#include <net/http/headers/name_value_with_parameters_header_value.h>
#include <net/http/headers/product_info_header_value.h>
#include <net/http/headers/range_condition_header_value.h>
#include <net/http/headers/range_header_value.h>
#include <net/http/headers/string_with_quality_header_value.h>
#include <net/http/headers/transfer_coding_with_quality_header_value.h>
#include <net/http/headers/http_header_value_collection.h>
#include <net/http/headers/cache_control_header_value.h>
#include <net/http/headers/via_header_value.h>
#include <net/http/headers/warning_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class HttpGeneralHeaders;

class ASPOSECPP_SHARED_CLASS HttpRequestHeaders FINAL : public System::Net::Http::Headers::HttpHeaders
{
    typedef HttpRequestHeaders ThisType;
    typedef System::Net::Http::Headers::HttpHeaders BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<MediaTypeWithQualityHeaderValue>>> get_Accept();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>>
        get_AcceptCharset();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>>
        get_AcceptEncoding();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>>
        get_AcceptLanguage();
    ASPOSECPP_SHARED_API System::SharedPtr<AuthenticationHeaderValue> get_Authorization();
    ASPOSECPP_SHARED_API void set_Authorization(System::SharedPtr<AuthenticationHeaderValue> value);
    ASPOSECPP_SHARED_API
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<NameValueWithParametersHeaderValue>>> get_Expect();
    ASPOSECPP_SHARED_API Nullable<bool> get_ExpectContinue();
    ASPOSECPP_SHARED_API void set_ExpectContinue(Nullable<bool> value);
    ASPOSECPP_SHARED_API String get_From();
    ASPOSECPP_SHARED_API void set_From(String value);
    ASPOSECPP_SHARED_API String get_Host();
    ASPOSECPP_SHARED_API void set_Host(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<EntityTagHeaderValue>>>
        get_IfMatch();
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_IfModifiedSince();
    ASPOSECPP_SHARED_API void set_IfModifiedSince(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<EntityTagHeaderValue>>>
        get_IfNoneMatch();
    ASPOSECPP_SHARED_API System::SharedPtr<RangeConditionHeaderValue> get_IfRange();
    ASPOSECPP_SHARED_API void set_IfRange(System::SharedPtr<RangeConditionHeaderValue> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_IfUnmodifiedSince();
    ASPOSECPP_SHARED_API void set_IfUnmodifiedSince(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API Nullable<int32_t> get_MaxForwards();
    ASPOSECPP_SHARED_API void set_MaxForwards(Nullable<int32_t> value);
    ASPOSECPP_SHARED_API System::SharedPtr<AuthenticationHeaderValue> get_ProxyAuthorization();
    ASPOSECPP_SHARED_API void set_ProxyAuthorization(System::SharedPtr<AuthenticationHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<RangeHeaderValue> get_Range();
    ASPOSECPP_SHARED_API void set_Range(System::SharedPtr<RangeHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_Referrer();
    ASPOSECPP_SHARED_API void set_Referrer(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<TransferCodingWithQualityHeaderValue>>> get_TE();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductInfoHeaderValue>>>
        get_UserAgent();
    ASPOSECPP_SHARED_API System::SharedPtr<CacheControlHeaderValue> get_CacheControl();
    ASPOSECPP_SHARED_API void set_CacheControl(System::SharedPtr<CacheControlHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_Connection();
    ASPOSECPP_SHARED_API Nullable<bool> get_ConnectionClose();
    ASPOSECPP_SHARED_API void set_ConnectionClose(Nullable<bool> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Date();
    ASPOSECPP_SHARED_API void set_Date(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Pragma();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_Trailer();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<TransferCodingHeaderValue>>>
        get_TransferEncoding();
    ASPOSECPP_SHARED_API Nullable<bool> get_TransferEncodingChunked();
    ASPOSECPP_SHARED_API void set_TransferEncodingChunked(Nullable<bool> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductHeaderValue>>>
        get_Upgrade();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ViaHeaderValue>>> get_Via();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<WarningHeaderValue>>>
        get_Warning();

    ASPOSECPP_SHARED_API HttpRequestHeaders();

    ASPOSECPP_SHARED_API static void
        AddKnownHeaders(System::SharedPtr<Collections::Generic::HashSet<String>> headerSet);
    ASPOSECPP_SHARED_API virtual void AddHeaders(System::SharedPtr<HttpHeaders> sourceHeaders) override;

protected:
    System::Object::shared_members_type GetSharedMembers() override;

private:
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        s_parserStore;
    static System::SharedPtr<Collections::Generic::HashSet<String>> s_invalidHeaders;
    System::SharedPtr<HttpGeneralHeaders> _generalHeaders;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<MediaTypeWithQualityHeaderValue>>> _accept;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<NameValueWithParametersHeaderValue>>> _expect;
    bool _expectContinueSet;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<EntityTagHeaderValue>>> _ifMatch;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<EntityTagHeaderValue>>> _ifNoneMatch;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<TransferCodingWithQualityHeaderValue>>> _te;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductInfoHeaderValue>>> _userAgent;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>> _acceptCharset;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>> _acceptEncoding;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<StringWithQualityHeaderValue>>> _acceptLanguage;

    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<NameValueWithParametersHeaderValue>>>
        get_ExpectCore();

    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        CreateParserStore();
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        GetParserStore();
    static System::SharedPtr<Collections::Generic::HashSet<String>> CreateInvalidHeaders();

    void InitStatic();
};

}}}} // namespace System::Net::Http::Headers
