﻿/// @file net/http/headers/entity_tag_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS EntityTagHeaderValue : public System::ICloneable
{
    typedef EntityTagHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Tag();
    ASPOSECPP_SHARED_API bool get_IsWeak();
    ASPOSECPP_SHARED_API static System::SharedPtr<EntityTagHeaderValue> get_Any();

    ASPOSECPP_SHARED_API EntityTagHeaderValue(String tag);
    ASPOSECPP_SHARED_API EntityTagHeaderValue(String tag, bool isWeak);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<EntityTagHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<EntityTagHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetEntityTagLength(String input, int32_t startIndex,
                                      System::SharedPtr<EntityTagHeaderValue>& parsedValue);

private:
    static System::SharedPtr<EntityTagHeaderValue> s_any;
    String _tag;
    bool _isWeak;

    EntityTagHeaderValue(System::SharedPtr<EntityTagHeaderValue> source);
    EntityTagHeaderValue();

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
