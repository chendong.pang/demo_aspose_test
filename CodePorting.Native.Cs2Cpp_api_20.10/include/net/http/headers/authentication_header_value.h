﻿/// @file net/http/headers/authentication_header_value.h
#pragma once

#include <cstdint>
#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS AuthenticationHeaderValue : public System::ICloneable
{
    typedef AuthenticationHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API String get_Scheme();
    ASPOSECPP_SHARED_API String get_Parameter();

    ASPOSECPP_SHARED_API AuthenticationHeaderValue(String scheme);
    ASPOSECPP_SHARED_API AuthenticationHeaderValue(String scheme, String parameter);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<AuthenticationHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<AuthenticationHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetAuthenticationLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    String _scheme;
    String _parameter;

    AuthenticationHeaderValue(System::SharedPtr<AuthenticationHeaderValue> source);
    AuthenticationHeaderValue();

    static bool TrySkipFirstBlob(String input, int32_t& current, int32_t& parameterEndIndex);
    static bool TryGetParametersEndIndex(String input, int32_t& parseEndIndex, int32_t& parameterEndIndex);
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
