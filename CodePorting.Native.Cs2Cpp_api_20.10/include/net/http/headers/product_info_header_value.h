﻿/// @file net/http/headers/product_info_header_value.h
#pragma once

#include <system/icloneable.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/http/headers/product_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS ProductInfoHeaderValue : public System::ICloneable
{
    typedef ProductInfoHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::SharedPtr<ProductHeaderValue> get_Product();
    ASPOSECPP_SHARED_API String get_Comment();

    ASPOSECPP_SHARED_API ProductInfoHeaderValue(String productName, String productVersion);
    ASPOSECPP_SHARED_API ProductInfoHeaderValue(System::SharedPtr<ProductHeaderValue> product);
    ASPOSECPP_SHARED_API ProductInfoHeaderValue(String comment);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<ProductInfoHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<ProductInfoHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetProductInfoLength(String input, int32_t startIndex,
                                        System::SharedPtr<ProductInfoHeaderValue>& parsedValue);

private:
    System::SharedPtr<ProductHeaderValue> _product;
    String _comment;

    ProductInfoHeaderValue(System::SharedPtr<ProductInfoHeaderValue> source);
    ProductInfoHeaderValue();

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
