﻿/// @file net/http/headers/http_headers.h
#pragma once

#include <system/array.h>
#include <system/collections/dictionary.h>
#include <system/collections/hashset.h>
#include <system/collections/ienumerable.h>
#include <system/collections/ienumerator.h>
#include <system/collections/iequality_comparer.h>
#include <system/collections/keyvalue_pair.h>
#include <system/collections/list.h>
#include <system/collections/sorted_dictionary.h>
#include <system/constraints.h>
#include <system/diagnostics/debug.h>
#include <system/exceptions.h>
#include <system/object.h>
#include <system/object_ext.h>
#include <system/shared_ptr.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http { namespace Headers {

template <typename TResult>
using HeaderFunc = System::MulticastDelegate<TResult()>;

class HttpHeaderParser;

class ASPOSECPP_SHARED_CLASS ABSTRACT HttpHeaders
    : public System::Collections::Generic::IEnumerable<System::Collections::Generic::KeyValuePair<
          System::String, System::SharedPtr<System::Collections::Generic::IEnumerable<System::String>>>>
{
    typedef HttpHeaders ThisType;
    typedef System::Collections::Generic::IEnumerable<System::Collections::Generic::KeyValuePair<
        System::String, System::SharedPtr<System::Collections::Generic::IEnumerable<System::String>>>>
        BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

private:
    enum class StoreLocation
    {
        Raw,
        Invalid,
        Parsed
    };

private:
    class HeaderStoreItemInfo : public System::Object
    {
        typedef HeaderStoreItemInfo ThisType;
        typedef System::Object BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        ASPOSECPP_SHARED_RTTI_INFO_DECL();

    public:
        System::SharedPtr<Object> get_RawValue();
        void set_RawValue(System::SharedPtr<Object> value);
        System::SharedPtr<Object> get_InvalidValue();
        void set_InvalidValue(System::SharedPtr<Object> value);
        System::SharedPtr<Object> get_ParsedValue();
        void set_ParsedValue(System::SharedPtr<Object> value);
        System::SharedPtr<HttpHeaderParser> get_Parser();
        bool get_CanAddValue();
        bool get_IsEmpty();

        HeaderStoreItemInfo(System::SharedPtr<HttpHeaderParser> parser);

    private:
        System::SharedPtr<Object> _rawValue;
        System::SharedPtr<Object> _invalidValue;
        System::SharedPtr<Object> _parsedValue;
        System::SharedPtr<HttpHeaderParser> _parser;
    };

public:
    ASPOSECPP_SHARED_API void Add(String name, System::SharedPtr<Collections::Generic::IEnumerable<String>> values);
    ASPOSECPP_SHARED_API void Add(String name, String value);
    ASPOSECPP_SHARED_API bool TryAddWithoutValidation(String name, String value);
    ASPOSECPP_SHARED_API bool
        TryAddWithoutValidation(String name, System::SharedPtr<Collections::Generic::IEnumerable<String>> values);
    ASPOSECPP_SHARED_API void Clear();
    ASPOSECPP_SHARED_API bool Remove(String name);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::IEnumerable<String>> GetValues(String name);
    ASPOSECPP_SHARED_API bool TryGetValues(String name,
                                           System::SharedPtr<Collections::Generic::IEnumerable<String>>& values);
    ASPOSECPP_SHARED_API bool Contains(String name);
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API
    System::SharedPtr<Collections::Generic::IEnumerable<Collections::Generic::KeyValuePair<String, String>>>
        GetHeaderStrings();
    ASPOSECPP_SHARED_API String GetHeaderString(String headerName);
    ASPOSECPP_SHARED_API String GetHeaderString(String headerName, System::SharedPtr<Object> exclude);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::IEnumerator<
        Collections::Generic::KeyValuePair<String, System::SharedPtr<Collections::Generic::IEnumerable<String>>>>>
        GetEnumerator() override;
    ASPOSECPP_SHARED_API void SetConfiguration(
        System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>> parserStore,
        System::SharedPtr<Collections::Generic::HashSet<String>> invalidHeaders);
    ASPOSECPP_SHARED_API void AddParsedValue(String name, System::SharedPtr<Object> value);
    ASPOSECPP_SHARED_API void SetParsedValue(String name, System::SharedPtr<Object> value);
    ASPOSECPP_SHARED_API void SetOrRemoveParsedValue(String name, System::SharedPtr<Object> value);
    ASPOSECPP_SHARED_API bool RemoveParsedValue(String name, System::SharedPtr<Object> value);
    ASPOSECPP_SHARED_API bool ContainsParsedValue(String name, System::SharedPtr<Object> value);
    ASPOSECPP_SHARED_API virtual void AddHeaders(System::SharedPtr<HttpHeaders> sourceHeaders);
    ASPOSECPP_SHARED_API bool TryParseAndAddValue(String name, String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Object> GetParsedValues(String name);

protected:

    System::Object::shared_members_type GetSharedMembers() override;
    HttpHeaders();

private:
    String ToStringNC();

    System::SharedPtr<
        Collections::Generic::SortedDictionary<String, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo>>>
        _headerStore;
    System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>> _parserStore;
    System::SharedPtr<Collections::Generic::HashSet<String>> _invalidHeaders;

    String GetHeaderString(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info);
    String GetHeaderString(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info, System::SharedPtr<Object> exclude);
    void AddHeaderInfo(String headerName, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> sourceInfo);
    static void CloneAndAddValue(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> destinationInfo,
                                 System::SharedPtr<Object> source);
    static System::SharedPtr<Object> CloneStringHeaderInfoValues(System::SharedPtr<Object> source);
    System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> GetOrCreateHeaderInfo(String name, bool parseRawValues);
    System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> CreateAndAddHeaderToStore(String name);
    void AddHeaderToStore(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info);
    bool TryGetHeaderInfo(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo>& info);
    bool TryGetAndParseHeaderInfo(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo>& info);
    bool ParseRawHeaderValues(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info,
                              bool removeEmptyHeader);
    static void ParseMultipleRawHeaderValues(
        String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info,
        System::SharedPtr<Collections::Generic::List<System::SharedPtr<Object>>> rawValues);
    static void ParseSingleRawHeaderValue(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info);
    static bool TryParseAndAddRawHeaderValue(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info,
                                             String value, bool addWhenInvalid);
    static void AddValue(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info, System::SharedPtr<Object> value,
                         HttpHeaders::StoreLocation location);
    static void AddValueToStoreValue(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info,
                                     System::SharedPtr<Object> value, System::SharedPtr<Object>& currentStoreValue);

    void PrepareHeaderInfoForAdd(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo>& info,
                                 bool& addToStore);
    void ParseAndAddValue(String name, System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info, String value);
    System::SharedPtr<HttpHeaderParser> GetParser(String name);
    void CheckHeaderName(String name);
    bool TryCheckHeaderName(String name);
    static void CheckInvalidNewLine(String value);
    static bool ContainsInvalidNewLine(String value, String name);
    System::ArrayPtr<String> GetValuesAsStrings(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info);
    System::ArrayPtr<String> GetValuesAsStrings(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info,
                                                       System::SharedPtr<Object> exclude);
    static int32_t GetValueCount(System::SharedPtr<HttpHeaders::HeaderStoreItemInfo> info);
    template <typename T>
    static void UpdateValueCount(System::SharedPtr<Object> valueStore, int32_t& valueCount)
    {
        if (valueStore == nullptr)
        {
            return;
        }

        System::SharedPtr<Collections::Generic::List<T>> values =
            System::DynamicCast_noexcept<System::Collections::Generic::List<T>>(valueStore);
        if (values != nullptr)
        {
            valueCount += values->get_Count();
        }
        else
        {
            valueCount++;
        }
    }


    bool AreEqual(System::SharedPtr<Object> value, System::SharedPtr<Object> storeValue,
                  System::SharedPtr<Collections::Generic::IEqualityComparer<System::SharedPtr<Object>>> comparer);
};



}}}} // namespace System::Net::Http::Headers
