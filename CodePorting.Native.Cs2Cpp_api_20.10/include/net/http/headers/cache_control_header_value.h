﻿/// @file net/http/headers/cache_control_header_value.h
#pragma once

#include <cstdint>
#include <system/action.h>
#include <system/collections/icollection.h>
#include <system/collections/list.h>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/text/string_builder.h>
#include <system/timespan.h>

#include <net/http/headers/object_collection.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class NameValueHeaderValue;

class ASPOSECPP_SHARED_CLASS CacheControlHeaderValue : public System::ICloneable
{
    typedef CacheControlHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API bool get_NoCache();
    ASPOSECPP_SHARED_API void set_NoCache(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<String>> get_NoCacheHeaders();
    ASPOSECPP_SHARED_API bool get_NoStore();
    ASPOSECPP_SHARED_API void set_NoStore(bool value);
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_MaxAge();
    ASPOSECPP_SHARED_API void set_MaxAge(Nullable<TimeSpan> value);
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_SharedMaxAge();
    ASPOSECPP_SHARED_API void set_SharedMaxAge(Nullable<TimeSpan> value);
    ASPOSECPP_SHARED_API bool get_MaxStale();
    ASPOSECPP_SHARED_API void set_MaxStale(bool value);
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_MaxStaleLimit();
    ASPOSECPP_SHARED_API void set_MaxStaleLimit(Nullable<TimeSpan> value);
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_MinFresh();
    ASPOSECPP_SHARED_API void set_MinFresh(Nullable<TimeSpan> value);
    ASPOSECPP_SHARED_API bool get_NoTransform();
    ASPOSECPP_SHARED_API void set_NoTransform(bool value);
    ASPOSECPP_SHARED_API bool get_OnlyIfCached();
    ASPOSECPP_SHARED_API void set_OnlyIfCached(bool value);
    ASPOSECPP_SHARED_API bool get_Public();
    ASPOSECPP_SHARED_API void set_Public(bool value);
    ASPOSECPP_SHARED_API bool get_Private();
    ASPOSECPP_SHARED_API void set_Private(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<String>> get_PrivateHeaders();
    ASPOSECPP_SHARED_API bool get_MustRevalidate();
    ASPOSECPP_SHARED_API void set_MustRevalidate(bool value);
    ASPOSECPP_SHARED_API bool get_ProxyRevalidate();
    ASPOSECPP_SHARED_API void set_ProxyRevalidate(bool value);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::ICollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Extensions();

    ASPOSECPP_SHARED_API CacheControlHeaderValue();

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<CacheControlHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<CacheControlHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetCacheControlLength(String input, int32_t startIndex,
                                         System::SharedPtr<CacheControlHeaderValue> storeValue,
                                         System::SharedPtr<CacheControlHeaderValue>& parsedValue);

private:
    static const String maxAgeString;
    static const String maxStaleString;
    static const String minFreshString;
    static const String mustRevalidateString;
    static const String noCacheString;
    static const String noStoreString;
    static const String noTransformString;
    static const String onlyIfCachedString;
    static const String privateString;
    static const String proxyRevalidateString;
    static const String publicString;
    static const String sharedMaxAgeString;
    // static System::SharedPtr<HttpHeaderParser> s_nameValueListParser;
    static Action<String> s_checkIsValidToken;
    bool _noCache;
    System::SharedPtr<ObjectCollection<String>> _noCacheHeaders;
    bool _noStore;
    Nullable<TimeSpan> _maxAge;
    Nullable<TimeSpan> _sharedMaxAge;
    bool _maxStale;
    Nullable<TimeSpan> _maxStaleLimit;
    Nullable<TimeSpan> _minFresh;
    bool _noTransform;
    bool _onlyIfCached;
    bool _publicField;
    bool _privateField;
    System::SharedPtr<ObjectCollection<String>> _privateHeaders;
    bool _mustRevalidate;
    bool _proxyRevalidate;
    System::SharedPtr<ObjectCollection<System::SharedPtr<NameValueHeaderValue>>> _extensions;

    CacheControlHeaderValue(System::SharedPtr<CacheControlHeaderValue> source);

    static bool TrySetCacheControlValues(
        System::SharedPtr<CacheControlHeaderValue> cc,
        System::SharedPtr<Collections::Generic::List<System::SharedPtr<NameValueHeaderValue>>> nameValueList);
    static bool TrySetTokenOnlyValue(System::SharedPtr<NameValueHeaderValue> nameValue, bool& boolField);
    static bool TrySetOptionalTokenList(System::SharedPtr<NameValueHeaderValue> nameValue, bool& boolField,
                                        System::SharedPtr<ObjectCollection<String>>& destination);
    static bool TrySetTimeSpan(System::SharedPtr<NameValueHeaderValue> nameValue, Nullable<TimeSpan>& timeSpan);
    static void AppendValueIfRequired(System::SharedPtr<Text::StringBuilder> sb, bool appendValue, String value);
    static void AppendValueWithSeparatorIfRequired(System::SharedPtr<Text::StringBuilder> sb, String value);
    static void AppendValues(System::SharedPtr<Text::StringBuilder> sb,
                             System::SharedPtr<ObjectCollection<String>> values);
    static void CheckIsValidToken(String item);
    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
