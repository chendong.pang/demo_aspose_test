﻿/// @file net/http/headers/http_response_headers.h
#pragma once

#include <system/uri.h>

#include <net/http/headers/authentication_header_value.h>
#include <net/http/headers/entity_tag_header_value.h>
#include <net/http/headers/transfer_coding_header_value.h>
#include <net/http/headers/via_header_value.h>
#include <net/http/headers/http_headers.h>
#include <net/http/headers/http_header_value_collection.h>
#include <net/http/headers/product_info_header_value.h>
#include <net/http/headers/retry_condition_header_value.h>
#include <net/http/headers/cache_control_header_value.h>
#include <net/http/headers/warning_header_value.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class HttpGeneralHeaders;

class ASPOSECPP_SHARED_CLASS HttpResponseHeaders FINAL : public System::Net::Http::Headers::HttpHeaders
{
    typedef HttpResponseHeaders ThisType;
    typedef System::Net::Http::Headers::HttpHeaders BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_AcceptRanges();
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_Age();
    ASPOSECPP_SHARED_API void set_Age(Nullable<TimeSpan> value);
    ASPOSECPP_SHARED_API System::SharedPtr<EntityTagHeaderValue> get_ETag();
    ASPOSECPP_SHARED_API void set_ETag(System::SharedPtr<EntityTagHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_Location();
    ASPOSECPP_SHARED_API void set_Location(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<AuthenticationHeaderValue>>>
        get_ProxyAuthenticate();
    ASPOSECPP_SHARED_API System::SharedPtr<RetryConditionHeaderValue> get_RetryAfter();
    ASPOSECPP_SHARED_API void set_RetryAfter(System::SharedPtr<RetryConditionHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductInfoHeaderValue>>>
        get_Server();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_Vary();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<AuthenticationHeaderValue>>>
        get_WwwAuthenticate();
    ASPOSECPP_SHARED_API System::SharedPtr<CacheControlHeaderValue> get_CacheControl();
    ASPOSECPP_SHARED_API void set_CacheControl(System::SharedPtr<CacheControlHeaderValue> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_Connection();
    ASPOSECPP_SHARED_API Nullable<bool> get_ConnectionClose();
    ASPOSECPP_SHARED_API void set_ConnectionClose(Nullable<bool> value);
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Date();
    ASPOSECPP_SHARED_API void set_Date(Nullable<DateTimeOffset> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<NameValueHeaderValue>>>
        get_Pragma();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<String>> get_Trailer();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<TransferCodingHeaderValue>>>
        get_TransferEncoding();
    ASPOSECPP_SHARED_API Nullable<bool> get_TransferEncodingChunked();
    ASPOSECPP_SHARED_API void set_TransferEncodingChunked(Nullable<bool> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductHeaderValue>>>
        get_Upgrade();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ViaHeaderValue>>> get_Via();
    ASPOSECPP_SHARED_API System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<WarningHeaderValue>>>
        get_Warning();

    ASPOSECPP_SHARED_API HttpResponseHeaders();

    ASPOSECPP_SHARED_API static void
        AddKnownHeaders(System::SharedPtr<Collections::Generic::HashSet<String>> headerSet);
    ASPOSECPP_SHARED_API virtual void AddHeaders(System::SharedPtr<HttpHeaders> sourceHeaders) override;

protected:
    System::Object::shared_members_type GetSharedMembers() override;

private:
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        s_parserStore;
    static System::SharedPtr<Collections::Generic::HashSet<String>> s_invalidHeaders;
    System::SharedPtr<HttpGeneralHeaders> _generalHeaders;
    System::SharedPtr<HttpHeaderValueCollection<String>> _acceptRanges;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<AuthenticationHeaderValue>>> _wwwAuthenticate;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<AuthenticationHeaderValue>>> _proxyAuthenticate;
    System::SharedPtr<HttpHeaderValueCollection<System::SharedPtr<ProductInfoHeaderValue>>> _server;
    System::SharedPtr<HttpHeaderValueCollection<String>> _vary;

    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        CreateParserStore();
    static System::SharedPtr<Collections::Generic::Dictionary<String, System::SharedPtr<HttpHeaderParser>>>
        GetParserStore();
    static System::SharedPtr<Collections::Generic::HashSet<String>> CreateInvalidHeaders();

    static void InitStatic();
};

}}}} // namespace System::Net::Http::Headers
