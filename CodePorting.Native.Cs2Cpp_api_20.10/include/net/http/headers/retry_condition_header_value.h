﻿/// @file net/http/headers/retry_condition_header_value.h
#pragma once

#include "system/date_time_offset.h"
#include <cstdint>
#include <system/icloneable.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/timespan.h>

namespace System { namespace Net { namespace Http { namespace Headers {

class ASPOSECPP_SHARED_CLASS RetryConditionHeaderValue : public System::ICloneable
{
    typedef RetryConditionHeaderValue ThisType;
    typedef System::ICloneable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API Nullable<DateTimeOffset> get_Date();
    ASPOSECPP_SHARED_API Nullable<TimeSpan> get_Delta();

    ASPOSECPP_SHARED_API RetryConditionHeaderValue(DateTimeOffset date);
    ASPOSECPP_SHARED_API RetryConditionHeaderValue(TimeSpan delta);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API virtual bool Equals(System::SharedPtr<Object> obj) override;
    ASPOSECPP_SHARED_API virtual int32_t GetHashCode() const override;
    ASPOSECPP_SHARED_API static System::SharedPtr<RetryConditionHeaderValue> Parse(String input);
    ASPOSECPP_SHARED_API static bool TryParse(String input, System::SharedPtr<RetryConditionHeaderValue>& parsedValue);
    ASPOSECPP_SHARED_API static int32_t GetRetryConditionLength(String input, int32_t startIndex, System::SharedPtr<Object>& parsedValue);

private:
    Nullable<DateTimeOffset> _date;
    Nullable<TimeSpan> _delta;

    RetryConditionHeaderValue(System::SharedPtr<RetryConditionHeaderValue> source);
    RetryConditionHeaderValue();

    System::SharedPtr<Object> Clone() override;
};

}}}} // namespace System::Net::Http::Headers
