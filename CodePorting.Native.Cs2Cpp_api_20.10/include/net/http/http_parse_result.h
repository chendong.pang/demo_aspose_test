﻿/// @file net/http/http_parse_result.h
#pragma once

namespace System { namespace Net { namespace Http {

enum class HttpParseResult
{
    Parsed,
    NotParsed,
    InvalidFormat
};
}}} // namespace System::Net::Http
