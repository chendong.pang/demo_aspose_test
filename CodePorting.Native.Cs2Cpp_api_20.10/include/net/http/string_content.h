﻿/// @file net/http/string_content.h
#pragma once

#include <net/http/byte_array_content.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS StringContent : public System::Net::Http::ByteArrayContent
{
    typedef StringContent ThisType;
    typedef System::Net::Http::ByteArrayContent BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API StringContent(String content);
    ASPOSECPP_SHARED_API StringContent(String content, System::SharedPtr<Text::Encoding> encoding);
    ASPOSECPP_SHARED_API StringContent(String content, System::SharedPtr<Text::Encoding> encoding, String mediaType);

private:
    static const String defaultMediaType;

    static System::ArrayPtr<uint8_t> GetContentByteArray(String content, System::SharedPtr<Text::Encoding> encoding);
};

}}} // namespace System::Net::Http
