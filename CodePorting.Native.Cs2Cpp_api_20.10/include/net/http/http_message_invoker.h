﻿/// @file net/http/http_message_invoker.h
#pragma once

#include <system/idisposable.h>
#include <system/shared_ptr.h>

#include <net/http/http_message_handler.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpMessageInvoker : public System::IDisposable
{
    typedef HttpMessageInvoker ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API HttpMessageInvoker(System::SharedPtr<HttpMessageHandler> handler);
    ASPOSECPP_SHARED_API HttpMessageInvoker(System::SharedPtr<HttpMessageHandler> handler, bool disposeHandler);

    ASPOSECPP_SHARED_API virtual System::SharedPtr<HttpResponseMessage> Send(System::SharedPtr<HttpRequestMessage> request);
    ASPOSECPP_SHARED_API void Dispose() override;

protected:
    virtual void Dispose(bool disposing);

private:
    bool _disposed;
    bool _disposeHandler;
    System::SharedPtr<HttpMessageHandler> _handler;

    void CheckDisposed();
};

}}} // namespace System::Net::Http
