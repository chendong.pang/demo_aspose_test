﻿/// @file net/http/http_content.h
#pragma once

#include <cstdint>
#include <system/array.h>
#include <system/exceptions.h>
#include <system/idisposable.h>
#include <system/io/memory_stream.h>
#include <system/io/stream.h>
#include <system/nullable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/text/encoding.h>

#include <net/http/headers/http_content_headers.h>

namespace System { namespace Net { namespace Http {

class ABSTRACT ASPOSECPP_SHARED_CLASS HttpContent : public System::IDisposable
{
    typedef HttpContent ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

private:
    class LimitMemoryStream : public System::IO::MemoryStream
    {
        typedef LimitMemoryStream ThisType;
        typedef System::IO::MemoryStream BaseType;

        typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
        ASPOSECPP_SHARED_RTTI_INFO_DECL();

    public:
        LimitMemoryStream(int32_t maxSize, int32_t capacity);

        virtual void Write(System::ArrayPtr<uint8_t> buffer, int32_t offset, int32_t count) override;
        virtual void WriteByte(uint8_t value) override;

    private:
        int32_t _maxSize;

        void CheckSize(int32_t countToAdd);
    };

public:
    static const int64_t MaxBufferSize;
    static System::SharedPtr<Text::Encoding> DefaultStringEncoding;

    ASPOSECPP_SHARED_API System::SharedPtr<Headers::HttpContentHeaders> get_Headers();
    ASPOSECPP_SHARED_API System::SharedPtr<IO::Stream> ReadAsStream();
    ASPOSECPP_SHARED_API String ReadAsString();
    ASPOSECPP_SHARED_API System::ArrayPtr<uint8_t> ReadAsByteArray();
    ASPOSECPP_SHARED_API void LoadIntoBuffer();
    ASPOSECPP_SHARED_API void LoadIntoBuffer(int64_t maxBufferSize);

    ASPOSECPP_SHARED_API virtual bool TryComputeLength(int64_t& length) = 0;
    ASPOSECPP_SHARED_API void Dispose() override;

protected:
    HttpContent();
    virtual void SerializeToStream(System::SharedPtr<IO::Stream> stream) = 0;
    virtual void Dispose(bool disposing);

private:
    System::SharedPtr<Headers::HttpContentHeaders> _headers;
    System::SharedPtr<IO::MemoryStream> _bufferedContent;
    bool _disposed;
    System::SharedPtr<IO::Stream> _contentReadStream;
    bool _canCalculateLength;
    static const int32_t UTF8CodePage;
    static const int32_t UTF8PreambleLength;
    static const uint8_t UTF8PreambleByte0;
    static const uint8_t UTF8PreambleByte1;
    static const uint8_t UTF8PreambleByte2;
    static const int32_t UTF8PreambleFirst2Bytes;
    static const int32_t UTF32CodePage;
    static const int32_t UTF32PreambleLength;
    static const uint8_t UTF32PreambleByte0;
    static const uint8_t UTF32PreambleByte1;
    static const uint8_t UTF32PreambleByte2;
    static const uint8_t UTF32PreambleByte3;
    static const int32_t UTF32OrUnicodePreambleFirst2Bytes;
    static const int32_t UnicodeCodePage;
    static const int32_t UnicodePreambleLength;
    static const uint8_t UnicodePreambleByte0;
    static const uint8_t UnicodePreambleByte1;
    static const int32_t BigEndianUnicodeCodePage;
    static const int32_t BigEndianUnicodePreambleLength;
    static const uint8_t BigEndianUnicodePreambleByte0;
    static const uint8_t BigEndianUnicodePreambleByte1;
    static const int32_t BigEndianUnicodePreambleFirst2Bytes;

    bool get_IsBuffered();

    Nullable<int64_t> GetComputedOrBufferLength();
    virtual System::SharedPtr<IO::Stream> CreateContentReadStream();

    System::SharedPtr<IO::MemoryStream> CreateMemoryStream(int64_t maxBufferSize);
    System::ArrayPtr<uint8_t> GetDataBuffer(System::SharedPtr<IO::MemoryStream> stream);
    void CheckDisposed();
    static Exception GetStreamCopyException(Exception originalException);
    static int32_t GetPreambleLength(System::ArrayPtr<uint8_t> data, int32_t dataLength,
                                     System::SharedPtr<Text::Encoding> encoding);
    static bool TryDetectEncoding(System::ArrayPtr<uint8_t> data, int32_t dataLength,
                                  System::SharedPtr<Text::Encoding>& encoding, int32_t& preambleLength);
    static bool ByteArrayHasPrefix(System::ArrayPtr<uint8_t> byteArray, int32_t dataLength,
                                   System::ArrayPtr<uint8_t> prefix);
};

}}} // namespace System::Net::Http
