﻿/// @file net/http/http_message_handler.h
#pragma once

#include <system/idisposable.h>
#include <system/object.h>
#include <system/shared_ptr.h>

#include <net/http/http_request_message.h>
#include <net/http/http_response_message.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS ABSTRACT HttpMessageHandler : public System::IDisposable
{
    typedef HttpMessageHandler ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    virtual System::SharedPtr<HttpResponseMessage> Send(System::SharedPtr<HttpRequestMessage> request) = 0;
    ASPOSECPP_SHARED_API void Dispose() override;

protected:
    HttpMessageHandler();

    virtual void Dispose(bool disposing);
};

}}} // namespace System::Net::Http
