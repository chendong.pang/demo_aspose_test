﻿/// @file net/http/http_request_message.h
#pragma once

#include <net/http/headers/http_request_headers.h>
#include <net/http/http_content.h>
#include <net/http/http_method.h>
#include <cstdint>
#include <system/collections/idictionary.h>
#include <system/idisposable.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>
#include <system/version.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpRequestMessage : public System::IDisposable
{
    typedef HttpRequestMessage ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API System::Version get_Version();
    ASPOSECPP_SHARED_API void set_Version(System::Version value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpContent> get_Content();
    ASPOSECPP_SHARED_API void set_Content(System::SharedPtr<HttpContent> value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpMethod> get_Method();
    ASPOSECPP_SHARED_API void set_Method(System::SharedPtr<HttpMethod> value);
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_RequestUri();
    ASPOSECPP_SHARED_API void set_RequestUri(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API System::SharedPtr<System::Net::Http::Headers::HttpRequestHeaders> get_Headers();
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::IDictionary<String, System::SharedPtr<Object>>>
        get_Properties();

    ASPOSECPP_SHARED_API HttpRequestMessage();
    ASPOSECPP_SHARED_API HttpRequestMessage(System::SharedPtr<HttpMethod> method, System::SharedPtr<Uri> requestUri);
    ASPOSECPP_SHARED_API HttpRequestMessage(System::SharedPtr<HttpMethod> method, String requestUri);

    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API bool MarkAsSent();
    ASPOSECPP_SHARED_API void Dispose() override;

protected:
    virtual void Dispose(bool disposing);
    System::Object::shared_members_type GetSharedMembers() override;

private:
    static const int32_t MessageNotYetSent;
    static const int32_t MessageAlreadySent;
    int32_t _sendStatus;
    System::SharedPtr<HttpMethod> _method;
    System::SharedPtr<Uri> _requestUri;
    System::SharedPtr<Headers::HttpRequestHeaders> _headers;
    System::Version _version;
    System::SharedPtr<HttpContent> _content;
    bool _disposed;
    System::SharedPtr<Collections::Generic::IDictionary<String, System::SharedPtr<Object>>> _properties;

    void InitializeValues(System::SharedPtr<HttpMethod> method, System::SharedPtr<Uri> requestUri);
    void CheckDisposed();
};

}}} // namespace System::Net::Http
