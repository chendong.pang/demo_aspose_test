﻿/// @file net/http/http_client.h
#pragma once

#include <functional>
#include <system/globalization/culture_info.h>
#include <system/io/stream.h>
#include <system/shared_ptr.h>
#include <system/timespan.h>

#include <net/http/headers/http_request_headers.h>
#include <net/http/http_completion_option.h>
#include <net/http/http_content.h>
#include <net/http/http_message_handler.h>
#include <net/http/http_message_invoker.h>
#include <net/http/http_request_message.h>
#include <net/http/http_response_message.h>
//#include <net/http/http_utilities.h>
#include <net/http_request_exception.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpClient : public System::Net::Http::HttpMessageInvoker
{
    typedef HttpClient ThisType;
    typedef System::Net::Http::HttpMessageInvoker BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API System::SharedPtr<Headers::HttpRequestHeaders> get_DefaultRequestHeaders();
    ASPOSECPP_SHARED_API System::SharedPtr<Uri> get_BaseAddress();
    ASPOSECPP_SHARED_API void set_BaseAddress(System::SharedPtr<Uri> value);
    ASPOSECPP_SHARED_API TimeSpan get_Timeout();
    ASPOSECPP_SHARED_API void set_Timeout(TimeSpan value);
    ASPOSECPP_SHARED_API int64_t get_MaxResponseContentBufferSize();
    ASPOSECPP_SHARED_API void set_MaxResponseContentBufferSize(int64_t value);

    ASPOSECPP_SHARED_API HttpClient();
    ASPOSECPP_SHARED_API HttpClient(System::SharedPtr<HttpMessageHandler> handler);
    ASPOSECPP_SHARED_API HttpClient(System::SharedPtr<HttpMessageHandler> handler, bool disposeHandler);

    ASPOSECPP_SHARED_API void CancelPendingRequests();

    ASPOSECPP_SHARED_API System::SharedPtr<HttpResponseMessage> Send(System::SharedPtr<HttpRequestMessage> request,
                                                                     HttpCompletionOption completionOption);

protected:
    virtual void Dispose(bool disposing) override;

private:
    static TimeSpan s_defaultTimeout;
    static TimeSpan s_maxTimeout;
    static TimeSpan s_infiniteTimeout;
    static const HttpCompletionOption defaultCompletionOption;
    bool _operationStarted;
    bool _disposed;
    System::SharedPtr<Headers::HttpRequestHeaders> _defaultRequestHeaders;
    System::SharedPtr<Uri> _baseAddress;
    TimeSpan _timeout;
    int64_t _maxResponseContentBufferSize;

    void DisposeRequestContent(System::SharedPtr<HttpRequestMessage> request);
    void SetOperationStarted();
    void CheckDisposedOrStarted();
    void CheckDisposed();
    static void CheckRequestMessage(System::SharedPtr<HttpRequestMessage> request);
    void PrepareRequestMessage(System::SharedPtr<HttpRequestMessage> request);
    static void CheckBaseAddress(System::SharedPtr<Uri> baseAddress, String parameterName);
    System::SharedPtr<Uri> CreateUri(String uri);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}}} // namespace System::Net::Http
