﻿/// @file net/http/byte_array_content.h
#pragma once

#include <cstdint>
#include <system/array.h>
#include <system/io/stream.h>
#include <system/shared_ptr.h>
#include <net/http/http_content.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS ByteArrayContent : public System::Net::Http::HttpContent
{
    typedef ByteArrayContent ThisType;
    typedef System::Net::Http::HttpContent BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API ByteArrayContent(System::ArrayPtr<uint8_t> content);
    ASPOSECPP_SHARED_API ByteArrayContent(System::ArrayPtr<uint8_t> content, int32_t offset, int32_t count);

    ASPOSECPP_SHARED_API virtual bool TryComputeLength(int64_t& length) override;

protected:
    virtual System::SharedPtr<IO::Stream> CreateContentReadStream() override;
    void SerializeToStream(System::SharedPtr<IO::Stream> stream) override;

private:
    System::ArrayPtr<uint8_t> _content;
    int32_t _offset;
    int32_t _count;
};

}}} // namespace System::Net::Http
