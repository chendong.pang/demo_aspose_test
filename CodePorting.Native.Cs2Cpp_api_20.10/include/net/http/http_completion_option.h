﻿/// @file net/http/http_completion_option.h
#pragma once

namespace System { namespace Net { namespace Http {

enum class HttpCompletionOption
{
    ResponseContentRead = 0,
    ResponseHeadersRead
};
}}} // namespace System::Net::Http
