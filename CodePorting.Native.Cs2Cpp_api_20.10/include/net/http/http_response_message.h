﻿/// @file net/http/http_response_message.h
#pragma once

#include <system/idisposable.h>
#include <system/shared_ptr.h>
#include <system/string.h>

#include <net/http/headers/http_response_headers.h>
#include <net/http/http_request_message.h>
#include <net/http_status_code.h>

namespace System { namespace Net { namespace Http {

class ASPOSECPP_SHARED_CLASS HttpResponseMessage : public System::IDisposable
{
    typedef HttpResponseMessage ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    ASPOSECPP_SHARED_API System::Version get_Version() const;
    ASPOSECPP_SHARED_API void set_Version(System::Version value);
    ASPOSECPP_SHARED_API System::SharedPtr<HttpContent> get_Content() const;
    ASPOSECPP_SHARED_API void set_Content(System::SharedPtr<HttpContent> value);
    ASPOSECPP_SHARED_API HttpStatusCode get_StatusCode() const;
    ASPOSECPP_SHARED_API void set_StatusCode(HttpStatusCode value);
    ASPOSECPP_SHARED_API String get_ReasonPhrase() const;
    ASPOSECPP_SHARED_API void set_ReasonPhrase(String value);
    ASPOSECPP_SHARED_API System::SharedPtr<Headers::HttpResponseHeaders> get_Headers() const;
    ASPOSECPP_SHARED_API System::SharedPtr<HttpRequestMessage> get_RequestMessage() const;
    ASPOSECPP_SHARED_API void set_RequestMessage(System::SharedPtr<HttpRequestMessage> value);
    ASPOSECPP_SHARED_API bool get_IsSuccessStatusCode() const;

    ASPOSECPP_SHARED_API HttpResponseMessage();
    ASPOSECPP_SHARED_API HttpResponseMessage(HttpStatusCode statusCode);

    ASPOSECPP_SHARED_API System::SharedPtr<HttpResponseMessage> EnsureSuccessStatusCode();
    ASPOSECPP_SHARED_API virtual String ToString() const override;
    ASPOSECPP_SHARED_API void Dispose() override;

protected:
    virtual void Dispose(bool disposing);
    System::Object::shared_members_type GetSharedMembers() override;


private:
    static const HttpStatusCode defaultStatusCode;
    HttpStatusCode _statusCode;
    mutable System::SharedPtr<Headers::HttpResponseHeaders> _headers;
    String _reasonPhrase;
    System::SharedPtr<HttpRequestMessage> _requestMessage;
    System::Version _version;
    System::SharedPtr<HttpContent> _content;
    bool _disposed;

    bool ContainsNewLineCharacter(String value);
    void CheckDisposed();
};

}}} // namespace System::Net::Http
