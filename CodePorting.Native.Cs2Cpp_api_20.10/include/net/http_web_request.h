﻿/// @file net/http_web_request.h
#pragma once

#include <cstdint>
#include <system/async_callback.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>
#include <net/cookie_container.h>
#include <net/http/http_response_message.h>
#include <net/iweb_proxy.h>
#include <net/service_point.h>
#include <net/web_request.h>
#include "security/cryptography/x509_certificates/x509_certificate_collection.h"

namespace System { namespace Net {

class RequestStream;

class ASPOSECPP_SHARED_CLASS HttpWebRequest : public System::Net::WebRequest
{
    typedef HttpWebRequest ThisType;
    typedef System::Net::WebRequest BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API void set_ProtocolVersion(System::Version version);
    ASPOSECPP_SHARED_API void set_Timeout(int timeout) override;
    ASPOSECPP_SHARED_API void set_PreAuthenticate(bool value) override;
    ASPOSECPP_SHARED_API void set_ContentLength(int64_t length) override;

    ASPOSECPP_SHARED_API String get_Accept();
    ASPOSECPP_SHARED_API void set_Accept(String value);
    ASPOSECPP_SHARED_API virtual bool get_AllowReadStreamBuffering();
    ASPOSECPP_SHARED_API virtual void set_AllowReadStreamBuffering(bool value);
    ASPOSECPP_SHARED_API virtual String get_ContentType() override;
    ASPOSECPP_SHARED_API virtual void set_ContentType(String value) override;
    ASPOSECPP_SHARED_API int32_t get_ContinueTimeout();
    ASPOSECPP_SHARED_API void set_ContinueTimeout(int32_t value);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<System::Net::CookieContainer> get_CookieContainer();
    ASPOSECPP_SHARED_API virtual void set_CookieContainer(System::SharedPtr<System::Net::CookieContainer> value);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<ICredentials> get_Credentials() override;
    ASPOSECPP_SHARED_API virtual void set_Credentials(System::SharedPtr<ICredentials> value) override;
    ASPOSECPP_SHARED_API virtual bool get_HaveResponse();
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebHeaderCollection> get_Headers() override;
    ASPOSECPP_SHARED_API virtual void set_Headers(System::SharedPtr<WebHeaderCollection> value) override;
    ASPOSECPP_SHARED_API virtual String get_Method() override;
    ASPOSECPP_SHARED_API virtual void set_Method(String value) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Uri> get_RequestUri() override;
    ASPOSECPP_SHARED_API virtual bool get_SupportsCookieContainer();
    ASPOSECPP_SHARED_API virtual bool get_UseDefaultCredentials() override;
    ASPOSECPP_SHARED_API virtual void set_UseDefaultCredentials(bool value) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IWebProxy> get_Proxy() override;
    ASPOSECPP_SHARED_API virtual void set_Proxy(System::SharedPtr<IWebProxy> value) override;

    ASPOSECPP_SHARED_API System::SharedPtr<ServicePoint> get_ServicePoint();
    ASPOSECPP_SHARED_API virtual void set_AllowAutoRedirect(bool value);

    ASPOSECPP_SHARED_API HttpWebRequest(System::SharedPtr<Uri> uri);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebResponse> GetResponse() override;
    ASPOSECPP_SHARED_API virtual void Abort() override;
    ASPOSECPP_SHARED_API System::SharedPtr<System::IO::Stream> GetRequestStream() override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IAsyncResult> BeginGetRequestStream(AsyncCallback callback,
                                                                                       System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IO::Stream>
        EndGetRequestStream(System::SharedPtr<IAsyncResult> asyncResult) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<IAsyncResult> BeginGetResponse(AsyncCallback callback,
                                                                                  System::SharedPtr<Object> state) override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebResponse>
        EndGetResponse(System::SharedPtr<IAsyncResult> asyncResult) override;

    ASPOSECPP_SHARED_API virtual bool get_SendChunked();
    ASPOSECPP_SHARED_API virtual void set_SendChunked(bool value);
    ASPOSECPP_SHARED_API virtual bool get_AllowWriteStreamBuffering();
    ASPOSECPP_SHARED_API virtual void set_AllowWriteStreamBuffering(bool value);
    ASPOSECPP_SHARED_API virtual bool get_KeepAlive();
    ASPOSECPP_SHARED_API virtual void set_KeepAlive(bool value);
    ASPOSECPP_SHARED_API virtual System::SharedPtr<
        System::Security::Cryptography::X509Certificates::X509CertificateCollection>
        get_ClientCertificates();
    ASPOSECPP_SHARED_API virtual void set_ClientCertificates(
        System::SharedPtr<System::Security::Cryptography::X509Certificates::X509CertificateCollection> value);
    ASPOSECPP_SHARED_API virtual System::String get_UserAgent();
    ASPOSECPP_SHARED_API virtual void set_UserAgent(System::String value);
    ASPOSECPP_SHARED_API virtual int64_t get_ContentLength() override;
    ASPOSECPP_SHARED_API virtual bool get_PreAuthenticate() override;
    ASPOSECPP_SHARED_API virtual int32_t get_Timeout() override;
    ASPOSECPP_SHARED_API virtual void AddRange(int32_t range);
    ASPOSECPP_SHARED_API virtual void AddRange(System::String rangeSpecifier, int32_t from, int32_t to);

    ASPOSECPP_SHARED_API virtual System::String get_Referer();
    ASPOSECPP_SHARED_API virtual void set_Referer(System::String value);

    ASPOSECPP_SHARED_API virtual System::String get_ConnectionGroupName() override;
    ASPOSECPP_SHARED_API virtual void set_ConnectionGroupName(System::String value) override;

protected:
    System::Object::shared_members_type GetSharedMembers() override;

private:
    static const int32_t DefaultContinueTimeout;
    System::SharedPtr<WebHeaderCollection> _webHeaderCollection;
    System::SharedPtr<Uri> _requestUri;
    String _originVerb;
    int32_t _continueTimeout;
    bool _allowReadStreamBuffering;
    System::SharedPtr<System::Net::CookieContainer> _cookieContainer;
    System::SharedPtr<ICredentials> _credentials;
    System::SharedPtr<IWebProxy> _proxy;
    int32_t _beginGetRequestStreamCalled;
    int32_t _beginGetResponseCalled;
    int32_t _endGetRequestStreamCalled;
    int32_t _endGetResponseCalled;
    System::SharedPtr<RequestStream> _requestStream;
    AsyncCallback _requestStreamCallback;
    AsyncCallback _responseCallback;
    int32_t _abortCalled;
    int64_t _contentLength;
    bool _isVersionHttp10;
    int32_t _timeout;
    bool m_PreAuthenticate;

    bool get_RequestSubmitted();

    static System::ArrayPtr<String> s_wellKnownContentHeaders;

    void SetSpecialHeaders(String HeaderName, String value);
    void CheckAbort();
    bool IsWellKnownContentHeader(String header);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;

    String _connectionGroupName;
    bool _allowAutoRedirect;
    bool _keepAlive;
    String _userAgent;
};

}} // namespace System::Net
