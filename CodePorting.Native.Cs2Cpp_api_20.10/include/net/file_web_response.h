﻿/// @file net/file_web_response.h
#pragma once

#include <system/collections/ienumerable.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/uri.h>

#include <net/cookie_collection.h>
#include <net/cookie_container.h>
#include <net/http/http_response_message.h>
#include <net/http_status_code.h>
#include <net/web_header_collection.h>
#include <net/web_response.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS FileWebResponse : public System::Net::WebResponse
{
    typedef FileWebResponse ThisType;
    typedef System::Net::WebResponse BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    ASPOSECPP_SHARED_API virtual int64_t get_ContentLength() override;
    ASPOSECPP_SHARED_API virtual String get_ContentType() override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebHeaderCollection> get_Headers() override;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<Uri> get_ResponseUri() override;
    ASPOSECPP_SHARED_API virtual bool get_SupportsHeaders() override;

    ASPOSECPP_SHARED_API FileWebResponse(System::SharedPtr<Uri> uri);

    ASPOSECPP_SHARED_API virtual System::SharedPtr<IO::Stream> GetResponseStream() override;
    ASPOSECPP_SHARED_API virtual void Close() override;

protected:
    virtual void Dispose(bool disposing) override;
    System::Object::shared_members_type GetSharedMembers() override;

private:
    System::SharedPtr<Uri> _requestUri;

};

}} // namespace System::Net
