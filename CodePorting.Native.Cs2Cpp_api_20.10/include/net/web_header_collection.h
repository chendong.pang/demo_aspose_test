﻿/// @file net/web_header_collection.h
#pragma once

#include <cstdint>
#include <system/array.h>
#include <system/collections/dictionary.h>
#include <system/collections/list.h>
#include <system/string.h>

#include <net/http_request_header.h>
#include <net/http_response_header.h>

namespace System { namespace Net {

enum class WebHeaderCollectionType : uint8_t
{
    Unknown,
    WebRequest,
    WebResponse
};

class ASPOSECPP_SHARED_CLASS WebHeaderCollection : public Object
{
public:
    ASPOSECPP_SHARED_API void Add(String header, String value);
    ASPOSECPP_SHARED_API void Add(HttpResponseHeader header, String value);
    ASPOSECPP_SHARED_API void Add(HttpRequestHeader header, String value);
    ASPOSECPP_SHARED_API String idx_get(HttpRequestHeader header);
    ASPOSECPP_SHARED_API void idx_set(HttpRequestHeader header, String value);
    ASPOSECPP_SHARED_API String idx_get(HttpResponseHeader header);
    ASPOSECPP_SHARED_API void idx_set(HttpResponseHeader header, String value);
    ASPOSECPP_SHARED_API String idx_get(String name);
    ASPOSECPP_SHARED_API void idx_set(String name, String value);
    ASPOSECPP_SHARED_API void Remove(String name);
    ASPOSECPP_SHARED_API void Remove(HttpResponseHeader header);
    ASPOSECPP_SHARED_API void Remove(HttpRequestHeader header);
    ASPOSECPP_SHARED_API virtual String ToString() const override;

    ASPOSECPP_SHARED_API WebHeaderCollection();

    ASPOSECPP_SHARED_API int32_t get_Count() const;
    ASPOSECPP_SHARED_API System::ArrayPtr<String> AllKeys();
    ASPOSECPP_SHARED_API System::ArrayPtr<String> get_Keys();

    ASPOSECPP_SHARED_API String GetKey(int index);
    ASPOSECPP_SHARED_API System::ArrayPtr<System::String> GetValues(String header);

    ASPOSECPP_SHARED_API void Set(String name, String value);

    /// Not implemented
    ASPOSECPP_SHARED_API static bool IsRestricted(const String& headerName)
    {
        ASPOSE_UNUSED(headerName);
        throw NotImplementedException(ASPOSE_CURRENT_FUNCTION);
    }

protected:
    ASPOSECPP_SHARED_API ~WebHeaderCollection() override;

private:
    static const int32_t ApproxAveHeaderLineSize;
    static const int32_t ApproxHighAvgNumHeaders;
    System::SharedPtr<Collections::Generic::List<String>> _entriesList;
    System::SharedPtr<Collections::Generic::Dictionary<String, String>> _entriesDictionary;
    System::ArrayPtr<String> _allKeys;
    WebHeaderCollectionType _type;

    bool get_AllowHttpRequestHeader();
    bool get_AllowHttpResponseHeader();

    static System::ArrayPtr<char16_t> s_httpTrimCharacters;

    static String CheckBadHeaderValueChars(String value);
    static String CheckBadHeaderNameChars(String name);
    static bool ContainsNonAsciiChars(String token);
    bool IsInitialized();
    void EnsureInitialized();
    void InvalidateCachedArray();
    static System::ArrayPtr<String> ToArray(System::SharedPtr<Collections::Generic::List<String>> list);

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}} // namespace System::Net
