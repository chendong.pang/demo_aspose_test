﻿/// @file net/http_status_description.h
#pragma once

#include <cstdint>
#include <net/http_status_code.h>
#include <system/array.h>
#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net {

class HttpStatusDescription : public System::Object
{
    typedef HttpStatusDescription ThisType;
    typedef System::Object BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    static String Get(HttpStatusCode code);
    static String Get(int32_t code);

private:
    static System::ArrayPtr<System::ArrayPtr<String>> s_httpStatusDescriptions;

    static struct __StaticConstructor__
    {
        __StaticConstructor__();
    } s_constructor__;
};

}} // namespace System::Net
