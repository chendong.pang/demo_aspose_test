﻿/// @file net/web_exception.h
#pragma once

#include <system/exceptions.h>
#include <system/shared_ptr.h>
#include <system/string.h>

#include <net/http_web_request.h>
#include <net/web_exception_status.h>

namespace System { namespace Net {

class Details_WebException;
using WebException = System::ExceptionWrapper<Details_WebException>;


class ASPOSECPP_SHARED_CLASS Details_WebException : public System::Details_InvalidOperationException
{
public:
    virtual ~Details_WebException();

    ASPOSECPP_SHARED_API WebExceptionStatus get_Status();
    ASPOSECPP_SHARED_API System::SharedPtr<WebResponse> get_Response();

    ASPOSECPP_SHARED_API Details_WebException();
    ASPOSECPP_SHARED_API Details_WebException(std::nullptr_t);
    ASPOSECPP_SHARED_API Details_WebException(String message);
    ASPOSECPP_SHARED_API Details_WebException(String message, Exception innerException);
    ASPOSECPP_SHARED_API Details_WebException(String message, WebExceptionStatus status);
    ASPOSECPP_SHARED_API Details_WebException(String message, Exception innerException, WebExceptionStatus status,
                                      System::SharedPtr<WebResponse> response);

    static Exception CreateCompatibleException(Exception exception);

private:
    static const WebExceptionStatus DefaultStatus;
    WebExceptionStatus _status;
    System::SharedPtr<WebResponse> _response;
};

}} // namespace System::Net
