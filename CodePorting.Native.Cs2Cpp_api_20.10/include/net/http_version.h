﻿/// @file net/http_version.h
#pragma once

#include <system/object.h>
#include <system/version.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS HttpVersion
{
public:
    static ASPOSECPP_SHARED_API Version Unknown;
    static ASPOSECPP_SHARED_API Version Version10;
    static ASPOSECPP_SHARED_API Version Version11;
    static ASPOSECPP_SHARED_API Version Version20;
};

}} // namespace System::Net
