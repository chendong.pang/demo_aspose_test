﻿/// @file net/cookie_collection.h
#pragma once

#include <system/collections/icollection.h>
#include <system/collections/ienumerator.h>
#include <system/collections/list.h>
#include <system/shared_ptr.h>

#include <net/cookie.h>

namespace System { namespace Net {

// CookieCollection
// A list of cookies maintained in Sorted order. Only one cookie with matching Name/Domain/Path
class ASPOSECPP_SHARED_CLASS CookieCollection : public System::Collections::Generic::ICollection<System::SharedPtr<System::Net::Cookie>>
{
    typedef CookieCollection ThisType;
    typedef System::Collections::Generic::ICollection<System::SharedPtr<System::Net::Cookie>> BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

public:
    enum class Stamp
    {
        Check = 0,
        Set = 1,
        SetToUnused = 2,
        SetToMaxUsed = 3
    };

public:
    ASPOSECPP_SHARED_API int32_t get_Count() const override;
    ASPOSECPP_SHARED_API bool get_IsOtherVersionSeen();

    ASPOSECPP_SHARED_API CookieCollection();

    ASPOSECPP_SHARED_API System::SharedPtr<Cookie> idx_get(int32_t index);
    ASPOSECPP_SHARED_API System::SharedPtr<Cookie> idx_get(String name);

    ASPOSECPP_SHARED_API void Add(const System::SharedPtr<Cookie>& cookie) override;
    ASPOSECPP_SHARED_API bool Remove(const System::SharedPtr<Cookie>& cookie) override;
    ASPOSECPP_SHARED_API bool Contains(const System::SharedPtr<Cookie>& cookie) const override;
    ASPOSECPP_SHARED_API void Clear() override;

    ASPOSECPP_SHARED_API void Add(System::SharedPtr<CookieCollection> cookies);
    ASPOSECPP_SHARED_API DateTime TimeStamp(CookieCollection::Stamp how);
    ASPOSECPP_SHARED_API int32_t InternalAdd(System::SharedPtr<Cookie> cookie, bool isStrict);
    ASPOSECPP_SHARED_API int32_t IndexOf(System::SharedPtr<Cookie> cookie);
    ASPOSECPP_SHARED_API void RemoveAt(int32_t idx);
    ASPOSECPP_SHARED_API System::SharedPtr<Collections::Generic::IEnumerator<System::SharedPtr<Cookie>>> GetEnumerator() override;

private:
    System::SharedPtr<Collections::Generic::List<System::SharedPtr<Cookie>>> _list;
    DateTime _timeStamp;
    bool _hasOtherVersions;

    bool get_IsSynchronized();
    System::SharedPtr<Object> get_SyncRoot() const override ;

    void CopyTo(System::ArrayPtr<System::SharedPtr<Cookie>> array, int32_t index) override;
};

}} // namespace System::Net
