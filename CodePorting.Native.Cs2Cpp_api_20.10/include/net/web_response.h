﻿/// @file net/web_response.h
#pragma once

#include <cstdint>
#include <net/web_header_collection.h>
#include <system/idisposable.h>
#include <system/io/stream.h>
#include <system/object.h>
#include <system/shared_ptr.h>
#include <system/string.h>
#include <system/uri.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_CLASS ABSTRACT WebResponse : public System::IDisposable
{
    typedef WebResponse ThisType;
    typedef System::IDisposable BaseType;

    typedef ::System::BaseTypesInfo<BaseType> ThisTypeBaseTypesInfo;
    ASPOSECPP_SHARED_RTTI_INFO_DECL();

    FRIEND_FUNCTION_System_MakeObject;

public:
    virtual int64_t get_ContentLength() = 0;
    virtual String get_ContentType() = 0;
    virtual System::SharedPtr<Uri> get_ResponseUri() = 0;
    ASPOSECPP_SHARED_API virtual System::SharedPtr<WebHeaderCollection> get_Headers();
    ASPOSECPP_SHARED_API virtual bool get_SupportsHeaders();

    ASPOSECPP_SHARED_API virtual void Close();
    void Dispose() override;

    virtual System::SharedPtr<IO::Stream> GetResponseStream() = 0;

protected:
    WebResponse();
    virtual void Dispose(bool disposing);
};

}} // namespace System::Net
