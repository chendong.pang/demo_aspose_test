﻿/// @file net/web_request_methods.h
#pragma once

#include <system/object.h>
#include <system/string.h>

namespace System { namespace Net {

class ASPOSECPP_SHARED_API WebRequestMethods
{
public:
    class ASPOSECPP_SHARED_CLASS Ftp
    {
    public:
        static ASPOSECPP_SHARED_API const String DownloadFile;
        static ASPOSECPP_SHARED_API const String ListDirectory;
        static ASPOSECPP_SHARED_API const String UploadFile;
        static ASPOSECPP_SHARED_API const String DeleteFile;
        static ASPOSECPP_SHARED_API const String AppendFile;
        static ASPOSECPP_SHARED_API const String GetFileSize;
        static ASPOSECPP_SHARED_API const String UploadFileWithUniqueName;
        static ASPOSECPP_SHARED_API const String MakeDirectory;
        static ASPOSECPP_SHARED_API const String RemoveDirectory;
        static ASPOSECPP_SHARED_API const String ListDirectoryDetails;
        static ASPOSECPP_SHARED_API const String GetDateTimestamp;
        static ASPOSECPP_SHARED_API const String PrintWorkingDirectory;
        static ASPOSECPP_SHARED_API const String Rename;
    };

    class ASPOSECPP_SHARED_CLASS Http
    {
    public:
        static ASPOSECPP_SHARED_API const String Get;
        static ASPOSECPP_SHARED_API const String Connect;
        static ASPOSECPP_SHARED_API const String Head;
        static ASPOSECPP_SHARED_API const String Put;
        static ASPOSECPP_SHARED_API const String Post;
        static ASPOSECPP_SHARED_API const String MkCol;
    };

    class ASPOSECPP_SHARED_CLASS File
    {
    public:
        static ASPOSECPP_SHARED_API const String DownloadFile;
        static ASPOSECPP_SHARED_API const String UploadFile;
    };
};

}} // namespace System::Net
