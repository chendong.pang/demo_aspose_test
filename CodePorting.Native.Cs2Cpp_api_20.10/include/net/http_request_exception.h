﻿/// @file net/http_request_exception.h
#pragma once

#include <system/exceptions.h>
#include <system/string.h>

namespace System { namespace Net { namespace Http {

class Details_HttpRequestException;
using HttpRequestException = System::ExceptionWrapper<Details_HttpRequestException>;

class Details_HttpRequestException : public System::Details_Exception
{
    typedef System::Details_Exception Base;

public:
    using BaseType = Base;

    Details_HttpRequestException();
    Details_HttpRequestException(std::nullptr_t);
    Details_HttpRequestException(String message);
    Details_HttpRequestException(String message, Exception inner);
};

}}} // namespace System::Net::Http
