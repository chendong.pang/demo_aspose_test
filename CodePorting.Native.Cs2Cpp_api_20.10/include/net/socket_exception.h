﻿/// @file net/socket_exception.h
#pragma once

#include <cstdint>
#include <system/exceptions.h>

#include <net/sockets/socket_error.h>

namespace System { namespace Net { namespace Sockets {

class Details_SocketException;
using SocketException = System::ExceptionWrapper<Details_SocketException>;

class ASPOSECPP_SHARED_CLASS Details_SocketException : public System::Details_Exception
{
    typedef System::Details_Exception Base;

public:
    using BaseType = Base;

    ASPOSECPP_SHARED_API Details_SocketException(const System::String& message);
    ASPOSECPP_SHARED_API Details_SocketException(int32_t errorCode);
    ASPOSECPP_SHARED_API Details_SocketException(SocketError socketError);
    ASPOSECPP_SHARED_API Details_SocketException();
    ASPOSECPP_SHARED_API Details_SocketException(std::nullptr_t);

    ASPOSECPP_SHARED_API int32_t get_ErrorCode();

private:
    int32_t m_errorCode = 0;
};
}}} // namespace System::Net::Sockets
